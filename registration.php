<?php
require_once 'conf/conf.php';
include_once("conf/common_functions.php"); 
$_SESSION['OTPDetail'] = $_POST;
$customerId = sanitize_data(@$_POST['customerId']);
$dob = sanitize_data(@$_POST['dob']);

$policyNo = @$_SESSION['OTPDetail']['policyNo'];
$selectQuery = "SELECT COUNT(USERNAME) AS COUNT FROM RETUSERAUTH WHERE LOWER(USERNAME)='".strtolower(trim(@$_POST['email']))."'";
$sql = @oci_parse($conn, $selectQuery);	
@oci_execute($sql);
$record1 = oci_fetch_assoc($sql);
// GET USER ALREADY REGISTERED EMAIL ID 
$selectQuery2 = "SELECT USERNAME FROM RETUSERAUTH WHERE LOWER(USERNAME)='".strtolower(trim(@$_POST['email']))."'";
$sql2 = @oci_parse($conn, $selectQuery2);	
@oci_execute($sql2);
$record2 = oci_fetch_assoc($sql2);

require_once($apiurl);
$query2['CLNTNUM'] = $customerId;
$query2['CLTDOBX'] = $dob;

$policyNo= $query2['CHDRNUM'] = $policyNo; 
$dataArray=getPolicyEnquiry($query2); 
$dataRecord = @$dataArray['dataArray'][0];
 @$fromTyp=sanitize_data(@$_POST['formtype']);
 @$chkmobil=sanitize_data(@$_POST['chkmobile']);
 
 @$textfield2hide=sanitize_data(@$_POST['textfield2hide']);
 @$chkemail=sanitize_email(@$_POST['chkemail']);
 
 if(@$fromTyp == 'newpolicySameAccount'){
	if($record1['COUNT'])
	{
		if(is_numeric(@$textfield2hide) && @$chkmobil=='on')
		{
			echo OTP::sendOTPSms() ? 'successSameAccount' : 'failureSameAccount';
		}
		else if(strstr (@$textfield2hide, '@') && @$chkemail=='on')
		{    
			// Get email id from web services to send otp. will send otp only on email comming from web service
			$datdRecordEmail = $dataRecord['BGEN-EMAIL']['#text'] ;
			echo OTP::sendOTPMail($datdRecordEmail) ? 'successSameAccount' : 'failureSameAccount';
		}
		$_SESSION['successSameAccount'] ='successSameAccount';
	}
	exit;
 }
 else if($record1['COUNT'])
{
	echo 'emailareadyregistered';
	exit;
}
else if(@$_POST['formtype'] == 'newpolicyNewAccount'){
	 if(is_numeric(@$textfield2hide) && @$chkmobil=='on')
		{
			echo OTP::sendOTPSms() ? 'policyRegWithEmailidSuccess' : 'failurepolicyRegWithEmailid';
		}
		else if(strstr (@$textfield2hide, '@') && @$chkemail=='on')
		{  
			// Get email id from web services to send otp. will send otp only on email comming from web service 
			$datdRecordEmail = $dataRecord['BGEN-EMAIL']['#text'] ;
			echo OTP::sendOTPMail($datdRecordEmail) ? 'policyRegWithEmailidSuccess' : 'failurepolicyRegWithEmailid';
		}
		$_SESSION['newpolicyNewAccount'] ='newpolicyNewAccount';
	exit;
 } 
else if($dataRecord['BGEN-EMAIL']['#text'] != $_POST['email']){
	 // check if user edited the email id comming from web services.
	 echo 'policyRegWithEmailid';
	 $_SESSION['policyRegWithEmailid'] = $dataRecord['BGEN-EMAIL']['#text'];
	 exit;
 }
else{
/******************************* Insert OTP detail into db and send message or email ************************************************/
if(is_numeric(@$textfield2hide) && @$chkmobil=='on')
{
    echo OTP::sendOTPSms() ? 'success' : 'failure';
}
else if(strstr (@$textfield2hide, '@') && @$chkemail=='on')
{    
    echo OTP::sendOTPMail() ? 'success' : 'failure';
} 
}
exit;
/******************************* Insert OTP detail into db and send message or email ************************************************/

/**
 * OTP class.
 */
class OTP
{
    /**
     * Send OTP Mail.
     * @global string $conn
     */
    static function sendOTPMail($datdRecordEmail=null)
    {
        global $conn;
        $email      = sanitize_email($_POST['email']);
        $mobile     = sanitize_data($_POST['mobile']);
        $policyNo   = sanitize_data($_POST['policyNo']);
        $customerId = sanitize_data($_POST['customerId']);
        $OTPaddress = sanitize_data($_POST['textfield2hide']);
        $firstName  = sanitize_username($_POST['firstName']);
        $lastName   = sanitize_username($_POST['lastName']);
 		$name=$firstName." ".$lastName;
        // this condition check if user changed the email id comming from web service. because we will send otp only on registered email.
		if(@$_POST['formtype'] == 'newpolicyNewAccount'){
			if($datdRecordEmail != $email){
				 $email = $datdRecordEmail;
			}
		}
		
		if(isset($mobile)){
			$_SESSION['mobile']=$mobile;	
		}
		if(isset($email)){
			$_SESSION['email']=$email;	
		}
		
        if(@$_POST['chkemail']=='on')
        {
            $OTPType    = 'email';
        }
        if(@$_POST['chkmobile']=='on')
        {
            $OTPType    = 'mobile';
        }
		
		 if(@$_POST['formtype'] == 'newpolicySameAccount'){
			
			if($datdRecordEmail != $email){
				  $email = $datdRecordEmail;
			}
		 }
        $code = self::getRandomCode();

        $Mailtemplates=self::GetTemplateContentByTemplateName2('RET_OTP_EMAIL');
		$to = $email;
        $subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
        //$message  = stripslashes($Mailtemplates[0]['CONTENT']);   
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
		$message  = $Mailtemplates[0]['LARGECONTENT']?stripslashes($Mailtemplates[0]['LARGECONTENT']->load()):'';
        $message  = str_replace("##OTP", $code ,$message);			
        $message  = str_replace("##USER", $name ,$message);			
        $headers  = 'From:'.stripslashes($Mailtemplates[0]['FROMNAME']).' <'.stripslashes($Mailtemplates[0]['FROMEMAIL']). "> \r\n" .
                'MIME-Version: 1.0' . "\r\n" .
                'Content-type: text/html; charset=iso-8859-1';

        if(mail($to, $subject, $message, $headers))
        {  		 
			$createdOn = time();
            $query = "SELECT COUNT(*) as count FROM OTP_DETAIL WHERE CUSTOMER_ID=$customerId AND POLICY_NUM=$policyNo AND MOBILE= $mobile AND EMAIL='$email' AND STATUS=1";
            $sql1 = @oci_parse($conn, $query);	
			@oci_execute($sql1);
            $record = oci_fetch_assoc($sql1);
            
            if($record['COUNT'])
            {
                $sql = "UPDATE OTP_DETAIL SET CREATED_ON=$createdOn,OTP=$code,OTP_TYPE='$OTPType' WHERE CUSTOMER_ID=$customerId AND POLICY_NUM=$policyNo AND MOBILE= $mobile AND EMAIL='$email' AND STATUS=1";
            }
            else
            {
                 $sql = "INSERT INTO OTP_DETAIL (CUSTOMER_ID,POLICY_NUM,MOBILE,EMAIL,CREATED_ON,STATUS,OTP,OTP_TYPE) values($customerId, $policyNo, $mobile, '".$email."', $createdOn, 1, $code, '".$OTPType."')";                                 
            }
			$stdid1 = @oci_parse($conn, $sql);
            $r = @oci_execute($stdid1);
			
			$usertype = 'RETAIL';
			$reasontomail = 'REGISTRATIONOTP';
			$sendby = '';
			$entryTime=date('d-M-Y');
			$status = 'ACTIVE';
			$sendfrom = 'wellness@religare.com';
			$ipAddress = get_client_ip();
			//query to insert records
			$sqlEmailLog="INSERT INTO PPHCMAILLOG (MAILID,MAILTO,SUBJECT,MESSAGE,USERTYPE,REASONTOMAIL,SENDBY,SENDDATE,STATUS,SENDFROM,IPADDRESS) values(PPHCMAILLOG_SEQ.nextval,q'[".$to."]','".@$subject."','".@$message."','".@$usertype."','".@$reasontomail."','".@$sendby."','".@$entryTime."','".@$status."','".@$sendfrom."','".@$ipAddress."') ";		
			$stdidEmailExe = @oci_parse($conn, $sqlEmailLog);
			$r = @oci_execute($stdidEmailExe);
			
            return true;
        }
        return false;
    }

    /**
     * Send OTP Sms.
     * @global string $conn
     */
    static function sendOTPSms()
    {
        global $conn;
        $email      = sanitize_email($_POST['email']);
        $mobile     = sanitize_data($_POST['mobile']);
        $policyNo   = $_SESSION['policyNumber'];
        $customerId = sanitize_data($_POST['customerId']);
        $OTPaddress = sanitize_data($_POST['textfield2hide']);
        if(isset($mobile)){
			$_SESSION['mobile']=$mobile;	
		}
		if(isset($email)){
			$_SESSION['email']=$email;	
		}
        if(@$chkemail=='on')
        {
            $OTPType    = 'email';
        }
        if(@$chkmobil=='on')
        {
            $OTPType    = 'mobile';
        }
                
        $code = self::getRandomCode();
        //$Mailtemplates=self::GetContentBySMSTemplateName('OTP_CODE');
        //$message  = stripslashes($Mailtemplates[0]['CONTENT']); 
        $message = "One Time Password for Registration is ##code. Please use the password to complete the process. Pls do not share this with anyone.";
        $message  = str_replace("##code", $code ,$message);	
        
        $smsUrl = "http://luna.a2wi.co.in:7501/failsafe/HttpLink?aid=508460&pin=rh12";
        $mnumber= $mobile;
        $stringURL="&mnumber=91".trim($mnumber)."&message=".urlencode($message);
        $apiUrl= $smsUrl.@$stringURL;
        //file_get_contents($apiUrl);
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $apiUrl);
        curl_setopt($ch, CURLOPT_HTTP_VERSION, 1.0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, false);
        if(curl_exec($ch))
        {             
            $ch_info=curl_getinfo($ch);
            curl_close($ch);
            $createdOn = time();
            $query = "SELECT COUNT(*) as count FROM OTP_DETAIL WHERE CUSTOMER_ID=$customerId AND POLICY_NUM=$policyNo AND MOBILE= $mobile AND EMAIL='$email' AND STATUS=1";
            $sql1 = @oci_parse($conn, $query);	
			@oci_execute($sql1);
            $record = oci_fetch_assoc($sql1);
            
            if($record['COUNT'])
            {
                $sql = "UPDATE OTP_DETAIL SET CREATED_ON=$createdOn,OTP=$code,OTP_TYPE='$OTPType' WHERE CUSTOMER_ID=$customerId AND POLICY_NUM=$policyNo AND MOBILE= $mobile AND EMAIL='$email' AND STATUS=1";
            }
            else
            {
                 $sql = "INSERT INTO OTP_DETAIL (CUSTOMER_ID,POLICY_NUM,MOBILE,EMAIL,CREATED_ON,STATUS,OTP,OTP_TYPE) values($customerId, $policyNo, $mobile, '".$email."', $createdOn, 1, $code, '".$OTPType."')";                                 
            } 
            //echo $sql; die;
            $stdid = @oci_parse($conn, $sql);
            $r = @oci_execute($stdid); 

			$usertype = 'RETAIL';
			$reasontosms = 'REGISTRATIONOTP';
			$sendby = '';
			$entryTime=date('d-M-Y');
			$status = 'ACTIVE';
			$sendfrom = 'wellness@religare.com';
			$ipAddress = get_client_ip();
			//query to insert records to create SMS Logs
			$sqlSmsLog="INSERT INTO PPHCSMSLOG (SMSID,SMSTO,MESSAGE,USERTYPE,REASONTOSMS,SENDBY,SENDDATE,STATUS,SENDFROM,IPADDRESS,EMAIL) values(PPHCSMSLOG_SEQ.nextval,q'[".$mobile."]','".@$message."','".@$usertype."','".@$reasontosms."','".@$sendby."','".@$entryTime."','".@$status."','".@$sendfrom."','".@$ipAddress."','".@$email."') ";
			$sqlSmsLogExe = @oci_parse($conn, $sqlSmsLog);
			$r = @oci_execute($sqlSmsLogExe); 
            return true;
        }
        return false;
    }

    /**
     * Get 7 digit Random Code.
     * @return int
     */
    static function getRandomCode()
    {
        $number = rand(1,9);
        for ($i = 1; $i <= 6; $i++){
            $number .= rand(0,9);
        }
        return (int)$number;
    }
    
    static function GetContentBySMSTemplateName($templateName){
	global $conn;
	$dataArray=array();
	    $query="SELECT TEMPLATEID,SUBJECT,CONTENT,FROMEMAIL,FROMNAME,TEMPLATENAME from CRMSMSTEMPLTE where TEMPLATENAME='".$templateName."' ";
		
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
    }
    static function GetTemplateContentByTemplateName2($templateName){
	global $conn;
	$dataArray=array();
		 $query="SELECT TEMPLATEID,SUBJECT,CONTENT,LARGECONTENT,FROMEMAIL,FROMNAME,TEMPLATENAME from CRMEMAILTEMPLTE where TEMPLATENAME='".$templateName."' ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
    }
}
?>
