<?php 
include("conf/conf.php");
include("conf/common_functions.php");
include("inc/inc-header-registration.php");
//include_once($apiurl);
/*$browser = get_browser(null, true);
$_SESSION["tok"] = "Qwso0rdjyd7wrjvrmc39dnmcm4m";    /// add new line by wasim
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);*/
$username='';
if(isset($_POST['policyNumber']) && isset($_POST['customerId']) && $_POST['csrf']==$_SESSION["tok"]){
	
	/*$query['policyNo']=$_POST['policyNumber'];
	$renewalArray = getRenewalMemberDetails($query);
	$query['CLNTNUM'] = $_POST['customerId'];
    $query['CLTDOBX'] = '';//$dob;
    $query['CHDRNUM'] = $_POST['policyNumber']; 
	$query['policyNo']= $_POST['policyNumber'];
	$dataArray=getPolicyEnquiry($query); 
	$dataRecord = @$dataArray['dataArray'][0];
	//echo "<pre>".$dataRecord['BGEN-EMAIL']['#text'];//print_r($dataRecord);die;
	$username=($dataRecord['BGEN-EMAIL']['#text']!='')?'Your user name is '.$dataRecord['BGEN-EMAIL']['#text']:'User Name Not Found.';
	*/
	$select_corp_q = sprintf("SELECT EMAIL FROM CRMEMPLOYEELOGIN  WHERE POLICYNUMBER = '%s' AND CUSTOMERID = '%s' AND STATUS='ACTIVE'", $_POST['policyNumber'], $_POST['customerId']);
    $select_corp_p = oci_parse($conn, $select_corp_q);
    oci_execute($select_corp_p);
    $select_corp_f = oci_fetch_assoc($select_corp_p);
	//print_r($select_corp_f);die;
	$username=$select_corp_f['EMAIL'];
	if($username==''){
		$select_ret_q = sprintf("SELECT EMAILID FROM RETUSERPOLICYMAP  WHERE POLICYNUM = '%s' AND CUSTOMERID = '%s' ", $_POST['policyNumber'], $_POST['customerId']);
		$select_ret_p = oci_parse($conn, $select_ret_q);
		oci_execute($select_ret_p);
		$select_ret_f = oci_fetch_assoc($select_ret_p);
		//print_r($select_corp_f);die;
		$username=$select_ret_f['EMAILID'];
	}
	if($username!=''){
		$usernameArr=explode("@", $username);
		$username='Your user name is xxxxxx'.substr($usernameArr[0], -5)."@".$usernameArr[1];
	}else{
		$username='User Name Not Found.';
	}
}
?>
<style>
div.error {
	position:absolute;
	margin-top:3px;
	color:red;
	padding:3px;
	text-align:left;
	z-index:1;
	margin-left: -47px;
}
.owl-theme .owl-controls {
margin-top: -146px;
}
</style>
</head>
<body>
<!-- header -->
  <div class="headerMain">
<?php include('pphcheader.php'); ?>
</div>
<script src="js/bootstrap.min.js"></script>

<!-- body container -->
<section id="body_container"> 
  <div class="right-Cont ">
      <h1>Get User Name</h1>
    <!-- Heding top  closed-->
    <div class="clearfix"></div>
    <div class="spacer4"></div>
    <!-- Step start -->
    <div class="right-Cont-step1 spacer-margin-top"><h2>Help us identifying your details-</h2>
	<div class="notValid" style="min-height:10px; color:black"><?php if($username!='')echo $username; ?></div>
<form action="#" name="getUserName" id="getUserName" method="post">
    <input type="hidden" name="csrf" value="<?php echo $_SESSION['tok'] ?>">   <!-- line added by wasim  -->
      <div class="middleContainerBox">
        	<div class="graytxtBox3 space-marging "><span class="txtIcon3"></span>
              <div class="txtMrgn "><img src="images/question_mark.png" class="ques_mark" id="policy_no_help">                  
                  <input type="text" value="<?php echo isset($_POST['policyNumber'])?$_POST['policyNumber']:''; ?>" name="policyNumber" class="txtfieldFull" AUTOCOMPLETE="OFF"  id="policyNumber" maxlength="8" placeholder="Policy No *">
              </div>
             </div>
            <div class="graytxtBox3"><span class="txtIcon1"></span>
              <div class="txtMrgn"><img src="images/question_mark.png" class="ques_mark" id="customer_id_help">
                  <input type="text" value="<?php echo isset($_POST['customerId'])?$_POST['customerId']:''; ?>" name="customerId" class="txtfieldFull"  id="customerId" AUTOCOMPLETE="OFF" maxlength="8" placeholder="Customer ID *">
              </div>
            </div>
        </div>
        <div class="right-Cont-step1-button space-marging1">
		<a style="margin-right:10px;" onClick="location='index.php'"><img src="images/arrowleft.png"  border="0" alt="" title=""> Back </a> 
		<a id="btnClick">Proceed <img src="images/arrow.png"  border="0" alt="" title=""></a> 
		
      </div>
</form>
         <div class="spacer4"></div>
    </div>
    </div>
  <!-- Left container -->
    <div class="Left-Cont">
       <?php include("inc/inc.left-registration.php");?>
    </div>
  </div>
</section>
<div class="clearfix"></div>
<script type="text/javascript">
$("input").keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
		$('#btnClick').trigger('click');   
    }
});
</script>
<!-- Demo --> 
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/additional-methods.min.js" type="text/javascript"></script>
   <script type="text/javascript">
		$("#getUserName").validate({
			ignore: [], // this will allow to validate any hidden field inside a form
		rules :{
			"policyNumber" : {
				required : true,
				number: true,
				minlength: 8,
			    maxlength: 8,
			},
			"customerId" : {
			   required : true,
			   minlength: 8,
			   maxlength: 8,
			}

		},
		messages :{
			"policyNumber" : {
				required : 'Please enter policy number',
				number: 'Please enter valid policy number',
				minlength:'Please enter valid policy number',	
				maxlength:'Please enter valid policy number',	
			},
			"dob" : {
				required : 'Please enter DOB'
			},
			"customerId" :{
				required:'Please enter customer ID',
				minlength:'Please enter valid customer id',	
				maxlength:'Please enter valid customer id',	
			
			},
			
		},
		errorElement: "div"		
});
jQuery.validator.addMethod("customDateValidator", function(value, element) {
        // parseDate throws exception if the value is invalid
        try{jQuery.datepicker.parseDate( 'yy-mm-dd', value);return true;}
        catch(e){return false;}
    },
    "Please enter a valid date"
);
</script>
<script type="text/javascript">
$(function() {
 $('#btnClick').click(function(){
	$('#getUserName').validate();
	if ($('#getUserName').valid()) // check if form is valid
	{
		document.getElementById('getUserName').submit();
	}
	else 
	{
		return false;
	}
});
});
</script>
<script src="js/owl.carousel.js" type="text/javascript"></script>	
<script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
		  navigation : true,
		  slideSpeed : 300,
		  paginationSpeed : 400,
		  singleItem : true
      });
    });
</script>	

<?php 
if($browser['browser'] == 'IE' && $browser['version'] == '8.0'){ ?>
<script type="text/javascript" src="js/jquery.js"></script>   
<?php } else {  ?>
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<?php } ?>
<script src="js/datepiker.js"></script> 
<script>
 var DT = jQuery.noConflict();
DT(function() {
DT('#datepicker').datepicker( {
        changeMonth: true,
        changeYear: true,
	    yearRange: "-99:-0",
	    dateFormat: "dd/mm/yy"
 
    });
	DT('#dob').datepicker( {
        changeMonth: true,
        changeYear: true,
	    yearRange: "-99:-0",
	    dateFormat: "yy-mm-dd"
 
    });
	
});
</script>
<style>
    .popover{max-height: 170px;height: auto!important; border-radius: 6px;padding: 0;width:auto!important; max-width: none; left:auto !important;}
    .popover .popover-title {max-height: 170px;height: auto!important;padding: 0;width:auto!important; max-width: none; left:auto !important;}
</style>
<script>
$(document).ready(function(){
    $('#customer_id_help').popover({
        placement : 'auto',
        trigger : 'click hover focus',
        html : true,
        title : '<img src="images/customer-id-help.png">',
    }); 
    $('#policy_no_help').popover({
        placement : 'auto',
        trigger : 'click hover focus',
        html : true,
        title : '<img src="images/policy-no-help.png">',
    });   
});
</script>
<!-- body container --> 
<!-- footer -->
<?php include("inc/inc.ft-registration.php"); ?>
<!-- footer closed -->
</div>
</body>
</html>