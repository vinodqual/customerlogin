<?php
/*****************************************************************************
* COPYRIGHT
* Copyright 2016 Qualtech Consultants Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
* $Author: Chandan Singh $
*
****************************************************************************/ 
include("conf/conf.php");
include("conf/common_functions.php");
include("inc/inc-header-registration.php");
$browser = get_browser(null, true);
?>
<style>
div.error {
	position:absolute;
	margin-top:3px;
	color:red;
	padding:3px;
	text-align:left;
	z-index:1;
	margin-left: -47px;
}
.owl-theme .owl-controls {
margin-top: -146px;
}
</style>
</head>
<body>
<!-- header -->
  <div class="headerMain">
<?php include('pphcheader.php'); ?>
</div>
<link rel="stylesheet" type="text/css" href="css/datepiker.css" >
<script src="js/bootstrap.min.js"></script>

<!-- body container -->
<section id="body_container"> 
  <div class="right-Cont ">
      <h1>Individual Policy Registration</h1>
    <!-- Heding top  closed-->
    <div class="clearfix"></div>
    <div class="spacer4"></div>
    <!-- Step start -->
    <div class="right-Cont-step1 spacer-margin-top"><h2>Help us identifying your details-</h2>
	<div class="notValid" style="color: red; min-height:10px;"></div>
<form action="#" name="registrationForm" id="registrationForm" method="post">
      <div class="middleContainerBox">
        	<div class="graytxtBox3 space-marging "><span class="txtIcon3"></span>
              <div class="txtMrgn "><img src="images/question_mark.png" class="ques_mark" id="policy_no_help">                  
                  <input type="text" value="" name="policyNumber" class="txtfieldFull" AUTOCOMPLETE="OFF"  id="policyNumber" maxlength="8" placeholder="Policy No *">
              </div>
             </div>
            
            <div class="graytxtBox3"><span class="txtIcon4"></span>
              <div class="txtMrgn"><input type="text" value="" name="dob" class="txtfieldFull datepicker" id="dob" AUTOCOMPLETE="OFF" placeholder="DOB *" readonly >
              </div>                  
            </div>
            
            <div class="graytxtBox3"><span class="txtIcon1"></span>
              <div class="txtMrgn"><img src="images/question_mark.png" class="ques_mark" id="customer_id_help">
                  <input type="text" value="" name="customerId" class="txtfieldFull"  id="customerId" AUTOCOMPLETE="OFF" maxlength="8" placeholder="Customer ID *">
              </div>
            </div>
        </div>
        <div class="right-Cont-step1-button space-marging1">
		<a style="margin-right:10px;" href="register.php" onClick="$('#login_section').show();
		$('#forgot_pass_section').hide();"><img src="images/arrowleft.png"  border="0" alt="" title=""> Back </a> 
		<a id="btnClick">Proceed <img src="images/arrow.png"  border="0" alt="" title=""></a> 
		<div id="ajaxLoading" style="display:none; float:right; position:absolute; margin-left:10px;">
		Please wait...<img class="doc_load_img" alt="Loading.." src="images/ajax-loader_12.gif">
		</div>
      </div>
</form>
         <div class="spacer4"></div>
    </div>
    </div>
  <!-- Left container -->
    <div class="Left-Cont">
       <?php include("inc/inc.left-registration.php");?>
    </div>
  </div>
  <?php include('popup.php'); ?>
</section>
<div class="clearfix"></div>
<script type="text/javascript">
avgrund.activate();
$("input").keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
		$('#btnClick').trigger('click');   
    }
});
</script>
<!-- Demo --> 
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/additional-methods.min.js" type="text/javascript"></script>
   <script type="text/javascript">
		$("#registrationForm").validate({
			ignore: [], // this will allow to validate any hidden field inside a form
		rules :{
			"policyNumber" : {
				required : true,
				number: true,
				minlength: 8,
			    maxlength: 8,
			},
			"dob" : {
			   required : true,
			   customDateValidator: true,
			},
			"customerId" : {
			   required : true,
			   minlength: 8,
			   maxlength: 8,
			}

		},
		messages :{
			"policyNumber" : {
				required : 'Please enter policy number',
				number: 'Please enter valid policy number',
				minlength:'Please enter valid policy number',	
				maxlength:'Please enter valid policy number',	
			},
			"dob" : {
				required : 'Please enter DOB'
			},
			"customerId" :{
				required:'Please enter customer ID',
				minlength:'Please enter valid customer id',	
				maxlength:'Please enter valid customer id',	
			
			},
			
		},
		errorElement: "div"		
});
jQuery.validator.addMethod("customDateValidator", function(value, element) {
        // parseDate throws exception if the value is invalid
        try{jQuery.datepicker.parseDate( 'yy-mm-dd', value);return true;}
        catch(e){return false;}
    },
    "Please enter a valid date"
);
</script>
<script type="text/javascript">
$(function() {
 $('#btnClick').click(function(){
	$('#registrationForm').validate();
	if ($('#registrationForm').valid()) // check if form is valid
	{
		$.ajax({
		type    : 'POST',
		url     : 'checkPolicy.php', 
		data    : $('#registrationForm').serialize(),
		beforeSend: function () {
		$("#ajaxLoading").css("display", "inline");
		$("#ajaxLoading").show();
	   },
		success : function (data){
		if(data == 'success')
		{
			window.location.href = "individual-policy-registration-1.php";
		}  
		else if(data == 'failure')
		{
			$(".notValid").html("You are already registered, <a href='index.php'>click here</a> to login.").show();
			//window.location.href = "index.php";
		}
		else if(data == 'invaliddata') 
		{
			
			$(".notValid").text("Policy number or customer id is not valid.").show();
			return false;
		}
		else if(data == 'invalidpolicy') 
		{
			//$(".notValid").text("Policy no. not found, Please contact customer care").show();
			$('#errorFeedbackAnchor')[0].click();
			return false;
		}
		},
		complete: function(){
		$('#ajaxLoading').hide();
		}
		});
	}
	else 
	{
		return false;
	}
});
});
</script>
<script src="js/owl.carousel.js" type="text/javascript"></script>	
<script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
		  navigation : true,
		  slideSpeed : 300,
		  paginationSpeed : 400,
		  singleItem : true
      });
    });
</script>	

<?php 
if($browser['browser'] == 'IE' && $browser['version'] == '8.0'){ ?>
<script type="text/javascript" src="js/jquery.js"></script>   
<?php } else {  ?>
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<?php } ?>
<script src="js/datepiker.js"></script> 
<script>
 var DT = jQuery.noConflict();
DT(function() {
DT('#datepicker').datepicker( {
        changeMonth: true,
        changeYear: true,
	    yearRange: "-99:-0",
	    dateFormat: "dd/mm/yy"
 
    });
	DT('#dob').datepicker( {
        changeMonth: true,
        changeYear: true,
	    yearRange: "-99:-0",
	    dateFormat: "yy-mm-dd"
 
    });
	
});
</script>
<style>
    .popover{max-height: 170px;height: auto!important; border-radius: 6px;padding: 0;width:auto!important; max-width: none; left:auto !important;}
    .popover .popover-title {max-height: 170px;height: auto!important;padding: 0;width:auto!important; max-width: none; left:auto !important;}
</style>
<script>
$(document).ready(function(){
    $('#customer_id_help').popover({
        placement : 'auto',
        trigger : 'click hover focus',
        html : true,
        title : '<img src="images/customer-id-help.png">',
    }); 
    $('#policy_no_help').popover({
        placement : 'auto',
        trigger : 'click hover focus',
        html : true,
        title : '<img src="images/policy-no-help.png">',
    });   
});
</script>
<!-- body container --> 
<!-- footer -->
<?php include("inc/inc.ft-registration.php"); ?>
<!-- footer closed -->
</div>
</body>
</html>