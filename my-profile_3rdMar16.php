<?php
include("inc/inc.hd.php");
include_once("conf/session.php");
include_once("conf/conf.php"); //include configuration file to use databse connectivity 
include_once("conf/common_functions.php");
$fetchPolListByUserId=fetchListCond("RETUSERMASTER"," WHERE USERID=".@$_SESSION['USERID']."");//query 
$name = ucfirst(@$fetchPolListByUserId[0]['TITLE']).' '.ucfirst(@$fetchPolListByUserId[0]['FIRSTNAME']).' '.ucfirst(@$fetchPolListByUserId[0]['LASTNAME']);
$dob = date("d-m-Y", strtotime(@$fetchPolListByUserId[0]['BIRTHDT']));
$tab=base64_decode(sanitize_data(@$_REQUEST['tab']));
$successmsg='';
$rediret=0;
if($_SESSION['LOGINTYPE']=='RETAIL'){
	if(isset($_POST["newpass"]) && $_POST['newpass']!='') { //check condition to insert or update data into/database
	
		require_once 'passwordverify/PkcsKeyGenerator.php';
		require_once 'passwordverify/DesEncryptor.php';
		require_once 'passwordverify/PbeWithMd5AndDes.php';
		
	$newpass = $_POST['newpass'];
	$oldpass=base64_decode($_POST['oldpass']);
	
	$encryptOldPass = PbeWithMd5AndDes::encrypt($oldpass, $keystring);
	$decryptOldPass = PbeWithMd5AndDes::decrypt($encryptOldPass, $keystring);
	$encryptNewPass = PbeWithMd5AndDes::encrypt($newpass, $keystring);
	$decryptNewPass = PbeWithMd5AndDes::decrypt($encryptNewPass, $keystring);
	
		if($oldpass == $decryptOldPass && $newpass == $decryptNewPass){
			$id=@$_SESSION['USERID'];
			$check_password=fetchListCond("RETUSERAUTH"," WHERE USERID='".$id."'");
			$count = count($check_password[0]['PASSWORD']);
			$dbDecriptPass = PbeWithMd5AndDes::decrypt($check_password[0]['PASSWORD'], $keystring);
			if(($count>0) && ($dbDecriptPass == $_POST['oldpass'])){
				   $sql="UPDATE  RETUSERAUTH SET PASSWORD='".@$encryptNewPass."',ISPASSWORDCHANGED='YES' WHERE USERID='".@$id."'"; //query to Change password
							$stdid = @oci_parse($conn, $sql);
							$r = @oci_execute($stdid);
							$_SESSION['ISPASSWORDCHANGED']='YES';
						//$email=$check_password[0]['EMAILID'];
						//$username=$check_password[0]['USERNAME'];
						//SendMailToUser($email,$username,$passtosend);  //function to send mail to the user on updating password	
						$successmsg='<div><font color="green">Password Updated Successfully</font></div>'; 
						$tab = 'Y3Bhc3M=';		
						$tab=base64_decode(sanitize_data($tab));//error message when password entered wrong by user
					$rediret=1;
					}
					else
					{
						$successmsg='<div><font color="red">Wrong old password!</font></div>'; 
						$tab = 'Y3Bhc3M=';		
						$tab=base64_decode(sanitize_data($tab));//error message when password entered wrong by user
						//exit;
					}
					
		}else{
						$successmsg='<div><font color="red">Wrong old password!</font></div>';  
						$tab = 'Y3Bhc3M=';
						$tab=base64_decode(sanitize_data($tab));
					}
	}
}
if($_SESSION['LOGINTYPE']=='CORPORATE'){
	if(isset($_POST["newpass"]) && $_POST['newpass']!='') {

		$newpassword=$_POST['newpass'];		//decode password
		$oldpassword=$_POST['oldpass'];
		$query1 = sprintf("SELECT * FROM CRMEMPLOYEELOGIN  WHERE EMAIL like '%s' AND ENCRYPTPASSWORD like '%s'",
		sanitize_username(@$_SESSION['userName']),
		md5(@$oldpassword));
		// Parse Query 
		$login = @oci_parse($conn, $query1);
		// Execute Query
		@oci_execute($login);
		// Fetch Data IN Associative Array
		$nrows1 = @oci_fetch_all($login, $res1);
		if($nrows1>0 && strlen($newpassword)>5)	{	
		$query=sprintf("UPDATE  CRMEMPLOYEELOGIN SET ENCRYPTPASSWORD='".md5(@$newpassword)."',PASSWORD='".@$newpassword."',ISPASSWORDCHANGED='YES' WHERE EMAIL like '%s' ",		sanitize_username(@$_SESSION['userName']));
		$stdid = @oci_parse($conn, $query);
		$r = @oci_execute($stdid);	
		$_SESSION['ISPASSWORDCHANGED']='YES';
		$successmsg='<div><font color="green">Password Updated Successfully</font></div>'; 
		$tab = 'Y3Bhc3M=';		
		$rediret=1;
		$tab=base64_decode(sanitize_data($tab));//error message when password entered wrong by user
	}else{
		$successmsg='<div><font color="red">Wrong old password!</font></div>';  
		$tab = 'Y3Bhc3M=';
		$tab=base64_decode(sanitize_data($tab));
	}
	
	}
}
if($rediret==1){
	echo '<meta http-equiv="refresh" content="3;url=dashboard.php" />';
}
 ?>
<style>
div.error {
	position:absolute;
	margin-top:8px;
	color:red;
	padding:3px;
	text-align:left;
	z-index:1;
	margin-left: -18px;
	font-size:14px;
}
.demo{
	color:red; 
	padding-top:10px;
}
.demosuccess{
	color:#5e9d2d;
	padding-top:10px;
}
</style>

<section id="middleContainer">
  <div class="container-fluid">
    <div class="middlebox">
      <div class="col-md-9">
        <div class="middlebox-left">
          <ul class="nav nav-tabs responsive" id="myTab">
            <li class="test-class <?php if(empty($tab) || $tab=='' || $tab!='cpass'){ echo "active";  } ?>"><a class="deco-none misc-class" href="#buyPlan"> My Profile</a></li>
            <li class="<?php if(!empty($tab) && $tab!='' && $tab=='cpass'){ echo "active";  } ?>"><a class="deco-none" href="#myPlans">Change Password</a></li>
          </ul>
          <div class="tab-content responsive">
            <div class="tab-pane <?php if(empty($tab) || $tab=='' || $tab!='cpass'){ echo "active";  } ?>" id="buyPlan">            
            <div class="tab-paneIn">                          
              <div class="clearfix"></div>              
              <div class="tab-mid-container">
              <?php  echo $successmsg; ?>
                <table class="responsive" width="100%">
					<tr>
                    <th colspan="2">My Profile</th>
					</tr>
					<tr>
					<td>Name</td>
					<td><?php echo @$name; ?></td>
					</tr>
					<tr>
					<td>DOB</td>
					<td><?php echo @$dob; ?></td>
					</tr>
					<tr>
					<td>Email</td>
					<td><?php echo @$fetchPolListByUserId[0]['EMAILID'];?></td>
					</tr>
					<tr>
					<td>Mobile</td>
					<td><?php echo @$fetchPolListByUserId[0]['CONTACTNUM']? @$fetchPolListByUserId[0]['CONTACTNUM']:'NA';?></td>
					</tr>
					<tr>
					<td>Contact Phone No</td>
					<td><?php echo @$fetchPolListByUserId[0]['CONTACTPHONENUM']? @$fetchPolListByUserId[0]['CONTACTPHONENUM']: 'NA';?></td>
				  </tr>
                </table>
              </div>
              <div class="clearfix"></div>
              </div>            
              <div class="colfull">
			</div>
              <div class="clearfix"></div>
            </div>
			
            <div class="tab-pane <?php if(!empty($tab) && $tab!='' && $tab=='cpass'){ echo "active";  } ?>" id="myPlans">           
            	<div class="tab-paneIn" id="myplanForm">
				<form name="changePassForm" id="changePassForm" method="POST" action="">                
              	  <div class="col-sm-12">
            	<div class="title-bg textleft">
               <h1>Change Password <?php if($_SESSION['ISPASSWORDCHANGED']=='NO' || $_SESSION['ISPASSWORDCHANGED']=='') { echo "(Please reset your password first!)"; } ?></h1>
                </div>               
                <div class="grayBorder">
                	<h2>Fill Your Details</h2>
					 <div class="demo"><?php if(@$successmsg !=''){echo @$successmsg; @$successmsg=''; }?></div>
                    <div class="myPlanForm">                    
                    	<div class="myPlanformBox myPlanLeft">
                        	<label>Old Password<span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                            	  <input type="password" name="oldpass" id="oldpass" class="txtField password" value="" autocomplete="off" >
                            	</div>
                            </div>
                        <div class="clearfix"></div></div> 
				
                        <div class="myPlanformBox">
                        	<label>New Password <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                            	  <input type="password" name="newpass" id="newpass" class="txtField password" value="<?php echo @$_POST['newpass']; ?>" autocomplete="off" >
                            	</div>
								
                            </div>
							
                        <div class="clearfix"></div><span id="alertpasstext"></span></div>
						
						<div class="myPlanformBox myPlanLeft">
                        	<label>Confirm Password <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                            	  <input type="password" name="confirmpass" id="confirmpass" class="txtField password" value="<?php echo @$_POST['confirmpass']; ?>" autocomplete="off">
                            	</div>
                            </div>
							<div class="showpassword" style="float:right;"><input type="checkbox" id="showHide"> Show Password</div>
                        <div class="clearfix"></div><span id="alerttext"></span></div>
                          <div class="clearfix"></div>
					</div>
                        
                 
                    <!-- <div class="myPlanBtn"> -->
					<div class="">
					
					<input type="submit" name="submit" value="Submit" class="submit-btn"/>
					<input type="submit" name="submit" value="Cancel" class="cancel-btn"/>
                    	
                    </div>
                  
                 
                </div>   
                </div>
                  <div class="clearfix"></div>
                </div>
                  </form>
                
                <div class="tab-paneIn" id="myPlanList" style="display:none;">
                <div class="col-sm-12">
                	
                </div>
                <div class="clearfix"></div>
                </div>
                
                
            </div>
			</form>
          </div>
        </div>
        
      </div>
      <div class="col-md-3">
       <?php include("inc/inc.right.php"); ?> 
      </div>
	  
    </div>
  </div>
</section>
<?php include("inc/inc.ft.php"); ?>
<script src="js/jquery.min.js"></script> 
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/additional-methods.min.js" type="text/javascript"></script>
<script type="text/javascript">
var $k = jQuery.noConflict();
$k(document).ready(function() { 
    $k.validator.addMethod('passWithOneNumberOneAlphabatic', function(value, element) {
            return this.optional(element) || (value.match(/[a-zA-Z]/) && value.match(/[0-9]/));
        },
        'Password must contain at least one numeric and one alphabetic character.'); // additional method to get more secure password by user

	$k("#changePassForm").validate({
	rules :{
		"oldpass" : {
			required : true
		},
		"newpass" : {
		   required : true,
		   minlength: 8,
		   //passWithOneNumberOneAlphabatic: true
                        
		},
		"confirmpass" : {
			required: true,
			minlength: 8,
			equalTo: "#newpass"
		}		
	},
	messages :{
		"oldpass" : {
			required : 'Please enter old password'
		},
		"newpass" : {
			required : 'Please enter new password'
		},
		"confirmpass" : {
			required : 'Please enter confirm password',
			equalTo: 'Password does not match'
		}
	},
	errorElement: "div"		
});
$k("#changePassForm").removeAttr("novalidate");
});

</script>
<script type="text/javascript">
 $(document).ready(function () {
 $("#showHide").click(function () {
 if ($(".password").attr("type")=="password") {
 $(".password").attr("type", "text");
 }
 else{
 $(".password").attr("type", "password");
 }
 
 });
 });
</script> 
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 

