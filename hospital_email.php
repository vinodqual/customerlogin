<?php  
include_once("conf/conf.php");
include_once("conf/common_functions.php");
$emailAddress=sanitize_data_email(@$_POST['emailaddress']);
$firstname=sanitize_data(@$_POST['firstname']);
$hospitalList1=explode(",",@$_POST['hospitalList']);
if(trim(@$emailAddress)!='' && sanitize_data(trim(@$_POST['hospitalList']))!=''){
$pageList =  xml2array(@file_get_contents('../resource/hospital_locator.xml'),'1','');	
$hospitalListArray = @$pageList['HospitalLocator'];
$body1='<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
<table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="70" align="left" valign="middle" style="border-bottom:1px solid #0f6639;"><img src="http://www.religarehealthinsurance.com/images/religare_logo.jpg" alt="Religare Health Insurance" style="padding-left:6px;" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td style="font:normal 11px Tahoma, Arial, Helvetica, sans-serif; color:#424242;">Dear '.ucwords(@$firstname).',<br /><br />
Greetings !!!<br /><br />
Mentioned below are the list of hospitals as per your requirement.</td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td><table width="595" border="1" align="center" cellpadding="5" cellspacing="0" bordercolor="#A5F8CE">
      <tr>
        <td width="38" height="25" align="left" valign="top" bgcolor="#DCFCEC" style="font:bold 11px Tahoma, Arial, Helvetica, sans-serif; color:#424242;">Sr no</td>
        <td width="114" height="25" align="left" valign="top" bgcolor="#DCFCEC" style="font:bold 11px Tahoma, Arial, Helvetica, sans-serif; color:#424242;">Hospital Name</td>
        <td width="271" height="25" align="left" valign="top" bgcolor="#DCFCEC" style="font:bold 11px Tahoma, Arial, Helvetica, sans-serif; color:#424242;">Address</td>
        <td width="127" height="25" align="left" valign="top" bgcolor="#DCFCEC" style="font:bold 11px Tahoma, Arial, Helvetica, sans-serif; color:#424242;">Contact No</td>
      </tr>';
	  $l=1;  
	  for($i=0;$i<count($hospitalList1);$i++){
	  $hospitalDetails=hospital_particular_details(@$hospitalListArray,sanitize_data(@$hospitalList1[$i]));
     $body1.='<tr>
        <td height="25" align="left" valign="top" bgcolor="#FFFFFF" style="font:normal 11px Tahoma, Arial, Helvetica, sans-serif; color:#424242;">'.@$l.'.</td>
        <td height="25" align="left" valign="top" bgcolor="#FFFFFF" style="font:normal 11px Tahoma, Arial, Helvetica, sans-serif; color:#424242;">'.stripslashes(base64_decode($hospitalDetails[0]["hospitalName"])).'</td>
        <td height="25" align="left" valign="top" bgcolor="#FFFFFF" style="font:normal 11px Tahoma, Arial, Helvetica, sans-serif; color:#424242;">'.stripslashes(base64_decode($hospitalDetails[0]["address1"])).' '.stripslashes(base64_decode($hospitalDetails[0]["address2"])).' '.stripslashes(base64_decode($hospitalDetails[0]["cityTitle"])).' '.stripslashes(base64_decode($hospitalDetails[0]["pincode"])).'</td>
        <td height="25" align="left" valign="top" bgcolor="#FFFFFF" style="font:normal 11px Tahoma, Arial, Helvetica, sans-serif; color:#424242;">'.stripslashes(base64_decode($hospitalDetails[0]["phone"])).'</td>
      </tr>';
	 $l++; }
    $body1.='</table></td>
  </tr>
  <tr>
    <td style="font:normal 11px Tahoma, Arial, Helvetica, sans-serif; color:#424242;"><br />We hope that the information provided by us will be useful to you.<br /><br />
Regards,<br />
Religare Health Insurance Co. Ltd<br /><br /></td>
  </tr>
  <tr>
    <td height="50" align="left" valign="middle" style="font:normal 11px Tahoma, Arial, Helvetica, sans-serif; color:#cccccc;">Disclaimer: Religare Health Insurance Co Ltd. reserves the right to add / remove any Hospital from the network hospital list<br />
Religare Health Insurance Company Limited. Insurance is the subject matter of the solicitation. IRDA Reg.No.115</td>
  </tr>
</table>
';
// To send HTML mail, the Content-type header must be set
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

// Additional headers
//$headers .= 'To:  '.@$emailAddress. "\r\n";
$headers .= 'From: Religare Health Insurance <rhiclportaladmin@religarehealthinsurance.com>'." \r\n";
$subject='Hospital Network List';
// Mail it
mail(@$emailAddress, @$subject, $body1, $headers);
}
?>