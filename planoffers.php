<?php
include("inc/inc.hd.php");
$strErrorMsg = '';
if ($restictPlanVisibility!==true) {
	$strErrorMsg = "Something went wrong. please try again.";
}
$plink 	= urldecode(base64_decode($_REQUEST['plink']));
$ofpn 	= urldecode(base64_decode($_REQUEST['ofpn']));
$fpid 	= urldecode(base64_decode($_REQUEST['fpid']));

$healthCheckPlans = fetchhealthPlanList("where CRMCOMPANYHEALTHCHECKUPPLAN.POLICYID='" . @$wellnessList[0]['POLICYID'] . "' AND CRMCOMPANYHEALTHCHECKUPPLAN.STATUS='ACTIVE' AND CRMCORPHEALTHCHECKUP.STATUS='ACTIVE' AND CRMCOMPANYHEALTHCHECKUPPLAN.PAYMENTOPTIONS='BUY'");  
//echo "<pre>";print_r($healthCheckPlans);die;

$FreePlan = fetchhealthPlanList("where CRMCOMPANYHEALTHCHECKUPPLAN.POLICYID='" . @$wellnessList[0]['POLICYID'] . "' AND CRMCOMPANYHEALTHCHECKUPPLAN.STATUS='ACTIVE' AND CRMCORPHEALTHCHECKUP.STATUS='ACTIVE' AND CRMCOMPANYHEALTHCHECKUPPLAN.PAYMENTOPTIONS='FREE' AND PLANID = '" . $fpid . "'");  

$ShowPaidPlans = $restrictedPaidPlans[$ofpn];

if (count($restrictedPlans)==0) {
	$strErrorMsg = "Something went wrong. please try again.";
}
if (isset($_POST["AddDetails"])) {
    $corpplanid = sanitize_data($_POST['corpplanid']);
	$planId = $corpplanid;
    $totalSumInputval = sanitize_data($_POST['price_'.$corpplanid]);
	$masterPlanId = sanitize_data($_POST['masterplanid_'.$corpplanid]);
    $membersid[] = 1;
    $membersprice[] = $totalSumInputval;
    $membersidwithprice = array_combine(array_values($membersid), array_values($membersprice));
	$ss = array_combine(array_values($membersid), array_values($membersprice));
	$membersid = json_encode($membersid);
    $membersprice = json_encode($membersprice);
    $membersidwithprice = json_encode($membersidwithprice);
    $entryTime = date('d-m-Y H:i');
	$_SESSION['SELECTEDMEM'] = $membersid;
	foreach($ss as $mkey => $mlink) 
	{ 
		if($mkey === '') 
		{ 
			unset($ss[$mkey]); 
		} 
	} 
	$_SESSION['testpricem'] = $ss;
	$aaaaa= base64_encode($ss);
    if ($_SESSION['LOGINTYPE'] == 'CORPORATE') {
        $sql = "INSERT INTO CRMEMPLOYEESELECTEDPLAN(ID,CORPPLANID,HEALTHCHECKUPPLANID,TOTALPRICE,MEMBERIDS,MEMBERPRICES,MEMBERIDWITHPRICES,CREATEDON,CREATEDBY,UPDATEDON,UPDATEDBY,POLICYNUMBER,POLICYID,COMPANYID,CUSTOMERID,EMPLOYEEID,EMAILID) values(CRMEMPLOYEESELECTEDPLAN_SEQ.nextval,'" . $planId . "','" . @$masterPlanId . "','" . $totalSumInputval . "','" . $membersid . "','" . $membersprice . "','" . $membersidwithprice . "','" . $entryTime . "','" . $_SESSION['userName'] . "','" . @$entryTime . "','" . @$_SESSION['userName'] . "','" . @$_SESSION['policyNumber'] . "'," . $wellnessList[0]['POLICYID'] . "," . $_SESSION["COMPANYID"] . "," . $_SESSION["customerIdCheck"] . "," . $_SESSION["empId"] . ",'" . $_SESSION['userName'] . "') ";//query to insert record
        $stdid = @oci_parse($conn, $sql);
        $r = @oci_execute($stdid);
        //fetch current inserted healthcheckup plan id//
        $qu2 = "select CRMEMPLOYEESELECTEDPLAN_SEQ.currval from dual";
        $stdids2 = @oci_parse($conn, $qu2);
        @oci_execute($stdids2);
        $lastinsertId2 = oci_fetch_assoc($stdids2);
		$currentplanInsertedId = $lastinsertId2['CURRVAL'];
        $_SESSION["CURRENTPLANID"] = $currentplanInsertedId;
	
    } 
	ob_clean();
    ?>
    <script>window.location.href = 'buy_plan.php?id=<?php echo base64_encode(@$planId); ?>'</script> <!--locate to cms_listing.php -->
    <?php
}

?>

<script language="javascript" src="js/encrypt.js"></script>
<script>
$(document).ready(function() {
  $(".corpplanid").click(function() {
		var value  = $(this).val();
		var id  = $(this).attr('id');
		if (id == 'free'){
			$('#pricediv').hide();
			$('#buydiv').hide();
			$('#appdiv').show();
		} else {
			var price = $('#price_'+value).val();
			$('#totalsum').html(price);
			$('#appdiv').hide();
			$('#pricediv').show();
			$('#buydiv').show();	
		}
		
   });
});
function validateHealthCheckupForm() {
	$("#HealthChkupPlanList").submit();
}
</script>
<style>
.testdiv {
	display:inline-block;
}
</style>
<link media="screen" rel="stylesheet" href="css/jquery-ui.css" />
<section id="middleContainer">
    <div class="container-fluid">
        <div class="middlebox"> 
            <div class="col-md-9">
                <div class="middlebox-left">
                    <?php include('inc/inc.healthplan_tab.php'); ?>
                    <div class="tab-content ">
                        <?php if ($_SESSION['LOGINTYPE'] == 'CORPORATE') { ?>
                            <div class="tab-pane <?php
                            if (empty($tab) || $tab == "buyplan") {
                                echo "active";
                            }
                            ?>" id="buyPlan">
                                <div class="tab-paneIn" style="padding-bottom:13.5%;">
                                    <form name="HealthChkupPlanList" id="HealthChkupPlanList" class="HealthChkupPlanList" method="post" action="">									
										<div class="title-bg textleft">
											<h1><img src="img/health-check-up.png" alt="" title=""> <span style="font-weight:bold;">Upgrade Your Health Check-up Plan</span></h1>
											<p>Upgrade Your existing free plan with new add on plan.</p>
										</div>
										
										 <div class="col-sm-12"> 
											 <table class="" width="100%">
												<tr>
													<th></th>
													<th><div style="width:250px;">Select Plan</div></th>
													<th><div style="width:60px;">MRP</div></th>
													<th  style="min-width:300px;">Test Details</th>
												</tr>
												<?php
												if ($strErrorMsg=='') {	
													if (count($FreePlan)==1) {
														for ($i=0;$i<count($FreePlan);$i++) {
															$masterPlanId = $FreePlan[$i]['PLANNAMEID'];
															?>
															<tr>
																<td><input class="corpplanid" id="free" type="radio" name="corpplanid"  value="<?php echo $FreePlan[$i]['PLANID']; ?>" checked></td>
																<td>
																<?php echo trim($FreePlan[$i]['PLANNAME']) ? stripslashes($FreePlan[$i]['PLANNAME']) :$FreePlan[$i]['PLANNAME2']; ?></td>
																<td>Free</td>
																<td><b>Base Plan with following test details :</b><br /> 
																<?php 
																$query = "SELECT CRMCORPCATMAP.TESTID,CRMCORPTESTNAME.TESTNAME FROM CRMCORPCATMAP LEFT JOIN CRMCORPTESTNAME ON  CRMCORPCATMAP.TESTID = CRMCORPTESTNAME.TESTID WHERE CRMCORPCATMAP.HEALTHCHECKUPPLANID='" . $masterPlanId . "'";
																$sql = @oci_parse($conn, $query);
																// Execute Query
																@oci_execute($sql);
																$i=0;
																$catArray = array();
																while (($row = oci_fetch_assoc($sql))) {
																	foreach($row as $key=>$value){
																		$catArray[$i][$key] = $value;				
																	}
																	$i++;
																}
																$writeData = '';
																for ($i=0;$i<count($catArray);$i++) {
																	$writeData .= '<div class="testdiv"><strong>[</strong>'.$catArray[$i]['TESTNAME'].'<strong>]</strong></div> ';
																}
																echo $writeData;
																?>
																</td>
															</tr>
															<?php
														}
													} else {
														echo '<tr><td colspan="3">'.$strErrorMsg.'</td></tr>';
													}
												} else {
													echo '<tr><td colspan="3">'.$strErrorMsg.'</td></tr>';
												}
												if ($strErrorMsg=='') {	
													if (count($healthCheckPlans)>0) {
														for ($i=0;$i<count($healthCheckPlans);$i++) {
															$masterPlanId = $healthCheckPlans[$i]['PLANNAMEID'];
															$isPlanVisiblePlanName= trim(($healthCheckPlans[$i]['PLANNAME']) ? ($healthCheckPlans[$i]['PLANNAME']) : $healthCheckPlans[$i]['PLANNAME2']);
															
															$isPlanVisible = false;
															if($restictPlanVisibility === true && in_array($isPlanVisiblePlanName, $ShowPaidPlans)){
																$isPlanVisible = true;
															}
															if ( $isPlanVisible === true ) {
															?>
																<tr>
																	<td><input class="corpplanid" id="paid" type="radio" name="corpplanid"  value="<?php echo $healthCheckPlans[$i]['PLANID']; ?>"></td>
																	<td style="line-height:25px">
																	<div>
																	<?php echo $showPlanName[trim($healthCheckPlans[$i]['PLANNAME']) ? stripslashes($healthCheckPlans[$i]['PLANNAME']) :$healthCheckPlans[$i]['PLANNAME2']]; ?>
																	</div>
																	</td>
																	
																	<td>
																		<?php 
																		$planDetailsById = fetchListCondsWithColumn('EDITEDPRICE,DISCOUNTPRICE,AGEGROUPID,SI,GENDER,PLANTYPEID,PLANNAMEID,DESCRIPTION,UPLOADPDF,PLANLIMITS', "CRMCOMPANYHEALTHCHECKUPPLAN", "WHERE CRMCOMPANYHEALTHCHECKUPPLAN.POLICYID='" . @$wellnessList[0]['POLICYID'] . "' AND CRMCOMPANYHEALTHCHECKUPPLAN.STATUS='ACTIVE' AND CRMCOMPANYHEALTHCHECKUPPLAN.PAYMENTOPTIONS='BUY' AND CRMCOMPANYHEALTHCHECKUPPLAN.PLANID=" .$healthCheckPlans[$i]['PLANID']. "");
																		$memberList = $planDetailsById[0]['EDITEDPRICE'] ? json_decode($planDetailsById[0]['EDITEDPRICE']) : '';
																		$k=1;
																		foreach ($memberList as $memberid => $price) {
																			if ($k>1) break;
																			$price = floatval($price);
																			$k++;
																		}
																		?>
																		<input type='hidden' id="price_<?php echo $healthCheckPlans[$i]['PLANID']; ?>" name="price_<?php echo $healthCheckPlans[$i]['PLANID']; ?>" value="<?php echo $price; ?>">
																		<cite class="WebRupee">Rs.</cite>  <?php echo $price; ?>/-
																	</td>
																	
																	<td><b>Add on Plan with following test details over and above base plan:</b><br /> 
																	<input type='hidden' id="masterplanid_<?php echo $healthCheckPlans[$i]['PLANID']; ?>" name="masterplanid_<?php echo $healthCheckPlans[$i]['PLANID']; ?>" value="<?php echo $masterPlanId; ?>">
																	<?php 
																	
																	$query = "SELECT CRMCORPCATMAP.TESTID,CRMCORPTESTNAME.TESTNAME FROM CRMCORPCATMAP LEFT JOIN CRMCORPTESTNAME ON  CRMCORPCATMAP.TESTID = CRMCORPTESTNAME.TESTID WHERE CRMCORPCATMAP.HEALTHCHECKUPPLANID='" . $masterPlanId . "'";
																	$sql = @oci_parse($conn, $query);
																	// Execute Query
																	@oci_execute($sql);
																	$c=0;
																	$catArray = array();
																	while (($row = oci_fetch_assoc($sql))) {
																		foreach($row as $key=>$value){
																			$catArray[$c][$key] = $value;				
																		}
																		$c++;
																	}
																	$writeData = '';
																	for ($w=0;$w<count($catArray);$w++) {
																		$writeData .= '<div class="testdiv"><strong>[</strong>'.$catArray[$w]['TESTNAME'].'<strong>]</strong></div> ';
																	}
																	echo $writeData;
																	?>
																	</td>
																</tr>
															<?php
															}
														}
													} else {
														echo '<tr><td colspan="3">'.$strErrorMsg.'</td></tr>';
													}
												} else {
													echo '<tr><td colspan="3">'.$strErrorMsg.'</td></tr>';
												}
												?>
											</table>
										</div>
										<div class="clearfix">&nbsp;</div>
										<?php if ($strErrorMsg == '') { ?>
									
										<div class="col-sm-6 ">
											<div id="pricediv" style="display:none;">
											<span class="total-text"> Total Price :</span> <cite class="WebRupee1">Rs.</cite> <span  class="total-price totalSum"><span id='totalsum'></span>/-
											</span>
											
											</div>
											<div class="clearfix">&nbsp;</div>
											<div class="tab-mid-container-sm" style="font-size:15px; color:black;">
												<span>*</span> The price is inclusive of all taxes.
												<br/>
												<span>Disclaimer</span>: - We would suggest you to review the add on package's test details before purchasing it. As Post purchase, No refund will be initiated..
											</div>
										</div>
										<div class="col-sm-6">
											<div id="buydiv" style="display:none;">
												<div class="tab-mid-container-buy"><a href="javascript:void(0);" onclick="return validateHealthCheckupForm();">Buy Now ></a></div>
											<input type="hidden" class="submitbtnplan" value="Buy Now" id="button2" name="AddDetails">
											</div>
											<div id="appdiv">
												<div class="tab-mid-container-buy"><a href="<?php echo $plink; ?>">Book Appointments ></a></div>
											</div>
										</div>
										<?php } ?>
                                    </form>
                                </div>           
                            </div>
                        <?php } ?>
                        <div class="tab-pane" id="existingAppointments"> Please Wait... </div>
                    </div>
                </div>
            </div>  
            <div class="col-md-3">
                <?php include("inc/inc.right.php"); ?>
            </div> 
        </div>
    </div>
</section>
<?php include("inc/inc.ft.php"); ?>
