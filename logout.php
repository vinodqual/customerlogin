<?php
/*****************************************************************************
* COPYRIGHT
* Copyright 2013 Catabatic Automation Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: logout.php,v 1.0 2013/04/15 12:30:17 ashish gupta Exp $
* $Author: ashish gupta $
*
****************************************************************************/
include_once("conf/conf.php");				//include configuration file to access database connectivity
include_once("conf/session.php");
 clearstatcache();
session_start();
require_once 'googleplus/src/Google_Client.php';
require_once 'googleplus/src/contrib/Google_Oauth2Service.php';

$client = new Google_Client();
if(@$_SESSION['token'])
{
    unset($_SESSION['token']);
    $client->revokeToken();
}

$_SESSION["Flag"]			=	0;
@$_SESSION['policyNumber']	=	"";
@$_SESSION['productId']	=	"";
@$_SESSION['productName']	=	"";
@$_SESSION['customerId']	=	"";
@$_SESSION['userName']		=	"";
$_SESSION["firstName"]		=	"";
$_SESSION["PHPSESSID"]		=	"";
setcookie ('PHPSESSID', '', time()-3600, '/', '', 0, 0); // Destroy the cookie.
foreach (@$_COOKIE as $key => $value )
{
    setcookie( $key, '', 1, '/' );
}

if ($_SESSION["OSTYPE"] != 'MOBILEAPP') {
    session_destroy();
    $pLocation = 'index.php';
} else {
    session_destroy();
    $pLocation = 'login.php';
}
?>
<script>
window.location.href='<?=$pLocation;?>';
</script>