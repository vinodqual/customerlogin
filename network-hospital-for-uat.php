<?php include("inc/inc.hd.php");
include_once("conf/conf.php");	
include_once("conf/common_functions.php"); //include common_functions file to access defined function
include_once("paginator.php");	
//echo ADMINURL . 'resource/corporate_hospital_locator.xml';
error_reporting(0);
$advanceSearch = sanitize_search(@$_REQUEST['advanceSearch']);
$pageList = xml2array(@file_get_contents('../resource/static_pages.xml'), '1', '');
$pageListArray = @$pageList['StaticPages'];
$pageList = array_reverse(static_page_array($pageListArray));

for ($i = 0; $i < count($pageList); $i++) {
    if ($pageList[$i]["staticId"] == 28) {
        $opdDescription = base64_decode($pageList[$i]["staticDescription"]);
    } else {
        
    }
}
if($_SESSION['LOGINTYPE']=='CORPORATE'){ 
	$pageList = xml2array(@file_get_contents('../resource/corporate_hospital_locator.xml'), '1', '');
	$hospitalListArray = @$pageList['HospitalLocator'];
}
else{
	$pageList = xml2array(@file_get_contents('../resource/hospital_locator.xml'), '1', '');
	$hospitalListArray = @$pageList['HospitalLocator'];
}

// State List
$pageList = xml2array(@file_get_contents('../resource/hospital_locator_states.xml'), '1', '');
$pageListArray = $pageList['States'];
$stateList = states_page_array($pageListArray);
// City List 
$pageList = xml2array(@file_get_contents('../resource/hospital_locator_city.xml'), '1', '');
$pageListArray = $pageList['Cities'];
$cityList = cities_page_array($pageListArray);
$k = 0;
for ($i = 0; $i < count($cityList); $i++) {
    if ($cityList[$i]["stateId"] == @$stateList[0]['stateId']) {
        if ($k == 0) {
            $cityId = $cityList[$i]["cityId"];
            $k++;
        }
    }
}
if (sanitize_data(@$_REQUEST['city']) != '')
    $city = sanitize_data(@$_REQUEST['city']);
else
    $city = $cityId;   //echo $city; 
if (sanitize_data(@$_REQUEST['state']) != '') {
    $state = sanitize_data(@$_REQUEST['state']);
    if (preg_match('/^-{0,1}\d+$/', $state)) {
        
    } else {
        header("Location: " . _SITEURL);
        exit;
    }
} else {
    $state = $stateList[0]['stateId'];
}  //echo $state;
if (trim(@$advanceSearch) == "Locality / Hospital After selecting city above" || @$advanceSearch == "") {
    $search = '';
} else {
    $search = @$advanceSearch;
}


/*Finally sort your array*/

$hospitalList = hospital_page_array($hospitalListArray, $city, $search);
//echo '<pre>';print_r($hospitalList);exit;
$noOfPages = count($hospitalList) / 10;

if (sanitize_data(@$_REQUEST['pagel']) != '') {
    $page1 = explode("_", sanitize_data(@$_REQUEST['pagel']));

    if ($page1[0] == "next") {
        $start = ($page1[1] - 1) * 5;
        $pageloop = $start + 5;
        $page = $start;
        $prev = sanitize_data(@$_REQUEST['next']) - 1;
        $next = sanitize_data(@$_REQUEST['next']) + 1;
        $pageData = ($pageloop*10)-10;
     ?>
<script>showNetwork(<?php echo $pageData;?>);</script>
         <?php
    } else if ($page1[0] == "prev") {
        $start = (($page1[1] - 1) * 5) + 1;
        $pageloop = $start + 5;
        $page = $start;
        $next = sanitize_data(@$_REQUEST['prev']) + 1;
        $prev = sanitize_data(@$_REQUEST['prev']) - 1;
    } else {
        $page = $page1[1];
        $start = sanitize_data(@$_REQUEST['start']);
        $pageloop = $start + 5;
        $next = sanitize_data(@$_REQUEST['next']);
        $prev = sanitize_data(@$_REQUEST['prev']);
    }
} else {
    $page = 1;
    $next = 2;
    $prev = 0;
    $start = 1;
    $pageloop = 5;
}
if ($pageloop > ceil($noOfPages)) {
    $pageloop = ceil($noOfPages);
} else {
    $pageloop = $pageloop;
}
$total = ceil($noOfPages / 5);
$pagingConcat1 = "&start=" . @$start . "&next=" . @$next . "&prev=" . @$prev . "&city=" . $city . "&state=" . $state . "&advanceSearch=" . $search;
$paginationString = " <ul><li>";
if ((ceil($noOfPages) > 5) && ($prev > 0)) {
    $paginationString.= " <a href=\"network-hospital-for-uat.php?pagel=prev_" . @$prev . @$pagingConcat1 . "\">&laquo;</a>|";
}
for ($i = $start; $i <= $pageloop; $i++) {
	
    if ($i == $page) {
		$verticalline="";
		if($page==1 && $pageloop >1){
			$verticalline="|";
		}
		
        $paginationString.= "" . @$i . "$verticalline";
    } else {
        $paginationString.= "<a href=\"network-hospital-for-uat.php?pagel=page_" . @$i . $pagingConcat1 . "\" class=\"pagenum_link\">" . @$i . "</a>|";
    }
}
if ((ceil($noOfPages) > 5) && ($next <= $total)) {
    $paginationString.= " <a href=\"network-hospital-for-uat.php?pagel=next_" . @$next . @$pagingConcat1 . "\">&raquo;</a>";
}
$paginationString.= "</li> </ul>";
$limit1 = (@$page - 1) * 10;
$limit2 = $limit1 + 10;
?>
<script src="js/locator.js" charset="utf-8">
</script>
<script>
function removeErrorEmail(){  // function to hide jquery error message from login page if user click on forgot link and again came to login page
var emailDivEmailidError = document.getElementById("emailDivEmailidError");
var emailDivNameError = document.getElementById("emailDivNameError");
var emailDivSuccess = document.getElementById("emailDivSuccess");
emailDivEmailidError.style.display="none";
emailDivNameError.style.display="none";
emailDivSuccess.style.display="none";
}
function removeErrorSMS(){  // function to hide jquery error message from login page if user click on forgot link and again came to login page
var smsDivNameError = document.getElementById("smsDivNameError");
var smsDivSuccess = document.getElementById("smsDivSuccess");
var smsDivMobileError = document.getElementById("smsDivMobileError");
smsDivNameError.style.display="none";
smsDivSuccess.style.display="none";
smsDivMobileError.style.display="none";
}
</script>
<section id="middleContainer">
  <div class="container-fluid">
    <div class="middlebox">
      <div class="col-md-9">
        <div class="middlebox-network">       
            <div class="col-md-12">
			<form name="advance_search_form" id="advance_search_form" action="" method="post" style="margin:0px">
			<div class="hospitalLocationSearch">
				<div style="padding: 0 0 10px 0">
				  <div style="float:left;"><h1>Hospital Locator Overview </h1></div>
					  <div class="hospitalText">
					  Our strong association with over 4500+ leading hospitals in India will ensure hassle free cashless treatment to our customers. Your nearest network hospital/s can be viewed by entering the following details-
					  </div>
				</div>
				
				  <div class="inputBox2 networkInput">
					  <div class="inputBoxIn dropBg">
						<select name="state" id="state" onChange="stateChange(this.value,'network')" name="state">
						  <?php for ($i = 0; $i < count($stateList); $i++) { ?>
								<option value="<?php echo stripslashes($stateList[$i]["stateId"]); ?>" <?php if ($state == $stateList[$i]["stateId"]) {
								echo "selected";
								} ?>><?php echo stripslashes(base64_decode($stateList[$i]["stateTitle"])); ?></option>
							<?php
								}
								?>
						 </select>                                  
					  </div>
				  </div>
				  <div class="inputBox2 networkInput">
				  <div id="ajaxLoading" style="display:none; height:0;">
						<img class="" alt="Loading.." src="img/ajax-loader_12.gif">
				   </div>
						<div class="inputBoxIn dropBg" id="cities">
							<select>
							  <option>Select</option>
							</select>
						</div>
				  </div>
				<div class="graytxtBox-Spec networkInput"><span class="txtIcon11"></span>
				  <div class="txtMrgn">
				  <input type="text" name="advanceSearch" id="textfield" class="txtfieldFull" onfocus="if (this.value == 'Locality / Hospital After selecting city above')
            this.value = '';" onBlur="if (this.value == '')
            this.value = 'Locality / Hospital After selecting city above';" value="<?php if ((trim(@$advanceSearch) != '') && (trim(@$advanceSearch) != 'Locality / Hospital After selecting city above')) {
             echo @$advanceSearch;
            } else {
              echo "Locality / Hospital After selecting city above";
            } ?>">
				  
				  </div>
				</div>
				<input type="submit" class="greenSrchBtn" name="sub" id="button" value="Submit4" >
			</div>
			</form>
			<?php if (ceil($noOfPages) > 0) { ?>
			<div id="sendviaemailbtn" class="greenSrchBtnEmail">
			<input onclick="network_hospital_email_retail();" class="submit-btn right popupBtn" type="submit" border="0" value="Send Email" alt="Send Email" name="sendviaemail">
			<div id="divID"></div> <!-- display error message-->		
			</div>
                
			<div class="paginationBox2" style="float: right;">
	 <div class="digit"> <?php echo @$paginationString; ?> </div>
	 <span class="fr">Page <?php echo (int) $page; ?> of <?php echo ceil($noOfPages); ?></span>
	</div>
			
				<?php } else {
					echo '<div class="address fl"><h1>No Hospital Found</h1></div>';
				} ?>


 			
<?php
function sortArray($a, $b, $orderby='hospitalName') {
    if ($a[$orderby] == $b[$orderby]) {
        return 0;
    }
    return ($a[$orderby] < $b[$orderby]) ? -1 : 1;
}
function fix_keys($yourArray) {
    $numberCheck = false;
    foreach ($yourArray as $k => $val) {
        if (is_array($val)) $yourArray[$k] = fix_keys($val); //recurse
        if (is_numeric($k)) $numberCheck = true;
    }
    if ($numberCheck === true) {
        return array_values($yourArray);
    } else {
        return $yourArray;
    }
}

for($j=0; $j<count($hospitalList); $j++){
    $hospitalList[$j]['hospitalName'] = base64_decode($hospitalList[$j]['hospitalName']);
}

uasort($hospitalList, 'sortArray');
$hospitalList  = fix_keys($hospitalList);

for($j=0; $j<count($hospitalList); $j++){
    $hospitalList[$j]['hospitalName'] = base64_encode($hospitalList[$j]['hospitalName']);
}
// end : code added by ranvir for sorting hospitle list on 11 dec 2014
//echo '<pre>';print_r($hospitalList);exit;
$k = 0;
$looplimit = $limit2;
@$textValue = trim($_POST['textfield']);
if ($looplimit > count($hospitalList)) {
    $looplimit = count($hospitalList);
}
	 ?>
<?php 
for ($i = $limit1; $i < $looplimit; $i++) {
	if (($i%2)>0)
	{
	   $zigzaghospital = "";
	   $class = "addressBoxRight";
	}else{
	  $class = "addressBoxLeft";
	   $zigzaghospital = "zigzaghospital";
	   $selectColorMain = "selectColorMain";
	   if($i==$looplimit){
		   $zigzaghospital = '';
		}
	 if($i>($limit1+1)){
		 $selectColorMain = "";
		 $style = 'style="display:none;"';
	}
 ?>            
  <div class="hospitalLocationResultMain">
  <div class="hospitalLocationResultMainIn <?php echo $selectColorMain;?>">
 <?php 
   }
  ?>
		<div class="addressBox <?php echo $class; ?>">
		 <h3><span class="srcharrowup srcharrowup1"></span><code><?php echo stripslashes(base64_decode($hospitalList[$i]["hospitalName"])); ?> (<?php echo stripslashes(base64_decode($hospitalList[$i]["location"])); ?>)</code></h3>
		  <div class="addressDetail" <?php echo @$style; ?> >
			 <div class="addressDetailIn <?php echo $zigzaghospital; ?>">
			  <div class="locationTopBox">
				<div class="checkboxContainer">
				
				<input type="checkbox" id="radCreateMode<?php echo $i;?>" class="chkBoxValidate" name="checkboxName" value="<?php echo $hospitalList[$i]["hospitalId"]; ?>" />
				
				<label for="radCreateMode<?php echo $i;?>" class="custom-checkbox"></label></div>
				<div class="locationContainer"><strong><?php echo stripslashes(base64_decode($hospitalList[$i]["address1"]));?></strong> <strong><?php echo stripslashes(base64_decode($hospitalList[$i]["address2"]));?></strong><br />
				<?php echo stripslashes(base64_decode($hospitalList[$i]["cityTitle"])); ?><br />
				<strong> Pincode : </strong><?php echo stripslashes(base64_decode($hospitalList[$i]["pincode"])); ?><br />
				<strong> Phone : </strong><?php echo stripslashes(base64_decode($hospitalList[$i]["phone"])); ?>
				</div>
			  </div>
			  <div class="sendviasmsbtn greenSrchBtnEmail">
				<!-- <input type="submit" name="sendviasms" value="Send Via SMS" /> -->
				
				<input class="submit-btn right" type="submit" border="0" value="Send Via SMS" alt="Send Via SMS" name="sendviasms" onclick="network_hospital_sms_retail(<?php echo $hospitalList[$i]["hospitalId"]; ?>);">
			  </div>
			  <!-- <a href="#"><img src="img/sendViasms.png" alt="Send" border="0" class="fl"></a> -->
		  <div class="cl"></div></div>
		  </div>
	  </div>
	  
		<?php if ($i%2){ ?>	                 
  <div class="cl"></div>

  </div>
</div>              
	<?php } }?>
	<div class="clearfix">&nbsp;</div>
	
	</div>
	<?php 
 if (ceil($noOfPages) > 0) 
 { ?>
	<div class="paginationBox">
	 <div class="digit"> <?php echo @$paginationString; ?> </div>
	 <span class="fr">Page <?php echo (int) $page; ?> of <?php echo ceil($noOfPages); ?></span>
	</div>
<?php } ?>
</div>  
</div>
  <!-- bootstrap popup start for send email-->
  <div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick="return removeErrorEmail();"><span aria-hidden="true">×</span></button>
            <div class="model-header" id="exampleModalLabel">Send Email</div>
			<div class="" id="emailDivSuccess" style="color: red; font-size: 14px; position:absolute;"></div>
        </div>
        <div class="modal-middle">
            <div class="popupBrowsefull">
          <div class="model-input" style="margin:0;">
            <div class="inputBoxfull">
                <div class="inputBoxIn">
                    <input name="yourname" id="yourname" class="txtField" type="text" value="<?php echo @$_SESSION['employeeName']?$_SESSION['employeeName']:''; ?>" onkeypress='return kp_char(event);' placeholder="Enter your name*" onfocus="this.placeholder=''" onblur="this.placeholder='Enter your name*'"> 
 					
                    </div>
					<div id="emailDivNameError" style="clear:both; margin-bottom:5px;  color: red; font-size: 14px; position:absolute; padding-top: 15px;
    position: absolute;"></div> <!-- display error message-->
            </div>
              <div class="clearfix"></div></div>
                </div>
             
             <div class="popupBrowsefull" style="margin-top:12px;">
          <div class="model-input" style="margin:0;"> 
            <div class="inputBoxfull">
                <div class="inputBoxIn">
                    <input name="emailId" id="emailId" class="txtField"  type="text" value="<?php echo @$_SESSION['employeeEmail']?$_SESSION['employeeEmail']:''; ?>" placeholder="Enter your email*" onFocus="this.placeholder=''" onBlur="this.placeholder='Enter your email*'">
					
                </div>
				<div id="emailDivEmailidError" style="clear:both; margin-bottom:5px;  color: red; font-size: 14px; position:absolute; margin-top:13px;"></div> <!-- display error message-->
            </div> 
         <div class="clearfix"></div> </div> 
		 
            </div>
			
      </div> 
        
        <div class="model-footer" style="float:left;"> 
            <input name="submit" type="submit" border="0" alt="submit" class="submit-btn right popupBtn" value="Submit" onClick="sendHospitalListEmail2('D');">
        <div class="clearfix"></div></div>
        <div class="clearfix"></div>
    </div>
  </div>
</div>
  <!-- bootstrap popup for send email end -->
    <!-- bootstrap popup start for send sms-->
   <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick="return removeErrorSMS();"><span aria-hidden="true">×</span></button>
            <div class="model-header" id="exampleModalLabel">Send SMS</div>
			<div class="" id="smsDivSuccess" style="color: red; font-size: 14px; position:absolute; "></div>
        </div>
        <div class="modal-middle">
            <div class="popupBrowsefull">
          <div class="model-input" style="margin:0;">
            <div class="inputBoxfull">
                <div class="inputBoxIn">
                    <input type="text" size="33" autocomplete="OFF" name="firstName" id="firstName" value="<?php echo @$_SESSION['employeeName']?$_SESSION['employeeName']:''; ?>" placeholder="Enter your name*" onFocus="this.placeholder=''" onBlur="this.placeholder='Enter your name*'"  class="txtField" onkeypress='return kp_char(event)' />                    
                    </div>
					<div id="smsDivNameError" style="clear:both; margin-bottom:5px;  color: red; font-size: 14px; position:absolute; padding-top: 15px;"></div> <!-- display error message-->
            </div>
              <div class="clearfix"></div></div>
                </div>
               
             <div class="popupBrowsefull" style="margin-top:12px;">
          <div class="model-input" style="margin:0;"> 
            <div class="inputBoxfull">
                <div class="inputBoxIn">
					<input type="hidden" value="" id="hospitalId" name="hospitalId" />
                    <input type="text" size="33" maxlength="10" autocomplete="OFF" name="mobileNo" id="mobileNo" value="<?php echo @$_SESSION['employeePhone']?$_SESSION['employeePhone']:''; ?>" placeholder="Phone N0*" onFocus="this.placeholder=''" onBlur="this.placeholder='Phone No'" class="txtField" onkeypress='return kp_numeric(event)'/>
                </div>
				<div id="smsDivMobileError" style="clear:both; margin-bottom:5px;  color: red; font-size: 14px; position:absolute; padding-top: 13px;"></div> <!-- display error message-->
            </div> 
         <div class="clearfix"></div> </div> 
		 
            </div>
			
      </div> 
        
        <div class="model-footer" style="float:left;">
            <input name="submit" type="submit" border="0" alt="submit" class="submit-btn right" value="Submit" onClick="sendHospitalListSMS2();">
        <div class="clearfix"></div></div>
        <div class="clearfix"></div>
    </div>
  </div>
</div>
<!-- bootstrap popup ends for send sms-->

</div>
<?php if($looplimit % 2 == 0) {?>
<div class="col-md-3"> 
 <?php include("inc/inc.right.php"); ?>
</div>
<?php  } ?>  
</div>
<?php if($looplimit % 2 !=0) {?>
<div class="col-md-3">
 <?php include("inc/inc.right.php"); ?>
</div>
<?php  } ?>
</section>
<script type="text/javascript">
$(".custom-checkbox").click(function () {
	$(this).toggleClass('custom-checkboxActive');
});

$("h3").click(function(){
	$(".selectColorMain").removeClass("selectColorMain");
	$(this).parent().parent().addClass("selectColorMain");
	
	$(".addressDetail").hide();
	$(this).parent().parent().children().children().show();	
	
	$(".srcharrowup1").removeClass("srcharrowup1");
	$(this).parent().parent().children().children().children("span").addClass("srcharrowup1");
});
</script>
<script>stateChange(<?php echo @$state; ?>, <?php echo @$city; ?>, 'network');</script>
<?php include("inc/inc.ft.php"); ?>