        <meta charset="utf-8">
<div class="Left-Cont-box"> 
  <!-- quote -->
  <div class="Left-Cont-quot"><img src="images/quote.png" border="0" alt=""></div>
  <!-- quote end-->
  
 <h1>WHY RELIGARE?</h1>
<p> Religare Health Insurance (RHI), is a specialized Health Insurer offering health insurance services to employees of corporates, individual customers and for financial inclusion as well. Launched in July’12, Religare Health Insurance has made significant progress within a short span of time, and is already operating out of 54 offices with an employee strength of 2150, servicing more than 2.7 million lives (FY’15) across 400+ locations, including over 2500 corporates. </p>
  <div class="clearfix"></div>
  <!-- quote slider -->
 <div class="Left-Cont-test">
  <div id="owl-demo" class="owl-carousel">
          <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>BEST MEDICLAIM INSURANCE PRODUCT</h2>
            <p>FICCI HEALTHCARE EXCELLENCE AWARDS 2015</p>
          </div>
          <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>BEST HEALTH INSURANCE</h2>
            <p>MINT MEDICLAIM RATINGS 2015</p>
          </div>
          <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>BEST HEALTH INSURANCE COMPANY</h2>
            <p>ABP NEWS BFSI AWARDS 2015</p>
          </div>
		   <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>RISING STAR INSURER</h2>
            <p>INDIA INSURANCE AWARDS 2014</p>
          </div>
		   <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>BEST HEALTH INSURANCE FOR SENIOR CITIZENS</h2>
            <p>VOICE 2014</p>
          </div>
		   <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>"A" RATED HEALTH INSURANCE</h2>
            <p>MINT MEDICLAIM RATINGS 2014</p>
          </div>
		   <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>MOST AFFORDABLE COMPREHENSIVE HEALTH INSURANCE</h2>
           <p>MINT MEDICLAIM RATINGS 2014</p>
          </div>
		   <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>BEST PRODUCT INNOVATION AWARD</h2>
            <p>EDITOR'S CHOICE AWARD FINNOVITI 2013</p>
          </div>
		   <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>BEST TECHNOLOGY INNOVATION AWARD</h2>
            <p>INDIA INSURANCE AWARDS 2013</p>
          </div>
        </div>
</div>
  <!-- quote slider closed-->
  <div class="spacer2"></div>
</div>