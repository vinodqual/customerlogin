<?php
include_once("conf/conf.php");    //include configuration file to access database connectivity
include_once("conf/common_functions.php"); //include common_functions file to access defined function
include_once("conf/session.php"); //include session file to access session function
/* $previous_encoding = mb_internal_encoding();
  //Set the encoding to UTF-8, so when reading files it ignores the BOM
  mb_internal_encoding('UTF-8');
  //Process the CSS files...
  //Finally, return to the previous encoding
  mb_internal_encoding($previous_encoding);
 */
if (empty($_SESSION['policyNumber']) || $_SESSION['policyNumber'] == '') {
    $_SESSION['policyNumber'] = $_SESSION['policyNumNew'];
}
$browser = get_browser(null, true);
$_SESSION['POLICYEXPIRED'] = false;
$TPAManagedchkdis = "No";
$resxolaccess = array('SUPERTOPUP' => 'NO', 'PREVENTIVE' => 'NO', 'PARENTPOLICY' => 'NO', 'OPD' => 'NO');
if ($_SESSION['LOGINTYPE'] == 'RETAIL') {
    $fetchPolListByUserId = fetchListBySql("SELECT POLICYNUM as POLICYNUM FROM RETUSERPOLICYMAP WHERE USERID=" . @$_SESSION['USERID'] . " ORDER BY USERPOLICYMAPSEQ ASC");
}
if ($_SESSION['LOGINTYPE'] == 'CORPORATE') {
    $fetchPolListByUserId = fetchListBySql(" SELECT POLICYNUMBER as POLICYNUM FROM CRMEMPLOYEELOGIN WHERE LOWER(EMAIL)='" . strtolower(@$_SESSION['userName']) . "' AND STATUS='ACTIVE' ");
    $policy = sanitize_data(@$_SESSION['policyNumber']);
    $wellnessList = fetchPolicyDetails($policy); // check wellness option 
    $TPAManagedchk = @$wellnessList[0]['TPAMANAGED'];
    if ($TPAManagedchk == "Yes") {
        $TPAManagedchkdis = "Yes";
    } else {
        $TPAManagedchkdis = "No";
    }
    $empId10 = '';
    if (!empty($_SESSION['empNo'])) {
        $empId10 = str_pad($_SESSION['empNo'], 10, "0", STR_PAD_LEFT);
    }


    $xolproductlist = fetchXolProdListForEmployee($_SESSION['policyNumber'], @$empId10);

    if (count($xolproductlist) > 0) {
        $resxolaccess = array('SUPERTOPUP' => $xolproductlist[0]['SUPERTOPUP'], 'PREVENTIVE' => $xolproductlist[0]['PREVENTIVE'], 'PARENTPOLICY' => $xolproductlist[0]['PARENTPOLICY'], 'OPD' => $xolproductlist[0]['OPD'], 'EMAILID' => $xolproductlist[0]['EMAILID'], 'CORPORATEUNIQUECODE' => $xolproductlist[0]['CORPORATEUNIQUECODE'], 'TOPUPPLANEMPLOYEEID' => $xolproductlist[0]['TOPUPPLANEMPLOYEEID'], 'CORPORATEID' => $xolproductlist[0]['CORPORATEID']);
    }
//printr($resxolaccess);	
    //policyexpired conditions
    //echo $_SESSION['POLICYSTARTDATE']; die;
    $policyExpirydate = strtotime($_SESSION['POLICYENDDATE']);
    $currenttime = time();
    $_SESSION['policyexpiredisables'] = array();
    if ($currenttime > $policyExpirydate) {
        $_SESSION['POLICYEXPIRED'] = true;
        $policyexpiredisables = array(
            'dashboard.disable_download_policy_docs' => 'NO',
            'mypolicy.disable_download_policy_docs' => 'NO',
            'vas.discount_connect.download_coupon' => 'NO',
            'vas.discount_connect.download_in_search' => 'NO',
            'vas.my_plans.schedule_appointment_icon' => 'NO',
            'vas.existing_app.reschedule_appointment_icon' => 'NO'
        );
        $_SESSION['policyexpiredisables'] = $policyexpiredisables;
    }
    //end of policy expired conditions
}

$switchpolicy = sanitize_data(@$_POST['switchpolicysel']);
$productDetail = '';
//echo $_SESSION['policyNumber']; die;
if($switchpolicy!='' && ($_SESSION['switchpolicyCSRF']!=$_POST['switchpolicyCSRF']))
{
	die('Unauthorized Access');
}
if (isset($_SESSION['policyNumber']) && !empty($_SESSION['policyNumber']) && empty($switchpolicy)) {
    if (isset($_SESSION['response_error'])) {
        $responseError = $_SESSION['response_error'];
    } else {
        @$responseArray = $_SESSION['dataArray'];
    }

    $selectedPolicy = $_SESSION['policyNumber'];
} else {
    $selectedPolicy = $switchpolicy ? $switchpolicy : @$fetchPolListByUserId[0]['POLICYNUM'];
    if(isset($selectedPolicy) && $_SESSION['LOGINTYPE'] == 'CORPORATE'){
       $getresults = fetchListBySql("SELECT * FROM CRMEMPLOYEELOGIN WHERE LOWER(EMAIL)='" . strtolower(@$_SESSION['userName']) . "' AND POLICYNUMBER='$selectedPolicy' AND STATUS='ACTIVE' ");
                    //print"<pre>";print_r($getresults);die;
                    $_SESSION["employeeName"] = trim($getresults[0]['EMPLOYEENAME']);
                    $_SESSION["COMPANYID"] = trim($getresults[0]['COMPANYID']);
                    $_SESSION["policyNumberCheck"] = trim($getresults[0]['POLICYNUMBER']);
                    $_SESSION["policyNumber"] = $_SESSION['policyNumber'] = trim($getresults[0]['POLICYNUMBER']);
                    $_SESSION['policyNumNew'] = trim($getresults[0]['POLICYNUMBER']);
                    $_SESSION["customerIdCheck"] = trim($getresults[0]['CUSTOMERID']);
                    $_SESSION["employeeIdCheck"] = trim($getresults[0]['EMPLOYEEID']);
                    $_SESSION["emp_mobile"] = trim($getresults[0]['MOBILENUMBER']);
                    $_SESSION["USERID"] = trim($getresults[0]['LOGINID']);
                    $_SESSION['ISPASSWORDCHANGED'] = trim($getresults[0]['ISPASSWORDCHANGED']);
    
    }else{
    $_SESSION['policyNumber'] = @$selectedPolicy;
        $query['policyNo'] = $_SESSION['policyNumber'];//'10095042';//'10088252';//$_SESSION['policyNumber']; 
    }


    require_once($apiurl);
    if ($_SESSION['LOGINTYPE'] == 'RETAIL') {
        $renewalArray = getRenewalMemberDetails($query);
        $_SESSION['renewalArray'] = $renewalArray;
        $_SESSION['POLICYENDDATE'] = trim(@$renewalArray[0]['policyMaturityDt']['#text']);
        $_SESSION['POLICYSTARTDATE'] = trim(@$renewalArray[0]['policyCommencementDt']['#text']);
        //echo $_SESSION['POLICYSTARTDATE']; die;
        $policyExpirydate = strtotime(trim(@$renewalArray[0]['policyMaturityDt']['#text']));
    } else {
        // Check details in Webservice					
        if (isset($_SESSION["employeeIdCheck"])) {
            $query['EMPNO'] = @$_SESSION["employeeIdCheck"];
        }
        if (isset($_SESSION["customerIdCheck"])) {
            $query['CLNTNUM'] = @$_SESSION["customerIdCheck"];
        }
        if (isset($dob)) {
            $query['CLTDOBX'] = @$_SESSION["dobCheck"];
        }
        $query['CHDRNUM'] = @$_SESSION["policyNumber"];
        $dataArray = getPolicyEnquiry($query);
        if (isset($_SESSION['response_error'])) {
            $responseError = $_SESSION['response_error'];
        } else {
            $responseArray = $_SESSION['dataArray'];
        }

        if (@$responseArray[0]['BGEN-EMPNO']['#text'] != '') {
            $_SESSION['empId'] = @$responseArray[0]['BGEN-EMPNO']['#text'];
            $_SESSION['empNo'] = @$responseArray[0]['BGEN-EMPNO']['#text'];
            $_SESSION['clientNumber'] = @$responseArray[0]['BGEN-CLNTNM']['#text'];
        } else {
            $_SESSION['empNo'] = @$responseArray[0]['BGEN-EMPNO']['#text'];
            $_SESSION['empId'] = @$responseArray[0]['BGEN-CLNTNM']['#text'];
            $_SESSION['clientNumber'] = @$responseArray[0]['BGEN-CLNTNM']['#text'];
        }
    }
    /* echo "<pre>";
      print_r($renewalArray); die; */
// check policy expired or not 
    $currenttime = time();
    $_SESSION['policyexpiredisables'] = array();
    if ($currenttime > $policyExpirydate) {
        $_SESSION['POLICYEXPIRED'] = true;
        $policyexpiredisables = array(
            'dashboard.disable_download_policy_docs' => 'NO',
            'mypolicy.disable_download_policy_docs' => 'NO',
            'vas.discount_connect.download_coupon' => 'NO',
            'vas.discount_connect.download_in_search' => 'NO',
            'vas.my_plans.schedule_appointment_icon' => 'NO'
        );
        $_SESSION['policyexpiredisables'] = $policyexpiredisables;

        /* if(scriptname()!=='policy_expired.php'){
          header('location:policy_expired.php');
          }
          exit; */
    }
// end of check policy expired

    $dataArray = array();
    $stringMember = '';
//echo count($renewalArray); die;
    if (count($renewalArray) > 0) {
        if (isset($renewalArray[0]['policyNum']['#text']) && isset($renewalArray[0]['customerId']['#text']) && !empty($renewalArray[0]['policyNum']['#text']) && !empty($renewalArray[0]['customerId']['#text'])) {
            $_SESSION['customerId'] = $renewalArray[0]['customerId']['#text'];
            $query['CLNTNUM'] = $renewalArray[0]['customerId']['#text'];
            $query['CHDRNUM'] = $renewalArray[0]['policyNum']['#text'];
            //$query['CLNTNUM'] =50105600;
            //$query['CHDRNUM'] =10003683;
            $productDetail = fetchListCondsWithColumn("PRODUCTID,PRODUCTCODE,TITLE", "CRMRETAILPRODUCT", "WHERE regexp_like(PRODUCTCODE, '" . trim(@$renewalArray[0]['baseProductId']['#text']) . "', 'i') ");
            $_SESSION["productId"] = @$productDetail[0]['PRODUCTID'] ? $productDetail[0]['PRODUCTID'] : '';
            $_SESSION["productName"] = @$productDetail[0]['TITLE'] ? $productDetail[0]['TITLE'] : '';
            $_SESSION["productCode"] = @$productDetail[0]['PRODUCTCODE'] ? $productDetail[0]['PRODUCTCODE'] : '';
            $dataArray = getPolicyEnquiry($query);

            if (count($dataArray) > 0) {
                //$responseArray = $_SESSION['dataArray'];
                if (isset($_SESSION['response_error'])) {
                    $responseError = $_SESSION['response_error'];
                } else {
                    $responseArray = $_SESSION['dataArray'];
                }


                if (@$responseArray[0]['BGEN-EMPNO']['#text'] != '') {
                    $_SESSION['empId'] = @$responseArray[0]['BGEN-EMPNO']['#text'];
                    $_SESSION['empNo'] = @$responseArray[0]['BGEN-EMPNO']['#text'];
                    $_SESSION['clientNumber'] = @$responseArray[0]['BGEN-CLNTNM']['#text'];
                } else {
                    $_SESSION['empNo'] = @$responseArray[0]['BGEN-EMPNO']['#text'];
                    $_SESSION['empId'] = @$responseArray[0]['BGEN-CLNTNM']['#text'];
                    $_SESSION['clientNumber'] = @$responseArray[0]['BGEN-CLNTNM']['#text'];
                }



                $_SESSION["employeeName"] = trim($_SESSION['owner_name']);
                $_SESSION["employeeEmail"] = trim(@$responseArray[0]['BGEN-EMAIL']['#text']);
                $_SESSION["employeePhone"] = @$responseArray[0]['BGEN-CLTPHONE']['#text'] ? @$responseArray[0]['BGEN-CLTPHONE']['#text'] : trim(@$responseArray[0]['BGEN-CLTPHONE02']['#text']);
                $_SESSION["employeeName"] = trim(@$responseArray[0]['BGEN-MEMNAME']['#text']);

                // for member list

                if ((isset($responseArray[0]['BGEN-MEMNAME']['#text'])) && ($responseArray[0]['BGEN-MEMNAME']['#text'] != "")) {
                    $i = 0;
                    $membersarray = array();
                    foreach ($responseArray as $key => $response) {
                        if ($key == 0) {
                            $_SESSION['policyHolderName'] = trim($response['BGEN-MEMNAME']['#text']);
                            $_SESSION['policyHolderAge'] = trim($response['BGEN-AGE']['#text']);
                            $_SESSION['policyHolderMember'] = trim($response['BGEN-DPNTTYP']['#text']);
                            $_SESSION['policyHolderSI'] = trim($response['BGEN-ZBALCURR']['#text']);
                            if (trim($response['BGEN-GENDER']['#text']) == "F") {
                                $_SESSION['GENDER'] = "Female";
                            }
                            if (trim($response['BGEN-GENDER']['#text']) == "M") {
                                $_SESSION['GENDER'] = "Male";
                            }
                            $membersarray[] = strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);
                        } else {
                            $membersarray[] = strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);
                        }
                        $i++;
                    }
                    $_SESSION['membersarray'] = $membersarray;
                }
                // end of member list				
            }
            //$responseArray=getCorporatePolicyEnquiry($query2);
            //$_SESSION['NAME']==;
        }
    }
}

$scriptname = scriptname();
$tab = 'buyplan';
if ($scriptname = 'health_checkup.php') {
    $tab = base64_decode(sanitize_data(@$_REQUEST['tab']));
}
//$productSetupDetail=fetchcolumnListCond("WELLNESSHRASTART,WELLNESSHRAEND","CRMRETAILPRODUCTSETUP"," WHERE PRODUCTID=".@$_SESSION['productId']." ");
//Fetch theme color only for corporate
if ($_SESSION['LOGINTYPE'] == 'CORPORATE') {
    global $conn;
    $countQuery = "SELECT * FROM CRMPARTNERTHEME WHERE PARTNERID = " . @$wellnessList[0]['PARENTBROKERID'] . "";
    $countSql = @oci_parse($conn, $countQuery);
    @oci_execute($countSql);
    $row = @oci_fetch_row($countSql);

    $agentDetails = fetchcolumnListCond("BROKERLOGO", "CRMAGENT", " WHERE AGENTID=" . @$wellnessList[0]['PARENTBROKERID'] . " ");
    if (!empty($agentDetails)) {
        //$logoImagePath = RELIGAREHRCRMURL.'fileupload/'. $agentDetails[0]['BROKERLOGO'];

        if (($row[32] == 'DEACTIVE') || ($row[32] == '')) {
            $logoImagePath = 'img/logo.png';
        } else if (isset($agentDetails[0]['BROKERLOGO']) && !empty($agentDetails[0]['BROKERLOGO'])) {
            $logoImagePath = $imgURL . $agentDetails[0]['BROKERLOGO'];
        } else {
            $logoImagePath = 'img/logo.png';
        }
    } else {
        $logoImagePath = 'img/logo.png';
    }


    if (@$_SESSION['color'] == null) {
        $themeColor = array('MENUBG' => '#fff', 'MENUBGHOV' => '#fafafa', 'MENUFONT' => '#5F5F5F', 'MENUFONTHOV' => '#312c29', 'MENUFLOORHOV' => '#b7dc7d',
            'BOXBG' => '#eaeff5', 'BOXFONT' => '#444', 'BOXFONTHOV' => '#5e9d2d',
            'PAGETITLEFONT' => '#2e7d07', 'DASHBOARDNAMECOL' => '#ffffff',
            'BUTTONBG' => '#5e9d2d', 'BUTTONFONT' => '#fff', 'BUTTONBGHOV' => '#b7dc7d', 'BUTTONFONTHOV' => '#FFF',
            'SELDATEFONT' => '#fff', 'SELDATEBG' => '#5e9d2d', 'SELDTBGSHADOW' => '#178fe5', 'DATEBGHOV' => '#8a8a8a', 'DATEFONTHOV' => '#ffffff',
            'BORDER' => '#e0e2e3', 'BORDERHOV' => '#b7dc7d', 'ROWBORDER' => '#e0e2e3', 'SIDEICON' => '#000', 'SIDEICONHOV' => '#5e9d2d',
            'CONTBOXFONT' => '#444', 'CONTBOXBG' => '#d9f5c5', 'CONTBOXBORDER' => '#e2e4e5', 'CONTBOXBGHOVER' => '#b8db87', 'CONTBOXFONTHOVER' => '#2e7d07',
            'BORDER1' => '#e2e4e5');
        if (isset($row) && !empty($row)) {
            if (in_array('ACTIVE', @$row)) {
                $themeColor = array('MENUBG', 'MENUBGHOV', 'MENUFONT', 'MENUFONTHOV', 'MENUFLOORHOV', 'BOXBG', 'BOXFONT', 'BOXFONTHOV',
                    'PAGETITLEFONT', 'DASHBOARDNAMECOL', 'BUTTONBG', 'BUTTONFONT', 'BUTTONBGHOV', 'BUTTONFONTHOV',
                    'SELDATEFONT', 'SELDATEBG', 'SELDTBGSHADOW', 'DATEBGHOV', 'DATEFONTHOV', 'BORDER', 'BORDERHOV', 'ROWBORDER', 'SIDEICON', 'SIDEICONHOV',
                    'CONTBOXFONT', 'CONTBOXBG', 'CONTBOXBORDER', 'CONTBOXBGHOVER', 'CONTBOXFONTHOVER', 'BORDER1');
                $i = 2;
                $themeColorArray = array();
                foreach ($themeColor as $thCol) {
                    $themeColorArray[$thCol] = $row[$i];
                    $i++;
                }
                $_SESSION['color'] = $themeColorArray;
            } else if (in_array('DEACTIVE', @$row)) {

                @$_SESSION['color'] = $themeColor;
            }
        } else {
            $_SESSION['color'] = $themeColor;
        }
    }
} else {
    $logoImagePath = 'img/logo.png';
}
if ($_SESSION['ISPASSWORDCHANGED'] != 'YES' && scriptname() != 'my-profile.php') {
    echo "<script>window.location.href='my-profile.php?tab=Y3Bhc3M='</script>";
    exit;
}
if ($_SESSION['LOGINTYPE'] == 'CORPORATE') {
    $freeservices = GetCorpServiceListByPolicy(@$wellnessList[0]['POLICYID'], 'NO');
    $paidservices = GetCorpServiceListByPolicy(@$wellnessList[0]['POLICYID'], 'YES');
} else {
    $freeservices = GetRetServiceListByProduct(@$_SESSION['productId'], 'NO');
    $paidservices = GetRetServiceListByProduct(@$_SESSION['productId'], 'YES');
    $productlist = fetchListCondsWithColumn('BANNERID,EXTERNALLINK,BANNER,DESCRIPTION', "CRMCORPORATEPRODUCTS ", "where CRMCORPORATEPRODUCTS.STATUS='ACTIVE' ");
}
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Dashboard</title>
        <link rel="icon" type="image/ico" href="img/favicon.png">
        <!-- Bootstrap -->
        <?php if ($_SESSION['LOGINTYPE'] == 'RETAIL') { ?>
            <link href="css/bootstrap.min.css" rel="stylesheet">
            <link href="css/bootstrap-theme.css" rel="stylesheet">
            <link href="css/cs-select.css" rel="stylesheet">
            <link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/>
            <link href="css/owl.theme.css" rel="stylesheet">
        <?php } else { ?>
            <?php include_once 'css/bootstrap.min.php'; ?>
            <?php include_once 'css/bootstrap-theme.php'; ?>
            <?php include_once 'css/jquery.datetimepicker.php'; ?>
            <?php include_once 'css/owl.theme.php'; ?>
        <?php } ?>
        <!--<link href="css/bootstrap.min.css" rel="stylesheet">-->
        <!--<link href="css/bootstrap-theme.css" rel="stylesheet">-->

        <link href="css/font-awesome.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="css/demo.css" />
        <link rel="stylesheet" type="text/css" href="css/cs-select.css" />
        <!--<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/>-->

        <link href="css/owl.carousel.css" rel="stylesheet">

        <link rel="stylesheet" href="css/globals.css">
        <link rel="stylesheet" href="css/mobile.css">
        <!-- End Combine and Compress These CSS Files -->
        <link rel="stylesheet" href="css/responsive-tables.css">
        <!--[if lt IE 9]>
        <script src="js/html5.js"></script>
        <![endif]-->

        <script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
        <script src="js/common.js" type="text/javascript"></script>
        <script src="js/jquery.barrating.js"></script> 

        <?php if ($browser['browser'] == 'IE' && $browser['version'] == '8.0') { ?>
            <script>
                $(document).ready(function () {
                    $('.switchpolicysel').change(function () {
                        $('#switchpolicy').submit();
                    });
                });
            </script>
        <?php } else { ?>
            <script>
                $(document).ready(function () {
                    $('.switchpolicy li').click(function () {
                        $('#switchpolicy').submit();
                    });
                });
            </script>
        <?php } ?>

        <script>
            function remove_notify_counter() {
                $.ajax({
                    type: "post",
                    url: 'ajax/remove_notify_counter.php',
                    success: function (resp) {
                        $("#notify_counter").html("");
                    }
                });
            }
        </script>

        <meta charset="UTF-8">
        <meta name="Generator" content="EditPlus">
        <meta name="Author" content="">
        <meta name="Keywords" content="">
        <meta name="Description" content="">
    </head>
    <?php
    if (empty($_SESSION['loc_name']) or empty($_SESSION['loc_id'])) {
        $loc_name = "";
        $loc_id = "";
    }
    ?>
    <body>
        <div class=" container-fluid">
            <div class="topContainer">
                <div class="col-md-8">
                    <div class="topContainer-logo"> <a href="javascript:void(0);"><img src="<?php echo $logoImagePath; ?>" alt="Logo" title=""></a> </div>

                    <div class="con-in1">
                        <form action="" method="post" name="switchpolicy" id="switchpolicy" class="switchpolicy" title="Switch Policy">
                            <?php if (count($fetchPolListByUserId) > 0): ?>
                                <select class="cs-select cs-skin-border fa arrow switchpolicysel" name="switchpolicysel">
                                    <option value="" disabled>Switch Policy</option>
                                    <?php
                                    $p = 0;
                                    while ($p < count($fetchPolListByUserId)) {
                                        if ($_SESSION['policyNumber'] == $fetchPolListByUserId[$p]['POLICYNUM']) {
                                            $sel = "selected";
                                        } else {
                                            $sel = '';
                                        }
                                        ?>
                                        <option value="<?php echo @$fetchPolListByUserId[$p]['POLICYNUM']; ?>" <?php echo $sel; ?>><?php echo @$fetchPolListByUserId[$p]['POLICYNUM']; ?></option>
                                        <?php $p++;
                                    }
									$_SESSION['switchpolicyCSRF']=md5(rand(1, 99999999));
                                    ?>
                                </select>
								<input type="hidden" name="switchpolicyCSRF" id="switchpolicyCSRF" value="<?php echo $_SESSION['switchpolicyCSRF']; ?>">
                            <?php endif; ?>
                        </form>
                    </div>
                    <!--<div class="con-in">
                      <form>
                        <input type="text" class="form-control" placeholder="Search">
                        <input type="submit" class="btn btn-danger" value="">
                      </form>
                    </div>-->
                </div>
                <div class="col-md-4 topContainer-us-icon-pad">

                    <div class="topContainer-us-icon topContainer-us-icon1 ">
                        <ul>
                            <li><a href="logout.php" title="Logout"><i class="fa fa-sign-out fa-fw"></i></a></li>
                            <li class="dropdown" title="Get in touch with us">
                                <a class="dropdown-toggle usertoggle" data-toggle="dropdown" href="#"> <i class="fa fa-fw">?</i></a>  
                                <ul class="dropdown-menu dropdown-tasks">
                                    <h1>Get in touch with us</h1>
                                    <div class="successMsg" style="display:none;text-align:center;color:green;">Enquiry Has Been Sent Successfully</div>
                                    <form action="" method="post" name="enquiryForm" id="enquiryForm" class="enquiryForm" onSubmit="return validateEnquiry()">
                                        <div class="Quote-box">
                                            <li>
                                                <input type="text" class="form-control" id="employeeName"  name="employeeName" required placeholder="Employee Name *" value="<?php echo trim(@$_SESSION['employeeName']); ?>">
                                            </li>
                                            <li>
                                                <input type="text" onKeyPress="return kp_numeric(event)" class="form-control phone" maxlength="10"  id="phone" required name="phone" placeholder="Phone" value="<?php echo trim(@$_SESSION['employeePhone']); ?>">
                                            </li>
                                            <li>
                                                <input type="text" class="form-control emailid" id="emailid" required name="emailid" placeholder="Email Id" value="<?php echo trim(@$_SESSION['employeeEmail']); ?>">
                                            </li>
                                            <li>
                                                <input type="text" onKeyPress="return kp_numeric(event)" maxlength="8" class="form-control policyNumber"  required id="policyNumber" name="policyNumber" placeholder="Policy Number" value="<?php echo trim(@$_SESSION['policyNumber']); ?>">
                                            </li>
                                            <li>
                                                <textarea placeholder="Enquiry" rows="3" class="enquiry" required name="message" id="enquiry"></textarea>
                                            </li>
                                            <li>
                                                <input name="enquiry" type="submit" class="btn btn-danger" value="Submit">
                                        </div>
                                        </li>
                                    </form>
                                    <!-- <li> <i class="fa fa-fw"></i> 1860-500-4488 | 1800-200-4488 </li> -->
                                </ul>
                                <!-- /.dropdown-tasks -->
                            </li>
                            <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#" title="Account Settings"> <i class="fa fa-user fa-fw"></i> </a>
                                <ul class="dropdown-menu dropdown-user">
                                    <h1>Account Settings</h1>
                                    <li><a href="my-profile.php">My Profile</a> </li>
                                    <li><a href="my-profile.php?tab=<?php echo base64_encode('cpass'); ?>">Change Password</a> </li>
                                    <?php if ($_SESSION['LOGINTYPE'] == 'RETAIL') { ?>
                                        <li><a href="proposals.php">My Proposals</a> </li>
                                    <?php } ?>
                                    <li><a href="individual-policy-registration.php" target="_blank">Register Policy</a> </li>
                                    <li><a href="logout.php"> Logout</a> </li>
                                </ul>
                                <!-- /.dropdown-user --> 
                            </li>
                            <li class="dropdown" title="Notifications">
                                <?php
                                $currentDate = date("d-M-Y");
                                $date = "to_date('$currentDate','DD-MONTH-YYYY')"; // ORACLE TO_DATE() FUCNTION TO COMPARE DATES
                                $notification_list = fetchListCond("LWNOTIFICATIONMASTER", " WHERE STARTDATE<=" . $date . "AND ENDDATE>=" . $date . " AND (USERTYPE='retail' OR USERTYPE='retailcorporate') AND STATUS='ACTIVE' AND ISREAD='NO'  order by CREATEDON DESC");
                                ?>
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#"> 
                                    <i class="fa fa-bell fa-fw">
                                        <span onclick="remove_notify_counter();" id="notify_counter" ><?php
                                            if (count($notification_list) > 0) {
                                                echo count($notification_list);
                                            }
                                            ?></span>
                                    </i> 
                                </a>


                                <ul class="dropdown-menu dropdown-alerts">
                                    <?php include_once("notifications.php"); ?>
                                </ul>
                                <!-- /.dropdown-alerts --> 
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <section class="header"> 

                <!-- nevigation -->
                <div class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
                    </div>
                    <div class="navbar-collapse collapse">
                        <ul class="nav navbar-nav">
                            <li class="dropdown-toggle <?php
                            if (scriptname() == 'dashboard.php') {
                                echo 'active';
                            }
                            ?>"><a href="dashboard.php"><img src="img/dashboard.png" alt="" title=""> Dashboard</a></li>
                                <?php if (@$TPAManagedchkdis == 'No') { ?>
                                <li class="dropdown-toggle <?php
                                if (scriptname() == 'my_policy.php') {
                                    echo 'active';
                                }
                                ?>"> <a href="my_policy.php"><img src="img/policy.png" alt="" title=""> My Policy</a></li>
                                <li class="dropdown-toggle  <?php
                                if (scriptname() == 'claims.php') {
                                    echo 'active';
                                }
                                ?>"> <a href="claims.php"><img src="img/claim.png" alt="" title=""> Claims &amp; Network</a> </li>
                                <?php } ?>
                                <?php
// condition to hide Value Added Services menu from header if user comes from mobile
// only for corporate
                                if ($_SESSION["OSTYPE"] != 'MOBILEAPP') {
                                    ?>
                                <li class="dropdown-toggle <?php
                                if (scriptname() == 'value_added_services.php') {
                                    echo 'active';
                                }
                                ?>"> <a href="value_added_services.php"><img src="img/service.png" alt="" title=""> Value Added Services</a> </li>

<?php } ?>
<?php  if (env("DISABLE_CMS")=="false"){ ?>
  <li class="dropdown-toggle <?php
                                if (scriptname() == 'add-testimonial.php' || scriptname() == 'questionnaire.php') {
                                    echo 'active';
                                }
                                ?>"> <a href="add-testimonial.php"><i class="feedback_icon fa fa-comments-o"></i> Feedback</a> </li>
                                <?php } ?>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                    <!--nav-collapse --> 
                </div>
                <!-- nevigation closed --> 

            </section>
        </div>