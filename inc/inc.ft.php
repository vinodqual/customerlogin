<div class="footer">
  <div class="container-fluid">
    <div class="copy">
      <div class="footertext"> Insurance is the subject matter of solicitation | IRDA Registration No. 148. Copyrights 2013, All right reserved by Religare Health Insurance Company ltd.
        Reg Office - Religare Health Insurance Company Limited, 5th Floor, 19, Chawla House, Nehru Place, New Delhi-110019 This site is best viewed in Firefox 3 +, IE 10 +, Chrome 10 + and Safari 5 </div>
      <div class="col-md-4 cont-right" style="display:none;">
        <div class="soc"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> 
          <!--<a href="#"><i class="fa fa-fw"></i></a>--> 
          
        </div>
      </div>
    </div>
  </div>
 </div>
 <?php
 if($_SESSION["OSTYPE"] != 'MOBILEAPP'){
 include_once('feedback_mail.php');
 }
 ?>  
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 

<script src="js/owl.carousel.js"></script> 
<script src="js/responsive-tables.js"></script> 

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<!-- Demo --> 

<script>
    $(document).ready(function() {
         $('.highlight').attr('title','Health Checkup');
      $("#owl-demo").owlCarousel({

      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true

      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false

      });
    });
    </script> 
<script src="js/classie.js"></script> 
<script src="js/selectFx.js"></script> 
<script>
			(function() {
				[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
					new SelectFx(el);
				} );
			})();
		</script> 
<script src="js/jquery.datetimepicker.js"></script> 
<?php  
$date_cur=date("Y/m/d", time());
$appointment_dates[$date_cur]='Some_desc';
if(@$_SESSION["policyNumber"]!=""){
     @$appointments=fetchListCondsWithColumn('POLICYNUMBER,SLOTID,CENTERID,DATE1,DATE2','CRMRETAILPOSTAPPOINTMENT'," where POLICYNUMBER = '".$_SESSION["policyNumber"]."'  "); 
    $appointment_dates=array();  
     foreach(@$appointments as $appointment)
     { 
         if($appointment["DATE1"]!=""){ $date1='';
                $date1=date("Y/m/d",  strtotime($appointment["DATE1"]));
                $appointment_dates[$date1]="Some_desc";  
          }
          if($appointment["DATE2"]!=""){ 
                 $date2=date("Y/m/d",  strtotime($appointment["DATE2"]));
                 $appointment_dates[$date2]="Some_desc"; 
          }
     } 
} 
 
 
?>
<script>  
  var dates=[]; 
 <?php  foreach($appointment_dates as $key => $val){ ?>  
    dates['<?php echo $key ?>'] = "<?php echo $val ?>" ;
 <?php } ?>
 
 
  $('#datetimepicker3').datetimepicker({ 
   inline:true,
    beforeShowDay: function(date) {  
      var search = date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate(); 
      if (search in dates) { 
        return [false, 'highlight', (dates[search] || '')];
      }

      return [false, '', ''];
    }
  });
 
</script> 
<!-- popup tab --> 
<script src="js/responsive-tabs.js"></script> 
<script type="text/javascript">
      $( '#myTab a' ).click( function ( e ) {
        e.preventDefault();
        $(this).tab('show');
      } );

      $( '#moreTabs a' ).click( function ( e ) {
        e.preventDefault();
        $(this).tab('show');
      } );

      ( function( $ ) {
          // Test for making sure event are maintained
          $( '.js-alert-test' ).click( function () {
            alert( 'Button Clicked: Event was maintained' );
          } );
          fakewaffle.responsiveTabs( [ 'xs', 'sm' ] );
      } )( jQuery );

/*$(".submit-btn").click(function () {
	$("#myplanForm").hide();
	$("#myPlanList").show();
});*/
    </script> 

 <script src="js/jquery-ui_1.11.4.js"></script>

<!-- popup tab -->


<script type="text/javascript">
var $ = jQuery.noConflict();
$(document).ready(function() {
	  var owl = $(".brands");
      owl.owlCarousel({
      items : 3, //10 items above 1000px browser width
      itemsDesktop : [1400,3], //5 items between 1400px and 1025px
      itemsDesktopSmall : [1024,3], // 3 items betweem 1024px and 901px
      itemsTablet: [900,2], //2 items between 900 and 481;
      itemsMobile : [480,1], //1 item between 480 and 0;
	  rewindNav: false,
	  pagination: false,
	  navigation : true // itemsMobile disabled - inherit from itemsTablet option
      });
	  /*-----------------------------------*/
});
</script>  

</body>
<?php $_SESSION['securityToken']=md5(rand(1, 99999999)); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-87350784-2', 'auto');
  ga('send', 'pageview');
//added by cks
$.ajaxSetup({
headers: {  'csrfQc' : '<?php echo $_SESSION['securityToken']; ?>' }
});
//ended by cks
</script>




</html>