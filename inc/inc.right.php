<?php
/*****************************************************************************
* COPYRIGHT
* Copyright 2016 Qualtech Consultants Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
* $Author: Chandan Singh $
*
****************************************************************************/
?>
<!--   <script src="js/jquery.min.js"></script> -->
<script src="js/bootstrap.min.js"></script> 
<script src="js/owl.carousel.js"></script>
<script src="js/jquery.datetimepicker.js"></script> 
<script src="js/responsive-tabs.js"></script> 
<script src="js/classie.js"></script> 
<script src="js/selectFx.js"></script> 
<script src="js/jquery-ui.js"></script> 
<link media="screen" rel="stylesheet" href="css/jquery-ui.css" />
   <script>
    
     $('#search_keyword').bind("keyup",function(e){ 
    var city_id=$("#city_id").val(); 
       if(e.keyCode!=13){
        $("#search_keyword_virtual").val("");
       }
        $( "#search_keyword" ).autocomplete({
                  source: "ajax/search_doc_ajax.php?city_id="+city_id+"&",
                  data:"data[city_id]=" + city_id,
                  minLength: 2, 
                  autoSelectFirst: true,
                  search: function(event, ui) {
                                        $("#doc_load_image").show();
                                         $("#doc_search_result").html("");
                                    },
                                    response: function(event, ui) {
                                        $("#doc_load_image").hide();
                                    },
                  select: function( event, ui ) { 
           
                 result_id=ui.item.id; 
                            $("#search_keyword_virtual").val(result_id); 
                            
                            search_for_doc(result_id); 
          $("#doc_load_image").hide();
                              
                }
                });
   });
  
   $(document).on('keyup','#search_keyword_side', function() {
   var city_id=$("#city_id_side").val();
        $("#search_keyword_virtual_side").val("");
        $("#doc_load_image_side").show();
        $( "#search_keyword_side" ).autocomplete({
                  source: "ajax/search_doc_ajax.php?city_id="+city_id+"&",
                  data:"data[city_id]=".city_id,
                  minLength: 2,
                  search: function(event, ui) {
                                        $("#doc_load_image_side").show();
                                    },
                                    response: function(event, ui) {
                                        $("#doc_load_image_side").hide();
                                    },
                  select: function( event, ui ) { 
                       
                      $("#search_keyword_virtual_side").val(ui.item.id);
                    
                            search_for_doc(ui.item.id);    
                            $("#doc_load_image_side").hide();
                }
                });
   
   });
  function search_for_doc(search_id)
  {
      
       $("#search_result").html("");
       $("#doc_load_image").show();
       var city_id=$("#city_id").val(); 
           var sorting_id=$("#sorting_id").val(); 
         $.ajax({
                type:"post",
                url:'ajax/search_for_doctors.php',
                data:{'search_id':search_id,'city_id':city_id,'sorting_id':sorting_id},
                success:function(resp){                                         
                      //  $("#search_result").html(resp);
                      $("#doc_search_result").html(resp);
                       $("#doc_load_image").hide();
                        } 
                });      
  }
  $('#search_city').bind("keyup",function(e){
   if(e.keyCode!=13){
         $("#city_id").val("");
       }
      
        $("#search_keyword").val("Doctors, Centers, Specialities, Services");
        $("#search_keyword_virtual").val("");
        
        $("#search_result").html("");
                 $( "#search_city" ).autocomplete({
                            source: "ajax/search_city_ajax.php",
                            minLength: 2,
                            search: function(event, ui) {
                                        $("#doc_load_image").show();
                                    },
                                    response: function(event, ui) {
                                        $("#doc_load_image").hide();
                                    },
                            select: function(event, ui) {
                                           $("#city_id").val(ui.item.id);
                                        
                                       }
                                   });
    });
 
         
         
         
         $(document).on('keyup','#search_city_side', function(e) {
                if(e.keyCode!=13){
                       $("#city_id_side").val("");
                     }
                $("#search_keyword").val("Doctors, Centers, Specialities, Services");
                $("#search_keyword_virtual_side").val("");
               // $("#doc_load_image_side").show();
                $("#search_result").html("");
                            $("#search_city_side").autocomplete({
                     source: "ajax/search_city_ajax.php",
                     minLength: 2,
                     search: function(event, ui) {
                         $("#doc_load_image_side").show();
                     },
                     response: function(event, ui) {
                         $("#doc_load_image_side").hide();
                     },
                     select: function(event, ui) {
                         $("#city_id_side").val(ui.item.id);
                     }
                 });
             });
    
      function log( message ) {
      $( "<div>" ).text( message ).prependTo( "#log" );
      $( "#log" ).scrollTop( 0 );
    }
    
    function search_by_button()
    {
      //  $("#search_result").html("");
        $("#doc_load_image").show();
        var city_id=$("#city_id").val(); 
        var search_id= $("#search_keyword_virtual").val();
        var sorting_id=$("#sorting_id").val(); 
        if(search_id=="")
            {
               // alert("Please select Any Center, Doctor ,Service ,Specialty or Ailment");
               $("#doc_load_image").hide();
                return false;
            }
         $.ajax({
                type:"post",
                url:'ajax/search_for_doctors.php',
                data:{'search_id':search_id,'city_id':city_id,'sorting_id':sorting_id},
                success:function(resp){    $("#doc_search_result").html(resp);
                         $("#doc_load_image").hide();
                        } 
                });   
    }
    $(document).ready(function(){ 
     $('.sorting li').click(function()
                   {
                       $("#doc_search_result").html("");
                  search_by_button();
                   });  });
  </script> 
  <style>    
    .doc_load_img
    {
        margin-left: 9px;
        margin-top: 29px;
    }
    .vsms {
        margin: 10px 0 0 23px;
        padding: 0;
    }
    .optional_input_large
    {
        background: #ffffff;
        width:280px;
        font:15px/28px "FS Humana",Arial,Helvetica,sans-serif;
    }
   .ui-menu-item a
   {
       font-size:11px;
   }
   .ui-menu
{
max-height: 300px;
    overflow: auto;
}
 
    .highlight div {
    border: 1px solid #ccc;
    margin: 3px;
     
}
    


    .ui-menu-item
    {
        border-bottom: 1px solid #ccc;
    }
    
    .ui-autocomplete li
    {
        font-weight  :normal;
        font-size: 14px;
        padding: 5px !important;
        
    }
    
    .ui-autocomplete  
    {
        font-weight  :normal;
        font-size: 14px;
        padding: 5px !important;
        
    }
     .ui-menu-item:last-child {
   border-bottom: none;
}
.ui-menu{
    border:1px solid #ccc;
}
/*    .ui-menu .ui-menu-item
    {
        
    }*/
    div.cs-selectwidth
    {
        width:195px;
    }
    /*
    ::-webkit-scrollbar{
  width:0px;   
  background:transparent;
}*/
    ::-moz-scrollbar{
  width:0px;   
  background:transparent;
}
scrollbar {
/*  clear useragent default style*/
   -moz-appearance: none !important;
}
/* buttons at two ends */
scrollbarbutton {
   -moz-appearance: none !important;
}
/* the sliding part*/
thumb{
   -moz-appearance: none !important;
}
scrollcorner {
   -moz-appearance: none !important;
   resize:both;
}
/* vertical or horizontal */
scrollbar[orient="vertical"] {
    color:silver;
}


</style>

<div class="middlebox-right">
<?php
/* code added by Dinesh to display appointments in Date month format 28-12-2015 11:52 am */
$employeeID = @$_SESSION['empId'];
if(@$_SESSION["policyNumber"]!=""){
	if(count($freeservices) > 0 || count($paidservices) > 0){
		$pk=0;
		$displayCount=0;
		
		
		 while($pk<count($freeservices)){
			 $recorddata=preg_replace('/\s+/', '', strtolower($freeservices[$pk]['SUBMODULENAME']));
			 if($recorddata=='healthcheckup'){
				 $displayCount=1;
			 }
		 $pk++;
		 }
		 $pk=0;
		 while($pk<count($paidservices)){
			  $recorddata=preg_replace('/\s+/', '', strtolower($paidservices[$pk]['SUBMODULENAME']));
			  if($recorddata=='healthcheckup'){
				 $displayCount=1;
			 }
		 $pk++;
		 }
		 if($displayCount>0){
	 ?>
	 
<?php
// condition to hide Book Appointment Now section from right side bar if user comes from mobile
// only for corporate
	if($_SESSION["OSTYPE"] != 'MOBILEAPP')
	{
?>	 
<div class="claim">
	<div id="demo">
	  <div id="owl-demo" class="owl-carousel">
		<div class="item item2" style="width:100%;">
		  <img src="img/claim1.png" alt="" title=""> <div class="bookap">Book Appointment Now</div>
			<div class="link_bookappointment"><a href="Health_checkup.php?tab=<?php echo base64_encode('myplan'); ?>">click here</a></div>
		</div>
	  </div>
	</div>
</div>
<?php } ?>

<?php } } ?>
<!-- upcomming events block starts-->

<div class="upcomming">          
	<h1>Upcoming Events</h1>
	<div class="upcomming-scroll">
	<?php
     //@$appointments=fetchListCondsWithColumn('POLICYNUMBER,SLOTID,CENTERID,DATE1,DATE2','CRMRETAILPOSTAPPOINTMENT'," where POLICYNUMBER = '".$_SESSION["policyNumber"]."' and EMPLOYEEID = '".@$employeeID."' order by APPOINTMENTID DESC ");  // query to get appointmnets
    //$appointment_dates=array();  
	$ST = 'PENDING';
        $ST2 = 'CONFIRM';
$today_date = date('d-M-Y');
                @$appointments = fetchListCondsWithColumn('POLICYNUMBER,SLOTID,CENTERID,DATE1,DATE2', 'CRMRETAILPOSTAPPOINTMENT', " where POLICYNUMBER = '" . $_SESSION["policyNumber"] . "' AND (STATUS = '" . $ST . "' OR STATUS = '" . $ST2 . "')  AND DATE1>='$today_date' order by APPOINTMENTID DESC ");  // query to get appointmnets
	
	
     foreach(@$appointments as $appointment)
     { 
         if($appointment["DATE1"]!=""){ 
				$date1='';
			    $date1=date("d M Y",  strtotime($appointment["DATE1"]));	   
          } ?>
		  <div class="upcomming-events">
		  <?php echo $date1.' Health check-up appointment'; ?>
		  </div>
<?php       // if($appointment["DATE2"]!=""){ 
                //$date2='';
				 //$date2=date("d M",  strtotime($appointment["DATE2"]));
			?>
			<!--<div class="upcomming-events">
		   <?php //echo $date2.' Health check-up appointment'; ?>
		   </div> -->
<?php   //}
     }
	 if(empty($_SESSION['POLICYENDDATE'])){
	  @$policies=fetchListCondsWithColumn('POLICYENDDATE','CRMPOLICY'," where POLICYNUMBER = '".$_SESSION["policyNumber"]."'  ");  // get policy expiry date
	  foreach ($policies as $policy){
		  if($policy['POLICYENDDATE'] !=''){ 
		   $policyExpiry ='';
		  $policyExpiry=date("d M Y",  strtotime($policy['POLICYENDDATE']));
		  ?>
			<div class="upcomming-events">
		   <?php echo $policyExpiry.' Policy expiry date'; ?>
		   </div>
<?php   }
	  } } else {
		  $policyExpiry=date("d M Y",  strtotime($_SESSION['POLICYENDDATE']));
		 ?>
		 <div class="upcomming-events">
		   <?php echo $policyExpiry.' Policy expiry date'; ?>
		   </div>        
         <?php 
	  }
	 if(@$appointment["DATE1"]=="" && @$appointment["DATE2"]==""){ 
		echo '<div class="upcomming-events"> No appointments</div>';
	 }	 
?>
</div>
</div>
<?php }?>
<!-- upcomming events block ends-->	  
          <!--<div class="calander">
            <h1></h1>
            <input type="text" id="datetimepicker3"/>
          </div> -->
<?php
// condition to hide Find Doctors section from right side bar if user comes from mobile
// only for corporate
	if($_SESSION["OSTYPE"] != 'MOBILEAPP')
	{
?>
          <div class="doctor">
              
            <h1>Find Doctors, Centers, Services, etc.</h1>
            <div class="doctor-bot">
                <form action="wecare.php" method="post">
              <input type="text"   class="doctor-map-pin"   id="search_city_side"   name="city_name"   value="<?php echo @$loc_name?$loc_name:'Location*' ;?>" onFocus="if (this.value =='<?php echo @$loc_name?$loc_name:'Location*' ;?>') {this.value = '';}" onBlur="if (this.value == '') {this.value = '<?php echo $loc_name?$loc_name:'Location*' ;?>';}" >
<!--              <input type="text"   class="doctor-map-pin"   id="search_city_side" onkeyup="city_function_side()"  name="city_name"   value="<?php //echo @$loc_name?$loc_name:'Location*' ;?>" onFocus="if (this.value =='<?php // echo @$loc_name?$loc_name:'Location*' ;?>') {this.value = '';}" onBlur="if (this.value == '') {this.value = '<?php //echo $loc_name?$loc_name:'Location*' ;?>';}" >-->
              
              <input id="city_id_side" type="hidden" class="txtfield_185"  name="city_id" value='<?php echo  $loc_id;?>' />
              <input type="text" placeholder="Doctors, Centers, Specialities, Services" name="keyword"  class="doctor-sesarch"  id="search_keyword_side"   onFocus="if (this.value == 'Doctors, Centers, Specialities, Services.') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Doctors, Centers, Specialities, Services.';}"  value="Doctors, Centers, Specialities, Services."  >
<!--               <input type="text" placeholder="Doctors, Centers, Specialities, Services" name="keyword"  class="doctor-sesarch"  id="search_keyword_side"  onkeyup="search_function_side()" onFocus="if (this.value == 'Doctors, Centers, Specialities, Services.') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Doctors, Centers, Specialities, Services.';}"  value="Doctors, Centers, Specialities, Services."  >-->
              <input id="search_keyword_virtual_side" type="hidden" name="search_keyword_virtual" />
              <img style="display:none;" id="doc_load_image_side" src="images/loading.gif" />
              <input type="submit" value="Search >" >
            </form>
              </div>
          </div>
<?php } ?>
         <!--   <div class="claim">
            <div id="demo">
              <div id="owl-demo" class="owl-carousel">
                <div class="item">
                  <p> <img src="img/claim1.png" alt="" title=""> Claim Intimation<br>
                    <a href="claims.php?tab=<?php //echo base64_encode("reimbur"); ?>">click here</a></p>
                </div>
                <div class="item">
                  <p> <img src="img/claim1.png" alt="" title=""> Claim Status <br>
                    <a href="claims.php">click here</a></p>
                </div>
              </div>
            </div>
          </div> -->
           <?php if(@$TPAManagedchkdis=='No'){ ?>
          <div class="network" style="display:none;">
            <div class="network-left"> <a href="downloadCenter.php"> <img src="img/download.png" alt="" title=""><br>
              Download Documents </a> </div>
            <div class="network-right"> <a href="network-hospital-for-uat.php"> <img src="img/network.png" alt="" title=""><br>
              Network Hospitalss </a> </div>
          </div>
          <?php } if($_SESSION['LOGINTYPE']=='RETAIL'){?>

        <div class="doctor" style="min-height:0px;">
            <a target="_blank" style="text-decoration:none;" href="individual-policy-registration.php"><h1><!--Register-->Map Policy</h1></a>
        </div>
    <?php } ?> 
</div>

 
