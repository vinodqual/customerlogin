<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Religare Register</title>
<meta name="description" content="{$METADESC}">
<meta name="keywords" content="{$METAKEYWORDS}"/>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="author" content="">
<link rel="shortcut icon" href="favicon.ico">
<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css" >

<link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="css/stylesheet.css" >
<link rel="stylesheet" type="text/css" href="css/styles.css" >
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" >
<link rel="stylesheet" type="text/css" href="css/style.css" >

<?php $browser = get_browser(null, true);
if ($browser['browser'] == 'IE' && $browser['version'] == '8.0') {
    ?>
    <script type="text/javascript" src="js/jquery.js"></script> 	
<?php } else { ?>
    <script type="text/javascript" src="js/jquery.min.js"></script>
<?php } ?>
			
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">	
<script language="javascript" src="js/encrypt.js"></script>
<script src="js/common.js"></script>		
