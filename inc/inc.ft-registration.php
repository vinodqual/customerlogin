<footer class="footer">
  <div class="footer-box">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="disclaimer"><a href="#">Disclaimer</a> | <a href="#">Privacy Statement</a> | <a href="#">Terms &amp; Conditions</a> | <a href="#">Sitemap</a> | <a href="#">Media Center</a> | <a href="#">Public Disclosures</a> | <a href="#">Wellness</a> | <a href="#">IRDA  &amp;  Consumer Education</a> | <a href="#">Do not call</a><br>
                            Insurance is the subject matter of solicitation | IRDA Registration No. 148. Copyrights 2013, All right reserved by Religare Health Insurance Company ltd.<br>
                            Reg Office - Religare Health Insurance Company Limited, 5th Floor, 19, Chawla House, Nehru Place, New Delhi-110019</div>
                    </div>
                    <div class="col-md-4">
                        <div class="nortonIcon"></div>
                        <div class="followus"> 
						<a class="fusIcon1" href="https://www.facebook.com/ReligareHealthInsurance"></a>
						<a class="fusIcon2" href="https://plus.google.com/+ReligareHealthInsuranceIndia/posts"></a>
						<a class="fusIcon3" href="https://www.youtube.com/user/ReligareHealthIns"></a>
						<a class="fusIcon4" href="https://www.linkedin.com/company/religare-health-insurance"></a> 
						</div>
                    </div>
                </div>
            </div>
        </div>
</footer>
<script src="js/owl.carousel.js" type="text/javascript"></script>
<!-- Demo --> 
<script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true
      });
    });
    </script>
<?php 
if($browser['browser'] == 'IE' && $browser['version'] == '8.0'){ ?>
<script type="text/javascript" src="js/jquery.js"></script>   
<?php } else {  ?>
<script  src="js/jquery.min.js"></script> 
<?php } ?>
<?php include_once('feedback_mail.php'); ?> 
<script src="js/bootstrap.min.js"></script>
<script src="js/datepiker.js"></script> 
<script>
 var DT = jQuery.noConflict();
DT(function() {
DT('#datepicker').datepicker( {
        changeMonth: true,
        changeYear: true,
	    yearRange: "-99:-0",
	   dateFormat: "dd/mm/yy"
 
    });
});
</script>
<script type="text/javascript">
$(document).ready(function(){
$(".grCross").click(function(event){
    $('.clickarrow, .clickarrowDiv').hide();

});
    $('#selectMeClaim').change(function () {
        $('.claimdisplayContent').hide();
        $('#'+$(this).val()).show();
    })
	$('#selectMeService').change(function () {
        $('.claimdisplayContent1').hide();
        $('#'+$(this).val()).show();
    })
});

$(".rightnavBox ul li a.claimcl").click(function(){
	$("#messagevalue").html('');
        $('#clickarrowDivServices,.clickarrow').hide();
	$(this).parent().children("div").show();	
	$('#clickarrowDivClaims').show();
     
});

$(".rightnavBox ul li a.servicecl").click(function(){
	$("#messagevalue").html('');
        $('#clickarrowDivClaims,.clickarrow').hide();	
	$(this).parent().children("div").show();	
	$('#clickarrowDivServices').show();    
});

$('.navigation, .nav1').click(function(event) {
  $('html').one('click',function() {
	$('.nav1').hide();
  });
  event.stopPropagation();
});
</script>
<script type="text/javascript">
function ranvir_tab(en)
{
    var tabcont_id  = $(en).attr("id");
	$("#claimstatustab,#claimprocesstab,#nhospitaltab").hide();
	$("#"+tabcont_id+"tab").show();
	$("#claimstatus,#claimprocess,#nhospital").removeClass("activetab");
	$(".claimstatus ul li a").removeClass("activetab");
	$(en).addClass("activetab");
}
function ranvir_tabS(en)
{
    var tabcont_id  = $(en).attr("id");
    $("#tax_receiptstab,#customertab,#branch_locatortab").hide();
    $("#"+tabcont_id+"tab").show();
    
    $(".claimdisplaynav1 ul li a").removeClass("activetab");
    $(en).addClass("activetab");

}
</script>
