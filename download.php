<?php //$strictfilenamearray = array(0=>'php',1=>'PHP',2=>'html',3=>'HTML',4=>'.php',5=>'.PHP',6=>'.html',7=>'.HTML',8=>'css',9=>'CSS',10=>'.css',11=>'.CSS',12=>'.js',13=>'.JS',14=>'\/',15=>'.in',16=>'boot');
$strictfilenamearray = array('php','PHP','html','HTML','.php','.PHP','.html','.HTML','css','CSS','.css','.CSS','.js','.JS','.in','boot','ini','in');

/*****************************************************************************
* COPYRIGHT
* Copyright 2013 Catabatic Automation Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: download.php,v 1.0 2013/04/01 17:30:17 ashish gupta Exp $
* $Author: ashish gupta $
*
****************************************************************************/

$file = @$_GET['fileName']?@$_GET['fileName']:@$_GET['filename'];

$file_parts = pathinfo(@$file);

$fileExt= $file_parts['extension'];
$fileExt=strtolower($fileExt);
if(in_array($fileExt,$strictfilenamearray)==1 or empty($file)){
echo "error";
} else {

include_once("conf/conf.php");           //include configuration file 
include_once("conf/common_functions.php");          // include function file
   // The file path where the file exists
//$file = $fileuploadurl.$_GET['fileName']."";
$file = FILE_UPLOAD_TEMP_PATH.$_GET['fileName']; //get file name from temp dir
//header('Cache-Control: no-cache, no-store, must-revalidate'); // HTTP 1.1.
header("Cache-Control: private");
header("Pragma: cache");
header('Expires: 0'); // Proxies.
header("Content-Type: application/force-download");
header( "Content-Disposition: attachment; filename=".basename($file));
header( "Content-Description: File Transfer");

@readfile($file); 

unset($_SESSION['uid']); 
} 
 
  ?>