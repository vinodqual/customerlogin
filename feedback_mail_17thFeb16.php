<?php  include_once("conf/conf.php");    //include configuration file to access database connectivity
include_once("conf/common_functions.php");  //include common_functions file to access defined function
$pageName = basename($_SERVER['HTTP_REFERER']);
if($pageName!="")
{
  $fPageName = $pageName;
} else { 
  $fPageName = "index";
}

if(isset($_POST['feedbackEmail']))
{
    if(sanitize_data($_POST['pageName'])!=''){
$email=sanitize_data_email($_POST['feedbackEmail']);
$message=sanitize_data($_POST['feedbackMessage']);
$ext = array('pdf', 'docx', 'doc', 'txt', 'gif', 'jpg', 'jpeg', 'png');
$random_hash = "";
if ($_FILES['upload_file']['name'] != "") {
$file_name_val = $_FILES["upload_file"]["name"];
        $file_name_ext = @end(@explode(".", $file_name_val));
        $file_type = $_FILES["upload_file"]["type"];
        $file_size = $_FILES["upload_file"]["size"];
        $tmp_name = $_FILES["upload_file"]["tmp_name"];
        $timeval = time();
        $str_rans = substr($timeval,5,6);
        $file_name = $str_rans.'_'.str_replace(" ","-",$file_name_val);
        if ($file_name != "" && @in_array($file_name_ext, $ext)) {
        if ($file_name != "" && $file_size <= 2097152) {
                
                $file_path =  'feedback_files/' . $file_name;
                @move_uploaded_file($tmp_name, $file_path);
                $random_hash = md5(date('r', time()));
        }}
}
if($email!='')
{
        $to_mail = "divij.sonak@religare.com,kawaljeet@catpl.co.in";
      //  $to_mail = "amit.k@catpl.co.in";
        $cc = "sunit@catpl.co.in";
	$subject = "Feedback Mail";
	$from_email = $email;
	$body1='<style type="text/css">
	<!--
	.style1 {
	color: #000000;
	font-weight: bold;
	}
	-->
	</style>
	<table width="650" border="0" cellspacing="2" cellpadding="2">
	<tr>
	<td height="30" colspan="2" bgcolor="#CCCCCC"><p class="style1">Feedback</p></td>
	</tr>
	<tr>
	<td align="left"><p>Email: </p></td>
	<td>'.$email.'</td>
	</tr>
	<tr>
	<td align="left">Message: </td>
	<td>'.stripslashes(@$message).'</td>
	</tr>
	<tr>
	<td align="left"><p>Page Name: </p></td>
	<td>'.$pageName.'</td>
	</tr>
	</table>
	';
	if ($random_hash != '') {
             $file_to_attach = _SITEURL_CUSTOMER . 'feedback_files/' . $file_name;
            $headers = 'From: '.@$from_email . "\r\n";
            //$headers .= 'Cc: '.$cc.' \r\n';
            //$headers .= 'Bcc: '.$bcc.' \r\n';
            $headers .= "Content-Type: multipart/mixed; boundary=\"PHP-mixed-" . $random_hash . "\"\r\n";
            $attachment = chunk_split(base64_encode(file_get_contents($file_to_attach))); // Set your file path here
            $message = "--PHP-mixed-$random_hash\r\n" . "Content-Type: multipart/alternative; boundary=\"PHP-alt-$random_hash\"\r\n\r\n";
            $message .= "--PHP-alt-$random_hash\r\n" . "Content-Type: text/html; charset=\"iso-8859-1\"\r\n" . "Content-Transfer-Encoding: 7bit\r\n\r\n";
            $message .= $body1;


            $message .="\r\n\r\n--PHP-alt-$random_hash--\r\n\r\n";
            $message .= "--PHP-mixed-$random_hash\r\n" . "Content-Type: application/pdf; name=\"$file_name\"\r\n" . "Content-Transfer-Encoding: base64\r\n" . "Content-Disposition: attachment\r\n\r\n";
            $message .= $attachment;
            //$message .= "/r/n--PHP-mixed-$random_hash--";
        } else {
	$headers  = 'MIME-Version: 1.0' . "\r\n";
	$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
	//$headers .= 'Cc: '.$cc.' \r\n';
        //$headers .= 'Bcc: '.$bcc.' \r\n';
	$headers .= 'From: '.@$from_email . "\r\n";
        $message = $body1;
        }
        
	if ($_FILES['upload_file']['name'] != "" && $random_hash=="")
        {
            echo "<script>alert('Invalid file format, please try again.');</script>";
        }
        else
        {
           if(mail($to_mail, @$subject, $message, $headers)){
			//echo "sent";   
		   }
            echo "<script>alert('We thank you for sending your suggestion in Religare Health Insurance.');</script>";
        }

}}}
?>

<script>
function submitEmailToFeedback()
{    
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	var feedbackEmail=$.trim($("#feedbackEmail").val());       
	var feedbackMessage=$.trim($("#feedbackMessage").val());
	var fileName=$.trim($("#upload_file").val());
	var ext = fileName.substring(fileName.lastIndexOf('.') + 1);
	//var fup = document.getElementById('upload_file');
    //var fileName = fup.value;
	var pname=$.trim($("#pageName").val());             

	if((feedbackEmail=="") || (feedbackEmail=="Email")){
		alert("Please enter your email address");
		$("#feedbackEmail").focus();
		return false;
	} else{
		if(reg.test(feedbackEmail) == false) {
			alert( "Please enter your email address in correct format");
			$("#feedbackEmail").focus();
			return false;
		}
	}
	
	if((feedbackMessage=="") || (feedbackMessage=="Suggestion")){
		alert("Please enter your Suggestion");
		$("#feedbackMessage").focus();
		return false;
	}
	
	if(fileName !="")
	{
	    if(ext == "jpeg" || ext == "png" || ext == "gif" || ext == "JPEG" || ext == "PNG" || ext == "GIF" || ext == "jpg" || ext == "JPG" || ext == "pdf" || ext == "PDF" || ext == "TXT" || ext == "txt" || ext == "doc" || ext == "DOC" || ext == "docx" || ext == "DOCX")
        {
            //return true;
        }
        else
        {
            alert("Please upload valid file format");
            return false;
        } 
	}
	
	$("#feedbackFrm").submit();
	$("#mailMeFeedbackButton").html('<img src="images/loading.gif" class="fl">');               
}
</script>
<style>
.feedback_send_mail {
    bottom: -2px;
    position: fixed;
    right: 0; 
	z-index: 999999;
}

@media only screen and (max-width: 767px) {
.feedback_send_mail img { width:80px; }
	}

a.feedback_send_mail:link:focus, a.feedback_send_mail:visited:focus {outline: none; border:0px;} 
.graytxtBox-full {
    background: #f6f6f6 none repeat scroll 0 0;
    border: 1px solid #dbdbdb;
    border-radius: 5px;
    float: left;
    margin: 0;
    padding: 0 8px;
    width: 90%;
}
.txtfieldFullNormal {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    border: 0 none;
    color: #000000;
    font: 15px/18px "FS Humana",tahoma,Helvetica,sans-serif;
    padding: 7px 0;
    width: 100%;
}
.submit-btn1, .submit-btn1:hover {
    background: #5e9d2d none repeat scroll 0 0;
    border: medium none;
    color: #fff;
    font-size: 18px;
    font-weight: 600;
    margin: 0 3px;
    padding: 10px;
    text-shadow: 1px 1px #6e6e6e;
}
.applicationNoTable {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: #f5f3f3 #f5f3f3 -moz-use-text-color;
    border-image: none;
    border-width: 1px 1px 0;
    float: left;
    margin-top: 15px;
    width: 100%;
}
.applicationNoTable table {
    float: left;
    width: 100%;
	border:none;
}
.applicationNoTable table tr th {
    background: #e4ffc6 none repeat scroll 0 0;
    border-bottom: 1px solid #f5f3f3;
    color: #000000;
    font: bold 16px/22px "FS Humana",tahoma,Helvetica,sans-serif;
    padding: 7px 15px;
    width: 33%;
}
.applicationNoTable table tr td {
    background: #fcfff8 none repeat scroll 0 0;
    border: 1px solid #f5f3f3;
	
    color: #000000;
    font: 16px/22px "FS Humana",tahoma,Helvetica,sans-serif;
    padding: 7px 15px;
}

.close {
	float: right;
	font-size: 30px;
	font-weight: 700;
	line-height: 1;
	color: #fff;
	text-shadow: 0 1px 0 #fff;
	filter: alpha(opacity=20);
	opacity:1
}
.modal-body {
	position: relative;
	padding: 15px;
	color:#444;
	overflow:hidden;
	z-index:999999 !important;
}
.modal-header {
	min-height: 16.43px;
	padding: 15px;
	background:#5e9d2d ;
	color:#FFF;
}
</style>

<a class="feedback_send_mail cboxElement item" data-toggle="modal" data-target="#exampleModal" href="javascript:void(0);"><img border="0" alt="Feedback" src="images/feedback_Btnimg.png"></a>


<!--model box start here-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header" style="padding-bottom:0px;">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel" style="padding-bottom:15px;">Feedback</h4>
      </div>
      <div class="modal-body">
      
      <h5>Please provide us the following information:</h5>
<p>
<div class="applicationNoTable">
<form method="post" action="" id="feedbackFrm" enctype="multipart/form-data">
<table cellspacing="0" cellpadding="0" border="0">
                    
                    <tbody><tr>
                      <td valign="top" align="left">Email*</td>
                      <td valign="top" align="left">
                          <div class="graytxtBox-full">
                               <input type="text" onkeyup="if (/[\s+]/g.test(this.value)) this.value = this.value.replace(/\s{2,}/g, '');if(this.value.length &gt; 50)this.value = this.value.substring(0, 50 );" onblur="if (this.value == '') this.value = this.defaultValue;" onfocus="if (this.value == 'Email') this.value = '';" autocomplete="OFF" placeholder="Email" maxlength="100" class="txtfieldFullNormal" id="feedbackEmail" name="feedbackEmail">
                          </div>
                      </td>
                    </tr>
                    
                    <tr>
                      <td valign="top" align="left">Suggestion*</td>
                      <td valign="top" align="left">
                          <div class="graytxtBox-full">
                               <!--<textarea autocomplete="OFF" onkeyup="if(this.value.length &gt; 500)this.value = this.value.substring(0, 500 );" onblur="if(this.value=='')this.value='Suggestion';" onfocus="if(this.value=='Suggestion')this.value='';" placeholder="Suggestion" class="txtfieldFullNormal" id="feedbackMessage" name="feedbackMessage"></textarea> -->
							   <textarea autocomplete="OFF" placeholder="Suggestion" class="txtfieldFullNormal" id="feedbackMessage" name="feedbackMessage"></textarea>
                          <input type="hidden" readonly="readonly" value="<?php echo $fPageName; ?>" id="pageName" name="pageName">
                          </div>
                          </td>
                    </tr>
                    
                    <tr>
                      <td valign="top" align="left">Upload File</td>
                      <td valign="top" align="left">
                          <div class="graytxtBox-full">
                               <input type="file" title="Upload File" class="txtfieldFullNormal" id="upload_file" name="upload_file">
                          </div>
                          <span class="smgentxt">Allowed file formats are .pdf, .doc, .docx, .txt, .gif, .jpg, .jpeg, .png (Max size 2MB)</span>
                          </td>
                    </tr>
                    
                    <tr>
                      <td valign="top" align="left"></td>
                      <td valign="top" align="left" id="mailMeFeedbackButton">
                           
                          <a href="javascript://" id="mailMeFeedbackButton" onclick="submitEmailToFeedback();" class="submit-btn1">Submit</a>
                      </td>
                    </tr>
                  </tbody></table>
                 </form> 
                  </div>
                
                 
   </p>     
      </div>
      
    </div>
  </div>
</div>
<!--model box end here-->

