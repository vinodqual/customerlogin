function stateChange(e,t,n){
	var selected = $("#state option:selected").text();
	$(".customStyleSelectBox").text(selected);
	$('#ajaxLoading').show(); // display loading image
	$.ajax({type:"POST",url:"get_cities_ui.php",data:"stateId="+e+"&cityId="+t+"&type="+n,success:function(e){$("#cities").html(jQuery.trim(e))},
	complete: function(){
        $('#ajaxLoading').hide();
      }
	})
}
function nonPreferredHospitalStateChange(e,t,n){
	var selected = $("#state option:selected").text();
	$(".customStyleSelectBox").text(selected);
	$('#ajaxLoading').show(); // display loading image
	$.ajax({type:"POST",url:"get_cities_ui.php",data:"stateId="+e+"&cityId="+t+"&type="+n,success:function(e){$("#cities").html(jQuery.trim(e))},
	complete: function(){
        $('#ajaxLoading').hide();
      }
	})
}
function searchHospital(){
	if(document.advance_search_form.state.value==""){alert("Please select state");return false}document.advance_search_form.submit()
	}
	
 function kp_char(e) 
{
	if(window.event) {
		// for IE, e.keyCode or window.event.keyCode can be used
		keynum = e.keyCode; 
	}
	else if(e.which) {
		// netscape
		keynum = e.which; 
	}
	else {
		// no event, so pass through
		return true;
	}
	
	if((keynum > 64 && keynum < 91)){
	}else if((keynum > 96 && keynum <123)){
	}else if(keynum==32){
	}	else if(keynum==8){
	}else if(keynum==46){
	}else{
	 return false;
	}   
 }
 
 function network_doctor_email(){
		var doctorList=	getInboxSelectedValue('doctor'); 
		if(doctorList==""){
			alert("Please select atleast one doctor for mailing");
			$("#cboxClose").click();
			return false;
			}
		$("#senddoctoremail").colorbox({width:"400px", inline:true, href:"#inline_example7"});
}
function network_hospital_email() {
    var hospitalList = getInboxSelectedValue('chkBoxValidate');
    if (hospitalList != "") {
        $("#sendhospitalemail").colorbox({width: "400px", inline: true, href: "#inline_example6"});
    }
    else {
        alert("Please select atleast one hospital for mailing");
        return false;
    }

}

function network_hospital_sms(id){ 
			$("#hospitalId").val(id);
			$("#h_"+id).colorbox({width:"400px", inline:true, href:"#inline_example5"});
			
}
/* function non_preferred_network_hospital_sms(id) {
    $("#hospitalId").val(id);
    $("#h_" + id).colorbox({width: "400px", inline: true, href: "#inline_example5"});
} */
function sendHospitalListEmail(){
		var e=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var t=trim($("#yourname").val());
		var n=trim($("#emailId").val());
		if(t==""||t=="Your Name *"){
			alert("Please enter your name");
			return false;
		}
		if(n==""||n=="Email ID *"){
			alert("Please enter your email address");
			return false;
			}else{
				if(e.test(n)==false){alert("Please enter your email address in correct format");return false}
			}
	var hospitalList=	getInboxSelectedValue('hospital');
        
	if(hospitalList==""){
			alert("Please select atleast one hospital for mailing");
			$("#cboxClose").click();
			return false;
			}
	$("#hospitallistemail").html('<img src="images/loading.gif" class="fl">');
	$.ajax({ 
				type: "POST", 
				url:"hospital_email.php",
				data:"firstname="+t+"&emailaddress="+n+"&hospitalList="+hospitalList,
				success: function(msg){
				$("#hospitallistemail").html('<img src="images/sendemail_large.png" alt="Send" border="0"  style="cursor:pointer" onclick="sendHospitalListEmail()" />');
				alert("Network Hospital details have been sent to your email ID.");
				$("#yourname").val('');
				$("#emailId").val('');
				$("#cboxClose").click();
				}
				});
}


function sendHospitalListSMS1(){  
		var t=trim($("#firstName").val());
		var n=trim($("#mobileNo").val());
		var hospitalId=trim($("#hospitalId").val());
		if(t==""||t=="Your Name *"){
			alert("Please enter your name");
			return false;
		}
		if(n==""||n=="Phone No. *"){
			alert("Please enter your Phone");
			return false;
		}else if(!isCharsInBag1(n, "0123456789")) {
				alert( "Mobile must only contain Numbers");					
				return false;
			}
                         if(n.length<10)
                    {
                        alert( "Mobile Number Not valid.");
                        return false;
                    }
		$("#hospitallistsms1").html('<img src="images/loading.gif" class="fl">');
		$.ajax({ 
				type: "POST", 
				url:"hospital_sms.php",
				data:"firstname="+t+"&mobilephone="+n+"&hospitalId="+hospitalId,
				success: function(msg){
				$("#hospitallistsms1").html('<img src="images/sendviasms.png" alt="Send" border="0"  style="cursor:pointer" onclick="sendHospitalListSMS1()" />');
				alert("Network Hospital detail has been sent to your Mobile.");
				$("#firstName").val('');
				$("#mobileNo").val('');
				$("#cboxClose").click();
				}
				});
}
function sendNonPreferredHospitalListSMS1() {
    
    var t = checkTrim($("#firstName").val());
    var n = checkTrim($("#mobileNo").val());
    var hospitalId = checkTrim($("#hospitalId").val());

    if (t == "" || t == "Your Name*") {
        alert("Please enter your name");
        return false;
    }
    if (n == "" || n == "Phone*") {
        alert("Please enter your Phone");
        return false;
    } else if (!isCharsInBagNetwork(n, "0123456789")) {
        alert("Mobile must only contain Numbers");
        return false;
    }
    $("#hospitallistsms1").html('<img src="cpimages/loading.gif" class="fl">');
    $.ajax({
        type: "POST",
        url: "non_preferred_hospital_sms.php",
        data: "firstname=" + t + "&mobilephone=" + n + "&hospitalId=" + hospitalId,
        success: function(msg) {             
            $("#hospitallistsms1").html('<a class="nextBtn"  onclick="sendNonPreferredHospitalListSMS1();"  href="javascript://">SEND VIA SMS</a>');
            alert("Non Preferred Hospital detail has been sent to your Mobile.");
            $("#firstName").val('');
            $("#mobileNo").val('');
            $("#cboxClose").click();
        }
    });
}
function sendHospitalListSMS2(){  
		var t=trim($("#firstName").val());
		var n=trim($("#mobileNo").val());
		var hospitalId=trim($("#hospitalId").val());
		//var hospitalId=	getInboxSelectedValue('chkBoxValidate');
		if(t==""||t=="Your Name *"){
			//alert("Please enter your name");
			var msg = 'Please enter your name';
			$('#smsDivNameError').show();
			$('#smsDivNameError').html(msg);	
			$("#firstName").focus();
			return false;
		}
		else{
			var msg = '';
			$('#smsDivNameError').hide();
		}
		if(n==""||n=="Phone No. *"){
			//alert("Please enter your Phone");
			var msg = 'Please enter your Phone';
			$('#smsDivMobileError').show();
			$('#smsDivMobileError').html(msg);	
			$("#mobileNo").focus();
			return false;
		}else if(!isCharsInBag1(n, "0123456789")) {
				//alert( "Mobile must only contain Numbers");	
				var msg = 'Mobile must only contain Numbers';
				$('#smsDivMobileError').show();
				$('#smsDivMobileError').html(msg);				
				return false;
			}
			else{
				$('#smsDivMobileError').hide();
			}
					if(n.length<10)
					{
						//alert( "Mobile Number Not valid.");
						var msg = 'Mobile Number Not valid.';
						$('#smsDivMobileError').show();
						$('#smsDivMobileError').html(msg);
						return false;
                    }
					else{
						var msg = '';
						$('#smsDivMobileError').hide();
					}
		$("#smsDivSuccess").html('<img src="images/ajax-loader_12.gif" class="fl">');
		$.ajax({ 
				type: "POST", 
				url:"hospital_sms.php",
				data:"firstname="+t+"&mobilephone="+n+"&hospitalId="+hospitalId,
				success: function(msg){
				$("#hospitallistsms1").html('<img src="images/sendviasms.png" alt="Send" border="0"  style="cursor:pointer" onclick="sendHospitalListSMS1()" />');
				//alert("Network Hospital detail has been sent to your Mobile.");
				var msg = 'Network Hospital detail has been sent to your Mobile.';
				$('#smsDivSuccess').show();
				$('#smsDivSuccess').html(msg);
				$("#firstName").val('');
				$("#mobileNo").val('');
				$("#cboxClose").click();
				}
				});
}
function send_sms_non_preferred_hospital(){  
		var t=trim($("#firstName").val());
		var n=trim($("#mobileNo").val());
		var hospitalId=trim($("#hospitalId").val());
		//var hospitalId=	getInboxSelectedValue('chkBoxValidate');
		if(t==""||t=="Your Name *"){
			//alert("Please enter your name");
			var msg = 'Please enter your name';
			$('#smsDivNameError').show();
			$('#smsDivNameError').html(msg);	
			$("#firstName").focus();
			return false;
		}
		else{
			var msg = '';
			$('#smsDivNameError').hide();
		}
		if(n==""||n=="Phone No. *"){
			//alert("Please enter your Phone");
			var msg = 'Please enter your Phone';
			$('#smsDivMobileError').show();
			$('#smsDivMobileError').html(msg);	
			$("#mobileNo").focus();
			return false;
		}else if(!isCharsInBag1(n, "0123456789")) {
				//alert( "Mobile must only contain Numbers");	
				var msg = 'Mobile must only contain Numbers';
				$('#smsDivMobileError').show();
				$('#smsDivMobileError').html(msg);				
				return false;
			}
			else{
				$('#smsDivMobileError').hide();
			}
					if(n.length<10)
					{
						//alert( "Mobile Number Not valid.");
						var msg = 'Mobile Number Not valid.';
						$('#smsDivMobileError').show();
						$('#smsDivMobileError').html(msg);
						return false;
                    }
					else{
						var msg = '';
						$('#smsDivMobileError').hide();
					}
		$("#smsDivSuccess").html('<img src="images/ajax-loader_12.gif" class="fl">');
		$.ajax({ 
				type: "POST", 
				url:"non_preferred_hospital_sms.php",
				data:"firstname="+t+"&mobilephone="+n+"&hospitalId="+hospitalId,
				success: function(msg){
				$("#hospitallistsms1").html('<img src="images/sendviasms.png" alt="Send" border="0"  style="cursor:pointer" onclick="send_sms_non_preferred_hospital()" />');
				//alert("Network Hospital detail has been sent to your Mobile.");
				var msg = 'Non Preferred Hospital detail has been sent to your Mobile.';
				$('#smsDivSuccess').show();
				$('#smsDivSuccess').html(msg);
				$("#firstName").val('');
				$("#mobileNo").val('');
				$("#cboxClose").click();
				}
				});
}

function getInboxSelectedValue(className) {
  var strID="";
   
  $("."+className).each(function() 
  {  
   if(this.checked==true)
   {  
    if(strID=="")
    {
     strID=this.value
    }
    else
    {
     strID=strID + ',' + this.value ;
    }
   }
  });
  return strID;
 }
function isCharsInBag1(s, bag)
{
    var i;
    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1)
            return false;
    }
    return true;
}
function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
function kp_numeric(e) 
{
	if(window.event) {
		// for IE, e.keyCode or window.event.keyCode can be used
		keynum = e.keyCode; 
	}
	else if(e.which) {
		// netscape
		keynum = e.which; 
	}
	else {
		// no event, so pass through
		return true;
	}
     if ((keynum != 46) && (keynum != 8) && (keynum < 48 || keynum > 57))
         return false;
 
 }
function CheckAll()
{
    var className = 'checkbox_chk';

    var is_chk = $("#checkbox_").attr('checked');
    if (is_chk == "checked")
    {

        $(".checkbox_chk").attr('checked', true);
    }
    else
    {
        $(".checkbox_chk").attr('checked', false);
    }
}
function CheckAllDoctors()
{
    var className = 'checkboxdoc_chk';

    var is_chk = $("#checkboxdoc_").attr('checked');
    if (is_chk == "checked")
    {

        $(".checkboxdoc_chk").attr('checked', true);
    }
    else
    {
        $(".checkboxdoc_chk").attr('checked', false);
    }
}

function sendDoctorListEmail(){
		var e=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var t=trim($("#yourname_doc").val());
		var n=trim($("#emailId_doc").val());
                hospital_id=trim($("#hospital_id").val()); 
		if(t==""||t=="Your Name *"){
			alert("Please enter your name");
			return false;
		}
		if(n==""||n=="Email ID *"){
			alert("Please enter your email address");
			return false;
			}else{
				if(e.test(n)==false){alert("Please enter your email address in correct format");return false}
			}
	var doctorList=	getInboxSelectedValue('doctor');
        
	if(doctorList==""){
			alert("Please select atleast one Doctor for mailing");
			$("#cboxClose").click();
			return false;
			}
	$("#hospitallistemail").html('<img src="images/loading.gif" class="fl">');
	$.ajax({ 
				type: "POST", 
				url:"doctor_email.php",
				data:"firstname="+t+"&emailaddress="+n+"&doctorList="+doctorList+"&hospital_id="+hospital_id,
				success: function(msg){
				$("#hospitallistemail").html('<img src="images/sendemail_large.png" alt="Send" border="0"  style="cursor:pointer" onclick="sendDoctorListEmail()" />');
				alert("Doctors details have been sent to your email ID.");
				$("#yourname_doc").val('');
				$("#emailId_doc").val('');
				$("#cboxClose").click();
				}
				});
}

 function network_hospital_email_retail(){
		var hospitalList=	getInboxSelectedValue('chkBoxValidate'); 
		if(hospitalList==""){
			//alert("Please select atleast one hospital for mailing");
			var msg = 'Please select atleast one hospital for mailing';
			$('#divID').show();
			 $('#divID').html(msg);
			return false;
			}
			else{
				msg = '';
				$('#divID').hide();
			}
			
		$('#exampleModal1').modal('show');
}
 function non_preferred_network_hospital_email(){
		var hospitalList=	getInboxSelectedValue('chkBoxValidate'); 
		if(hospitalList==""){
			//alert("Please select atleast one hospital for mailing");
			var msg = 'Please select atleast one hospital for mailing';
			$('#divID').show();
			 $('#divID').html(msg);
			return false;
			}
			else{
				msg = '';
				$('#divID').hide();
			}
			
		$('#exampleModal1').modal('show');
}
 function network_hospital_sms_retail(value){
		document.getElementById("hospitalId").value = value;
		$('#exampleModal2').modal('show');
		
}
 function non_preferred_network_hospital_sms(value){
		document.getElementById("hospitalId").value = value;
		$('#exampleModal2').modal('show');
		
}
function sendHospitalListEmail2(){
	//$("#emailDivSuccess").html('<img src="images/loading-gif2.gif" class="fl">');
		var e=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var t=trim($("#yourname").val());
		var n=trim($("#emailId").val());
		if(t==""||t=="Your Name *"){
			//alert("Please enter your name");
			var msg = 'Please enter your name';
			$('#emailDivNameError').show();
			 $('#emailDivNameError').html(msg);
			return false;
		}
		else{
			var msg = '';
			$('#emailDivNameError').hide();
		}
		if(n==""||n=="Your Email Id *"){
			//alert("Please enter your email address");
			var msg = 'Please enter your email address';
			$('#emailDivEmailidError').show();
			 $('#emailDivEmailidError').html(msg);
			return false;
			}else{
				if(e.test(n)==false){
				//alert("Please enter your email address in correct format");
				var msg = 'Please enter correct email address';
				$('#emailDivEmailidError').show();
				$('#emailDivEmailidError').html(msg);
				return false
				}
				else{
					msg = '';
					$('#emailDivEmailidError').hide();
				}
			}
	var hospitalList=	getInboxSelectedValue('chkBoxValidate');
        
	if(hospitalList==""){
			//alert("Please select atleast one hospital for mailing");
			var msg = 'Please select atleast one hospital for mailing';
			$('#emailDivEmailidError').show();
			 $('#emailDivEmailidError').html(msg);
			$("#cboxClose").click();
			return false;
			}
			else{
				var msg = '';
			$('#emailDivEmailidError').hide();
			}
	//$("#hospitallistemail").html('<img src="images/loading.gif" class="fl">');
	$("#emailDivSuccess").html('<img src="images/ajax-loader_12.gif" class="fl">');
	$.ajax({ 
		type: "POST", 
		url:"hospital_email.php",
		data:"firstname="+t+"&emailaddress="+n+"&hospitalList="+hospitalList,
		success: function(msg){
		$("#hospitallistemail").html('<img src="images/sendemail_large.png" alt="Send" border="0"  style="cursor:pointer" onclick="sendHospitalListEmail2()" />');
		//alert("Network Hospital details have been sent to your email ID.");
		var msg = 'Network Hospital details have been sent to your email ID.';
		$('#emailDivSuccess').show();
		$('#emailDivSuccess').html(msg);
		
		$("#yourname").val('');
		$("#emailId").val('');
		$("#cboxClose").click();
		}
		});
}
function send_email_non_preferred_hospital(){
	//$("#emailDivSuccess").html('<img src="images/loading-gif2.gif" class="fl">');
		var e=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var t=trim($("#yourname").val());
		var n=trim($("#emailId").val());
		if(t==""||t=="Your Name *"){
			//alert("Please enter your name");
			var msg = 'Please enter your name';
			$('#emailDivNameError').show();
			 $('#emailDivNameError').html(msg);
			return false;
		}
		else{
			var msg = '';
			$('#emailDivNameError').hide();
		}
		if(n==""||n=="Your Email Id *"){
			//alert("Please enter your email address");
			var msg = 'Please enter your email address';
			$('#emailDivEmailidError').show();
			 $('#emailDivEmailidError').html(msg);
			return false;
			}else{
				if(e.test(n)==false){
				//alert("Please enter your email address in correct format");
				var msg = 'Please enter correct email address';
				$('#emailDivEmailidError').show();
				$('#emailDivEmailidError').html(msg);
				return false
				}
				else{
					msg = '';
					$('#emailDivEmailidError').hide();
				}
			}
	var hospitalList=	getInboxSelectedValue('chkBoxValidate');
        
	if(hospitalList==""){
			//alert("Please select atleast one hospital for mailing");
			var msg = 'Please select atleast one hospital for mailing';
			$('#emailDivEmailidError').show();
			 $('#emailDivEmailidError').html(msg);
			$("#cboxClose").click();
			return false;
			}
			else{
				var msg = '';
			$('#emailDivEmailidError').hide();
			}
	//$("#hospitallistemail").html('<img src="images/loading.gif" class="fl">');
	$("#emailDivSuccess").html('<img src="images/ajax-loader_12.gif" class="fl">');
	$.ajax({ 
		type: "POST", 
		url:"non_preferred_hospital_email.php",
		data:"firstname="+t+"&emailaddress="+n+"&hospitalList="+hospitalList,
		success: function(msg){
		$("#hospitallistemail").html('<img src="images/sendemail_large.png" alt="Send" border="0"  style="cursor:pointer" onclick="send_email_non_preferred_hospital()" />');
		//alert("Network Hospital details have been sent to your email ID.");
		var msg = 'Non Preferred Hospital details have been sent to your email ID.';
		$('#emailDivSuccess').show();
		$('#emailDivSuccess').html(msg);
		
		$("#yourname").val('');
		$("#emailId").val('');
		$("#cboxClose").click();
		}
		});
}
function sendDownloadCenterEmail(){
		var e=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var t=$.trim($("#yourname").val());
		var n=$.trim($("#emailId").val());
        var m=$.trim($("#downloademailid").val());
		var o=$.trim($("#pagename").val());
		if(t==""||t=="Your Name"){
			//alert("Please enter your name");
              var msg = 'Please enter your name';
			$('#emailDivNameError').show();
			 $('#emailDivNameError').html(msg);         
			return false;
		}
		else{
			var msg = '';
			$('#emailDivNameError').hide();
		}
		
		if(n==""||n=="Your Email Id *"){
			//alert("Please enter your email address");
			var msg = 'Please enter your email address';
			$('#emailDivEmailidError').show();
			 $('#emailDivEmailidError').html(msg);
			return false;
			}else{
				if(e.test(n)==false){
				//alert("Please enter your email address in correct format");
				var msg = 'Please enter correct email address';
				$('#emailDivEmailidError').show();
				$('#emailDivEmailidError').html(msg);
				return false
				}
				else{
					msg = '';
					$('#emailDivEmailidError').hide();
				}
			}
	
	$("#emailDivSuccess").html('<img src="images/ajax-loader_12.gif" class="fl">');
	$.ajax({ 
		type: "POST", 
		url:"download_center_email.php",
		data:"firstname="+t+"&emailaddress="+n+"&downloademailid="+m+"&pagename="+o,
		success: function(msg){
		//alert("Network Hospital details have been sent to your email ID.");
		var msg = 'Mail sent to your email ID.';
		$('#emailDivSuccess').show();
		$('#emailDivSuccess').html(msg);
		
		$("#yourname").val('');
		$("#emailId").val('');
		$("#cboxClose").click();
		}
		});
}