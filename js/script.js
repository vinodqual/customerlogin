var ww = document.body.clientWidth;

$(document).ready(function () {
	
    $(".nav1 li a").each(function () {
        if ($(this).next().length > 0) {
            $(this).addClass("parent");
        };
    })
    $(".navbot li a").each(function () {
        if ($(this).next().length > 0) {
            $(this).addClass("parent");
        }
        ;
    })
    $(".toggleMenu").click(function (e) {
        e.preventDefault();
        $(this).toggleClass("active");
        $(".nav1").toggle();
    });
    adjustMenu();
})
$(window).bind('resize orientationchange', function () {
    ww = document.body.clientWidth;
    adjustMenu();
});
var adjustMenu = function () {
    if (ww < 1000) {
        $(".toggleMenu").css("display", "inline-block");
        if (!$(".toggleMenu").hasClass("active")) {
            $(".nav1").hide();
        } else {
            $(".nav1").show();
        }
        $(".nav1 li").unbind('mouseenter mouseleave');
        $(".nav1 li a.parent").unbind('click').bind('click', function (e) {
            e.preventDefault();
            $(this).parent("li").toggleClass("hover");
        });
        $(".navbot li").unbind('mouseenter mouseleave');
        $(".navbot li a.parent").unbind('click').bind('click', function (e) {
            e.preventDefault();
            $(this).parent("li").toggleClass("hover");
        });
    }
    else if (ww >= 1000) {
        $(".toggleMenu").css("display", "none");
        $(".nav1").hide();
        $(".nav1 li").removeClass("hover");
        $(".nav1 li a").unbind('click');
        $(".nav1 li").unbind('mouseenter mouseleave').bind('mouseenter mouseleave', function () {
            $(this).toggleClass('hover');
        });
    }
}

$(document).ready(function () {
    $(".darrow").click(function () {
        $(".darrow").hide();
        $(".uparrow").css('display', 'inline-block');
        $(".quickNavMiddle").show();
    });
    $(".uparrow").click(function () {
        $(".darrow").show();
        $(".uparrow").hide();
        $(".darrow").css('display', 'inline-block');
        $(".quickNavMiddle").hide();
    });
});
$("#hIns").hover(function () {
    $("#hInsp").show();
    $("#hIns").hide();
}, function () {
    $("#hInsp").hide();
    $("#hIns").show();
});
$("#hInsp").hover(function () {
    $("#hInsp").show();
    $("#hIns").hide();
}, function () {
    $("#hInsp").hide();
    $("#hIns").show();
});
$("#tIns").hover(function () {
    $("#tInsp").show();
    $("#tIns").hide();
}, function () {
    $("#tInsp").hide();
    $("#hIns").show();
});
$("#tInsp").hover(function () {
    $("#tInsp").show();
    $("#tIns").hide();
}, function () {
    $("#tInsp").hide();
    $("#tIns").show();
});


// create the back to top button
$('body').prepend('<a href="#" class="back-to-top">Back to Top</a>');

// change the value with how many pixels scrolled down the button will appear
var amountScrolled = 200;

$(window).scroll(function () {
    if ($(window).scrollTop() > amountScrolled) {
        $('a.back-to-top').fadeIn('slow');
    } else {
        $('a.back-to-top').fadeOut('slow');
    }
});

$('a.back-to-top, a.simple-back-to-top').click(function () {
    $('body, html').animate({
        scrollTop: 0
    }, 'slow');
    return false;
});


