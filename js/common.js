function file_get_ext(filename)
    {
    return typeof filename != "undefined" ? filename.substring(filename.lastIndexOf(".")+1, filename.length).toLowerCase() : false;
    }

function checkFileFormate(file_ext) 
{
	
		if(file_ext!='xls' && file_ext!='doc' && file_ext!='jpg' && file_ext!='pdf' && file_ext!='xlsx' && file_ext!='jpeg') 
		{  
			return false;
		}
		else{
			
			return true;
		}
}

//Disable right mouse click Script
//For full source code, visit http://www.dynamicdrive.com

var message="Right click Disabled!";

///////////////////////////////////
function clickIE4(){
if (event.button==2){
alert(message);
return false;
}
}

function clickNS4(e){
if (document.layers||document.getElementById&&!document.all){
if (e.which==2||e.which==3){
alert(message);
return false;
}
}
}

if (document.layers){
document.captureEvents(Event.MOUSEDOWN);
document.onmousedown=clickNS4;
}
else if (document.all&&!document.getElementById){
document.onmousedown=clickIE4;
}

document.oncontextmenu=new Function("alert(message);return false")

// --> 


 function kp_char(e) 
{
	if(window.event) {
		// for IE, e.keyCode or window.event.keyCode can be used
		keynum = e.keyCode; 
	}
	else if(e.which) {
		// netscape
		keynum = e.which; 
	}
	else {
		// no event, so pass through
		return true;
	}
	
	if((keynum > 64 && keynum < 91)){
	}else if((keynum > 96 && keynum <123)){
	}else if(keynum==32){
	}	else if(keynum==8){
	}else if(keynum==46){
	}else{
	 return false;
	}   
 }
function kp_numeric(e) 
{
	if(window.event) {
		// for IE, e.keyCode or window.event.keyCode can be used
		keynum = e.keyCode; 
	}
	else if(e.which) {
		// netscape
		keynum = e.which; 
	}
	else {
		// no event, so pass through
		return true;
	}
     if ((keynum != 46) && (keynum != 8) && (keynum < 48 || keynum > 57))
         return false;
 
 }
function kp_alphanumeric(e) 
{
	if(window.event) {
		// for IE, e.keyCode or window.event.keyCode can be used
		keynum = e.keyCode; 
	}
	else if(e.which) {
		// netscape
		keynum = e.which; 
	}
	else {
		// no event, so pass through
		return true;
	}
    if((keynum > 64 && keynum < 91)){
	}else if((keynum > 96 && keynum <123)){
	}else if(keynum==32){
	}	else if(keynum==8){
	}else if(keynum==46){
	}else if(keynum>48 &&  keynum < 58 ){
	}else{
	 return false;
	}    
 }
function trim(stringToTrim) {
	return stringToTrim.replace(/^\s+|\s+$/g,"");
}
// JavaScript Document
	var whitespace = " \t\n\r";
// Function to check whether the value in a Text Field is Null
	function isEmpty(s)
	{  return ((s == null) || (s.length == 0))
	}
// Function to check whether the value in a Text Field is a WhiteSpace
	function isWhitespace (s)
	{  var i;
 	  // Is s empty?
 	  if (isEmpty(s)) return true;
 	   // Search through string's characters one by one
 	   // until we find a non-whitespace character.
 	   // When we do, return false; if we don't, return true.
 	   for (i = 0; i < s.length; i++)
 	   {   
 	  // Check that current character isn't whitespace.
 	  var c = s.charAt(i);
 	  if (whitespace.indexOf(c) == -1) return false;
 	   }
 	   // All characters are whitespace.
	  return true;
	}
// Function to ensure that the email address is in proper format
	function isEmail (s)
  	{
    var i = 1;
    var sLength = s.length;
    // look for @
    while ((i < sLength) && (s.charAt(i) != "@"))
    { i++
    }
    if ((i >= sLength) || (s.charAt(i) != "@")) return false;
    else i += 2;
    // look for .
    while ((i < sLength) && (s.charAt(i) != "."))
    { i++
    }
	  // there must be at least one character after the .
    if ((i >= sLength - 1) || (s.charAt(i) != ".")) return false;
    else return true;
    }
 function kp_alphanumeric(e) 
{
	if(window.event) {
		// for IE, e.keyCode or window.event.keyCode can be used
		keynum = e.keyCode; 
	}
	else if(e.which) {
		// netscape
		keynum = e.which; 
	}
	else {
		// no event, so pass through
		return true;
	}
	if((keynum > 64 && keynum < 91)){
	}else if((keynum > 96 && keynum <123)){
	}else if((keynum > 47 && keynum <58)){
	}else if(keynum==46){
	}else{
	 return false;
	}   
 }
function numberOfSelectedCheckbox(className){
	var selector_checked = $("input[class="+className+"]:checked").length;
	return selector_checked;
}
 // start validation for retail login form
 
function  validateLogin(){
	var username=$.trim($("#username").val());
	var password=$.trim($("#password").val());
	var csid = document.getElementById("userType").value;
	if (csid == ""){
		alert("\nPlease Select your user type.");
		return false;
	}
	if (username == "" || username== "Username*")
	{
		alert("\nPlease enter Username.")
		$("#username").focus();
		return false;
	}
	if ((password == "")||(password.length < 4) || password == "Password *")
	{
		alert("\nThe PASSWORD field is either empty or less than 4 chars.\n\nPlease re-enter your Password.")
		$("#fake_Password").focus();
		return false;
	}
	 var encoded_val=base64_encode(password);
	 $('#password').val(encoded_val);
	return true;
}

function validateEnquiry(){
	var employeeName=$.trim($("#employeeName").val());
	var emailid=$.trim($("#emailid").val());
	var phone=$.trim($("#phone").val());
	var policyNumber=$.trim($("#policyNumber").val());
	var enquiry=$.trim($("#enquiry").val());
	intRegex = /[0-9 -()+]+$/;
	
		
	 if(employeeName=='' || employeeName=='Employee Name *')
		{ 
		 alert("\nPlease enter Employee Name.");
		 $("#employeeName").focus();
		 return false;
		}
	 if(phone=='' || phone=='Phone')
		{ 
		 alert("\nPlease enter Phone.");
		 $("#phone").focus();
		 return false;
		}
	 if(emailid==''  || emailid=='Email Id')
		{ 
		 alert("\nPlease enter Email Id.");
		 $("#emailid").focus();
		 return false;
		}
	 if(emailid !='')
		{
			if(!validateEmail(emailid))
			{
			alert('Please enter a valid Email address');
			 $("#emailid").focus();
			return false;	
			}
		}		
	 if(policyNumber=='' || policyNumber=='Policy Number')
		{ 
		 alert("\nPlease enter Policy Number.");
		 $("#policyNumber").focus();
		 return false;
		}
	 if(enquiry=='' || enquiry=='Enquiry')
		{ 
		 alert("\nPlease enter Enquiry.");
		 $("#enquiry").focus();
		 return false;
		}
				$.ajax({ 
				type: "POST", 
				url: "ajax/saveenquiryform.php",
				data: {'employeeName':employeeName,'phone':phone,'emailid':emailid,'policyNumber':policyNumber,'enquiry':enquiry}, 
				beforeSend:function(){
				},
				success: function(msg){
					if(msg == 1){
						$(".successMsg").show();
						setTimeout(function() {
							  // Do something after 2 seconds
							  $(".usertoggle .fa-fw").click();
						}, 3000);
					} else {
						$(".successMsg").html("Some Error. Please Try Again");
						$(".successMsg").show();
						setTimeout(function() {
							  // Do something after 2 seconds
							  $(".usertoggle .fa-fw").click();
						}, 3000);
						
					}
					}
				});	
				return false;
	 
}
////end of functions///////////////////
function validate(frm){
		var employeeId = frm.employeeId.value;
		var customerId = frm.customerId.value;
		var policyNumber = trim(frm.policyNumber.value);
		
		if((employeeId == "") && (customerId == "")){
			alert("Either Employee Id / Customer Id is required");
				return false;
		}
		if(policyNumber.length<4){
				alert("Please enter correct Policy Number");
				return false;			
		}
	}

function  validateAccount()
{
	var name=trim(document.createAccount.name.value);
	if (name == "")
	{
		alert("\nPlease enter Name.")
		document.createAccount.name.focus();
		return false;
	}
	var mobile=trim(document.createAccount.mobile.value);
	if (mobile == "")
	{
		alert("\nPlease enter Mobile.")
		document.createAccount.mobile.focus();
		return false;
	}
	if(!isCharsInBag(mobile, "0123456789")){
					alert("\nPlease enter Correct Mobile.")
					document.createAccount.mobile.focus();
		return false;
			}
	var username=trim(document.createAccount.email.value);
	if (username == "")
	{
		alert("\nPlease enter Email.")
		document.createAccount.email.focus();
		return false;
	}else{
			if(!isEmail(username)){
					alert("\nPlease enter Email in correct format.")
					document.createAccount.email.focus();
		return false;
			}
	}
	var password = trim(document.createAccount.password.value);
	if ((password == "")||(password.length < 4))
	{
		alert("\nThe PASSWORD field is either empty or less than 4 chars.\n\nPlease re-enter your Password.")
		document.createAccount.password.focus();
		return false;
	}
	 var encoded_val=base64_encode(password);
	 $('#password').val(encoded_val);
	return true;
}
function validateWebenrol ()
{
	var companyName=trim(document.webenroll.companyName.value);
	if (companyName == "")
	{
		alert("\nPlease enter Company Name.")
		document.webenroll.companyName.focus();
		return false;
	}
	var empEmail=trim(document.createAccount.empEmail.value);
	if (empEmail!= "")
	{
		if(!isEmail(empEmail)){
				alert("\nPlease enter Email.")
				document.createAccount.mobile.focus();
				return false;
			}
	}
	var dateOfBirth=trim(document.webenroll.dateOfBirth.value);
	if (dateOfBirth == "")
	{
		alert("\nPlease select correct Date Of Birth.")
		document.webenroll.dateOfBirth.focus();
		return false;
	}
	return true;
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_validateForm() { //v4.0
  var i,p,q,nm,test,num,min,max,errors='',args=MM_validateForm.arguments;
  for (i=0; i<(args.length-2); i+=3) { test=args[i+2]; val=MM_findObj(args[i]);
    if (val) { nm=val.name; if ((val=val.value)!="") {
      if (test.indexOf('isEmail')!=-1) { p=val.indexOf('@');
        if (p<1 || p==(val.length-1)) errors+='- '+nm+' must contain an e-mail address.\n';
      } else if (test!='R') { num = parseFloat(val);
        if (isNaN(val)) errors+='- '+nm+' must contain a number.\n';
        if (test.indexOf('inRange') != -1) { p=test.indexOf(':');
          min=test.substring(8,p); max=test.substring(p+1);
          if (num<min || max<num) errors+='- '+nm+' must contain a number between '+min+' and '+max+'.\n';
    } } } else if (test.charAt(0) == 'R') errors += '- '+nm+' is required.\n'; }
  } if (errors) alert('The following error(s) occurred:\n'+errors);
  document.MM_returnValue = (errors == '');
}
function sendForgotpassword(){
MM_validateForm('emailId','','RisEmail');
	var str1=document.MM_returnValue;
	if(str1==false){
	}else{
			var emailId=$("#emailId").val();
				$.ajax({ 
				type: "POST", 
				url: "ajax/send_forgot_pwd.php",
				data: "emailId="+emailId, 
				beforeSend:function(){
/*				$(".input_container").hide();
				$(".submit_container_0").hide();
*/					
					},
				success: function(msg){
					if(msg == "<b>Password sent in your email id</b>"){
						$(".input_container").hide();
						$(".submit_container_0").hide();
						$("#forgot_pwd_msg").html(msg);
					} else {
						$("#forgot_pwd_msg").html(msg);
					}
					}
				});	
	}
}
function validateChangePassword(){
	var oldPassword=trim(document.changePwdForm.oldPassword.value);
	if (oldPassword == "")
	{
		alert("\nPlease enter Old password")
		document.changePwdForm.oldPassword.focus();
		return false;
	}
	var password=trim(document.changePwdForm.password.value);
	if (password == "")
	{
		alert("\nPlease enter New password")
		document.changePwdForm.password.focus();
		return false;
	}
	if(checkStrength(password) != 'Strong'){
		alert('Password should be atleast 8 characters long with at least one special character and one number');
		document.changePwdForm.password.focus();
		  return false;
	}
	if (password.length<8){
		alert("\nPassword should have minimum 8 chars.")
		document.changePwdForm.password.focus();
		return false;
	}
	
	var repassword=trim(document.changePwdForm.repassword.value);
	if (repassword == "")
	{
		alert("\nPlease enter Re-Type new password")
		document.changePwdForm.repassword.focus();
		return false;
	}
	if(password!=repassword){
		alert("\n New and Re-type Password Mismatch")
		document.changePwdForm.repassword.focus();
		return false;
	}
	 var encoded_val=base64_encode(oldPassword);
	 $('#oldPassword').val(encoded_val);
	 var encoded_valN=base64_encode(password);
	 $('#newpassword').val(encoded_valN);
	 var encoded_valR=base64_encode(repassword);
	 $('#repassword').val(encoded_valR);
}
function validateReimbursementDoc()
{
var docTitle1=document.docForm.docTitle1.value;	
var upload1=document.docForm.upload1.value;
var docTitle2=document.docForm.docTitle2.value;	
var upload2=document.docForm.upload2.value;	
var docTitle3=document.docForm.docTitle3.value;	
var upload3=document.docForm.upload3.value;
var docTitle4=document.docForm.docTitle4.value;
var upload4=document.docForm.upload4.value;
var docTitle5=document.docForm.docTitle5.value;
var upload5=document.docForm.upload5.value;
	
var docTitle=document.docForm.docTitle1.value;

	if (docTitle == "")
	{
		alert("\nPlease enter Doc Name.")
		document.docForm.docTitle1.focus();
		return false;
	}
	if (docTitle1!= "")
	{
		if (upload1== "")
		{
		alert("\nPlease browse File for File 1 ")
		document.docForm.docTitle1.focus();
		return false;
		}
	}
	if (docTitle2!= "")
	{
		if (upload2== "")
		{
		alert("\nPlease browse File for File 2 ")
		document.docForm.docTitle2.focus();
		return false;
		}
	}
	if (docTitle3!= "")
	{
		if (upload3== "")
		{
		alert("\nPlease browse File for File 3")
		document.docForm.docTitle3.focus();
		return false;
		}
	}
	if (docTitle4!= "")
	{
		if (upload4== "")
		{
		alert("\nPlease browse File for File 4")
		document.docForm.docTitle4.focus();
		return false;
		}
	}
	if (docTitle5!= "")
	{
		if (upload5== "")
		{
		alert("\nPlease browse File for File 5")
		document.docForm.docTitle5.focus();
		return false;
		}
	}

if (upload1!= "")
	{
		if (docTitle1== "")
		{
			alert("\nPlease Enter Doc Title 1")
			document.docForm.docTitle1.focus();
			return false;
		}
		
		if(checkFileFormate(file_get_ext(upload1))== false) {
			
			alert("Please Upload correct format file in Doc file 1 ");
			return false;
		}
		
	}
	if (upload2!= "")
	{
		if (docTitle2== "")
		{
			alert("\nPlease Enter Doc Title 2 ")
			document.docForm.docTitle2.focus();
			return false;
		}
		if(checkFileFormate(file_get_ext(upload2))== false) {
			alert("Please Upload correct format file in Doc file 2 ");
			return false;
		}
	}
	if (upload3!= "")
	{
		if (docTitle3== "")
		{
		alert("\nPlease Enter Doc Title 3 ")
		document.docForm.docTitle3.focus();
		return false;
		}
		if(checkFileFormate(file_get_ext(upload3))== false) {
		alert("Please Upload correct format file in Doc file 3 ");
		return false;
		}
	}
	if (upload4!= "")
	{
		if (docTitle4== "")
		{
		alert("\nPlease Enter Doc Title 4 ")
		document.docForm.docTitle4.focus();
		return false;
		}
		if(checkFileFormate(file_get_ext(upload4))== false) {
		alert("Please Upload correct format file in Doc file 4 ");
		return false;
		}
	}
	if (upload5!= "")
	{
		if (docTitle5== "")
		{
		alert("\nPlease Enter Doc Title 5 ")
		document.docForm.docTitle5.focus();
		return false;
		}
		if(checkFileFormate(file_get_ext(upload5))== false) {
		alert("Please Upload correct format file in Doc file 5 ");
		return false;
		}
	}
}
function  validateReimbursementRequest()
{
	var txtEmail=document.DocRequestForm.txtEmail.value;
	if (txtEmail == "" || txtEmail == "Enter Email"){
		alert("\nPlease enter Email.")
		document.DocRequestForm.txtEmail.focus();
		return false;
	}
	if(!isEmail(txtEmail)){
		alert("\nPlease enter Email in correct format.")
		document.DocRequestForm.txtEmail.focus();
		return false;
	}
	var phone=document.DocRequestForm.txtPhone.value;
	if (phone == "" || phone == "Enter Contact No")
	{
		alert("\nPlease enter Contact Number.")
		document.DocRequestForm.txtPhone.focus();
		return false;
	}
	if(phone.length<10){
			alert("Contact Number should be 10 digits");
			document.DocRequestForm.txtPhone.focus();
			return false;
		}
	var txtEnquiry=document.DocRequestForm.txtEnquiry.value;
	
	if (txtEnquiry.length=='' || txtEnquiry == "Enter Request")	{
		alert("\nPlease enter Reason for request");
		document.DocRequestForm.txtEnquiry.focus();
		return false;
	}
	var docTitle=document.DocRequestForm.docTitle1.value;
	if (docTitle == "" || docTitle == "Enter Title")
	{
		alert("\nPlease enter Doc Title 1")
		document.DocRequestForm.docTitle1.focus();
		return false;
	}
	
var docTitle1=document.DocRequestForm.docTitle1.value;	
var upload1=document.DocRequestForm.upload1.value;
var docTitle2=document.DocRequestForm.docTitle2.value;	
var upload2=document.DocRequestForm.upload2.value;	
var docTitle3=document.DocRequestForm.docTitle3.value;	
var upload3=document.DocRequestForm.upload3.value;
var docTitle4=document.DocRequestForm.docTitle4.value;
var upload4=document.DocRequestForm.upload4.value;
var docTitle5=document.DocRequestForm.docTitle5.value;
var upload5=document.DocRequestForm.upload5.value;

	if (docTitle1!= "" && docTitle1!= "Enter Title")
	{
		if (upload1== "")
		{
		alert("\nPlease browse File for File 1 ")
		document.DocRequestForm.docTitle1.focus();
		return false;
		}
	}
	if (docTitle2!= ""  && docTitle2!= "Enter Title")
	{
		if (upload2== "")
		{
			alert("\nPlease browse File for File 2 ")
			document.DocRequestForm.docTitle2.focus();
			return false;
		}
	}
	if (docTitle3!= "" && docTitle3!= "Enter Title")
	{
		if (upload3== "")
		{
			alert("\nPlease browse File for File 3")
			document.DocRequestForm.docTitle3.focus();
			return false;
		}
	}
	if (docTitle4!= "" && docTitle4!= "Enter Title")
	{
		if (upload4== "")
		{
		alert("\nPlease browse File for File 4")
		document.DocRequestForm.docTitle4.focus();
		return false;
		}
	}
	if (docTitle5!= "" && docTitle5!= "Enter Title")
	{
		if (upload5== "")
		{
		alert("\nPlease browse File for File 5")
		document.DocRequestForm.docTitle5.focus();
		return false;
		}
	}
	
if (upload1!= "")
	{
		if (docTitle1== "")
		{
		alert("\nPlease Enter Doc Title 1")
		document.DocRequestForm.docTitle1.focus();
		return false;
		}
		//alert(file_get_ext(upload1));
		if(checkFileFormate(file_get_ext(upload1))== false) {
		alert("Please Upload correct format file in Doc file 1 ");
		return false;
		}
		
	}
	if (upload2!= "")
	{
		if (docTitle2== ""  || docTitle2 == "Enter Title")
		{
		alert("\nPlease Enter Doc Title 2 ")
		document.DocRequestForm.docTitle2.focus();
		return false;
		}
		if(checkFileFormate(file_get_ext(upload2))== false) {
		alert("Please Upload correct format file in Doc file 2 ");
		return false;
		}
	}
	if (upload3!= "")
	{
		if (docTitle3== ""  || docTitle3 == "Enter Title")
		{
		alert("\nPlease Enter Doc Title 3 ")
		document.DocRequestForm.docTitle3.focus();
		return false;
		}
		if(checkFileFormate(file_get_ext(upload3))== false) {
		alert("Please Upload correct format file in Doc file 3 ");
		return false;
		}
	}
	if (upload4!= "")
	{
		if (docTitle4== ""  || docTitle4 == "Enter Title")
		{
		alert("\nPlease Enter Doc Title 4 ")
		document.DocRequestForm.docTitle4.focus();
		return false;
		}
		if(checkFileFormate(file_get_ext(upload4))== false) {
		alert("Please Upload correct format file in Doc file 4 ");
		return false;
		}
	}
	if (upload5!="")
	{
		if (docTitle5== ""  || docTitle5 == "Enter Title")
		{
		alert("\nPlease Enter Doc Title 5 ")
		document.DocRequestForm.docTitle5.focus();
		return false;
		}
		if(checkFileFormate(file_get_ext(upload5))== false) {
		alert("Please Upload correct format file in Doc file 5 ");
		return false;
		}
	} 
	//alert(555);
	$("#DocRequestForm").submit();
}
function  validateFirstTimeUser()
{
	
	var policyNumber=$.trim($("#policyNumber").val());
	var empname=$.trim($("#empname").val());
	var empid=$.trim($("#empid").val());
	var Email_values=$.trim($("#email").val());
	var Password=$.trim($("#Password").val());
	var CustomerId=$.trim($("#CustomerId").val());
	var Password=$.trim($("#Password").val());
	var CPassword=$.trim($("#CPassword").val());
	var Numbers=$.trim($("#Numbers").val());
	var code=$.trim($("#code").val());
	intRegex = /[0-9 -()+]+$/;

	
if(policyNumber=='' || policyNumber=='Policy No. *')
		{ 
		 alert("\nPlease enter Policy Number.");
		 $("#policyNumber").focus();
		 return false;
		}
		
		 if(empname=='' || empname=='Employee Name *')
		{ 
		 alert("\nPlease enter Employee Name.");
		 $("#empname").focus();
		 return false;
		}
		if(empid=='' || empid=='Employee ID *')
		{ 
		 alert("\nPlease enter Employee Id.");
		 $("#empid").focus();
		 return false;
		}
		if(empid != ""){
			var addedEmp = zeroPad(empid,10);
			//alert(addedEmp);
		 $("#empid").val(addedEmp);
		}
		
	 if(Email_values==''  || Email_values=='Email *')
		{ 
		 alert("\nPlease enter Email Id.");
		 $("#email").focus();
		 return false;
		}
	 if(Email_values !='')
		{
			if(!validateEmail(Email_values))
			{
			alert('Please enter a valid Email address');
			 $("#email").focus();
			return false;	
			}
		}		
	 
	 /*if(CustomerId==''  || CustomerId=='Corporate Id *')
		{ 
		 alert("\nPlease enter Customer Id.");
		 $("#CustomerId").focus();
		 return false;
		}*/
	
	if(Numbers!='' && Numbers!='Phone'){
	
	if((!intRegex.test(Numbers)))
						{
							 alert("\Mobile Number Should be Numeric.");
		 $("#Numbers").focus();
							return false;
						}	
						if(Numbers.length < 10 || Numbers.length >12)
						{
							 alert("\Mobile Number Not Valid.");
						 $("#Numbers").focus();
							return false;
						}
	}
	 if(Password==''  || Password=='Password *')
		{ 
		 alert("\nPlease enter Password.");
		 $("#fake_Password").focus();
		 return false;
		}
	if(checkStrength(Password) != 'Strong'){
		alert('Password should be atleast 8 characters long with at least one special character and one number');
		 $("#fake_Password").focus();
		  return false;
	}
	 if(CPassword==''  || CPassword=='Confirm Password *')
		{ 
		 alert("\nPlease enter Confirm Password .");
		 $("#fake_CPassword").focus();
		 return false;
		}
	 if(Password != CPassword)
		{ 
		 alert("\nPassword Not Matched.");
		 $("#fake_CPassword").focus();
		 return false;
		}
	
	 if(code == '')
		{ 
		 alert("\nCaptcha Not Validate.");
		 $("#code").focus();
		 return false;
		}
	
}
function checkStrength(password){
 
 if(password == 'Password *'){
	 password = '';
 }
    //initial strength
    var strength = 0
 
    //if the password length is less than 6, return message.
    if (password.length < 8) {
        $('#result').removeClass()
        $('#result').addClass('short')
        return 'Too short'
    }
 
    //length is ok, lets continue.
 
    //if length is 8 characters or more, increase strength value
    if (password.length > 8) strength += 1
 
    //if password contains both lower and uppercase characters, increase strength value
    if (password.match(/([a-z].*[A-Z])|([A-Z].*[a-z])/))  strength += 1
 
    //if it has numbers and characters, increase strength value
    if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/))  strength += 1 
 
    //if it has one special character, increase strength value
    if (password.match(/([!,%,&,@,#,$,^,*,?,_,~])/))  strength += 1
 
    //if it has two special characters, increase strength value
    if (password.match(/(.*[!,%,&,@,#,$,^,*,?,_,~].*[!,",%,&,@,#,$,^,*,?,_,~])/)) strength += 1
 
    //now we have calculated strength value, we can return messages
 
    //if value is less than 2
    if (strength < 2 ) {
        return 'Weak'
    } else if (strength == 2 ) {
        return 'Good'
    } else {
        return 'Strong'
    }
}

function zeroPad(num, places) {
  var zero = places - num.toString().length + 1;
  return Array(+(zero > 0 && zero)).join("0") + num;
}

function validateEmail($email) {
		emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		  if( !emailReg.test( $email ) ) {
			return false;
		  } else {
			return true;
		  }
}

function getCity(id,city){
	var selected = $("#state option:selected").text();
	$("#statesel .customStyleSelectBox").text(selected);
	var planId=$.trim($("#planId").val());
	$("#centerName").val('');
	$.ajax({
		   type:"post",
		   url:'ajax/getCity.php',
		   data:{'id':id,'cityId':city},
		   success:function(resp){
			   $("#city").html(resp);
			   $("#cityId").trigger('onchange');
			   }
		   });
}
function getRetCity(id){
	var selected = $("#state option:selected").text();
	$("#statesel .customStyleSelectBox").text(selected);
	var planId=$.trim($("#planId").val());
	$("#centerName").val('');
	$.ajax({
		   type:"post",
		   url:'ajax/getRetCity.php',
		   data:{'id':id,'planId':planId},
		   success:function(resp){
			   $("#city").html(resp);
			   
			   }
		   
		   
		   });
}
function getCityPlan(id,city){
	var selected = $("#state option:selected").text();
	$("#statesel .customStyleSelectBox").text(selected);
	
	$("#centerName").val('');
	$.ajax({
		   type:"post",
		   url:'ajax/getCityPlan.php',
		   data:{'id':id},
		   success:function(resp){
			   $("#city").html(resp);
			   
			   }
		   
		   
		   });
}
function getCityName(id,cityname){
	$("#centerName").val('');
	$.ajax({
		   type:"post",
		   url:'ajax/getCityName.php',
		   data:{'id':id},
		   success:function(resp){
			   $("#cityname").html(resp);
			
			   }
		   
		   
		   });
}
function CheckAll()
{
	var className='checkbox_chk';
	 if($("#checkbox_").attr('checked')==true)
	{
		$(".checkbox_chk").attr('checked', true);
	}
	else
	{
		$(".checkbox_chk").attr('checked', false);
	}	
}
 function validatePost(){		
		if(document.appointment_schedular_form.EMAILID.value==""){
			alert("Please Enter the Email Id");
			document.appointment_schedular_form.EMAILID.focus();
			return false;
		}else{
		if (!isEmail(document.appointment_schedular_form.EMAILID.value))
			{
				alert("Please enter the Email address in the proper Format");
				document.appointment_schedular_form.EMAILID.focus( );	
				return false;
			}
		}
		if(document.appointment_schedular_form.MOBILENO.value==""){
			alert("Please Enter the Mobile No.");
			document.appointment_schedular_form.MOBILENO.focus();
			return false;
		}
		if(document.appointment_schedular_form.MOBILENO.value.length<10){
			alert("Mobile Number should be 10 digits");
			document.appointment_schedular_form.MOBILENO.focus();
			return false;
		}
		
		if(document.appointment_schedular_form.state.value==""){
			alert("Please select State");
			document.appointment_schedular_form.state.focus();
			return false;
		}
		if(document.appointment_schedular_form.city.value==""){
			alert("Please select City");
			document.appointment_schedular_form.city.focus();
			return false;
		}
		if(document.appointment_schedular_form.centerName.value==""){
			alert("Please enter the Center Name");
			document.appointment_schedular_form.centerName.focus();
			return false;
		}
		if(document.appointment_schedular_form.appointmentDate.value==""){
			alert("Please select the Appointment Date 1");
			document.appointment_schedular_form.appointmentDate.focus();
			return false;
		}
		if(document.appointment_schedular_form.appointmentDate2.value==""){
			alert("Please select the Appointment Date 2");
			document.appointment_schedular_form.appointmentDate2.focus();
			return false;
		}
		
		if(document.appointment_schedular_form.appointmentDate2.value==document.appointment_schedular_form.appointmentDate.value){
			alert("Please select different Appointment Date for slot 2");
			document.appointment_schedular_form.appointmentDate2.focus();
			return false;
		}
		var checkedLength=numberOfSelectedCheckbox('memberc');
		if(checkedLength==0){		
			alert("Please select the member");
			return false;
		}else{
			document.appointment_schedular_form.submit();
		}		
}
function numberOfSelectedCheckbox(className)
 {
 var selector_checked = $("input[class="+className+"]:checked").length;
 return selector_checked;
 }
 function SublitForm(formName){
	// alert(formName);
	 $("#"+formName).submit();
 }
 
 // this function is used for only select state where center exists created by amit kumar dubey on 3 september 2015
function getStatePlan(){
	var planId=$("#corpplanid").val();
	$.ajax({
		   type:"post",
		   url:'ajax/getStatePlan.php',
		   data:{'planId':planId},
		   success:function(resp){
			   $("#statesel").html(resp);
			   
			   }
		   
		   
		   });
}
function submitForm(formid){
	$("#"+formid).submit();
}
function get_value(city){
	$("#centerName").val('');
		var state=$("#state").val();
		var planId = $("#planId").val();
		var selectedcenterid=$("#selectedcenterid").val();
		$.ajax({ 
			type: "POST", 
			url: "ajax/getCenterAjax.php",
			data: "stateId="+state+"&cityId="+city+"&planId="+planId+"&selectedcenterid="+selectedcenterid,
			success: function(msg){
			 $("#center").html(msg);
			}
	});
}

function getNoOfAppInSlot(id){
		$.ajax({
				type:'POST',
				dataType: "json",
				url:'ajax/get_data.php',
				data:{'id':id,'type':'GETNOOFAPPBYSLOT'},
				success:function(response) {
					$('#pendingapp').val(response.AVAILABLEAPPOINTMENT);
					$('#slotid').val(response.SLOTID);
					$('#maxappointment').val(response.MAXAPPOINTMENT);
					$('#noofappointment').val(response.NOOFAPPOINTMENT);
					if(response.AVAILABLEAPPOINTMENT ==0){
						alert('Appointment Full Please Select Another Slot');
					}
				}
		});

}

// function to validation schedule appointment form created by amit kumar dubey on 6 oct 2015 at 01:32:54PM

function validate_retappointment(){
	var EMAILID=$.trim($("#EMAILID").val());
	var MOBILENO=$.trim($("#MOBILENO").val());
	var address=$.trim($("#address").val());
	var state=$.trim($("#state").val());
	var city=$.trim($("#cityId").val());
	var center_autocomp=$.trim($("#center_autocomp").val());
	var centerId=$.trim($("#centerId").val());
	var centerName2=$.trim($("#centerName2").val());
	
	var datepicker1=$.trim($("#datepicker1").val());
	var timeslot1=$.trim($("#timeslot1").val());
	var datepicker2=$.trim($("#datepicker2").val());
	var timeslot2=$.trim($("#timeslot2").val());
		if(EMAILID==""){
			alert("Please Enter the Email Id");
			$("#EMAILID").focus();
			return false;
		}else{
		if (!isEmail(EMAILID))
			{
				alert("Please enter the Email address in the proper Format");
				$("#EMAILID").focus();
				return false;
			}
		}
		if(MOBILENO==""){
			alert("Please Enter the Mobile No.");
				$("#MOBILENO").focus();
				return false;
		}
		if(MOBILENO.length<10){
			alert("Mobile Number should be 10 digits");
				$("#MOBILENO").focus();
			return false;
		}
/*		if(address==""){
			alert("Please Enter the Address.");
				$("#address").focus();
				return false;
		}
*/		if(state==""){
			alert("Please select State");
			$("#state").focus();
			return false;
		}
		if(city==""){
			alert("Please select City");
			$("#city").focus();
			return false;
		}
//alert(document.appointment_schedular_form.centerName.value);
	//alert(center_autocomp+"/"+centerId+"/"+centerName2);
		if(center_autocomp=="" || centerId=="No match" || centerName2 !=1){
			alert("Please enter the Center Name");
			$("#center_autocomp").focus();
			return false;
		}
		if(datepicker1==""){
			alert("Please select the Appointment Date 1");
			$("#datepicker1").focus();
			return false;
		}
		if(datepicker1 != ""){
			if(timeslot1 == ""){
				alert("Please select the Time Slot 1");
				$("#timeslot1").focus();
				return false;
			}
		}
		var checkedLength=numberOfSelectedCheckbox('memberc');
		if(checkedLength==0){		
			alert("Please select the member");
			return false;
		}else{
			$("#appointmentForm").submit();
		}		
		
		
}
function validatePolicyReg(frm){
        var dob = frm.dob.value;
        var customerId = trim(frm.customerId.value);
        var policyNumber = trim(frm.policyNumber.value);
        if(policyNumber == "" || policyNumber == "Policy No *"){
            alert("Please fill policy number.");
            return false;
        }        
        if(!$.isNumeric(policyNumber))
        {
            alert("Policy Number must be numeric.");
            return false;
        }        
        if(policyNumber.length != 8){
            alert("Please enter correct Policy Number");
            return false;			
        }
        if (dob == "" || dob == "DOB *"){
            alert("Please fill date of birth.");
            return false;
        }
        if(customerId == "" || customerId == "Customer ID *"){
            alert("Please fill customer id.");
            return false;
        }
        
        $.ajax({
            type    : 'POST',
            url     : 'checkPolicy.php', 
            data    : $('#registrationForm').serialize(),
            success : function (data){
                                        if(data == 'success')
                                        {
                                            window.location.href = "../retail/individual-policy-registration-1.php";
                                        }  
                                        else if(data == 'failure')
                                        {
                                            alert('You are already registered, please proceed to login.');
                                            window.location.href = "../retail";
                                        }
                                        else if(data == 'invaliddata')
                                        {
                                            alert('Policy number or customer id is not valid.');
                                            return false;
                                        }
                            }
        });
}

function validatePolicyReg1(frm){
        var firstName   = frm.firstName.value;
        var lastName    = frm.lastName.value;
        var dob         = frm.dob.value;
        var email       = frm.email.value;
        var customerId  = frm.customerId.value;
        var mobile      = frm.mobile.value;
        var OTPchkemail = frm.chkemail.checked;
        var OTPchkmobile= frm.chkmobile.checked;
        
        if(firstName == "" || firstName == "First Name *"){
            alert("Please fill first name.");
            return false;
        }
        if(lastName == "" || lastName == "Last Name *"){
            alert("Please fill last name.");
            return false;
        }
        if (dob == "" || dob == "DOB *"){
            alert("Please fill date of birth.");
            return false;
        }
        if (email == "" || email == "Email *"){
            alert("Please fill email.");
            return false;
        }
        if(customerId == "" || customerId == "Customer ID *"){
            alert("Please fill customer id.");
            return false;
        }
        if (mobile == "" || mobile == "Mobile *"){
            alert("Please fill mobile no.");
            return false;
        }        
        if(mobile.length != 10 || !$.isNumeric(mobile)){
            alert("Please enter correct mobile number.");
            return false;			
        }
        if((OTPchkemail==false && OTPchkmobile==false))
        {
            alert("Please select generate OTP method.");
            return false;			
        }
}
function showClass(className){
	$("."+className).show();
}
//this function is used for fetch range wise sequence value like an age range Ex [1,2,3,4,8,9,10] => 1-2,8-10 created by amit kumar dubey on 27 november 2015 at 4:21 PM
function getRanges(array) {
  var ranges = [], rstart, rend;
  for (var i = 0; i < array.length; i++) {
    rstart = array[i];
    rend = rstart;
    while (array[i + 1] - array[i] == 1) {
      rend = array[i + 1]; // increment the index if the numbers sequential
      i++;
    }
    ranges.push(rstart == rend ? rstart+'' : rstart + '-' + rend);
  }
  //$("#ageRange").html(ranges);
  return ranges;
}
function checkProfileUpdation(){
	var preEmailvalue=$.trim($("#EMAILID").val());	
	var preMobilevalue=$.trim($("#MOBILENO").val());	
	var newEmailvalue=$.trim($("#emailcheck").val());	
	var newMobilevalue=$.trim($("#mobilecheck").val());
	if((preEmailvalue != newEmailvalue) || (preMobilevalue != newMobilevalue)){
		if(confirm('Do you wish to update this email id / mobile in your profile?')){
			$("#changeprofile").val('YES');	
		} else {
			$("#changeprofile").val('NO');	
		}
	} else {
			$("#changeprofile").val('NO');	
		}
		
}
//************************* login - Dinesh. Jan 25, 2016 15:45 ********************

function removeErrorLogin(){  // function to hide jquery error message from login page if user click on forgot link and again came to login page
var divsToHide1 = document.getElementById("username-error");
var divsToHide2 = document.getElementById("userType-error");
var divsToHide3 = document.getElementById("password-error");

if(divsToHide1){ //  remove other section errors
	divsToHide1.style.display="none";	
}
if(divsToHide2){
	divsToHide2.style.display="none";	
}
if(divsToHide3){
	divsToHide3.style.display="none";	
}
// apply css class to display box selected or not selected
document.getElementById("tpaicon_login").className = "maleIcon";
document.getElementById("chainicon_login").className = "femaleIcon";
document.getElementById("tpaicon_activate_ac").className = "maleIcon";
document.getElementById("chainicon_activate_ac").className = "femaleIcon";
document.getElementById("userType").value = "";
document.getElementById("userType3").value = "";
}

function removeErrorAccountActivate(){
	
var divsToHideUtype = document.getElementById("userType3-error");
var emailidActivate = document.getElementById("emailidActivate-error");
var mobile = document.getElementById("mobile-error");
var chkboxrole = document.getElementById("chkboxrole-error");

if(divsToHideUtype){ //  remove other section errors
	divsToHideUtype.style.display="none";	
}
if(emailidActivate){ //  remove other section errors
	emailidActivate.style.display="none";	
}
if(mobile){ //  remove other section errors
	mobile.style.display="none";	
}
if(chkboxrole){ //  remove other section errors
	chkboxrole.style.display="none";	
}
document.getElementById("tpaicon_activate_ac").className = "maleIcon";
document.getElementById("chainicon_activate_ac").className = "femaleIcon";
document.getElementById("userType3").value = "";
document.getElementById("userType").value = "";
}

function removeErrorForgot(){ // function to hide jquery error message from forgot password page if user click on back button and again came to forgot password page
var divsToHide4 = document.getElementById("userType2-error");
var divsToHide5 = document.getElementById("emailid-error");
if(divsToHide4){ //  remove other section errors
	divsToHide4.style.display="none";	
}
if(divsToHide5){
	divsToHide5.style.display="none";	
}
// apply css class to display box selected or not selected
document.getElementById("tpaicon_forgot").className = "maleIcon";
document.getElementById("chainicon_forgot").className = "femaleIcon";
document.getElementById("userType2").value = "";

} 

function getCenterValue(value){ // pass selected user type value to hidden field for login section
document.getElementById("userType").value = value;
}

function getCenterValue2(value){ // pass selected user type value to hidden for forgot password section
document.getElementById("userType2").value = value;
}
function getCenterValue3(value){ // pass selected user type value to hidden field for activate user account section
document.getElementById("userType3").value = value;
}

//***************************login -EOC- Dinesh*********************

function ranvir_tab(en)
{
    var tabcont_id  = $(en).attr("id");
    
    $(".claimdisplayContent").hide();
    $("#"+tabcont_id+"tab, .service_tabs").show();
    
    $(".claimdisplaynav ul li a").removeClass("activetab");
    $(en).addClass("activetab");

}

function ranvir_tabS(en)
{
    var tabcont_id  = $(en).attr("id");
    
    $(".service_tabs").hide();
    $("#"+tabcont_id+"tab").show();
    
    $(".claimdisplaynav1 ul li a").removeClass("activetab");
    $(en).addClass("activetab");

}

/* WeCare Doctor Search  JS Functions */ 
$(".custom-checkbox").click(function () {
	$(this).toggleClass('custom-checkboxActive');
});

$("h3").click(function(){ 
	$(".selectColorMain").removeClass("selectColorMain");
	$(this).parent().parent().children().addClass("selectColorMain");
	
	$(".addressDetail").hide();
	$(this).parent().parent().children().children().show();	
	
	$(".srcharrowup1").removeClass("srcharrowup1");
	$(this).parent().parent().children().children().children("span").addClass("srcharrowup1");
});
 function generate_coupon(doc_id,policy_number)
    {  
        $("#generate_coupon_load_image"+doc_id).show();
         $(".wecare-generateCoupon").hide();
        $(".wecare-generateCoupon").html("");
        $.ajax({
                type:"post",
                url:'ajax/generate_coupon.php',
                 data:{'id':doc_id,'policyNumber':policy_number},
                success:function(resp){         
                        $("#coupon_form"+doc_id).show();
                        $("#coupon_form"+doc_id).html(resp);
                        $("#generate_coupon_load_image"+doc_id).hide();
                        $('html,body').animate({
                        scrollTop: $("#coupon_form"+doc_id).offset().top},
                        500);
                        } 
//                        doctor_unique_id
//                        coupon_form
                });
       
                
    }  
   function feedback(doc_id)
    { 
         $(".wecare-generateCoupon").hide();
         $("#generate_coupon_load_image"+doc_id).show();
         
        $(".wecare-generateCoupon").html("");
         $.ajax({
                type:"post",
                url:'ajax/feedback.php',
                data:{'id':doc_id},
                success:function(resp){     
                     $("#coupon_form"+doc_id).show();
                     $("#coupon_form"+doc_id).html(resp);
                      $("#generate_coupon_load_image"+doc_id).hide();
                      //  $("#inline_example1").html(resp);
                        } 
                });
    }

 
function doctor_sms(doc_id)
{       $(".wecare-generateCoupon").hide();
      $("#generate_coupon_load_image"+doc_id).show();
        $(".wecare-generateCoupon").html("");
         $.ajax({
                type:"post",
                url:'ajax/send_sms_doc.php',
                data:{'id':doc_id},
                success:function(resp){     
                     $("#coupon_form"+doc_id).show();
                     $("#coupon_form"+doc_id).html(resp);
                      $("#generate_coupon_load_image"+doc_id).hide();
                     
                        } 
                });
}

function sendDoctorListSMS1(){  
		var t=trim($("#firstName").val());
		var n=trim($("#mobileNo").val());
		var doctorId=trim($("#doctorId").val());
		if(t==""||t=="Your Name *"){
			//alert("Please enter your name");
                          $("#error_message").show();
                          $("#error_message").html("Please enter your name.");
                          $("#error_message").hide(5000);
			return false;
		}
		if(n==""||n=="Phone No. *"){ 
                          $("#error_message").show();
                          $("#error_message").html("Please enter your Mobile Number.");
                          $("#error_message").hide(5000);
                        
			return false;
		}else if(!isCharsInBag1(n, "0123456789")) {
			 
                                 $("#error_message").show();
                          $("#error_message").html("Mobile must only contain Numbers.");
                          $("#error_message").hide(5000);
				return false;
			}
                         if(n.length<10)
                    { 
                         $("#error_message").show();
                          $("#error_message").html("Mobile Number Not valid.");
                          $("#error_message").hide(5000);
                        return false;
                    }
		$("#hospitallistsms1").html('<img src="images/loading.gif" class="fl">');
		$.ajax({ 
				type: "POST", 
				url:"ajax/doctor_sms.php",
				data:"firstname="+t+"&mobilephone="+n+"&doctorId="+doctorId,
				success: function(msg){
				 
				alert("Doctor details has been sent to your Mobile.");
                                  $("#coupon_form"+doctorId).html(""); 
				 
				}
				});
}
function isCharsInBag1(s, bag)
{
    var i;
    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1)
            return false;
    }
    return true;
}
 
function network_doctor_email(docid) {
    //var hospitalList = getInboxSelectedValue('hospital');
     $(".wecare-generateCoupon").hide();
     $("#generate_coupon_load_image"+docid).show();
      $(".wecare-generateCoupon").html("");
        $.ajax({
                type:"post",
                url:'ajax/send_email_doc.php', 
                data:"doctorid="+docid,
                success:function(resp){     
                     $("#coupon_form"+docid).show();
                      $("#coupon_form"+docid).html(resp);
                       $("#generate_coupon_load_image"+docid).hide();
                        } 
                });
      
        
        
    

}
 
 
 
 
 function sendDoctorListEmail(doctorid){
		var e=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var t=trim($("#yourname").val());
		var n=trim($("#emailId").val());
		if(t==""||t=="Your Name *"){
			 
                          $("#error_message").show();
                          $("#error_message").html("Please enter your name.");
                          $("#error_message").hide(5000);
			return false;
		}
		if(n==""||n=="Email ID *"){ 
                          $("#error_message").show();
                          $("#error_message").html("Please enter your email address.");
                          $("#error_message").hide(5000);
			return false;
			}else{
				if(e.test(n)==false){
                                    $("#error_message").show();
                                    $("#error_message").html("Please enter your email address in correct format.");
                                    $("#error_message").hide(5000); 
                                    return false
                                }
			}
	 
	$("#hospitallistemail").html('<img src="images/loading.gif" class="fl">');
	$.ajax({ 
				type: "POST", 
				url:"ajax/doctor_email.php",
				data:"firstname="+t+"&emailaddress="+n+"&doctorid="+doctorid,
				success: function(msg){ 
                                        alert("Doctor details have been sent to your email ID.");
                                        $("#coupon_form"+doctorid).html(""); 
				}
				});
}

function pad (max,id, val) {
	var str = val ? val : $("#"+id).val();
	
  return astr = str.length < max ? pad(max, id, "0" + str) : str;  
}

function display(max, id){
	//astr = pad(3,id);
	if($("#"+id).val() && $("#"+id).val()!=0)
	{
		$("#"+id).val(pad(max,id));
	}
}