<?php 
/*
$menuBg             = @$menuBg          ? 'lightgray'   : '#fff';
$menuFont           = @$menuFont        ? 'blue'        : '#5f5f5f';
$menuFontHover      = $menuFontHover    ? 'green'       : '#312c29';
$menuBgHover        = @$menuBgHover     ? 'pink'        : '#fafafa';
$menuFloorHover     = @$menuFloorHover  ? 'yellow'      : '#b7dc7d';
$boxBg              = @$boxBg           ? '#B7CCCC'     : '#8a8a8a';
$boxBgHover         = @$boxBgHover      ? '#EAEFF5'     : '#eaeff5';
$boxFont            = @boxFont          ? 'brown'       : '#fff';
$boxFontHover       = @$boxFontHover    ? 'white'       : '#444';
$pgTitleFont        = @$pgTitleFont     ? 'blue'        : '#2e7d07';
$dboardNameCol      = @$dboardNameCol   ? 'color: red;' : '';
$border             = @$border          ? 'red'         : '#e0e2e3';
$contBoxBg          = @$contBoxBg       ? '#E7E4EC'     : '#fafafa';
$btnBg              = @$btnBg           ? '#B0C3A1'     : '#5e9d2d';
$btnFont            = @$btnFont         ? '#EAA8A8'     : '#fff';
$btnFontHover       = @$btnFontHover    ? 'white'       : '#fff';
$btnBgHover         = @$btnBgHover      ? '#5E9D2D'     : '#b7dc7d';
//$contBoxBg1         = @$contBoxBg1      ? '#E7E4EC'     : '#fafafa';
//$contBox1BgHover    = @$contBox1BgHover ? '#EAEFF5'     : '#eaeff5';
//$border1            = @$border1         ? 'red'         : '#e2e4e5';
//$box1Font           = @$boxFont1        ? 'brown'       : '#444';
//$box1FontHover      = @$boxFontHover1    ? 'white'       : '#2e7d07';

$contBoxBg1         = $contBoxBg;
$border1            = $border;
$box1Font           = $boxFont;
$box1FontHover      = @$boxFontHover;

$borderHover        = @$borderHover     ? 'blue'        : '#b7dc7d';
$rowBorder          = @$rowBorder       ? 'red'         : '#e0e2e3';
 */

$menuBg = $_SESSION['color']['MENUBG'];
$menuFont = $_SESSION['color']['MENUFONT'];
$menuFontHover = $_SESSION['color']['MENUFONTHOV'];
$menuBgHover = $_SESSION['color']['MENUBGHOV'];
$menuFloorHover = $_SESSION['color']['MENUFLOORHOV'];
$boxBg = $_SESSION['color']['BOXBG'];
$boxFont = $_SESSION['color']['BOXFONT'];
$boxFontHover = $_SESSION['color']['BOXFONTHOV'];
$pgTitleFont = $_SESSION['color']['PAGETITLEFONT'];
$dboardNameCol = $_SESSION['color']['DASHBOARDNAMECOL'];
$border = $_SESSION['color']['BORDER'];
$btnBg = $_SESSION['color']['BUTTONBG'];
$btnFont = $_SESSION['color']['BUTTONFONT'];
$btnFontHover = $_SESSION['color']['BUTTONFONTHOV'];
$btnBgHover = $_SESSION['color']['BUTTONBGHOV'];
$borderHover = $_SESSION['color']['BORDERHOV'];
$rowBorder = $_SESSION['color']['ROWBORDER'];

$selDateFont = $_SESSION['color']['SELDATEFONT'];
$dateBgHover = $_SESSION['color']['DATEBGHOV'];

$border1 = $_SESSION['color']['BORDER1'];
$contBoxBg = $_SESSION['color']['CONTBOXBG'];
$contBoxFont = $_SESSION['color']['CONTBOXFONT'];
$contBoxBorder = $_SESSION['color']['CONTBOXBORDER'];
$contBoxBgHover = $_SESSION['color']['CONTBOXBGHOVER'];
$contBoxFontHover = $_SESSION['color']['CONTBOXFONTHOVER'];

$contBoxBg1         = $contBoxBg;
$box1Font           = $boxFont;
$box1FontHover      = $boxFontHover;

echo
"<style type='text/css'> body {
  background:#edf9e5;
  color: #666;
  font-size: 16px;
  line-height:16px;
  font-family:'SEGOEUIL', Arial, Helvetica, sans-serif;
}
 @font-face {
font-family:'SEGOEUIL';
src:url('fonts/SEGOEUIL.eot?') format('eot'), url('fonts/SEGOEUIL.woff') format('woff'), url('fonts/SEGOEUIL.ttf') format('truetype');
font-weight:normal;
font-style:normal;
}
@font-face {
font-family:'seguisb_0';
src:url('fonts/seguisb_0.eot?') format('eot'), url('fonts/seguisb_0.woff') format('woff'), url('fonts/seguisb_0.ttf') format('truetype'), url('fonts/seguisb_0.svg#seguisb_0') format('svg');
font-weight:normal;
font-style:normal;
}
@font-face {
font-family:'Segoe_UI';
src:url('fonts/Segoe_UI.eot?') format('eot'), url('fonts/Segoe_UI.woff') format('woff'), url('fonts/Segoe_UI.ttf') format('truetype'), url('fonts/Segoe_UI.svg#Segoe_UI') format('svg');
font-weight:normal;
font-style:normal;
}
@font-face {
font-family:'WebRupee';
src:url('WebRupee.V2.0.eot');
src:local('WebRupee'), url('fonts/WebRupee.V2.0.ttf') format('truetype'), url('fonts/WebRupee.V2.0.woff') format('woff'), url('fonts/WebRupee.V2.0.svg') format('svg');
font-weight:normal;
font-style:normal;
}
.img-responsive{width:80%;}
.textbold-1{text-align:center; font-size:18px;}

.sorting{float:right;}
   .textbold{text-align:left; font-size:18px; margin:0 -30px;}
   .textbold p{text-align:left; font-size:14px !important; font-weight:300; margin:10px 0; line-height:24px;}

  .textboldProducts{
	font-size: 14px !important;
    margin-top: 10px;
  } 
.topContainer {
  margin:0;
  padding:0 .5%;
}
.topContainer .con-in {
  padding:10px 0;
  margin:0;
  float:left
}
.topContainer .con-in form label {
  font-weight:normal;
  float:left;
  font-size:25px;
  color:#5D5D5D;
}
.topContainer .con-in form .form-control {
  height:38px;
  line-height:38px;
  width:200px;
  border-color:#dfe6ef;
  font-size:14px;
  color:#8d8d8d;
  font-weight:400;
  float:left;
  margin:0;
  border-radius: 0px;
  box-shadow: 0 0 0 rgba(0, 0, 0, 0.075)
}
.topContainer .con-in form .btn {
  float:left;
}
.topContainer .con-in1 {
  padding:0;
  margin:11px 0 2% 0;
  float:left;
  margin-left:-15px;
  margin-right:-15px;
}
.break-text {
  text-decoration:line-through;
}
.total-text {
  font-size:18px;
  text-transform:uppercase;
  font-weight:600;
}
.total-price {
  font-size:24px;
  text-transform:uppercase;
  color:#66a503;
  font-weight:600;
}
.tab-mid-container-buy {
  margin:0;
  padding:7% 0 6% 0;
  display:block;
  overflow:hidden;
  text-align:right;
}
.tab-mid-container-sm {
  margin:0;
  padding:1 1.5%;
  font-size:12px;
  color:#8a8a8a;
}
.tab-mid-container-sm span {
  font-size:12px;
  color:#5e9d2d;
  font-weight:600;
}
.tab-mid-container-buy a {
  margin:0;
  padding:3.5% 5%;
  background:$btnBg;
  color:$btnFont;
  border:none;
  font-weight:600;
  font-size:18px;
  text-shadow: 1px 1px #6e6e6e;
}
.tab-mid-container-buy a:hover {
  margin:0;
  padding:3.5% 5%;
  background:$btnBgHover;
  color:$btnFontHover;
  border:none;
  font-weight:600;
  font-size:18px;
  text-shadow: 1px 1px #6e6e6e;
}
.tab-mid-container-buy:hover {
  text-decoration:none;
  transition: all 0.4s ease-in-out 0s;
  color:#FFF
}
.slect-border {
  border-left:1px solid #999;
  height:25px;
  padding:50px;
}
.topContainer .con-in form .btn-danger {
  background:#fff url(img/search-icon.png) no-repeat 10px center;
  border-color: #dfe6ef;
  border-left:0;
  border-radius:0px;
  padding:8px 22px;
  height:38px;
}
.topContainer .con-in form .btn-danger:hover, .btn-danger:focus {
  background:#fafafa url(img/search-icon.png) no-repeat 10px center;
  border-color: #dfe6ef;
}
.topContainer-logo {
  height:auto;
  padding:15px 0 0 0;
  float:left;
  margin-right:30px;
}
.topContainer-logo img {
  margin:0;
}


.topContainer-us-icon {
  margin:0;
  margin-right:-7px;
  z-index:9999;
  padding:0;
}
.topContainer-us-icon ul {
  margin:0;
  padding:0;
}
.topContainer-us-icon li {
  margin:0;
  margin-top:10px;
  padding-left:5px;
  list-style:none;
  float:right;
}
.topContainer-us-icon li a {
  border-radius: 20px;
  display: block;
  font-size: 24px;
  height: 40px;
  line-height: 40px;
  padding: 0;
  display:block;
  text-align: center;
  width: 40px;
  background:#fff;
 
}
.topContainer-us-icon .dropdown-user {
  right: 0;
  left: auto;
  padding: 10px 15px !important;
}
.topContainer-us-icon .dropdown-user h1 {
  margin:5px 0 10px 0 !important;
  padding:0;
  font-size:24px !important;
  color:#0f6633 !important;
}
.topContainer-us-icon .dropdown-user li {
  float:inherit;
  display:block !important;
  width:100%;
}
.topContainer-us-icon .dropdown-user li a {
  float:left;
  background:#eaeff5;
  display:block;
  margin:5px 0;
  padding:10px 10px 12px 10px!important;
  width:98%;
  text-align:left;
  border-radius: 0px;
  font-size:18px;
  color:#5f5f5f;
  line-height:inherit;
  height:inherit;
}
.topContainer-us-icon .dropdown-user li a:hover {
  background:#8a8a8a;
  color:#fff;
}
.topContainer-us-icon .dropdown-alerts {
  right: 0;
  left: auto;
}
.topContainer-us-icon .dropdown-alerts h1 {
  margin:5px 0 10px 0 !important;
  padding: 10px 15px !important;
  font-size:24px !important;
  color:#0f6633 !important;
}
.topContainer-us-icon .dropdown-alerts h2 {
  margin:5px 0!important;
  padding:0 !important;
  font-size:14px !important;
  color:#444 !important;
  font-weight:700;
}
.topContainer-us-icon .dropdown-alerts p {
  margin:0;
  padding:0;
  font-size:14px !important;
  color:#777777 !important;
}
.topContainer-us-icon .dropdown-alerts li {
  float:right;
  width:90%;
  margin:0;
  margin-left:0;
  padding-left:0;
  padding-right:0;
  border-right:0 !important;
  border-left:5px solid #cddfea !important;
}
.topContainer-us-icon .dropdown-alerts li a {
  float:left;
  background:#fdfdfd;
  display:block;
  margin:0;
  padding:10px 10px 12px 10px!important;
  width:100%;
  text-align:left;
  border-radius: 0px;
  font-size:18px;
  color:#5f5f5f;
  line-height:inherit;
  height:inherit;
}
.dropdown-menu .divider {
  background-color: #e5e5e5;
  height: 1px;
  margin:0;
  overflow: hidden;
  width:100% !important;
}
.topContainer-us-icon .dropdown-tasks {
  right: 0;
  left: auto;
  padding: 10px 15px !important;
}
.topContainer-us-icon .dropdown-tasks h1 {
  margin:5px 0 10px 0 !important;
  padding:0;
  font-size:24px !important;
  color:#0f6633 !important;
}
.topContainer-us-icon .dropdown-tasks li {
  float:left;
  width:100%;
  margin:5px 0;
  display:block;
}
.topContainer-us-icon .dropdown-tasks {
  padding:10px 0;
  margin:0;
  float:left
}
.topContainer-us-icon .dropdown-tasks form label {
  font-weight:normal;
  float:left;
  font-size:25px;
  color:#5D5D5D;
}
.topContainer-us-icon .dropdown-tasks form .form-control {
  height:38px;
  line-height:38px;
  width:99%;
  background:#eaeff5;
  border-color:#dfe6ef;
  font-size:14px;
  color:#8d8d8d;
  font-weight:400;
  float:left;
  margin:0;
  border-radius: 0px;
  box-shadow: 0 0 0 rgba(0, 0, 0, 0.075)
}
.topContainer-us-icon .dropdown-tasks form .btn {
  float:left;
  width:99%;
  background:#8a8a8a;
  color:#FFF;
  font-size:18px;
  border-radius: 0px;
  border:1px solid #000;
}
.topContainer-us-icon .dropdown-tasks form .btn:hover {
  background:#121921;
  color:#FFF;
  transition: all 0.4s ease-in-out 0s;
}
.Quote-box input[type='text'], .Quote-box textarea {
  margin:0;
  padding:5px 10px;
  border-radius:0px;
  background:#eaeff5;
  -webkit-border-radius:4px;
  -moz-border-radius:4px;
  border:1px solid #d6dcce;
  width:99%;
  display:block
}
.Quote-box input[type='text']:hover, .Quote-box input[type='text']:hover, .Quote-box textarea:hover, .Quote-box textarea:focus {
  transition: all 0.4s ease-in-out 0s;
  border-color:#8a8a8a
}
.dropdown-menu {
  background-clip: padding-box;
  background-color: #fff;
  border-top:0 !important;
  border: 1px solid rgba(0, 0, 0, 0.15);
  border-radius: 0px;
  box-shadow: 0 6px 12px rgba(0, 0, 0, 0.176);
  display: none;
  float: left;
  font-size: 14px;
  left: 0;
  list-style: none outside none;
  margin: 7px 0 0 !important;
  padding: 10px 0 !important;
  position: absolute;
  text-align: left;
  top: 100%;
  z-index: 99999;
  width:300px;
}
.dropdown-menu.pull-right {
  left: auto;
  right: 0;
}
.graytxtBox-Location {
  width:42%;
  float:left;
  padding:5px 8px;
  background:#fff;
  border:1px solid #e0e2e3;
  -webkit-border-radius:0px;
  -moz-border-radius:0px;
  border-radius:0px;
  margin:0 10px 0px 0px;
}
.graytxtBox-Spec {
  width:50%;
  float:left;
  padding:5px 8px;
  background:#fff;
  border:1px solid #e0e2e3;
  -webkit-border-radius:0px;
  -moz-border-radius:0px;
  border-radius:0px;
  margin:0 0px;
}
.txtMrgn {
  margin:0 8px 0 45px;
}
.txtfieldFull {
  width:100%;
  background:none;
  border:0px;
  color:#4444;
  font-size:16px;
  padding:7px 0px;
}
.greenSrchBtn {
  color:$btnFont;
  width:10%;
  float:right;
  padding:12px 5px;
  background:url(img/srch-icon.png) center center no-repeat $btnBg;
  border:1px solid #0f6633;
  -webkit-border-radius:0px;
  -moz-border-radius:5px;
  border-radius:5px;
  margin:0 0px;
  text-indent:-999999px;
  cursor:pointer;
}
.greenSrchBtn {
  width:6%;
  color:$btnFont;
  float:right;
  padding:13px 5px;
  background:url(img/srch-icon.png) center center no-repeat $btnBg;
  border:1px solid #0f6633;
  -webkit-border-radius:0px;
  -moz-border-radius:0px;
  border-radius:0px;
  margin:0 0px;
  text-indent:-999999px;
  cursor:pointer;
}
.greenSrchBtn:hover {
  color:$btnFontHover;
  background:url(img/srch-icon.png) center center no-repeat $btnBgHover;
}
.existingSincedd {
  width:41%;
  overflow:hidden;
  padding:7px 1px;
  background:url(img/gray-arrow1.png) right center no-repeat #ffffff;
  border:1px solid #dbdbdb;
  -webkit-border-radius:0px;
  -moz-border-radius:5px;
  border-radius:5px;
  display:inline-block;
}
.existingSincedd select {
  width:132%;
  background:none;
  border:0px;
  color:#000000;
  font:normal 15px/25px 'Segoe_UI', Arial, Helvetica, sans-serif;
  outline:none;
}
.existingSincemm {
  width:35%;
  overflow:hidden;
  padding:7px 1px;
  background:url(img/gray-arrow1.png) right center no-repeat #ffffff;
  border:1px solid #dbdbdb;
  -webkit-border-radius:0px;
  -moz-border-radius:0px;
  border-radius:0px;
  display:inline-block;
  margin-right:4px;
}
.existingSincemm select {
  width:139%;
  background:none;
  border:0px;
  color:#000000;
  font:normal 15px/25px 'Segoe_UI', Arial, Helvetica, sans-serif;
  outline:none;
}
.zigzaghospital {
  background:url(img/zigzaghhos.png) right center no-repeat;
}
section.sectionInner .sectionRight .brochureOuter .brochureOuterIn .hospitalLocationResultMain .hospitalLocationResultMainIn {
  margin:0 0 0 0px;
  border:1px solid #dedede;
  background:#f9f9f9;
}
.hospitalLocationSearch {
  float:left;
  width:100%;
  padding-bottom:20px;
}
.positionHospital{
  float:right;
}
.hospitalText{
  clear:both; 
  font-size:15px;
  margin-bottom:10px;
}
.hospitalLocationResultMain {
  float:left;
  width:100%;
  margin:4px 0px;
}
.hospitalLocationResultMainIn {
  margin:0 0 0 0px;
  border:1px solid #dedede;
  background:#f9f9f9;
  overflow:hidden;
}
.hospitalLocationResultMainIn .addressBox {
  width:50%;
  padding:12px 0px;
}
.hospitalLocationResultMainIn .addressBoxLeft {
  float:left;
}
.hospitalLocationResultMainIn .addressBoxRight {
  float:right;
}
.hospitalLocationResultMainIn .addressBox h3 {
  float:left;
  width:100%;
  padding:0px 0px;
  margin:0 0px;
  cursor:pointer;
}
.selectColorMain {
  background: none repeat scroll 0 0 #e4ffc6 !important;
}
.addressBox h3 {
  cursor: pointer;
  float: left;
  margin: 0;
  padding: 0;
  width: 100%;
}
.addressBox .addressDetail {
  float: left;
  padding-top: 12px;
  width: 100%;
}
.addressBox .addressDetail .addressDetailIn {
  margin: 0 0 0 35px;
  padding-right: 25px;
}
.addressDetailIn .locationTopBox .checkboxContainer {
  float: left;
  position: relative;
  width: 40px;
}
.addressDetailIn .locationTopBox .locationContainer {
  color: #000000;
  font: 14px/22px 'Segoe_UI', Arial, Helvetica, sans-serif;
  margin: 0 0 0 40px;
}

.spacer{ clear: both;
    height: 20px;}
 


.fl {
  float: left;
}
.addressDetailIn .locationTopBox {
  float: left;
  padding-bottom: 15px;
  width: 100%;
}
.addressBox h3 code {
  color: #000000;
  display: flex;
  font: 15px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
.addressBox h3 span {
  background: url('img/selectArrowgr1.png') no-repeat scroll center center rgba(0, 0, 0, 0);
}
.addressBox h3 span.srcharrowup1 {
  background: url('img/selectArrowgr1.png') no-repeat scroll center center rgba(0, 0, 0, 0) !important;
}
.hospitalLocationResultMainIn .addressBox h3 span.srcharrowup {
  background: url('img/selectArrowgr.png') no-repeat scroll center center rgba(0, 0, 0, 0);
  display: block;
  float: left;
  height: 24px;
  width: 35px;
}
.hospitalLocationResultMainIn .addressBox h3 span.srcharrowup {
	background: url('../img/selectArrowgr.png') no-repeat scroll center center \9;

}
.custom-checkbox {
  background: url('img/checkboximg.png') no-repeat scroll left top rgba(0, 0, 0, 0);
  display: block!important;
  height: 23px;
  left: 0;
  position: absolute;
  top: 0;
  width: 25px;
  z-index:999 !important;
}
.custom-checkboxActive {
  background: url('img/checkboximg1.png') no-repeat scroll left top rgba(0, 0, 0, 0)!Important;
  display: block;
  height: 23px;
  left: 0;
  position: absolute;
  top: 0;
  width: 25px;
}
.addressBox h3 span.srcharrowup {
  background: url('img/selectArrowgr.png') no-repeat scroll center center rgba(0, 0, 0, 0);
  display: block;
  float: left;
  height: 24px;
  width: 35px;
}
span.txtIcon1 {
  width:40px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -436px -594px no-repeat;
}
span.txtIcon2 {
  width:45px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -490px -593px no-repeat;
}
span.txtIcon3 {
  width:45px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -436px -640px no-repeat;
}
span.txtIcon4 {
  width:45px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -490px -640px no-repeat;
}
span.txtIcon5 {
  width:45px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -8px -798px no-repeat;
}
span.txtIcon6 {
  width:45px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -52px -794px no-repeat;
}
span.txtIcon7 {
  width:35px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -104px -794px no-repeat;
}
span.txtIcon8 {
  width:45px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -7px -842px no-repeat;
}
span.txtIcon9 {
  width:45px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -138px -845px no-repeat;
}
span.txtIcon10 {
  width:45px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -537px -637px no-repeat;
}
span.txtIcon11 {
  width:35px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -420px -698px no-repeat;
}
/*------------------------- Dashboard -------------------*/

#middleContainer .dashboard-left {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #e0e2e3;
    margin: 0 0 3%;
    padding:2% 2% 4% 2%;
  overflow:hidden;
}
#middleContainer .dashboard-leftTop {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #e0e2e3;
    margin: 0 0 3%;
    padding:2% 2% 2% 2%;
}
#middleContainer .valueaddedBox {
    margin: 0 0 0%;
}
#middleContainer .valueaddedBox  .panel-group .panel + .panel {
    margin-top: 10px;
}
#middleContainer .valueaddedBox .panel-group .panel {
  margin-bottom: 0;
  border-radius: 0px;
  background: none repeat scroll 0 0 #fff;
    border: 1px solid #e0e2e3;
    padding:2% 2% 2% 2%;
}
#middleContainer .valueaddedBox .panel-default>.panel-heading {
  color: #312c29;
  background-color: #ffffff;
  border:1px solid $border;
}
#middleContainer .valueaddedBox .panel-body {
    padding: 0px!Important;
  border: 1px solid $border;
  border-top:0px;
}
#middleContainer .valueaddedBox .panel-title {
  margin-top: 0;
  margin-bottom: 0;
  font-size: 18px;
  color: inherit;
  font-family:'Segoe_UI',Arial,Helvetica,sans-serif;
}
#middleContainer .valueaddedBox .panel-title>a.collapsed {
  margin-top: 0;
  margin-bottom: 0;
  font-size: 18px;
  color: inherit;
  font-family:'Segoe_UI',Arial,Helvetica,sans-serif;
  background:url(img/selectArrowgr2.png) right center no-repeat;
}
#middleContainer .valueaddedBox .panel-title>a {
  margin-top: 0;
  margin-bottom: 0;
  font-size: 18px;
  color: inherit;
  font-family:'Segoe_UI',Arial,Helvetica,sans-serif;
  background:url(img/selectArrowgr1.png) right center no-repeat;
  outline:0px;
}
#middleContainer .valueaddedBox .panel-title strong {
    font-size: 30px;
    font-weight: 600;
}
#middleContainer .valueaddedBox .collapse {
  display: none;
  visibility: hidden;
  margin-top:-1px;
}
#middleContainer .valueaddedBox .collapse.in {
  display: block;
  visibility: visible;
  margin-top:-1px;
}
#middleContainer .dashboard-left h1{
    font-size:24px;
  color:$pgTitleFont;
  margin:0 0 3% 0;
  padding:0;
}
.topTittle{
    font-size:24px;
  color:#444444;
  margin:0 0 2% 0;
  padding:0;
}
#middleContainer h1{
    font-size:24px;
  color:#2e7d07;
  margin:0 0 2% 0;
  padding:0;
}
#middleContainer .dashboard-left h2{
    font-size:24px;
  color:#2e7d07;
  margin:0 0 3% 0;
  padding:0;
}

#middleContainer .dashboard-left h2 span{
    font-size:24px;
  color:$contBoxFont;
  margin:0 0 3% 0;
  padding:0;
  font-style:normal;
}
#middleContainer .dashboard-left_box{
margin:0; padding:0;
font-weight:600;
}

#middleContainer .dashboard-left_box a{
    font-size:13px;
  background:$contBoxBg;
  border:1px solid $contBoxBorder;
  color:$contBoxFont;
  margin:0;
  display:block;
  cursor:pointer;
  overflow:hidden;
  text-decoration:none;
  padding:5% 0;
  min-height:130px;
}


#middleContainer .dashboard-left_box a:hover{
  display:block;
  background:$contBoxBgHover !important;
  text-decoration:none;
  color:$contBoxFontHover !important;


}
#middleContainer .dashboard-left_box span{

  color:#2e7d07;
  margin-top:5px;
  display:block;
  clear:both;
  line-height:0px;
}



#middleContainer .dashboard-left_box IMG{
    text-align:left;
}




#middleContainer .dashboard-left_box1{
margin:0; padding:0;
font-weight:600;
}

#middleContainer .dashboard-left_box1 a{
    font-size:18px;
  background:$contBoxBg1;
  border:1px solid $border1;
  color:$box1Font;
  margin:0;
  font-weight:300;
  display:block;
  cursor:pointer;
  height:131px;
  overflow:hidden;
  text-decoration:none;
  padding:5% 0;
}


#middleContainer .dashboard-left_box1 a:hover{
  display:block;
  background:$contBoxBgHover !important;
  text-decoration:none;
    color:$contBoxFontHover !important;


}
#middleContainer .dashboard-left_box1 a:hover{
  display:block;
  background:$contBoxBgHover !important;
  text-decoration:none;
    color:$contBoxFontHover !important;


}
.grayBorderdashboard{
  border:1px solid $border;
  border-top:0px solid $border;
}
.dashboardName{
  border-top:1px solid $border;
  padding:12px 10px;
}
.dashboardName .colSm6{
  width:50%;
  float:left;
  font-size:18px;
  font-family:'Segoe_UI', Arial, Helvetica, sans-serif;
  line-height:30px;
}
.dashboardName .colSm7{
	width:50%;
	float:right;
}
.dashboardName .colSm7 img{
	float:right;
	vertical-align: middle;
}
.dashboardName .colSm6 strong{
  font-size:24px;
  font-weight:600; 
}
.responsivedash tr td{
	border-top:1px solid $border;
	/*border-bottom:1px solid $border; */
}
.panel-body table tr td{
	border-top:1px solid $border;
	/*border-bottom:1px solid $border; */
}
.model-header{
  width:100%;
  font:normal 24px 'Segoe_UI', Arial, Helvetica, sans-serif;
  /*color:#2e7d07; */
}
.model-footer  input[type='submit']{
	height: 40px;
    width: 87px;
}
.modal-middle {
    padding: 5px 15px;
    position: relative;
}
.model-footer{
  padding: 10px 15px 20px 15px;
    position: relative;
}
.popupBrowsefull{
  padding:8px 0px;
  width:100%;
}
.myPlanformBoxfull{
  padding:0px 0px 8px 0px;
  width:100%;
}
.myPlanformBoxfull .inputBox{
  padding:10px 0px;
  border-radius:2px;
  border:1px solid #e0e2e3;
  float:left;
  width:100%;
  box-shadow:inset 1px 1px 3px #eaeaea;
}
.myPlanformBoxfull .inputBox .inputBoxIn{
  margin:0px 15px;
  height:25px;
  overflow:hidden;
}
.myPlanformBoxfull .inputBox .dropBg{
  background:url(img/grayArrow.png) right center no-repeat;
}
.myPlanformBoxfull .inputBox .dropBg select{
  background:none;
  border:0px;
  color:#444444;
  font:normal 14px/25px 'Segoe_UI', Arial, Helvetica, sans-serif;
  width:105%;
}
.femaleIcon{
  width:41px;
  height:41px;
  display:inline-block;
  background:url(img/femaleIcon.png) left center no-repeat;
  float:left;
}
.maleIcon{
  width:41px;
  height:41px;
  display:inline-block;
  background:url(img/maleIcon.png) left center no-repeat;
  float:left;
}
.editIcon{
  width:33px;
  height:36px;
  display:inline-block;
  background:url(img/editIcon.png) left center no-repeat;
  cursor:pointer;
}
.healthcardeditIcon{
  width:33px;
  height:36px;
  display:inline-block;
   background:url(img/healthcardeditIcon.png) left center no-repeat;
  cursor:pointer;
}
.downloadIcon{
  width:23px;
  height:27px;
  display:inline-block;
  background:url(img/downloadIcon.png) left center no-repeat;
  cursor:pointer;
}
.umbIcon{
  width:33px;
  height:36px;
  display:inline-block;
  background:url(img/umbrellaIcon.png) left center no-repeat;
  margin-left:30px;
  cursor:pointer;
}
.dashboardName .colSm3{
  width:45px;
  float:right;
  text-align:right;
  margin-right:0px;
}
.dashboardName .colSm3 .dropdown-menu{
  margin-top:0px!Important;
  border: 1px solid rgba(0, 0, 0, 0.15)!Important;
  border-bottom:0px!Important;
  padding:0 0px!Important;
  width:150px!Important;
  right:0px!Important;
  left:-115px;
  margin-top:-1px;
}
.dashboardName .colSm3 .dropdown-menu>li>a {
  display: block;
  padding: 3px 20px;
  clear: both;
  font-weight: 400;
  line-height: 30px;
  color: #333;
  white-space: nowrap;
  border-bottom:1px solid #dfe6ef;
}
.locationtextfield {
  margin:0 1% 0px 0;
  padding:12px 15px 12px 4%;
  border:1px solid #e0e2e3;
  width:100%;
  font-size:16px;
  display:block;
  background:url(img/map-pin.png) no-repeat 3% center;
  width:46%;
  float:left;
  line-height: 22px;
}
.doctorsrchtextfield {
  margin:0 1% 0px 0;
  padding:12px 15px 12px 4%;
  border:1px solid #e0e2e3;
  width:100%;
  font-size:16px;
  display:block;
  background:url(img/search-icon1.png) no-repeat 3% center;
  width:46%;
  float:left;
  line-height: 22px;
}
#middleContainer .wecare-leftTop {
    margin: 0 0 3%;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #e0e2e3;
    padding:2% 2% 2% 2%;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .topBar {
    width:100%;
  float:left;
  border-bottom:1px solid #a5a5a5;
  padding:0 0 10px 0px;
  color:#444444;
  font:normal 24px 'Segoe_UI', Arial, Helvetica, sans-serif;
  margin-bottom:1%;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .topBar span {
  color:#919191;
  font:normal 16px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressLeft {
    width:40%;
  float:left;
  color:#444444;
  font:normal 16px/32px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressLeft p {
    width:100%;
  float:left;
  padding:0 0px;
  margin:0 0 10px 0px;
  color:#444444;
  font:normal 16px/32px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressLeft p span {
  color:#5e9d2d;
}
.smsIcon{
  width:46px;
  height:46px;
  display:inline-block;
  background:url(img/smsIcon.png) left center no-repeat;
  float:left;
}
.mailIcon{
  width:46px;
  height:46px;
  display:inline-block;
  background:url(img/mailIcon.png) left center no-repeat;
  float:left;
  margin-left:15px;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight {
  float:right;
  display:inline-block;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight .topStar {
  float:left;
  padding:0 0px;
  margin:0 0 10px 0px;
  color:#5e9d2d;
  font:normal 16px/32px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p {
  float:left;
  padding:0 0px;
  margin:0 0 10px 0px;
  color:#444444;
  font:normal 16px/32px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p a.feedbacknav {
  color:#444444;
  font:normal 16px/32px 'Segoe_UI', Arial, Helvetica, sans-serif;
  text-decoration:underline;
  background:url(img/feedbackIcon.jpg) left top no-repeat;
  padding-left:32px;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p span.officon {
  color:#444444;
  font:normal 16px/32px 'Segoe_UI', Arial, Helvetica, sans-serif;
  background:url(img/offIcon.jpg) left top no-repeat;
  padding-left:32px;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p span.locationicon {
  color:#444444;
  font:normal 16px/32px 'Segoe_UI', Arial, Helvetica, sans-serif;
  background:url(img/map-pin.png) left top no-repeat;
  padding-left:32px;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight .bottomNav {
  float:left;
  padding:0 0px;
  margin:0px 0 0px 0px;
}
.generate-btn, .generate-btn:hover {
  margin:0 3px;
  padding:12px 10px;
  background:$btnBgHover;
  color:$btnFontHover;
  border:none;
  font-weight:600;
  font-size:20px;
  text-shadow: 1px 1px #6e6e6e;
  line-height: 16px;
}
.generate-btn{
  margin:0 3px;
  padding:12px 10px;
  background:$btnBg;
  color:$btnFont;
  border:none;
  font-weight:600;
  font-size:20px;
  text-shadow: 1px 1px #6e6e6e;
  line-height: 16px;
}
.fedback-btn, .fedback-btn:hover {
  margin:0 3px;
  padding:12px 10px!Important;
   background:$btnBgHover;
  color:$btnFontHover;
  border:none;
  font-weight:600;
  font-size:20px!Important;
  text-shadow: 1px 1px #6e6e6e;
}
.fedback-btn {
  margin:0 3px;
  padding:12px 10px!Important;
  background:$btnBg;
  color:$btnFont;
  border:none;
  font-weight:600;
  font-size:20px!Important;
  text-shadow: 1px 1px #6e6e6e;
}
#middleContainer .wecare-leftTop .wecare-generateCoupon {
    background: none repeat scroll 0 0 #f7f6f6;
    border: 1px solid #e0e2e3;
    padding:0% 2% 2% 2%;
  border-top:0px!Important;
}
#middleContainer .wecare-leftTop .wecare-generateCoupon .topBarCode {
    width:100%;
  float:left;
  padding:10px 0px;
  color:#444444;
  font:normal 24px 'Segoe_UI', Arial, Helvetica, sans-serif;
  margin-bottom:1%;
}
#middleContainer .wecare-leftTop .wecare-generateCoupon .topBarCode span {
  color:#444444;
  font:normal 16px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
#middleContainer .wecare-leftTop .wecare-generateCoupon .generatecodePlan {
    width:100%;
  float:left;
}
#middleContainer .wecare-leftTop .wecare-generateCoupon .couponDetailtxt {
    width:100%;
  float:left;
  padding:10px 0px;
  color:#444444;
  font:normal 16px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
.coupontextfield {
  margin:0 1.8% 0px 0;
  padding:11px 15px;
  border:1px solid #e0e2e3;
  font-size:16px;
  display:block;
  width:34%;
  float:left;
}


/*------------------------- My Plans -------------------*/
.topMrgn{
  margin-top:25px;
}
.grayBorder{
  border:1px solid #e0e2e3;
  padding:20px 25px;
}
.grayBorder h2{
  border-bottom:1px solid #a5a5a5;
  padding:8px 0px;
  width:100%;
  color:#666;
  font-size:18px;
  font-weight:600;
  margin:0 0px;
  text-transform:uppercase;
}
.grayBorder .myPlanForm{
  padding:10px 0px 20px 0;
  width:100%;
}

.grayBorder .myPlanForm .myPlanLeft{
  float:left;
}
.grayBorder .myPlanForm .myPlanRight{
  float:right;
}
.grayBorder .myPlanForm .myPlanformBox{
  padding:8px 0px;
  width:49%;
}
.grayBorder .myPlanForm .myPlanformBox label{
  padding:7px 0px;
  width:100%;
  float:left;
  color:#212121;
  font:normal 14px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
.grayBorder .myPlanForm .myPlanformBox label span.greenTxt{
  color:#0f6633;
  font:normal 18px/16px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
.grayBorder .myPlanForm .myPlanformBox .inputBox{
  padding:10px 0px;
  border-radius:2px;
  border:1px solid #e0e2e3;
  float:left;
  width:100%;
  box-shadow:inset 1px 1px 3px #eaeaea;
}
.grayBorder .myPlanForm .myPlanformBox .inputBox .inputBoxIn{
  margin:0px 15px;
  height:25px;
  overflow:hidden;
}
.grayBorder .myPlanForm .myPlanformBox .inputBox .dropBg{
  background:url(img/grayArrow.png) right center no-repeat;
}
.grayBorder .myPlanForm .myPlanformBox .inputBox .dateBg{
  background:url(img/selectDateIcon.png) right center no-repeat;
}
.grayBorder .myPlanForm .myPlanformBox .inputBox .dropBg select{
  background:none;
  border:0px;
  color:#444444;
  
  width:105%;
}
.grayBorder .myPlanForm .myPlanformBox .inputBox .inputBoxIn .txtField{
  margin:0px 0px;
  height:25px;
  width:100%;
  border:0px;
  color:#444444;
  
  background:none;
}
.greenGeneralTxt{
  color:#5e9d2d;
}
.myPlanBtn{
  text-align:right;
  padding-bottom:15px;
  width:100%;
  display:inline-block;
}
.submit-btn:hover {
  margin:0 3px;
 padding:1.5%;
  background:$btnBgHover;
  border:none;
  color:$btnFontHover;
  font-weight:600;
  font-size:20px;
  text-shadow: 1px 1px #6e6e6e;
}
.submit-btn {
  margin:0 3px;
  padding:1.5%;
  background:$btnBg;
  border:none;
  color:$btnFont;
  font-weight:600;
  font-size:20px;
  text-shadow: 1px 1px #6e6e6e;
}
.cancel-btn:hover {
  margin:0 3px;
  padding:1.5%!Important;
  background:$btnBgHover;
  color:$btnFontHover;
  border:none;
  font-weight:600;
  font-size:20px!Important;
  text-shadow: 1px 1px #6e6e6e;
  width:auto!Important;
}
.cancel-btn {
  margin:0 3px;
  padding:1.5%!Important;
  background:$btnBg;
  color:$btnFont;
  border:none;
  font-weight:600;
  font-size:20px!Important;
  text-shadow: 1px 1px #6e6e6e;
  width:auto!Important;
}
.browseGray-btn, .browseGray-btn:hover {
  margin:0 3px;
  padding:1%!Important;
  background:#8a8a8a;
  border:none;
  color:#fff;
  font-weight:600;
  font-size:20px!Important;
  text-shadow: 1px 1px #6e6e6e;
}
.model-input{
  margin:0 0 0 135px;
}
.inputBoxfull{
  padding:10px 0px;
  border-radius:2px;
  border:1px solid #e0e2e3;
  float:left;
  width:74%;
  box-shadow:inset 1px 1px 3px #eaeaea;
  margin-right:5px;
  background:#f9f9f9;
}
.inputBoxfull .inputBoxIn{
  margin:0px 15px;
  height:25px;
  overflow:hidden;
  color:#444444;
  
}
.inputBoxfull .inputBoxIn .txtField{
  margin:0px 0px;
  height:25px;
  width:100%;
  border:0px;
  color:#444444;
  
  background:none;
}
.plusGr-btn, .plusGr-btn:hover {
  margin:0 3px;
  padding:10px 0px 17px 0px;
  background:#5e9d2d;
  border:none;
  color:#fff;
  font-weight:600;
  font-size:34px;
  line-height:20px;
  text-shadow: 1px 1px #6e6e6e;
  text-align:center;
  text-decoration:none;
  width:60px;
}
.colfull{
  width:100%;
  float:left;
}
.colfullBot{
  width:100%;
  float:left;
  border-top:1px solid #e0e2e3;
  margin-top:25px;
}
.eyeIcon{
  width:36px;
  height:24px;
  display:inline-block;
  cursor:pointer;
  background:url(img/eyeIcon.png) center center no-repeat;
  margin-top:5px;
}

.timeIcon{
  width:25px;
  height:24px;
  display:inline-block;
  margin-left:10px;
  cursor:pointer;
  background:url(img/timeIcon.png) center center no-repeat;
}




.grayBorder .myPlanForm .myPlanformBox30per{
  padding:8px 0px;
  width:32%;
}
.grayBorder .myPlanForm .mrgnMyPlan{
  margin-right:2%;
}
.grayBorder .myPlanForm .myPlanformBox30per label{
  padding:7px 0px;
  width:100%;
  float:left;
  color:#666;
  
}
.grayBorder .myPlanForm .myPlanformBox30per label span.greenTxt{
  color:#0f6633;
  
}
.grayBorder .myPlanForm .myPlanformBox30per .inputBox{
  padding:10px 0px;
  border-radius:2px;
  border:1px solid #e0e2e3;
  float:left;
  width:100%;
  box-shadow:inset 1px 1px 3px #eaeaea;
}
.grayBorder .myPlanForm .myPlanformBox30per .inputBoxcolor{
  background:#f9f9f9!Important;
}
.grayBorder .myPlanForm .myPlanformBox30per .inputBox .inputBoxIn{
  margin:0px 15px;
  height:25px;
  overflow:hidden;
  color:#666;

}
.grayBorder .myPlanForm .myPlanformBox30per .inputBox .inputBoxIn .txtField{
  margin:0px 0px;
  height:25px;
  width:100%;
  border:0px;
  color:#666;
  
  background:none;
}



.grayBorder .myPlanForm .myPlanformBox60per{
  padding:8px 0px;
  width:66%;
}
.grayBorder .myPlanForm .myPlanformBox60per label{
  padding:7px 0px;
  width:100%;
  float:left;
  color:#666;
  
}
.grayBorder .myPlanForm .myPlanformBox60per label span.greenTxt{
  color:#0f6633;
  
}
.grayBorder .myPlanForm .myPlanformBox60per .inputBox{
  padding:10px 0px;
  border-radius:2px;
  border:1px solid #e0e2e3;
  float:left;
  width:100%;
  box-shadow:inset 1px 1px 3px #eaeaea;
}
.grayBorder .myPlanForm .myPlanformBox60per .inputBox .inputBoxIn{
  margin:0px 15px;
  height:25px;
  overflow:hidden;
}
.grayBorder .myPlanForm .myPlanformBox60per .inputBox .inputBoxIn .txtField{
  margin:0px 0px;
  height:25px;
  width:100%;
  border:0px;
  color:#666;
  
  background:none;
}
.transFailBox{
  width:100%;
  background:#fbfbfb;
  text-align:center;
  border:1px solid #e0e2e3;
  padding:25px 0px;
  color:#666;
  
}
.transFailBox span{
  color:#5e9d2d;
  
}

.grayBorder .myPlanForm .uploadDocTitle{
  padding:7px 0px;
  width:100%;
  float:left;
  color:#212121;
  
}
.grayBorder .myPlanForm .uploadDocTitle span.greenTxt{
  color:#0f6633;
  
}
.grayBorder .myPlanForm .uploadDocFormBox{
  width:100%;
  float:left;
}
.grayBorder .myPlanForm .uploadDocFormBox .leftCol45{
  width:45%;
  float:left;
}
.grayBorder .myPlanForm .uploadDocFormBox .leftCol45 .inputBox{
  padding:10px 0px;
  border-radius:2px;
  border:1px solid #e0e2e3;
  float:left;
  width:65%;
  box-shadow:inset 1px 1px 3px #eaeaea;
}
.grayBorder .myPlanForm .uploadDocFormBox .leftCol45 .inputBox .inputBoxIn{
  margin:0px 15px;
  height:25px;
  overflow:hidden;
  color:#444444;

}
.grayBorder .myPlanForm .uploadDocFormBox .leftCol45 .inputBox .inputBoxIn .txtField{
  margin:0px 0px;
  height:25px;
  width:100%;
  border:0px;
  color:#444444;
  
  background:none;
}
.grayBorder .myPlanForm .uploadDocFormBox .leftCol45 .cancel-btn, .cancel-btn:hover {
  margin:0 3px;
  padding:1.5%;
  background:$btnBgHover;
  color:$btnFontHover;
  border:none;
  font-weight:600;
  font-size:20px;
  text-shadow: 1px 1px #6e6e6e;
  width:35%;
  text-align:center;
}
.grayBorder .myPlanForm .uploadDocFormBox .leftCol45 .cancel-btn{
  margin:0 3px;
  padding:1.5%;
  background:$btnBg;
  color:$btnFont;
  border:none;
  font-weight:600;
  font-size:20px;
  text-shadow: 1px 1px #6e6e6e;
  width:35%;
  text-align:center;
}
.grayBorder .myPlanForm .uploadDocFormBox .rightCol55{
  width:52%;
  float:right;
}
.grayBorder .myPlanForm .uploadDocFormBox .rightCol55 .inputBox{
  padding:10px 0px;
  border-radius:2px;
  border:1px solid #e0e2e3;
  float:left;
  width:80%;
  box-shadow:inset 1px 1px 3px #eaeaea;
  background:#f9f9f9;
}
.grayBorder .myPlanForm .uploadDocFormBox .rightCol55 .inputBox .inputBoxIn{
  margin:0px 15px;
  height:25px;
  overflow:hidden;
  color:#444444;
  
}
.grayBorder .myPlanForm .uploadDocFormBox .rightCol55 .plus-btn:hover {
  margin:0 3px;
  padding:10px 0px 17px 0px;
  background:$btnBgHover;
  color:$btnFontHover;
  border:none;
  font-weight:600;
  font-size:34px;
  line-height:20px;
  text-shadow: 1px 1px #6e6e6e;
  text-align:center;
  text-decoration:none;
  width:15%;
}
.grayBorder .myPlanForm .uploadDocFormBox .rightCol55 .plus-btn {
  margin:0 3px;
  padding:10px 0px 17px 0px;
  background:$btnBg;
  border:none;
  color:$btnFont;
  font-weight:600;
  font-size:34px;
  line-height:20px;
  text-shadow: 1px 1px #6e6e6e;
  text-align:center;
  text-decoration:none;
  width:15%;
}
.grayBorder .myPlanForm .uploadDocGrayTitle{
  padding:31px 0px 20px 0;
  width:35%;
  float:left;
  color:#444444;

}


/*------------------------- Our Clients -------------------*/
.owl-pagination {
  display:block;
}
.brands .item > img {
  height: auto;
  width: 100%;
  display: block;
  min-height: 75px;
}
.brands .item {
  background-color: #FFFFFF;
  border: 0px solid #e2e4e5;
  border-radius:0px;
}
.brands .item:hover {
  box-shadow: none;
}
.brands .owl-item {
  padding: 0 15px;
}
owl-carousel .owl-wrapper:after {
  content: '.';
  display: block;
  clear: both;
  visibility: hidden;
  line-height: 0;
  height: 0;
}
/* display none until init */
.owl-carousel {
  display: none;
  display: block\9 !important;
  position: relative;
  width: 100%;
  -ms-touch-action: pan-y;
}
.owl-carousel .owl-wrapper {
  display: none;
  display: block\9 !important;
  position: relative;
  -webkit-transform: translate3d(0px, 0px, 0px);
}
.owl-carousel .owl-wrapper-outer {
  overflow: hidden;
  position: relative;
  width: 100%;
}
.owl-carousel .owl-wrapper-outer.autoHeight {
  -webkit-transition: height 500ms ease-in-out;
  -moz-transition: height 500ms ease-in-out;
  -ms-transition: height 500ms ease-in-out;
  -o-transition: height 500ms ease-in-out;
  transition: height 500ms ease-in-out;
}
.owl-carousel .owl-item {
  float: left;
  padding: 0 15px;
}
.owl-controls .owl-page, .owl-controls .owl-buttons div {
  cursor: pointer;
}
.owl-controls {
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}
.owl-carousel .owl-wrapper, .owl-carousel .owl-item {
  -webkit-backface-visibility: hidden;
  -moz-backface-visibility:    hidden;
  -ms-backface-visibility:     hidden;
  -webkit-transform: translate3d(0, 0, 0);
  -moz-transform: translate3d(0, 0, 0);
  -ms-transform: translate3d(0, 0, 0);
}
/*
*   Owl Carousel Owl Demo Theme 
* v1.3.2
*/
/* Clickable class fix problem with hover on touch devices */
/* Use it for non-touch hover action */
.owl-theme .owl-controls.clickable .owl-buttons div:hover {
 
  opacity: 1;
  text-decoration: none;
}
/* Styling Pagination*/
.owl-theme .owl-controls .owl-page {
  display: inline-block;
  zoom: 1;
 *display: inline;/*IE7 life-saver */
}
.owl-theme .owl-controls .owl-page span {
  display: block;
  width: 12px;
  height: 12px;
  margin: 5px 7px;

  -webkit-border-radius: 20px;
  -moz-border-radius: 20px;
  border-radius: 20px;
  background: #869791;
}
.owl-theme .owl-controls .owl-page.active span, .owl-theme .owl-controls.clickable .owl-page:hover span {

}
/* If PaginationNumbers is true */
.owl-theme .owl-controls .owl-page span.owl-numbers {
  height: auto;
  width: auto;
  color: #FFF;
  padding: 2px 10px;
  font-size: 12px;
  -webkit-border-radius: 30px;
  -moz-border-radius: 30px;
  border-radius: 30px;
}
/* preloading images */
.owl-item.loading {
  min-height: 150px;
  background: url(AjaxLoader.gif) no-repeat center center
}
/* Product Slider */

.centerBoxWrapper .owl-theme .owl-controls .owl-buttons, .brands-slider .owl-theme .owl-controls .owl-buttons, .alsoPurchased #alsopurchased_products .owl-controls .owl-buttons, .centerBoxWrapper #featured-slider-inner .owl-buttons {
  position: absolute;
  right: 5px;
  top: -45px;
  z-index: 99;
}
.centerBoxWrapper .owl-theme .owl-controls .owl-buttons .owl-next, .centerBoxWrapper .owl-theme .owl-controls .owl-buttons .owl-prev, .brands-slider .owl-theme .owl-controls .owl-buttons .owl-next, .brands-slider .owl-theme .owl-controls .owl-buttons .owl-prev {
  opacity: 1;
  text-shadow: none;
  position:absolute;
  right:5px;
}
.centerBoxWrapper .owl-theme .owl-controls .owl-buttons .owl-prev, .brands-slider .owl-theme .owl-controls .owl-buttons .owl-prev {
  right: 25px;
}
.centerBoxWrapper .owl-theme .owl-controls .owl-buttons .owl-prev.disabled, .brands-slider .owl-theme .owl-controls .owl-buttons .owl-prev.disabled, .centerBoxWrapper .owl-theme .owl-controls .owl-buttons .owl-next.disabled, .brands-slider .owl-theme .owl-controls .owl-buttons .owl-next.disabled {
  opacity:0.65;
  cursor:default;
}
#Our_clients .owl-prev .fa {
  margin-left: 7px;
  vertical-align: 0;
  display:none;
}
#Our_clients .owl-next .fa {
  margin-left: 9px;
  vertical-align: 0;
  display:none;
}
#Our_clients .owl-prev {
  margin:0;
  padding:0;
  width:23px;
  height:43px;
  background:url(img/recent_project_btn.png) no-repeat scroll left top !important;
  top:73%;
  left:-18px;
  position:absolute;
}
#Our_clients .owl-prev:hover {
  background:url(img/recent_project_btn.png) no-repeat left -47px !important;
}
#Our_clients .owl-next:hover {
  background:url(img/recent_project_btn.png) no-repeat right -47px !important
}
#Our_clients .owl-next {
  margin:0;
  padding:0;
  width:24px;
  height:43px;
  background:url(img/recent_project_btn.png) no-repeat scroll right top !important;
  top:73%;
  right:13px;
  position:absolute;
}
#Our_clients .brands {
  padding:0 15px
}






/*------------------------- Nevigation -------------------*/

.collapse{display:inline\9; visibility:visible\9; float:left\9;}


.header {
  margin:0 15px;
  padding:0;
}
.header .navbar {
  margin:0 0 2% 0;
  padding:0;
  width:100% !important;
  background:#FFF;
  border-bottom:1px solid #e0e2e3 !important;
}
.header .navbar-default {
  border-radius:0;
  border:none;
  box-shadow:none;
  width:100% !important;
  overflow:hidden;
  background:#FFF;
  z-index:99;
}
.header navbar-brand, .header .navbar-nav > li > a {
  text-shadow:none;
  color:$menuFont;
  border-right:1px solid #f0f0f0;
  padding:0 25px !important;
  height:60px;
  line-height:55px;
}
.header .navbar-default .navbar-nav>li>a:hover, .header .navbar-default .navbar-nav>li>a:focus {
  background-color:$menuBgHover;
  color:$menuFontHover;
  border-bottom:7px solid $menuFloorHover;
}
.header .dropdown-menu {
  background-color:#ddeef7;
  color:#312c29;
  padding:0
}
.header .navbar-default .navbar-nav>.open>a, .header .navbar-default .navbar-nav>.active>a {
  background:#fff !important;
  color:#312c29 !important;
}
.header .nav .open>a, .nav .open>a:hover, .header .nav .open>a:focus {
  background:#57b2e4;
  color:#FFF
}
 @media(min-width:768px) {
   
   
   
.addressBox {
 border: 0px solid #dedede;
 margin:0;
 padding: 12px 0;
 width: 100%;
}
 .header .navbar-nav > li:first-child > a {
}
.header .navbar-nav > li > a {
background:$menuBg;
margin:0;
padding:0;
}
.header .navbar-nav > li > a img {
margin:0;
}
.header .navbar-collapse {
padding-left:0;
padding-right:0
}
.header .dropdown-menu>li> a {
border-right:1px solid #57b2e4
}
.header .dropdown-menu>li:first-child>a {
border-top:none
}
.header .dropdown-menu>li>a {
border-top:1px solid #57b2e4
}
header .navbar-nav {
float:left;
margin:0;
padding:0;
}
}
.carousel-caption p {
  font-size:18px;
  color:#5D5D5D;
}
.carousel-caption h1 {
  font-size:40px;
  font-weight:300;
}
@media (max-width:1400px) {
.navbar-brand img {
width:60px;
}
.navbar-nav {
 margin-top:0;
}
}
.btn-default {
  background-color: transparent;
  border-color: #5d5d5d;
  color: #5d5d5d0;
  border-radius:2px;
  padding:8px 15px;
}
.btn-primary {
  background-color: transparent;
  border-color: #fff;
  color: #fff;
  border-radius:2px;
  padding:8px 15px;
}
.btn-primary:hover, .btn-primary:focus {
  background-color: #fff;
  border-color: #fff;
  color: #5d5d5d;
  border-radius:2px;
  padding:8px 15px;
}
#middleContainer {
  margin:0;
  padding:0;
  width:100%;
}
#middleContainer .middlebox {
  margin:0;
  padding:0;
}
#middleContainer .middlebox-left {
  margin:0;
  padding:0;
  background:#FFF;
  margin-bottom:3%;
}
#middleContainer .middlebox-network {
  margin:0;
  padding:3% 1.5%;
  background:#FFF;
  margin-bottom:3%;
  overflow:hidden;
  border:1px solid #e0e2e3;
}
#middleContainer .middlebox-right {
  margin:0;
  padding:0;
}
#middleContainer .middlebox-right .calander {
  margin:0;
  padding:0;
  background:#FFF;
  min-height:250px;
  border:1px solid #e0e2e3;
  margin-bottom:10%;
}
#middleContainer .middlebox-right .calander h1 {
  margin:0;
  padding:10px 0;
  background:$dateBgHover;
  min-height:54px;
  font-size:22px;
  font-weight:300;
  color:$selDateFont;
  text-align:center;
}
#middleContainer .middlebox-right .doctor {
  margin:0;
  padding:0;
  background:#FFF;
  min-height:250px;
  border:1px solid #e0e2e3;
  margin-bottom:10%;
}
#middleContainer .middlebox-right .doctor h1 {
  margin:0;
  padding:20px 0;
  background:$dateBgHover;
 
  font-size:18px;
  font-weight:300;
  color:$selDateFont;
  text-align:center;
}
/*custom css starts by dinesh 28-12-2015 */
#middleContainer .middlebox-right .upcomming {
	margin:0;
	padding:0;
	background:#FFF;
	border-top:1px solid #e0e2e3;
	border-left:1px solid #e0e2e3;
	border-right:1px solid #e0e2e3;
	margin-bottom:10%;
}
.upcomming-scroll{
	overflow-y: auto;
	max-height:281px;
}
#middleContainer .middlebox-right .upcomming h1 {
	margin:0;
	padding:20px 0;
	background:#8a8a8a;
	 
	font-size:18px;
	font-weight:300;
	color:#fff;
	text-align:center;
}
#middleContainer .middlebox-right .doctor-bot {
	margin:0;
	padding:5%;
	overflow:hidden;
}
#middleContainer .middlebox-right .upcomming-events h1 {
	margin:0;
	padding:10px 0;
	background:#8a8a8a;
	min-height:54px;
	font-size:18px;
	font-weight:300;
	color:#fff;
	text-align:center;
}
#middleContainer .middlebox-right .upcomming-events {
	margin:0;
	padding:5%;
	overflow:hidden;
	border-bottom:1px solid #e0e2e3;
}
/*custom css ends by dinesh 28-12-2015 */
#middleContainer .middlebox-right .doctor-sesarch {
  background:url(img/search-icon1.png) no-repeat 3% center;
  padding:10%;
}
#middleContainer .middlebox-right .doctor-map-pin {
  background:url(img/map-pin.png) no-repeat 3% center;
  padding:10%;
}
#middleContainer .middlebox-right .doctor-bot input[type='text'], #middleContainer .middlebox-right .doctor-bot textarea {
  margin:0 0 15px 0;
  padding:5% 5% 5% 12%;
  border:1px solid $border;
  width:100%;
  font-size:16px;
  display:block
}
#middleContainer .middlebox-right .doctor-bot input[type='text']:hover, #middleContainer .middlebox-right .doctor-bot input[type='text']:hover, #middleContainer .middlebox-right .doctor-bot textarea:hover, #middleContainer .middlebox-right .doctor-bot textarea:focus {
  transition: all 0.4s ease-in-out 0s;
  border-color:$borderHover;
}
#middleContainer .middlebox-right .doctor-bot input[type='submit'] {
  margin:0;
  padding:0;
  float:left;
  width:100%;
  padding:5% 5%;
  background:$btnBg;
  border:none;
  color:$btnFont;
  font-weight:600;
  font-size:20px;
  text-shadow: 1px 1px #6e6e6e;
}
#middleContainer .middlebox-right .doctor-bot input[type='submit']:hover {
  background:$btnBgHover;
  transition: all 0.4s ease-in-out 0s;
  color:$btnFontHover;
}
#middleContainer .middlebox-right .claim {
  margin:0;
  padding:0;
  background:#FFF;
  line-height:18px;
  border:1px solid #e0e2e3;
  margin-bottom:10%;
}
#middleContainer .middlebox-right .claim img {
  margin:0;
  padding:0;
  float:left;
}
#middleContainer .middlebox-right .claim p {
  margin:0;
  padding:10px 0;
  font-size:18px;
  color:444;
  margin:5%;
  text-align:right;
}
#middleContainer .middlebox-right .claim a {
  margin:0;
  padding:15px 0;
  font-size:14px;
  color:444;
  text-decoration:none;
}
#middleContainer .middlebox-right .network {
  margin:0;
  padding:0;
  background:#FFF;
  border:1px solid #e0e2e3;
  margin-bottom:0%;
}
#middleContainer .middlebox-right .network-left {
  margin:0;
  padding:5%;
  background:#FFF;
  width:50%;
  font-size:15px;
  color:#444;
  cursor:pointer;
  display:inline-block;
  text-align:center;
}
#middleContainer .middlebox-right .network-left a {
  text-decoration:none;
  cursor:pointer;
}
#middleContainer .middlebox-right .network-left a:hover {
  font-size:15px;
  text-decoration:none;
}
#middleContainer .middlebox-right .network-right {
  margin:0;
  padding:5%;
  background:#FFF;
  width:48%;
  font-size:15px;
  color:#444;
  display:inline-block;
  text-align:center;
}
#middleContainer .middlebox-right .network-right a {
  text-decoration:none;
}
#middleContainer .middlebox-right .network-right a:hover {
  font-size:15px;
  text-decoration:none;
}
#middleContainer .middlebox-right .claim img {
  margin:0;
  padding:0;
  float:left;
}
.navbar-toggle {
	display: none\9;
}
.navbar-nav {
	float:left\9;
	margin-top:0px\9 !important;
}
.nav {
  padding-left: 0;
  margin-bottom: 0;
  list-style: none;
  font-size:16px;
}
.nav>li {
	position: relative;
	display: block;
	display: block\9;
	
}
.nav li {
	position: relative;
	display: block;
	display: inline\9;
	float:left\9;
}
.nav>li>a {
  position: relative;
  display: block;
  padding: 15px 25px;
  background:#fafafa;
  border-right: 1px solid #e6e8e8 !important;
}
.nav>li>a:hover, .nav>li>a:focus {
  text-decoration: none;
  background-color: #8a8a8a;
  color:#FFF;
}
.nav>li.disabled>a {
  color: #777
}
.nav>li.disabled>a:hover, .nav>li.disabled>a:focus {
  color: #777;
  text-decoration: none;
  cursor: not-allowed;
  background-color: transparent
}
.nav .open>a, .nav .open>a:hover, .nav .open>a:focus {
  background-color: #eee;
  border-color: #337ab7
}
.nav .nav-divider {
  height: 1px;
  margin: 9px 0;
  overflow: hidden;
  background-color: #e5e5e5
}
.nav>li>a>img {
  max-width: none
}
.nav-tabs {
  border: 1px solid #e6e8e8;
  background:#fafafa !important;
}
.nav-tabs>li {
  float: left;
  margin-bottom: -1px
}
.nav-tabs>li>a {
  margin-right: 2px;
  line-height: 1.42857143;
  border: 0px solid transparent;
  border-radius: 0px 0px 0 0;
}
.nav-tabs>li>a:hover {
  border-color: #eee #eee #ddd;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus {
  color: #555;
  cursor: default;
  background: #8a8a8a !important;
  border: 1px solid #8a8a8a;
  color:#FFF;
  border-bottom-color: transparent;
}
.nav-tabs>li.active>a:after {
  background:url(img/down-arrow.png) no-repeat center top !important;
  z-index:999 !important;
  position:absolute;
  z-index:999999999999 !important;
}
.stepProgress {
  margin: auto;
  width:100%;
  padding:0;
  overflow:hidden;
  position: relative;
  z-index:9;
}
.stepProgress .stepProgressBar {
  height:2px;
  margin: auto;
  position: relative;
  top: 18px;
  width: 70%;
  background:#8a8a8a;
  z-index:1;
}
.stepProgress .stepProgressIn {
  display: inline-block;
  float: left;
  text-align: center;
  width: 25%;
  position:relative;
  z-index:9999;
}
.stepProgress .stepProgressIn span.stepactive {
  background:$btnBg;
  color:$btnFont;
  border-radius: 20px;
  display: block;
  height: 33px;
  margin:0 auto;
  top: 22px;
  line-height:25px;
  width:33px;
  font-size:18px;
  font-weight:600;
  border:2px solid #a0cc68;
  z-index:9999999 !important;
}
.stepProgress .stepProgressIn span.stepBar {
  background: #8a8a8a;
  border-radius: 20px;
  display: block;
  height: 33px;
  margin:0 auto;
  top: 22px;
  color:#FFF;
  line-height:25px;
  width:33px;
  font-size:18px;
  font-weight:600;
  border:2px solid #8a8a8a;
  z-index: 9999 !important;
}
.stepProgress .stepProgressIn span.stepTxt {
  color: #444;
  display: block;
  
  font-weight:300;
  text-align: center;
  width:100%;
}
.stepBarMobile {
  display: none;
  margin: auto;
  min-width: 300px;
  padding: 10px;
}
.stepBarMobile .stepName {
  color: #444;
  float: left;
  
}
.nav-tabs.nav-justified {
  width: 100%;
  border-bottom: 0
}
.nav-tabs.nav-justified>li {
  float: none
}
.nav-tabs.nav-justified>li>a {
  margin-bottom: 5px;
  text-align: center
}
.nav-tabs.nav-justified>.dropdown .dropdown-menu {
  top: auto;
  left: auto
}
.tab-pane {
  margin: 0;
  padding:0 0px;
  background:#eaeff5;
}
.tab-pane .tab-paneIn {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: #fff none repeat scroll 0 0;
    border-bottom: 1px solid #e0e2e3;
    border-image: none;
    border-left: 1px solid #e0e2e3;
    border-right: 1px solid #e0e2e3;
    border-top: medium none !important;
    color: #000;
    margin: 0;
    padding: 1.500% 1%;
}
.tab-pane p {
  margin: 0;
  padding:2% 1.5%;
  line-height:24px;
  color:#444;

}
.tab-pane .more {
  margin: 0;
  padding:2% 1%;
  line-height:24px;
  font-weight:600;
  color:#66a503;
  text-decoration:none;
}
.tab-pane .more:hover {
  color:#66a503;
  text-decoration:underline;
}
.WebRupee {
  color: #66a503;
  display: inline-block;
  font-family: 'WebRupee';
  font-style: normal;
  margin: 0;
  padding: 0;
}
.WebRupee1 {
  color: #66a503;
  display: inline-block;
  font-family: 'WebRupee';
  font-style: normal;
  margin: 0;
  font-size:24px;
  padding: 0;
}
.tab-pane .tab-left-container {
  margin: 0;
  padding:0;
}
.tab-pane .tab-left-container h1 {
  margin: 0;
  padding:0;
  font-size:24px;
  display:block;
  margin-bottom:3%;
}
.tab-pane .tab-mid-container {
  margin: 0;
  padding:0 1.5%;
}
.insuredQuestioncheckBox {
  width: 100%;
}
.insuredQuestioncheckLeft {
}
.insuredQuestioncheckLeft span {
  color: #000000;
  font: 14px/14px 'SEGOEUIL', Arial, Helvetica, sans-serif;
  padding-left: 20px;
}
.insuredQuestioncheckLeft span strong {
  color: #ff0000;
  font: 18px/22px 'seguisb_0', Arial, Helvetica, sans-serif;
}
.insuredQuestioncheckRight {
  width: 100%;
}
.insuredQuestioncheck {
  text-align: left;
  padding-left:0;
  padding-top:0;
}
.insuredQuestionc-left {
  text-align: left;
  padding-top:20px !important;
  font-size:18px;
  color:#333;
  font-weight:600;
}
 .insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:not(:checked), .insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:checked {
 left: -9999px;
 position: absolute;
}
.insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:not(:checked) + label, .insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:checked + label {
 cursor: pointer;
 padding-left: 25px;
 position: relative;
}
.insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:not(:checked) + label:before, .insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:checked + label:before {
 background: none repeat scroll 0 0 #ffffff;
 border: 1px solid #a5a5a5;
 border-radius: 3px;
 content: '';
 height: 20px;
 left:0;
 position: absolute;
 top:-10px;
 width: 22px;
}
.insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:not(:checked) + label:after, .insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:checked + label:after {
 color: #66a503;
 content: '✔';
 font-size: 24px;
 left:0px;
 font-weight:700;
 position: absolute;
 top: -20px;
}
.insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:not(:checked) + label:after {
 opacity: 0;
 transform: scale(0);
}
.insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:checked + label:after {
  opacity: 1;
  transform: scale(1);
}
.insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:checked:focus + label:before, .insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:focus:not(:checked) + label:before {
}
.insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck label:hover:before {
  border: 1px solid #a5a5a5 !important;
}
.insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:not(:checked), .insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:checked {
 left: -9999px;
 position: absolute;
}
.insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:not(:checked) + label, .insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:checked + label {
 cursor: pointer;
 padding-left: 25px;
 position: relative;
}
.insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:not(:checked) + label:before, .insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:checked + label:before {
 background: none repeat scroll 0 0 #ffffff;
 border: 1px solid #a5a5a5;
 border-radius: 3px;
 content: '';
 height: 15px;
 left: 1px;
 position: absolute;
 top: 0;
 width: 20px;
}
.insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:not(:checked) + label:after, .insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:checked + label:after {
 color: #32764e;
 content: '✔';
 font-size: 23px;
 left: 3px;
 position: absolute;
 top: -5px;
}
.insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:not(:checked) + label:after {
 opacity: 0;
 transform: scale(0);
}
.insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:checked + label:after {
  opacity: 1;
  transform: scale(1);
}
.insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:checked:focus + label:before, .insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:focus:not(:checked) + label:before {
}
.insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob label:hover:before {
  border: 1px solid #a5a5a5 !important;
}
#demo-top-bar {
  text-align: left;
  background: #222;
  position: relative;
  zoom: 1;
  width: 100% !important;
  z-index: 6000;
  padding: 20px 0 20px;
}
#demo-bar-inside {
  width: 960px;
  margin: 0 auto;
  position: relative;
  overflow: hidden;
}
#demo-bar-buttons {
  padding-top: 10px;
  float: right;
}
#demo-bar-buttons a {
  font-size: 12px;
  margin-left: 20px;
  color: white;
  margin: 2px 0;
  text-decoration: none;
}
#demo-bar-buttons a:hover, #demo-bar-buttons a:focus {
  text-decoration: underline;
}
#demo-bar-badge {
  display: inline-block;
  width: 302px;
  padding: 0 !important;
  margin: 0 !important;
  background-color: transparent !important;
}
#demo-bar-badge a {
  display: block;
  width: 100%;
  height: 38px;
  border-radius: 0;
  bottom: auto;
  margin: 0;
  background: url(img/examples-logo.png) no-repeat;
  background-size: 100%;
  overflow: hidden;
  text-indent: -9999px;
}
#demo-bar-badge:before, #demo-bar-badge:after {
  display: none !important;
}
.pad-n {
  padding-left:0;
  padding-right:0;
}
.daty .produ {
  position:relative;
  text-align:center;
  overflow:hidden;
}
.daty .produ img {
  width:100%;
}
.overlay {
  position:absolute;
  padding:15px;
  width:100%;
  height:100%;
  background:rgba(0, 0, 0, 0.4);
  display:none;
  top:0;
  left:0;
  color:#fff;
}
.overlay h1 {
  margin-top:35px;
  font-size:25px;
  font-weight:300;
}
.daty .produ:hover .overlay {
  display:block;
}
.hos-p {
  position:relative;
  display:block;
}
.hos-p img {
  width:100%;
  display:block;
  overflow:hidden;
}
.hos-p .overlay {
  display:block;
  width:50%;
  background:rgba(265, 265, 265, 0.6);
  padding-left:10%;
  color:#5d5d5d;
}
.connect .con-in {
  width:665px;
  padding:25px 0;
  overflow:hidden;
  margin:0 auto;
}
.connect {
  overflow:hidden;
}
.connect .con-in form label {
  font-weight:normal;
  float:left;
  font-size:25px;
  color:#5D5D5D;
}
.connect .con-in form .form-control {
  height:39px;
  line-height:39px;
  width:255px;
  border-color:#5d5d5d;
  font-size:18px;
  font-weight:400;
  float:left;
  margin:0 8px;
}
.connect .con-in form .btn {
  float:left;
}
.connect .con-in .soc {
  float:left;
}
.connect .con-in .soc a {
  color:#5d5d5d;
  line-height:39px;
  font-size:22px;
  padding:5px 0 5px 10px;
}
.connect .con-in .soc a:hover {
  color:#e8242a
}
.footer {
  color:#5f5f5f;
  text-align:left;
  line-height:24px;
  margin:30px 15px 0 15px;
}
.footer p {
  font-size:25px;
  margin:0;
}
.footer p a {
  color:#fff;
}
.footer p span {
  margin-right:15px;
}
.footer p span img {
  margin-top:-5px;
}
.copy {
  font-size:12px;
  padding:20px 0;
  background:#fff;
  margin:0;
  margin-bottom:2%;
  overflow:hidden;
  border-top:1px solid #e0e2e3;
}
.copy a {
  color:#5d5d5d;
  margin:0 0 0 8px;
}
.copy a:hover {
  color:#e8242a
}
.footer .soc {
  float:right;
  padding-top:15px;
}
.footer .soc a {
  color:#5d5d5d;
  font-size:24px;
  font-weight:300 !important;
  padding:0 0 0 10px;
}
.footer .soc a:hover {
  color:#5e9d2d
}
.copy .col-sm-6:last-child {
  text-align:right;
}
.drp-menu {
  position:absolute;
  width:100%;
  z-index:12;
  background:#fff;
}
.drp-menu .inr-menu {
  width:60%;
  margin:0 auto;
  overflow:hidden;
}
.drp-menu .inr-menu ul {
  padding:15px 0 40px;
  margin:0 0 40px;
  background:url(img/menu-devider.jpg) right 15% no-repeat;
}
.drp-menu .inr-menu .col-sm-4:last-child ul {
  background:none;
}
.drp-menu .inr-menu ul li {
  list-style:none;
}
.drp-menu .inr-menu ul li a {
  font-size:18px;
  color:#7D7D7D;
  padding:5px 0;
  display:block;
}
.drp-menu .inr-menu ul li a:hover {
  text-decoration:none;
  color:#000;
}
.drp-menu .inr-menu h4 {
  color:#E8242A;
  font-size:18px;
}
.drp-menu .closee {
  width:100%;
  color:#E8242A;
  font-size:18px;
  padding:15px 60px;
}
.drp-menu .closee a {
  cursor:pointer;
  width:100%;
  color:#E8242A;
  font-size:18px;
}
.login .modal-lg .modal-content {
  overflow:hidden;
  color:#fff;
  min-height:360px;
  padding-bottom:50px;
  background:url(img/login-bg.jpg) center center no-repeat;
  background-size: cover;
}
.login .modal-lg .modal-content h3 {
  font-size:24px;
  margin:70px 0 35px;
}
.login .modal-lg .modal-content p {
  font-size:18px;
}
.login .modal-lg .modal-content p a {
  color:#FFFFFF;
}
.login .modal-lg .modal-content .form-control {
  height:44px;
  line-height:44px;
  padding:0 15px;
  font-size:18px;
}
.login .modal-lg .modal-content .btn-default {
  height:44px;
  line-height:44px;
  padding:0 15px;
  background:#5D5D5D;
  border-left:5px solid #000;
}
.login .close {
  margin:20px 25px;
}
.account .modal-lg .modal-content {
  overflow:hidden;
  color:#5D5D5D;
  min-height:360px;
  background:#E8E8E8;
}
.account .modal-lg .modal-content .modal-header .modal-title {
  font-size:40px;
  margin:20px 0;
  padding-bottom:30px;
  border-bottom:1px solid #B4B4B4;
}
.account .modal-lg .modal-content .modal-title2 {
  font-size:20px;
  margin:0px 0 20px;
}
.account .modal-lg .modal-content label {
  font-size:12px;
  font-weight:normal;
  line-height:44px;
  width:40%;
  padding:0;
  float:left;
}
.account .modal-lg .modal-content p a {
  color:#FFFFFF;
}
.account .modal-lg .modal-content form .set {
  margin-bottom:15px;
  overflow:hidden;
  display:block;
}
.account .modal-lg .modal-content .form-control {
  height:44px;
  border:none;
  background:#fff;
  border-radius:0;
  line-height:44px;
  padding:0 15px;
  font-weight:lighter;
  width:60%;
  float:left;
  font-size:18px;
}
.account .modal-lg .modal-content .btn-default {
  height:44px;
  line-height:44px;
  padding:0 15px;
  background:#5D5D5D;
  border-left:5px solid #000;
}
.account .close {
  margin:5px 0px;
  float:left;
  font-size:12px;
  font-weight:normal;
  margin-bottom:50px;
  display:block;
}
.account .modal-lg .f-btn {
  width:60%;
  float:right;
}
.account .modal-lg .btn-danger {
  font-size:20px;
  padding:8px 25px;
}
.login2 .modal-lg .modal-content {
  overflow:hidden;
  color:#5D5D5D;
  min-height:360px;
  background:#E8E8E8;
  padding-bottom:50px;
}
.login2 .modal-lg .modal-content .modal-header .modal-title {
  font-size:40px;
  margin:20px 0;
  padding-bottom:30px;
  border-bottom:1px solid #B4B4B4;
}
.login2 .modal-lg .modal-content .bdr-r {
  border-right:1px solid #B4B4B4;
}
.login2 .modal-lg .modal-content .modal-title2 {
  font-size:20px;
  margin:0px 0 0px;
}
.login2 .modal-lg .modal-content label {
  font-size:12px;
  font-weight:normal;
  line-height:44px;
  padding:0;
  float:left;
}
.login2 .modal-lg .modal-content p {
  color:#5d5d5d;
  font-size:18px;
  margin-top:35px;
  min-height:205px;
}
.login2 .modal-lg .modal-content form .set {
  margin-bottom:15px;
  overflow:hidden;
  display:block;
}
.login2 .modal-lg .modal-content .form-control {
  height:44px;
  border:none;
  background:#fff;
  border-radius:0;
  line-height:44px;
  padding:0 15px;
  font-weight:lighter;
  width:60%;
  float:left;
  font-size:18px;
}
.login2 .modal-lg .modal-content .btn-default {
  height:44px;
  line-height:44px;
  padding:0 15px;
  background:#5D5D5D;
  border-left:5px solid #000;
}
.login2 .close {
  margin:5px 0px;
  float:left;
  font-size:12px;
  font-weight:normal;
  margin-bottom:50px;
  display:block;
}
.login2 .modal-lg .f-btn {
  float:left;
}
.login2 .modal-lg .fog {
  margin:0px 15px;
  color:#5d5d5d;
  float:left;
  font-size:12px;
  font-weight:normal;
  line-height:44px;
}
.login2 .modal-lg .socc {
  margin-bottom:5px;
}
.login2 .modal-lg .btn-danger {
  font-size:20px;
  padding:8px 25px;
}
 @media (max-width:992px) {
.colfullBot{
  width:100%;
  float:left;
  border-top:0px solid #e0e2e3;
  margin-top:25px;
}
.carousel-caption h1 {
font-size:24px;
}
.carousel-caption p {
font-size:14px;
}
.login .modal-lg .modal-content p {
 font-size: 14px;
}
.login2 .modal-lg .modal-content .modal-header .modal-title {
font-size:24px;
margin:20px 0;
padding-bottom:30px;
border-bottom:1px solid #5d5d5d;
}
.login2 .modal-lg .modal-content .modal-title2 {
font-size:16px;
margin:0px 0 0px;
}
 .account .modal-lg .modal-content .modal-header .modal-title {
font-size:24px;
margin:20px 0;
padding-bottom:30px;
border-bottom:1px solid #5d5d5d;
}
.account .modal-lg .modal-content .modal-title2 {
font-size:16px;
margin:0px 0 20px;
}
.login2 .modal-lg .modal-content p {
font-size:14px;
}
.login .modal-lg .modal-content p {
font-size:14px;
}
.account .modal-lg .modal-content p {
font-size:14px;
}
.navbar-nav > li > a {
 padding: 15px 3px;
}
.overlay h1 {
 font-size: 20px;
 font-weight: 300;
}
.short h1 {
 font-size: 20px;
 font-weight: 300;
 margin-top: -11px;
}
.footer p {
 font-size: 14px;
 margin: 0;
}
.drp-menu .inr-menu {
 margin: 0 auto;
 overflow: hidden;
 width: 100%;
}
}
@media(max-width:768px) {
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressLeft {
    width:100%;
  float:left;
  color:#444444;
  
}
#divID {
	float:left; 
	margin-bottom:5px; 
	color: red;
	font-size:14px;
	margin-top: 7.5%; 
	position: absolute;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight {
  float:left;
  width:100%;
  display:inline-block;
  padding-top:1%;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight .topStar {
  float:left;
  padding:0 0px;
  margin:0 0 10px 0px;
  color:#5e9d2d;
  
  width:100%;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p {
  float:left;
  padding:0 0px;
  margin:0 0 0px 0px;
  color:#444444;
  
  width:100%;
}
.locationtextfield {
  margin:0 1% 1% 0;
  padding:12px 15px 12px 7%;
  border:1px solid #e0e2e3;
  width:100%;
  font-size:16px;
  display:block;
  background:url(img/map-pin.png) no-repeat 3% center;
  width:100%;
  float:left;
  line-height: 22px;
}
.doctorsrchtextfield {
  margin:0 1% 0px 0;
  padding:12px 15px 12px 7%;
  border:1px solid #e0e2e3;
  width:100%;
  font-size:16px;
  display:block;
  background:url(img/search-icon1.png) no-repeat 3% center;
  width:86%;
  float:left;
  line-height: 22px;
}
.graytxtBox-Location {
 background: none repeat scroll 0 0 #fff;
 float: left;
 margin: 0 0 10px;
 padding: 5px 8px;
 width: 100%;
}
 .greenSrchBtn {
 cursor: pointer;
 float: right;
 margin: 0;
 padding: 12px 5px;
 text-indent: -999999px;
 width: 9%;
 background:url(img/srch-icon.png) center center no-repeat $btnBg;
color:$btnFont;
}
.greenSrchBtn:hover {
  background:$btnBgHover;
 color:$btnFontHover;
 background:url(img/srch-icon.png) center center no-repeat $btnBg;
}
 .graytxtBox-Spec {
 float: left;
 margin: 0;
 padding: 5px 8px;
 width: 89%;
}
 .navbar-brand img {
margin:0 0;
}
.navbar-toggle {
top:
}
.login2 .modal-lg .modal-content p {
font-size:14px;
min-height:1px;
}
.navbar-nav {
 margin-top: 0;
}
.navbar-brand {
 margin-top: 0px;
}
.ico-nav li {
float:left;
}
.navbar-nav > li > a {
padding: 8px;
}
 .footer p span, .footer p a {
display:block;
}
.footer p {
 font-size: 14px;
 line-height: 35px;
 margin: 0;
}
.copy {
 font-size: 12px;
 text-align: center;
}
.footer .soc {
float:inherit;
padding-top:15px;
}
.footer .soc a {
color:#5d5d5d;
font-size:24px;
font-weight:300 !important;
padding:0 0 0 10px;
}
.footer .soc a:hover {
color:#e8242a
}
 .footer {
 padding: 20px 0;
 text-align: center;
}
.more {
 color: #66a503;
 font-size: 16px;
 font-weight: 600;
 line-height: 24px;
 margin: 0;
 padding: 2% 2%;
 text-decoration: none;
}
 .stepProgress {
 margin: auto;
 overflow: hidden;
 padding-bottom:5%;
 position: relative;
 width: 100%;
 z-index: 9;
}
.middlebox-left p {
 margin:0 0 7% 0;
 padding:0;
 font-size:14px;
 line-height:24px;
}
 div.cs-select {
 margin-left: 0% !important;
 margin-bottom:0;
 width:100%;
 display:block;
}
}
@media(max-width:1050px) {
.coupontextfield {
  margin:0 1.8% 0px 0;
  padding:11px 15px;
  border:1px solid #e0e2e3;
  font-size:16px;
  display:block;
  width:31%;
  float:left;
}
}
 @media(max-width:600px) {
 .btn-default {
padding:3px 5px
}
 .header navbar-brand, .header .navbar-nav > li > a {
margin-bottom:5px;
}
}
 @media(max-width:640px) {
.coupontextfield {
  margin:0 1.8% 1.8% 0;
  padding:11px 15px;
  border:1px solid #e0e2e3;
  font-size:16px;
  display:block;
  width:100%;
  float:left;
}

#divID {
	float:left; 
	margin-bottom:5px; 
	color: red;
	font-size:14px;
	margin-top: 10.5%; 
	position: absolute;
}
.grayBorder .myPlanForm .uploadDocFormBox .leftCol45{
  width:100%;
  float:left;
  padding:5px 0px;
}
.grayBorder .myPlanForm .uploadDocFormBox .rightCol55{
  width:100%;
  float:left;
  padding:5px 0px;
}
div.cs-select {
 margin-left: 5% !important;
 margin-bottom:0;
}
.stepProgress {
 margin: auto;
 overflow: hidden;
 padding-bottom:5%;
 position: relative;
 width: 100%;
 z-index: 9;
}
.middlebox-left p {
 margin:0 0 7% 0;
 padding:0;
 font-size:14px;
 line-height:24px;
}
 .more {
 color: #66a503;
 font-size: 16px;
 font-weight: 600;
 line-height: 24px;
 margin: 0;
 padding: 2% 2%;
 text-decoration: none;
}
.grayBorder .myPlanForm .myPlanformBox60per{
  padding:8px 0px;
  width:100%;
}
.grayBorder .myPlanForm .myPlanformBox30per{
  padding:8px 0px;
  width:100%;
}
.grayBorder .myPlanForm .mrgnMyPlan{
  margin-right:0%;
}
.grayBorder .myPlanForm .myPlanformBox{
  padding:8px 0px;
  width:100%;
}
.grayBorder .myPlanForm .myPlanRight{
  float:left;
}
.submit-btn:hover {
  margin:0 3px;
  padding:6.5%;
  background:$btnBgHover;
  border:none;
  color:$btnFontHover;
  font-weight:600;
  font-size:17px;
  text-shadow: 1px 1px #6e6e6e;
}
.submit-btn {
  margin:0 3px;
  padding:6.5%;
  background:$btnBg;
  border:none;
  color:$btnFont;
  font-weight:600;
  font-size:17px;
  text-shadow: 1px 1px #6e6e6e;
}
.cancel-btn:hover {
  margin:0 3px!important;
  padding:6.5%!important;
  background:$btnBgHover;
  color:$btnFontHover;
  border:none;
  font-weight:600;
  font-size:17px!important;
  text-shadow: 1px 1px #6e6e6e;
}
.popupBtn{
	padding:14.5% !important;
}
.cancel-btn {
  margin:0 3px!important;
  padding:6.5%!important;
  background:$btnBg;
  color:$btnFont;
  border:none;
  font-weight:600;
  font-size:17px!important;
  text-shadow: 1px 1px #6e6e6e;
}

}
 @media(max-width:1200px) {
#middleContainer .dashboard-left_box1 a{
  font-size:14px !important;
  background: none repeat scroll 0 0 $contBoxBg;
	border: 1px solid $contBoxBorder;
	color: $contBoxFont;
  margin:0;
  font-weight:300;
  display:block;
  cursor:pointer;
  height:131px;
  overflow:hidden;
  text-decoration:none;
  padding:5% 0;
}
.textboldProducts{
	margin-top:10px;
	font-size:14px !important;
}
}




 @media(max-width:360px) {
 
 .showpassword{
        margin-top:10px;
	 }
.model-input{
  margin:10px 0 0 0px;
  float:left;
}

#divID {
	float:left; 
	margin-bottom:5px; 
	color: red;
	font-size:14px;
	margin-top: 25.5% !important; 
	position: absolute;
}
.inputBoxfull {
    border: 1px solid #e0e2e3;
    border-radius: 2px;
    box-shadow: 1px 1px 3px #eaeaea inset;
    float: left;
    margin-right: 5px;
    padding: 10px 0;
    width: 105%;
    margin-top:5%;
}
div.cs-select {
 margin-left: 5% !important;
 margin-bottom:0;
}
 .topContainer .con-in form .form-control {
 width:86% !important;
}
.generate-btn, .generate-btn:hover {
  margin:0 3px 1% 3px;
  padding:12px 10px;
   background:$btnBgHover;
   color:$btnFontHover;
  border:none;
  font-weight:600;
  font-size:16px;
  text-shadow: 1px 1px #6e6e6e;
  line-height: 16px;
}
.generate-btn {
  margin:0 3px 1% 3px;
  padding:12px 10px;
  background:$btnBg;
b color:$btnFont;
  border:none;
  font-weight:600;
  font-size:16px;
  text-shadow: 1px 1px #6e6e6e;
         line-height: 16px;
}

.fedback-btn, .fedback-btn:hover {
     background:$btnBgHover;
    color:$btnFontHover;
    border: medium none;
    font-size: 16px !important;
    font-weight: 600;
    margin: 0 3px;
    padding: 12px 10px !important;
    text-shadow: 1px 1px #6e6e6e;
}
.fedback-btn {
    background:$btnBg;
	color:$btnFont;
    border: medium none;
    font-size: 16px !important;
    font-weight: 600;
    margin: 0 3px;
    padding: 12px 10px !important;
    text-shadow: 1px 1px #6e6e6e;
}
.grayBorder .myPlanForm .uploadDocFormBox .leftCol45 .cancel-btn, .cancel-btn:hover {
  margin:0 3px;
  padding:1.5%;
  background:$btnBgHover;
  color:$btnFontHover;
  border:none;
  font-weight:600;
  font-size:12px;
  text-shadow: 1px 1px #6e6e6e;
  width:35%;
  text-align:center;
}
.grayBorder .myPlanForm .uploadDocFormBox .leftCol45 .cancel-btn {
  margin:0 3px;
  padding:1.5%;
  background:$btnBg;
  color:$btnFont;
  border:none;
  font-weight:600;
  font-size:12px;
  text-shadow: 1px 1px #6e6e6e;
  width:35%;
  text-align:center;
}

#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressLeft p {
    color: #444444;
    float: left;
    font: 14px/24px 'Segoe_UI',Arial,Helvetica,sans-serif;
    margin: 0 0 10px;
    padding: 0;
    width: 100%;
}


#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight .topStar {

    font: 14px/24px 'Segoe_UI',Arial,Helvetica,sans-serif;

}

#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p {

    font: 14px/24px 'Segoe_UI',Arial,Helvetica,sans-serif;

}



#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p span.locationicon {
    font: 14px/24px 'Segoe_UI',Arial,Helvetica,sans-serif;
}



#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p a.feedbacknav {

    font: 14px/24px 'Segoe_UI',Arial,Helvetica,sans-serif;

}


#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p a.feedbacknav {

    font: 14px/24px 'Segoe_UI',Arial,Helvetica,sans-serif;

}

}
 @media(max-width:480px) {
   #middleContainer .dashboard-left h1 {
    color: #2e7d07;
    font-size: 24px;
    margin: 0 0 3%;
    padding: 0;
  text-align:center;
}
   
#divID {
	float:left; 
	margin-bottom:5px; 
	color: red;
	font-size:14px;
	margin-top: 20.5%; 
	position: absolute;
}
   #middleContainer .dashboard-left_box a {
    background: none repeat scroll 0 0 $contBoxBg;
    border: 1px solid $contBoxBorder;
    color: $contBoxFont;
    cursor: pointer;
    display: block;
    font-size: 13px;
    line-height: 24px;
    margin: 0;
    overflow: hidden;
    padding: 5% 0 0 0;
    text-decoration: none;
}
   
   
   .textCenter{text-align:center;}
   .textbold{text-align:center; font-size:16px;}
   
   #middleContainer .dashboard-left {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #e0e2e3;
    margin: 0 -14px 0 -14px;
    overflow: hidden;
    padding: 2% 2% 4%;
}
   
   
   
   
   #middleContainer .dashboard-leftTop {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #e0e2e3;
    margin: 0 -14px 3%;
    padding: 2%;
}

.dashboardName .colSm6 strong {
    font-size: 24px;
    font-weight: 600;
}


.dashboardName .colSm6 {
    float: left;
    font-family: 'Segoe_UI',Arial,Helvetica,sans-serif;
    font-size: 16px;
    line-height: 30px;
    width:100%;
}


#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight .topStar{float:none !important;}
   
   
   
.topContainer-us-icon {
padding-bottom:10px;
 position:absolute;
 z-index: 99;
 margin-left:39% !important;
 float:inherit;
 display:block;
 z-index:99;
 clear:both;
}

.topContainer-us-icon1 {
margin-top:13px !important;
}
   
.locationtextfield {
  margin:1% 1% 2% 0;
  padding:12px 15px 12px 10%;
  border:1px solid #e0e2e3;
  width:100%;
  font-size:16px;
  display:block;
  background:url(img/map-pin.png) no-repeat 3% center;
  width:100%;
  float:left;
  line-height: 22px;
}
.doctorsrchtextfield {
  margin:0 1% 1% 0;
  padding:12px 15px 12px 10%;
  border:1px solid #e0e2e3;
  width:100%;
  font-size:16px;
  display:block;
  background:url(img/search-icon1.png) no-repeat 3% center;
  width:100%;
  float:left;
  line-height: 22px;
}
.hospitalLocationResultMainIn .addressBox {
 padding: 12px 0;
 width: 100%;
 margin: 4px 0;
 border: 1px solid #dedede !important;
}
 selectColorMain {
 background: none repeat scroll 0 0 #e4ffc6 !important;
 overflow:hidden;
}
 .hospitalLocationResultMainIn {
 background: none repeat scroll 0 0 #f9f9f9;
 border: 0px solid #dedede;
 margin: 0;
 overflow: hidden;
}
 #middleContainer .middlebox-network {
 background: none repeat scroll 0 0 #fff;
 border: 1px solid #e0e2e3;
 margin: 0 -15px;
 overflow: hidden;
 padding: 3% 1.5%;
}
 .positionHospital{
  float:left !important;
  margin-bottom:10px;
}
.networkInput {
    margin-top:3% !important;
    width: 100% !important;
}
.forMob{
	float:left !important;
	/*width:100% !important; */
	margin-top: 20px !important;
}
.forMob:hover{
	float:left !important;
	/*width:100% !important; */
	margin-top: 20px !important;
}
.forMobInput{
	margin-bottom:10px !important;
	width:100% !important;
}
div #upload1-error {
    margin-left: -4px !important;
    margin-top: 177px !important;
}
div #upload2-error {
    margin-left: -4px !important;
    margin-top: 177px !important;
}
div #upload3-error {
    margin-left: -4px !important;
    margin-top: 177px !important;
}
div #upload4-error {
    margin-left: -4px !important;
    margin-top: 177px !important;
}
div #upload5-error {
    margin-left: -4px !important;
    margin-top: 177px !important;
}
.uploadDocGrayTitleMob{
	color: #444444 !important;
    float: left !important;
    padding: 31px 0 20px !important;
    width: 103% !important;
	font-size:13px;
	
}



 .graytxtBox-Location {
 background: none repeat scroll 0 0 #fff;
 float: left;
 margin: 0 0 10px;
 padding: 5px 8px;
 width: 100%;
}
 .greenSrchBtn {
 cursor: pointer;
 float: right;
 margin: 2% 0 0 0;
 padding: 12px 5px;
 text-indent: -999999px;
 width: 100%;
 background:url(img/srch-icon.png) center center no-repeat $btnBg;
color:$btnFont;
}
.greenSrchBtn:hover {
  background:$btnBgHover;
 color:$btnFontHover;
 background:url(img/srch-icon.png) center center no-repeat $btnBg;
}
.greenSrchBtnEmail {
    cursor: pointer;
    float: right;
    margin: 2% 0 0;
    width: 100% !important;
}
.greenSrchBtnEmail input[type='submit'] {
  width: 100% !important;
  padding: 4.6% !important;
}
 .graytxtBox-Spec {
 float: left;
 margin: 0;
 padding: 5px 8px;
 width: 100%;
 margin: 0 0 10px;
}
 .stepProgress {
 margin: auto;
 width:100%;
 padding:8% 0;
 display:block;
 z-index:9;
}
 .stepProgress .stepProgressIn span.stepTxt {
 color: #444;
 display: block;
 font: 300 10px 'Segoe_UI', Arial, Helvetica, sans-serif;
 text-align: center;
 width: 100%;
}
 .tab-left-container {
 margin: 0 -10px;
 padding:0;
}
 .tab-left-container h1 {
 margin: 0;
 padding:0;
 font-size:16px !important;
 display:block;
 margin-bottom:3%;
}
 .middlebox-left p {
 margin:0 0 7% 0;
 padding:0;
 font-size:14px;
 line-height:24px;
}
 .topContainer-logo {
 float: inherit;
 height: auto;
 text-align:center;
 margin:0 -15px !important;
 padding: 10px 0 10px 0;
}
 .topContainer .con-in {
 float: inherit;
 margin: 0 -15px;
 display:block;
padding:5px 0;
 overflow:hidden;
}
 .topContainer .con-in1 {
 float: inherit;
margin-top:0;
margin-left:-15px;
 width:108%;
 display:block;
}
 .topContainer .con-in form .form-control {
 width:85.9%;
}
 div.cs-select:before {
 border-left: 1px solid #dfe6ef;
 content: '';
 height: 38px;
 position: absolute;
 right: 0;
 width: 45px;
}
 div.cs-select {
 width: 100% !important;
 z-index: 100;
 margin-left:0 !important;
}
 .topContainer-us-icon {
 margin: 0;
 padding: 0;
 padding-bottom:5%;
 z-index: 9999;
 float:inherit;
 display:block;
 z-index:999;
 clear:both;
}
 .header {
 margin: 0 1%;
 padding: 0;
 margin-top:1.5%;
}
 #middleContainer .middlebox-left {
 background: none repeat scroll 0 0 #fff;
 border: 1px solid #e0e2e3;
 margin: 3% -3% 5% -3% !important;
 padding: 0;
}
 #middleContainer .middlebox-right {
 margin: 5% -3% 0 -3%;
 padding: 0;
}
 #middleContainer .middlebox-right .calander {
 margin: 0 0 5%;
}
 #middleContainer .middlebox-right .doctor {
 margin: 0 0 5%;
}
 #middleContainer .middlebox-right .claim {
 margin: 0 0 5%;
}
 #middleContainer .middlebox-right .network {
 margin: 0 0 5%;
}
.footer {
 padding:0;
 margin:0 1%;
}
 .topContainer-us-icon li {
 display:inline-table;
 list-style: none outside none;
 margin:0 auto;
 display:block;
 padding-left: 5px;
}
 .topContainer-us-icon .dropdown-user {
 left: -160px;
 padding: 10px 15px !important;
 right: 0;
}
 .topContainer-us-icon .dropdown-alerts {
 left: -115px;
 padding: 10px 15px !important;
 right: 0;
}
 .topContainer-us-icon .dropdown-tasks {
 left: -200px;
 padding: 10px 15px !important;
 right: 0;
}
 .navbar-toggle {
 margin-right: 10px !;
}
 .tab-pane .tab-left-container {
 margin: 0;
 padding:0 -15px !important;
}
 .tab-pane .tab-left-container {
 margin: 0 -15px;
 padding:0 -15px !important;
}
.tab-pane .tab-left-container h1 {
 margin: 0;
 padding:0;
 font-size:14px;
 display:block;
 margin-bottom:3%;
}
 .xdsoft_datetimepicker .xdsoft_mounthpicker {
 margin-top: -40px !important;
 position: relative;
 text-align: center;
}
 .xdsoft_datetimepicker .xdsoft_label {
 background-color: #8a8a8a;
 color: #fff;
 cursor: pointer;
 display: inline;
 float: inherit;
 font-size: 22px;
 font-weight: 300;
 line-height: 20px;
 margin: 0;
 padding: 0 3px !important;
 position: relative;
 text-align: center;
 width: 100%;
 z-index: 99;
}
 div.cs-select {
 display: inline-block;
 vertical-align: middle;
 position: relative;
 text-align: left;
 background: #fff;
 z-index: 100;
 width: 100%;
 
 max-width: 500px;
 -webkit-touch-callout: none;
 -webkit-user-select: none;
 -khtml-user-select: none;
 -moz-user-select: none;
 -ms-user-select: none;
 user-select: none;
 border:1px solid #dfe6ef;
 border-radius: 0;
 box-shadow: 0 0 0 rgba(0, 0, 0, 0.075);
 color: #8d8d8d;
 margin-left:0;
 font-size: 14px;
 font-weight: 400;
}
   .topContainer-us-icon-pad {
  margin:0 -15px !important;

}
 #middleContainer .valueaddedBox {
    margin:0 -14px;
  margin-top:-3px;
}
   .img-responsive {
    width: 14%;
}
  
.textCenter {
    text-align: center !important;
}

.textbold p {
    line-height: 24px;
    margin:0;
    text-align: center;
}

#middleContainer .dashboard-left_box IMG {
    text-align: center !important;
}

 .policy-pad{ margin-left:-3% !important; clear:both;}
.spacer{height:20px}



.tab-mid-container-buy a {
  text-align:center;
  display:block;
  width:100%;
}

#middleContainer .wecare-leftTop {
    margin: 0 -5%;
}

}
 @media(max-width:320px) {
   
   #middleContainer .wecare-leftTop {
    margin: 0 -5%;
}
 
#divID {
    color: red;
    float: left;
    font-size: 14px;
    margin-bottom: 5px;
    margin-top: 27.5% !important;
    position: absolute;
}
  .policy-pad{ margin-left:-7% !important;}

 .topContainer-us-icon li a {
    background: #fff none repeat scroll 0 0;
    border-radius: 20px;
    display: block;
    font-size: 24px;
    height: 40px;
    line-height: 40px;
    padding: 0;
    text-align: center;
    width: 30px;
}
 
 #middleContainer .valueaddedBox {
    margin:0 -15px;
    margin-top:-1px;
}

   .img-responsive {
    width: 27%;
}
   
   .topContainer-us-icon1 {
margin-top:0 !important;
}
   
 .stepProgress {
 margin: auto;
 width:100%;
 padding:8% 0;
 display:block;
 z-index:9;
}
 .hospitalLocationSearch {
 /*background: url('img/zigzagHospital.png') no-repeat scroll center bottom rgba(0, 0, 0, 0); */
 float: left;
 padding:15px 0 10px 0;
 width: 100%;
}
 .stepProgress .stepProgressIn span.stepTxt {
 color: #444;
 display: block;
 
 text-align: center;
 width: 100%;
}
 .tab-left-container {
 margin: 0 -15px;
 padding:0;
}
 .tab-left-container h1 {
 margin: 0;
 padding:0;
 font-size:16px !important;
 display:block;
 margin-bottom:3%;
}
 .middlebox-left p {
 margin:0 0 7% 0;
 padding:0;
 font-size:14px;
 line-height:24px;
}
 .topContainer-logo {
 float: inherit;
 height: auto;
 text-align:center;
 margin:0 -15px !important;
 padding: 10px 0 10px 0;
}
 .topContainer .con-in {
 float: inherit;
 margin: 0 -15px;
 display:block;
padding:5px 0;
 overflow:hidden;
}
 .topContainer .con-in1 {
 float: inherit;
 margin:0 0 0 -8%;
 width:116%;
 display:block;
}
 .topContainer .con-in form .form-control {
 width:84% !important;
}
 div.cs-select:before {
 border-left: 1px solid #dfe6ef;
 content: '';
 height: 38px;
 position: absolute;
 right: 0;
 width: 45px;
}
 div.cs-select {
 width: 100% !important;
 z-index: 100;
 margin-left:0 !important;
}


.topContainer-us-icon {
margin-top:13px !important; 
 margin-left:39% !important;

}

 .header {
 margin: 0 0;
 padding: 0;
 margin-top:2%;
}
 #middleContainer .middlebox-left {
 background: none repeat scroll 0 0 #fff;
 border: 1px solid #e0e2e3;
 margin: 3% -6% 5% -6% !important;
 padding: 0;
}
 #middleContainer .middlebox-right {
 margin: 5% -6% 0 -6%;
 padding: 0;
}
 #middleContainer .middlebox-right .calander {
 margin: 0 0 5%;
}
 #middleContainer .middlebox-right .doctor {
 margin: 0 0 5%;
}
 #middleContainer .middlebox-right .claim {
 margin: 0 0 5%;
}
 #middleContainer .middlebox-right .network {
 margin: 0 0 5%;
}
.footer {
 padding:0;
 margin:0 0;
}
 .topContainer-us-icon li {
 display:inline-table;
 list-style: none outside none;
 margin:0 auto;
 display:block;
 padding-left: 5px;
}
 .topContainer-us-icon .dropdown-user {
 left: -160px;
 padding: 10px 15px !important;
 right: 0;
}
 .topContainer-us-icon .dropdown-alerts {
 left: -115px;
 padding: 10px 15px !important;
 right: 0;
}
 .topContainer-us-icon .dropdown-tasks {
 left: -200px;
 padding: 10px 15px !important;
 right: 0;
}
 .navbar-toggle {
 margin-right: 10px !;
}
 .tab-pane .tab-left-container {
 margin: 0;
 padding:0 -15px !important;
}
 .tab-pane .tab-left-container {
 margin: 0 -15px;
 padding:0 -15px !important;
}
.tab-pane .tab-left-container h1 {
 margin: 0;
 padding:0;
 font-size:14px;
 display:block;
 margin-bottom:3%;
}
 .xdsoft_datetimepicker .xdsoft_mounthpicker {
 margin-top: -40px !important;
 position: relative;
 text-align: center;
}
 .xdsoft_datetimepicker .xdsoft_label {
 background-color: #8a8a8a;
 color: #fff;
 cursor: pointer;
 display: inline;
 float: inherit;
 font-size: 22px;
 font-weight: 300;
 line-height: 20px;
 margin: 0;
 padding: 0 3px !important;
 position: relative;
 text-align: center;
 width: 100%;
 z-index: 99;
}

 div.cs-select {
 display: inline-block;
 vertical-align: middle;
 position: relative;
 text-align: left;
 background: #fff;
 z-index: 100;
 width: 100% !important;
 
 max-width: 500px;
 -webkit-touch-callout: none;
 -webkit-user-select: none;
 -khtml-user-select: none;
 -moz-user-select: none;
 -ms-user-select: none;
 user-select: none;
 border:1px solid #dfe6ef;
 border-radius: 0;
 box-shadow: 0 0 0 rgba(0, 0, 0, 0.075);
 color: #8d8d8d;
 margin-left:0;
 font-size: 14px;
 font-weight: 400;
}






#middleContainer .dashboard-left {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #e0e2e3;
    margin:0 -15px;
  margin-bottom:10px;
    padding:2% 2% 4% 2%;
  overflow:hidden;
}

#middleContainer .dashboard-left  img{

    margin:0 auto;
  margin-bottom:0;
    padding:0;
  overflow:hidden;
}

#middleContainer .dashboard-left h1{
    font-size:20px;
  color:#2e7d07;
  text-align:center;
  margin:2% 0 5% 0;
  padding:0;
}
#middleContainer .dashboard-left h2{
    font-size:20px;
  color:#2e7d07;
  margin:2% 0 5% 0;
  padding:0;
  font-style:italic;
  text-align:center;
}

#middleContainer .dashboard-left h2 span{
    font-size:20px;
  color:#444;
  font-style:normal;
}

#middleContainer .dashboard-left1 {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #e0e2e3;
    margin:0 -15px;
  margin-bottom:10px;
    padding:2% 2% 4% 2%;
  overflow:hidden;
}

 .sorting{float:inherit; margin-top:10px; clear:both;}


.spacer{height:20px}

}
 @media(max-width:240px) {
 
 .policy-pad{padding:0 -15px !important; margin-left:-5%; margin-right:-5%; }

 
 .sorting{float:inherit; margin-top:10px; clear:both;}
 .topContainer-us-icon li a {
    background: #fff none repeat scroll 0 0;
    border-radius: 20px;
    display: block;
    font-size: 24px;
    height: 40px;
    line-height: 40px;
    padding: 0;
    text-align: center;
    width: 20px;
}

.topContainer-us-icon {
margin-top:13px !important; 
 margin-left:10% !important;

}
}



.downloadCenter{
  width:100%;
  background:#fbfbfb;
  margin-bottom:6%;
  text-align:center;
  border:1px solid #e0e2e3;
  padding:0 0 3% 0;
  color:#444444;
  overflow:hidden;
  
}
.downloadCenter .top{
  padding:0  0 5% 0;
  margin:0;
  width:98%;
}
.downloadCenter .top a{
  width:100%;
  display:block;
  padding:3% 0;
  text-align:center;
  color:#5e9d2d;
  text-decoration:none;
  
  text-decoration:nonel
}
.downloadCenter .top a:hover{
  width:100%;
  display:block;
  background:#fff;
  text-align:center;
  color:#444;
  
  text-decoration:nonel
}
.downloadCenter .bottom{
  padding:3% 0 5% 0;
  margin:0;
}
.downloadCenter .bottom a{
  text-align:center;
  color:#444;
  border:1px solid #5e9d2d;
  padding:5px 10px;
  text-decoration:none;
  
  text-decoration:nonel
}
.downloadCenter .bottom a:hover{
  background:#5e9d2d;
  text-align:center;
  color:#fff;
  
  text-decoration:nonel
}
.downloadCenter span{
  color:#5e9d2d;
  font:normal 24px/35px 'SEGOEUIL', Arial, Helvetica, sans-serif;
  font-weight:600;
}
.paginationBox {
    color: #000000;
    float: left;
    font: 14px/22px 'Segoe_UI',tahoma,Helvetica,sans-serif;
    padding-bottom: 10px;
    width: 100%;
}
.paginationBox2 {
    color: #000000;
    float: right;
    font: 14px/22px 'Segoe_UI',tahoma,Helvetica,sans-serif;
    padding-bottom: 10px;
}
.digit {
    float: right;
}

.digit ul {
    list-style: outside none none;
    margin: 0;
    padding: 0;
}

.digit ul li {
    border-right: 1px solid #e9e9e9;
    color: #076533;
    display: inline;
    list-style: outside none none;
    margin: 0;
    padding: 0 10px;
}

.digit ul li a {
    color: #000000;
    font: 14px/22px 'Segoe_UI',tahoma,Helvetica,sans-serif;
    text-decoration: none;
}
.fr {
    float: right;
}
#demo{
	padding:2%;
	
}
.bookap{
	
	 margin-top: 6%;
}
.link{
	
	margin-left:0;
	margin-top: 3%;
        display: block;
        width: 100% !important;
      
        test-align:right;
        display: block;
     
}
.item2{
	display: inline-block;
	margin-bottom:3%;
}

@media (min-width:1024px) {
	
	#divID {
	float:left; 
	margin-bottom:5px; 
	color: red;
	font-size:14px;
	margin-top: 7% !important; 
	position: absolute;
}
}

</style>";
?>
