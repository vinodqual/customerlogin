
﻿<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<title>Dashboard</title>
<link rel="icon" type="image/ico" href="img/favicon.png">
<!-- Bootstrap -->
<!--<link href="css/bootstrap.min.css" rel="stylesheet">-->
<style type='text/css'>
    /*!
 * Bootstrap v3.3.1 (http://getbootstrap.com)
 * Copyright 2011-2014 Twitter, Inc.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 *//*! normalize.css v3.0.2 | MIT License | git.io/normalize */html {
  font-family: sans-serif;
  -webkit-text-size-adjust: 100%;
  -ms-text-size-adjust: 100%
}
body {
  margin: 0
}
input:focus,
select:focus,
textarea:focus,
button:focus {
    outline: none;
}
article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary {
  display: block
}
audio, canvas, progress, video {
  display: inline-block;
  vertical-align: baseline
}
audio:not([controls]) {
  display: none;
  height: 0
}
[hidden], template {
display:none
}
a {
  background-color: transparent
}
a:active, a:hover {
  outline: 0
}
abbr[title] {
  border-bottom: 1px dotted
}
b, strong {
  font-weight: 700
}
dfn {
  font-style: italic
}
h1 {
/*  margin: .67em 0;
  font-size: 2em*/
}
mark {
  color: #000;
  background: #ff0
}
small {
  font-size: 80%
}
sub, sup {
  position: relative;
  font-size: 75%;
  line-height: 0;
  vertical-align: baseline
}
sup {
  top: -.5em
}
sub {
  bottom: -.25em
}
img {
  border: 0
}
svg:not(:root) {
  overflow: hidden
}
figure {
  margin: 1em 40px
}
hr {
  height: 0;
  -webkit-box-sizing: content-box;
  -moz-box-sizing: content-box;
  box-sizing: content-box
}
pre {
  overflow: auto
}
code, kbd, pre, samp {
  font-family: monospace, monospace;
  font-size: 1em
}
button, input, optgroup, select, textarea {
  margin: 0;
  font: inherit;
  color: inherit
}
button {
  overflow: visible
}
button, select {
  text-transform: none
}
button, html input[type=button], input[type=reset], input[type=submit] {
  -webkit-appearance: button;
  cursor: pointer
}
button[disabled], html input[disabled] {
  cursor: default
}
button::-moz-focus-inner, input::-moz-focus-inner {
padding:0;
border:0
}
input {
  line-height: normal
}
input[type=checkbox], input[type=radio] {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  padding: 0
}
input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button {
height:auto
}
input[type=search] {
  -webkit-box-sizing: content-box;
  -moz-box-sizing: content-box;
  box-sizing: content-box;
  -webkit-appearance: textfield
}
input[type=search]::-webkit-search-cancel-button, input[type=search]::-webkit-search-decoration {
-webkit-appearance:none
}
fieldset {
  padding: .35em .625em .75em;
  margin: 0 2px;
  border: 1px solid silver
}
legend {
  padding: 0;
  border: 0
}
textarea {
  overflow: auto
}
optgroup {
  font-weight: 700
}
table {
  border-spacing: 0;
  border-collapse: collapse
}
td, th {
  padding: 0
}/*! Source: https://github.com/h5bp/html5-boilerplate/blob/master/src/css/main.css */
@media print {
*, :before, :after {
  color: #000!important;
  text-shadow: none!important;
  background: transparent!important;
  -webkit-box-shadow: none!important;
  box-shadow: none!important
}
a, a:visited {
  text-decoration: underline
}
a[href]:after {
  content: ' (' attr(href) ')'
}
abbr[title]:after {
  content: ' (' attr(title) ')'
}
a[href^='#']:after, a[href^='javascript:']:after {
  content: ''
}
pre, blockquote {
  border: 1px solid #999;
  page-break-inside: avoid
}
thead {
  display: table-header-group
}
tr, img {
  page-break-inside: avoid
}
img {
  max-width: 100%!important
}
p, h2, h3 {
  orphans: 3;
  widows: 3
}
h2, h3 {
  page-break-after: avoid
}
select {
  background: #fff!important
}
.navbar {
  display: none
}
.btn>.caret, .dropup>.btn>.caret {
  border-top-color: #000!important
}
.label {
  border: 1px solid #000
}
.table {
  border-collapse: collapse!important
}
.table td, .table th {
  background-color: #fff!important
}
.table-bordered th, .table-bordered td {
  border: 1px solid #ddd!important
}
}
@font-face {
  font-family: 'Glyphicons Halflings';
  src: url(../fonts/glyphicons-halflings-regular.eot);
  src: url(../fonts/glyphicons-halflings-regular.eot?#iefix) format('embedded-opentype'), url(../fonts/glyphicons-halflings-regular.woff) format('woff'), url(../fonts/glyphicons-halflings-regular.ttf) format('truetype'), url(../fonts/glyphicons-halflings-regular.svg#glyphicons_halflingsregular) format('svg')
}
.glyphicon {
  position: relative;
  top: 1px;
  display: inline-block;
  font-family: 'Glyphicons Halflings';
  font-style: normal;
  font-weight: 400;
  line-height: 1;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale
}
.glyphicon-asterisk:before {
  content: 'a'
}
.glyphicon-plus:before {
  content: 'b'
}
.glyphicon-euro:before, .glyphicon-eur:before {
  content: 'ac'
}
.glyphicon-minus:before {
  content: '�2'
}
.glyphicon-cloud:before {
  content: '�1'
}
.glyphicon-envelope:before {
  content: '�9'
}
.glyphicon-pencil:before {
  content: '�f'
}
.glyphicon-glass:before {
  content: '\e001'
}
.glyphicon-music:before {
  content: '\e002'
}
.glyphicon-search:before {
  content: '\e003'
}
.glyphicon-heart:before {
  content: '\e005'
}
.glyphicon-star:before {
  content: '\e006'
}
.glyphicon-star-empty:before {
  content: '\e007'
}
.glyphicon-user:before {
  content: '\e008'
}
.glyphicon-film:before {
  content: '\e009'
}
.glyphicon-th-large:before {
  content: '\e010'
}
.glyphicon-th:before {
  content: '\e011'
}
.glyphicon-th-list:before {
  content: '\e012'
}
.glyphicon-ok:before {
  content: '\e013'
}
.glyphicon-remove:before {
  content: '\e014'
}
.glyphicon-zoom-in:before {
  content: '\e015'
}
.glyphicon-zoom-out:before {
  content: '\e016'
}
.glyphicon-off:before {
  content: '\e017'
}
.glyphicon-signal:before {
  content: '\e018'
}
.glyphicon-cog:before {
  content: '\e019'
}
.glyphicon-trash:before {
  content: '\e020'
}
.glyphicon-home:before {
  content: '\e021'
}
.glyphicon-file:before {
  content: '\e022'
}
.glyphicon-time:before {
  content: '\e023'
}
.glyphicon-road:before {
  content: '\e024'
}
.glyphicon-download-alt:before {
  content: '\e025'
}
.glyphicon-download:before {
  content: '\e026'
}
.glyphicon-upload:before {
  content: '\e027'
}
.glyphicon-inbox:before {
  content: '\e028'
}
.glyphicon-play-circle:before {
  content: '\e029'
}
.glyphicon-repeat:before {
  content: '\e030'
}
.glyphicon-refresh:before {
  content: '\e031'
}
.glyphicon-list-alt:before {
  content: '\e032'
}
.glyphicon-lock:before {
  content: '\e033'
}
.glyphicon-flag:before {
  content: '\e034'
}
.glyphicon-headphones:before {
  content: '\e035'
}
.glyphicon-volume-off:before {
  content: '\e036'
}
.glyphicon-volume-down:before {
  content: '\e037'
}
.glyphicon-volume-up:before {
  content: '\e038'
}
.glyphicon-qrcode:before {
  content: '\e039'
}
.glyphicon-barcode:before {
  content: '\e040'
}
.glyphicon-tag:before {
  content: '\e041'
}
.glyphicon-tags:before {
  content: '\e042'
}
.glyphicon-book:before {
  content: '\e043'
}
.glyphicon-bookmark:before {
  content: '\e044'
}
.glyphicon-print:before {
  content: '\e045'
}
.glyphicon-camera:before {
  content: '\e046'
}
.glyphicon-font:before {
  content: '\e047'
}
.glyphicon-bold:before {
  content: '\e048'
}
.glyphicon-italic:before {
  content: '\e049'
}
.glyphicon-text-height:before {
  content: '\e050'
}
.glyphicon-text-width:before {
  content: '\e051'
}
.glyphicon-align-left:before {
  content: '\e052'
}
.glyphicon-align-center:before {
  content: '\e053'
}
.glyphicon-align-right:before {
  content: '\e054'
}
.glyphicon-align-justify:before {
  content: '\e055'
}
.glyphicon-list:before {
  content: '\e056'
}
.glyphicon-indent-left:before {
  content: '\e057'
}
.glyphicon-indent-right:before {
  content: '\e058'
}
.glyphicon-facetime-video:before {
  content: '\e059'
}
.glyphicon-picture:before {
  content: '\e060'
}
.glyphicon-map-marker:before {
  content: '\e062'
}
.glyphicon-adjust:before {
  content: '\e063'
}
.glyphicon-tint:before {
  content: '\e064'
}
.glyphicon-edit:before {
  content: '\e065'
}
.glyphicon-share:before {
  content: '\e066'
}
.glyphicon-check:before {
  content: '\e067'
}
.glyphicon-move:before {
  content: '\e068'
}
.glyphicon-step-backward:before {
  content: '\e069'
}
.glyphicon-fast-backward:before {
  content: '\e070'
}
.glyphicon-backward:before {
  content: '\e071'
}
.glyphicon-play:before {
  content: '\e072'
}
.glyphicon-pause:before {
  content: '\e073'
}
.glyphicon-stop:before {
  content: '\e074'
}
.glyphicon-forward:before {
  content: '\e075'
}
.glyphicon-fast-forward:before {
  content: '\e076'
}
.glyphicon-step-forward:before {
  content: '\e077'
}
.glyphicon-eject:before {
  content: '\e078'
}
.glyphicon-chevron-left:before {
  content: '\e079'
}
.glyphicon-chevron-right:before {
  content: '\e080'
}
.glyphicon-plus-sign:before {
  content: '\e081'
}
.glyphicon-minus-sign:before {
  content: '\e082'
}
.glyphicon-remove-sign:before {
  content: '\e083'
}
.glyphicon-ok-sign:before {
  content: '\e084'
}
.glyphicon-question-sign:before {
  content: '\e085'
}
.glyphicon-info-sign:before {
  content: '\e086'
}
.glyphicon-screenshot:before {
  content: '\e087'
}
.glyphicon-remove-circle:before {
  content: '\e088'
}
.glyphicon-ok-circle:before {
  content: '\e089'
}
.glyphicon-ban-circle:before {
  content: '\e090'
}
.glyphicon-arrow-left:before {
  content: '\e091'
}
.glyphicon-arrow-right:before {
  content: '\e092'
}
.glyphicon-arrow-up:before {
  content: '\e093'
}
.glyphicon-arrow-down:before {
  content: '\e094'
}
.glyphicon-share-alt:before {
  content: '\e095'
}
.glyphicon-resize-full:before {
  content: '\e096'
}
.glyphicon-resize-small:before {
  content: '\e097'
}
.glyphicon-exclamation-sign:before {
  content: '\e101'
}
.glyphicon-gift:before {
  content: '\e102'
}
.glyphicon-leaf:before {
  content: '\e103'
}
.glyphicon-fire:before {
  content: '\e104'
}
.glyphicon-eye-open:before {
  content: '\e105'
}
.glyphicon-eye-close:before {
  content: '\e106'
}
.glyphicon-warning-sign:before {
  content: '\e107'
}
.glyphicon-plane:before {
  content: '\e108'
}
.glyphicon-calendar:before {
  content: '\e109'
}
.glyphicon-random:before {
  content: '\e110'
}
.glyphicon-comment:before {
  content: '\e111'
}
.glyphicon-magnet:before {
  content: '\e112'
}
.glyphicon-chevron-up:before {
  content: '\e113'
}
.glyphicon-chevron-down:before {
  content: '\e114'
}
.glyphicon-retweet:before {
  content: '\e115'
}
.glyphicon-shopping-cart:before {
  content: '\e116'
}
.glyphicon-folder-close:before {
  content: '\e117'
}
.glyphicon-folder-open:before {
  content: '\e118'
}
.glyphicon-resize-vertical:before {
  content: '\e119'
}
.glyphicon-resize-horizontal:before {
  content: '\e120'
}
.glyphicon-hdd:before {
  content: '\e121'
}
.glyphicon-bullhorn:before {
  content: '\e122'
}
.glyphicon-bell:before {
  content: '\e123'
}
.glyphicon-certificate:before {
  content: '\e124'
}
.glyphicon-thumbs-up:before {
  content: '\e125'
}
.glyphicon-thumbs-down:before {
  content: '\e126'
}
.glyphicon-hand-right:before {
  content: '\e127'
}
.glyphicon-hand-left:before {
  content: '\e128'
}
.glyphicon-hand-up:before {
  content: '\e129'
}
.glyphicon-hand-down:before {
  content: '\e130'
}
.glyphicon-circle-arrow-right:before {
  content: '\e131'
}
.glyphicon-circle-arrow-left:before {
  content: '\e132'
}
.glyphicon-circle-arrow-up:before {
  content: '\e133'
}
.glyphicon-circle-arrow-down:before {
  content: '\e134'
}
.glyphicon-globe:before {
  content: '\e135'
}
.glyphicon-wrench:before {
  content: '\e136'
}
.glyphicon-tasks:before {
  content: '\e137'
}
.glyphicon-filter:before {
  content: '\e138'
}
.glyphicon-briefcase:before {
  content: '\e139'
}
.glyphicon-fullscreen:before {
  content: '\e140'
}
.glyphicon-dashboard:before {
  content: '\e141'
}
.glyphicon-paperclip:before {
  content: '\e142'
}
.glyphicon-heart-empty:before {
  content: '\e143'
}
.glyphicon-link:before {
  content: '\e144'
}
.glyphicon-phone:before {
  content: '\e145'
}
.glyphicon-pushpin:before {
  content: '\e146'
}
.glyphicon-usd:before {
  content: '\e148'
}
.glyphicon-gbp:before {
  content: '\e149'
}
.glyphicon-sort:before {
  content: '\e150'
}
.glyphicon-sort-by-alphabet:before {
  content: '\e151'
}
.glyphicon-sort-by-alphabet-alt:before {
  content: '\e152'
}
.glyphicon-sort-by-order:before {
  content: '\e153'
}
.glyphicon-sort-by-order-alt:before {
  content: '\e154'
}
.glyphicon-sort-by-attributes:before {
  content: '\e155'
}
.glyphicon-sort-by-attributes-alt:before {
  content: '\e156'
}
.glyphicon-unchecked:before {
  content: '\e157'
}
.glyphicon-expand:before {
  content: '\e158'
}
.glyphicon-collapse-down:before {
  content: '\e159'
}
.glyphicon-collapse-up:before {
  content: '\e160'
}
.glyphicon-log-in:before {
  content: '\e161'
}
.glyphicon-flash:before {
  content: '\e162'
}
.glyphicon-log-out:before {
  content: '\e163'
}
.glyphicon-new-window:before {
  content: '\e164'
}
.glyphicon-record:before {
  content: '\e165'
}
.glyphicon-save:before {
  content: '\e166'
}
.glyphicon-open:before {
  content: '\e167'
}
.glyphicon-saved:before {
  content: '\e168'
}
.glyphicon-import:before {
  content: '\e169'
}
.glyphicon-export:before {
  content: '\e170'
}
.glyphicon-send:before {
  content: '\e171'
}
.glyphicon-floppy-disk:before {
  content: '\e172'
}
.glyphicon-floppy-saved:before {
  content: '\e173'
}
.glyphicon-floppy-remove:before {
  content: '\e174'
}
.glyphicon-floppy-save:before {
  content: '\e175'
}
.glyphicon-floppy-open:before {
  content: '\e176'
}
.glyphicon-credit-card:before {
  content: '\e177'
}
.glyphicon-transfer:before {
  content: '\e178'
}
.glyphicon-cutlery:before {
  content: '\e179'
}
.glyphicon-header:before {
  content: '\e180'
}
.glyphicon-compressed:before {
  content: '\e181'
}
.glyphicon-earphone:before {
  content: '\e182'
}
.glyphicon-phone-alt:before {
  content: '\e183'
}
.glyphicon-tower:before {
  content: '\e184'
}
.glyphicon-stats:before {
  content: '\e185'
}
.glyphicon-sd-video:before {
  content: '\e186'
}
.glyphicon-hd-video:before {
  content: '\e187'
}
.glyphicon-subtitles:before {
  content: '\e188'
}
.glyphicon-sound-stereo:before {
  content: '\e189'
}
.glyphicon-sound-dolby:before {
  content: '\e190'
}
.glyphicon-sound-5-1:before {
  content: '\e191'
}
.glyphicon-sound-6-1:before {
  content: '\e192'
}
.glyphicon-sound-7-1:before {
  content: '\e193'
}
.glyphicon-copyright-mark:before {
  content: '\e194'
}
.glyphicon-registration-mark:before {
  content: '\e195'
}
.glyphicon-cloud-download:before {
  content: '\e197'
}
.glyphicon-cloud-upload:before {
  content: '\e198'
}
.glyphicon-tree-conifer:before {
  content: '\e199'
}
.glyphicon-tree-deciduous:before {
  content: '\e200'
}
* {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box
}
:before, :after {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box
}
html {
  font-size: 10px;
  -webkit-tap-highlight-color: rgba(0,0,0,0)
}
body {
  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-size: 14px;
  line-height: 1.42857143;
  color: #333;
  background-color: #fff
}
input, button, select, textarea {
  font-family: inherit;
  font-size: inherit;
  line-height: inherit
}
a {
  color: ;//#000;
  text-decoration: none
}
a:hover, a:focus {
  color: ;//#5e9d2d;
  text-decoration: underline;
  transition: all 0.5s ease 0s;
}
a:focus {
  outline: thin dotted;
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px
}
figure {
  margin: 0
}
img {
  vertical-align: middle
}
.img-responsive, .thumbnail>img, .thumbnail a>img {
  display: block;
  max-width: 100%;
  height: auto
}

.img-rounded {
  border-radius: 6px
}
.img-thumbnail {
  display: inline-block;
  max-width: 100%;
  height: auto;
  padding: 4px;
  line-height: 1.42857143;
  background-color: #fff;
  border: 1px solid #ddd;
  border-radius: 4px;
  -webkit-transition: all .2s ease-in-out;
  -o-transition: all .2s ease-in-out;
  transition: all .2s ease-in-out
}
.img-circle {
  border-radius: 50%
}
hr {
  margin-top: 20px;
  margin-bottom: 20px;
  border: 0;
  border-top: 1px solid #e6e6e6
}
.sr-only {
  position: absolute;
  width: 1px;
  height: 1px;
  padding: 0;
  margin: -1px;
  overflow: hidden;
  clip: rect(0,0,0,0);
  border: 0
}
.sr-only-focusable:active, .sr-only-focusable:focus {
  position: static;
  width: auto;
  height: auto;
  margin: 0;
  overflow: visible;
  clip: auto
}
h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
  font-family: inherit;
  font-weight: 500;
  line-height: 1.1;
  color: inherit
}
h1 small, h2 small, h3 small, h4 small, h5 small, h6 small, .h1 small, .h2 small, .h3 small, .h4 small, .h5 small, .h6 small, h1 .small, h2 .small, h3 .small, h4 .small, h5 .small, h6 .small, .h1 .small, .h2 .small, .h3 .small, .h4 .small, .h5 .small, .h6 .small {
  font-weight: 400;
  line-height: 1;
  color: #777
}
h1, .h1, h2, .h2, h3, .h3 {
  margin-top: 20px;
  margin-bottom: 10px
}
h1 small, .h1 small, h2 small, .h2 small, h3 small, .h3 small, h1 .small, .h1 .small, h2 .small, .h2 .small, h3 .small, .h3 .small {
  font-size: 65%
}
h4, .h4, h5, .h5, h6, .h6 {
  margin-top: 10px;
  margin-bottom: 10px
}
h4 small, .h4 small, h5 small, .h5 small, h6 small, .h6 small, h4 .small, .h4 .small, h5 .small, .h5 .small, h6 .small, .h6 .small {
  font-size: 75%
}
h1, .h1 {
/*  font-size: 36px*/
}
h2, .h2 {
  font-size: 30px
}
h3, .h3 {
  font-size: 24px
}
h4, .h4 {
  font-size: 18px
}
h5, .h5 {
  font-size: 14px
}
h6, .h6 {
  font-size: 12px
}
p {
  margin: 0 0 10px
}
.lead {
  margin-bottom: 20px;
  font-size: 16px;
  font-weight: 300;
  line-height: 1.4
}
@media (min-width:768px) {
.lead {
  font-size: 21px
}
}
small, .small {
  font-size: 85%
}
mark, .mark {
  padding: .2em;
  background-color: #fcf8e3
}
.text-left {
  text-align: left
}
.text-right {
  text-align: right
}
.text-center {
  text-align: center
}
.text-justify {
  text-align: justify
}
.text-nowrap {
  white-space: nowrap
}
.text-lowercase {
  text-transform: lowercase
}
.text-uppercase {
  text-transform: uppercase
}
.text-capitalize {
  text-transform: capitalize
}
.text-muted {
  color: #777
}
.text-primary {
  color: #337ab7
}
a.text-primary:hover {
  color: #286090
}
.text-success {
  color: #3c763d
}
a.text-success:hover {
  color: #2b542c
}
.text-info {
  color: #31708f
}
a.text-info:hover {
  color: #245269
}
.text-warning {
  color: #8a6d3b
}
a.text-warning:hover {
  color: #66512c
}
.text-danger {
  color: #a94442
}
a.text-danger:hover {
  color: #843534
}
.bg-primary {
  color: #fff;
  background-color: #337ab7
}
a.bg-primary:hover {
  background-color: #286090
}
.bg-success {
  background-color: #dff0d8
}
a.bg-success:hover {
  background-color: #c1e2b3
}
.bg-info {
  background-color: #d9edf7
}
a.bg-info:hover {
  background-color: #afd9ee
}
.bg-warning {
  background-color: #fcf8e3
}
a.bg-warning:hover {
  background-color: #f7ecb5
}
.bg-danger {
  background-color: #f2dede
}
a.bg-danger:hover {
  background-color: #e4b9b9
}
.page-header {
  padding-bottom: 9px;
  margin: 40px 0 20px;
  border-bottom: 1px solid #eee
}
ul, ol {
  margin-top: 0;
  margin-bottom: 10px
}
ul ul, ol ul, ul ol, ol ol {
  margin-bottom: 0
}
.list-unstyled {
  padding-left: 0;
  list-style: none
}
.list-inline {
  padding-left: 0;
  margin-left: -5px;
  list-style: none
}
.list-inline>li {
  display: inline-block;
  padding-right: 5px;
  padding-left: 5px
}
dl {
  margin-top: 0;
  margin-bottom: 20px
}
dt, dd {
  line-height: 1.42857143
}
dt {
  font-weight: 700
}
dd {
  margin-left: 0
}
@media (min-width:768px) {
.dl-horizontal dt {
  float: left;
  width: 160px;
  overflow: hidden;
  clear: left;
  text-align: right;
  text-overflow: ellipsis;
  white-space: nowrap
}
.dl-horizontal dd {
  margin-left: 180px
}
}
abbr[title], abbr[data-original-title] {
  cursor: help;
  border-bottom: 1px dotted #777
}
.initialism {
  font-size: 90%;
  text-transform: uppercase
}
blockquote {
  padding: 10px 20px;
  margin: 0 0 20px;
  font-size: 17.5px;
  border-left: 5px solid #eee
}
blockquote p:last-child, blockquote ul:last-child, blockquote ol:last-child {
  margin-bottom: 0
}
blockquote footer, blockquote small, blockquote .small {
  display: block;
  font-size: 80%;
  line-height: 1.42857143;
  color: #777
}
blockquote footer:before, blockquote small:before, blockquote .small:before {
  content: '�4 A0'
}
.blockquote-reverse, blockquote.pull-right {
  padding-right: 15px;
  padding-left: 0;
  text-align: right;
  border-right: 5px solid #eee;
  border-left: 0
}
.blockquote-reverse footer:before, blockquote.pull-right footer:before, .blockquote-reverse small:before, blockquote.pull-right small:before, .blockquote-reverse .small:before, blockquote.pull-right .small:before {
  content: ''
}
.blockquote-reverse footer:after, blockquote.pull-right footer:after, .blockquote-reverse small:after, blockquote.pull-right small:after, .blockquote-reverse .small:after, blockquote.pull-right .small:after {
  content: 'A0 �4'
}
address {
  margin-bottom: 20px;
  font-style: normal;
  line-height: 1.42857143
}
code, kbd, pre, samp {

}
code {

}
kbd {
  padding: 2px 4px;
  font-size: 90%;
  color: #fff;
  background-color: #333;
  border-radius: 3px;
  -webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.25);
  box-shadow: inset 0 -1px 0 rgba(0,0,0,.25)
}
kbd kbd {
  padding: 0;
  font-size: 100%;
  font-weight: 700;
  -webkit-box-shadow: none;
  box-shadow: none
}
pre {
  display: block;
  padding: 9.5px;
  margin: 0 0 10px;
  font-size: 13px;
  line-height: 1.42857143;
  color: #333;
  word-break: break-all;
  word-wrap: break-word;
  background-color: #f5f5f5;
  border: 1px solid #ccc;
  border-radius: 4px
}
pre code {
  padding: 0;
  font-size: inherit;
  color: inherit;
  white-space: pre-wrap;
  background-color: transparent;
  border-radius: 0
}
.pre-scrollable {
  max-height: 340px;
  overflow-y: scroll
}
.container {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto
}
@media (min-width:768px) {
.container {
  width: 750px
}
}
@media (min-width:992px) {
.container {
  width: 970px
}
}
@media (min-width:1200px) {
.container {
  width: 1170px
}
}
.container-fluid {
  padding-right: 15px;
  padding-left: 15px;
  margin-right: auto;
  margin-left: auto
}
.row {
  margin-right: -15px;
  margin-left: -15px
}
.col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
  position: relative;
  min-height: 1px;
  padding-right: 15px;
  padding-left: 15px
}
.col-xs-1, .col-xs-2, .col-xs-3, .col-xs-4, .col-xs-5, .col-xs-6, .col-xs-7, .col-xs-8, .col-xs-9, .col-xs-10, .col-xs-11, .col-xs-12 {
  float: left
}
.col-xs-12 {
  width: 100%
}
.col-xs-11 {
  width: 91.66666667%
}
.col-xs-10 {
  width: 83.33333333%
}
.col-xs-9 {
  width: 75%
}
.col-xs-8 {
  width: 66.66666667%
}
.col-xs-7 {
  width: 58.33333333%
}
.col-xs-6 {
  width: 50%
}
.col-xs-5 {
  width: 41.66666667%
}
.col-xs-4 {
  width: 33.33333333%
}
.col-xs-3 {
  width: 25%
}
.col-xs-2 {
  width: 16.66666667%
}
.col-xs-1 {
  width: 8.33333333%
}
.col-xs-pull-12 {
  right: 100%
}
.col-xs-pull-11 {
  right: 91.66666667%
}
.col-xs-pull-10 {
  right: 83.33333333%
}
.col-xs-pull-9 {
  right: 75%
}
.col-xs-pull-8 {
  right: 66.66666667%
}
.col-xs-pull-7 {
  right: 58.33333333%
}
.col-xs-pull-6 {
  right: 50%
}
.col-xs-pull-5 {
  right: 41.66666667%
}
.col-xs-pull-4 {
  right: 33.33333333%
}
.col-xs-pull-3 {
  right: 25%
}
.col-xs-pull-2 {
  right: 16.66666667%
}
.col-xs-pull-1 {
  right: 8.33333333%
}
.col-xs-pull-0 {
  right: auto
}
.col-xs-push-12 {
  left: 100%
}
.col-xs-push-11 {
  left: 91.66666667%
}
.col-xs-push-10 {
  left: 83.33333333%
}
.col-xs-push-9 {
  left: 75%
}
.col-xs-push-8 {
  left: 66.66666667%
}
.col-xs-push-7 {
  left: 58.33333333%
}
.col-xs-push-6 {
  left: 50%
}
.col-xs-push-5 {
  left: 41.66666667%
}
.col-xs-push-4 {
  left: 33.33333333%
}
.col-xs-push-3 {
  left: 25%
}
.col-xs-push-2 {
  left: 16.66666667%
}
.col-xs-push-1 {
  left: 8.33333333%
}
.col-xs-push-0 {
  left: auto
}
.col-xs-offset-12 {
  margin-left: 100%
}
.col-xs-offset-11 {
  margin-left: 91.66666667%
}
.col-xs-offset-10 {
  margin-left: 83.33333333%
}
.col-xs-offset-9 {
  margin-left: 75%
}
.col-xs-offset-8 {
  margin-left: 66.66666667%
}
.col-xs-offset-7 {
  margin-left: 58.33333333%
}
.col-xs-offset-6 {
  margin-left: 50%
}
.col-xs-offset-5 {
  margin-left: 41.66666667%
}
.col-xs-offset-4 {
  margin-left: 33.33333333%
}
.col-xs-offset-3 {
  margin-left: 25%
}
.col-xs-offset-2 {
  margin-left: 16.66666667%
}
.col-xs-offset-1 {
  margin-left: 8.33333333%
}
.col-xs-offset-0 {
  margin-left: 0
}
@media (min-width:768px) {
.col-sm-1, .col-sm-2, .col-sm-3, .col-sm-4, .col-sm-5, .col-sm-6, .col-sm-7, .col-sm-8, .col-sm-9, .col-sm-10, .col-sm-11, .col-sm-12 {
  float: left
}
.col-sm-12 {
  width: 100%
}
.col-sm-11 {
  width: 91.66666667%
}
.col-sm-10 {
  width: 83.33333333%
}
.col-sm-9 {
  width: 75%
}
.col-sm-8 {
  width: 66.66666667%
}
.col-sm-7 {
  width: 58.33333333%
}
.col-sm-6 {
  width: 50%
}
.col-sm-5 {
  width: 41.66666667%
}
.col-sm-4 {
  width: 33.33333333%
}
.col-sm-3 {
  width: 25%
}
.col-sm-2 {
  width: 16.66666667%
}
.col-sm-1 {
  width: 8.33333333%
}
.col-sm-pull-12 {
  right: 100%
}
.col-sm-pull-11 {
  right: 91.66666667%
}
.col-sm-pull-10 {
  right: 83.33333333%
}
.col-sm-pull-9 {
  right: 75%
}
.col-sm-pull-8 {
  right: 66.66666667%
}
.col-sm-pull-7 {
  right: 58.33333333%
}
.col-sm-pull-6 {
  right: 50%
}
.col-sm-pull-5 {
  right: 41.66666667%
}
.col-sm-pull-4 {
  right: 33.33333333%
}
.col-sm-pull-3 {
  right: 25%
}
.col-sm-pull-2 {
  right: 16.66666667%
}
.col-sm-pull-1 {
  right: 8.33333333%
}
.col-sm-pull-0 {
  right: auto
}
.col-sm-push-12 {
  left: 100%
}
.col-sm-push-11 {
  left: 91.66666667%
}
.col-sm-push-10 {
  left: 83.33333333%
}
.col-sm-push-9 {
  left: 75%
}
.col-sm-push-8 {
  left: 66.66666667%
}
.col-sm-push-7 {
  left: 58.33333333%
}
.col-sm-push-6 {
  left: 50%
}
.col-sm-push-5 {
  left: 41.66666667%
}
.col-sm-push-4 {
  left: 33.33333333%
}
.col-sm-push-3 {
  left: 25%
}
.col-sm-push-2 {
  left: 16.66666667%
}
.col-sm-push-1 {
  left: 8.33333333%
}
.col-sm-push-0 {
  left: auto
}
.col-sm-offset-12 {
  margin-left: 100%
}
.col-sm-offset-11 {
  margin-left: 91.66666667%
}
.col-sm-offset-10 {
  margin-left: 83.33333333%
}
.col-sm-offset-9 {
  margin-left: 75%
}
.col-sm-offset-8 {
  margin-left: 66.66666667%
}
.col-sm-offset-7 {
  margin-left: 58.33333333%
}
.col-sm-offset-6 {
  margin-left: 50%
}
.col-sm-offset-5 {
  margin-left: 41.66666667%
}
.col-sm-offset-4 {
  margin-left: 33.33333333%
}
.col-sm-offset-3 {
  margin-left: 25%
}
.col-sm-offset-2 {
  margin-left: 16.66666667%
}
.col-sm-offset-1 {
  margin-left: 8.33333333%
}
.col-sm-offset-0 {
  margin-left: 0
}
}
@media (min-width:992px) {
.col-md-1, .col-md-2, .col-md-3, .col-md-4, .col-md-5, .col-md-6, .col-md-7, .col-md-8, .col-md-9, .col-md-10, .col-md-11, .col-md-12 {
  float: left
}
.col-md-12 {
  width: 100%
}
.col-md-11 {
  width: 91.66666667%
}
.col-md-10 {
  width: 83.33333333%
}
.col-md-9 {
  width: 75%
}
.col-md-8 {
  width: 66.66666667%
}
.col-md-7 {
  width: 58.33333333%
}
.col-md-6 {
  width: 50%
}
.col-md-5 {
  width: 41.66666667%
}
.col-md-4 {
  width: 33.33333333%
}
.col-md-3 {
  width: 25%
}
.col-md-2 {
  width: 16.66666667%
}
.col-md-1 {
  width: 8.33333333%
}
.col-md-pull-12 {
  right: 100%
}
.col-md-pull-11 {
  right: 91.66666667%
}
.col-md-pull-10 {
  right: 83.33333333%
}
.col-md-pull-9 {
  right: 75%
}
.col-md-pull-8 {
  right: 66.66666667%
}
.col-md-pull-7 {
  right: 58.33333333%
}
.col-md-pull-6 {
  right: 50%
}
.col-md-pull-5 {
  right: 41.66666667%
}
.col-md-pull-4 {
  right: 33.33333333%
}
.col-md-pull-3 {
  right: 25%
}
.col-md-pull-2 {
  right: 16.66666667%
}
.col-md-pull-1 {
  right: 8.33333333%
}
.col-md-pull-0 {
  right: auto
}
.col-md-push-12 {
  left: 100%
}
.col-md-push-11 {
  left: 91.66666667%
}
.col-md-push-10 {
  left: 83.33333333%
}
.col-md-push-9 {
  left: 75%
}
.col-md-push-8 {
  left: 66.66666667%
}
.col-md-push-7 {
  left: 58.33333333%
}
.col-md-push-6 {
  left: 50%
}
.col-md-push-5 {
  left: 41.66666667%
}
.col-md-push-4 {
  left: 33.33333333%
}
.col-md-push-3 {
  left: 25%
}
.col-md-push-2 {
  left: 16.66666667%
}
.col-md-push-1 {
  left: 8.33333333%
}
.col-md-push-0 {
  left: auto
}
.col-md-offset-12 {
  margin-left: 100%
}
.col-md-offset-11 {
  margin-left: 91.66666667%
}
.col-md-offset-10 {
  margin-left: 83.33333333%
}
.col-md-offset-9 {
  margin-left: 75%
}
.col-md-offset-8 {
  margin-left: 66.66666667%
}
.col-md-offset-7 {
  margin-left: 58.33333333%
}
.col-md-offset-6 {
  margin-left: 50%
}
.col-md-offset-5 {
  margin-left: 41.66666667%
}
.col-md-offset-4 {
  margin-left: 33.33333333%
}
.col-md-offset-3 {
  margin-left: 25%
}
.col-md-offset-2 {
  margin-left: 16.66666667%
}
.col-md-offset-1 {
  margin-left: 8.33333333%
}
.col-md-offset-0 {
  margin-left: 0
}
}
@media (min-width:1200px) {
.col-lg-1, .col-lg-2, .col-lg-3, .col-lg-4, .col-lg-5, .col-lg-6, .col-lg-7, .col-lg-8, .col-lg-9, .col-lg-10, .col-lg-11, .col-lg-12 {
  float: left
}
.col-lg-12 {
  width: 100%
}
.col-lg-11 {
  width: 91.66666667%
}
.col-lg-10 {
  width: 83.33333333%
}
.col-lg-9 {
  width: 75%
}
.col-lg-8 {
  width: 66.66666667%
}
.col-lg-7 {
  width: 58.33333333%
}
.col-lg-6 {
  width: 50%
}
.col-lg-5 {
  width: 41.66666667%
}
.col-lg-4 {
  width: 33.33333333%
}
.col-lg-3 {
  width: 25%
}
.col-lg-2 {
  width: 16.66666667%
}
.col-lg-1 {
  width: 8.33333333%
}
.col-lg-pull-12 {
  right: 100%
}
.col-lg-pull-11 {
  right: 91.66666667%
}
.col-lg-pull-10 {
  right: 83.33333333%
}
.col-lg-pull-9 {
  right: 75%
}
.col-lg-pull-8 {
  right: 66.66666667%
}
.col-lg-pull-7 {
  right: 58.33333333%
}
.col-lg-pull-6 {
  right: 50%
}
.col-lg-pull-5 {
  right: 41.66666667%
}
.col-lg-pull-4 {
  right: 33.33333333%
}
.col-lg-pull-3 {
  right: 25%
}
.col-lg-pull-2 {
  right: 16.66666667%
}
.col-lg-pull-1 {
  right: 8.33333333%
}
.col-lg-pull-0 {
  right: auto
}
.col-lg-push-12 {
  left: 100%
}
.col-lg-push-11 {
  left: 91.66666667%
}
.col-lg-push-10 {
  left: 83.33333333%
}
.col-lg-push-9 {
  left: 75%
}
.col-lg-push-8 {
  left: 66.66666667%
}
.col-lg-push-7 {
  left: 58.33333333%
}
.col-lg-push-6 {
  left: 50%
}
.col-lg-push-5 {
  left: 41.66666667%
}
.col-lg-push-4 {
  left: 33.33333333%
}
.col-lg-push-3 {
  left: 25%
}
.col-lg-push-2 {
  left: 16.66666667%
}
.col-lg-push-1 {
  left: 8.33333333%
}
.col-lg-push-0 {
  left: auto
}
.col-lg-offset-12 {
  margin-left: 100%
}
.col-lg-offset-11 {
  margin-left: 91.66666667%
}
.col-lg-offset-10 {
  margin-left: 83.33333333%
}
.col-lg-offset-9 {
  margin-left: 75%
}
.col-lg-offset-8 {
  margin-left: 66.66666667%
}
.col-lg-offset-7 {
  margin-left: 58.33333333%
}
.col-lg-offset-6 {
  margin-left: 50%
}
.col-lg-offset-5 {
  margin-left: 41.66666667%
}
.col-lg-offset-4 {
  margin-left: 33.33333333%
}
.col-lg-offset-3 {
  margin-left: 25%
}
.col-lg-offset-2 {
  margin-left: 16.66666667%
}
.col-lg-offset-1 {
  margin-left: 8.33333333%
}
.col-lg-offset-0 {
  margin-left: 0
}
}
table {
  background-color: transparent
}
caption {
  padding-top: 8px;
  padding-bottom: 8px;
  color: #777;
  text-align: left
}
th {
  text-align: left
}
.table {
  width: 100%;
  max-width: 100%;
  margin-bottom: 20px
}
.table>thead>tr>th, .table>tbody>tr>th, .table>tfoot>tr>th, .table>thead>tr>td, .table>tbody>tr>td, .table>tfoot>tr>td {
  padding: 8px;
  line-height: 1.42857143;
  vertical-align: top;
  border-top: 1px solid #ddd
}
.table>thead>tr>th {
  vertical-align: bottom;
  border-bottom: 2px solid #ddd
}
.table>caption+thead>tr:first-child>th, .table>colgroup+thead>tr:first-child>th, .table>thead:first-child>tr:first-child>th, .table>caption+thead>tr:first-child>td, .table>colgroup+thead>tr:first-child>td, .table>thead:first-child>tr:first-child>td {
  border-top: 0
}
.table>tbody+tbody {
  border-top: 2px solid #ddd
}
.table .table {
  background-color: #fff
}
.table-condensed>thead>tr>th, .table-condensed>tbody>tr>th, .table-condensed>tfoot>tr>th, .table-condensed>thead>tr>td, .table-condensed>tbody>tr>td, .table-condensed>tfoot>tr>td {
  padding: 5px
}
.table-bordered {
  border: 1px solid #ddd
}
.table-bordered>thead>tr>th, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>tbody>tr>td, .table-bordered>tfoot>tr>td {
  border: 1px solid #ddd
}
.table-bordered>thead>tr>th, .table-bordered>thead>tr>td {
  border-bottom-width: 2px
}
.table-striped>tbody>tr:nth-child(odd) {
  background-color: #f9f9f9
}
.table-hover>tbody>tr:hover {
  background-color: #f5f5f5
}
table col[class*=col-] {
  position: static;
  display: table-column;
  float: none
}
table td[class*=col-], table th[class*=col-] {
  position: static;
  display: table-cell;
  float: none
}
.table>thead>tr>td.active, .table>tbody>tr>td.active, .table>tfoot>tr>td.active, .table>thead>tr>th.active, .table>tbody>tr>th.active, .table>tfoot>tr>th.active, .table>thead>tr.active>td, .table>tbody>tr.active>td, .table>tfoot>tr.active>td, .table>thead>tr.active>th, .table>tbody>tr.active>th, .table>tfoot>tr.active>th {
  background-color: #f5f5f5
}
.table-hover>tbody>tr>td.active:hover, .table-hover>tbody>tr>th.active:hover, .table-hover>tbody>tr.active:hover>td, .table-hover>tbody>tr:hover>.active, .table-hover>tbody>tr.active:hover>th {
  background-color: #e8e8e8
}
.table>thead>tr>td.success, .table>tbody>tr>td.success, .table>tfoot>tr>td.success, .table>thead>tr>th.success, .table>tbody>tr>th.success, .table>tfoot>tr>th.success, .table>thead>tr.success>td, .table>tbody>tr.success>td, .table>tfoot>tr.success>td, .table>thead>tr.success>th, .table>tbody>tr.success>th, .table>tfoot>tr.success>th {
  background-color: #dff0d8
}
.table-hover>tbody>tr>td.success:hover, .table-hover>tbody>tr>th.success:hover, .table-hover>tbody>tr.success:hover>td, .table-hover>tbody>tr:hover>.success, .table-hover>tbody>tr.success:hover>th {
  background-color: #d0e9c6
}
.table>thead>tr>td.info, .table>tbody>tr>td.info, .table>tfoot>tr>td.info, .table>thead>tr>th.info, .table>tbody>tr>th.info, .table>tfoot>tr>th.info, .table>thead>tr.info>td, .table>tbody>tr.info>td, .table>tfoot>tr.info>td, .table>thead>tr.info>th, .table>tbody>tr.info>th, .table>tfoot>tr.info>th {
  background-color: #d9edf7
}
.table-hover>tbody>tr>td.info:hover, .table-hover>tbody>tr>th.info:hover, .table-hover>tbody>tr.info:hover>td, .table-hover>tbody>tr:hover>.info, .table-hover>tbody>tr.info:hover>th {
  background-color: #c4e3f3
}
.table>thead>tr>td.warning, .table>tbody>tr>td.warning, .table>tfoot>tr>td.warning, .table>thead>tr>th.warning, .table>tbody>tr>th.warning, .table>tfoot>tr>th.warning, .table>thead>tr.warning>td, .table>tbody>tr.warning>td, .table>tfoot>tr.warning>td, .table>thead>tr.warning>th, .table>tbody>tr.warning>th, .table>tfoot>tr.warning>th {
  background-color: #fcf8e3
}
.table-hover>tbody>tr>td.warning:hover, .table-hover>tbody>tr>th.warning:hover, .table-hover>tbody>tr.warning:hover>td, .table-hover>tbody>tr:hover>.warning, .table-hover>tbody>tr.warning:hover>th {
  background-color: #faf2cc
}
.table>thead>tr>td.danger, .table>tbody>tr>td.danger, .table>tfoot>tr>td.danger, .table>thead>tr>th.danger, .table>tbody>tr>th.danger, .table>tfoot>tr>th.danger, .table>thead>tr.danger>td, .table>tbody>tr.danger>td, .table>tfoot>tr.danger>td, .table>thead>tr.danger>th, .table>tbody>tr.danger>th, .table>tfoot>tr.danger>th {
  background-color: #f2dede
}
.table-hover>tbody>tr>td.danger:hover, .table-hover>tbody>tr>th.danger:hover, .table-hover>tbody>tr.danger:hover>td, .table-hover>tbody>tr:hover>.danger, .table-hover>tbody>tr.danger:hover>th {
  background-color: #ebcccc
}
.table-responsive {
  min-height: .01%;
  overflow-x: auto
}
@media screen and (max-width:767px) {
.table-responsive {
  width: 100%;
  margin-bottom: 15px;
  overflow-y: hidden;
  -ms-overflow-style: -ms-autohiding-scrollbar;
  border: 1px solid #ddd
}
.table-responsive>.table {
  margin-bottom: 0
}
.table-responsive>.table>thead>tr>th, .table-responsive>.table>tbody>tr>th, .table-responsive>.table>tfoot>tr>th, .table-responsive>.table>thead>tr>td, .table-responsive>.table>tbody>tr>td, .table-responsive>.table>tfoot>tr>td {
  white-space: nowrap
}
.table-responsive>.table-bordered {
  border: 0
}
.table-responsive>.table-bordered>thead>tr>th:first-child, .table-responsive>.table-bordered>tbody>tr>th:first-child, .table-responsive>.table-bordered>tfoot>tr>th:first-child, .table-responsive>.table-bordered>thead>tr>td:first-child, .table-responsive>.table-bordered>tbody>tr>td:first-child, .table-responsive>.table-bordered>tfoot>tr>td:first-child {
  border-left: 0
}
.table-responsive>.table-bordered>thead>tr>th:last-child, .table-responsive>.table-bordered>tbody>tr>th:last-child, .table-responsive>.table-bordered>tfoot>tr>th:last-child, .table-responsive>.table-bordered>thead>tr>td:last-child, .table-responsive>.table-bordered>tbody>tr>td:last-child, .table-responsive>.table-bordered>tfoot>tr>td:last-child {
  border-right: 0
}
.table-responsive>.table-bordered>tbody>tr:last-child>th, .table-responsive>.table-bordered>tfoot>tr:last-child>th, .table-responsive>.table-bordered>tbody>tr:last-child>td, .table-responsive>.table-bordered>tfoot>tr:last-child>td {
  border-bottom: 0
}
.ico-nav{margin-top:0 !important}
}
fieldset {
  min-width: 0;
  padding: 0;
  margin: 0;
  border: 0
}
legend {
  display: block;
  width: 100%;
  padding: 0;
  margin-bottom: 20px;
  font-size: 21px;
  line-height: inherit;
  color: #333;
  border: 0;
  border-bottom: 1px solid #e5e5e5
}
label {
  display: inline-block;
  max-width: 100%;
  margin-bottom: 5px;
  font-weight: 300;
}
input[type=search] {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box
}
input[type=radio], input[type=checkbox] {
  margin: 4px 0 0;
  margin-top: 1px \9;
  line-height: normal
}
input[type=file] {
  display: block
}
input[type=range] {
  display: block;
  width: 100%
}
select[multiple], select[size] {
  height: auto
}
input[type=file]:focus, input[type=radio]:focus, input[type=checkbox]:focus {
  outline: thin dotted;
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px
}
output {
  display: block;
  padding-top: 7px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555
}
.form-control {
  display: block;
  width: 100%;
  height: 34px;
  padding: 6px 12px;
  font-size: 14px;
  line-height: 1.42857143;
  color: #555;
  background-color: #fff;
  background-image: none;
  border: 1px solid #ccc;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  -webkit-transition: border-color ease-in-out .15s, -webkit-box-shadow ease-in-out .15s;
  -o-transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s;
  transition: border-color ease-in-out .15s, box-shadow ease-in-out .15s
}
.form-control:focus {
  border-color: #66afe9;
  outline: 0;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6);
  box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 8px rgba(102,175,233,.6)
}
.form-control::-moz-placeholder {
color:#999;
opacity:1
}
.form-control:-ms-input-placeholder {
color:#999
}
.form-control::-webkit-input-placeholder {
color:#999
}
.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
  cursor: not-allowed;
  background-color: #eee;
  opacity: 1
}
textarea.form-control {
  height: auto
}
input[type=search] {
  -webkit-appearance: none
}
@media screen and (-webkit-min-device-pixel-ratio:0) {
input[type=date], input[type=time], input[type=datetime-local], input[type=month] {
  line-height: 34px
}
input[type=date].input-sm, input[type=time].input-sm, input[type=datetime-local].input-sm, input[type=month].input-sm {
  line-height: 30px
}
input[type=date].input-lg, input[type=time].input-lg, input[type=datetime-local].input-lg, input[type=month].input-lg {
  line-height: 46px
}
}
.form-group {
  margin-bottom: 15px
}
.radio, .checkbox {
  position: relative;
  display: block;
  margin-top: 10px;
  margin-bottom: 10px
}
.radio label, .checkbox label {
  min-height: 20px;
  padding-left: 20px;
  margin-bottom: 0;
  font-weight: 400;
  cursor: pointer
}
.radio input[type=radio], .radio-inline input[type=radio], .checkbox input[type=checkbox], .checkbox-inline input[type=checkbox] {
  position: absolute;
  margin-top: 4px \9;
  margin-left: -20px
}
.radio+.radio, .checkbox+.checkbox {
  margin-top: -5px
}
.radio-inline, .checkbox-inline {
  display: inline-block;
  padding-left: 20px;
  margin-bottom: 0;
  font-weight: 400;
  vertical-align: middle;
  cursor: pointer
}
.radio-inline+.radio-inline, .checkbox-inline+.checkbox-inline {
  margin-top: 0;
  margin-left: 10px
}
input[type=radio][disabled], input[type=checkbox][disabled], input[type=radio].disabled, input[type=checkbox].disabled, fieldset[disabled] input[type=radio], fieldset[disabled] input[type=checkbox] {
  cursor: not-allowed
}
.radio-inline.disabled, .checkbox-inline.disabled, fieldset[disabled] .radio-inline, fieldset[disabled] .checkbox-inline {
  cursor: not-allowed
}
.radio.disabled label, .checkbox.disabled label, fieldset[disabled] .radio label, fieldset[disabled] .checkbox label {
  cursor: not-allowed
}
.form-control-static {
  padding-top: 7px;
  padding-bottom: 7px;
  margin-bottom: 0
}
.form-control-static.input-lg, .form-control-static.input-sm {
  padding-right: 0;
  padding-left: 0
}
.input-sm, .form-group-sm .form-control {
  height: 30px;
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
  border-radius: 3px
}
select.input-sm, select.form-group-sm .form-control {
  height: 30px;
  line-height: 30px
}
textarea.input-sm, textarea.form-group-sm .form-control, select[multiple].input-sm, select[multiple].form-group-sm .form-control {
  height: auto
}
.input-lg, .form-group-lg .form-control {
  height: 46px;
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.33;
  border-radius: 6px
}
select.input-lg, select.form-group-lg .form-control {
  height: 46px;
  line-height: 46px
}
textarea.input-lg, textarea.form-group-lg .form-control, select[multiple].input-lg, select[multiple].form-group-lg .form-control {
  height: auto
}
.has-feedback {
  position: relative
}
.has-feedback .form-control {
  padding-right: 42.5px
}
.form-control-feedback {
  position: absolute;
  top: 0;
  right: 0;
  z-index: 2;
  display: block;
  width: 34px;
  height: 34px;
  line-height: 34px;
  text-align: center;
  pointer-events: none
}
.input-lg+.form-control-feedback {
  width: 46px;
  height: 46px;
  line-height: 46px
}
.input-sm+.form-control-feedback {
  width: 30px;
  height: 30px;
  line-height: 30px
}
.has-success .help-block, .has-success .control-label, .has-success .radio, .has-success .checkbox, .has-success .radio-inline, .has-success .checkbox-inline, .has-success.radio label, .has-success.checkbox label, .has-success.radio-inline label, .has-success.checkbox-inline label {
  color: #3c763d
}
.has-success .form-control {
  border-color: #3c763d;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  box-shadow: inset 0 1px 1px rgba(0,0,0,.075)
}
.has-success .form-control:focus {
  border-color: #2b542c;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 6px #67b168;
  box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 6px #67b168
}
.has-success .input-group-addon {
  color: #3c763d;
  background-color: #dff0d8;
  border-color: #3c763d
}
.has-success .form-control-feedback {
  color: #3c763d
}
.has-warning .help-block, .has-warning .control-label, .has-warning .radio, .has-warning .checkbox, .has-warning .radio-inline, .has-warning .checkbox-inline, .has-warning.radio label, .has-warning.checkbox label, .has-warning.radio-inline label, .has-warning.checkbox-inline label {
  color: #8a6d3b
}
.has-warning .form-control {
  border-color: #8a6d3b;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  box-shadow: inset 0 1px 1px rgba(0,0,0,.075)
}
.has-warning .form-control:focus {
  border-color: #66512c;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 6px #c0a16b;
  box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 6px #c0a16b
}
.has-warning .input-group-addon {
  color: #8a6d3b;
  background-color: #fcf8e3;
  border-color: #8a6d3b
}
.has-warning .form-control-feedback {
  color: #8a6d3b
}
.has-error .help-block, .has-error .control-label, .has-error .radio, .has-error .checkbox, .has-error .radio-inline, .has-error .checkbox-inline, .has-error.radio label, .has-error.checkbox label, .has-error.radio-inline label, .has-error.checkbox-inline label {
  color: #a94442
}
.has-error .form-control {
  border-color: #a94442;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
  box-shadow: inset 0 1px 1px rgba(0,0,0,.075)
}
.has-error .form-control:focus {
  border-color: #843534;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 6px #ce8483;
  box-shadow: inset 0 1px 1px rgba(0,0,0,.075), 0 0 6px #ce8483
}
.has-error .input-group-addon {
  color: #a94442;
  background-color: #f2dede;
  border-color: #a94442
}
.has-error .form-control-feedback {
  color: #a94442
}
.has-feedback label~.form-control-feedback {
  top: 25px
}
.has-feedback label.sr-only~.form-control-feedback {
  top: 0
}
.help-block {
  display: block;
  margin-top: 5px;
  margin-bottom: 10px;
  color: #737373
}
@media (min-width:768px) {
.form-inline .form-group {
  display: inline-block;
  margin-bottom: 0;
  vertical-align: middle
}
.form-inline .form-control {
  display: inline-block;
  width: auto;
  vertical-align: middle
}
.form-inline .form-control-static {
  display: inline-block
}
.form-inline .input-group {
  display: inline-table;
  vertical-align: middle
}
.form-inline .input-group .input-group-addon, .form-inline .input-group .input-group-btn, .form-inline .input-group .form-control {
  width: auto
}
.form-inline .input-group>.form-control {
  width: 100%
}
.form-inline .control-label {
  margin-bottom: 0;
  vertical-align: middle
}
.form-inline .radio, .form-inline .checkbox {
  display: inline-block;
  margin-top: 0;
  margin-bottom: 0;
  vertical-align: middle
}
.form-inline .radio label, .form-inline .checkbox label {
  padding-left: 0
}
.form-inline .radio input[type=radio], .form-inline .checkbox input[type=checkbox] {
  position: relative;
  margin-left: 0
}
.form-inline .has-feedback .form-control-feedback {
  top: 0
}
}
.form-horizontal .radio, .form-horizontal .checkbox, .form-horizontal .radio-inline, .form-horizontal .checkbox-inline {
  padding-top: 7px;
  margin-top: 0;
  margin-bottom: 0
}
.form-horizontal .radio, .form-horizontal .checkbox {
  min-height: 27px
}
.form-horizontal .form-group {
  margin-right: -15px;
  margin-left: -15px
}
@media (min-width:768px) {
.form-horizontal .control-label {
  padding-top: 7px;
  margin-bottom: 0;
  text-align: right
}
}
.form-horizontal .has-feedback .form-control-feedback {
  right: 15px
}
@media (min-width:768px) {
.form-horizontal .form-group-lg .control-label {
  padding-top: 14.3px
}
}
@media (min-width:768px) {
.form-horizontal .form-group-sm .control-label {
  padding-top: 6px
}
}
.btn {
  display: inline-block;
  padding: 6px 12px;
  margin-bottom: 0;
  font-size: 14px;
  font-weight: 400;
  line-height: 1.42857143;
  text-align: center;
  white-space: nowrap;
  vertical-align: middle;
  -ms-touch-action: manipulation;
  touch-action: manipulation;
  cursor: pointer;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 4px
}
.btn:focus, .btn:active:focus, .btn.active:focus, .btn.focus, .btn:active.focus, .btn.active.focus {
  outline: thin dotted;
  outline: 5px auto -webkit-focus-ring-color;
  outline-offset: -2px
}
.btn:hover, .btn:focus, .btn.focus {
  color: #333;
  text-decoration: none
}
.btn:active, .btn.active {
  background-image: none;
  outline: 0;
  -webkit-box-shadow: inset 0 3px 5px rgba(0,0,0,.125);
  box-shadow: inset 0 3px 5px rgba(0,0,0,.125)
}
.btn.disabled, .btn[disabled], fieldset[disabled] .btn {
  pointer-events: none;
  cursor: not-allowed;
  filter: alpha(opacity=65);
  -webkit-box-shadow: none;
  box-shadow: none;
  opacity: .65
}
.btn-default {
  color: #333;
  background-color: #fff;
  border-color: #ccc
}
.btn-default:hover, .btn-default:focus, .btn-default.focus, .btn-default:active, .btn-default.active, .open>.dropdown-toggle.btn-default {
  color: #333;
  background-color: #e6e6e6;
  border-color: #adadad
}
.btn-default:active, .btn-default.active, .open>.dropdown-toggle.btn-default {
  background-image: none
}
.btn-default.disabled, .btn-default[disabled], fieldset[disabled] .btn-default, .btn-default.disabled:hover, .btn-default[disabled]:hover, fieldset[disabled] .btn-default:hover, .btn-default.disabled:focus, .btn-default[disabled]:focus, fieldset[disabled] .btn-default:focus, .btn-default.disabled.focus, .btn-default[disabled].focus, fieldset[disabled] .btn-default.focus, .btn-default.disabled:active, .btn-default[disabled]:active, fieldset[disabled] .btn-default:active, .btn-default.disabled.active, .btn-default[disabled].active, fieldset[disabled] .btn-default.active {
  background-color: #fff;
  border-color: #ccc
}
.btn-default .badge {
  color: #fff;
  background-color: #333
}
.btn-primary {
  color: #fff;
  background-color: #337ab7;
  border-color: #2e6da4
}
.btn-primary:hover, .btn-primary:focus, .btn-primary.focus, .btn-primary:active, .btn-primary.active, .open>.dropdown-toggle.btn-primary {
  color: #fff;
  background-color: #286090;
  border-color: #204d74
}
.btn-primary:active, .btn-primary.active, .open>.dropdown-toggle.btn-primary {
  background-image: none
}
.btn-primary.disabled, .btn-primary[disabled], fieldset[disabled] .btn-primary, .btn-primary.disabled:hover, .btn-primary[disabled]:hover, fieldset[disabled] .btn-primary:hover, .btn-primary.disabled:focus, .btn-primary[disabled]:focus, fieldset[disabled] .btn-primary:focus, .btn-primary.disabled.focus, .btn-primary[disabled].focus, fieldset[disabled] .btn-primary.focus, .btn-primary.disabled:active, .btn-primary[disabled]:active, fieldset[disabled] .btn-primary:active, .btn-primary.disabled.active, .btn-primary[disabled].active, fieldset[disabled] .btn-primary.active {
  background-color: #337ab7;
  border-color: #2e6da4
}
.btn-primary .badge {
  color: #337ab7;
  background-color: #fff
}
.btn-success {
  color: #fff;
  background-color: #5cb85c;
  border-color: #4cae4c
}
.btn-success:hover, .btn-success:focus, .btn-success.focus, .btn-success:active, .btn-success.active, .open>.dropdown-toggle.btn-success {
  color: #fff;
  background-color: #449d44;
  border-color: #398439
}
.btn-success:active, .btn-success.active, .open>.dropdown-toggle.btn-success {
  background-image: none
}
.btn-success.disabled, .btn-success[disabled], fieldset[disabled] .btn-success, .btn-success.disabled:hover, .btn-success[disabled]:hover, fieldset[disabled] .btn-success:hover, .btn-success.disabled:focus, .btn-success[disabled]:focus, fieldset[disabled] .btn-success:focus, .btn-success.disabled.focus, .btn-success[disabled].focus, fieldset[disabled] .btn-success.focus, .btn-success.disabled:active, .btn-success[disabled]:active, fieldset[disabled] .btn-success:active, .btn-success.disabled.active, .btn-success[disabled].active, fieldset[disabled] .btn-success.active {
  background-color: #5cb85c;
  border-color: #4cae4c
}
.btn-success .badge {
  color: #5cb85c;
  background-color: #fff
}
.btn-info {
  color: #fff;
  background-color: #5bc0de;
  border-color: #46b8da
}
.btn-info:hover, .btn-info:focus, .btn-info.focus, .btn-info:active, .btn-info.active, .open>.dropdown-toggle.btn-info {
  color: #fff;
  background-color: #31b0d5;
  border-color: #269abc
}
.btn-info:active, .btn-info.active, .open>.dropdown-toggle.btn-info {
  background-image: none
}
.btn-info.disabled, .btn-info[disabled], fieldset[disabled] .btn-info, .btn-info.disabled:hover, .btn-info[disabled]:hover, fieldset[disabled] .btn-info:hover, .btn-info.disabled:focus, .btn-info[disabled]:focus, fieldset[disabled] .btn-info:focus, .btn-info.disabled.focus, .btn-info[disabled].focus, fieldset[disabled] .btn-info.focus, .btn-info.disabled:active, .btn-info[disabled]:active, fieldset[disabled] .btn-info:active, .btn-info.disabled.active, .btn-info[disabled].active, fieldset[disabled] .btn-info.active {
  background-color: #5bc0de;
  border-color: #46b8da
}
.btn-info .badge {
  color: #5bc0de;
  background-color: #fff
}
.btn-warning {
  color: #fff;
  background-color: #f0ad4e;
  border-color: #eea236
}
.btn-warning:hover, .btn-warning:focus, .btn-warning.focus, .btn-warning:active, .btn-warning.active, .open>.dropdown-toggle.btn-warning {
  color: #fff;
  background-color: #ec971f;
  border-color: #d58512
}
.btn-warning:active, .btn-warning.active, .open>.dropdown-toggle.btn-warning {
  background-image: none
}
.btn-warning.disabled, .btn-warning[disabled], fieldset[disabled] .btn-warning, .btn-warning.disabled:hover, .btn-warning[disabled]:hover, fieldset[disabled] .btn-warning:hover, .btn-warning.disabled:focus, .btn-warning[disabled]:focus, fieldset[disabled] .btn-warning:focus, .btn-warning.disabled.focus, .btn-warning[disabled].focus, fieldset[disabled] .btn-warning.focus, .btn-warning.disabled:active, .btn-warning[disabled]:active, fieldset[disabled] .btn-warning:active, .btn-warning.disabled.active, .btn-warning[disabled].active, fieldset[disabled] .btn-warning.active {
  background-color: #f0ad4e;
  border-color: #eea236
}
.btn-warning .badge {
  color: #f0ad4e;
  background-color: #fff
}
.btn-danger {
  color: #fff;
  background-color: #d9534f;
  border-color: #d43f3a
}
.btn-danger:hover, .btn-danger:focus, .btn-danger.focus, .btn-danger:active, .btn-danger.active, .open>.dropdown-toggle.btn-danger {
  color: #fff;
  background-color: #c9302c;
  border-color: #ac2925
}
.btn-danger:active, .btn-danger.active, .open>.dropdown-toggle.btn-danger {
  background-image: none
}
.btn-danger.disabled, .btn-danger[disabled], fieldset[disabled] .btn-danger, .btn-danger.disabled:hover, .btn-danger[disabled]:hover, fieldset[disabled] .btn-danger:hover, .btn-danger.disabled:focus, .btn-danger[disabled]:focus, fieldset[disabled] .btn-danger:focus, .btn-danger.disabled.focus, .btn-danger[disabled].focus, fieldset[disabled] .btn-danger.focus, .btn-danger.disabled:active, .btn-danger[disabled]:active, fieldset[disabled] .btn-danger:active, .btn-danger.disabled.active, .btn-danger[disabled].active, fieldset[disabled] .btn-danger.active {
  background-color: #d9534f;
  border-color: #d43f3a
}
.btn-danger .badge {
  color: #d9534f;
  background-color: #fff
}
.btn-link {
  font-weight: 400;
  color: #337ab7;
  border-radius: 0
}
.btn-link, .btn-link:active, .btn-link.active, .btn-link[disabled], fieldset[disabled] .btn-link {
  background-color: transparent;
  -webkit-box-shadow: none;
  box-shadow: none
}
.btn-link, .btn-link:hover, .btn-link:focus, .btn-link:active {
  border-color: transparent
}
.btn-link:hover, .btn-link:focus {
  color: #23527c;
  text-decoration: underline;
  background-color: transparent
}
.btn-link[disabled]:hover, fieldset[disabled] .btn-link:hover, .btn-link[disabled]:focus, fieldset[disabled] .btn-link:focus {
  color: #777;
  text-decoration: none
}
.btn-lg, .btn-group-lg>.btn {
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.33;
  border-radius: 6px
}
.btn-sm, .btn-group-sm>.btn {
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
  border-radius: 3px
}
.btn-xs, .btn-group-xs>.btn {
  padding: 1px 5px;
  font-size: 12px;
  line-height: 1.5;
  border-radius: 3px
}
.btn-block {
  display: block;
  width: 100%
}
.btn-block+.btn-block {
  margin-top: 5px
}
input[type=submit].btn-block, input[type=reset].btn-block, input[type=button].btn-block {
  width: 100%
}
.fade {
  opacity: 0;
  -webkit-transition: opacity .15s linear;
  -o-transition: opacity .15s linear;
  transition: opacity .15s linear
}
.fade.in {
  opacity: 1
}
.collapse {
  display: none;
  visibility: hidden
}
.collapse.in {
  display: block;
  visibility: visible
}
tr.collapse.in {
  display: table-row
}
tbody.collapse.in {
  display: table-row-group
}
.collapsing {
  position: relative;
  height: 0;
  overflow: hidden;
  -webkit-transition-timing-function: ease;
  -o-transition-timing-function: ease;
  transition-timing-function: ease;
  -webkit-transition-duration: .35s;
  -o-transition-duration: .35s;
  transition-duration: .35s;
  -webkit-transition-property: height, visibility;
  -o-transition-property: height, visibility;
  transition-property: height, visibility
}
.caret {
  display: inline-block;
  width: 0;
  height: 0;
  margin-left: 2px;
  vertical-align: middle;
  border-top: 4px solid;
  border-right: 4px solid transparent;
  border-left: 4px solid transparent
}
.dropdown, .dropup {
  position: relative
}
.dropdown-toggle:focus {
  outline: 0
}
.dropdown-menu {
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 1000;
  display: none;
  float: left;
  min-width: 160px;
  padding: 5px 0;
  margin: 2px 0 0;
  font-size: 14px;
  text-align: left;
  list-style: none;
  background-color: #fff;
  -webkit-background-clip: padding-box;
  background-clip: padding-box;
  border: 1px solid #ccc;
  border: 1px solid rgba(0,0,0,.15);
  border-radius: 4px;
  -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  box-shadow: 0 6px 12px rgba(0,0,0,.175)
}
.dropdown-menu.pull-right {
  right: 0;
  left: auto
}
.dropdown-menu .divider {
  height: 1px;
  margin: 9px 0;
  overflow: hidden;
  background-color: #e5e5e5
}
.dropdown-menu>li>a {
  display: block;
  padding: 3px 20px;
  clear: both;
  font-weight: 400;
  line-height: 1.42857143;
  color: #333;
  white-space: nowrap
}
.dropdown-menu>li>a:hover, .dropdown-menu>li>a:focus {
  color: #262626;
  text-decoration: none;
  background-color: #f5f5f5
}
.dropdown-menu>.active>a, .dropdown-menu>.active>a:hover, .dropdown-menu>.active>a:focus {
  color: #fff;
  text-decoration: none;
  background-color: #337ab7;
  outline: 0
}
.dropdown-menu>.disabled>a, .dropdown-menu>.disabled>a:hover, .dropdown-menu>.disabled>a:focus {
  color: #777
}
.dropdown-menu>.disabled>a:hover, .dropdown-menu>.disabled>a:focus {
  text-decoration: none;
  cursor: not-allowed;
  background-color: transparent;
  background-image: none;
filter:progid:DXImageTransform.Microsoft.gradient(enabled=false)
}
.open>.dropdown-menu {
  display: block
}
.open>a {
  outline: 0
}
.dropdown-menu-right {
  right: 0;
  left: auto
}
.dropdown-menu-left {
  right: auto;
  left: 0
}
.dropdown-header {
  display: block;
  padding: 3px 20px;
  font-size: 12px;
  line-height: 1.42857143;
  color: #777;
  white-space: nowrap
}
.dropdown-backdrop {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 990
}
.pull-right>.dropdown-menu {
  right: 0;
  left: auto
}
.dropup .caret, .navbar-fixed-bottom .dropdown .caret {
  content: '';
  border-top: 0;
  border-bottom: 4px solid
}
.dropup .dropdown-menu, .navbar-fixed-bottom .dropdown .dropdown-menu {
  top: auto;
  bottom: 100%;
  margin-bottom: 1px
}
@media (min-width:768px) {
.navbar-right .dropdown-menu {
  right: 0;
  left: auto
}
.navbar-right .dropdown-menu-left {
  right: auto;
  left: 0
}
}
.btn-group, .btn-group-vertical {
  position: relative;
  display: inline-block;
  vertical-align: middle
}
.btn-group>.btn, .btn-group-vertical>.btn {
  position: relative;
  float: left
}
.btn-group>.btn:hover, .btn-group-vertical>.btn:hover, .btn-group>.btn:focus, .btn-group-vertical>.btn:focus, .btn-group>.btn:active, .btn-group-vertical>.btn:active, .btn-group>.btn.active, .btn-group-vertical>.btn.active {
  z-index: 2
}
.btn-group .btn+.btn, .btn-group .btn+.btn-group, .btn-group .btn-group+.btn, .btn-group .btn-group+.btn-group {
  margin-left: -1px
}
.btn-toolbar {
  margin-left: -5px
}
.btn-toolbar .btn-group, .btn-toolbar .input-group {
  float: left
}
.btn-toolbar>.btn, .btn-toolbar>.btn-group, .btn-toolbar>.input-group {
  margin-left: 5px
}
.btn-group>.btn:not(:first-child):not(:last-child):not(.dropdown-toggle) {
  border-radius: 0
}
.btn-group>.btn:first-child {
  margin-left: 0
}
.btn-group>.btn:first-child:not(:last-child):not(.dropdown-toggle) {
  border-top-right-radius: 0;
  border-bottom-right-radius: 0
}
.btn-group>.btn:last-child:not(:first-child), .btn-group>.dropdown-toggle:not(:first-child) {
  border-top-left-radius: 0;
  border-bottom-left-radius: 0
}
.btn-group>.btn-group {
  float: left
}
.btn-group>.btn-group:not(:first-child):not(:last-child)>.btn {
  border-radius: 0
}
.btn-group>.btn-group:first-child>.btn:last-child, .btn-group>.btn-group:first-child>.dropdown-toggle {
  border-top-right-radius: 0;
  border-bottom-right-radius: 0
}
.btn-group>.btn-group:last-child>.btn:first-child {
  border-top-left-radius: 0;
  border-bottom-left-radius: 0
}
.btn-group .dropdown-toggle:active, .btn-group.open .dropdown-toggle {
  outline: 0
}
.btn-group>.btn+.dropdown-toggle {
  padding-right: 8px;
  padding-left: 8px
}
.btn-group>.btn-lg+.dropdown-toggle {
  padding-right: 12px;
  padding-left: 12px
}
.btn-group.open .dropdown-toggle {
  -webkit-box-shadow: inset 0 3px 5px rgba(0,0,0,.125);
  box-shadow: inset 0 3px 5px rgba(0,0,0,.125)
}
.btn-group.open .dropdown-toggle.btn-link {
  -webkit-box-shadow: none;
  box-shadow: none
}
.btn .caret {
  margin-left: 0
}
.btn-lg .caret {
  border-width: 5px 5px 0;
  border-bottom-width: 0
}
.dropup .btn-lg .caret {
  border-width: 0 5px 5px
}
.btn-group-vertical>.btn, .btn-group-vertical>.btn-group, .btn-group-vertical>.btn-group>.btn {
  display: block;
  float: none;
  width: 100%;
  max-width: 100%
}
.btn-group-vertical>.btn-group>.btn {
  float: none
}
.btn-group-vertical>.btn+.btn, .btn-group-vertical>.btn+.btn-group, .btn-group-vertical>.btn-group+.btn, .btn-group-vertical>.btn-group+.btn-group {
  margin-top: -1px;
  margin-left: 0
}
.btn-group-vertical>.btn:not(:first-child):not(:last-child) {
  border-radius: 0
}
.btn-group-vertical>.btn:first-child:not(:last-child) {
  border-top-right-radius: 4px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0
}
.btn-group-vertical>.btn:last-child:not(:first-child) {
  border-top-left-radius: 0;
  border-top-right-radius: 0;
  border-bottom-left-radius: 4px
}
.btn-group-vertical>.btn-group:not(:first-child):not(:last-child)>.btn {
  border-radius: 0
}
.btn-group-vertical>.btn-group:first-child:not(:last-child)>.btn:last-child, .btn-group-vertical>.btn-group:first-child:not(:last-child)>.dropdown-toggle {
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0
}
.btn-group-vertical>.btn-group:last-child:not(:first-child)>.btn:first-child {
  border-top-left-radius: 0;
  border-top-right-radius: 0
}
.btn-group-justified {
  display: table;
  width: 100%;
  table-layout: fixed;
  border-collapse: separate
}
.btn-group-justified>.btn, .btn-group-justified>.btn-group {
  display: table-cell;
  float: none;
  width: 1%
}
.btn-group-justified>.btn-group .btn {
  width: 100%
}
.btn-group-justified>.btn-group .dropdown-menu {
  left: auto
}
[data-toggle=buttons]>.btn input[type=radio], [data-toggle=buttons]>.btn-group>.btn input[type=radio], [data-toggle=buttons]>.btn input[type=checkbox], [data-toggle=buttons]>.btn-group>.btn input[type=checkbox] {
position:absolute;
clip:rect(0,0,0,0);
pointer-events:none
}
.input-group {
  position: relative;
  display: table;
  border-collapse: separate
}
.input-group[class*=col-] {
  float: none;
  padding-right: 0;
  padding-left: 0
}
.input-group .form-control {
  position: relative;
  z-index: 2;
  float: left;
  width: 100%;
  margin-bottom: 0
}
.input-group-lg>.form-control, .input-group-lg>.input-group-addon, .input-group-lg>.input-group-btn>.btn {
  height: 46px;
  padding: 10px 16px;
  font-size: 18px;
  line-height: 1.33;
  border-radius: 6px
}
select.input-group-lg>.form-control, select.input-group-lg>.input-group-addon, select.input-group-lg>.input-group-btn>.btn {
  height: 46px;
  line-height: 46px
}
textarea.input-group-lg>.form-control, textarea.input-group-lg>.input-group-addon, textarea.input-group-lg>.input-group-btn>.btn, select[multiple].input-group-lg>.form-control, select[multiple].input-group-lg>.input-group-addon, select[multiple].input-group-lg>.input-group-btn>.btn {
  height: auto
}
.input-group-sm>.form-control, .input-group-sm>.input-group-addon, .input-group-sm>.input-group-btn>.btn {
  height: 30px;
  padding: 5px 10px;
  font-size: 12px;
  line-height: 1.5;
  border-radius: 3px
}
select.input-group-sm>.form-control, select.input-group-sm>.input-group-addon, select.input-group-sm>.input-group-btn>.btn {
  height: 30px;
  line-height: 30px
}
textarea.input-group-sm>.form-control, textarea.input-group-sm>.input-group-addon, textarea.input-group-sm>.input-group-btn>.btn, select[multiple].input-group-sm>.form-control, select[multiple].input-group-sm>.input-group-addon, select[multiple].input-group-sm>.input-group-btn>.btn {
  height: auto
}
.input-group-addon, .input-group-btn, .input-group .form-control {
  display: table-cell
}
.input-group-addon:not(:first-child):not(:last-child), .input-group-btn:not(:first-child):not(:last-child), .input-group .form-control:not(:first-child):not(:last-child) {
  border-radius: 0
}
.input-group-addon, .input-group-btn {
  width: 1%;
  white-space: nowrap;
  vertical-align: middle
}
.input-group-addon {
  padding: 6px 12px;
  font-size: 14px;
  font-weight: 400;
  line-height: 1;
  color: #555;
  text-align: center;
  background-color: #eee;
  border: 1px solid #ccc;
  border-radius: 4px
}
.input-group-addon.input-sm {
  padding: 5px 10px;
  font-size: 12px;
  border-radius: 3px
}
.input-group-addon.input-lg {
  padding: 10px 16px;
  font-size: 18px;
  border-radius: 6px
}
.input-group-addon input[type=radio], .input-group-addon input[type=checkbox] {
  margin-top: 0
}
.input-group .form-control:first-child, .input-group-addon:first-child, .input-group-btn:first-child>.btn, .input-group-btn:first-child>.btn-group>.btn, .input-group-btn:first-child>.dropdown-toggle, .input-group-btn:last-child>.btn:not(:last-child):not(.dropdown-toggle), .input-group-btn:last-child>.btn-group:not(:last-child)>.btn {
  border-top-right-radius: 0;
  border-bottom-right-radius: 0
}
.input-group-addon:first-child {
  border-right: 0
}
.input-group .form-control:last-child, .input-group-addon:last-child, .input-group-btn:last-child>.btn, .input-group-btn:last-child>.btn-group>.btn, .input-group-btn:last-child>.dropdown-toggle, .input-group-btn:first-child>.btn:not(:first-child), .input-group-btn:first-child>.btn-group:not(:first-child)>.btn {
  border-top-left-radius: 0;
  border-bottom-left-radius: 0
}
.input-group-addon:last-child {
  border-left: 0
}
.input-group-btn {
  position: relative;
  font-size: 0;
  white-space: nowrap
}
.input-group-btn>.btn {
  position: relative
}
.input-group-btn>.btn+.btn {
  margin-left: -1px
}
.input-group-btn>.btn:hover, .input-group-btn>.btn:focus, .input-group-btn>.btn:active {
  z-index: 2
}
.input-group-btn:first-child>.btn, .input-group-btn:first-child>.btn-group {
  margin-right: -1px
}
.input-group-btn:last-child>.btn, .input-group-btn:last-child>.btn-group {
  margin-left: -1px
}
.nav {
  padding-left: 0;
  margin-bottom: 0;
  list-style: none
}
.nav>li {
  position: relative;
  display: block
}
.nav>li>a {
  position: relative;
  display: block;
  padding: 25px 15px
}
.nav>li>a:hover, .nav>li>a:focus {
  text-decoration: none;
  background-color: #eee
}
.nav>li.disabled>a {
  color: #777
}
.nav>li.disabled>a:hover, .nav>li.disabled>a:focus {
  color: #777;
  text-decoration: none;
  cursor: not-allowed;
  background-color: transparent
}
.nav .open>a, .nav .open>a:hover, .nav .open>a:focus {
  background-color: #eee;
  border-color: #337ab7
}
.nav .nav-divider {
  height: 1px;
  margin: 9px 0;
  overflow: hidden;
  background-color: #e5e5e5
}
.nav>li>a>img {
  max-width: none
}
.nav-tabs {
  border-bottom: 1px solid #ddd
}
.nav-tabs>li {
  float: left;
  margin-bottom: -1px
}
.nav-tabs>li>a {
  margin-right: 2px;
  line-height: 1.42857143;
  border: 1px solid transparent;
  border-radius: 4px 4px 0 0
}
.nav-tabs>li>a:hover {
  border-color: #eee #eee #ddd
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus {
  color: #555;
  cursor: default;
  background-color: #fff;
  border: 1px solid #ddd;
  border-bottom-color: transparent
}
.nav-tabs.nav-justified {
  width: 100%;
  border-bottom: 0
}
.nav-tabs.nav-justified>li {
  float: none
}
.nav-tabs.nav-justified>li>a {
  margin-bottom: 5px;
  text-align: center
}
.nav-tabs.nav-justified>.dropdown .dropdown-menu {
  top: auto;
  left: auto
}
@media (min-width:768px) {
.nav-tabs.nav-justified>li {
  display: table-cell;
  width: 1%
}
.nav-tabs.nav-justified>li>a {
  margin-bottom: 0
}
}
.nav-tabs.nav-justified>li>a {
  margin-right: 0;
  border-radius: 4px
}
.nav-tabs.nav-justified>.active>a, .nav-tabs.nav-justified>.active>a:hover, .nav-tabs.nav-justified>.active>a:focus {
  border: 1px solid #ddd
}
@media (min-width:768px) {
.nav-tabs.nav-justified>li>a {
  border-bottom: 1px solid #ddd;
  border-radius: 4px 4px 0 0
}
.nav-tabs.nav-justified>.active>a, .nav-tabs.nav-justified>.active>a:hover, .nav-tabs.nav-justified>.active>a:focus {
  border-bottom-color: #fff
}
}
.nav-pills>li {
  float: left
}
.nav-pills>li>a {
  border-radius: 4px
}
.nav-pills>li+li {
  margin-left: 2px
}
.nav-pills>li.active>a, .nav-pills>li.active>a:hover, .nav-pills>li.active>a:focus {
  color: #fff;
  background-color: #337ab7
}
.nav-stacked>li {
  float: none
}
.nav-stacked>li+li {
  margin-top: 2px;
  margin-left: 0
}
.nav-justified {
  width: 100%
}
.nav-justified>li {
  float: none
}
.nav-justified>li>a {
  margin-bottom: 5px;
  text-align: center
}
.nav-justified>.dropdown .dropdown-menu {
  top: auto;
  left: auto
}
@media (min-width:768px) {
.nav-justified>li {
  display: table-cell;
  width: 1%
}
.nav-justified>li>a {
  margin-bottom: 0
}
}
.nav-tabs-justified {
  border-bottom: 0
}
.nav-tabs-justified>li>a {
  margin-right: 0;
  border-radius: 4px
}
.nav-tabs-justified>.active>a, .nav-tabs-justified>.active>a:hover, .nav-tabs-justified>.active>a:focus {
  border: 1px solid #ddd
}
@media (min-width:768px) {
.nav-tabs-justified>li>a {
  border-bottom: 1px solid #ddd;
  border-radius: 4px 4px 0 0
}
.nav-tabs-justified>.active>a, .nav-tabs-justified>.active>a:hover, .nav-tabs-justified>.active>a:focus {
  border-bottom-color: #fff
}
}
.tab-content>.tab-pane {
  display: none;
  visibility: hidden
}
.tab-content>.active {
  display: block;
  visibility: visible
}
.nav-tabs .dropdown-menu {
  margin-top: -1px;
  border-top-left-radius: 0;
  border-top-right-radius: 0
}
.navbar {
  position: relative;
  min-height: 50px;
  margin-bottom: 20px;
  border: 1px solid transparent
}
@media (min-width:768px) {
.navbar {
  border-radius: 4px
}
}
@media (min-width:768px) {
.navbar-header {
  float: left
}
}
.navbar-collapse {
  padding-right: 15px;
  padding-left: 15px;
  overflow-x: visible;
  -webkit-overflow-scrolling: touch;
  border-top: 1px solid transparent;
  -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.1);
  box-shadow: inset 0 1px 0 rgba(255,255,255,.1)
}
.navbar-collapse.in {
  overflow-y: auto
}
@media (min-width:768px) {
.navbar-collapse {
  width: auto;
  border-top: 0;
  -webkit-box-shadow: none;
  box-shadow: none
}
.navbar-collapse.collapse {
  display: block!important;
  height: auto!important;
  padding-bottom: 0;
  overflow: visible!important;
  visibility: visible!important
}
.navbar-collapse.in {
  overflow-y: visible
}
.navbar-fixed-top .navbar-collapse, .navbar-static-top .navbar-collapse, .navbar-fixed-bottom .navbar-collapse {
  padding-right: 0;
  padding-left: 0
}
}
.navbar-fixed-top .navbar-collapse, .navbar-fixed-bottom .navbar-collapse {
  max-height: 340px
}
@media (max-device-width:480px) and (orientation:landscape) {
.navbar-fixed-top .navbar-collapse, .navbar-fixed-bottom .navbar-collapse {
  max-height: 200px
}
}
.container>.navbar-header, .container-fluid>.navbar-header, .container>.navbar-collapse, .container-fluid>.navbar-collapse {
  margin-right: -15px;
  margin-left: -15px
}
@media (min-width:768px) {
.container>.navbar-header, .container-fluid>.navbar-header, .container>.navbar-collapse, .container-fluid>.navbar-collapse {
  margin-right: 0;
  margin-left: 0
}
}
.navbar-static-top {
  z-index: 1000;
  border-width: 0 0 1px
}
@media (min-width:768px) {
.navbar-static-top {
  border-radius: 0
}
}
.navbar-fixed-top, .navbar-fixed-bottom {
  position: fixed;
  right: 0;
  left: 0;
  z-index: 1030
}
@media (min-width:768px) {
.navbar-fixed-top, .navbar-fixed-bottom {
  border-radius: 0
}
}
.navbar-fixed-top {
  top: 0;
  border-width: 0 0 1px
}
.navbar-fixed-bottom {
  bottom: 0;
  margin-bottom: 0;
  border-width: 1px 0 0
}
.navbar-brand {
  float: left;
  height: 50px;
  padding: 15px 15px;
  font-size: 18px;
  line-height: 20px
}
.navbar-brand:hover, .navbar-brand:focus {
  text-decoration: none
}
.navbar-brand>img {
  display: block
}
@media (min-width:768px) {
.navbar>.container .navbar-brand, .navbar>.container-fluid .navbar-brand {
  margin-left: -15px
}
}
.navbar-toggle {
  position: relative;
  float: left;
  padding: 9px 10px;
  margin-top: 8px;
  margin-left:8px;
  margin-right: 15px;
  margin-bottom: 8px;
  background-color: transparent;
  background-image: none;
  border: 1px solid transparent;
  border-radius: 0px;
}
.navbar-toggle:focus {
  outline: 0
}
.navbar-toggle .icon-bar {
  display: block;
  width: 22px;
  height: 2px;
  border-radius: 1px
}
.navbar-toggle .icon-bar+.icon-bar {
  margin-top: 4px
}
@media (min-width:768px) {
.navbar-toggle {
  display: none
}
}
.navbar-nav {
  margin: 7.5px -15px
}
.navbar-nav>li>a {
  padding-top: 10px;
  padding-bottom: 10px;
  line-height: 20px
}
@media (max-width:767px) {
.navbar-nav .open .dropdown-menu {
  position: static;
  float: none;
  width: auto;
  margin-top: 0;
  background-color: transparent;
  border: 0;
  -webkit-box-shadow: none;
  box-shadow: none
}
.navbar-nav .open .dropdown-menu>li>a, .navbar-nav .open .dropdown-menu .dropdown-header {
  padding: 5px 15px 5px 25px
}
.navbar-nav .open .dropdown-menu>li>a {
  line-height: 20px
}
.navbar-nav .open .dropdown-menu>li>a:hover, .navbar-nav .open .dropdown-menu>li>a:focus {
  background-image: none
}
}
@media (min-width:768px) {
.navbar-nav {
  float: left;
  margin: 0
}
.navbar-nav>li {
  float: left
}
.navbar-nav>li>a {
  padding-top: 15px;
  padding-bottom: 15px
}
}
.navbar-form {
  padding: 10px 15px;
  margin-top: 8px;
  margin-right: -15px;
  margin-bottom: 8px;
  margin-left: -15px;
  border-top: 1px solid transparent;
  border-bottom: 1px solid transparent;
  -webkit-box-shadow: inset 0 1px 0 rgba(255,255,255,.1), 0 1px 0 rgba(255,255,255,.1);
  box-shadow: inset 0 1px 0 rgba(255,255,255,.1), 0 1px 0 rgba(255,255,255,.1)
}
@media (min-width:768px) {
.navbar-form .form-group {
  display: inline-block;
  margin-bottom: 0;
  vertical-align: middle
}
.navbar-form .form-control {
  display: inline-block;
  width: auto;
  vertical-align: middle
}
.navbar-form .form-control-static {
  display: inline-block
}
.navbar-form .input-group {
  display: inline-table;
  vertical-align: middle
}
.navbar-form .input-group .input-group-addon, .navbar-form .input-group .input-group-btn, .navbar-form .input-group .form-control {
  width: auto
}
.navbar-form .input-group>.form-control {
  width: 100%
}
.navbar-form .control-label {
  margin-bottom: 0;
  vertical-align: middle
}
.navbar-form .radio, .navbar-form .checkbox {
  display: inline-block;
  margin-top: 0;
  margin-bottom: 0;
  vertical-align: middle
}
.navbar-form .radio label, .navbar-form .checkbox label {
  padding-left: 0
}
.navbar-form .radio input[type=radio], .navbar-form .checkbox input[type=checkbox] {
  position: relative;
  margin-left: 0
}
.navbar-form .has-feedback .form-control-feedback {
  top: 0
}
}
@media (max-width:767px) {
.navbar-form .form-group {
  margin-bottom: 5px
}
.navbar-form .form-group:last-child {
  margin-bottom: 0
}
}
@media (min-width:768px) {
.navbar-form {
  width: auto;
  padding-top: 0;
  padding-bottom: 0;
  margin-right: 0;
  margin-left: 0;
  border: 0;
  -webkit-box-shadow: none;
  box-shadow: none
}
}
.navbar-nav>li>.dropdown-menu {
  margin-top: 0;
  border-top-left-radius: 0;
  border-top-right-radius: 0
}
.navbar-fixed-bottom .navbar-nav>li>.dropdown-menu {
  border-top-left-radius: 4px;
  border-top-right-radius: 4px;
  border-bottom-right-radius: 0;
  border-bottom-left-radius: 0
}
.navbar-btn {
  margin-top: 8px;
  margin-bottom: 8px
}
.navbar-btn.btn-sm {
  margin-top: 10px;
  margin-bottom: 10px
}
.navbar-btn.btn-xs {
  margin-top: 14px;
  margin-bottom: 14px
}
.navbar-text {
  margin-top: 15px;
  margin-bottom: 15px
}
@media (min-width:768px) {
.navbar-text {
  float: left;
  margin-right: 15px;
  margin-left: 15px
}
}
@media (min-width:768px) {
.navbar-left {
  float: left!important
}
.navbar-right {
  float: right!important;
  margin-right: -15px
}
.navbar-right~.navbar-right {
  margin-right: 0
}
}
.navbar-default {
  background-color: #f8f8f8;
  border-color: #e7e7e7
}
.navbar-default .navbar-brand {
  color: #777
}
.navbar-default .navbar-brand:hover, .navbar-default .navbar-brand:focus {
  color: #5e5e5e;
  background-color: transparent
}
.navbar-default .navbar-text {
  color: #777
}
.navbar-default .navbar-nav>li>a {
  
}
.navbar-default .navbar-nav>li>a:hover, .navbar-default .navbar-nav>li>a:focus {
  color: #E8242A;
  background-color: transparent;
}
.navbar-default .navbar-nav>.active>a, .navbar-default .navbar-nav>.active>a:hover, .navbar-default .navbar-nav>.active>a:focus {
  color: #555;
  background-color: #e7e7e7
}
.navbar-default .navbar-nav>.disabled>a, .navbar-default .navbar-nav>.disabled>a:hover, .navbar-default .navbar-nav>.disabled>a:focus {
  color: #ccc;
  background-color: transparent
}
.navbar-default .navbar-toggle {
  border-color: #ddd
}
.navbar-default .navbar-toggle:hover, .navbar-default .navbar-toggle:focus {
  background-color:transparent
}
.navbar-default .navbar-toggle .icon-bar {
  background-color: #888
}
.navbar-default .navbar-collapse, .navbar-default .navbar-form {
  border-color: #e7e7e7
}
.navbar-default .navbar-nav>.open>a, .navbar-default .navbar-nav>.open>a:hover, .navbar-default .navbar-nav>.open>a:focus {
  color: #555;
  background-color: #e7e7e7
}
@media (max-width:767px) {
.navbar-default .navbar-nav .open .dropdown-menu>li>a {
  color: #777
}
.navbar-default .navbar-nav .open .dropdown-menu>li>a:hover, .navbar-default .navbar-nav .open .dropdown-menu>li>a:focus {
  color: #333;
  background-color: transparent
}
.navbar-default .navbar-nav .open .dropdown-menu>.active>a, .navbar-default .navbar-nav .open .dropdown-menu>.active>a:hover, .navbar-default .navbar-nav .open .dropdown-menu>.active>a:focus {
  color: #555;
  background-color: #e7e7e7
}
.navbar-default .navbar-nav .open .dropdown-menu>.disabled>a, .navbar-default .navbar-nav .open .dropdown-menu>.disabled>a:hover, .navbar-default .navbar-nav .open .dropdown-menu>.disabled>a:focus {
  color: #ccc;
  background-color: transparent
}
}
.navbar-default .navbar-link {
  color: #777
}
.navbar-default .navbar-link:hover {
  color: #333
}
.navbar-default .btn-link {
  color: #777
}
.navbar-default .btn-link:hover, .navbar-default .btn-link:focus {
  color: #333
}
.navbar-default .btn-link[disabled]:hover, fieldset[disabled] .navbar-default .btn-link:hover, .navbar-default .btn-link[disabled]:focus, fieldset[disabled] .navbar-default .btn-link:focus {
  color: #ccc
}
.navbar-inverse {
  background-color: #222;
  border-color: #080808
}
.navbar-inverse .navbar-brand {
  color: #9d9d9d
}
.navbar-inverse .navbar-brand:hover, .navbar-inverse .navbar-brand:focus {
  color: #fff;
  background-color: transparent
}
.navbar-inverse .navbar-text {
  color: #9d9d9d
}
.navbar-inverse .navbar-nav>li>a {
  color: #9d9d9d
}
.navbar-inverse .navbar-nav>li>a:hover, .navbar-inverse .navbar-nav>li>a:focus {
  color: #fff;
  background-color: transparent
}
.navbar-inverse .navbar-nav>.active>a, .navbar-inverse .navbar-nav>.active>a:hover, .navbar-inverse .navbar-nav>.active>a:focus {
  color: #fff;
  background-color: #080808
}
.navbar-inverse .navbar-nav>.disabled>a, .navbar-inverse .navbar-nav>.disabled>a:hover, .navbar-inverse .navbar-nav>.disabled>a:focus {
  color: #444;
  background-color: transparent
}
.navbar-inverse .navbar-toggle {
  border-color: #333
}
.navbar-inverse .navbar-toggle:hover, .navbar-inverse .navbar-toggle:focus {
  background-color: #333
}
.navbar-inverse .navbar-toggle .icon-bar {
  background-color: #fff
}
.navbar-inverse .navbar-collapse, .navbar-inverse .navbar-form {
  border-color: #101010
}
.navbar-inverse .navbar-nav>.open>a, .navbar-inverse .navbar-nav>.open>a:hover, .navbar-inverse .navbar-nav>.open>a:focus {
  color: #fff;
  background-color: #080808
}
@media (max-width:767px) {
.navbar-inverse .navbar-nav .open .dropdown-menu>.dropdown-header {
  border-color: #080808
}
.navbar-inverse .navbar-nav .open .dropdown-menu .divider {
  background-color: #080808
}
.navbar-inverse .navbar-nav .open .dropdown-menu>li>a {
  color: #9d9d9d
}
.navbar-inverse .navbar-nav .open .dropdown-menu>li>a:hover, .navbar-inverse .navbar-nav .open .dropdown-menu>li>a:focus {
  color: #fff;
  background-color: transparent
}
.navbar-inverse .navbar-nav .open .dropdown-menu>.active>a, .navbar-inverse .navbar-nav .open .dropdown-menu>.active>a:hover, .navbar-inverse .navbar-nav .open .dropdown-menu>.active>a:focus {
  color: #fff;
  background-color: #080808
}
.navbar-inverse .navbar-nav .open .dropdown-menu>.disabled>a, .navbar-inverse .navbar-nav .open .dropdown-menu>.disabled>a:hover, .navbar-inverse .navbar-nav .open .dropdown-menu>.disabled>a:focus {
  color: #444;
  background-color: transparent
}
}
.navbar-inverse .navbar-link {
  color: #9d9d9d
}
.navbar-inverse .navbar-link:hover {
  color: #fff
}
.navbar-inverse .btn-link {
  color: #9d9d9d
}
.navbar-inverse .btn-link:hover, .navbar-inverse .btn-link:focus {
  color: #fff
}
.navbar-inverse .btn-link[disabled]:hover, fieldset[disabled] .navbar-inverse .btn-link:hover, .navbar-inverse .btn-link[disabled]:focus, fieldset[disabled] .navbar-inverse .btn-link:focus {
  color: #444
}
.breadcrumb {
  padding: 8px 15px;
  margin-bottom: 20px;
  list-style: none;
  background-color: #f5f5f5;
  border-radius: 4px
}
.breadcrumb>li {
  display: inline-block
}
.breadcrumb>li+li:before {
  padding: 0 5px;
  color: #ccc;
  content: '/a0'
}
.breadcrumb>.active {
  color: #777
}
.pagination {
  display: inline-block;
  padding-left: 0;
  margin: 20px 0;
  border-radius: 4px
}
.pagination>li {
  display: inline
}
.pagination>li>a, .pagination>li>span {
  position: relative;
  float: left;
  padding: 6px 12px;
  margin-left: -1px;
  line-height: 1.42857143;
  color: #337ab7;
  text-decoration: none;
  background-color: #fff;
  border: 1px solid #ddd
}
.pagination>li:first-child>a, .pagination>li:first-child>span {
  margin-left: 0;
  border-top-left-radius: 4px;
  border-bottom-left-radius: 4px
}
.pagination>li:last-child>a, .pagination>li:last-child>span {
  border-top-right-radius: 4px;
  border-bottom-right-radius: 4px
}
.pagination>li>a:hover, .pagination>li>span:hover, .pagination>li>a:focus, .pagination>li>span:focus {
  color: #23527c;
  background-color: #eee;
  border-color: #ddd
}
.pagination>.active>a, .pagination>.active>span, .pagination>.active>a:hover, .pagination>.active>span:hover, .pagination>.active>a:focus, .pagination>.active>span:focus {
  z-index: 2;
  color: #fff;
  cursor: default;
  background-color: #337ab7;
  border-color: #337ab7
}
.pagination>.disabled>span, .pagination>.disabled>span:hover, .pagination>.disabled>span:focus, .pagination>.disabled>a, .pagination>.disabled>a:hover, .pagination>.disabled>a:focus {
  color: #777;
  cursor: not-allowed;
  background-color: #fff;
  border-color: #ddd
}
.pagination-lg>li>a, .pagination-lg>li>span {
  padding: 10px 16px;
  font-size: 18px
}
.pagination-lg>li:first-child>a, .pagination-lg>li:first-child>span {
  border-top-left-radius: 6px;
  border-bottom-left-radius: 6px
}
.pagination-lg>li:last-child>a, .pagination-lg>li:last-child>span {
  border-top-right-radius: 6px;
  border-bottom-right-radius: 6px
}
.pagination-sm>li>a, .pagination-sm>li>span {
  padding: 5px 10px;
  font-size: 12px
}
.pagination-sm>li:first-child>a, .pagination-sm>li:first-child>span {
  border-top-left-radius: 3px;
  border-bottom-left-radius: 3px
}
.pagination-sm>li:last-child>a, .pagination-sm>li:last-child>span {
  border-top-right-radius: 3px;
  border-bottom-right-radius: 3px
}
.pager {
  padding-left: 0;
  margin: 20px 0;
  text-align: center;
  list-style: none
}
.pager li {
  display: inline
}
.pager li>a, .pager li>span {
  display: inline-block;
  padding: 5px 14px;
  background-color: #fff;
  border: 1px solid #ddd;
  border-radius: 15px
}
.pager li>a:hover, .pager li>a:focus {
  text-decoration: none;
  background-color: #eee
}
.pager .next>a, .pager .next>span {
  float: right
}
.pager .previous>a, .pager .previous>span {
  float: left
}
.pager .disabled>a, .pager .disabled>a:hover, .pager .disabled>a:focus, .pager .disabled>span {
  color: #777;
  cursor: not-allowed;
  background-color: #fff
}
.label {
  display: inline;
  padding: .2em .6em .3em;
  font-size: 75%;
  font-weight: 700;
  line-height: 1;
  color: #fff;
  text-align: center;
  white-space: nowrap;
  vertical-align: baseline;
  border-radius: .25em
}
a.label:hover, a.label:focus {
  color: #fff;
  text-decoration: none;
  cursor: pointer
}
.label:empty {
  display: none
}
.btn .label {
  position: relative;
  top: -1px
}
.label-default {
  background-color: #777
}
.label-default[href]:hover, .label-default[href]:focus {
  background-color: #5e5e5e
}
.label-primary {
  background-color: #337ab7
}
.label-primary[href]:hover, .label-primary[href]:focus {
  background-color: #286090
}
.label-success {
  background-color: #5cb85c
}
.label-success[href]:hover, .label-success[href]:focus {
  background-color: #449d44
}
.label-info {
  background-color: #5bc0de
}
.label-info[href]:hover, .label-info[href]:focus {
  background-color: #31b0d5
}
.label-warning {
  background-color: #f0ad4e
}
.label-warning[href]:hover, .label-warning[href]:focus {
  background-color: #ec971f
}
.label-danger {
  background-color: #d9534f
}
.label-danger[href]:hover, .label-danger[href]:focus {
  background-color: #c9302c
}
.badge {
  display: inline-block;
  min-width: 10px;
  padding: 3px 7px;
  font-size: 12px;
  font-weight: 700;
  line-height: 1;
  color: #fff;
  text-align: center;
  white-space: nowrap;
  vertical-align: baseline;
  background-color: #777;
  border-radius: 10px
}
.badge:empty {
  display: none
}
.btn .badge {
  position: relative;
  top: -1px
}
.btn-xs .badge {
  top: 0;
  padding: 1px 5px
}
a.badge:hover, a.badge:focus {
  color: #fff;
  text-decoration: none;
  cursor: pointer
}
.list-group-item.active>.badge, .nav-pills>.active>a>.badge {
  color: #337ab7;
  background-color: #fff
}
.list-group-item>.badge {
  float: right
}
.list-group-item>.badge+.badge {
  margin-right: 5px
}
.nav-pills>li>a>.badge {
  margin-left: 3px
}
.jumbotron {
  padding: 30px 15px;
  margin-bottom: 30px;
  color: inherit;
  background-color: #eee
}
.jumbotron h1, .jumbotron .h1 {
  color: inherit
}
.jumbotron p {
  margin-bottom: 15px;
  font-size: 21px;
  font-weight: 200
}
.jumbotron>hr {
  border-top-color: #d5d5d5
}
.container .jumbotron, .container-fluid .jumbotron {
  border-radius: 6px
}
.jumbotron .container {
  max-width: 100%
}
@media screen and (min-width:768px) {
.jumbotron {
  padding: 48px 0
}
.container .jumbotron, .container-fluid .jumbotron {
  padding-right: 60px;
  padding-left: 60px
}
.jumbotron h1, .jumbotron .h1 {
  font-size: 63px
}
}
.thumbnail {
  display: block;
  padding: 4px;
  margin-bottom: 20px;
  line-height: 1.42857143;
  background-color: #fff;
  border: 1px solid #ddd;
  border-radius: 4px;
  -webkit-transition: border .2s ease-in-out;
  -o-transition: border .2s ease-in-out;
  transition: border .2s ease-in-out
}
.thumbnail>img, .thumbnail a>img {
  margin-right: auto;
  margin-left: auto
}
a.thumbnail:hover, a.thumbnail:focus, a.thumbnail.active {
  border-color: #337ab7
}
.thumbnail .caption {
  padding: 9px;
  color: #333
}
.alert {
  padding: 15px;
  margin-bottom: 20px;
  border: 1px solid transparent;
  border-radius: 4px
}
.alert h4 {
  margin-top: 0;
  color: inherit
}
.alert .alert-link {
  font-weight: 700
}
.alert>p, .alert>ul {
  margin-bottom: 0
}
.alert>p+p {
  margin-top: 5px
}
.alert-dismissable, .alert-dismissible {
  padding-right: 35px
}
.alert-dismissable .close, .alert-dismissible .close {
  position: relative;
  top: -2px;
  right: -21px;
  color: inherit
}
.alert-success {
  color: #3c763d;
  background-color: #dff0d8;
  border-color: #d6e9c6
}
.alert-success hr {
  border-top-color: #c9e2b3
}
.alert-success .alert-link {
  color: #2b542c
}
.alert-info {
  color: #31708f;
  background-color: #d9edf7;
  border-color: #bce8f1
}
.alert-info hr {
  border-top-color: #a6e1ec
}
.alert-info .alert-link {
  color: #245269
}
.alert-warning {
  color: #8a6d3b;
  background-color: #fcf8e3;
  border-color: #faebcc
}
.alert-warning hr {
  border-top-color: #f7e1b5
}
.alert-warning .alert-link {
  color: #66512c
}
.alert-danger {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1
}
.alert-danger hr {
  border-top-color: #e4b9c0
}
.alert-danger .alert-link {
  color: #843534
}
@-webkit-keyframes progress-bar-stripes {
from {
background-position:40px 0
}
to {
  background-position: 0 0
}
}
@-o-keyframes progress-bar-stripes {
from {
background-position:40px 0
}
to {
  background-position: 0 0
}
}
@keyframes progress-bar-stripes {
from {
background-position:40px 0
}
to {
  background-position: 0 0
}
}
.progress {
  height: 20px;
  margin-bottom: 20px;
  overflow: hidden;
  background-color: #f5f5f5;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,.1);
  box-shadow: inset 0 1px 2px rgba(0,0,0,.1)
}
.progress-bar {
  float: left;
  width: 0;
  height: 100%;
  font-size: 12px;
  line-height: 20px;
  color: #fff;
  text-align: center;
  background-color: #337ab7;
  -webkit-box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
  box-shadow: inset 0 -1px 0 rgba(0,0,0,.15);
  -webkit-transition: width .6s ease;
  -o-transition: width .6s ease;
  transition: width .6s ease
}
.progress-striped .progress-bar, .progress-bar-striped {
  background-image: -webkit-linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent);
  background-image: -o-linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent);
  background-image: linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent);
  -webkit-background-size: 40px 40px;
  background-size: 40px 40px
}
.progress.active .progress-bar, .progress-bar.active {
  -webkit-animation: progress-bar-stripes 2s linear infinite;
  -o-animation: progress-bar-stripes 2s linear infinite;
  animation: progress-bar-stripes 2s linear infinite
}
.progress-bar-success {
  background-color: #5cb85c
}
.progress-striped .progress-bar-success {
  background-image: -webkit-linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent);
  background-image: -o-linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent);
  background-image: linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent)
}
.progress-bar-info {
  background-color: #5bc0de
}
.progress-striped .progress-bar-info {
  background-image: -webkit-linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent);
  background-image: -o-linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent);
  background-image: linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent)
}
.progress-bar-warning {
  background-color: #f0ad4e
}
.progress-striped .progress-bar-warning {
  background-image: -webkit-linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent);
  background-image: -o-linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent);
  background-image: linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent)
}
.progress-bar-danger {
  background-color: #d9534f
}
.progress-striped .progress-bar-danger {
  background-image: -webkit-linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent);
  background-image: -o-linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent);
  background-image: linear-gradient(45deg, rgba(255,255,255,.15) 25%, transparent 25%, transparent 50%, rgba(255,255,255,.15) 50%, rgba(255,255,255,.15) 75%, transparent 75%, transparent)
}
.media {
  margin-top: 15px
}
.media:first-child {
  margin-top: 0
}
.media-right, .media>.pull-right {
  padding-left: 10px
}
.media-left, .media>.pull-left {
  padding-right: 10px
}
.media-left, .media-right, .media-body {
  display: table-cell;
  vertical-align: top
}
.media-middle {
  vertical-align: middle
}
.media-bottom {
  vertical-align: bottom
}
.media-heading {
  margin-top: 0;
  margin-bottom: 5px
}
.media-list {
  padding-left: 0;
  list-style: none
}
.list-group {
  padding-left: 0;
  margin-bottom: 20px
}
.list-group-item {
  position: relative;
  display: block;
  padding: 10px 15px;
  margin-bottom: -1px;
  background-color: #fff;
  border: 1px solid #ddd
}
.list-group-item:first-child {
  border-top-left-radius: 4px;
  border-top-right-radius: 4px
}
.list-group-item:last-child {
  margin-bottom: 0;
  border-bottom-right-radius: 4px;
  border-bottom-left-radius: 4px
}
a.list-group-item {
  color: #555
}
a.list-group-item .list-group-item-heading {
  color: #333
}
a.list-group-item:hover, a.list-group-item:focus {
  color: #555;
  text-decoration: none;
  background-color: #f5f5f5
}
.list-group-item.disabled, .list-group-item.disabled:hover, .list-group-item.disabled:focus {
  color: #777;
  cursor: not-allowed;
  background-color: #eee
}
.list-group-item.disabled .list-group-item-heading, .list-group-item.disabled:hover .list-group-item-heading, .list-group-item.disabled:focus .list-group-item-heading {
  color: inherit
}
.list-group-item.disabled .list-group-item-text, .list-group-item.disabled:hover .list-group-item-text, .list-group-item.disabled:focus .list-group-item-text {
  color: #777
}
.list-group-item.active, .list-group-item.active:hover, .list-group-item.active:focus {
  z-index: 2;
  color: #fff;
  background-color: #337ab7;
  border-color: #337ab7
}
.list-group-item.active .list-group-item-heading, .list-group-item.active:hover .list-group-item-heading, .list-group-item.active:focus .list-group-item-heading, .list-group-item.active .list-group-item-heading>small, .list-group-item.active:hover .list-group-item-heading>small, .list-group-item.active:focus .list-group-item-heading>small, .list-group-item.active .list-group-item-heading>.small, .list-group-item.active:hover .list-group-item-heading>.small, .list-group-item.active:focus .list-group-item-heading>.small {
  color: inherit
}
.list-group-item.active .list-group-item-text, .list-group-item.active:hover .list-group-item-text, .list-group-item.active:focus .list-group-item-text {
  color: #c7ddef
}
.list-group-item-success {
  color: #3c763d;
  background-color: #dff0d8
}
a.list-group-item-success {
  color: #3c763d
}
a.list-group-item-success .list-group-item-heading {
  color: inherit
}
a.list-group-item-success:hover, a.list-group-item-success:focus {
  color: #3c763d;
  background-color: #d0e9c6
}
a.list-group-item-success.active, a.list-group-item-success.active:hover, a.list-group-item-success.active:focus {
  color: #fff;
  background-color: #3c763d;
  border-color: #3c763d
}
.list-group-item-info {
  color: #31708f;
  background-color: #d9edf7
}
a.list-group-item-info {
  color: #31708f
}
a.list-group-item-info .list-group-item-heading {
  color: inherit
}
a.list-group-item-info:hover, a.list-group-item-info:focus {
  color: #31708f;
  background-color: #c4e3f3
}
a.list-group-item-info.active, a.list-group-item-info.active:hover, a.list-group-item-info.active:focus {
  color: #fff;
  background-color: #31708f;
  border-color: #31708f
}
.list-group-item-warning {
  color: #8a6d3b;
  background-color: #fcf8e3
}
a.list-group-item-warning {
  color: #8a6d3b
}
a.list-group-item-warning .list-group-item-heading {
  color: inherit
}
a.list-group-item-warning:hover, a.list-group-item-warning:focus {
  color: #8a6d3b;
  background-color: #faf2cc
}
a.list-group-item-warning.active, a.list-group-item-warning.active:hover, a.list-group-item-warning.active:focus {
  color: #fff;
  background-color: #8a6d3b;
  border-color: #8a6d3b
}
.list-group-item-danger {
  color: #a94442;
  background-color: #f2dede
}
a.list-group-item-danger {
  color: #a94442
}
a.list-group-item-danger .list-group-item-heading {
  color: inherit
}
a.list-group-item-danger:hover, a.list-group-item-danger:focus {
  color: #a94442;
  background-color: #ebcccc
}
a.list-group-item-danger.active, a.list-group-item-danger.active:hover, a.list-group-item-danger.active:focus {
  color: #fff;
  background-color: #a94442;
  border-color: #a94442
}
.list-group-item-heading {
  margin-top: 0;
  margin-bottom: 5px
}
.list-group-item-text {
  margin-bottom: 0;
  line-height: 1.3
}
.panel {
  margin-top:0;
  background-color: #fff;
  border-radius: 0px;
  -webkit-box-shadow: 0 1px 1px rgba(0,0,0,.05);
}
.panel-body {
  padding:15px
}
.panel-heading {
  padding: 15px 15px;
  border-bottom: 0px solid transparent;
  border-top-left-radius: 0px;
  border-top-right-radius: 0px
}
.panel-heading>.dropdown .dropdown-toggle {
  color: inherit
}
.panel-title {
  margin-top: 0;
  margin-bottom: 0;
  font-size: 16px;
  color: inherit
}
.panel-title>a {
  color: inherit;
  display:block;
  text-decoration:none;
}
.panel-footer {
  padding: 10px 15px;
  background-color: #f5f5f5;
  border-top: 1px solid #ddd;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px
}
.panel>.list-group, .panel>.panel-collapse>.list-group {
  margin-bottom: 0
}
.panel>.list-group .list-group-item, .panel>.panel-collapse>.list-group .list-group-item {
  border-width: 1px 0;
  border-radius: 0
}
.panel>.list-group:first-child .list-group-item:first-child, .panel>.panel-collapse>.list-group:first-child .list-group-item:first-child {
  border-top: 0;
  border-top-left-radius: 3px;
  border-top-right-radius: 3px
}
.panel>.list-group:last-child .list-group-item:last-child, .panel>.panel-collapse>.list-group:last-child .list-group-item:last-child {
  border-bottom: 0;
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px
}
.panel-heading+.list-group .list-group-item:first-child {
  border-top-width: 0
}
.list-group+.panel-footer {
  border-top-width: 0
}
.panel>.table, .panel>.table-responsive>.table, .panel>.panel-collapse>.table {
  margin-bottom: 0
}
.panel>.table caption, .panel>.table-responsive>.table caption, .panel>.panel-collapse>.table caption {
  padding-right: 15px;
  padding-left: 15px
}
.panel>.table:first-child, .panel>.table-responsive:first-child>.table:first-child {
  border-top-left-radius: 3px;
  border-top-right-radius: 3px
}
.panel>.table:first-child>thead:first-child>tr:first-child, .panel>.table-responsive:first-child>.table:first-child>thead:first-child>tr:first-child, .panel>.table:first-child>tbody:first-child>tr:first-child, .panel>.table-responsive:first-child>.table:first-child>tbody:first-child>tr:first-child {
  border-top-left-radius: 3px;
  border-top-right-radius: 3px
}
.panel>.table:first-child>thead:first-child>tr:first-child td:first-child, .panel>.table-responsive:first-child>.table:first-child>thead:first-child>tr:first-child td:first-child, .panel>.table:first-child>tbody:first-child>tr:first-child td:first-child, .panel>.table-responsive:first-child>.table:first-child>tbody:first-child>tr:first-child td:first-child, .panel>.table:first-child>thead:first-child>tr:first-child th:first-child, .panel>.table-responsive:first-child>.table:first-child>thead:first-child>tr:first-child th:first-child, .panel>.table:first-child>tbody:first-child>tr:first-child th:first-child, .panel>.table-responsive:first-child>.table:first-child>tbody:first-child>tr:first-child th:first-child {
  border-top-left-radius: 3px
}
.panel>.table:first-child>thead:first-child>tr:first-child td:last-child, .panel>.table-responsive:first-child>.table:first-child>thead:first-child>tr:first-child td:last-child, .panel>.table:first-child>tbody:first-child>tr:first-child td:last-child, .panel>.table-responsive:first-child>.table:first-child>tbody:first-child>tr:first-child td:last-child, .panel>.table:first-child>thead:first-child>tr:first-child th:last-child, .panel>.table-responsive:first-child>.table:first-child>thead:first-child>tr:first-child th:last-child, .panel>.table:first-child>tbody:first-child>tr:first-child th:last-child, .panel>.table-responsive:first-child>.table:first-child>tbody:first-child>tr:first-child th:last-child {
  border-top-right-radius: 3px
}
.panel>.table:last-child, .panel>.table-responsive:last-child>.table:last-child {
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px
}
.panel>.table:last-child>tbody:last-child>tr:last-child, .panel>.table-responsive:last-child>.table:last-child>tbody:last-child>tr:last-child, .panel>.table:last-child>tfoot:last-child>tr:last-child, .panel>.table-responsive:last-child>.table:last-child>tfoot:last-child>tr:last-child {
  border-bottom-right-radius: 3px;
  border-bottom-left-radius: 3px
}
.panel>.table:last-child>tbody:last-child>tr:last-child td:first-child, .panel>.table-responsive:last-child>.table:last-child>tbody:last-child>tr:last-child td:first-child, .panel>.table:last-child>tfoot:last-child>tr:last-child td:first-child, .panel>.table-responsive:last-child>.table:last-child>tfoot:last-child>tr:last-child td:first-child, .panel>.table:last-child>tbody:last-child>tr:last-child th:first-child, .panel>.table-responsive:last-child>.table:last-child>tbody:last-child>tr:last-child th:first-child, .panel>.table:last-child>tfoot:last-child>tr:last-child th:first-child, .panel>.table-responsive:last-child>.table:last-child>tfoot:last-child>tr:last-child th:first-child {
  border-bottom-left-radius: 3px
}
.panel>.table:last-child>tbody:last-child>tr:last-child td:last-child, .panel>.table-responsive:last-child>.table:last-child>tbody:last-child>tr:last-child td:last-child, .panel>.table:last-child>tfoot:last-child>tr:last-child td:last-child, .panel>.table-responsive:last-child>.table:last-child>tfoot:last-child>tr:last-child td:last-child, .panel>.table:last-child>tbody:last-child>tr:last-child th:last-child, .panel>.table-responsive:last-child>.table:last-child>tbody:last-child>tr:last-child th:last-child, .panel>.table:last-child>tfoot:last-child>tr:last-child th:last-child, .panel>.table-responsive:last-child>.table:last-child>tfoot:last-child>tr:last-child th:last-child {
  border-bottom-right-radius: 3px
}
.panel>.panel-body+.table, .panel>.panel-body+.table-responsive, .panel>.table+.panel-body, .panel>.table-responsive+.panel-body {
  border-top: 1px solid #ddd
}
.panel>.table>tbody:first-child>tr:first-child th, .panel>.table>tbody:first-child>tr:first-child td {
  border-top: 0
}
.panel>.table-bordered, .panel>.table-responsive>.table-bordered {
  border: 0
}
.panel>.table-bordered>thead>tr>th:first-child, .panel>.table-responsive>.table-bordered>thead>tr>th:first-child, .panel>.table-bordered>tbody>tr>th:first-child, .panel>.table-responsive>.table-bordered>tbody>tr>th:first-child, .panel>.table-bordered>tfoot>tr>th:first-child, .panel>.table-responsive>.table-bordered>tfoot>tr>th:first-child, .panel>.table-bordered>thead>tr>td:first-child, .panel>.table-responsive>.table-bordered>thead>tr>td:first-child, .panel>.table-bordered>tbody>tr>td:first-child, .panel>.table-responsive>.table-bordered>tbody>tr>td:first-child, .panel>.table-bordered>tfoot>tr>td:first-child, .panel>.table-responsive>.table-bordered>tfoot>tr>td:first-child {
  border-left: 0
}
.panel>.table-bordered>thead>tr>th:last-child, .panel>.table-responsive>.table-bordered>thead>tr>th:last-child, .panel>.table-bordered>tbody>tr>th:last-child, .panel>.table-responsive>.table-bordered>tbody>tr>th:last-child, .panel>.table-bordered>tfoot>tr>th:last-child, .panel>.table-responsive>.table-bordered>tfoot>tr>th:last-child, .panel>.table-bordered>thead>tr>td:last-child, .panel>.table-responsive>.table-bordered>thead>tr>td:last-child, .panel>.table-bordered>tbody>tr>td:last-child, .panel>.table-responsive>.table-bordered>tbody>tr>td:last-child, .panel>.table-bordered>tfoot>tr>td:last-child, .panel>.table-responsive>.table-bordered>tfoot>tr>td:last-child {
  border-right: 0
}
.panel>.table-bordered>thead>tr:first-child>td, .panel>.table-responsive>.table-bordered>thead>tr:first-child>td, .panel>.table-bordered>tbody>tr:first-child>td, .panel>.table-responsive>.table-bordered>tbody>tr:first-child>td, .panel>.table-bordered>thead>tr:first-child>th, .panel>.table-responsive>.table-bordered>thead>tr:first-child>th, .panel>.table-bordered>tbody>tr:first-child>th, .panel>.table-responsive>.table-bordered>tbody>tr:first-child>th {
  border-bottom: 0
}
.panel>.table-bordered>tbody>tr:last-child>td, .panel>.table-responsive>.table-bordered>tbody>tr:last-child>td, .panel>.table-bordered>tfoot>tr:last-child>td, .panel>.table-responsive>.table-bordered>tfoot>tr:last-child>td, .panel>.table-bordered>tbody>tr:last-child>th, .panel>.table-responsive>.table-bordered>tbody>tr:last-child>th, .panel>.table-bordered>tfoot>tr:last-child>th, .panel>.table-responsive>.table-bordered>tfoot>tr:last-child>th {
  border-bottom: 0
}
.panel>.table-responsive {
  margin-bottom: 0;
  border: 0
}
.panel-group {
  margin-bottom: 0
}
.panel-group .panel {
  margin-bottom: 0;
  border-radius: 0px
}
.panel-group .panel+.panel {
  margin-top: 5px
}
.panel-group .panel-heading {
  border-bottom: 0
}
.panel-group .panel-heading+.panel-collapse>.panel-body, .panel-group .panel-heading+.panel-collapse>.list-group {
  border-top: 0px solid #ddd
}
.panel-group .panel-footer {
  border-top: 0
}
.panel-group .panel-footer+.panel-collapse .panel-body {
  border-bottom: 1px solid #ddd
}
.panel-default {
  border-color: #ddd
}
.panel-default>.panel-heading {
  color: #fff;
  background-color: #8a8a8a;
  border-color:
}
.panel-default>.panel-heading+.panel-collapse>.panel-body {
  border-top-color: #ddd
}
.panel-default>.panel-heading .badge {
  color: #f5f5f5;
  background-color: #333
}
.panel-default>.panel-footer+.panel-collapse>.panel-body {
  border-bottom-color: #ddd
}
.panel-primary {
  border-color: #337ab7
}
.panel-primary>.panel-heading {
  color: #fff;
  background-color: #337ab7;
  border-color: #337ab7
}
.panel-primary>.panel-heading+.panel-collapse>.panel-body {
  border-top-color: #337ab7
}
.panel-primary>.panel-heading .badge {
  color: #337ab7;
  background-color: #fff
}
.panel-primary>.panel-footer+.panel-collapse>.panel-body {
  border-bottom-color: #337ab7
}
.panel-success {
  border-color: #d6e9c6
}
.panel-success>.panel-heading {
  color: #3c763d;
  background-color: #dff0d8;
  border-color: #d6e9c6
}
.panel-success>.panel-heading+.panel-collapse>.panel-body {
  border-top-color: #d6e9c6
}
.panel-success>.panel-heading .badge {
  color: #dff0d8;
  background-color: #3c763d
}
.panel-success>.panel-footer+.panel-collapse>.panel-body {
  border-bottom-color: #d6e9c6
}
.panel-info {
  border-color: #bce8f1
}
.panel-info>.panel-heading {
  color: #31708f;
  background-color: #d9edf7;
  border-color: #bce8f1
}
.panel-info>.panel-heading+.panel-collapse>.panel-body {
  border-top-color: #bce8f1
}
.panel-info>.panel-heading .badge {
  color: #d9edf7;
  background-color: #31708f
}
.panel-info>.panel-footer+.panel-collapse>.panel-body {
  border-bottom-color: #bce8f1
}
.panel-warning {
  border-color: #faebcc
}
.panel-warning>.panel-heading {
  color: #8a6d3b;
  background-color: #fcf8e3;
  border-color: #faebcc
}
.panel-warning>.panel-heading+.panel-collapse>.panel-body {
  border-top-color: #faebcc
}
.panel-warning>.panel-heading .badge {
  color: #fcf8e3;
  background-color: #8a6d3b
}
.panel-warning>.panel-footer+.panel-collapse>.panel-body {
  border-bottom-color: #faebcc
}
.panel-danger {
  border-color: #ebccd1
}
.panel-danger>.panel-heading {
  color: #a94442;
  background-color: #f2dede;
  border-color: #ebccd1
}
.panel-danger>.panel-heading+.panel-collapse>.panel-body {
  border-top-color: #ebccd1
}
.panel-danger>.panel-heading .badge {
  color: #f2dede;
  background-color: #a94442
}
.panel-danger>.panel-footer+.panel-collapse>.panel-body {
  border-bottom-color: #ebccd1
}
.embed-responsive {
  position: relative;
  display: block;
  height: 0;
  padding: 0;
  overflow: hidden
}
.embed-responsive .embed-responsive-item, .embed-responsive iframe, .embed-responsive embed, .embed-responsive object, .embed-responsive video {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  width: 100%;
  height: 100%;
  border: 0
}
.embed-responsive.embed-responsive-16by9 {
  padding-bottom: 56.25%
}
.embed-responsive.embed-responsive-4by3 {
  padding-bottom: 75%
}
.well {
  min-height: 20px;
  padding: 19px;
  margin-bottom: 20px;
  background-color: #f5f5f5;
  border: 1px solid #e3e3e3;
  border-radius: 4px;
  -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);
  box-shadow: inset 0 1px 1px rgba(0,0,0,.05)
}
.well blockquote {
  border-color: #ddd;
  border-color: rgba(0,0,0,.15)
}
.well-lg {
  padding: 24px;
  border-radius: 6px
}
.well-sm {
  padding: 9px;
  border-radius: 3px
}
.close {
  float: right;
  font-size: 21px;
  font-weight: 700;
  line-height: 1;
  color: #000;
  text-shadow: 0 1px 0 #fff;
  filter: alpha(opacity=20);
  opacity:1
}
.close:hover, .close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
  filter: alpha(opacity=50);
  opacity: .5
}
button.close {
  -webkit-appearance: none;
  padding: 0;
  cursor: pointer;
  background: 0 0;
  border: 0
}
.modal-open {
  overflow: hidden
}
.modal {
  position: fixed;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  z-index: 9999;
  display: none;
  overflow: hidden;
  -webkit-overflow-scrolling: touch;
  outline: 0
}
.modal.fade .modal-dialog {
  -webkit-transition: -webkit-transform .3s ease-out;
  -o-transition: -o-transform .3s ease-out;
  transition: transform .3s ease-out;
  -webkit-transform: translate(0, -25%);
  -ms-transform: translate(0, -25%);
  -o-transform: translate(0, -25%);
  transform: translate(0, -25%)
}
.modal.in .modal-dialog {
  -webkit-transform: translate(0, 0);
  -ms-transform: translate(0, 0);
  -o-transform: translate(0, 0);
  transform: translate(0, 0)
}
.modal-open .modal {
  overflow-x: hidden;
  overflow-y: auto
}
.modal-dialog {
  position: relative;
  width: auto;
  margin: 10px
}
.modal-content {
  position: relative;
  background-color: #fff;
  -webkit-background-clip: padding-box;
  background-clip: padding-box;
  border: 1px solid #999;
  border: 1px solid rgba(0,0,0,.2);
  border-radius: 0px;
  outline: 0;
  -webkit-box-shadow: 0 3px 9px rgba(0,0,0,.5);
  box-shadow: 0 3px 9px rgba(0,0,0,.5)
}
.modal-backdrop {
  position: absolute;
  top: 0;
  right: 0;
  left: 0;
  background-color: #000
}
.modal-backdrop.fade {
  filter: alpha(opacity=0);
  opacity: 0
}
.modal-backdrop.in {
  filter: alpha(opacity=50);
  opacity: .5
}
.modal-header {
  min-height: 16.43px;
  padding: 15px;
}
.modal-header .close {
  margin-top: -2px
}
.modal-title {
  margin: 0;
  line-height: 1.42857143
}
.modal-body {
  position: relative;
  padding: 15px
}
.modal-footer {
  padding: 15px;
  text-align: right;
  border-top: 1px solid #e5e5e5
}
.modal-footer .btn+.btn {
  margin-bottom: 0;
  margin-left: 5px
}
.modal-footer .btn-group .btn+.btn {
  margin-left: -1px
}
.modal-footer .btn-block+.btn-block {
  margin-left: 0
}
.modal-scrollbar-measure {
  position: absolute;
  top: -9999px;
  width: 50px;
  height: 50px;
  overflow: scroll
}
@media (min-width:768px) {
.modal-dialog {
  width: 600px;
  margin: 30px auto
}
.modal-content {
  -webkit-box-shadow: 0 5px 15px rgba(0,0,0,.5);
  box-shadow: 0 5px 15px rgba(0,0,0,.5)
}
.modal-sm {
  width: 300px
}
}
@media (min-width:1024px) {
.modal-lg {
  width: 80%;
  max-width:1001px;
}
}
.tooltip {
  position: absolute;
  z-index: 1070;
  display: block;
  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-size: 12px;
  font-weight: 400;
  line-height: 1.4;
  visibility: visible;
  filter: alpha(opacity=0);
  opacity: 0
}
.tooltip.in {
  filter: alpha(opacity=90);
  opacity: .9
}
.tooltip.top {
  padding: 5px 0;
  margin-top: -3px
}
.tooltip.right {
  padding: 0 5px;
  margin-left: 3px
}
.tooltip.bottom {
  padding: 5px 0;
  margin-top: 3px
}
.tooltip.left {
  padding: 0 5px;
  margin-left: -3px
}
.tooltip-inner {
  max-width: 200px;
  padding: 3px 8px;
  color: #fff;
  text-align: center;
  text-decoration: none;
  background-color: #000;
  border-radius: 4px
}
.tooltip-arrow {
  position: absolute;
  width: 0;
  height: 0;
  border-color: transparent;
  border-style: solid
}
.tooltip.top .tooltip-arrow {
  bottom: 0;
  left: 50%;
  margin-left: -5px;
  border-width: 5px 5px 0;
  border-top-color: #000
}
.tooltip.top-left .tooltip-arrow {
  right: 5px;
  bottom: 0;
  margin-bottom: -5px;
  border-width: 5px 5px 0;
  border-top-color: #000
}
.tooltip.top-right .tooltip-arrow {
  bottom: 0;
  left: 5px;
  margin-bottom: -5px;
  border-width: 5px 5px 0;
  border-top-color: #000
}
.tooltip.right .tooltip-arrow {
  top: 50%;
  left: 0;
  margin-top: -5px;
  border-width: 5px 5px 5px 0;
  border-right-color: #000
}
.tooltip.left .tooltip-arrow {
  top: 50%;
  right: 0;
  margin-top: -5px;
  border-width: 5px 0 5px 5px;
  border-left-color: #000
}
.tooltip.bottom .tooltip-arrow {
  top: 0;
  left: 50%;
  margin-left: -5px;
  border-width: 0 5px 5px;
  border-bottom-color: #000
}
.tooltip.bottom-left .tooltip-arrow {
  top: 0;
  right: 5px;
  margin-top: -5px;
  border-width: 0 5px 5px;
  border-bottom-color: #000
}
.tooltip.bottom-right .tooltip-arrow {
  top: 0;
  left: 5px;
  margin-top: -5px;
  border-width: 0 5px 5px;
  border-bottom-color: #000
}
.popover {
  position: absolute;
  top: 0;
  left: 0;
  z-index: 1060;
  display: none;
  max-width: 276px;
  padding: 1px;
  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  font-size: 14px;
  font-weight: 400;
  line-height: 1.42857143;
  text-align: left;
  white-space: normal;
  background-color: #fff;
  -webkit-background-clip: padding-box;
  background-clip: padding-box;
  border: 1px solid #ccc;
  border: 1px solid rgba(0,0,0,.2);
  border-radius: 6px;
  -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
  box-shadow: 0 5px 10px rgba(0,0,0,.2)
}
.popover.top {
  margin-top: -10px
}
.popover.right {
  margin-left: 10px
}
.popover.bottom {
  margin-top: 10px
}
.popover.left {
  margin-left: -10px
}
.popover-title {
  padding: 8px 14px;
  margin: 0;
  font-size: 14px;
  background-color: #f7f7f7;
  border-bottom: 1px solid #ebebeb;
  border-radius: 5px 5px 0 0
}
.popover-content {
  padding: 9px 14px
}
.popover>.arrow, .popover>.arrow:after {
  position: absolute;
  display: block;
  width: 0;
  height: 0;
  border-color: transparent;
  border-style: solid
}
.popover>.arrow {
  border-width: 11px
}
.popover>.arrow:after {
  content: '';
  border-width: 10px
}
.popover.top>.arrow {
  bottom: -11px;
  left: 50%;
  margin-left: -11px;
  border-top-color: #999;
  border-top-color: rgba(0,0,0,.25);
  border-bottom-width: 0
}
.popover.top>.arrow:after {
  bottom: 1px;
  margin-left: -10px;
  content: ' ';
  border-top-color: #fff;
  border-bottom-width: 0
}
.popover.right>.arrow {
  top: 50%;
  left: -11px;
  margin-top: -11px;
  border-right-color: #999;
  border-right-color: rgba(0,0,0,.25);
  border-left-width: 0
}
.popover.right>.arrow:after {
  bottom: -10px;
  left: 1px;
  content: ' ';
  border-right-color: #fff;
  border-left-width: 0
}
.popover.bottom>.arrow {
  top: -11px;
  left: 50%;
  margin-left: -11px;
  border-top-width: 0;
  border-bottom-color: #999;
  border-bottom-color: rgba(0,0,0,.25)
}
.popover.bottom>.arrow:after {
  top: 1px;
  margin-left: -10px;
  content: ' ';
  border-top-width: 0;
  border-bottom-color: #fff
}
.popover.left>.arrow {
  top: 50%;
  right: -11px;
  margin-top: -11px;
  border-right-width: 0;
  border-left-color: #999;
  border-left-color: rgba(0,0,0,.25)
}
.popover.left>.arrow:after {
  right: 1px;
  bottom: -10px;
  content: ' ';
  border-right-width: 0;
  border-left-color: #fff
}
.carousel {
  position: relative
}
.carousel-inner {
  position: relative;
  width: 100%;
  overflow: hidden
}
.carousel-inner>.item {
  position: relative;
  display: none;
  -webkit-transition: .6s ease-in-out left;
  -o-transition: .6s ease-in-out left;
  transition: .6s ease-in-out left
}
.carousel-inner>.item>img, .carousel-inner>.item>a>img {
  line-height: 1; width:100%;
}
@media all and (transform-3d), (-webkit-transform-3d) {
.carousel-inner>.item {
  -webkit-transition: -webkit-transform .6s ease-in-out;
  -o-transition: -o-transform .6s ease-in-out;
  transition: transform .6s ease-in-out;
  -webkit-backface-visibility: hidden;
  backface-visibility: hidden;
  -webkit-perspective: 1000;
  perspective: 1000
}
.carousel-inner>.item.next, .carousel-inner>.item.active.right {
  left: 0;
  -webkit-transform: translate3d(100%, 0, 0);
  transform: translate3d(100%, 0, 0)
}
.carousel-inner>.item.prev, .carousel-inner>.item.active.left {
  left: 0;
  -webkit-transform: translate3d(-100%, 0, 0);
  transform: translate3d(-100%, 0, 0)
}
.carousel-inner>.item.next.left, .carousel-inner>.item.prev.right, .carousel-inner>.item.active {
  left: 0;
  -webkit-transform: translate3d(0, 0, 0);
  transform: translate3d(0, 0, 0)
}
}
.carousel-inner>.active, .carousel-inner>.next, .carousel-inner>.prev {
  display: block
}
.carousel-inner>.active {
  left: 0
}
.carousel-inner>.next, .carousel-inner>.prev {
  position: absolute;
  top: 0;
  width: 100%
}
.carousel-inner>.next {
  left: 100%
}
.carousel-inner>.prev {
  left: -100%
}
.carousel-inner>.next.left, .carousel-inner>.prev.right {
  left: 0
}
.carousel-inner>.active.left {
  left: -100%
}
.carousel-inner>.active.right {
  left: 100%
}
.carousel-control {
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  width: 15%;
  font-size: 20px;
  color: #fff;
  text-align: center;
  text-shadow: 0 1px 2px rgba(0,0,0,.6);
  filter: alpha(opacity=50);
  opacity: .5
}
.carousel-control.left {
  background-image: -webkit-linear-gradient(left, rgba(0,0,0,.5) 0, rgba(0,0,0,.0001) 100%);
  background-image: -o-linear-gradient(left, rgba(0,0,0,.5) 0, rgba(0,0,0,.0001) 100%);
  background-image: -webkit-gradient(linear, left top, right top, from(rgba(0,0,0,.5)), to(rgba(0,0,0,.0001)));
  background-image: linear-gradient(to right, rgba(0,0,0,.5) 0, rgba(0,0,0,.0001) 100%);
filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#80000000', endColorstr='#00000000', GradientType=1);
  background-repeat: repeat-x
}
.carousel-control.right {
  right: 0;
  left: auto;
  background-image: -webkit-linear-gradient(left, rgba(0,0,0,.0001) 0, rgba(0,0,0,.5) 100%);
  background-image: -o-linear-gradient(left, rgba(0,0,0,.0001) 0, rgba(0,0,0,.5) 100%);
  background-image: -webkit-gradient(linear, left top, right top, from(rgba(0,0,0,.0001)), to(rgba(0,0,0,.5)));
  background-image: linear-gradient(to right, rgba(0,0,0,.0001) 0, rgba(0,0,0,.5) 100%);
filter:progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#80000000', GradientType=1);
  background-repeat: repeat-x
}
.carousel-control:hover, .carousel-control:focus {
  color: #fff;
  text-decoration: none;
  filter: alpha(opacity=90);
  outline: 0;
  opacity: .9
}
.carousel-control .icon-prev, .carousel-control .icon-next, .carousel-control .glyphicon-chevron-left, .carousel-control .glyphicon-chevron-right {
  position: absolute;
  top: 50%;
  z-index: 5;
  display: inline-block
}
.carousel-control .icon-prev, .carousel-control .glyphicon-chevron-left {
  left: 50%;
  margin-left: -10px
}
.carousel-control .icon-next, .carousel-control .glyphicon-chevron-right {
  right: 50%;
  margin-right: -10px
}
.carousel-control .icon-prev, .carousel-control .icon-next {
  width: 20px;
  height: 20px;
  margin-top: -10px;
  font-family: serif
}
.carousel-control .icon-prev:before {
  content: '�9'
}
.carousel-control .icon-next:before {
  content: '�a'
}
.carousel-indicators {
  position: absolute;
  bottom: 10px;
  left: 50%;
  z-index: 15;
  width: 60%;
  padding-left: 0;
  margin-left: -30%;
  text-align: center;
  list-style: none
}
.carousel-indicators li {
  display: inline-block;
  width: 10px;
  height: 10px;
  margin: 1px;
  text-indent: -999px;
  cursor: pointer;
  background-color: #000 \9;
  background-color: rgba(0,0,0,0);
  border: 1px solid #5D5D5D;
  border-radius: 10px
}
.carousel-indicators .active {
  width: 10px;
  height: 10px;
  background-color: #5D5D5D
}
.carousel-caption {
  position: absolute;
  right: 15%;
  top: 20px;
  left: 15%;
  z-index: 10;
  padding-top: 20px;
  padding-bottom: 20px;
  color: #5D5D5D;
}
.carousel-caption .btn {
  text-shadow: none
}
@media screen and (min-width:768px) {
.carousel-control .glyphicon-chevron-left, .carousel-control .glyphicon-chevron-right, .carousel-control .icon-prev, .carousel-control .icon-next {
  width: 30px;
  height: 30px;
  margin-top: -15px;
  font-size: 30px
}
.carousel-control .glyphicon-chevron-left, .carousel-control .icon-prev {
  margin-left: -15px
}
.carousel-control .glyphicon-chevron-right, .carousel-control .icon-next {
  margin-right: -15px
}
.carousel-caption {
  right: 5%;
  left: 5%;
  padding-bottom: 30px
}
.carousel-indicators {
  bottom: 20px
}
}
.clearfix:before, .clearfix:after, .dl-horizontal dd:before, .dl-horizontal dd:after, .container:before, .container:after, .container-fluid:before, .container-fluid:after, .row:before, .row:after, .form-horizontal .form-group:before, .form-horizontal .form-group:after, .btn-toolbar:before, .btn-toolbar:after, .btn-group-vertical>.btn-group:before, .btn-group-vertical>.btn-group:after, .nav:before, .nav:after, .navbar:before, .navbar:after, .navbar-header:before, .navbar-header:after, .navbar-collapse:before, .navbar-collapse:after, .pager:before, .pager:after, .panel-body:before, .panel-body:after, .modal-footer:before, .modal-footer:after {
  display: table;
  content: ' '
}
.clearfix:after, .dl-horizontal dd:after, .container:after, .container-fluid:after, .row:after, .form-horizontal .form-group:after, .btn-toolbar:after, .btn-group-vertical>.btn-group:after, .nav:after, .navbar:after, .navbar-header:after, .navbar-collapse:after, .pager:after, .panel-body:after, .modal-footer:after {
  clear: both
}
.center-block {
  display: block;
  margin-right: auto;
  margin-left: auto
}
.pull-right {
  float: right!important
}
.pull-left {
  float: left!important
}
.hide {
  display: none!important
}
.show {
  display: block!important
}
.invisible {
  visibility: hidden
}
.text-hide {
  font: 0/0 a;
  color: transparent;
  text-shadow: none;
  background-color: transparent;
  border: 0
}
.hidden {
  display: none!important;
  visibility: hidden!important
}
.affix {
  position: fixed
}
@-ms-viewport {
width:device-width
}
.visible-xs, .visible-sm, .visible-md, .visible-lg {
  display: none!important
}
.visible-xs-block, .visible-xs-inline, .visible-xs-inline-block, .visible-sm-block, .visible-sm-inline, .visible-sm-inline-block, .visible-md-block, .visible-md-inline, .visible-md-inline-block, .visible-lg-block, .visible-lg-inline, .visible-lg-inline-block {
  display: none!important
}
@media (max-width:767px) {
.visible-xs {
  display: block!important
}
table.visible-xs {
  display: table
}
tr.visible-xs {
  display: table-row!important
}
th.visible-xs, td.visible-xs {
  display: table-cell!important
}
}
@media (max-width:767px) {
.visible-xs-block {
  display: block!important
}
}
@media (max-width:767px) {
.visible-xs-inline {
  display: inline!important
}
}
@media (max-width:767px) {
.visible-xs-inline-block {
  display: inline-block!important
}
}
@media (min-width:768px) and (max-width:991px) {
.visible-sm {
  display: block!important
}
table.visible-sm {
  display: table
}
tr.visible-sm {
  display: table-row!important
}
th.visible-sm, td.visible-sm {
  display: table-cell!important
}
}
@media (min-width:768px) and (max-width:991px) {
.visible-sm-block {
  display: block!important
}
}
@media (min-width:768px) and (max-width:991px) {
.visible-sm-inline {
  display: inline!important
}
}
@media (min-width:768px) and (max-width:991px) {
.visible-sm-inline-block {
  display: inline-block!important
}
}
@media (min-width:992px) and (max-width:1199px) {
.visible-md {
  display: block!important
}
table.visible-md {
  display: table
}
tr.visible-md {
  display: table-row!important
}
th.visible-md, td.visible-md {
  display: table-cell!important
}
}
@media (min-width:992px) and (max-width:1199px) {
.visible-md-block {
  display: block!important
}
}
@media (min-width:992px) and (max-width:1199px) {
.visible-md-inline {
  display: inline!important
}
}
@media (min-width:992px) and (max-width:1199px) {
.visible-md-inline-block {
  display: inline-block!important
}
}
@media (min-width:1200px) {
.visible-lg {
  display: block!important
}
table.visible-lg {
  display: table
}
tr.visible-lg {
  display: table-row!important
}
th.visible-lg, td.visible-lg {
  display: table-cell!important
}
}
@media (min-width:1200px) {
.visible-lg-block {
  display: block!important
}
}
@media (min-width:1200px) {
.visible-lg-inline {
  display: inline!important
}
}
@media (min-width:1200px) {
.visible-lg-inline-block {
  display: inline-block!important
}
}
@media (max-width:767px) {
.hidden-xs {
  display: none!important
}
}
@media (min-width:768px) and (max-width:991px) {
.hidden-sm {
  display: none!important
}
}
@media (min-width:992px) and (max-width:1199px) {
.hidden-md {
  display: none!important
}
}
@media (min-width:1200px) {
.hidden-lg {
  display: none!important
}
}
.visible-print {
  display: none!important
}
@media print {
.visible-print {
  display: block!important
}
table.visible-print {
  display: table
}
tr.visible-print {
  display: table-row!important
}
th.visible-print, td.visible-print {
  display: table-cell!important
}
}
.visible-print-block {
  display: none!important
}
@media print {
.visible-print-block {
  display: block!important
}
}
.visible-print-inline {
  display: none!important
}
@media print {
.visible-print-inline {
  display: inline!important
}
}
.visible-print-inline-block {
  display: none!important
}
@media print {
.visible-print-inline-block {
  display: inline-block!important
}
}
@media print {
.hidden-print {
  display: none!important
}
}
/*
custom css starts by dinesh
*/

 .inputBox2{
    border: 1px solid #e0e2e3;
    border-radius: 2px;
    box-shadow: 1px 1px 3px #eaeaea inset;
    float: left;
    padding: 10px 0;
    width: 100%;
    margin-right: 10px;
 }
 
 .inputBox2 .dropBg {
    background: rgba(0, 0, 0, 0) url('../img/grayArrow.png') no-repeat scroll right center;
    background-repeat: no-repeat;
    background-position: right center;
    margin-right: 10px;
}
.inputBox2 .dropBg select {
    background: rgba(0, 0, 0, 0) none repeat scroll 0 0;
    border: 0 none;
    color: #444444;
    font: 14px/25px 'Segoe_UI',Arial,Helvetica,sans-serif;
    width: 110%;
    -webkit-appearance: none;
    -moz-appearance: none;
     appearance: none;
     overflow:hidden;
    text-indent: 0.01px;
    text-overflow: '';
    overflow:hidden;
    margin-left:12px;
}
.inputBox2 .dropBg select::-ms-expand {
    display: none;
}
.greenSrchBtn2 {
  width:6%;
  float:right;
  padding:13px 5px;
  background:#5e9d2d;
  border:1px solid #0f6633;
  -webkit-border-radius:0px;
  -moz-border-radius:0px;
  border-radius:0px;
  margin:0 0px;
  text-indent:-999999px;
  cursor:pointer;
}
.sendviasmsbtn input[type='submit'] {
  background: #5e9d2d none repeat scroll 0 0;
  color: #fff;
    float: left;
    font-size: 18px;
    font-weight: 600;
    margin: 0;
    padding: 3%;
    text-shadow: 1px 1px #6e6e6e;
    width: 35%;
  
}
.sendviasmsbtn input[type='submit']:hover {
  background:#b7dc7d;
  transition: all 0.4s ease-in-out 0s;
  color:#FFF
}
#sendviaemailbtn{
  padding-bottom:20px;
  float:left;
  width:81%;
}
#sendviaemailbtn input[type='submit'] {
  background: #5e9d2d none repeat scroll 0 0;
  color: #fff;
    float: left;
    font-size: 18px;
    font-weight: 600;
    margin: 0;
    padding: 1.6%;
    text-shadow: 1px 1px #6e6e6e;
    width: 17%;
}
#sendviaemailbtn input[type='submit']:hover {
  background:#b7dc7d;
  transition: all 0.4s ease-in-out 0s;
  color:#FFF
}

/*
custom css ends by dinesh
*/

@media (min-width:768px) {
.networkInput{
  width: 30% !important; 
  float:left;
}
}
</style><!--<link href="css/bootstrap-theme.css" rel="stylesheet">-->
﻿<style type='text/css'> body {
  background:#eaeff5;
  color: #666;
  font-size: 16px;
  line-height:16px;
  font-family:'SEGOEUIL', Arial, Helvetica, sans-serif;
}
 @font-face {
font-family:'SEGOEUIL';
src:url('fonts/SEGOEUIL.eot?') format('eot'), url('fonts/SEGOEUIL.woff') format('woff'), url('fonts/SEGOEUIL.ttf') format('truetype');
font-weight:normal;
font-style:normal;
}
@font-face {
font-family:'seguisb_0';
src:url('fonts/seguisb_0.eot?') format('eot'), url('fonts/seguisb_0.woff') format('woff'), url('fonts/seguisb_0.ttf') format('truetype'), url('fonts/seguisb_0.svg#seguisb_0') format('svg');
font-weight:normal;
font-style:normal;
}
@font-face {
font-family:'Segoe_UI';
src:url('fonts/Segoe_UI.eot?') format('eot'), url('fonts/Segoe_UI.woff') format('woff'), url('fonts/Segoe_UI.ttf') format('truetype'), url('fonts/Segoe_UI.svg#Segoe_UI') format('svg');
font-weight:normal;
font-style:normal;
}
@font-face {
font-family:'WebRupee';
src:url('WebRupee.V2.0.eot');
src:local('WebRupee'), url('fonts/WebRupee.V2.0.ttf') format('truetype'), url('fonts/WebRupee.V2.0.woff') format('woff'), url('fonts/WebRupee.V2.0.svg') format('svg');
font-weight:normal;
font-style:normal;
}
.img-responsive{width:80%;}
.textbold-1{text-align:center; font-size:18px;}

.sorting{float:right;}
   .textbold{text-align:left; font-size:18px; margin:0 -30px;}
   .textbold p{text-align:left; font-size:14px !important; font-weight:300; margin:10px 0; line-height:24px;}

.topContainer {
  margin:0;
  padding:0 .5%;
}
.topContainer .con-in {
  padding:10px 0;
  margin:0;
  float:left
}
.topContainer .con-in form label {
  font-weight:normal;
  float:left;
  font-size:25px;
  color:#5D5D5D;
}
.topContainer .con-in form .form-control {
  height:38px;
  line-height:38px;
  width:200px;
  border-color:#dfe6ef;
  font-size:14px;
  color:#8d8d8d;
  font-weight:400;
  float:left;
  margin:0;
  border-radius: 0px;
  box-shadow: 0 0 0 rgba(0, 0, 0, 0.075)
}
.topContainer .con-in form .btn {
  float:left;
}
.topContainer .con-in1 {
  padding:0;
  margin:11px 0 2% 0;
  float:left;
  margin-left:-15px;
  margin-right:-15px;
}
.break-text {
  text-decoration:line-through;
}
.total-text {
  font-size:18px;
  text-transform:uppercase;
  font-weight:600;
}
.total-price {
  font-size:24px;
  text-transform:uppercase;
  color:#66a503;
  font-weight:600;
}
.tab-mid-container-buy {
  margin:0;
  padding:7% 0 6% 0;
  display:block;
  overflow:hidden;
  text-align:right;
}
.tab-mid-container-sm {
  margin:0;
  padding:1 1.5%;
  font-size:12px;
  color:#8a8a8a;
}
.tab-mid-container-sm span {
  font-size:12px;
  color:#5e9d2d;
  font-weight:600;
}
.tab-mid-container-buy a {
  margin:0;
  padding:3.5% 5%;
  background:#5e9d2d;
  border:none;
  color:#fff;
  font-weight:600;
  font-size:18px;
  text-shadow: 1px 1px #6e6e6e;
}
.tab-mid-container-buy:hover {
  text-decoration:none;
  transition: all 0.4s ease-in-out 0s;
  color:#FFF
}
.slect-border {
  border-left:1px solid #999;
  height:25px;
  padding:50px;
}
.topContainer .con-in form .btn-danger {
  background:#fff url(img/search-icon.png) no-repeat 10px center;
  border-color: #dfe6ef;
  border-left:0;
  border-radius:0px;
  padding:8px 22px;
  height:38px;
}
.topContainer .con-in form .btn-danger:hover, .btn-danger:focus {
  background:#fafafa url(img/search-icon.png) no-repeat 10px center;
  border-color: #dfe6ef;
}
.topContainer-logo {
  height:auto;
  padding:15px 0 0 0;
  float:left;
  margin-right:30px;
}
.topContainer-logo img {
  margin:0;
}


.topContainer-us-icon {
  margin:0;
  margin-right:-7px;
  z-index:9999;
  padding:0;
}
.topContainer-us-icon ul {
  margin:0;
  padding:0;
}
.topContainer-us-icon li {
  margin:0;
  margin-top:10px;
  padding-left:5px;
  list-style:none;
  float:right;
}
.topContainer-us-icon li a {
  border-radius: 20px;
  display: block;
  font-size: 24px;
  height: 40px;
  line-height: 40px;
  padding: 0;
  display:block;
  text-align: center;
  width: 40px;
  background:#fff;
}
.topContainer-us-icon .dropdown-user {
  right: 0;
  left: auto;
  padding: 10px 15px !important;
}
.topContainer-us-icon .dropdown-user h1 {
  margin:5px 0 10px 0 !important;
  padding:0;
  font-size:24px !important;
  color:#0f6633 !important;
}
.topContainer-us-icon .dropdown-user li {
  float:inherit;
  display:block !important;
  width:100%;
}
.topContainer-us-icon .dropdown-user li a {
  float:left;
  background:#eaeff5;
  display:block;
  margin:5px 0;
  padding:10px 10px 12px 10px!important;
  width:98%;
  text-align:left;
  border-radius: 0px;
  font-size:18px;
  color:#5f5f5f;
  line-height:inherit;
  height:inherit;
}
.topContainer-us-icon .dropdown-user li a:hover {
  background:#8a8a8a;
  color:#fff;
}
.topContainer-us-icon .dropdown-alerts {
  right: 0;
  left: auto;
}
.topContainer-us-icon .dropdown-alerts h1 {
  margin:5px 0 10px 0 !important;
  padding: 10px 15px !important;
  font-size:24px !important;
  color:#0f6633 !important;
}
.topContainer-us-icon .dropdown-alerts h2 {
  margin:5px 0!important;
  padding:0 !important;
  font-size:14px !important;
  color:#444 !important;
  font-weight:700;
}
.topContainer-us-icon .dropdown-alerts p {
  margin:0;
  padding:0;
  font-size:14px !important;
  color:#777777 !important;
}
.topContainer-us-icon .dropdown-alerts li {
  float:right;
  width:90%;
  margin:0;
  margin-left:0;
  padding-left:0;
  padding-right:0;
  border-right:0 !important;
  border-left:5px solid #cddfea !important;
}
.topContainer-us-icon .dropdown-alerts li a {
  float:left;
  background:#fdfdfd;
  display:block;
  margin:0;
  padding:10px 10px 12px 10px!important;
  width:100%;
  text-align:left;
  border-radius: 0px;
  font-size:18px;
  color:#5f5f5f;
  line-height:inherit;
  height:inherit;
}
.dropdown-menu .divider {
  background-color: #e5e5e5;
  height: 1px;
  margin:0;
  overflow: hidden;
  width:100% !important;
}
.topContainer-us-icon .dropdown-tasks {
  right: 0;
  left: auto;
  padding: 10px 15px !important;
}
.topContainer-us-icon .dropdown-tasks h1 {
  margin:5px 0 10px 0 !important;
  padding:0;
  font-size:24px !important;
  color:#0f6633 !important;
}
.topContainer-us-icon .dropdown-tasks li {
  float:left;
  width:100%;
  margin:5px 0;
  display:block;
}
.topContainer-us-icon .dropdown-tasks {
  padding:10px 0;
  margin:0;
  float:left
}
.topContainer-us-icon .dropdown-tasks form label {
  font-weight:normal;
  float:left;
  font-size:25px;
  color:#5D5D5D;
}
.topContainer-us-icon .dropdown-tasks form .form-control {
  height:38px;
  line-height:38px;
  width:99%;
  background:#eaeff5;
  border-color:#dfe6ef;
  font-size:14px;
  color:#8d8d8d;
  font-weight:400;
  float:left;
  margin:0;
  border-radius: 0px;
  box-shadow: 0 0 0 rgba(0, 0, 0, 0.075)
}
.topContainer-us-icon .dropdown-tasks form .btn {
  float:left;
  width:99%;
  background:#8a8a8a;
  color:#FFF;
  font-size:18px;
  border-radius: 0px;
  border:1px solid #000;
}
.topContainer-us-icon .dropdown-tasks form .btn:hover {
  background:#121921;
  color:#FFF;
  transition: all 0.4s ease-in-out 0s;
}
.Quote-box input[type='text'], .Quote-box textarea {
  margin:0;
  padding:5px 10px;
  border-radius:0px;
  background:#eaeff5;
  -webkit-border-radius:4px;
  -moz-border-radius:4px;
  border:1px solid #d6dcce;
  width:99%;
  display:block
}
.Quote-box input[type='text']:hover, .Quote-box input[type='text']:hover, .Quote-box textarea:hover, .Quote-box textarea:focus {
  transition: all 0.4s ease-in-out 0s;
  border-color:#8a8a8a
}
.dropdown-menu {
  background-clip: padding-box;
  background-color: #fff;
  border-top:0 !important;
  border: 1px solid rgba(0, 0, 0, 0.15);
  border-radius: 0px;
  box-shadow: 0 6px 12px rgba(0, 0, 0, 0.176);
  display: none;
  float: left;
  font-size: 14px;
  left: 0;
  list-style: none outside none;
  margin: 7px 0 0 !important;
  padding: 10px 0 !important;
  position: absolute;
  text-align: left;
  top: 100%;
  z-index: 1000;
  width:300px;
}
.dropdown-menu.pull-right {
  left: auto;
  right: 0;
}
.graytxtBox-Location {
  width:42%;
  float:left;
  padding:5px 8px;
  background:#fff;
  border:1px solid #e0e2e3;
  -webkit-border-radius:0px;
  -moz-border-radius:0px;
  border-radius:0px;
  margin:0 10px 0px 0px;
}
.graytxtBox-Spec {
  width:50%;
  float:left;
  padding:5px 8px;
  background:#fff;
  border:1px solid #e0e2e3;
  -webkit-border-radius:0px;
  -moz-border-radius:0px;
  border-radius:0px;
  margin:0 0px;
}
.txtMrgn {
  margin:0 8px 0 45px;
}
.txtfieldFull {
  width:100%;
  background:none;
  border:0px;
  color:#4444;
  font-size:16px;
  padding:7px 0px;
}
.greenSrchBtn {
  width:10%;
  float:right;
  padding:12px 5px;
  background:url(img/srch-icon.png) center center no-repeat #0f6633;
  border:1px solid #0f6633;
  -webkit-border-radius:0px;
  -moz-border-radius:5px;
  border-radius:5px;
  margin:0 0px;
  text-indent:-999999px;
  cursor:pointer;
}
.greenSrchBtn {
  width:6%;
  float:right;
  padding:13px 5px;
  background:url(img/srch-icon.png) center center no-repeat #5e9d2d;
  border:1px solid #0f6633;
  -webkit-border-radius:0px;
  -moz-border-radius:0px;
  border-radius:0px;
  margin:0 0px;
  text-indent:-999999px;
  cursor:pointer;
}
.existingSincedd {
  width:41%;
  overflow:hidden;
  padding:7px 1px;
  background:url(img/gray-arrow1.png) right center no-repeat #ffffff;
  border:1px solid #dbdbdb;
  -webkit-border-radius:0px;
  -moz-border-radius:5px;
  border-radius:5px;
  display:inline-block;
}
.existingSincedd select {
  width:132%;
  background:none;
  border:0px;
  color:#000000;
  font:normal 15px/25px 'Segoe_UI', Arial, Helvetica, sans-serif;
  outline:none;
}
.existingSincemm {
  width:35%;
  overflow:hidden;
  padding:7px 1px;
  background:url(img/gray-arrow1.png) right center no-repeat #ffffff;
  border:1px solid #dbdbdb;
  -webkit-border-radius:0px;
  -moz-border-radius:0px;
  border-radius:0px;
  display:inline-block;
  margin-right:4px;
}
.existingSincemm select {
  width:139%;
  background:none;
  border:0px;
  color:#000000;
  font:normal 15px/25px 'Segoe_UI', Arial, Helvetica, sans-serif;
  outline:none;
}
.zigzaghospital {
  background:url(img/zigzaghhos.png) right center no-repeat;
}
section.sectionInner .sectionRight .brochureOuter .brochureOuterIn .hospitalLocationResultMain .hospitalLocationResultMainIn {
  margin:0 0 0 0px;
  border:1px solid #dedede;
  background:#f9f9f9;
}
.hospitalLocationSearch {
  float:left;
  width:100%;
  padding-bottom:20px;
}
.positionHospital{
  float:right;
}
.hospitalText{
  clear:both; 
  font-size:13px;
  margin-bottom:10px;
}
.hospitalLocationResultMain {
  float:left;
  width:100%;
  margin:4px 0px;
}
.hospitalLocationResultMainIn {
  margin:0 0 0 0px;
  border:1px solid #dedede;
  background:#f9f9f9;
  overflow:hidden;
}
.hospitalLocationResultMainIn .addressBox {
  width:50%;
  padding:12px 0px;
}
.hospitalLocationResultMainIn .addressBoxLeft {
  float:left;
}
.hospitalLocationResultMainIn .addressBoxRight {
  float:right;
}
.hospitalLocationResultMainIn .addressBox h3 {
  float:left;
  width:100%;
  padding:0px 0px;
  margin:0 0px;
  cursor:pointer;
}
.selectColorMain {
  background: none repeat scroll 0 0 #e4ffc6 !important;
}
.addressBox h3 {
  cursor: pointer;
  float: left;
  margin: 0;
  padding: 0;
  width: 100%;
}
.addressBox .addressDetail {
  float: left;
  padding-top: 12px;
  width: 100%;
}
.addressBox .addressDetail .addressDetailIn {
  margin: 0 0 0 35px;
  padding-right: 25px;
}
.addressDetailIn .locationTopBox .checkboxContainer {
  float: left;
  position: relative;
  width: 40px;
}
.addressDetailIn .locationTopBox .locationContainer {
  color: #000000;
  font: 14px/22px 'Segoe_UI', Arial, Helvetica, sans-serif;
  margin: 0 0 0 40px;
}

.spacer{ clear: both;
    height: 20px;}
 


.fl {
  float: left;
}
.addressDetailIn .locationTopBox {
  float: left;
  padding-bottom: 15px;
  width: 100%;
}
.addressBox h3 code {
  color: #000000;
  display: flex;
  font: 15px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
.addressBox h3 span {
  background: url('img/selectArrowgr1.png') no-repeat scroll center center rgba(0, 0, 0, 0);
}
.addressBox h3 span.srcharrowup1 {
  background: url('img/selectArrowgr1.png') no-repeat scroll center center rgba(0, 0, 0, 0) !important;
}
.hospitalLocationResultMainIn .addressBox h3 span.srcharrowup {
  background: url('img/selectArrowgr.png') no-repeat scroll center center rgba(0, 0, 0, 0);
  display: block;
  float: left;
  height: 24px;
  width: 35px;
}
.custom-checkbox {
  background: url('img/checkboximg.png') no-repeat scroll left top rgba(0, 0, 0, 0);
  display: block!important;
  height: 23px;
  left: 0;
  position: absolute;
  top: 0;
  width: 25px;
  z-index:999 !important;
}
.custom-checkboxActive {
  background: url('img/checkboximg1.png') no-repeat scroll left top rgba(0, 0, 0, 0)!Important;
  display: block;
  height: 23px;
  left: 0;
  position: absolute;
  top: 0;
  width: 25px;
}
.addressBox h3 span.srcharrowup {
  background: url('img/selectArrowgr.png') no-repeat scroll center center rgba(0, 0, 0, 0);
  display: block;
  float: left;
  height: 24px;
  width: 35px;
}
span.txtIcon1 {
  width:40px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -436px -594px no-repeat;
}
span.txtIcon2 {
  width:45px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -490px -593px no-repeat;
}
span.txtIcon3 {
  width:45px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -436px -640px no-repeat;
}
span.txtIcon4 {
  width:45px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -490px -640px no-repeat;
}
span.txtIcon5 {
  width:45px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -8px -798px no-repeat;
}
span.txtIcon6 {
  width:45px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -52px -794px no-repeat;
}
span.txtIcon7 {
  width:35px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -104px -794px no-repeat;
}
span.txtIcon8 {
  width:45px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -7px -842px no-repeat;
}
span.txtIcon9 {
  width:45px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -138px -845px no-repeat;
}
span.txtIcon10 {
  width:45px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -537px -637px no-repeat;
}
span.txtIcon11 {
  width:35px;
  height:32px;
  float:left;
  display:block;
  background:url(img/sprite-img.png) -420px -698px no-repeat;
}
/*------------------------- Dashboard -------------------*/

#middleContainer .dashboard-left {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #e0e2e3;
    margin: 0 0 3%;
    padding:2% 2% 4% 2%;
  overflow:hidden;
}
#middleContainer .dashboard-leftTop {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #e0e2e3;
    margin: 0 0 3%;
    padding:2% 2% 2% 2%;
}
#middleContainer .valueaddedBox {
    margin: 0 0 0%;
}
#middleContainer .valueaddedBox  .panel-group .panel + .panel {
    margin-top: 10px;
}
#middleContainer .valueaddedBox .panel-group .panel {
  margin-bottom: 0;
  border-radius: 0px;
  background: none repeat scroll 0 0 #fff;
    border: 1px solid #e0e2e3;
    padding:2% 2% 2% 2%;
}
#middleContainer .valueaddedBox .panel-default>.panel-heading {
  color: #312c29;
  background-color: #ffffff;
  border:1px solid #e0e2e3;
}
#middleContainer .valueaddedBox .panel-body {
    padding: 0px!Important;
  border: 1px solid #e0e2e3;
  border-top:0px;
}
#middleContainer .valueaddedBox .panel-title {
  margin-top: 0;
  margin-bottom: 0;
  font-size: 18px;
  color: inherit;
  font-family:'Segoe_UI',Arial,Helvetica,sans-serif;
}
#middleContainer .valueaddedBox .panel-title>a.collapsed {
  margin-top: 0;
  margin-bottom: 0;
  font-size: 18px;
  color: inherit;
  font-family:'Segoe_UI',Arial,Helvetica,sans-serif;
  background:url(img/selectArrowgr2.png) right center no-repeat;
}
#middleContainer .valueaddedBox .panel-title>a {
  margin-top: 0;
  margin-bottom: 0;
  font-size: 18px;
  color: inherit;
  font-family:'Segoe_UI',Arial,Helvetica,sans-serif;
  background:url(img/selectArrowgr1.png) right center no-repeat;
  outline:0px;
}
#middleContainer .valueaddedBox .panel-title strong {
    font-size: 30px;
    font-weight: 600;
}
#middleContainer .valueaddedBox .collapse {
  display: none;
  visibility: hidden;
  margin-top:-1px;
}
#middleContainer .valueaddedBox .collapse.in {
  display: block;
  visibility: visible;
  margin-top:-1px;
}
#middleContainer .dashboard-left h1{
    font-size:24px;
  color:;
  margin:0 0 3% 0;
  padding:0;
}
.topTittle{
    font-size:24px;
  color:#444444;
  margin:0 0 2% 0;
  padding:0;
}
#middleContainer h1{
    font-size:24px;
  color:#2e7d07;
  margin:0 0 2% 0;
  padding:0;
}
#middleContainer .dashboard-left h2{
    font-size:24px;
  color:#2e7d07;
  margin:0 0 3% 0;
  padding:0;
}

#middleContainer .dashboard-left h2 span{
    font-size:24px;
  color:;
  margin:0 0 3% 0;
  padding:0;
  font-style:normal;
}
#middleContainer .dashboard-left_box{
margin:0; padding:0;
font-weight:600;
}

#middleContainer .dashboard-left_box a{
    font-size:13px;
  background:;
  border:1px solid ;
  color:;
  margin:0;
  display:block;
  cursor:pointer;
  overflow:hidden;
  text-decoration:none;
  padding:5% 0;
  min-height:130px;
}


#middleContainer .dashboard-left_box a:hover{
  display:block;
  background: !important;
  text-decoration:none;
  color: !important;


}
#middleContainer .dashboard-left_box span{

  color:#2e7d07;
  margin-top:5px;
  display:block;
  clear:both;
  line-height:0px;
}



#middleContainer .dashboard-left_box IMG{
    text-align:left;
}




#middleContainer .dashboard-left_box1{
margin:0; padding:0;
font-weight:600;
}

#middleContainer .dashboard-left_box1 a{
    font-size:18px;
  background:;
  border:1px solid ;
  color:;
  margin:0;
  font-weight:300;
  display:block;
  cursor:pointer;
  line-height:60px;
  overflow:hidden;
  text-decoration:none;
  padding:5% 0;
}


#middleContainer .dashboard-left_box1 a:hover{
  display:block;
  background: !important;
  text-decoration:none;
    color: !important;


}
#middleContainer .dashboard-left_box1 a:hover{
  display:block;
  background: !important;
  text-decoration:none;
    color: !important;


}
.grayBorderdashboard{
  border:1px solid ;
  border-top:0px solid ;
}
.dashboardName{
  border-top:1px solid ;
  padding:12px 10px;
}
.dashboardName .colSm6{
  width:50%;
  float:left;
  font-size:18px;
  font-family:'Segoe_UI', Arial, Helvetica, sans-serif;
  line-height:30px;
}
.dashboardName .colSm6 strong{
  font-size:24px;
  font-weight:600;
  
}
.model-header{
  width:100%;
  font:normal 24px 'Segoe_UI', Arial, Helvetica, sans-serif;
  color:#2e7d07;
}
.modal-middle {
    padding: 5px 15px;
    position: relative;
}
.model-footer{
  padding: 10px 15px 20px 15px;
    position: relative;
}
.popupBrowsefull{
  padding:8px 0px;
  width:100%;
}
.myPlanformBoxfull{
  padding:0px 0px 8px 0px;
  width:100%;
}
.myPlanformBoxfull .inputBox{
  padding:10px 0px;
  border-radius:2px;
  border:1px solid #e0e2e3;
  float:left;
  width:100%;
  box-shadow:inset 1px 1px 3px #eaeaea;
}
.myPlanformBoxfull .inputBox .inputBoxIn{
  margin:0px 15px;
  height:25px;
  overflow:hidden;
}
.myPlanformBoxfull .inputBox .dropBg{
  background:url(img/grayArrow.png) right center no-repeat;
}
.myPlanformBoxfull .inputBox .dropBg select{
  background:none;
  border:0px;
  color:#444444;
  font:normal 14px/25px 'Segoe_UI', Arial, Helvetica, sans-serif;
  width:105%;
}
.femaleIcon{
  width:41px;
  height:41px;
  display:inline-block;
  background:url(img/femaleIcon.png) left center no-repeat;
  float:left;
}
.maleIcon{
  width:41px;
  height:41px;
  display:inline-block;
  background:url(img/maleIcon.png) left center no-repeat;
  float:left;
}
.editIcon{
  width:33px;
  height:36px;
  display:inline-block;
  background:url(img/editIcon.png) left center no-repeat;
  cursor:pointer;
}
.downloadIcon{
  width:23px;
  height:27px;
  display:inline-block;
  background:url(img/downloadIcon.png) left center no-repeat;
  cursor:pointer;
}
.umbIcon{
  width:33px;
  height:36px;
  display:inline-block;
  background:url(img/umbrellaIcon.png) left center no-repeat;
  margin-left:30px;
  cursor:pointer;
}
.dashboardName .colSm3{
  width:45px;
  float:right;
  text-align:right;
  margin-right:0px;
}
.dashboardName .colSm3 .dropdown-menu{
  margin-top:0px!Important;
  border: 1px solid rgba(0, 0, 0, 0.15)!Important;
  border-bottom:0px!Important;
  padding:0 0px!Important;
  width:150px!Important;
  right:0px!Important;
  left:-115px;
  margin-top:-1px;
}
.dashboardName .colSm3 .dropdown-menu>li>a {
  display: block;
  padding: 3px 20px;
  clear: both;
  font-weight: 400;
  line-height: 30px;
  color: #333;
  white-space: nowrap;
  border-bottom:1px solid #dfe6ef;
}
.locationtextfield {
  margin:0 1% 0px 0;
  padding:14px 15px 14px 4%;
  border:1px solid #e0e2e3;
  width:100%;
  font-size:16px;
  display:block;
  background:url(img/map-pin.png) no-repeat 3% center;
  width:46%;
  float:left;
}
.doctorsrchtextfield {
  margin:0 1% 0px 0;
  padding:14px 15px 14px 4%;
  border:1px solid #e0e2e3;
  width:100%;
  font-size:16px;
  display:block;
  background:url(img/search-icon1.png) no-repeat 3% center;
  width:46%;
  float:left;
}
#middleContainer .wecare-leftTop {
    margin: 0 0 3%;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #e0e2e3;
    padding:2% 2% 2% 2%;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .topBar {
    width:100%;
  float:left;
  border-bottom:1px solid #a5a5a5;
  padding:0 0 10px 0px;
  color:#444444;
  font:normal 24px 'Segoe_UI', Arial, Helvetica, sans-serif;
  margin-bottom:1%;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .topBar span {
  color:#919191;
  font:normal 16px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressLeft {
    width:40%;
  float:left;
  color:#444444;
  font:normal 16px/32px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressLeft p {
    width:100%;
  float:left;
  padding:0 0px;
  margin:0 0 10px 0px;
  color:#444444;
  font:normal 16px/32px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressLeft p span {
  color:#5e9d2d;
}
.smsIcon{
  width:46px;
  height:46px;
  display:inline-block;
  background:url(img/smsIcon.png) left center no-repeat;
  float:left;
}
.mailIcon{
  width:46px;
  height:46px;
  display:inline-block;
  background:url(img/mailIcon.png) left center no-repeat;
  float:left;
  margin-left:15px;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight {
  float:right;
  display:inline-block;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight .topStar {
  float:left;
  padding:0 0px;
  margin:0 0 10px 0px;
  color:#5e9d2d;
  font:normal 16px/32px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p {
  float:left;
  padding:0 0px;
  margin:0 0 10px 0px;
  color:#444444;
  font:normal 16px/32px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p a.feedbacknav {
  color:#444444;
  font:normal 16px/32px 'Segoe_UI', Arial, Helvetica, sans-serif;
  text-decoration:underline;
  background:url(img/feedbackIcon.jpg) left top no-repeat;
  padding-left:32px;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p span.officon {
  color:#444444;
  font:normal 16px/32px 'Segoe_UI', Arial, Helvetica, sans-serif;
  background:url(img/offIcon.jpg) left top no-repeat;
  padding-left:32px;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p span.locationicon {
  color:#444444;
  font:normal 16px/32px 'Segoe_UI', Arial, Helvetica, sans-serif;
  background:url(img/map-pin.png) left top no-repeat;
  padding-left:32px;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight .bottomNav {
  float:left;
  padding:0 0px;
  margin:0px 0 0px 0px;
}
.generate-btn, .generate-btn:hover {
  margin:0 3px;
  padding:12px 10px;
  background:#5e9d2d;
  border:none;
  color:#fff;
  font-weight:600;
  font-size:20px;
  text-shadow: 1px 1px #6e6e6e;
        line-height: 16px;
}
.fedback-btn, .fedback-btn:hover {
  margin:0 3px;
  padding:12px 10px!Important;
  background:#8a8a8a;
  border:none;
  color:#fff;
  font-weight:600;
  font-size:20px!Important;
  text-shadow: 1px 1px #6e6e6e;
}
#middleContainer .wecare-leftTop .wecare-generateCoupon {
    background: none repeat scroll 0 0 #f7f6f6;
    border: 1px solid #e0e2e3;
    padding:0% 2% 2% 2%;
  border-top:0px!Important;
}
#middleContainer .wecare-leftTop .wecare-generateCoupon .topBarCode {
    width:100%;
  float:left;
  padding:10px 0px;
  color:#444444;
  font:normal 24px 'Segoe_UI', Arial, Helvetica, sans-serif;
  margin-bottom:1%;
}
#middleContainer .wecare-leftTop .wecare-generateCoupon .topBarCode span {
  color:#444444;
  font:normal 16px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
#middleContainer .wecare-leftTop .wecare-generateCoupon .generatecodePlan {
    width:100%;
  float:left;
}
#middleContainer .wecare-leftTop .wecare-generateCoupon .couponDetailtxt {
    width:100%;
  float:left;
  padding:10px 0px;
  color:#444444;
  font:normal 16px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
.coupontextfield {
  margin:0 1.8% 0px 0;
  padding:11px 15px;
  border:1px solid #e0e2e3;
  font-size:16px;
  display:block;
  width:38%;
  float:left;
}


/*------------------------- My Plans -------------------*/
.topMrgn{
  margin-top:25px;
}
.grayBorder{
  border:1px solid #e0e2e3;
  padding:20px 25px;
}
.grayBorder h2{
  border-bottom:1px solid #a5a5a5;
  padding:8px 0px;
  width:100%;
  color:#666;
  font-size:18px;
  font-weight:600;
  margin:0 0px;
  text-transform:uppercase;
}
.grayBorder .myPlanForm{
  padding:10px 0px 20px 0;
  width:100%;
}

.grayBorder .myPlanForm .myPlanLeft{
  float:left;
}
.grayBorder .myPlanForm .myPlanRight{
  float:right;
}
.grayBorder .myPlanForm .myPlanformBox{
  padding:8px 0px;
  width:49%;
}
.grayBorder .myPlanForm .myPlanformBox label{
  padding:7px 0px;
  width:100%;
  float:left;
  color:#212121;
  font:normal 14px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
.grayBorder .myPlanForm .myPlanformBox label span.greenTxt{
  color:#0f6633;
  font:normal 18px/16px 'Segoe_UI', Arial, Helvetica, sans-serif;
}
.grayBorder .myPlanForm .myPlanformBox .inputBox{
  padding:10px 0px;
  border-radius:2px;
  border:1px solid #e0e2e3;
  float:left;
  width:100%;
  box-shadow:inset 1px 1px 3px #eaeaea;
}
.grayBorder .myPlanForm .myPlanformBox .inputBox .inputBoxIn{
  margin:0px 15px;
  height:25px;
  overflow:hidden;
}
.grayBorder .myPlanForm .myPlanformBox .inputBox .dropBg{
  background:url(img/grayArrow.png) right center no-repeat;
}
.grayBorder .myPlanForm .myPlanformBox .inputBox .dateBg{
  background:url(img/selectDateIcon.png) right center no-repeat;
}
.grayBorder .myPlanForm .myPlanformBox .inputBox .dropBg select{
  background:none;
  border:0px;
  color:#444444;
  
  width:105%;
}
.grayBorder .myPlanForm .myPlanformBox .inputBox .inputBoxIn .txtField{
  margin:0px 0px;
  height:25px;
  width:100%;
  border:0px;
  color:#444444;
  
  background:none;
}
.greenGeneralTxt{
  color:#5e9d2d;
}
.myPlanBtn{
  text-align:right;
  padding-bottom:15px;
  width:100%;
  display:inline-block;
}
.submit-btn, .submit-btn:hover {
  margin:0 3px;
  padding:1.5%;
  background:#5e9d2d;
  border:none;
  color:#fff;
  font-weight:600;
  font-size:20px;
  text-shadow: 1px 1px #6e6e6e;
}
.cancel-btn, .cancel-btn:hover {
  margin:0 3px;
  padding:15px 13px!Important;
  background:#8a8a8a;
  border:none;
  color:#fff;
  font-weight:600;
  font-size:20px!Important;
  text-shadow: 1px 1px #6e6e6e;
  width:auto!Important;
}
.browseGray-btn, .browseGray-btn:hover {
  margin:0 3px;
  padding:1%!Important;
  background:#8a8a8a;
  border:none;
  color:#fff;
  font-weight:600;
  font-size:20px!Important;
  text-shadow: 1px 1px #6e6e6e;
}
.model-input{
  margin:0 0 0 135px;
}
.inputBoxfull{
  padding:10px 0px;
  border-radius:2px;
  border:1px solid #e0e2e3;
  float:left;
  width:74%;
  box-shadow:inset 1px 1px 3px #eaeaea;
  margin-right:5px;
  background:#f9f9f9;
}
.inputBoxfull .inputBoxIn{
  margin:0px 15px;
  height:25px;
  overflow:hidden;
  color:#444444;
  
}
.inputBoxfull .inputBoxIn .txtField{
  margin:0px 0px;
  height:25px;
  width:100%;
  border:0px;
  color:#444444;
  
  background:none;
}
.plusGr-btn, .plusGr-btn:hover {
  margin:0 3px;
  padding:10px 0px 17px 0px;
  background:#5e9d2d;
  border:none;
  color:#fff;
  font-weight:600;
  font-size:34px;
  line-height:20px;
  text-shadow: 1px 1px #6e6e6e;
  text-align:center;
  text-decoration:none;
  width:60px;
}
.colfull{
  width:100%;
  float:left;
}
.colfullBot{
  width:100%;
  float:left;
  border-top:1px solid #e0e2e3;
  margin-top:25px;
}
.eyeIcon{
  width:36px;
  height:24px;
  display:inline-block;
  cursor:pointer;
  background:url(img/eyeIcon.png) center center no-repeat;
  margin-top:5px;
}

.timeIcon{
  width:25px;
  height:24px;
  display:inline-block;
  margin-left:10px;
  cursor:pointer;
  background:url(img/timeIcon.png) center center no-repeat;
}




.grayBorder .myPlanForm .myPlanformBox30per{
  padding:8px 0px;
  width:32%;
}
.grayBorder .myPlanForm .mrgnMyPlan{
  margin-right:2%;
}
.grayBorder .myPlanForm .myPlanformBox30per label{
  padding:7px 0px;
  width:100%;
  float:left;
  color:#666;
  
}
.grayBorder .myPlanForm .myPlanformBox30per label span.greenTxt{
  color:#0f6633;
  
}
.grayBorder .myPlanForm .myPlanformBox30per .inputBox{
  padding:10px 0px;
  border-radius:2px;
  border:1px solid #e0e2e3;
  float:left;
  width:100%;
  box-shadow:inset 1px 1px 3px #eaeaea;
}
.grayBorder .myPlanForm .myPlanformBox30per .inputBoxcolor{
  background:#f9f9f9!Important;
}
.grayBorder .myPlanForm .myPlanformBox30per .inputBox .inputBoxIn{
  margin:0px 15px;
  height:25px;
  overflow:hidden;
  color:#666;

}
.grayBorder .myPlanForm .myPlanformBox30per .inputBox .inputBoxIn .txtField{
  margin:0px 0px;
  height:25px;
  width:100%;
  border:0px;
  color:#666;
  
  background:none;
}



.grayBorder .myPlanForm .myPlanformBox60per{
  padding:8px 0px;
  width:66%;
}
.grayBorder .myPlanForm .myPlanformBox60per label{
  padding:7px 0px;
  width:100%;
  float:left;
  color:#666;
  
}
.grayBorder .myPlanForm .myPlanformBox60per label span.greenTxt{
  color:#0f6633;
  
}
.grayBorder .myPlanForm .myPlanformBox60per .inputBox{
  padding:10px 0px;
  border-radius:2px;
  border:1px solid #e0e2e3;
  float:left;
  width:100%;
  box-shadow:inset 1px 1px 3px #eaeaea;
}
.grayBorder .myPlanForm .myPlanformBox60per .inputBox .inputBoxIn{
  margin:0px 15px;
  height:25px;
  overflow:hidden;
}
.grayBorder .myPlanForm .myPlanformBox60per .inputBox .inputBoxIn .txtField{
  margin:0px 0px;
  height:25px;
  width:100%;
  border:0px;
  color:#666;
  
  background:none;
}
.transFailBox{
  width:100%;
  background:#fbfbfb;
  text-align:center;
  border:1px solid #e0e2e3;
  padding:25px 0px;
  color:#666;
  
}
.transFailBox span{
  color:#5e9d2d;
  
}

.grayBorder .myPlanForm .uploadDocTitle{
  padding:7px 0px;
  width:100%;
  float:left;
  color:#212121;
  
}
.grayBorder .myPlanForm .uploadDocTitle span.greenTxt{
  color:#0f6633;
  
}
.grayBorder .myPlanForm .uploadDocFormBox{
  width:100%;
  float:left;
}
.grayBorder .myPlanForm .uploadDocFormBox .leftCol45{
  width:45%;
  float:left;
}
.grayBorder .myPlanForm .uploadDocFormBox .leftCol45 .inputBox{
  padding:10px 0px;
  border-radius:2px;
  border:1px solid #e0e2e3;
  float:left;
  width:71%;
  box-shadow:inset 1px 1px 3px #eaeaea;
}
.grayBorder .myPlanForm .uploadDocFormBox .leftCol45 .inputBox .inputBoxIn{
  margin:0px 15px;
  height:25px;
  overflow:hidden;
  color:#444444;

}
.grayBorder .myPlanForm .uploadDocFormBox .leftCol45 .inputBox .inputBoxIn .txtField{
  margin:0px 0px;
  height:25px;
  width:100%;
  border:0px;
  color:#444444;
  
  background:none;
}
.grayBorder .myPlanForm .uploadDocFormBox .leftCol45 .cancel-btn, .cancel-btn:hover {
  margin:0 3px;
  padding:15px 0px;
  background:#8a8a8a;
  border:none;
  color:#fff;
  font-weight:600;
  font-size:20px;
  text-shadow: 1px 1px #6e6e6e;
  width:35%;
  text-align:center;
}
.grayBorder .myPlanForm .uploadDocFormBox .rightCol55{
  width:52%;
  float:right;
}
.grayBorder .myPlanForm .uploadDocFormBox .rightCol55 .inputBox{
  padding:10px 0px;
  border-radius:2px;
  border:1px solid #e0e2e3;
  float:left;
  width:80%;
  box-shadow:inset 1px 1px 3px #eaeaea;
  background:#f9f9f9;
}
.grayBorder .myPlanForm .uploadDocFormBox .rightCol55 .inputBox .inputBoxIn{
  margin:0px 15px;
  height:25px;
  overflow:hidden;
  color:#444444;
  
}
.grayBorder .myPlanForm .uploadDocFormBox .rightCol55 .plus-btn, .plus-btn:hover {
  margin:0 3px;
  padding:10px 0px 17px 0px;
  background:#5e9d2d;
  border:none;
  color:#fff;
  font-weight:600;
  font-size:34px;
  line-height:20px;
  text-shadow: 1px 1px #6e6e6e;
  text-align:center;
  text-decoration:none;
  width:15%;
}
.grayBorder .myPlanForm .uploadDocGrayTitle{
  padding:7px 0px 20px 0;
  width:100%;
  float:left;
  color:#444444;

}


/*------------------------- Our Clients -------------------*/
.owl-pagination {
  display:block;
}
.brands .item > img {
  height: auto;
  width: 100%;
  display: block;
  min-height: 75px;
}
.brands .item {
  background-color: #FFFFFF;
  border: 0px solid #e2e4e5;
  border-radius:0px;
}
.brands .item:hover {
  box-shadow: none;
}
.brands .owl-item {
  padding: 0 15px;
}
owl-carousel .owl-wrapper:after {
  content: '.';
  display: block;
  clear: both;
  visibility: hidden;
  line-height: 0;
  height: 0;
}
/* display none until init */
.owl-carousel {
  display: none;
  position: relative;
  width: 100%;
  -ms-touch-action: pan-y;
}
.owl-carousel .owl-wrapper {
  display: none;
  position: relative;
  -webkit-transform: translate3d(0px, 0px, 0px);
}
.owl-carousel .owl-wrapper-outer {
  overflow: hidden;
  position: relative;
  width: 100%;
}
.owl-carousel .owl-wrapper-outer.autoHeight {
  -webkit-transition: height 500ms ease-in-out;
  -moz-transition: height 500ms ease-in-out;
  -ms-transition: height 500ms ease-in-out;
  -o-transition: height 500ms ease-in-out;
  transition: height 500ms ease-in-out;
}
.owl-carousel .owl-item {
  float: left;
  padding: 0 15px;
}
.owl-controls .owl-page, .owl-controls .owl-buttons div {
  cursor: pointer;
}
.owl-controls {
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
}
.owl-carousel .owl-wrapper, .owl-carousel .owl-item {
  -webkit-backface-visibility: hidden;
  -moz-backface-visibility:    hidden;
  -ms-backface-visibility:     hidden;
  -webkit-transform: translate3d(0, 0, 0);
  -moz-transform: translate3d(0, 0, 0);
  -ms-transform: translate3d(0, 0, 0);
}
/*
*   Owl Carousel Owl Demo Theme 
* v1.3.2
*/
/* Clickable class fix problem with hover on touch devices */
/* Use it for non-touch hover action */
.owl-theme .owl-controls.clickable .owl-buttons div:hover {
  filter: Alpha(Opacity=100);/*IE7 fix*/
  opacity: 1;
  text-decoration: none;
}
/* Styling Pagination*/
.owl-theme .owl-controls .owl-page {
  display: inline-block;
  zoom: 1;
 *display: inline;/*IE7 life-saver */
}
.owl-theme .owl-controls .owl-page span {
  display: block;
  width: 12px;
  height: 12px;
  margin: 5px 7px;
  filter: Alpha(Opacity=50);/*IE7 fix*/
  opacity: 0.5;
  -webkit-border-radius: 20px;
  -moz-border-radius: 20px;
  border-radius: 20px;
  background: #869791;
}
.owl-theme .owl-controls .owl-page.active span, .owl-theme .owl-controls.clickable .owl-page:hover span {
  filter: Alpha(Opacity=100);/*IE7 fix*/
  opacity: 1;
}
/* If PaginationNumbers is true */
.owl-theme .owl-controls .owl-page span.owl-numbers {
  height: auto;
  width: auto;
  color: #FFF;
  padding: 2px 10px;
  font-size: 12px;
  -webkit-border-radius: 30px;
  -moz-border-radius: 30px;
  border-radius: 30px;
}
/* preloading images */
.owl-item.loading {
  min-height: 150px;
  background: url(AjaxLoader.gif) no-repeat center center
}
/* Product Slider */

.centerBoxWrapper .owl-theme .owl-controls .owl-buttons, .brands-slider .owl-theme .owl-controls .owl-buttons, .alsoPurchased #alsopurchased_products .owl-controls .owl-buttons, .centerBoxWrapper #featured-slider-inner .owl-buttons {
  position: absolute;
  right: 5px;
  top: -45px;
  z-index: 99;
}
.centerBoxWrapper .owl-theme .owl-controls .owl-buttons .owl-next, .centerBoxWrapper .owl-theme .owl-controls .owl-buttons .owl-prev, .brands-slider .owl-theme .owl-controls .owl-buttons .owl-next, .brands-slider .owl-theme .owl-controls .owl-buttons .owl-prev {
  opacity: 1;
  text-shadow: none;
  position:absolute;
  right:5px;
}
.centerBoxWrapper .owl-theme .owl-controls .owl-buttons .owl-prev, .brands-slider .owl-theme .owl-controls .owl-buttons .owl-prev {
  right: 25px;
}
.centerBoxWrapper .owl-theme .owl-controls .owl-buttons .owl-prev.disabled, .brands-slider .owl-theme .owl-controls .owl-buttons .owl-prev.disabled, .centerBoxWrapper .owl-theme .owl-controls .owl-buttons .owl-next.disabled, .brands-slider .owl-theme .owl-controls .owl-buttons .owl-next.disabled {
  opacity:0.65;
  cursor:default;
}
#Our_clients .owl-prev .fa {
  margin-left: 7px;
  vertical-align: 0;
  display:none;
}
#Our_clients .owl-next .fa {
  margin-left: 9px;
  vertical-align: 0;
  display:none;
}
#Our_clients .owl-prev {
  margin:0;
  padding:0;
  width:23px;
  height:43px;
  background:url(img/recent_project_btn.png) no-repeat scroll left top !important;
  top:40%;
  left:-5px;
  position:absolute;
}
#Our_clients .owl-prev:hover {
  background:url(img/recent_project_btn.png) no-repeat left -47px !important;
}
#Our_clients .owl-next:hover {
  background:url(img/recent_project_btn.png) no-repeat right -47px !important
}
#Our_clients .owl-next {
  margin:0;
  padding:0;
  width:24px;
  height:43px;
  background:url(img/recent_project_btn.png) no-repeat scroll right top !important;
  top:40%;
  right:-5px;
  position:absolute;
}
#Our_clients .brands {
  padding:0 15px
}






/*------------------------- Nevigation -------------------*/




.header {
  margin:0 15px;
  padding:0;
}
.header .navbar {
  margin:0 0 2% 0;
  padding:0;
  width:100% !important;
  background:#FFF;
  border-bottom:1px solid #e0e2e3 !important;
}
.header .navbar-default {
  border-radius:0;
  border:none;
  box-shadow:none;
  width:100% !important;
  overflow:hidden;
  background:#FFF;
  z-index:99;
}
.header navbar-brand, .header .navbar-nav > li > a {
  text-shadow:none;
  color:;
  border-right:1px solid #f0f0f0;
  padding:0 25px !important;
  height:60px;
  line-height:55px;
}
.header .navbar-default .navbar-nav>li>a:hover, .header .navbar-default .navbar-nav>li>a:focus {
  background-color:;
  color:;
  border-bottom:7px solid ;
}
.header .dropdown-menu {
  background-color:#ddeef7;
  color:#312c29;
  padding:0
}
.header .navbar-default .navbar-nav>.open>a, .header .navbar-default .navbar-nav>.active>a {
  background:#fff !important;
  color:#312c29 !important;
}
.header .nav .open>a, .nav .open>a:hover, .header .nav .open>a:focus {
  background:#57b2e4;
  color:#FFF
}
 @media(min-width:768px) {
   
   
   
.addressBox {
 border: 0px solid #dedede;
 margin:0;
 padding: 12px 0;
 width: 100%;
}
 .header .navbar-nav > li:first-child > a {
}
.header .navbar-nav > li > a {
background:;
margin:0;
padding:0;
}
.header .navbar-nav > li > a img {
margin:0;
}
.header .navbar-collapse {
padding-left:0;
padding-right:0
}
.header .dropdown-menu>li> a {
border-right:1px solid #57b2e4
}
.header .dropdown-menu>li:first-child>a {
border-top:none
}
.header .dropdown-menu>li>a {
border-top:1px solid #57b2e4
}
header .navbar-nav {
float:left;
margin:0;
padding:0;
}
}
.carousel-caption p {
  font-size:18px;
  color:#5D5D5D;
}
.carousel-caption h1 {
  font-size:40px;
  font-weight:300;
}
@media (max-width:1400px) {
.navbar-brand img {
width:60px;
}
.navbar-nav {
 margin-top:0;
}
}
.btn-default {
  background-color: transparent;
  border-color: #5d5d5d;
  color: #5d5d5d0;
  border-radius:2px;
  padding:8px 15px;
}
.btn-primary {
  background-color: transparent;
  border-color: #fff;
  color: #fff;
  border-radius:2px;
  padding:8px 15px;
}
.btn-primary:hover, .btn-primary:focus {
  background-color: #fff;
  border-color: #fff;
  color: #5d5d5d;
  border-radius:2px;
  padding:8px 15px;
}
#middleContainer {
  margin:0;
  padding:0;
  width:100%;
}
#middleContainer .middlebox {
  margin:0;
  padding:0;
}
#middleContainer .middlebox-left {
  margin:0;
  padding:0;
  background:#FFF;
  margin-bottom:3%;
}
#middleContainer .middlebox-network {
  margin:0;
  padding:3% 1.5%;
  background:#FFF;
  margin-bottom:3%;
  overflow:hidden;
  border:1px solid #e0e2e3;
}
#middleContainer .middlebox-right {
  margin:0;
  padding:0;
}
#middleContainer .middlebox-right .calander {
  margin:0;
  padding:0;
  background:#FFF;
  min-height:250px;
  border:1px solid #e0e2e3;
  margin-bottom:10%;
}
#middleContainer .middlebox-right .calander h1 {
  margin:0;
  padding:10px 0;
  background:;
  min-height:54px;
  font-size:22px;
  font-weight:300;
  color:;
  text-align:center;
}
#middleContainer .middlebox-right .doctor {
  margin:0;
  padding:0;
  background:#FFF;
  min-height:250px;
  border:1px solid #e0e2e3;
  margin-bottom:10%;
}
#middleContainer .middlebox-right .doctor h1 {
  margin:0;
  padding:10px 0;
  background:;
  min-height:54px;
  font-size:18px;
  font-weight:300;
  color:;
  text-align:center;
}
#middleContainer .middlebox-right .doctor-bot {
  margin:0;
  padding:5%;
  overflow:hidden;
}
#middleContainer .middlebox-right .doctor-sesarch {
  background:url(img/search-icon1.png) no-repeat 3% center;
  padding:10%;
}
#middleContainer .middlebox-right .doctor-map-pin {
  background:url(img/map-pin.png) no-repeat 3% center;
  padding:10%;
}
#middleContainer .middlebox-right .doctor-bot input[type='text'], #middleContainer .middlebox-right .doctor-bot textarea {
  margin:0 0 15px 0;
  padding:5% 5% 5% 12%;
  border:1px solid ;
  width:100%;
  font-size:16px;
  display:block
}
#middleContainer .middlebox-right .doctor-bot input[type='text']:hover, #middleContainer .middlebox-right .doctor-bot input[type='text']:hover, #middleContainer .middlebox-right .doctor-bot textarea:hover, #middleContainer .middlebox-right .doctor-bot textarea:focus {
  transition: all 0.4s ease-in-out 0s;
  border-color:;
}
#middleContainer .middlebox-right .doctor-bot input[type='submit'] {
  margin:0;
  padding:0;
  float:left;
  width:100%;
  padding:5% 5%;
  background:;
  border:none;
  color:;
  font-weight:600;
  font-size:20px;
  text-shadow: 1px 1px #6e6e6e;
}
#middleContainer .middlebox-right .doctor-bot input[type='submit']:hover {
  background:;
  transition: all 0.4s ease-in-out 0s;
  color:;
}
#middleContainer .middlebox-right .claim {
  margin:0;
  padding:0;
  background:#FFF;
  line-height:18px;
  border:1px solid #e0e2e3;
  margin-bottom:10%;
}
#middleContainer .middlebox-right .claim img {
  margin:0;
  padding:0;
  float:left;
}
#middleContainer .middlebox-right .claim p {
  margin:0;
  padding:10px 0;
  font-size:18px;
  color:444;
  margin:5%;
  text-align:right;
}
#middleContainer .middlebox-right .claim a {
  margin:0;
  padding:15px 0;
  font-size:14px;
  color:444;
  text-decoration:none;
}
#middleContainer .middlebox-right .network {
  margin:0;
  padding:0;
  background:#FFF;
  border:1px solid #e0e2e3;
  margin-bottom:0%;
}
#middleContainer .middlebox-right .network-left {
  margin:0;
  padding:5%;
  background:#FFF;
  width:50%;
  font-size:15px;
  color:#444;
  cursor:pointer;
  display:inline-block;
  text-align:center;
}
#middleContainer .middlebox-right .network-left a {
  text-decoration:none;
  cursor:pointer;
}
#middleContainer .middlebox-right .network-left a:hover {
  font-size:15px;
  text-decoration:none;
}
#middleContainer .middlebox-right .network-right {
  margin:0;
  padding:5%;
  background:#FFF;
  width:48%;
  font-size:15px;
  color:#444;
  display:inline-block;
  text-align:center;
}
#middleContainer .middlebox-right .network-right a {
  text-decoration:none;
}
#middleContainer .middlebox-right .network-right a:hover {
  font-size:15px;
  text-decoration:none;
}
#middleContainer .middlebox-right .claim img {
  margin:0;
  padding:0;
  float:left;
}
.nav {
  padding-left: 0;
  margin-bottom: 0;
  list-style: none;
  font-size:16px;
}
.nav>li {
  position: relative;
  display: block
}
.nav>li>a {
  position: relative;
  display: block;
  padding: 15px 25px;
  background:#fafafa;
  border-right: 1px solid #e6e8e8 !important;
}
.nav>li>a:hover, .nav>li>a:focus {
  text-decoration: none;
  background-color: #8a8a8a;
  color:#FFF;
}
.nav>li.disabled>a {
  color: #777
}
.nav>li.disabled>a:hover, .nav>li.disabled>a:focus {
  color: #777;
  text-decoration: none;
  cursor: not-allowed;
  background-color: transparent
}
.nav .open>a, .nav .open>a:hover, .nav .open>a:focus {
  background-color: #eee;
  border-color: #337ab7
}
.nav .nav-divider {
  height: 1px;
  margin: 9px 0;
  overflow: hidden;
  background-color: #e5e5e5
}
.nav>li>a>img {
  max-width: none
}
.nav-tabs {
  border: 1px solid #e6e8e8;
  background:#fafafa !important;
}
.nav-tabs>li {
  float: left;
  margin-bottom: -1px
}
.nav-tabs>li>a {
  margin-right: 2px;
  line-height: 1.42857143;
  border: 0px solid transparent;
  border-radius: 0px 0px 0 0;
}
.nav-tabs>li>a:hover {
  border-color: #eee #eee #ddd;
}
.nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus {
  color: #555;
  cursor: default;
  background: #8a8a8a !important;
  border: 1px solid #8a8a8a;
  color:#FFF;
  border-bottom-color: transparent;
}
.nav-tabs>li.active>a:after {
  background:url(img/down-arrow.png) no-repeat center top !important;
  z-index:999 !important;
  position:absolute;
  z-index:999999999999 !important;
}
.stepProgress {
  margin: auto;
  width:100%;
  padding:0;
  overflow:hidden;
  position: relative;
  z-index:9;
}
.stepProgress .stepProgressBar {
  height:2px;
  margin: auto;
  position: relative;
  top: 18px;
  width: 70%;
  background:#8a8a8a;
  z-index:1;
}
.stepProgress .stepProgressIn {
  display: inline-block;
  float: left;
  text-align: center;
  width: 25%;
  position:relative;
  z-index:9999;
}
.stepProgress .stepProgressIn span.stepactive {
  background:#5e9d2d;
  border-radius: 20px;
  display: block;
  height: 33px;
  margin:0 auto;
  top: 22px;
  color:#FFF;
  line-height:25px;
  width:33px;
  font-size:18px;
  font-weight:600;
  border:2px solid #a0cc68;
  z-index:9999999 !important;
}
.stepProgress .stepProgressIn span.stepBar {
  background: #8a8a8a;
  border-radius: 20px;
  display: block;
  height: 33px;
  margin:0 auto;
  top: 22px;
  color:#FFF;
  line-height:25px;
  width:33px;
  font-size:18px;
  font-weight:600;
  border:2px solid #8a8a8a;
  z-index: 9999 !important;
}
.stepProgress .stepProgressIn span.stepTxt {
  color: #444;
  display: block;
  
  font-weight:300;
  text-align: center;
  width:100%;
}
.stepBarMobile {
  display: none;
  margin: auto;
  min-width: 300px;
  padding: 10px;
}
.stepBarMobile .stepName {
  color: #444;
  float: left;
  
}
.nav-tabs.nav-justified {
  width: 100%;
  border-bottom: 0
}
.nav-tabs.nav-justified>li {
  float: none
}
.nav-tabs.nav-justified>li>a {
  margin-bottom: 5px;
  text-align: center
}
.nav-tabs.nav-justified>.dropdown .dropdown-menu {
  top: auto;
  left: auto
}
.tab-pane {
  margin: 0;
  padding:0 0px;
  background:#eaeff5;
}
.tab-pane .tab-paneIn {
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    background: #fff none repeat scroll 0 0;
    border-bottom: 1px solid #e0e2e3;
    border-image: none;
    border-left: 1px solid #e0e2e3;
    border-right: 1px solid #e0e2e3;
    border-top: medium none !important;
    color: #000;
    margin: 0;
    padding: 1.500% 1%;
}
.tab-pane p {
  margin: 0;
  padding:2% 1.5%;
  line-height:24px;
  color:#444;

}
.tab-pane .more {
  margin: 0;
  padding:2% 1%;
  line-height:24px;
  font-weight:600;
  color:#66a503;
  text-decoration:none;
}
.tab-pane .more:hover {
  color:#66a503;
  text-decoration:underline;
}
.WebRupee {
  color: #66a503;
  display: inline-block;
  font-family: 'WebRupee';
  font-style: normal;
  margin: 0;
  padding: 0;
}
.WebRupee1 {
  color: #66a503;
  display: inline-block;
  font-family: 'WebRupee';
  font-style: normal;
  margin: 0;
  font-size:24px;
  padding: 0;
}
.tab-pane .tab-left-container {
  margin: 0;
  padding:0;
}
.tab-pane .tab-left-container h1 {
  margin: 0;
  padding:0;
  font-size:24px;
  display:block;
  margin-bottom:3%;
}
.tab-pane .tab-mid-container {
  margin: 0;
  padding:0 1.5%;
}
.insuredQuestioncheckBox {
  width: 100%;
}
.insuredQuestioncheckLeft {
}
.insuredQuestioncheckLeft span {
  color: #000000;
  font: 14px/14px 'SEGOEUIL', Arial, Helvetica, sans-serif;
  padding-left: 20px;
}
.insuredQuestioncheckLeft span strong {
  color: #ff0000;
  font: 18px/22px 'seguisb_0', Arial, Helvetica, sans-serif;
}
.insuredQuestioncheckRight {
  width: 100%;
}
.insuredQuestioncheck {
  text-align: left;
  padding-left:0;
  padding-top:0;
}
.insuredQuestionc-left {
  text-align: left;
  padding-top:20px !important;
  font-size:18px;
  color:#333;
  font-weight:600;
}
 .insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:not(:checked), .insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:checked {
 left: -9999px;
 position: absolute;
}
.insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:not(:checked) + label, .insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:checked + label {
 cursor: pointer;
 padding-left: 25px;
 position: relative;
}
.insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:not(:checked) + label:before, .insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:checked + label:before {
 background: none repeat scroll 0 0 #ffffff;
 border: 1px solid #a5a5a5;
 border-radius: 3px;
 content: '';
 height: 20px;
 left:0;
 position: absolute;
 top:-10px;
 width: 22px;
}
.insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:not(:checked) + label:after, .insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:checked + label:after {
 color: #66a503;
 content: '✔';
 font-size: 24px;
 left:0px;
 font-weight:700;
 position: absolute;
 top: -20px;
}
.insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:not(:checked) + label:after {
 opacity: 0;
 transform: scale(0);
}
.insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:checked + label:after {
  opacity: 1;
  transform: scale(1);
}
.insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:checked:focus + label:before, .insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck [type='checkbox']:focus:not(:checked) + label:before {
}
.insuredQuestioncheckBox .insuredQuestioncheckRight .insuredQuestioncheck label:hover:before {
  border: 1px solid #a5a5a5 !important;
}
.insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:not(:checked), .insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:checked {
 left: -9999px;
 position: absolute;
}
.insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:not(:checked) + label, .insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:checked + label {
 cursor: pointer;
 padding-left: 25px;
 position: relative;
}
.insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:not(:checked) + label:before, .insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:checked + label:before {
 background: none repeat scroll 0 0 #ffffff;
 border: 1px solid #a5a5a5;
 border-radius: 3px;
 content: '';
 height: 15px;
 left: 1px;
 position: absolute;
 top: 0;
 width: 20px;
}
.insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:not(:checked) + label:after, .insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:checked + label:after {
 color: #32764e;
 content: '✔';
 font-size: 23px;
 left: 3px;
 position: absolute;
 top: -5px;
}
.insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:not(:checked) + label:after {
 opacity: 0;
 transform: scale(0);
}
.insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:checked + label:after {
  opacity: 1;
  transform: scale(1);
}
.insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:checked:focus + label:before, .insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob [type='checkbox']:focus:not(:checked) + label:before {
}
.insuredQuestioncheckBoxMob .insuredQuestioncheckBoxInMob .checkboxMob label:hover:before {
  border: 1px solid #a5a5a5 !important;
}
#demo-top-bar {
  text-align: left;
  background: #222;
  position: relative;
  zoom: 1;
  width: 100% !important;
  z-index: 6000;
  padding: 20px 0 20px;
}
#demo-bar-inside {
  width: 960px;
  margin: 0 auto;
  position: relative;
  overflow: hidden;
}
#demo-bar-buttons {
  padding-top: 10px;
  float: right;
}
#demo-bar-buttons a {
  font-size: 12px;
  margin-left: 20px;
  color: white;
  margin: 2px 0;
  text-decoration: none;
}
#demo-bar-buttons a:hover, #demo-bar-buttons a:focus {
  text-decoration: underline;
}
#demo-bar-badge {
  display: inline-block;
  width: 302px;
  padding: 0 !important;
  margin: 0 !important;
  background-color: transparent !important;
}
#demo-bar-badge a {
  display: block;
  width: 100%;
  height: 38px;
  border-radius: 0;
  bottom: auto;
  margin: 0;
  background: url(img/examples-logo.png) no-repeat;
  background-size: 100%;
  overflow: hidden;
  text-indent: -9999px;
}
#demo-bar-badge:before, #demo-bar-badge:after {
  display: none !important;
}
.pad-n {
  padding-left:0;
  padding-right:0;
}
.daty .produ {
  position:relative;
  text-align:center;
  overflow:hidden;
}
.daty .produ img {
  width:100%;
}
.overlay {
  position:absolute;
  padding:15px;
  width:100%;
  height:100%;
  background:rgba(0, 0, 0, 0.4);
  display:none;
  top:0;
  left:0;
  color:#fff;
}
.overlay h1 {
  margin-top:35px;
  font-size:25px;
  font-weight:300;
}
.daty .produ:hover .overlay {
  display:block;
}
.hos-p {
  position:relative;
  display:block;
}
.hos-p img {
  width:100%;
  display:block;
  overflow:hidden;
}
.hos-p .overlay {
  display:block;
  width:50%;
  background:rgba(265, 265, 265, 0.6);
  padding-left:10%;
  color:#5d5d5d;
}
.connect .con-in {
  width:665px;
  padding:25px 0;
  overflow:hidden;
  margin:0 auto;
}
.connect {
  overflow:hidden;
}
.connect .con-in form label {
  font-weight:normal;
  float:left;
  font-size:25px;
  color:#5D5D5D;
}
.connect .con-in form .form-control {
  height:39px;
  line-height:39px;
  width:255px;
  border-color:#5d5d5d;
  font-size:18px;
  font-weight:400;
  float:left;
  margin:0 8px;
}
.connect .con-in form .btn {
  float:left;
}
.connect .con-in .soc {
  float:left;
}
.connect .con-in .soc a {
  color:#5d5d5d;
  line-height:39px;
  font-size:22px;
  padding:5px 0 5px 10px;
}
.connect .con-in .soc a:hover {
  color:#e8242a
}
.footer {
  color:#5f5f5f;
  text-align:left;
  line-height:24px;
  margin:30px 15px 0 15px;
}
.footer p {
  font-size:25px;
  margin:0;
}
.footer p a {
  color:#fff;
}
.footer p span {
  margin-right:15px;
}
.footer p span img {
  margin-top:-5px;
}
.copy {
  font-size:12px;
  padding:20px 0;
  background:#fff;
  margin:0;
  margin-bottom:2%;
  overflow:hidden;
  border-top:1px solid #e0e2e3;
}
.copy a {
  color:#5d5d5d;
  margin:0 0 0 8px;
}
.copy a:hover {
  color:#e8242a
}
.footer .soc {
  float:right;
  padding-top:15px;
}
.footer .soc a {
  color:#5d5d5d;
  font-size:24px;
  font-weight:300 !important;
  padding:0 0 0 10px;
}
.footer .soc a:hover {
  color:#5e9d2d
}
.copy .col-sm-6:last-child {
  text-align:right;
}
.drp-menu {
  position:absolute;
  width:100%;
  z-index:12;
  background:#fff;
}
.drp-menu .inr-menu {
  width:60%;
  margin:0 auto;
  overflow:hidden;
}
.drp-menu .inr-menu ul {
  padding:15px 0 40px;
  margin:0 0 40px;
  background:url(img/menu-devider.jpg) right 15% no-repeat;
}
.drp-menu .inr-menu .col-sm-4:last-child ul {
  background:none;
}
.drp-menu .inr-menu ul li {
  list-style:none;
}
.drp-menu .inr-menu ul li a {
  font-size:18px;
  color:#7D7D7D;
  padding:5px 0;
  display:block;
}
.drp-menu .inr-menu ul li a:hover {
  text-decoration:none;
  color:#000;
}
.drp-menu .inr-menu h4 {
  color:#E8242A;
  font-size:18px;
}
.drp-menu .closee {
  width:100%;
  color:#E8242A;
  font-size:18px;
  padding:15px 60px;
}
.drp-menu .closee a {
  cursor:pointer;
  width:100%;
  color:#E8242A;
  font-size:18px;
}
.login .modal-lg .modal-content {
  overflow:hidden;
  color:#fff;
  min-height:360px;
  padding-bottom:50px;
  background:url(img/login-bg.jpg) center center no-repeat;
  background-size: cover;
}
.login .modal-lg .modal-content h3 {
  font-size:24px;
  margin:70px 0 35px;
}
.login .modal-lg .modal-content p {
  font-size:18px;
}
.login .modal-lg .modal-content p a {
  color:#FFFFFF;
}
.login .modal-lg .modal-content .form-control {
  height:44px;
  line-height:44px;
  padding:0 15px;
  font-size:18px;
}
.login .modal-lg .modal-content .btn-default {
  height:44px;
  line-height:44px;
  padding:0 15px;
  background:#5D5D5D;
  border-left:5px solid #000;
}
.login .close {
  margin:20px 25px;
}
.account .modal-lg .modal-content {
  overflow:hidden;
  color:#5D5D5D;
  min-height:360px;
  background:#E8E8E8;
}
.account .modal-lg .modal-content .modal-header .modal-title {
  font-size:40px;
  margin:20px 0;
  padding-bottom:30px;
  border-bottom:1px solid #B4B4B4;
}
.account .modal-lg .modal-content .modal-title2 {
  font-size:20px;
  margin:0px 0 20px;
}
.account .modal-lg .modal-content label {
  font-size:12px;
  font-weight:normal;
  line-height:44px;
  width:40%;
  padding:0;
  float:left;
}
.account .modal-lg .modal-content p a {
  color:#FFFFFF;
}
.account .modal-lg .modal-content form .set {
  margin-bottom:15px;
  overflow:hidden;
  display:block;
}
.account .modal-lg .modal-content .form-control {
  height:44px;
  border:none;
  background:#fff;
  border-radius:0;
  line-height:44px;
  padding:0 15px;
  font-weight:lighter;
  width:60%;
  float:left;
  font-size:18px;
}
.account .modal-lg .modal-content .btn-default {
  height:44px;
  line-height:44px;
  padding:0 15px;
  background:#5D5D5D;
  border-left:5px solid #000;
}
.account .close {
  margin:5px 0px;
  float:left;
  font-size:12px;
  font-weight:normal;
  margin-bottom:50px;
  display:block;
}
.account .modal-lg .f-btn {
  width:60%;
  float:right;
}
.account .modal-lg .btn-danger {
  font-size:20px;
  padding:8px 25px;
}
.login2 .modal-lg .modal-content {
  overflow:hidden;
  color:#5D5D5D;
  min-height:360px;
  background:#E8E8E8;
  padding-bottom:50px;
}
.login2 .modal-lg .modal-content .modal-header .modal-title {
  font-size:40px;
  margin:20px 0;
  padding-bottom:30px;
  border-bottom:1px solid #B4B4B4;
}
.login2 .modal-lg .modal-content .bdr-r {
  border-right:1px solid #B4B4B4;
}
.login2 .modal-lg .modal-content .modal-title2 {
  font-size:20px;
  margin:0px 0 0px;
}
.login2 .modal-lg .modal-content label {
  font-size:12px;
  font-weight:normal;
  line-height:44px;
  padding:0;
  float:left;
}
.login2 .modal-lg .modal-content p {
  color:#5d5d5d;
  font-size:18px;
  margin-top:35px;
  min-height:205px;
}
.login2 .modal-lg .modal-content form .set {
  margin-bottom:15px;
  overflow:hidden;
  display:block;
}
.login2 .modal-lg .modal-content .form-control {
  height:44px;
  border:none;
  background:#fff;
  border-radius:0;
  line-height:44px;
  padding:0 15px;
  font-weight:lighter;
  width:60%;
  float:left;
  font-size:18px;
}
.login2 .modal-lg .modal-content .btn-default {
  height:44px;
  line-height:44px;
  padding:0 15px;
  background:#5D5D5D;
  border-left:5px solid #000;
}
.login2 .close {
  margin:5px 0px;
  float:left;
  font-size:12px;
  font-weight:normal;
  margin-bottom:50px;
  display:block;
}
.login2 .modal-lg .f-btn {
  float:left;
}
.login2 .modal-lg .fog {
  margin:0px 15px;
  color:#5d5d5d;
  float:left;
  font-size:12px;
  font-weight:normal;
  line-height:44px;
}
.login2 .modal-lg .socc {
  margin-bottom:5px;
}
.login2 .modal-lg .btn-danger {
  font-size:20px;
  padding:8px 25px;
}
 @media (max-width:992px) {
.colfullBot{
  width:100%;
  float:left;
  border-top:0px solid #e0e2e3;
  margin-top:25px;
}
.carousel-caption h1 {
font-size:24px;
}
.carousel-caption p {
font-size:14px;
}
.login .modal-lg .modal-content p {
 font-size: 14px;
}
.login2 .modal-lg .modal-content .modal-header .modal-title {
font-size:24px;
margin:20px 0;
padding-bottom:30px;
border-bottom:1px solid #5d5d5d;
}
.login2 .modal-lg .modal-content .modal-title2 {
font-size:16px;
margin:0px 0 0px;
}
 .account .modal-lg .modal-content .modal-header .modal-title {
font-size:24px;
margin:20px 0;
padding-bottom:30px;
border-bottom:1px solid #5d5d5d;
}
.account .modal-lg .modal-content .modal-title2 {
font-size:16px;
margin:0px 0 20px;
}
.login2 .modal-lg .modal-content p {
font-size:14px;
}
.login .modal-lg .modal-content p {
font-size:14px;
}
.account .modal-lg .modal-content p {
font-size:14px;
}
.navbar-nav > li > a {
 padding: 15px 3px;
}
.overlay h1 {
 font-size: 20px;
 font-weight: 300;
}
.short h1 {
 font-size: 20px;
 font-weight: 300;
 margin-top: -11px;
}
.footer p {
 font-size: 14px;
 margin: 0;
}
.drp-menu .inr-menu {
 margin: 0 auto;
 overflow: hidden;
 width: 100%;
}
}
@media(max-width:768px) {
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressLeft {
    width:100%;
  float:left;
  color:#444444;
  
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight {
  float:left;
  width:100%;
  display:inline-block;
  padding-top:1%;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight .topStar {
  float:left;
  padding:0 0px;
  margin:0 0 10px 0px;
  color:#5e9d2d;
  
  width:100%;
}
#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p {
  float:left;
  padding:0 0px;
  margin:0 0 0px 0px;
  color:#444444;
  
  width:100%;
}
.locationtextfield {
  margin:0 1% 1% 0;
  padding:14px 15px 14px 7%;
  border:1px solid #e0e2e3;
  width:100%;
  font-size:16px;
  display:block;
  background:url(img/map-pin.png) no-repeat 3% center;
  width:100%;
  float:left;
}
.doctorsrchtextfield {
  margin:0 1% 0px 0;
  padding:14px 15px 14px 7%;
  border:1px solid #e0e2e3;
  width:100%;
  font-size:16px;
  display:block;
  background:url(img/search-icon1.png) no-repeat 3% center;
  width:86%;
  float:left;
}
.graytxtBox-Location {
 background: none repeat scroll 0 0 #fff;
 float: left;
 margin: 0 0 10px;
 padding: 5px 8px;
 width: 100%;
}
 .greenSrchBtn {
 cursor: pointer;
 float: right;
 margin: 0;
 padding: 12px 5px;
 text-indent: -999999px;
 width: 9%;
}
 .graytxtBox-Spec {
 float: left;
 margin: 0;
 padding: 5px 8px;
 width: 89%;
}
 .navbar-brand img {
margin:0 0;
}
.navbar-toggle {
top:
}
.login2 .modal-lg .modal-content p {
font-size:14px;
min-height:1px;
}
.navbar-nav {
 margin-top: 0;
}
.navbar-brand {
 margin-top: 0px;
}
.ico-nav li {
float:left;
}
.navbar-nav > li > a {
padding: 8px;
}
 .footer p span, .footer p a {
display:block;
}
.footer p {
 font-size: 14px;
 line-height: 35px;
 margin: 0;
}
.copy {
 font-size: 12px;
 text-align: center;
}
.footer .soc {
float:inherit;
padding-top:15px;
}
.footer .soc a {
color:#5d5d5d;
font-size:24px;
font-weight:300 !important;
padding:0 0 0 10px;
}
.footer .soc a:hover {
color:#e8242a
}
 .footer {
 padding: 20px 0;
 text-align: center;
}
.more {
 color: #66a503;
 font-size: 16px;
 font-weight: 600;
 line-height: 24px;
 margin: 0;
 padding: 2% 2%;
 text-decoration: none;
}
 .stepProgress {
 margin: auto;
 overflow: hidden;
 padding-bottom:5%;
 position: relative;
 width: 100%;
 z-index: 9;
}
.middlebox-left p {
 margin:0 0 7% 0;
 padding:0;
 font-size:14px;
 line-height:24px;
}
 div.cs-select {
 margin-left: 0% !important;
 margin-bottom:0;
 width:100%;
 display:block;
}
}
@media(max-width:1050px) {
.coupontextfield {
  margin:0 1.8% 0px 0;
  padding:11px 15px;
  border:1px solid #e0e2e3;
  font-size:16px;
  display:block;
  width:31%;
  float:left;
}
}
 @media(max-width:600px) {
 .btn-default {
padding:3px 5px
}
 .header navbar-brand, .header .navbar-nav > li > a {
margin-bottom:5px;
}
}
 @media(max-width:640px) {
.coupontextfield {
  margin:0 1.8% 1.8% 0;
  padding:11px 15px;
  border:1px solid #e0e2e3;
  font-size:16px;
  display:block;
  width:100%;
  float:left;
}
.grayBorder .myPlanForm .uploadDocFormBox .leftCol45{
  width:100%;
  float:left;
  padding:5px 0px;
}
.grayBorder .myPlanForm .uploadDocFormBox .rightCol55{
  width:100%;
  float:left;
  padding:5px 0px;
}
div.cs-select {
 margin-left: 5% !important;
 margin-bottom:0;
}
.stepProgress {
 margin: auto;
 overflow: hidden;
 padding-bottom:5%;
 position: relative;
 width: 100%;
 z-index: 9;
}
.middlebox-left p {
 margin:0 0 7% 0;
 padding:0;
 font-size:14px;
 line-height:24px;
}
 .more {
 color: #66a503;
 font-size: 16px;
 font-weight: 600;
 line-height: 24px;
 margin: 0;
 padding: 2% 2%;
 text-decoration: none;
}
.grayBorder .myPlanForm .myPlanformBox60per{
  padding:8px 0px;
  width:100%;
}
.grayBorder .myPlanForm .myPlanformBox30per{
  padding:8px 0px;
  width:100%;
}
.grayBorder .myPlanForm .mrgnMyPlan{
  margin-right:0%;
}
.grayBorder .myPlanForm .myPlanformBox{
  padding:8px 0px;
  width:100%;
}
.grayBorder .myPlanForm .myPlanRight{
  float:left;
}
.submit-btn, .submit-btn:hover {
  margin:0 3px;
  padding:12px 5px;
  background:#5e9d2d;
  border:none;
  color:#fff;
  font-weight:600;
  font-size:17px;
  text-shadow: 1px 1px #6e6e6e;
}
.cancel-btn, .cancel-btn:hover {
  margin:0 3px!important;
  padding:12px 5px!important;
  background:#8a8a8a;
  border:none;
  color:#fff;
  font-weight:600;
  font-size:17px!important;
  text-shadow: 1px 1px #6e6e6e;
}





}
 @media(max-width:1200px) {
#middleContainer .dashboard-left_box1 a{
    font-size:14px !important;
  background:#fafafa;
  border:1px solid #e2e4e5;
  color:#444;
  margin:0;
  font-weight:300;
  display:block;
  cursor:pointer;
  line-height:60px;
  overflow:hidden;
  text-decoration:none;
  padding:5% 0;
}
}




 @media(max-width:360px) {
.model-input{
  margin:10px 0 0 0px;
  float:left;
}
.inputBoxfull {
    border: 1px solid #e0e2e3;
    border-radius: 2px;
    box-shadow: 1px 1px 3px #eaeaea inset;
    float: left;
    margin-right: 5px;
    padding: 10px 0;
    width: 72%;
}
div.cs-select {
 margin-left: 5% !important;
 margin-bottom:0;
}
 .topContainer .con-in form .form-control {
 width:86% !important;
}
.generate-btn, .generate-btn:hover {
  margin:0 3px 1% 3px;
  padding:12px 10px;
  background:#5e9d2d;
  border:none;
  color:#fff;
  font-weight:600;
  font-size:16px;
  text-shadow: 1px 1px #6e6e6e;
         line-height: 16px;
}

.fedback-btn, .fedback-btn:hover {
    background: none repeat scroll 0 0 #8a8a8a;
    border: medium none;
    color: #fff;
    font-size: 16px !important;
    font-weight: 600;
    margin: 0 3px;
    padding: 12px 10px !important;
    text-shadow: 1px 1px #6e6e6e;
}
.grayBorder .myPlanForm .uploadDocFormBox .leftCol45 .cancel-btn, .cancel-btn:hover {
  margin:0 3px;
  padding:15px 0px;
  background:#8a8a8a;
  border:none;
  color:#fff;
  font-weight:600;
  font-size:12px;
  text-shadow: 1px 1px #6e6e6e;
  width:35%;
  text-align:center;
}

#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressLeft p {
    color: #444444;
    float: left;
    font: 14px/24px 'Segoe_UI',Arial,Helvetica,sans-serif;
    margin: 0 0 10px;
    padding: 0;
    width: 100%;
}


#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight .topStar {

    font: 14px/24px 'Segoe_UI',Arial,Helvetica,sans-serif;

}

#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p {

    font: 14px/24px 'Segoe_UI',Arial,Helvetica,sans-serif;

}



#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p span.locationicon {
    font: 14px/24px 'Segoe_UI',Arial,Helvetica,sans-serif;
}



#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p a.feedbacknav {

    font: 14px/24px 'Segoe_UI',Arial,Helvetica,sans-serif;

}


#middleContainer .wecare-leftTop .wecare-leftTopIn .doclorAddressRight p a.feedbacknav {

    font: 14px/24px 'Segoe_UI',Arial,Helvetica,sans-serif;

}

}
 @media(max-width:480px) {
   #middleContainer .dashboard-left h1 {
    color: #2e7d07;
    font-size: 24px;
    margin: 0 0 3%;
    padding: 0;
  text-align:center;
}
   
   #middleContainer .dashboard-left_box a {
    background: none repeat scroll 0 0 #fafafa;
    border: 1px solid #e2e4e5;
    color: #444;
    cursor: pointer;
    display: block;
    font-size: 13px;
    line-height: 24px;
    margin: 0;
    overflow: hidden;
    padding: 5% 0 0 0;
    text-decoration: none;
}
   
   
   .textCenter{text-align:center;}
   .textbold{text-align:center; font-size:16px;}
   
   #middleContainer .dashboard-left {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #e0e2e3;
    margin: 0 -14px 0 -14px;
    overflow: hidden;
    padding: 2% 2% 4%;
}
   
   
   
   
   #middleContainer .dashboard-leftTop {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #e0e2e3;
    margin: 0 -14px 3%;
    padding: 2%;
}

.dashboardName .colSm6 strong {
    font-size: 24px;
    font-weight: 600;
}


.dashboardName .colSm6 {
    float: left;
    font-family: 'Segoe_UI',Arial,Helvetica,sans-serif;
    font-size: 16px;
    line-height: 30px;
    width:100%;
}
   
   
   
.topContainer-us-icon {
padding-bottom:10px;
 position:absolute;
 z-index: 99;
 margin-left:39% !important;
 float:inherit;
 display:block;
 z-index:99;
 clear:both;
}

.topContainer-us-icon1 {
margin-top:13px !important;
}
   
.locationtextfield {
  margin:1% 1% 2% 0;
  padding:14px 15px 14px 10%;
  border:1px solid #e0e2e3;
  width:100%;
  font-size:16px;
  display:block;
  background:url(img/map-pin.png) no-repeat 3% center;
  width:100%;
  float:left;
}
.doctorsrchtextfield {
  margin:0 1% 1% 0;
  padding:14px 15px 14px 10%;
  border:1px solid #e0e2e3;
  width:100%;
  font-size:16px;
  display:block;
  background:url(img/search-icon1.png) no-repeat 3% center;
  width:100%;
  float:left;
}
.hospitalLocationResultMainIn .addressBox {
 padding: 12px 0;
 width: 100%;
 margin: 4px 0;
 border: 1px solid #dedede !important;
}
 selectColorMain {
 background: none repeat scroll 0 0 #e4ffc6 !important;
 overflow:hidden;
}
 .hospitalLocationResultMainIn {
 background: none repeat scroll 0 0 #f9f9f9;
 border: 0px solid #dedede;
 margin: 0;
 overflow: hidden;
}
 #middleContainer .middlebox-network {
 background: none repeat scroll 0 0 #fff;
 border: 1px solid #e0e2e3;
 margin: 0 -15px;
 overflow: hidden;
 padding: 3% 1.5%;
}
 .positionHospital{
  float:left !important;
  margin-bottom:10px;
}
.networkInput {
    margin-top:3% !important;
    width: 100% !important;
}
.senMailbtn{
  
}
 .graytxtBox-Location {
 background: none repeat scroll 0 0 #fff;
 float: left;
 margin: 0 0 10px;
 padding: 5px 8px;
 width: 100%;
}
 .greenSrchBtn {
 cursor: pointer;
 float: right;
 margin: 2% 0 0 0;
 padding: 12px 5px;
 text-indent: -999999px;
 width: 100%;
}
.greenSrchBtnEmail {
    cursor: pointer;
    float: right;
    margin: 2% 0 0;
    text-indent: -999999px;
    width: 100% !important;
}
.greenSrchBtnEmail input[type='submit'] {
  width: 100% !important;
  padding: 4.6% !important;
}
 .graytxtBox-Spec {
 float: left;
 margin: 0;
 padding: 5px 8px;
 width: 100%;
 margin: 0 0 10px;
}
 .stepProgress {
 margin: auto;
 width:100%;
 padding:8% 0;
 display:block;
 z-index:9;
}
 .stepProgress .stepProgressIn span.stepTxt {
 color: #444;
 display: block;
 font: 300 10px 'Segoe_UI', Arial, Helvetica, sans-serif;
 text-align: center;
 width: 100%;
}
 .tab-left-container {
 margin: 0 -10px;
 padding:0;
}
 .tab-left-container h1 {
 margin: 0;
 padding:0;
 font-size:16px !important;
 display:block;
 margin-bottom:3%;
}
 .middlebox-left p {
 margin:0 0 7% 0;
 padding:0;
 font-size:14px;
 line-height:24px;
}
 .topContainer-logo {
 float: inherit;
 height: auto;
 text-align:center;
 margin:0 -15px !important;
 padding: 10px 0 10px 0;
}
 .topContainer .con-in {
 float: inherit;
 margin: 0 -15px;
 display:block;
padding:5px 0;
 overflow:hidden;
}
 .topContainer .con-in1 {
 float: inherit;
margin-top:0;
margin-left:0;
 width:100%;
 display:block;
}
 .topContainer .con-in form .form-control {
 width:85.9%;
}
 div.cs-select:before {
 border-left: 1px solid #dfe6ef;
 content: '';
 height: 38px;
 position: absolute;
 right: 0;
 width: 45px;
}
 div.cs-select {
 width: 100% !important;
 z-index: 100;
 margin-left:0 !important;
}
 .topContainer-us-icon {
 margin: 0;
 padding: 0;
 padding-bottom:5%;
 z-index: 9999;
 float:inherit;
 display:block;
 z-index:999;
 clear:both;
}
 .header {
 margin: 0 1%;
 padding: 0;
 margin-top:1.5%;
}
 #middleContainer .middlebox-left {
 background: none repeat scroll 0 0 #fff;
 border: 1px solid #e0e2e3;
 margin: 3% -3% 5% -3% !important;
 padding: 0;
}
 #middleContainer .middlebox-right {
 margin: 5% -3% 0 -3%;
 padding: 0;
}
 #middleContainer .middlebox-right .calander {
 margin: 0 0 5%;
}
 #middleContainer .middlebox-right .doctor {
 margin: 0 0 5%;
}
 #middleContainer .middlebox-right .claim {
 margin: 0 0 5%;
}
 #middleContainer .middlebox-right .network {
 margin: 0 0 5%;
}
.footer {
 padding:0;
 margin:0 1%;
}
 .topContainer-us-icon li {
 display:inline-table;
 list-style: none outside none;
 margin:0 auto;
 display:block;
 padding-left: 5px;
}
 .topContainer-us-icon .dropdown-user {
 left: -160px;
 padding: 10px 15px !important;
 right: 0;
}
 .topContainer-us-icon .dropdown-alerts {
 left: -115px;
 padding: 10px 15px !important;
 right: 0;
}
 .topContainer-us-icon .dropdown-tasks {
 left: -200px;
 padding: 10px 15px !important;
 right: 0;
}
 .navbar-toggle {
 margin-right: 10px !;
}
 .tab-pane .tab-left-container {
 margin: 0;
 padding:0 -15px !important;
}
 .tab-pane .tab-left-container {
 margin: 0 -15px;
 padding:0 -15px !important;
}
.tab-pane .tab-left-container h1 {
 margin: 0;
 padding:0;
 font-size:14px;
 display:block;
 margin-bottom:3%;
}
 .xdsoft_datetimepicker .xdsoft_mounthpicker {
 margin-top: -40px !important;
 position: relative;
 text-align: center;
}
 .xdsoft_datetimepicker .xdsoft_label {
 background-color: #8a8a8a;
 color: #fff;
 cursor: pointer;
 display: inline;
 float: inherit;
 font-size: 22px;
 font-weight: 300;
 line-height: 20px;
 margin: 0;
 padding: 0 3px !important;
 position: relative;
 text-align: center;
 width: 100%;
 z-index: 99;
}
 div.cs-select {
 display: inline-block;
 vertical-align: middle;
 position: relative;
 text-align: left;
 background: #fff;
 z-index: 100;
 width: 100%;
 
 max-width: 500px;
 -webkit-touch-callout: none;
 -webkit-user-select: none;
 -khtml-user-select: none;
 -moz-user-select: none;
 -ms-user-select: none;
 user-select: none;
 border:1px solid #dfe6ef;
 border-radius: 0;
 box-shadow: 0 0 0 rgba(0, 0, 0, 0.075);
 color: #8d8d8d;
 margin-left:0;
 font-size: 14px;
 font-weight: 400;
}
   .topContainer-us-icon-pad {
  margin:0 -15px !important;

}
 #middleContainer .valueaddedBox {
    margin:0 -14px;
  margin-top:-3px;
}
   .img-responsive {
    width: 14%;
}
  
.textCenter {
    text-align: center !important;
}

.textbold p {
    line-height: 24px;
    margin:0;
    text-align: center;
}

#middleContainer .dashboard-left_box IMG {
    text-align: center !important;
}

 .policy-pad{ margin-left:-5% !important; margin-right:-5% !important; clear:both;}
.spacer{height:20px}



.tab-mid-container-buy a {
  text-align:center;
  display:block;
  width:100%;
}

#middleContainer .wecare-leftTop {
    margin: 0 -5%;
}

}
 @media(max-width:320px) {
   
   #middleContainer .wecare-leftTop {
    margin: 0 -5%;
}
 
  .policy-pad{ margin-left:-5% !important; margin-right:-5% !important; }

 .topContainer-us-icon li a {
    background: #fff none repeat scroll 0 0;
    border-radius: 20px;
    display: block;
    font-size: 24px;
    height: 40px;
    line-height: 40px;
    padding: 0;
    text-align: center;
    width: 30px;
}
 
 #middleContainer .valueaddedBox {
    margin:0 -15px;
    margin-top:-1px;
}

   .img-responsive {
    width: 27%;
}
   
   .topContainer-us-icon1 {
margin-top:0 !important;
}
   
 .stepProgress {
 margin: auto;
 width:100%;
 padding:8% 0;
 display:block;
 z-index:9;
}
 .hospitalLocationSearch {
 /*background: url('img/zigzagHospital.png') no-repeat scroll center bottom rgba(0, 0, 0, 0); */
 float: left;
 padding:15px 0 10px 0;
 width: 100%;
}
 .stepProgress .stepProgressIn span.stepTxt {
 color: #444;
 display: block;
 
 text-align: center;
 width: 100%;
}
 .tab-left-container {
 margin: 0 -15px;
 padding:0;
}
 .tab-left-container h1 {
 margin: 0;
 padding:0;
 font-size:16px !important;
 display:block;
 margin-bottom:3%;
}
 .middlebox-left p {
 margin:0 0 7% 0;
 padding:0;
 font-size:14px;
 line-height:24px;
}
 .topContainer-logo {
 float: inherit;
 height: auto;
 text-align:center;
 margin:0 -15px !important;
 padding: 10px 0 10px 0;
}
 .topContainer .con-in {
 float: inherit;
 margin: 0 -15px;
 display:block;
padding:5px 0;
 overflow:hidden;
}
 .topContainer .con-in1 {
 float: inherit;
 margin:0;
 width:100%;
 display:block;
}
 .topContainer .con-in form .form-control {
 width:84% !important;
}
 div.cs-select:before {
 border-left: 1px solid #dfe6ef;
 content: '';
 height: 38px;
 position: absolute;
 right: 0;
 width: 45px;
}
 div.cs-select {
 width: 100% !important;
 z-index: 100;
 margin-left:0 !important;
}


.topContainer-us-icon {
margin-top:13px !important; 
 margin-left:39% !important;

}

 .header {
 margin: 0 0;
 padding: 0;
 margin-top:2%;
}
 #middleContainer .middlebox-left {
 background: none repeat scroll 0 0 #fff;
 border: 1px solid #e0e2e3;
 margin: 3% -6% 5% -6% !important;
 padding: 0;
}
 #middleContainer .middlebox-right {
 margin: 5% -6% 0 -6%;
 padding: 0;
}
 #middleContainer .middlebox-right .calander {
 margin: 0 0 5%;
}
 #middleContainer .middlebox-right .doctor {
 margin: 0 0 5%;
}
 #middleContainer .middlebox-right .claim {
 margin: 0 0 5%;
}
 #middleContainer .middlebox-right .network {
 margin: 0 0 5%;
}
.footer {
 padding:0;
 margin:0 0;
}
 .topContainer-us-icon li {
 display:inline-table;
 list-style: none outside none;
 margin:0 auto;
 display:block;
 padding-left: 5px;
}
 .topContainer-us-icon .dropdown-user {
 left: -160px;
 padding: 10px 15px !important;
 right: 0;
}
 .topContainer-us-icon .dropdown-alerts {
 left: -115px;
 padding: 10px 15px !important;
 right: 0;
}
 .topContainer-us-icon .dropdown-tasks {
 left: -200px;
 padding: 10px 15px !important;
 right: 0;
}
 .navbar-toggle {
 margin-right: 10px !;
}
 .tab-pane .tab-left-container {
 margin: 0;
 padding:0 -15px !important;
}
 .tab-pane .tab-left-container {
 margin: 0 -15px;
 padding:0 -15px !important;
}
.tab-pane .tab-left-container h1 {
 margin: 0;
 padding:0;
 font-size:14px;
 display:block;
 margin-bottom:3%;
}
 .xdsoft_datetimepicker .xdsoft_mounthpicker {
 margin-top: -40px !important;
 position: relative;
 text-align: center;
}
 .xdsoft_datetimepicker .xdsoft_label {
 background-color: #8a8a8a;
 color: #fff;
 cursor: pointer;
 display: inline;
 float: inherit;
 font-size: 22px;
 font-weight: 300;
 line-height: 20px;
 margin: 0;
 padding: 0 3px !important;
 position: relative;
 text-align: center;
 width: 100%;
 z-index: 99;
}

 div.cs-select {
 display: inline-block;
 vertical-align: middle;
 position: relative;
 text-align: left;
 background: #fff;
 z-index: 100;
 width: 100% !important;
 
 max-width: 500px;
 -webkit-touch-callout: none;
 -webkit-user-select: none;
 -khtml-user-select: none;
 -moz-user-select: none;
 -ms-user-select: none;
 user-select: none;
 border:1px solid #dfe6ef;
 border-radius: 0;
 box-shadow: 0 0 0 rgba(0, 0, 0, 0.075);
 color: #8d8d8d;
 margin-left:0;
 font-size: 14px;
 font-weight: 400;
}






#middleContainer .dashboard-left {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #e0e2e3;
    margin:0 -15px;
  margin-bottom:10px;
    padding:2% 2% 4% 2%;
  overflow:hidden;
}

#middleContainer .dashboard-left  img{

    margin:0 auto;
  margin-bottom:0;
    padding:0;
  overflow:hidden;
}

#middleContainer .dashboard-left h1{
    font-size:20px;
  color:#2e7d07;
  text-align:center;
  margin:2% 0 5% 0;
  padding:0;
}
#middleContainer .dashboard-left h2{
    font-size:20px;
  color:#2e7d07;
  margin:2% 0 5% 0;
  padding:0;
  font-style:italic;
  text-align:center;
}

#middleContainer .dashboard-left h2 span{
    font-size:20px;
  color:#444;
  font-style:normal;
}

#middleContainer .dashboard-left1 {
    background: none repeat scroll 0 0 #fff;
    border: 1px solid #e0e2e3;
    margin:0 -15px;
  margin-bottom:10px;
    padding:2% 2% 4% 2%;
  overflow:hidden;
}

 .sorting{float:inherit; margin-top:10px; clear:both;}


.spacer{height:20px}

}
 @media(max-width:240px) {
 
 .policy-pad{padding:0 -15px !important; margin-left:-5%; margin-right:-5%; }

 
 .sorting{float:inherit; margin-top:10px; clear:both;}
 .topContainer-us-icon li a {
    background: #fff none repeat scroll 0 0;
    border-radius: 20px;
    display: block;
    font-size: 24px;
    height: 40px;
    line-height: 40px;
    padding: 0;
    text-align: center;
    width: 20px;
}

.topContainer-us-icon {
margin-top:13px !important; 
 margin-left:10% !important;

}
}



.downloadCenter{
  width:100%;
  background:#fbfbfb;
  margin-bottom:6%;
  text-align:center;
  border:1px solid #e0e2e3;
  padding:0 0 3% 0;
  color:#444444;
  overflow:hidden;
  
}
.downloadCenter .top{
  padding:0  0 5% 0;
  margin:0;
}
.downloadCenter .top a{
  width:100%;
  display:block;
  padding:3% 0;
  text-align:center;
  color:#5e9d2d;
  text-decoration:none;
  
  text-decoration:nonel
}
.downloadCenter .top a:hover{
  width:100%;
  display:block;
  background:#fff;
  text-align:center;
  color:#444;
  
  text-decoration:nonel
}
.downloadCenter .bottom{
  padding:3% 0 5% 0;
  margin:0;
}
.downloadCenter .bottom a{
  text-align:center;
  color:#444;
  border:1px solid #5e9d2d;
  padding:5px 10px;
  text-decoration:none;
  
  text-decoration:nonel
}
.downloadCenter .bottom a:hover{
  background:#5e9d2d;
  text-align:center;
  color:#fff;
  
  text-decoration:nonel
}
.downloadCenter span{
  color:#5e9d2d;
  font:normal 24px/35px 'SEGOEUIL', Arial, Helvetica, sans-serif;
  font-weight:600;
}
</style><link href="css/font-awesome.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="css/demo.css" />
<link rel="stylesheet" type="text/css" href="css/cs-select.css" />
<!--<link rel="stylesheet" type="text/css" href="css/jquery.datetimepicker.css"/>-->
<style type='text/css'>
    .xdsoft_datetimepicker {
  box-shadow: 0 5px 15px -5px rgba(0, 0, 0, 0.506);
  background: #fff;

  color: #333;
  padding: 8px;
  padding-left: 0;
  padding-top: 2px;
  position: absolute;
  z-index: 9999;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  display: none;
  width:100%;
}
.xdsoft_datetimepicker.xdsoft_rtl {
  padding: 8px 0 8px 8px;
}

.xdsoft_datetimepicker iframe {
  position: absolute;
  left: 0;
  top: 0;
  width: 75px;
  height: 210px;
  background: transparent;
  border: none;
}

/*For IE8 or lower*/
.xdsoft_datetimepicker button {
  border: none !important;
}

.xdsoft_noselect {
  -webkit-touch-callout: none;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  -o-user-select: none;
  user-select: none;
}

.xdsoft_noselect::selection { background: transparent }
.xdsoft_noselect::-moz-selection { background: transparent }

.xdsoft_datetimepicker.xdsoft_inline {
  display: inline-block;
  position: static;
  box-shadow: none;
}

.xdsoft_datetimepicker * {
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  padding: 0;
  margin: 0;
}

.xdsoft_datetimepicker .xdsoft_datepicker, .xdsoft_datetimepicker .xdsoft_timepicker {
  display: none;
}

.xdsoft_datetimepicker .xdsoft_datepicker.active, .xdsoft_datetimepicker .xdsoft_timepicker.active {
  display: block;
}

.xdsoft_datetimepicker .xdsoft_datepicker {
  width: 90%;
  float: left;
  margin: 5%;
}
.xdsoft_datetimepicker.xdsoft_rtl .xdsoft_datepicker {
  float: right;
  margin-right: 8px;
  margin-left: 0;
}

.xdsoft_datetimepicker.xdsoft_showweeks .xdsoft_datepicker {
  width: 256px;
  
}

.xdsoft_datetimepicker .xdsoft_timepicker {
  width: 58px;
  float: left;
  text-align: center;
  margin-left: 8px;
  margin-top: 0;
  display:none !important;
}
.xdsoft_datetimepicker.xdsoft_rtl .xdsoft_timepicker {
  float: right;
  margin-right: 8px;
  margin-left: 0;
}

.xdsoft_datetimepicker .xdsoft_datepicker.active+.xdsoft_timepicker {
  margin-top: 8px;
  margin-bottom: 3px
}

.xdsoft_datetimepicker .xdsoft_mounthpicker {
  position: relative;
  text-align: center;
  margin-top:-60px !important;
  padding-bottom:50px
}

.xdsoft_datetimepicker .xdsoft_label i,
.xdsoft_datetimepicker .xdsoft_prev,
.xdsoft_datetimepicker .xdsoft_next,
.xdsoft_datetimepicker .xdsoft_today_button {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAAAeCAYAAADaW7vzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6Q0NBRjI1NjM0M0UwMTFFNDk4NkFGMzJFQkQzQjEwRUIiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6Q0NBRjI1NjQ0M0UwMTFFNDk4NkFGMzJFQkQzQjEwRUIiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpDQ0FGMjU2MTQzRTAxMUU0OTg2QUYzMkVCRDNCMTBFQiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpDQ0FGMjU2MjQzRTAxMUU0OTg2QUYzMkVCRDNCMTBFQiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PoNEP54AAAIOSURBVHja7Jq9TsMwEMcxrZD4WpBYeKUCe+kTMCACHZh4BFfHO/AAIHZGFhYkBBsSEqxsLCAgXKhbXYOTxh9pfJVP+qutnZ5s/5Lz2Y5I03QhWji2GIcgAokWgfCxNvcOCCGKqiSqhUp0laHOne05vdEyGMfkdxJDVjgwDlEQgYQBgx+ULJaWSXXS6r/ER5FBVR8VfGftTKcITNs+a1XpcFoExREIDF14AVIFxgQUS+h520cdud6wNkC0UBw6BCO/HoCYwBhD8QCkQ/x1mwDyD4plh4D6DDV0TAGyo4HcawLIBBSLDkHeH0Mg2yVP3l4TQMZQDDsEOl/MgHQqhMNuE0D+oBh0CIr8MAKyazBH9WyBuKxDWgbXfjNf32TZ1KWm/Ap1oSk/R53UtQ5xTh3LUlMmT8gt6g51Q9p+SobxgJQ/qmsfZhWywGFSl0yBjCLJCMgXail3b7+rumdVJ2YRss4cN+r6qAHDkPWjPjdJCF4n9RmAD/V9A/Wp4NQassDjwlB6XBiCxcJQWmZZb8THFilfy/lfrTvLghq2TqTHrRMTKNJ0sIhdo15RT+RpyWwFdY96UZ/LdQKBGjcXpcc1AlSFEfLmouD+1knuxBDUVrvOBmoOC/rEcN7OQxKVeJTCiAdUzUJhA2Oez9QTkp72OTVcxDcXY8iKNkxGAJXmJCOQwOa6dhyXsOa6XwEGAKdeb5ET3rQdAAAAAElFTkSuQmCC);
}

.xdsoft_datetimepicker .xdsoft_label i {
  opacity: 0.5;
  background-position: -92px -19px;
  display: none !important;
  width: 9px;
  height: 20px;
  vertical-align: middle;
}

.xdsoft_datetimepicker .xdsoft_prev {
  float: left;
  background-position: -20px 0;
}
.xdsoft_datetimepicker .xdsoft_today_button {
  float: left;
  background-position: -70px 0;
  margin-left: 5px;
  display:none !important;
}

.xdsoft_datetimepicker .xdsoft_next {
  float: right;
  background-position: 0 0;
}

.xdsoft_datetimepicker .xdsoft_next,
.xdsoft_datetimepicker .xdsoft_prev ,
.xdsoft_datetimepicker .xdsoft_today_button {
  background-color: transparent;
  background-repeat: no-repeat;
  border: 0 none;
  cursor: pointer;
  display: block;
  height: 30px;
  opacity: 0.5;
  -ms-filter: 'progid:DXImageTransform.Microsoft.Alpha(Opacity=50)';
  outline: medium none;
  overflow: hidden;
  padding: 0;
  position: relative;
  text-indent: 100%;
  white-space: nowrap;
  width: 20px;
  min-width: 0;
}

.xdsoft_datetimepicker .xdsoft_timepicker .xdsoft_prev,
.xdsoft_datetimepicker .xdsoft_timepicker .xdsoft_next {
  float: none;
  background-position: -40px -15px;
  height: 15px;
  width: 30px;
  display: block;
  margin-left: 14px;
  margin-top: 7px;
}
.xdsoft_datetimepicker.xdsoft_rtl .xdsoft_timepicker .xdsoft_prev,
.xdsoft_datetimepicker.xdsoft_rtl .xdsoft_timepicker .xdsoft_next {
  float: none;
  margin-left: 0;
  margin-right: 14px;
}

.xdsoft_datetimepicker .xdsoft_timepicker .xdsoft_prev {
  background-position: -40px 0;
  margin-bottom: 7px;
  margin-top: 0;
}

.xdsoft_datetimepicker .xdsoft_timepicker .xdsoft_time_box {
  height: 151px;
  overflow: hidden;
  border-bottom: 1px solid #ddd;
}

.xdsoft_datetimepicker .xdsoft_timepicker .xdsoft_time_box >div >div {
  background: #f5f5f5;
  border-top: 1px solid #ddd;
  color: #666;
  font-size: 12px;
  text-align: center;
  border-collapse: collapse;
  cursor: pointer;
  border-bottom-width: 0;
  height: 25px;
  line-height: 25px;
}

.xdsoft_datetimepicker .xdsoft_timepicker .xdsoft_time_box >div > div:first-child {
  border-top-width: 0;
}

.xdsoft_datetimepicker .xdsoft_today_button:hover,
.xdsoft_datetimepicker .xdsoft_next:hover,
.xdsoft_datetimepicker .xdsoft_prev:hover {
  opacity: 1;
  -ms-filter: 'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)';
}

.xdsoft_datetimepicker .xdsoft_label {
  display: inline;
  position: relative;
  z-index: 99;
  margin: 0;
  padding: 7px 3px;
  font-size: 18px;
  line-height: 20px;
  font-weight: 300;
  background-color: #8a8a8a;
  float: inherit;
  color:;//#FFF;
  width: 100%;
  text-align: center;
  cursor: pointer;
}

.xdsoft_datetimepicker .xdsoft_label:hover>span {
  text-decoration: underline;
}

.xdsoft_datetimepicker .xdsoft_label:hover i {
  opacity: 1.0;
}

.xdsoft_datetimepicker .xdsoft_label > .xdsoft_select {
  border: 1px solid #ccc;
  position: absolute;
  right: 0;
  top: 30px;
  z-index: 101;
  display: none;
  background: #fff;
  max-height: 160px;
  overflow-y: hidden;
}

.xdsoft_datetimepicker .xdsoft_label > .xdsoft_select.xdsoft_monthselect{ right: -7px }
.xdsoft_datetimepicker .xdsoft_label > .xdsoft_select.xdsoft_yearselect{ right: 2px }
.xdsoft_datetimepicker .xdsoft_label > .xdsoft_select > div > .xdsoft_option:hover {
  color: #fff;
  background: #5e9d2d;
}

.xdsoft_datetimepicker .xdsoft_label > .xdsoft_select > div > .xdsoft_option {
  padding: 2px 10px 2px 5px;
  text-decoration: none !important;
  color:#444;
  font-size:16px;
}

.xdsoft_datetimepicker .xdsoft_label > .xdsoft_select > div > .xdsoft_option.xdsoft_current {
  background: #eaeff5;
  /*box-shadow: #178fe5 0 1px 3px 0 inset;*/
  color: #444;
  font-weight: 700;
}

.xdsoft_datetimepicker .xdsoft_month {
  width: 100px;
  text-align: left;
}

.xdsoft_datetimepicker .xdsoft_calendar {
  clear: both;
}

.xdsoft_datetimepicker .xdsoft_year{
  width: 48px;
  margin-left: 5px;
}

.xdsoft_datetimepicker .xdsoft_calendar table {
  border-collapse: collapse;
  width: 100%;
  

}

.xdsoft_datetimepicker .xdsoft_calendar td > div {
  padding-right: 5px;

}

.xdsoft_datetimepicker .xdsoft_calendar th {
  height: 40px;
}

.xdsoft_datetimepicker .xdsoft_calendar td,.xdsoft_datetimepicker .xdsoft_calendar th {
  width: 14.2857142%;
  background: #fff;

  color: #666;
  font-size: 16px;
  text-align:center;
  vertical-align: middle;
  padding: 0;
  border-collapse: collapse;
  cursor: pointer;
  height: 40px;
}
.xdsoft_datetimepicker.xdsoft_showweeks .xdsoft_calendar td,.xdsoft_datetimepicker.xdsoft_showweeks .xdsoft_calendar th {
  width: 12.5%;
}

.xdsoft_datetimepicker .xdsoft_calendar th {
  background: #fff;
}

.xdsoft_datetimepicker .xdsoft_calendar td.xdsoft_today {
  color: #5e9d2d;
  font-weight:700;
}

.xdsoft_datetimepicker .xdsoft_calendar td.xdsoft_highlighted_default {
  background: #ffe9d2;
  box-shadow: #ffb871 0 1px 4px 0 inset;
  color: #000;
}
.xdsoft_datetimepicker .xdsoft_calendar td.xdsoft_highlighted_mint {
  background: #c1ffc9;
  box-shadow: #00dd1c 0 1px 4px 0 inset;
  color: #000;
}

.xdsoft_datetimepicker .xdsoft_calendar td.xdsoft_default,
.xdsoft_datetimepicker .xdsoft_calendar td.xdsoft_current,
.xdsoft_datetimepicker .xdsoft_timepicker .xdsoft_time_box >div >div.xdsoft_current {
  background: ;//#5e9d2d;
  box-shadow: /*#178fe5*/ 0 1px 3px 0 inset;
  color: #fff;
  font-weight: 700;
}

.xdsoft_datetimepicker .xdsoft_calendar td.xdsoft_other_month,
.xdsoft_datetimepicker .xdsoft_calendar td.xdsoft_disabled,
.xdsoft_datetimepicker .xdsoft_time_box >div >div.xdsoft_disabled {
  opacity: 0.5;
  -ms-filter: 'progid:DXImageTransform.Microsoft.Alpha(Opacity=50)';
  cursor: default;
}

.xdsoft_datetimepicker .xdsoft_calendar td.xdsoft_other_month.xdsoft_disabled {
  opacity: 0.2;
  -ms-filter: 'progid:DXImageTransform.Microsoft.Alpha(Opacity=20)';
}

.xdsoft_datetimepicker .xdsoft_calendar td:hover,
.xdsoft_datetimepicker .xdsoft_timepicker .xdsoft_time_box >div >div:hover {
  color:  !important;
  background:  !important;
  box-shadow: none !important;
}

.xdsoft_datetimepicker .xdsoft_calendar td.xdsoft_current.xdsoft_disabled:hover,
.xdsoft_datetimepicker .xdsoft_timepicker .xdsoft_time_box>div>div.xdsoft_current.xdsoft_disabled:hover {
  background: #33aaff !important;
  box-shadow: #178fe5 0 1px 3px 0 inset !important;
  color: #fff !important;
}

.xdsoft_datetimepicker .xdsoft_calendar td.xdsoft_disabled:hover,
.xdsoft_datetimepicker .xdsoft_timepicker .xdsoft_time_box >div >div.xdsoft_disabled:hover {
  color: inherit  !important;
  background: inherit !important;
  box-shadow: inherit !important;
}

.xdsoft_datetimepicker .xdsoft_calendar th {
  font-weight: 700;
  text-align: center;
  color: #999;
  cursor: default;
}

.xdsoft_datetimepicker .xdsoft_copyright {
  color: #ccc !important;
  font-size: 10px;
  clear: both;
  float: none;
  margin-left: 8px;
}

.xdsoft_datetimepicker .xdsoft_copyright a { color: #eee !important }
.xdsoft_datetimepicker .xdsoft_copyright a:hover { color: #aaa !important }

.xdsoft_time_box {
  position: relative;
  border: 1px solid #ccc;
}
.xdsoft_scrollbar >.xdsoft_scroller {
  background: #ccc !important;
  height: 20px;
  border-radius: 3px;
}
.xdsoft_scrollbar {
  position: absolute;
  width: 7px;
  right: 0;
  top: 0;
  bottom: 0;
  cursor: pointer;
}
.xdsoft_datetimepicker.xdsoft_rtl .xdsoft_scrollbar {
  left: 0;
  right: auto;
}
.xdsoft_scroller_box {
  position: relative;
}

.xdsoft_datetimepicker.xdsoft_dark {
  box-shadow: 0 5px 15px -5px rgba(255, 255, 255, 0.506);
  background: #000;
  border-bottom: 1px solid #444;
  border-left: 1px solid #333;
  border-right: 1px solid #333;
  border-top: 1px solid #333;
  color: #ccc;
}

.xdsoft_datetimepicker.xdsoft_dark .xdsoft_timepicker .xdsoft_time_box {
  border-bottom: 1px solid #222;
}
.xdsoft_datetimepicker.xdsoft_dark .xdsoft_timepicker .xdsoft_time_box >div >div {
  background: #0a0a0a;
  border-top: 1px solid #222;
  color: #999;
}

.xdsoft_datetimepicker.xdsoft_dark .xdsoft_label {
  background-color: #000;
}
.xdsoft_datetimepicker.xdsoft_dark .xdsoft_label > .xdsoft_select {
  border: 1px solid #333;
  background: #000;
}

.xdsoft_datetimepicker.xdsoft_dark .xdsoft_label > .xdsoft_select > div > .xdsoft_option:hover {
  color: #000;
  background: #007fff;
}

.xdsoft_datetimepicker.xdsoft_dark .xdsoft_label > .xdsoft_select > div > .xdsoft_option.xdsoft_current {
  background: #cc5500;
  box-shadow: #b03e00 0 1px 3px 0 inset;
  color: #000;
}

.xdsoft_datetimepicker.xdsoft_dark .xdsoft_label i,
.xdsoft_datetimepicker.xdsoft_dark .xdsoft_prev,
.xdsoft_datetimepicker.xdsoft_dark .xdsoft_next,
.xdsoft_datetimepicker.xdsoft_dark .xdsoft_today_button {
  background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAAAeCAYAAADaW7vzAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6QUExQUUzOTA0M0UyMTFFNDlBM0FFQTJENTExRDVBODYiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6QUExQUUzOTE0M0UyMTFFNDlBM0FFQTJENTExRDVBODYiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpBQTFBRTM4RTQzRTIxMUU0OUEzQUVBMkQ1MTFENUE4NiIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDpBQTFBRTM4RjQzRTIxMUU0OUEzQUVBMkQ1MTFENUE4NiIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pp0VxGEAAAIASURBVHja7JrNSgMxEMebtgh+3MSLr1T1Xn2CHoSKB08+QmR8Bx9A8e7RixdB9CKCoNdexIugxFlJa7rNZneTbLIpM/CnNLsdMvNjM8l0mRCiQ9Ye61IKCAgZAUnH+mU3MMZaHYChBnJUDzWOFZdVfc5+ZFLbrWDeXPwbxIqrLLfaeS0hEBVGIRQCEiZoHQwtlGSByCCdYBl8g8egTTAWoKQMRBRBcZxYlhzhKegqMOageErsCHVkk3hXIFooDgHB1KkHIHVgzKB4ADJQ/A1jAFmAYhkQqA5TOBtocrKrgXwQA8gcFIuAIO8sQSA7hidvPwaQGZSaAYHOUWJABhWWw2EMIH9QagQERU4SArJXo0ZZL18uvaxejXt/Em8xjVBXmvFr1KVm/AJ10tRe2XnraNqaJvKE3KHuUbfK1E+VHB0q40/y3sdQSxY4FHWeKJCunP8UyDdqJZenT3ntVV5jIYCAh20vT7ioP8tpf6E2lfEMwERe+whV1MHjwZB7PBiCxcGQWwKZKD62lfGNnP/1poFAA60T7rF1UgcKd2id3KDeUS+oLWV8DfWAepOfq00CgQabi9zjcgJVYVD7PVzQUAUGAQkbNJTBICDhgwYTjDYD6XeW08ZKh+A4pYkzenOxXUbvZcWz7E8ykRMnIHGX1XPl+1m2vPYpL+2qdb8CDAARlKFEz/ZVkAAAAABJRU5ErkJggg==);
}

.xdsoft_datetimepicker.xdsoft_dark .xdsoft_calendar td,
.xdsoft_datetimepicker.xdsoft_dark .xdsoft_calendar th {
  background: #0a0a0a;
  border: 1px solid #222;
  color: #999;
}

.xdsoft_datetimepicker.xdsoft_dark .xdsoft_calendar th {
  background: #0e0e0e;
}

.xdsoft_datetimepicker.xdsoft_dark .xdsoft_calendar td.xdsoft_today {
  color: #cc5500;
}

.xdsoft_datetimepicker.xdsoft_dark .xdsoft_calendar td.xdsoft_highlighted_default {
  background: #ffe9d2;
  box-shadow: #ffb871 0 1px 4px 0 inset;
  color:#000;
}
.xdsoft_datetimepicker.xdsoft_dark .xdsoft_calendar td.xdsoft_highlighted_mint {
  background: #c1ffc9;
  box-shadow: #00dd1c 0 1px 4px 0 inset;
  color:#000;
}

.xdsoft_datetimepicker.xdsoft_dark .xdsoft_calendar td.xdsoft_default,
.xdsoft_datetimepicker.xdsoft_dark .xdsoft_calendar td.xdsoft_current,
.xdsoft_datetimepicker.xdsoft_dark .xdsoft_timepicker .xdsoft_time_box >div >div.xdsoft_current {
  background: #cc5500;
  box-shadow: #b03e00 0 1px 3px 0 inset;
  color: #000;
}

.xdsoft_datetimepicker.xdsoft_dark .xdsoft_calendar td:hover,
.xdsoft_datetimepicker.xdsoft_dark .xdsoft_timepicker .xdsoft_time_box >div >div:hover {
  color: #000 !important;
  background: #007fff !important;
}

.xdsoft_datetimepicker.xdsoft_dark .xdsoft_calendar th {
  color: #666;
}

.xdsoft_datetimepicker.xdsoft_dark .xdsoft_copyright { color: #333 !important }
.xdsoft_datetimepicker.xdsoft_dark .xdsoft_copyright a { color: #111 !important }
.xdsoft_datetimepicker.xdsoft_dark .xdsoft_copyright a:hover { color: #555 !important }

.xdsoft_dark .xdsoft_time_box {
  border: 1px solid #333;
}

.xdsoft_dark .xdsoft_scrollbar >.xdsoft_scroller {
  background: #333 !important;
}
.xdsoft_datetimepicker .xdsoft_save_selected {
    display: block;
    border: 1px solid #dddddd !important;
    margin-top: 5px;
    width: 100%;
    color: #454551;
    font-size: 13px;
}
.xdsoft_datetimepicker .blue-gradient-button {
  font-family: 'museo-sans', 'Book Antiqua', sans-serif;
  font-size: 12px;
  font-weight: 300;
  color: #82878c;
  height: 28px;
  position: relative;
  padding: 4px 17px 4px 33px;
  border: 1px solid #d7d8da;
  background: -moz-linear-gradient(top, #fff 0%, #f4f8fa 73%);
  /* FF3.6+ */
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #fff), color-stop(73%, #f4f8fa));
  /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top, #fff 0%, #f4f8fa 73%);
  /* Chrome10+,Safari5.1+ */
  background: -o-linear-gradient(top, #fff 0%, #f4f8fa 73%);
  /* Opera 11.10+ */
  background: -ms-linear-gradient(top, #fff 0%, #f4f8fa 73%);
  /* IE10+ */
  background: linear-gradient(to bottom, #fff 0%, #f4f8fa 73%);
  /* W3C */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#fff', endColorstr='#f4f8fa',GradientType=0 );
/* IE6-9 */
}
.xdsoft_datetimepicker .blue-gradient-button:hover, .xdsoft_datetimepicker .blue-gradient-button:focus, .xdsoft_datetimepicker .blue-gradient-button:hover span, .xdsoft_datetimepicker .blue-gradient-button:focus span {
  color: #454551;
  background: -moz-linear-gradient(top, #f4f8fa 0%, #FFF 73%);
  /* FF3.6+ */
  background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #f4f8fa), color-stop(73%, #FFF));
  /* Chrome,Safari4+ */
  background: -webkit-linear-gradient(top, #f4f8fa 0%, #FFF 73%);
  /* Chrome10+,Safari5.1+ */
  background: -o-linear-gradient(top, #f4f8fa 0%, #FFF 73%);
  /* Opera 11.10+ */
  background: -ms-linear-gradient(top, #f4f8fa 0%, #FFF 73%);
  /* IE10+ */
  background: linear-gradient(to bottom, #f4f8fa 0%, #FFF 73%);
  /* W3C */
  filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f4f8fa', endColorstr='#FFF',GradientType=0 );
  /* IE6-9 */
}
</style>
<link href="css/owl.carousel.css" rel="stylesheet">
<link href="css/owl.theme.css" rel="stylesheet">
<link rel="stylesheet" href="css/globals.css">
<link rel="stylesheet" href="css/mobile.css">
<!-- End Combine and Compress These CSS Files -->
<link rel="stylesheet" href="css/responsive-tables.css">
<script src="js/jquery-1.11.3.min.js" type="text/javascript"></script>
<script src="js/common.js" type="text/javascript"></script>
 <script src="js/jquery.barrating.js"></script> 
<script>
   
$(document).ready(function(){  
              $('.switchpolicy li').click(function()
                   {
                       $('#switchpolicy').submit();
                   });
 });</script>

 <meta charset="UTF-8">
  <meta name="Generator" content="EditPlus�">
  <meta name="Author" content="">
  <meta name="Keywords" content="">
  <meta name="Description" content="">

</head>

 
   


<body>
<div class=" container-fluid">
  <div class="topContainer">
    <div class="col-md-8">
      <div class="topContainer-logo"> <a href="#"><img src="img/logo.png" alt="" title=""></a> </div>
      
      <div class="con-in1">
        <form action="" method="post" name="switchpolicy" id="switchpolicy" class="switchpolicy">
          <select class="cs-select cs-skin-border fa arrow " name="switchpolicysel">
            <option value="" disabled >Switch Policy</option>
                        <option value="10003683" selected>10003683</option>
			          </select>
          
        </form>
      </div>
      <!--<div class="con-in">
        <form>
          <input type="text" class="form-control" placeholder="Search">
          <input type="submit" class="btn btn-danger" value="">
        </form>
      </div>-->
    </div>
    <div class="col-md-4 topContainer-us-icon-pad">
      
      <div class="topContainer-us-icon topContainer-us-icon1 ">
        <ul>
          <li><a href="logout.php"><i class="fa fa-sign-out fa-fw"></i></a></li>
          <li class="dropdown">
          <a class="dropdown-toggle usertoggle" data-toggle="dropdown" href="#"> <i class="fa fa-fw"></i></a>  
          <ul class="dropdown-menu dropdown-tasks">
            <h1>Get in touch with us</h1>
            <h1 class="successMsg" style="display:none;">Get in touch with us</h1>
            <form action="" method="post" name="enquiryForm" id="enquiryForm" class="enquiryForm" onSubmit="return validateEnquiry()">
              <div class="Quote-box">
                <li>
                  <input type="text" class="form-control" id="employeeName" name="employeeName" required placeholder="Employee Name *" value="Ayush Agarwal">
                </li>
                <li>
                  <input type="text" class="form-control" class="phone" id="phone" required name="phone" placeholder="Phone" value="">
                </li>
                <li>
                  <input type="text" class="form-control" class="emailid" id="emailid" required name="emailid" placeholder="Email Id" value="">
                </li>
                <li>
                  <input type="text" class="form-control policyNumber"  required id="policyNumber" name="policyNumber" placeholder="Policy Number" value="10003683">
                </li>
                <li>
                  <textarea placeholder="Enquiry" rows="3" class="enquiry" required name="message" id="enquiry"></textarea>
                </li>
                <li>
                  <input name="enquiry" type="submit" class="btn btn-danger" value="Submit">
              </div>
              </li>
            </form>
            <li> <i class="fa fa-fw"></i> 1860-500-4488 | 1800-200-4488 </li>
          </ul>
          <!-- /.dropdown-tasks -->
          </li>
          <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="fa fa-user fa-fw"></i> </a>
            <ul class="dropdown-menu dropdown-user">
              <h1>Account Settings</h1>
              <li><a href="my-profile.php">My Profile</a> </li>
              <li><a href="my-profile.php?tab=Y3Bhc3M=">Change Password</a> </li>
              <li><a href="individual-policy-registration.php" target="_blank">Register Policy</a> </li>
              <li><a href="logout.php"> Logout</a> </li>
            </ul>
            <!-- /.dropdown-user --> 
          </li>
          <li class="dropdown"> <a class="dropdown-toggle" data-toggle="dropdown" href="#"> <i class="fa fa-bell fa-fw"></i> </a>
            <ul class="dropdown-menu dropdown-alerts">
                
<h1>Notifications</h1>
<li class="divider"></li>
<li><a href="#">
<h2>Notification latest</h2>
<p>Notification latest content</p>
</a> </li>
            </ul>
            <!-- /.dropdown-alerts --> 
          </li>
        </ul>
      </div>
    </div>
  </div>
</div>
<div class="col-md-12">
  <section class="header"> 
    
    <!-- nevigation -->
    <div class="navbar navbar-default" role="navigation">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      </div>
      <div class="navbar-collapse collapse">
        <ul class="nav navbar-nav">
          <li class="dropdown-toggle active"><a href="dashboard.php"><img src="img/dashboard.png" alt="" title=""> Dashboard</a></li>
          <li class="dropdown-toggle "> <a href="my_policy.php"><img src="img/policy.png" alt="" title=""> My Policy</a></li>
          <li class="dropdown-toggle  "> <a href="claims.php"><img src="img/claim.png" alt="" title=""> Claims</a> </li>
          <li class="dropdown-toggle "> <a href="value_added_services.php"><img src="img/service.png" alt="" title=""> Value Added Services</a> </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <!--nav-collapse --> 
    </div>
    <!-- nevigation closed --> 
    
  </section>
</div><section id="middleContainer">
  <div class="container-fluid">
    <div class="middlebox">
      <div class="col-md-9">
      
      <div class="dashboard-leftTop">
      	<div class="grayBorderdashboard">
        <div class="dashboardName">
        	<div class="colSm6"><strong>10003683</strong> </div>
            
        <div class="clearfix"></div></div>
        
        <table class="responsive responsivedash" width="100%">
                               <tr>
                    <td colspan="4">
					                    <font color="#cc0000"><b>No record(s) found.</b></font>
                                        </td>
                  </tr>    
			                                         
                </table>

		        </div>
      </div>
        <div class="dashboard-left">
          <div id="Our_clients">
            <div class="border-box">
              <div class="title-bg textleft">
                <h1>Free Value Added Services</h1>
              </div>
              <!-- Logo Here -->
              <div>
                <div class="brands owl-carousel">
                                  <div class="item dashboard-left_box"><a href="wecare.php">
                    <div class="col-md-4 textCenter"><img src="../religarehrcrm/fileupload/26404we-care.png" class="img-responsive" title="" alt=""></div>
                    <div class="col-md-8 textbold" align="left" >Discount Connect                    <p>Get discount coupons for </p>
                    
                    </div>
                    </a> </div>
                                   <div class="item dashboard-left_box"><a href="health_checkup.php?tab=bXlwbGFu">
                    <div class="col-md-4 textCenter"><img src="../religarehrcrm/fileupload/26329healthChec-up.png" class="img-responsive" title="" alt=""></div>
                    <div class="col-md-8 textbold" align="left" >Health CheckUp                    <p>Book an appointment for y</p>
                    
                    </div>
                    </a> </div>
                                   <div class="item dashboard-left_box"><a href="hra.php">
                    <div class="col-md-4 textCenter"><img src="../religarehrcrm/fileupload/14911hra.png" class="img-responsive" title="" alt=""></div>
                    <div class="col-md-8 textbold" align="left" >HRA                    <p>Get a Health Risk Assessm</p>
                    
                    </div>
                    </a> </div>
                    
                </div>
              </div>
              <!-- Logo Here Closed --> 
            </div>
          </div>
        </div>
        <div class="dashboard-left">
          <div id="Our_clients">
            <div class="border-box">
              <div class="title-bg textleft">
                <h1>Paid Value Added Services</h1>
              </div>
              <!-- Logo Here -->
              <div>
                <div class="brands owl-carousel ">
                                  <div class="item dashboard-left_box"><a href="health_checkup.php?tab=YnV5cGxhbg">
                    <div class="col-md-4 textCenter"><img src="../religarehrcrm/fileupload/0212026329healthChec-up.png" class="img-responsive" title="" alt=""></div>
                    <div class="col-md-8 textbold" align="center">Health CheckUp                    <p>Buy a health checkup plan</p>
                    </div>
                    </a> </div>
                    
                </div>
              </div>
              <!-- Logo Here Closed --> 
            </div>
          </div>
        </div>
        <div class="dashboard-left">
          <div id="Our_clients">
            <div class="border-box">
              <div class="title-bg textleft">
                <h2>Stay worry free..Hamesha! <span>with our products</span></h2>
              </div>
              <!-- Logo Here -->
              <div>
                <div class="brands owl-carousel ">
                      

                  
                  <div class="item dashboard-left_box1"><a href="index.html">
                    <div class="col-md-12 " align="center"><img src="img/icon2.png" width="" height="" title="" alt=""></div>
                    <div class="col-md-12 textbold" align="center">Maternity Focus Insurance Plan</div>
                    </a> </div>
                  
                  
                  
                  <div class="item dashboard-left_box1"><a href="index.html">
                    <div class="col-md-12 " align="center"><img src="img/icon3.png" width="" height="" title="" alt=""></div>
                    <div class="col-md-12 textbold" align="center">Travel Insurance Plan</div>
                    </a> </div>
                    
                    
                    <div class="item dashboard-left_box1"><a href="index.html">
                    <div class="col-md-12 " align="center"><img src="img/icon4.png" width="" height="" title="" alt=""></div>
                    <div class="col-md-12  textbold" align="center">Critical Illness Benefit Plan</div>
                    </a> </div>
                    
                    <div class="item dashboard-left_box1"><a href="index.html">
                    <div class="col-md-12 " align="center"><img src="img/icon5.png" width="" height="" title="" alt=""></div>
                    <div class="col-md-12 textbold" align="center">Personal Accident Insurance</div>
                    </a> </div>
                    
                    <div class="item dashboard-left_box1"><a href="index.html">
                    <div class="col-md-12 " align="center"><img src="img/icon6.png" width="" height="" title="" alt=""></div>
                    <div class="col-md-12 textbold" align="center">Top-up Insurance</div>
                    </a> </div>
                    
                    <div class="item dashboard-left_box1"><a href="index.html">
                    <div class="col-md-12 " align="center"><img src="img/icon7.png" width="" height="" title="" alt=""></div>
                    <div class="col-md-12 textbold" align="center">Instant Policy Renewal</div>
                    </a> </div>
                    
                    
                    <div class="item dashboard-left_box1"><a href="index.html">
                    <div class="col-md-12 " align="center"><img src="img/icon8.png" width="" height="" title="" alt=""></div>
                    <div class="col-md-12 textbold" align="center">Welcome to Religare Family</div>
                    </a> </div>
                  
                  
                </div>
              </div>
              <!-- Logo Here Closed --> 
            </div>
          </div>
        </div>
      </div>
      <div class="col-md-3">
         <!--   <script src="js/jquery.min.js"></script> -->
<script src="js/bootstrap.min.js"></script> 
<script src="js/owl.carousel.js"></script>
<script src="js/jquery.datetimepicker.js"></script> 
<script src="js/responsive-tabs.js"></script> 
<script src="js/classie.js"></script> 
<script src="js/selectFx.js"></script> 
<script src="js/jquery-ui.js"></script> 
<link media="screen" rel="stylesheet" href="css/jquery-ui.css" />







   <script>
    
  function search_function()
  {
        city_id=$("#city_id").val(); 
        $("#search_keyword_virtual").val("");
        $( "#search_keyword" ).autocomplete({
                  source: "ajax/search_doc_ajax.php?city_id="+city_id+"&",
                  data:"data[city_id]=".city_id,
                  minLength: 2, 
                  search: function(event, ui) {
                                        $("#doc_load_image").show();
                                         $("#doc_search_result").html("");
                                    },
                                    response: function(event, ui) {
                                        $("#doc_load_image").hide();
                                    },
                  select: function( event, ui ) { 
                      $("#search_keyword_virtual").val(ui.item.id);
                       
        
                            search_for_doc(ui.item.id);    
                }
                });
  }
  function search_function_side()
  {
        city_id=$("#city_id_side").val();
        $("#search_keyword_virtual_side").val("");
        $("#doc_load_image_side").show();
        $( "#search_keyword_side" ).autocomplete({
                  source: "ajax/search_doc_ajax.php?city_id="+city_id+"&",
                  data:"data[city_id]=".city_id,
                  minLength: 2,
                  search: function(event, ui) {
                                        $("#doc_load_image_side").show();
                                    },
                                    response: function(event, ui) {
                                        $("#doc_load_image_side").hide();
                                    },
                  select: function( event, ui ) { 
                       
                      $("#search_keyword_virtual_side").val(ui.item.id);
                    
                            search_for_doc(ui.item.id);    
                }
                });
                 // $("#doc_load_image_side").hide();
  }
  
   $(document).on('keyup','#search_keyword_side', function() {
   city_id=$("#city_id_side").val();
        $("#search_keyword_virtual_side").val("");
        $("#doc_load_image_side").show();
        $( "#search_keyword_side" ).autocomplete({
                  source: "ajax/search_doc_ajax.php?city_id="+city_id+"&",
                  data:"data[city_id]=".city_id,
                  minLength: 2,
                  search: function(event, ui) {
                                        $("#doc_load_image_side").show();
                                    },
                                    response: function(event, ui) {
                                        $("#doc_load_image_side").hide();
                                    },
                  select: function( event, ui ) { 
                       
                      $("#search_keyword_virtual_side").val(ui.item.id);
                    
                            search_for_doc(ui.item.id);    
                }
                });
   
   });
  function search_for_doc(search_id)
  {
      
       $("#search_result").html("");
       $("#doc_load_image").show();
       city_id=$("#city_id").val(); 
           sorting_id=$("#sorting_id").val(); 
         $.ajax({
                type:"post",
                url:'ajax/search_for_doctors.php',
                data:{'search_id':search_id,'city_id':city_id,'sorting_id':sorting_id},
                success:function(resp){                                         
                      //  $("#search_result").html(resp);
                      $("#doc_search_result").html(resp);
                       $("#doc_load_image").hide();
                        } 
                });      
  }
  
     function city_function(){ 
        $("#city_id").val("");
        $("#search_keyword").val("Doctors, Centers, Specialities, Services");
        $("#search_keyword_virtual").val("");
        
        $("#search_result").html("");
                 $( "#search_city" ).autocomplete({
                            source: "ajax/search_city_ajax.php",
                            minLength: 2,
                            search: function(event, ui) {
                                        $("#doc_load_image").show();
                                    },
                                    response: function(event, ui) {
                                        $("#doc_load_image").hide();
                                    },
                            select: function(event, ui) {
                                           $("#city_id").val(ui.item.id);
                                        
                                       }
                                   });
      }
         
         function city_function_side(){ 
                $("#city_id_side").val("");
                $("#search_keyword").val("Doctors, Centers, Specialities, Services");
                $("#search_keyword_virtual_side").val("");
               // $("#doc_load_image_side").show();
                $("#search_result").html("");
                            $( "#search_city_side" ).autocomplete({
                                    source: "ajax/search_city_ajax.php",
                                    minLength: 2,
                                    search: function(event, ui) {
                                        $("#doc_load_image_side").show();
                                    },
                                    response: function(event, ui) {
                                        $("#doc_load_image_side").hide();
                                    },
                                    select: function(event, ui) {  
                                        $("#city_id_side").val(ui.item.id);
                                    }
                             });
             }
         
         
         
         
         $(document).on('keyup','#search_city_side', function() {
     $("#city_id_side").val("");
                $("#search_keyword").val("Doctors, Centers, Specialities, Services");
                $("#search_keyword_virtual_side").val("");
               // $("#doc_load_image_side").show();
                $("#search_result").html("");
                            $( "#search_city_side" ).autocomplete({
                                    source: "ajax/search_city_ajax.php",
                                    minLength: 2,
                                    search: function(event, ui) {
                                        $("#doc_load_image_side").show();
                                    },
                                    response: function(event, ui) {
                                        $("#doc_load_image_side").hide();
                                    },
                                    select: function(event, ui) {  
                                        $("#city_id_side").val(ui.item.id);
                                    }
                             });
});
    
      function log( message ) {
      $( "<div>" ).text( message ).prependTo( "#log" );
      $( "#log" ).scrollTop( 0 );
    }
    
    function search_by_button()
    {
      //  $("#search_result").html("");
        $("#doc_load_image").show();
        city_id=$("#city_id").val(); 
        search_id= $("#search_keyword_virtual").val();
        sorting_id=$("#sorting_id").val(); 
        if(search_id=="")
            {
               // alert("Please select Any Center, Doctor ,Service ,Specialty or Ailment");
               $("#doc_load_image").hide();
                return false;
            }
         $.ajax({
                type:"post",
                url:'ajax/search_for_doctors.php',
                data:{'search_id':search_id,'city_id':city_id,'sorting_id':sorting_id},
                success:function(resp){    $("#doc_search_result").html(resp);
                         $("#doc_load_image").hide();
                        } 
                });   
    }
    $(document).ready(function(){ 
     $('.sorting li').click(function()
                   {
                       $("#doc_search_result").html("");
                  search_by_button();
                   });  });
  </script> 
  <style>    
    .doc_load_img
    {
        margin-left: 9px;
        margin-top: 29px;
    }
    .vsms {
        margin: 10px 0 0 23px;
        padding: 0;
    }
    .optional_input_large
    {
        background: #ffffff;
        width:280px;
        font:15px/28px "FS Humana",Arial,Helvetica,sans-serif;
    }
   .ui-menu-item a
   {
       font-size:11px;
   }
   .ui-menu
{
max-height: 300px;
    overflow: auto;
}
   .highlight{
          border: 1px solid #0c7d3b  ;
      }
       
</style>
<div class="middlebox-right">
          <div class="calander">
            <h1></h1>
            <input type="text" id="datetimepicker3"/>
          </div>
          <div class="doctor">
              
            <h1>Find Doctors, Centers, Services, etc.</h1>
            <div class="doctor-bot">
                <form action="wecare.php" method="post">
              <input type="text"   class="doctor-map-pin"   id="search_city_side"   name="city_name"   value="Location*" onFocus="if (this.value =='Location*') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Location*';}" >
<!--              <input type="text"   class="doctor-map-pin"   id="search_city_side" onkeyup="city_function_side()"  name="city_name"   value="" onFocus="if (this.value =='') {this.value = '';}" onBlur="if (this.value == '') {this.value = '';}" >-->
              
              <input id="city_id_side" type="hidden" class="txtfield_185"  name="city_id" value='' />
              <input type="text" placeholder="Doctors, Centers, Specialities, Services" name="keyword"  class="doctor-sesarch"  id="search_keyword_side"   onFocus="if (this.value == 'Doctors, Centers, Specialities, Services.') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Doctors, Centers, Specialities, Services.';}"  value="Doctors, Centers, Specialities, Services."  >
<!--               <input type="text" placeholder="Doctors, Centers, Specialities, Services" name="keyword"  class="doctor-sesarch"  id="search_keyword_side"  onkeyup="search_function_side()" onFocus="if (this.value == 'Doctors, Centers, Specialities, Services.') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Doctors, Centers, Specialities, Services.';}"  value="Doctors, Centers, Specialities, Services."  >-->
              <input id="search_keyword_virtual_side" type="hidden" name="search_keyword_virtual" />
              <img style="display:none;" id="doc_load_image_side" src="images/loading.gif" />
              <input type="submit" value="Search >" >
            </form>
              </div>
          </div>
          <div class="claim">
            <div id="demo">
              <div id="owl-demo" class="owl-carousel">
                <div class="item">
                  <p> <img src="img/claim1.png" alt="" title=""> Claim Intimation<br>
                    <a href="claims.php?tab=cmVpbWJ1cg==">click here</a></p>
                </div>
                <div class="item">
                  <p> <img src="img/claim1.png" alt="" title=""> Claim Status <br>
                    <a href="claims.php">click here</a></p>
                </div>
              </div>
            </div>
          </div>
          <div class="network">
            <div class="network-left"> <a href="downloadCenter.php"> <img src="img/download.png" alt="" title=""><br>
              Download Documents </a> </div>
            <div class="network-right"> <a href="network-hospital-for-uat.php"> <img src="img/network.png" alt="" title=""><br>
              Network Hospitalss </a> </div>
          </div>
        </div>
 
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="model-header" id="exampleModalLabel">Policy Documents</div>
      </div>
      <div class="modal-middle">
        <table class="responsive" width="100%">
                  <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>File  Name</th>
                    <th class="text-right">Download</th>
                  </tr>
                   
                  <tr>
                    <td>1</td>
                    <td>PPT</td>
                    <td>80996MSCRM_2013.pptx</td>
                    <td class="text-right"><a href="download.php?fileName=80996MSCRM_2013.pptx"><span class="downloadIcon"></span></a></td>
                  </tr> 
				    
                  <tr>
                    <td>2</td>
                    <td>PPT</td>
                    <td>53044Test_file_for_group_upload_Tnc.docx</td>
                    <td class="text-right"><a href="download.php?fileName=53044Test_file_for_group_upload_Tnc.docx"><span class="downloadIcon"></span></a></td>
                  </tr> 
				    
                  <tr>
                    <td>3</td>
                    <td>PPT</td>
                    <td>52695Test_file_for_group_upload_Tnc.docx</td>
                    <td class="text-right"><a href="download.php?fileName=52695Test_file_for_group_upload_Tnc.docx"><span class="downloadIcon"></span></a></td>
                  </tr> 
				    
                  <tr>
                    <td>4</td>
                    <td>PPT</td>
                    <td>66220TestKJ.pptx</td>
                    <td class="text-right"><a href="download.php?fileName=66220TestKJ.pptx"><span class="downloadIcon"></span></a></td>
                  </tr> 
				    
                </table>
      </div>
      
    </div>
  </div>
</div>
<div class="footer">
  <div class="container-fluid">
    <div class="copy">
      <div class="col-md-8"> Insurance is the subject matter of solicitation | IRDA Registration No. 148. Copyrights 2013, All right reserved by Religare Health Insurance Company ltd.
        Reg Office - Religare Health Insurance Company Limited, D3, P3B, District Center, Saket, New Delhi -110017 This site is best viewed in Firefox 3 +, IE 7 +, Chrome 10 + and Safari 5 </div>
      <div class="col-md-4">
        <div class="soc"> <a href="#"><i class="fa fa-facebook"></i></a> <a href="#"><i class="fa fa-google-plus"></i></a> 
          <!--<a href="#"><i class="fa fa-fw"></i></a>--> 
          
        </div>
      </div>
    </div>
  </div>
 </div>
 
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) --> 
<!-- Include all compiled plugins (below), or include individual files as needed --> 

<script src="js/owl.carousel.js"></script> 
<script src="js/responsive-tables.js"></script> 

<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();   
});
</script>
<!-- Demo --> 

<script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({

      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true

      // "singleItem:true" is a shortcut for:
      // items : 1, 
      // itemsDesktop : false,
      // itemsDesktopSmall : false,
      // itemsTablet: false,
      // itemsMobile : false

      });
    });
    </script> 
<script src="js/classie.js"></script> 
<script src="js/selectFx.js"></script> 
<script>
			(function() {
				[].slice.call( document.querySelectorAll( 'select.cs-select' ) ).forEach( function(el) {	
					new SelectFx(el);
				} );
			})();
		</script> 
<script src="js/jquery.datetimepicker.js"></script> 
<script>/*
window.onerror = function(errorMsg) {
	$('#console').html($('#console').html()+'<br>'+errorMsg)
}*/

$.datetimepicker.setLocale('en');

$('#datetimepicker_format').datetimepicker({value:'2015/04/15 05:03', format: $("#datetimepicker_format_value").val()});
$("#datetimepicker_format_change").on("click", function(e){
	$("#datetimepicker_format").data('xdsoft_datetimepicker').setOptions({format: $("#datetimepicker_format_value").val()});
});
$("#datetimepicker_format_locale").on("change", function(e){
	$.datetimepicker.setLocale($(e.currentTarget).val());
});

$('#datetimepicker').datetimepicker({
dayOfWeekStart : 1,
lang:'en',
disabledDates:['1986/01/08','1986/01/09','1986/01/10'],
startDate:	'1986/01/05'
});
$('#datetimepicker').datetimepicker({value:'2015/04/15 05:03',step:10});

$('.some_class').datetimepicker();

$('#default_datetimepicker').datetimepicker({
	formatTime:'H:i',
	formatDate:'d.m.Y',
	//defaultDate:'8.12.1986', // it's my birthday
	defaultDate:'+03.01.1970', // it's my birthday
	defaultTime:'10:00',
	timepickerScrollbar:false
});

$('#datetimepicker10').datetimepicker({
	step:5,
	inline:true
});
$('#datetimepicker_mask').datetimepicker({
	mask:'9999/19/39 29:59'
});

$('#datetimepicker3').datetimepicker({
	inline:true
});
</script> 
<!-- popup tab --> 
<script src="js/responsive-tabs.js"></script> 
<script type="text/javascript">
      $( '#myTab a' ).click( function ( e ) {
        e.preventDefault();
        $(this).tab('show');
      } );

      $( '#moreTabs a' ).click( function ( e ) {
        e.preventDefault();
        $(this).tab('show');
      } );

      ( function( $ ) {
          // Test for making sure event are maintained
          $( '.js-alert-test' ).click( function () {
            alert( 'Button Clicked: Event was maintained' );
          } );
          fakewaffle.responsiveTabs( [ 'xs', 'sm' ] );
      } )( jQuery );

/*$(".submit-btn").click(function () {
	$("#myplanForm").hide();
	$("#myPlanList").show();
});*/
    </script> 

 <script src="js/jquery-ui_1.11.4.js"></script>

<!-- popup tab -->


<script type="text/javascript">
var $ = jQuery.noConflict();
$(document).ready(function() {

      
	  
	  
	  var owl = $(".brands");
      owl.owlCarousel({
      items : 3, //10 items above 1000px browser width
      itemsDesktop : [1400,3], //5 items between 1400px and 1025px
      itemsDesktopSmall : [1024,3], // 3 items betweem 1024px and 901px
      itemsTablet: [900,2], //2 items between 900 and 481;
      itemsMobile : [480,1], //1 item between 480 and 0;
	  rewindNav: false,
	  pagination: false,
	  navigation : true // itemsMobile disabled - inherit from itemsTablet option
      });
	  /*-----------------------------------*/
});
</script>  

</body>
</html>
