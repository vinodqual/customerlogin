<?php 
include("inc/inc.hd.php");
require_once 'conf/conf.php';
include_once("conf/common_functions.php");
$pageList = xml2array(@file_get_contents('../resource/download_centre.xml'), '1', '');
//$pageList = xml2array(@file_get_contents('resource/download_centre.xml'), '1', '');
$pageListArray = $pageList['DownloadCentre'];
$fileList = download_page_array($pageListArray);

$pageList2 =  xml2array(@file_get_contents('../resource/claimforms.xml'),'1','');
$pageListArray2 = $pageList2['ClaimForms'];	
$fileList2  = claim_forms_page_array($pageListArray2);

$pageList3 =  xml2array(@file_get_contents('../resource/goodnesswallpapers.xml'),'1','');
$pageListArray3 = $pageList3['GoodnessWallpapers'];	
$fileList3  = goodnesswallpapers_page_array($pageListArray3);

?>
<script src="js/locator.js" charset="utf-8">
</script>
<script>
function removeErrorEmail(){  // function to hide jquery error message from login page if user click on forgot link and again came to login page
var emailDivEmailidError = document.getElementById("emailDivEmailidError");
var emailDivNameError = document.getElementById("emailDivNameError");
var emailDivSuccess = document.getElementById("emailDivSuccess");
emailDivEmailidError.style.display="none";
emailDivNameError.style.display="none";
emailDivSuccess.style.display="none";
}
</script>
<style>
.mouse_not_allowed { cursor:not-allowed;}
</style>
<section id="middleContainer">
  <div class="container-fluid">
    <div class="middlebox">
      <div class="col-md-9">
    
        <div class="middlebox-left">
        
          <ul class="nav nav-tabs responsive" id="myTab">
            <li class="test-class active"><a class="deco-none misc-class" href="#buyPlan"> Brochures & Prospectus </a></li>
            <li class="test-class"><a href="#existingAppointments">Forms</a></li>
            <li><a class="deco-none" href="#myPlans">Policy Terms & Conditions</a></li>
          </ul>
          <div class="tab-content responsive">
            <div class="tab-pane active" id="buyPlan">            
            <div class="tab-paneIn">
              <h1>Download center</h1>
              <?php
                $f=0;
                for ($i = 0; $i < count($fileList); $i++)
                {
                    if(@$fileList[$i]["fileName"] != '')
                    {
                        if($f%3==0 && $f)
                        { ?>
                        <div class="spacer"></div>
                    <?php
                        }
                    ?>
                        <div class="col-md-4 col-sm-4">
                            <div class="downloadCenter" id="downloadCenter<?= $f; ?>">
                                <?php if(file_exists("../siteupload/doc/".$fileList[$i]["fileName"])) { ?>
                                <div class="top">
                                    <a href="download_file.php?fileName=<?php echo $fileList[$i]["fileName"]; ?>"  target="_blank"><?php echo stripslashes(base64_decode($fileList[$i]["downloadTitle"])); ?></a>
                                </div>
                                <div class="bottom">
                                    <a data-toggle="modal" data-target="#exampleModal1" onClick="getValue('<?php echo $fileList[$i]['downloadId'];?>','CENTER');" class="downloadEmailCenter<?=$fileList[$i]['downloadId'];?>" id="<?=$fileList[$i]['downloadId'];?>" href="#"> <i class="fa fa-envelope"></i> Send Email</a>
                                </div>
                                <?php } else { ?>
                                <div class="top">
                                    <a href="#" class="mouse_not_allowed"><?php echo stripslashes(base64_decode($fileList[$i]["downloadTitle"])); ?></a>
                                </div>
                                <div class="bottom">
                                    <a class="downloadEmailCenter<?=$fileList[$i]['downloadId'];?>  mouse_not_allowed" href="#"> <i class="fa fa-envelope"></i> Send Email</a>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
              <?php $f++; } } ?>
              <div class="clearfix"></div>
              </div>
            </div>
				<div class="tab-pane" id="existingAppointments">
					<div class="tab-paneIn">
					  <h1>Forms</h1>
					  <?php
						$g=0;
						for ($j = 0; $j < count($fileList2); $j++)
						{
							if(@$fileList2[$i]["fileName"] != '')
							{
								if($g%3==0 && $g)
								{ ?>
								<div class="spacer"></div>
							<?php
								}
							?>
								<div class="col-md-4 col-sm-4">
									<div class="downloadCenter" id="downloadCenter<?= $g; ?>">
										<?php if(file_exists("../siteupload/doc/".$fileList2[$j]["fileName"])) { ?>
										<div class="top">
											<a href="download_file.php?fileName=<?php echo $fileList2[$j]["fileName"]; ?>"  target="_blank"><?php echo stripslashes(base64_decode($fileList2[$j]["downloadTitle"])); ?></a>
										</div>
										<div class="bottom">
											<a data-toggle="modal" data-target="#exampleModal1" onClick="getValue('<?php echo $fileList2[$j]['claimformsId'];?>','CLAIMFORMS');" class="downloadEmailCenter<?=$fileList2[$j]['claimformsId'];?>" id="<?=$fileList2[$j]['claimformsId'];?>" href="#"> <i class="fa fa-envelope"></i> Send Email</a>
										</div>
										<?php } else { ?>
										<div class="top">
											<a href="#" class="mouse_not_allowed"><?php echo stripslashes(base64_decode($fileList2[$j]["downloadTitle"])); ?></a>
										</div>
										<div class="bottom">
											<a class="downloadEmailCenter<?=$fileList2[$j]['claimformsId'];?> mouse_not_allowed" href="#"> <i class="fa fa-envelope"></i> Send Email</a>
										</div>
										<?php } ?>
									</div>
								</div>
					  <?php $g++; } } ?>
					  <div class="clearfix"></div>
					  </div>
				</div>
            <div class="tab-pane" id="myPlans">
            <div class="tab-paneIn" id="myplanForm">
				<h1>Policy Term & Conditions</h1>
				<?php
				$h=0;
				for ($k = 0; $k < count($fileList3); $k++)
				{
					if(@$fileList3[$k]["fileName"] != '')
					{
						if($h%3==0 && $h)
						{ ?>
						<div class="spacer"></div>
					<?php
						}
					?>
						<div class="col-md-4 col-sm-4">
							<div class="downloadCenter" id="downloadCenter<?= $h; ?>">
								<?php if(file_exists("../siteupload/doc/".$fileList3[$k]["fileName"])) { ?>
								<div class="top">
									<a href="download_file.php?fileName=<?php echo $fileList3[$k]["fileName"]; ?>"  target="_blank"><?php echo stripslashes(base64_decode($fileList3[$k]["downloadTitle"])); ?></a>
								</div>
								<div class="bottom">
									<a data-toggle="modal" data-target="#exampleModal1" onClick="getValue('<?php echo $fileList3[$k]['goodnesswallpapersId'];?>','GOODNESSWALLPAPERS');" class="downloadEmailCenter<?=$fileList3[$k]['goodnesswallpapersId'];?>" id="<?=$fileList3[$k]['goodnesswallpapersId'];?>" href="#"> <i class="fa fa-envelope"></i> Send Email</a>
								</div>
								<?php } else { ?>
								<div class="top">
									<a href="#" class="mouse_not_allowed"><?php echo stripslashes(base64_decode($fileList3[$k]["downloadTitle"])); ?></a>
								</div>
								<div class="bottom">
									<a class="downloadEmailCenter<?=$fileList3[$k]['goodnesswallpapersId'];?> mouse_not_allowed" href="#"> <i class="fa fa-envelope"></i> Send Email</a>
								</div>
								<?php } ?>
							</div>
						</div>
				<?php $h++; } } ?>
				<div class="clearfix"></div>
				</div>
            </div>
          </div>
        </div>

      </div>
      <div class="col-md-3">
         <?php include("inc/inc.right.php"); ?>
      </div>
    </div>
  </div>
</section>

<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
             <button type="button" class="close" data-dismiss="modal" aria-label="Close" onClick="return removeErrorEmail();"><span aria-hidden="true">x</span></button>
            <div class="model-header" id="exampleModalLabel">Send Email</div>
			<div class="" id="emailDivSuccess" style="color: red; font-size: 14px; position:absolute;"></div>
        </div>
        <div class="modal-middle">
            <div class="popupBrowsefull">
          <div class="model-input" style="margin:0;">
            <div class="inputBoxfull">
                <div class="inputBoxIn">
                    <input  name="yourname" id="yourname" class="txtField" type="text" value="<?php echo @$_SESSION['employeeName']?$_SESSION['employeeName']:'Your Name'; ?>" onkeypress='return kp_char(event);' onFocus="if (this.value == 'Your Name') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Your Name';}">
                    </div>
					<div id="emailDivNameError" style="clear:both; margin-bottom:5px;  color: red; font-size: 14px; position:absolute; padding-top: 15px;
    position: absolute;"></div> <!-- display error message-->
            </div>
              <div class="clearfix"></div></div>
                </div>

             <div class="popupBrowsefull" style="margin-top: 12px;">
          <div class="model-input" style="margin:0;">
            <div class="inputBoxfull">
                <div class="inputBoxIn">
                    <input name="emailId" id="emailId" class="txtField"  type="text" value="<?php echo @$_SESSION['employeeEmail']?$_SESSION['employeeEmail']:'Your Email Id'; ?>" onFocus="if (this.value == 'Your Email Id') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Your Email Id';}">
                </div>
				<div id="emailDivEmailidError" style="clear:both; margin-bottom:5px;  color: red; font-size: 14px; position:absolute; margin-top:13px;"></div> <!-- display error message-->
            </div>
         <div class="clearfix"></div> </div>
            </div>
      </div>

        <div class="model-footer" style="float: left;">
            <input type="hidden" value=""  autocomplete="OFF" name="downloademailid" id="downloademailid" />
			 <input type="hidden" value=""  autocomplete="OFF" name="pagename" id="pagename" />
            <input name="submit" type="submit" border="0" alt="submit" class="submit-btn right" value="Submit" onClick="sendDownloadCenterEmail();">
        <div class="clearfix"></div></div>
        <div class="clearfix"></div>
    </div>
  </div>
</div>

<script>
function getValue(docId,pagename)
{
    $('#downloademailid').val(docId);
	$('#pagename').val(pagename);
}
</script>
<!-- Demo -->
<?php include("inc/inc.ft.php"); ?>