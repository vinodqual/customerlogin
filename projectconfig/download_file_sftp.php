<?php

/**
 * 
 * @author Sanjeev Kumar
 * @todo This file created to download file from SFTP.
 * @uses sftp_connect.php. This file created in projectconfig folder.
 * @version 1.0
 * File created on 06-Feb-2017
 * 
 */
//include_once("conf/session.php");				//include session file
require_once(__DIR__."/sftp_connect.php");			//include SFTP Connection File
if(isset($_GET['folder_name'],$_GET['file_name'])){     // These variables defines in C:\xampp\htdocs\Tractus\religare1\.htaccess
    $folder_name=$_GET['folder_name'];
    $file_name=$_GET['file_name'];
	
	require_once(__DIR__."/crypto-functions.php");
    $folder_name	=	trim(decryptUrlData($folder_name));     // Encode File Directory Path
    $file_name		=	trim(decryptUrlData($file_name));     // Encode Name of File
	
    $response_sftp_arr=sftp_connection();               // Connect to SFTP Server
   if(!empty($response_sftp_arr) && ($response_sftp_arr['status']=='SUCCESS')){
        $response_sftp=$response_sftp_arr['response_sftp'];
        $filepath= $folder_name.'/';
		//die("ssh2.sftp://{$response_sftp}{$filepath}{$file_name}");
        if(file_exists("ssh2.sftp://{$response_sftp}{$filepath}{$file_name}")){
            //$data = file_get_contents("ssh2.sftp://{$response_sftp}{$filepath}{$file_name}");
            //file_put_contents($file_name,$data);
            header('Content-type: application/force-download');
            header('Content-Disposition: attachment; filename="'.$file_name.'"');
            header('Content-Length: ' . filesize($file_name));
			 echo file_get_contents("ssh2.sftp://{$response_sftp}{$filepath}{$file_name}");
            //readfile($file_name);
           // unlink($file_name);
        }else{
            echo 'File does not exist on server.';
        }
    }else{
        echo $response_sftp_arr['msg']; 
    }
}
else{
    echo "Unable to Find related File.";   
}
?>
