<?php  
class SetSessionToDB{
	private static $dbCon;
    function __construct($dbCon) //object constructor
    {
        if(!$dbCon){
			dbDown();
        }
        SetSessionToDB::$dbCon=$dbCon;
        
       session_set_save_handler(
           array($this,'open'),
           array($this,'close'),
           array($this,'read'),
           array($this,'write'),
           array($this,'destroy'),
           array($this,'gc')
       );
    }
	private function dbDown(){
		header("location: religarehrcrm/sitedown.php");
		exit;
	}
	public function start($session_name = null)
	{
		session_start($session_name);  //Start it here
	}

    public static function open()
    {
        $dbCon=SetSessionToDB::$dbCon;
		return true;
    }

    public static function read($id)
    {
        $dbCon=SetSessionToDB::$dbCon;
		$query="SELECT DATA	FROM CRMEMPSESSION WHERE id = '$id'"; 
		if($sql = oci_parse($dbCon, $query)){
			if($result = oci_execute($sql)){
				if($row=oci_fetch_row($sql)){
					if(trim($row[0])!=''){
						return $row[0];
					} else{
						return (string)$row[0];
					}
				} else {
					return '';	
				}
			}
		}
        dbDown();
    }

    public static function write($id, $data)
    {
        $dbCon=SetSessionToDB::$dbCon;
		$currentTime = time();
		$query="DELETE FROM CRMEMPSESSION WHERE ID='".$id."'";  
		if($sql = oci_parse($dbCon, $query)){
			if(oci_execute($sql)){
				$insquery = "INSERT INTO CRMEMPSESSION (ID,ACCESSTIME,DATA) values ('".$id."','".$currentTime."','".$data."')";
	   			if($stdid2 = oci_parse($dbCon, $insquery)){
					if(oci_execute($stdid2)){
						return true;
					}
				}
			}
		} 
		dbDown();
    }

    public static function destroy($id)
    {
        $dbCon=SetSessionToDB::$dbCon;
		$query="DELETE FROM CRMEMPSESSION WHERE ID='".$id."'";  
		if($sql = oci_parse($dbCon, $query)){
			oci_execute($sql);
		}
    }

    public static function gc()
    {
        return true;
    }
    public static function close()
    {
        $dbCon=SetSessionToDB::$dbCon;
		return true;
    }
    public function __destruct()
    {
        session_write_close();
    }
}