<?php class SessionHandlerDb{ 
    private static $lifetime = 0; 
	public $conn=null;
    function __construct() //object constructor
    { 
	$this->debug = false;
       session_set_save_handler(
           array($this,'open'),
           array($this,'close'),
           array($this,'read'),
           array($this,'write'),
           array($this,'destroy'),
           array($this,'gc')
       );
    }

   public function start($session_name = null)
   {
       session_start($session_name);  //Start it here
   }

    public static function open()
    {
        global $conn;
			if($_SERVER['HTTP_HOST']=='localhost'){
					if(preg_match('/customerlogin/',$_SERVER['DOCUMENT_ROOT'])==true){
						include_once(dirname($_SERVER['DOCUMENT_ROOT']).'/religare1/projectconfig/conf.php');
					} else {
						include_once($_SERVER['DOCUMENT_ROOT'].'/religare1/projectconfig/conf.php');
					}
			} else {
				
					if(preg_match('/customerlogin/',$_SERVER['DOCUMENT_ROOT'])==true){
						include_once(dirname($_SERVER['DOCUMENT_ROOT']).'/religare1/projectconfig/conf.php');
					} else {
						include_once($_SERVER['DOCUMENT_ROOT'].'/projectconfig/conf.php');
					}
			}
		return true;
    }

    public static function read($id)
    {
	 global $conn;
			$query="SELECT LARGEDATA
		FROM CRMEMPSESSION
		WHERE id = '$id'"; 
			$sql = @oci_parse($conn, $query);
			// Execute Query
			$result = @oci_execute($sql);
		if(isset($result) && !empty($result)){
			$row=oci_fetch_row(@$sql);
			return $row[0]?$row[0]->load():'';
		} else {
			return '';	
		}
        //Get data from DB with id = $id;
    }

    public static function write($id, $data)
    {
	 global $conn;
	$CurrentTime = time();
		$query="SELECT * FROM CRMEMPSESSION WHERE ID='".$id."'";  
		$sql = @oci_parse($conn, $query);
		$result = @oci_execute($sql);
		$row=oci_fetch_assoc(@$sql);
		$access = time();
		
		if(count($row) > 0){
			
			$delquery = "DELETE FROM CRMEMPSESSION WHERE ID='".$id."'";
	   			$stdid = @oci_parse($conn, $delquery);					//query to update records
				$r = @oci_execute($stdid);
				
			 $insquery = "INSERT INTO CRMEMPSESSION (ID,ACCESSTIME,LARGEDATA) values ('".$id."','".$access."','".$data."')";
	   			$stdid2 = @oci_parse($conn, $insquery);					//query to update records
				$r2 = @oci_execute($stdid2);
			
		} else {
			 $insquery = "INSERT INTO CRMEMPSESSION (ID,ACCESSTIME,LARGEDATA) values ('".$id."','".$access."','".$data."')"; 
	   			$stdid2 = @oci_parse($conn, $insquery);					//query to update records
				$r2 = @oci_execute($stdid2) or die(oci_error($sql));
		
		}
	//echo mysql_affected_rows(); 
		return 1; 
    }

    public static function destroy($id)
    {
       global $conn;
	$q="delete from CRMEMPSESSION where id='".$id."'";
		$sql = @oci_parse($conn, $q);
		// Execute Query
		$result = @oci_execute($sql) ;
	//return mysql_query($sdbc, $q);
    }

    public static function gc()
    {
        return true;
    }
    public static function close()
    {
        global $conn;
	
		return true;
    }
    public function __destruct()
    {
        session_write_close();
    }
}?>