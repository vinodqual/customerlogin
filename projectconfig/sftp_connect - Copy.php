<?php
/**
 * 
 * @author Sanjeev Kumar
 * @todo This file created for define functions to upload documents using SFTP
 * 
 */
error_reporting(E_ERROR);
ini_set('display_errors', 1);
include_once(__DIR__.'/conf.php');
define('UPLOAD_FOLDER','/Tractus_PROD/');     // File Upload Folder Name
define('REPORT_DOC','report_doc/');     // Folder name for report file upload
define('POLICY_PDF','corporate_policy/##POLICY_NO##/');     // Folder name in which policy pdf upload
define('POLICY_DOC','corporate_policy/##POLICY_NO##/policy_doc/');     // Folder name in which policy document upload
define('POLICY_OTHER_DOC','corporate_policy/##POLICY_NO##/policy_other_doc/');     // Folder name in which policy document upload
define('PLAN_DOC','plan_doc/');      // Folder name in which plan document upload
define('ENDORSEMENT_DOC','endorsement_doc/');      // Folder name in which endorsement document upload
define('PRODUCT_DOC','product_doc/');      // Folder name in which product document upload
define('REIMBURSEMENT_DOC','reimbursement_doc/##POLICY_NO##/');      // Folder name in which product document upload
define('CORPORATE_DOC','corporate_doc/##COMPANY_ID##/');      // Folder name in which corporate document upload
define('HEALTHTIME_DOC','healthtime_doc/##MONTH_YEAR##/');      // Folder name in which Healthtime document upload
//define('DOWNLOAD_FILE_URL_SFTP','https://localhost/Tractus/religare1/');	// Define common download path
//define('DOWNLOAD_FILE_URL_SFTP_CRMADMIN','https://localhost/Tractus/religare1/religarehrcrm/crmadmin2/##FOLDER_NAME##/##FILE_NAME##.sfdl');

/*----------------------------- Define Portal Name for create url ----------*/
define('CRMADMIN','CRMADMIN');
define('TRACTUSADMIN','TRACTUSADMIN');
define('CUSTOMERLOGIN','CUSTOMERLOGIN');
define('CS','CS');
define('HR','HR');
define('AGENT','AGENT');
define('LIVEWELL','LIVEWELL');
define('SCOPEPORTAL','SCOPEPORTAL');

/*------------------- Common Message --------------------------*/
define('SFTP0025','SFTP does not configured on server. Please try again later');		// This message display when SSH2 Extension is not installed on Server.
define('SFTP0026','Invalid Link');		// This message display when pass empty file name or empty folder name or invalid portal name

/*------------------------- Messages for Report document uploading ------------------------*/
define('SFTP0000',"Found incomplete details during report upload");            // This message will display when found wrong appointmentid
define('SFTP0001',"Could not connect to SFTP server.");            // This message will display when unable to connect with SFTP Server
define('SFTP0002',"Could not authenticate SFTP server. Incorrect Username or Password.");            // This message will display when username or password incorrect
define('SFTP0003',"Could not initialize SFTP Subsystem.");            // This message will display when unable to initialize SFTP Subsystem
define('SFTP0004',"You are trying to upload invalid file format. Please upload valid file format.");            // This message will display when user upload invalid file format
define('SFTP0005',"Unable to create directory on SFTP.");            // This message will display when unable to create folder on SFTP server
define('SFTP0006',"Could not open remote file: ");            // This message will display when unable to open file on SFTP server for write
define('SFTP0007',"Could not open local file: ");            // This message will display when unable to open local file on SFTP server for write
define('SFTP0008',"Could not send data from file: ");            // This message will display when unable to write file on SFTP Server
define('SFTP0009',"Appointment creation date is not found");            // This message will display when created on date in appointment table is empty
define('SFTP0010',"Please select atleast one report file for upload.");       // This message will display when user does not select blood report file for upload.
define('SFTP0011',"Unable to update details in database. Please try again later.");     //This will message display when update query in appointment table fails for update report files name in table.
define('SFTP0012',"#FILENAME# uploaded successfully.");     // This message will display when all report files successfully uploaded.

/*------------------------- Messages for Policy document uploading ------------------------*/
define('SFTP0013','Policy number or Policyid does not found while document uploading');     // This message display when upload policy document and policy number or policyid does not found.
define('SFTP0014','Please select a file for upload.');     // This message display when user submit empty policy pdf  upload input field
define('SFTP0015','Please provide mandatory (*) details.');     // This message display when user submit empty policy other document  upload input field
define('SFTP0016','Please select user type.');     // This message display when user does not select user type in policy document uploading
define('SFTP0017','Policy number does not exist in database');     // This message display when policy number does not find in CRMPOLICY Table


/*----------------------- Message for Plan document uploading ----------------------------*/
define('SFTP0018','Plan type does not match when upload plan document');     // This message display when plan type is not exist in array (HCK,RETAIL, CORPORATE)

/*----------------------- Message for product document uploading ----------------------------*/
define('SFTP0019','Please make it applicable for atleast one Portal.');     // This message display when user does not select any portal for add product.
define('SFTP0020','You select incorrect applicable for portal.');     // This message display when pass incorrect portal other than RETAIL OR CORPORATE


/*----------------------- Message for reimbursement document uploading ----------------------------*/
define('SFTP0021','Policy number does not found while uploading reimbursement document.');     // This message display when policy number does not found whe user upload document
define('SFTP0022','Required details for upload document is invailid.');     // This message display no file input field submit for upload.

/*----------------------- Message for Corporate Document uploading ----------------------------*/
define('SFTP0023','Company details does not found while document uploading.');     // This message display when companyId does not found in url when user upload document in manage company


/*----------------------- Message for Healthtimes Document uploading ----------------------------*/
define('SFTP0024','Please select date for upload document');     // This message display when user does not select date for upload document

/**
 * @author Sanjeev Kumar
 * @todo Declare global variable
 */
$conn_id='';		// Variable defines for SFTP Connection id.
$response_sftp='';	// Variable defines for SFTP Subsystem response resource id
$response_sftp_arr=array();	// Array declares for SFTP Subsystem resource id and status.


/**
 * 
 * @author Sanjeev Kumar
 * @todo This code written for establish a connection to SFTP Server.
 * @return string
 * This function written on 24-Jan-2017
 */
function sftp_connection() {
    global $conn_id;
    global $response_sftp;
    global $response_sftp_arr;
	
    // Credentials for SFTP Server
    $sftp_server    =   "sftp.religarehealthinsurance.com";
    $sftp_port      =   '8071';
    $sftp_user_name =   "rhiclportal";
    $sftp_user_pass =   "rh!cl@123";   
    
    // For Religare My system only
    //$sftp_user_name =   "sanjeev.kr";
    //$sftp_user_pass =   "mail@123";   
    
    
    
   // Connect to SFTP Server using SSH
    if (!function_exists('ssh2_connect')) {		// Check if ssh connection function is exist.
		return array('status'=>'ERROR','msg'=>'SFTP does not configured on server. Please try again later','code'=>'SFTP0025');
	} 	
    $conn_id = ssh2_connect($sftp_server, $sftp_port);       //Connect to SFTP Server
    if(!$conn_id){
        return array('status'=>'ERROR','msg'=>SFTP0001,'code'=>'SFTP0001');
    }
	
	// Authenticate SFTP Server using SSH
	if (!function_exists('ssh2_auth_password')) {		// Check if ssh authentication function is exist.
		return array('status'=>'ERROR','msg'=>'SFTP does not configured on server. Please try again later','code'=>'SFTP0025');
	}
    if(!ssh2_auth_password($conn_id, $sftp_user_name, $sftp_user_pass)){        // Authenticate SFTP Server with user name and password
        return array('status'=>'ERROR','msg'=>SFTP0002,'code'=>'SFTP0002');
    }
	
	// Initialize SFTP Subsystem
	if (!function_exists('ssh2_sftp')) {		// Check if ssh function of initialize SFTP Subsystem is exist.
		return array('status'=>'ERROR','msg'=>'SFTP does not configured on server. Please try again later','code'=>'SFTP0025');
	}
    $response_sftp = ssh2_sftp($conn_id);                   // Initialize SFTP Subsystem.    
    if($response_sftp){
        $response_sftp_arr=array('status'=>'SUCCESS','response_sftp'=>$response_sftp);
        return $response_sftp_arr;
    } else{
        return array('status'=>'ERROR','msg'=>SFTP0003,'code'=>'SFTP0003');
    }    
}

/**
 * 
 * @author Sanjeev Kumar
 * @todo This function is used to remove space between words in file name
 * @param type $string
 * @return type
 * This function written on 24-Jan-2017
 * 
 */
function remove_space($string) {
    $string = ereg_replace("[ \t\n\r]+", " ", $string);
    $text = str_replace(" ", "_", $string);
    return $text;
}


/**
 * 
 * @author Sanjeev Kumar
 * @todo This function used to encode data.
 * @param type $data
 * @return type
 * This function created on 30-March-2017
 * 
 */
function encode_data($data){
    return str_replace(array('+','/'),array('_','-'),rtrim(base64_encode($data),'='));
}

/**
 * 
 * @author Sanjeev Kumar
 * @todo This function used to decode data.
 * @param type $data
 * @return type
 * This function created on 30-March-2017
 * 
 */
function decode_data($data){
    return base64_decode(str_replace(array('_','-'),array('+','/'),$data));
}





/**
 * 
 * @author Sanjeev Kumar
 * @todo This function is used to create url for download file.
 * @param type $folder_name
 * @param type $file_name
 * @param type $portal_url
 * @return type
 * This function written on 30-March-2017
 * 
 */
function create_url($folder_name='',$file_name='',$portal_name=''){    
    if(empty($folder_name) || empty($file_name) || empty($portal_name)){
        return FALSE;
    }
    $folder_name=encode_data($folder_name);     // Encode File Directory Path
    $file_name=encode_data($file_name);     // Encode Name of File
    $base_url=base_url($portal_name);
    if($base_url!==FALSE){
       $url=$base_url.$folder_name.'/'.$file_name.'.sfdl';
        return $url; 
    }else{
        return FALSE;
    }
}



/**
 * 
 * @author Sanjeev Kumar
 * @todo This function written for create base url.
 * @param type $portal_name
 * @return boolean
 * This function written on 30-March-2017
 * 
 */
function base_url($portal_name){
    if(empty($portal_name)){
        return FALSE;
    }
    $portal_name    =   strtoupper($portal_name);
    
    /*----------------------------- Devlopment Server ---------------------------*/
    /*$portal_array   =   array(
                            'CRMADMIN'      =>      'https://localhost/Tractus/religare1/religarehrcrm/crmadmin2/',
                            'TRACTUSADMIN'  =>      'https://localhost/Tractus/tractusadmin/',
                            'HR'            =>      'https://localhost/Tractus/religare1/religarehrcrm/hr/',
                            'CS'            =>      'https://localhost/Tractus/religare1/cs/',
                            'LIVEWELL'      =>      'https://localhost/Tractus/religare1/livewell/admin/',
                            'AGENT'         =>      'https://localhost/Tractus/religare1/religarehrcrm/agent/',
                            'CUSTOMERLOGIN' =>      'https://localhost/Tractus/customerlogin/',
                            'SCOPEPORTAL'   =>      'https://localhost/Tractus/religare/scopeportal/'
                        ); */
    
    /*------------------------------- QC Server ---------------------------*/
    /*$portal_array   =   array(
                            'CRMADMIN'      =>      'https://qc-tractus.religarehealthinsurance.com/religarehrcrm/crmadmin2/',
                            'TRACTUSADMIN'  =>      'https://qc-tractusadmin.religarehealthinsurance.com/',
                            'HR'            =>      'https://qc-tractus.religarehealthinsurance.com/religarehrcrm/hr/',
                            'CS'            =>      'https://qc-tractus.religarehealthinsurance.com/cs/',
                            'LIVEWELL'      =>      'https://qc-tractus.religarehealthinsurance.com/livewell/admin/',
                            'AGENT'         =>      'https://qc-tractus.religarehealthinsurance.com/religarehrcrm/agent/',
                            'CUSTOMERLOGIN' =>      'https://qc-my.religarehealthinsurance.com/',
                            'SCOPEPORTAL'   =>      'qc.religarehealthinsurance.com/scopeportal/'
                        ); */
    
    /*------------------------------- UAT Server ---------------------------*/
   /* $portal_array   =   array(
                            'CRMADMIN'      =>      'https://uat-tractus.religarehealthinsurance.com/religarehrcrm/crmadmin2/',
                            'TRACTUSADMIN'  =>      'https://uat-tractusadmin.religarehealthinsurance.com/',
                            'HR'            =>      'https://uat-tractus.religarehealthinsurance.com/religarehrcrm/hr/',
                            'CS'            =>      'https://uat-tractus.religarehealthinsurance.com/cs/',
                            'LIVEWELL'      =>      'https://uat-tractus.religarehealthinsurance.com/livewell/admin/',
                            'AGENT'         =>      'https://uat-tractus.religarehealthinsurance.com/religarehrcrm/agent/',
                            'CUSTOMERLOGIN' =>      'https://uat-my.religarehealthinsurance.com/',
                            'SCOPEPORTAL'   =>      ''
                        ); */
    
    /*------------------------- Production Server --------------------------*/
    $portal_array   =   array(
                            'CRMADMIN'      =>      'https://tractus.religarehealthinsurance.com/religarehrcrm/crmadmin2/',
                            'TRACTUSADMIN'  =>      'https://tractusadmin.religarehealthinsurance.com/',
                            'HR'            =>      'https://tractus.religarehealthinsurance.com/religarehrcrm/hr/',
                            'CS'            =>      'https://tractus.religarehealthinsurance.com/cs/',
                            'LIVEWELL'      =>      'https://tractus.religarehealthinsurance.com/livewell/admin/',
                            'AGENT'         =>      'https://tractus.religarehealthinsurance.com/religarehrcrm/agent/',
                            'CUSTOMERLOGIN' =>      'https://my.religarehealthinsurance.com/',
                            'SCOPEPORTAL'   =>      'religarehealthinsurance.com/scopeportal/'
                        ); 
    
    if(array_key_exists($portal_name,$portal_array)){
        return $portal_array[$portal_name];
    }else{
        return FALSE;
    }

}




/**
 * 
 * @author Sanjeev Kumar
 * @todo This function write to upload file using SFTP.
 * @param type $upload
 * @param type $folder_name
 * @param type $rename_file
 * @return string
 * This function written on 24-Jan-2017
 * 
 */
function sftp_file_upload($upload='',$folder_name='',$rename_file='',$response_sftp_arr, $multiple_upload_index='', $extQTPDocs=''){
    if(empty($upload)){
        return array('status'=>'ERROR','msg'=>'upload field can not be empty.','code'=>'');
    }
    if(empty($folder_name)){
        return array('status'=>'ERROR','msg'=>'Directory can not be empty.' ,'code'=>'');
    }
    if(empty($rename_file)){
        return array('status'=>'ERROR','msg'=>'File name can not be empty' ,'code'=>'');
    }
    if(is_array($_FILES[$upload]['name']) && ($multiple_upload_index!='')){       // This condition check if multiple files upload in array
        $extension=strtolower(pathinfo($_FILES[$upload]['name'][$multiple_upload_index],PATHINFO_EXTENSION));
    }elseif(!is_array($_FILES[$upload]['name'])){
        $extension=strtolower(pathinfo($_FILES[$upload]['name'],PATHINFO_EXTENSION));
    }else{
        return array('status'=>'ERROR','msg'=>'Incorrect file parameter submission for file upload.','code'=>'SFTP0025');
    }
    $filepath=UPLOAD_FOLDER.$folder_name;                // Path where file will be upload.
	if(empty($extQTPDocs)){
        $extension_array=array('pdf','doc','docx','ppt','pptx','xls','xlsx','rtf','txt','jpg','png','jpeg','zip','7z','rar','zipx','tar');     // Allowed file format which can be upload.
    } else {
		$filepath = '/'.$folder_name;                // Path where file will be upload for QTP.
		$extension_array=array('pdf','doc','docx','xls','xlsx','msg','zip','rar','7z');     // Allowed file format which can be upload in QTP.
	}
	
    if(!in_array($extension,$extension_array)){          // Validate file of valid format.
        return array('status'=>'ERROR','msg'=>SFTP0004,'code'=>'SFTP0004');
    }
	if(empty($extQTPDocs)){
		$file_name=remove_space($rename_file);                // Remove space and tabs from file name.
	} else {
		$file_name=$rename_file;                // Retain space and tabs in file name for QTP.
	}
    //$file_name=remove_space($_FILES[$upload]['name']);
//    $explode=explode(pathinfo($_FILES[$upload]['name'],PATHINFO_FILENAME),'_');
//    for($i=2;$i<count($explode)-2;$i++){                    
//        $explode1[$i]=$explode[$i];
//    }
//    $file_name_error=implode('_',$explode1);
    $remote_file=$filepath.'/'.$file_name;                    // Remote file name in which uploaded file will be write
	
    if(is_array($_FILES[$upload]['tmp_name']) && ($multiple_upload_index!='')){
        $file_tmp_name=$_FILES[$upload]['tmp_name'][$multiple_upload_index];
    }elseif(!is_array($_FILES[$upload]['name'])){
        $file_tmp_name=$_FILES[$upload]['tmp_name'];
    }else{
        return array('status'=>'ERROR','msg'=>'Incorrect file parameter submission for file upload.','code'=>'SFTP0025');
    }
    $response_sftp=$response_sftp_arr['response_sftp'];   // Response get from SFTP response array
    $folder_exists = file_exists('ssh2.sftp://' .$response_sftp.$filepath);     // Check if directory exists on SFTP Server
    if(!$folder_exists){                                  // If director not exists then created on SFTP Server
        $dir_response=ssh2_sftp_mkdir($response_sftp,rtrim($filepath, '/'),0777,TRUE);      // ssh2_sftp_mkdir(SFTPRESPONSE, FILEPATH, FOLDERMODE, RECURSIVE_DIRECTOR)
        if(!$dir_response){
            return array('status'=>'ERROR','msg'=>SFTP0005,'code'=>'SFTP0005');
        }
    } 
    $stream = fopen("ssh2.sftp://{$response_sftp}{$remote_file}", 'w');     // Open Remote file in write mode.
    if (!$stream){
        return array('status'=>'ERROR','msg'=> SFTP0006.$file_name,'code'=>'SFTP0006');
    }
	if($file_tmp_name != '') {		
    $file_contents = file_get_contents($file_tmp_name);   // Get contents of file uploaded currently.
	} else {
		$file_contents = file_get_contents($_FILES[$upload]['path'], FILE_USE_INCLUDE_PATH);
	}
    if ($file_contents === false){
        fclose($stream);
        return array('status'=>'ERROR','msg'=> SFTP0007.$file_name,'code'=>'SFTP0007');
    }
    if (fwrite($stream, $file_contents) === false){       // If file writting fails then return an error.
        fclose($stream);
        return array('status'=>'ERROR','msg'=>SFTP0008.$file_name,'code'=>'SFTP0008');
    } else{                                               // If file successfully writes then return file name and status to be SUCCESS.
        fclose($stream);
        $response_arr=array('status'=>'SUCCESS','file_name'=>$file_name);
        return $response_arr;
    }   
}


/**
 * 
 * @author Sanjeev Kumar
 * @todo This function write to upload report file.
 * @global type $conn
 * @param type $appointment_array
 * @param type $upload_array
 * @return string
 * Date: 02-Feb-2017
 * In upload_report() function first argument pass the appointment details, second argument pass upload field array and 
 * third argument pass user type that is RETAIL or CORPORATE.
 * 
 */
function upload_report($appointment_array=array(), $upload_array=array(), $user_type=''){
    global $conn; 
    global $conn_id;            //SFTP Connection
    $return_array=array();                                      // Initialize empty array for returning message and status
    $entryTime = date('d-M-Y h:i:s A');
    if(!empty($user_type) && ($user_type=='RETAIL')){
        $app_table='CRMRETAILPOSTAPPOINTMENT';
        $app_history_table='CRMRETAILAPPHISTORY';
        $app_type='R';
    } elseif(!empty($user_type) && ($user_type=='CORPORATE')) {
        $app_table='CRMCOMPANYPOSTAPPOINTMENT';
        $app_history_table='CRMCORPORATEAPPHISTORY'; 
        $app_type='C';
    } else{  
        $return_array[09]['msg']=SFTP0000;
        $return_array[09]['code']='SFTP0000';          
        $return_array[09]['status']='ERROR';
    }
    if(empty($upload_array['bloodupload']['name']) && empty($upload_array['urineupload']['name']) && empty($upload_array['upload']['name'])){          // Check if blood report field is empty
        $return_array[01]['msg']=SFTP0010;
        $return_array[01]['code']='SFTP0010';          
        $return_array[01]['status']='ERROR';          
    }    
    if(empty($return_array)){
        $created_on=$appointment_array['CREATEDON'];
        $appointment_id=$appointment_array['APPOINTMENTID'];
        $created_on=strtotime($created_on);
        $month_year=strtoupper(date('M',$created_on).'_'.date('Y',$created_on));
        $response_sftp_arr=sftp_connection();                 // Connect to SFTP Server.
        //$response_sftp_arr=array('status'=>'SUCCESS','response_sftp'=>'5'); // For Testing
        if(!empty($response_sftp_arr) && ($response_sftp_arr['status']=='ERROR')){      // Check if successfully connect to SFTP Server.
            $return_array[02]['msg']=$response_sftp_arr['msg'];
            $return_array[02]['code']=$response_sftp_arr['code'];
            $return_array[02]['status']=$response_sftp_arr['status'];
        }
        if(empty($return_array) && !empty($response_sftp_arr) && ($response_sftp_arr['status']=='SUCCESS')){       // If successfully connect to SFTP Server
            $report_upload_fields='';
            $success_files_names='';
            /*---------------- For upload blood report --------------------*/
            if(!empty($upload_array['bloodupload']['name'])){
                $ext = pathinfo($upload_array['bloodupload']['name'], PATHINFO_EXTENSION);
                $name_blood = pathinfo($upload_array['bloodupload']['name'], PATHINFO_FILENAME).'_BLOOD_REPORT';
                $file_name_blood=$app_type."_".$appointment_id."_".strtolower($name_blood).".".$ext;
                $folder_name=REPORT_DOC.$month_year.'/';
                $response_blood_report=sftp_file_upload('bloodupload',$folder_name,$file_name_blood,$response_sftp_arr);
                //$response_blood_report=array('status'=>'ERROR','code'=>'SFTP0008','msg'=>SFTP0008.$file_name_blood);        // For Testing       
                if(isset($response_blood_report['status']) && $response_blood_report['status']=='SUCCESS'){     //Check if no error occurs in blood report file uploading
                    $report_upload_fields.="BLOODREPORTFILE='" .$response_blood_report['file_name'] . "',";
                    $success_files_names.='Blood Report';
                    
                } else {      
                    $return_array[03]['msg']=$response_blood_report['msg'];
                    $return_array[03]['code']=$response_blood_report['code'];
                    $return_array[03]['status']=$response_blood_report['status'];
                }
            }           

            /*---------------- For upload urine report --------------------*/
            if(!empty($upload_array['urineupload']['name'])){
                $ext = pathinfo($upload_array['urineupload']['name'], PATHINFO_EXTENSION);
                $name_urine = pathinfo($upload_array['urineupload']['name'], PATHINFO_FILENAME).'_URINE_REPORT';
                $file_name_urine=$app_type."_".$appointment_id."_".strtolower($name_urine).".".$ext;
                $folder_name=REPORT_DOC.$month_year.'/';
                $response_urine_report=sftp_file_upload('urineupload',$folder_name,$file_name_urine,$response_sftp_arr);
                //$response_urine_report=array('status'=>'ERROR','code'=>'SFTP0008','msg'=>SFTP0008.$file_name_urine);       // For Testing
                if(isset($response_urine_report['status']) && $response_urine_report['status']=='SUCCESS'){       //Check if no error occurs in urine report file uploading
                    $report_upload_fields.="URINEREPORTFILE='" .$response_urine_report['file_name'] . "',";
                    if(empty($success_files_names))
                        $success_files_names.='Urine Report';
                    else
                        $success_files_names.=', Urine Report';
                } else {
                    $return_array[04]['msg']=$response_urine_report['msg'];
                    $return_array[04]['code']=$response_urine_report['code'];
                    $return_array[04]['status']=$response_urine_report['status'];
                }
            }
            
            /*--------------- For upload other report ---------------------*/
            if(!empty($upload_array['upload']['name'])){
                $ext = pathinfo($upload_array['upload']['name'], PATHINFO_EXTENSION);
                $name_other = pathinfo($upload_array['upload']['name'], PATHINFO_FILENAME).'_OTHER_REPORT';
                $file_name_other=$app_type."_".$appointment_id."_".strtolower($name_other).".".$ext;
                $folder_name=REPORT_DOC.$month_year.'/';
                $response_other_report=sftp_file_upload('upload',$folder_name,$file_name_other,$response_sftp_arr);       
                //$response_other_report=array('status'=>'SUCCESS','file_name'=>$file_name_other);        // For Testing 
                if(isset($response_other_report['status']) && $response_other_report['status']=='SUCCESS'){       //Check if no error occurs in other report file uploading
                    $report_upload_fields.="REPORTFILE='" .$response_other_report['file_name'] . "',";
                    if(empty($success_files_names))
                        $success_files_names.='Other Report';
                    else
                        $success_files_names.=', Other Report';
                } else {
                    $return_array[05]['msg']=$response_other_report['msg'];
                    $return_array[05]['code']=$response_other_report['code'];
                    $return_array[05]['status']=$response_other_report['status'];
                }
            }
            if(!empty($report_upload_fields)){        // If report successfully uploaded on SFTP
                $sql = "UPDATE  {$app_table} SET $report_upload_fields REPORTSTATUS='UPLOADED', UPDATEDON = TO_DATE('".$entryTime."','DD-MON-YYYY HH:MI:SS AM') WHERE APPOINTMENTID='" .$appointment_id. "'";     // Query for update report file names in database with respect to appointmentid.
                $stdid = oci_parse($conn, $sql);
                if(oci_execute($stdid,OCI_NO_AUTO_COMMIT)){
                    $sql2 = "INSERT INTO {$app_history_table}(ID,APPOINTMENTID,STATUS,REPORTSTATUS,CREATEDON,CREATEDBY) VALUES({$app_history_table}_SEQ.nextval,'".$appointment_id."','DONE','UPLOADED',TO_DATE('".$entryTime."','DD-MON-YYYY HH:MI:SS AM'),'" . $_SESSION['userName'] . "') ";  //     Query for update status with respect to appointmentid.
                    $stdid2 = oci_parse($conn, $sql2);
                    if(oci_execute($stdid2)){
                        oci_commit($conn);
                        $return_array[07]['msg']=str_replace('#FILENAME#',$success_files_names,SFTP0012);
                        $return_array[07]['code']='SFTP0012';    
                        $return_array[07]['status']='SUCCESS';                           
                    } else {
                        oci_rollback($conn);
                        $return_array[08]['msg']=SFTP0011;
                        $return_array[08]['code']='SFTP0011'; 
                        $return_array[08]['status']='ERROR'; 
                    }                        
                } else {
                    $return_array[06]['msg']=SFTP0011;
                    $return_array[06]['code']='SFTP0011';
                    $return_array[06]['status']='ERROR';
                }
            }               // End of Report uploaded successfully
        }                   // End of connection establishment to SFTP
    }                       // End of File input field check
    unset($conn_id);
    return $return_array;
}


/**
 * 
 * @author Sanjeev Kumar
 * @todo This file used to upload policy pdf
 * @global type $conn
 * @param type $upload
 * @param type $policy_number
 * @return string
 * Code written on 06-Feb-2017
 * In upload_policy_pdf() function receive name of upload input field and policy number.
 * 
 */
function upload_policy_pdf($upload='',$policy_number=''){
    global $conn_id;            //SFTP Connection
    global $conn;    
    $return_array=array();                                      // Initialize empty array for returning message and status
    if(empty($_FILES[$upload]['name'])){          // Check if blood report field is empty
        $return_array[01]['msg']=SFTP0014;
        $return_array[01]['code']='SFTP0014';          
        $return_array[01]['status']='ERROR';          
    } 
    if(empty($policy_number)){
        $return_array[02]['msg']=SFTP0013;
        $return_array[02]['code']='SFTP0013';          
        $return_array[02]['status']='ERROR';
    }
    if(empty($return_array)){        
        $response_sftp_arr=sftp_connection();                 // Connect to SFTP Server.
        //$response_sftp_arr=array('status'=>'SUCCESS','response_sftp'=>'5'); // For Testing
        if(!empty($response_sftp_arr) && ($response_sftp_arr['status']=='ERROR')){      // Check if successfully connect to SFTP Server.
            $return_array[03]['msg']=$response_sftp_arr['msg'];
            $return_array[03]['code']=$response_sftp_arr['code'];
            $return_array[03]['status']=$response_sftp_arr['status'];
        }
        if(empty($return_array) && !empty($response_sftp_arr) && ($response_sftp_arr['status']=='SUCCESS')){       // If successfully connect to SFTP Server
            /*--------------- For upload policy document ---------------------*/
            if(!empty($_FILES[$upload]['name'])){
                $ext = pathinfo($_FILES[$upload]['name'], PATHINFO_EXTENSION);
                $name_policy_doc = pathinfo($_FILES[$upload]['name'], PATHINFO_FILENAME);
                $file_name='PolicyDoc_'.$policy_number.".".$ext;
                $folder_name=str_replace('##POLICY_NO##',$policy_number,POLICY_PDF);
                $response=sftp_file_upload($upload, $folder_name, $file_name,$response_sftp_arr);       
                //$response=array('status'=>'SUCCESS','file_name'=>$file_name);        // For Testing 
                if(isset($response['status']) && $response['status']=='SUCCESS'){       //Check if no error occurs in other report file uploading
                    $return_array=$response;
                } else {
                    $return_array[05]['msg']=$response['msg'];
                    $return_array[05]['code']=$response['code'];
                    $return_array[05]['status']=$response['status'];
                }
            }
        }                   // End of connection establishment to SFTP
    }                       // End of policy document input field check
    unset($conn_id);
    return $return_array;
}



/**
 * 
 * @author Sanjeev Kumar
 * @todo This function write for upload policy other document
 * @global type $conn_id
 * @global type $conn
 * @param type $upload
 * @param type $policy_number
 * @return string
 * Code written on 08-Feb-2017
 * In upload_policy_other_doc() function receive name of upload input field and policy number.
 *  
 */
function upload_policy_other_doc($upload='',$policy_number=''){
    global $conn_id;            //SFTP Connection
    global $conn;    
    $return_array=array();                                      // Initialize empty array for returning message and status
    if(empty($_FILES[$upload]['name'])){          // Check if blood report field is empty
        $return_array[01]['msg']=SFTP0014;
        $return_array[01]['code']='SFTP0014';          
        $return_array[01]['status']='ERROR';          
    } 
    if(empty($policy_number)){
        $return_array[02]['msg']=SFTP0013;
        $return_array[02]['code']='SFTP0013';          
        $return_array[02]['status']='ERROR';
    }
    if(empty($return_array)){        
        $response_sftp_arr=sftp_connection();                 // Connect to SFTP Server.
        //$response_sftp_arr=array('status'=>'SUCCESS','response_sftp'=>'5'); // For Testing
        if(!empty($response_sftp_arr) && ($response_sftp_arr['status']=='ERROR')){      // Check if successfully connect to SFTP Server.
            $return_array[03]['msg']=$response_sftp_arr['msg'];
            $return_array[03]['code']=$response_sftp_arr['code'];
            $return_array[03]['status']=$response_sftp_arr['status'];
        }
        if(empty($return_array) && !empty($response_sftp_arr) && ($response_sftp_arr['status']=='SUCCESS')){       // If successfully connect to SFTP Server
            /*--------------- For upload policy other document ---------------------*/
            if(!empty($_FILES[$upload]['name'])){
                $ext = pathinfo($_FILES[$upload]['name'], PATHINFO_EXTENSION);
                $name_policy_doc = pathinfo($_FILES[$upload]['name'], PATHINFO_FILENAME);
                $file_name=time().'_'.strtolower($name_policy_doc).".".$ext;
                $folder_name=str_replace('##POLICY_NO##',$policy_number,POLICY_OTHER_DOC);
                $response=sftp_file_upload($upload, $folder_name, $file_name,$response_sftp_arr);       
                //$response=array('status'=>'SUCCESS','file_name'=>$file_name);        // For Testing 
                if(isset($response['status']) && $response['status']=='SUCCESS'){       //Check if no error occurs in other report file uploading
                    $return_array=$response;
                } else {
                    $return_array[05]['msg']=$response['msg'];
                    $return_array[05]['code']=$response['code'];
                    $return_array[05]['status']=$response['status'];
                }
            }
        }                   // End of connection establishment to SFTP
    }                       // End of policy document input field check
    unset($conn_id);
    return $return_array;
}


/**
 * 
 * @author Sanjeev Kumar
 * @todo This function written for upload policy documents
 * @global type $conn_id
 * @global type $conn
 * @param type $upload
 * @param type $policy_number
 * @return type
 * code written on 08-Feb-2017
 * In upload_policy_doc() function receive name of upload input field and policy number.
 * 
 */
function upload_policy_doc($upload='', $policy_number=''){
    global $conn_id;            //SFTP Connection
    global $conn;    
    $return_array=array();                                      // Initialize empty array for returning message and status
    $total_file_uploaded = count($_FILES[$upload]['name']);    
    for($r=0;$r<$total_file_uploaded;$r++){
        if(empty($_FILES[$upload]['name'][$r])){    // Check if file upload input field is empty
            $return_array[01]['msg']=SFTP0014;
            $return_array[01]['code']='SFTP0014';          
            $return_array[01]['status']='ERROR';
            break;
        }
    }
    if(empty($policy_number)){
        $return_array[02]['msg']=SFTP0013;
        $return_array[02]['code']='SFTP0013';          
        $return_array[02]['status']='ERROR';
    }
    if(empty($return_array)){        
        $response_sftp_arr=sftp_connection();                 // Connect to SFTP Server.
        //$response_sftp_arr=array('status'=>'SUCCESS','response_sftp'=>'5'); // For Testing
        if(!empty($response_sftp_arr) && ($response_sftp_arr['status']=='ERROR')){      // Check if successfully connect to SFTP Server.
            $return_array[03]['msg']=$response_sftp_arr['msg'];
            $return_array[03]['code']=$response_sftp_arr['code'];
            $return_array[03]['status']=$response_sftp_arr['status'];
        }
        if(empty($return_array) && !empty($response_sftp_arr) && ($response_sftp_arr['status']=='SUCCESS')){       // If successfully connect to SFTP Server
            /*--------------- For upload policy other document ---------------------*/
            $file_name_arr=array();
            for($r=0;$r<$total_file_uploaded;$r++){
                if(!empty($_FILES[$upload]['name'][$r])){
                    $ext = pathinfo($_FILES[$upload]['name'][$r], PATHINFO_EXTENSION);
                    $name_policy_doc = pathinfo($_FILES[$upload]['name'][$r], PATHINFO_FILENAME);
                    $file_name=time().$r.'_'.strtolower($name_policy_doc).".".$ext;
                    $folder_name=str_replace('##POLICY_NO##',$policy_number,POLICY_DOC);
                    $multiple_file_index="$r";
                    $response=sftp_file_upload($upload, $folder_name, $file_name,$response_sftp_arr, $multiple_file_index);       
                    //$response=array('status'=>'SUCCESS','file_name'=>$file_name);        // For Testing 
                    if(isset($response['status']) && $response['status']=='SUCCESS'){       //Check if no error occurs in file uploading
                        $return_array[$r]['file_name']=$response['file_name'];
                        $return_array[$r]['status']=$response['status'];
                    } else {                        
                        $return_array[$r]['msg']=$response['msg'];
                        $return_array[$r]['code']=$response['code'];
                        $return_array[$r]['status']=$response['status'];
                    }
                }
            }
        }                   // End of connection establishment to SFTP
    }                       // End of policy document input field check
    unset($conn_id);
    return $return_array;
}




/**
 * 
 * @author Sanjeev Kumar
 * @todo THis function writtem for upload Plan document.
 * @global type $conn_id
 * @global type $conn
 * @param type $upload
 * @param type $plan_type
 * @return type
 * Code written on 10-Feb-2017
 * In upload_plan_doc() function receive upload input field name and plan type that is Retail or Corporate or HCK.
 * 
 */
function upload_plan_doc($upload='',$plan_type=''){
    global $conn_id;            //SFTP Connection
    global $conn;  
    $return_array=array();                                      // Initialize empty array for returning message and status
    $plan_type_array=array('H'=>'HCK','R'=>'RETAIL','C'=>'CORPORATE');      // Define array for plan type
    if(empty($_FILES[$upload]['name'])){                        // Check if plan document field is empty
        $return_array[00]['msg']=SFTP0014;
        $return_array[00]['code']='SFTP0014';          
        $return_array[00]['status']='ERROR';          
    }
    
    if(empty($plan_type) || !in_array($plan_type,$plan_type_array)){
        $return_array[01]['msg']=SFTP0018;
        $return_array[01]['code']='SFTP0018';          
        $return_array[01]['status']='ERROR';
    }
    if(empty($return_array)){                                   //  Check if no error occurs
        if($plan_type=='HCK'){
            $type='HCK';
        }elseif($plan_type=='RETAIL'){
            $type='R';
        }elseif($plan_type=='CORPORATE'){
            $type='C';
        }
        $response_sftp_arr=sftp_connection();                   // Connect to SFTP Server.
        //$response_sftp_arr=array('status'=>'SUCCESS','response_sftp'=>'5'); // For Testing
        if(!empty($response_sftp_arr) && ($response_sftp_arr['status']=='ERROR')){      // Check if successfully connect to SFTP Server.
            $return_array[01]['msg']=$response_sftp_arr['msg'];
            $return_array[01]['code']=$response_sftp_arr['code'];
            $return_array[01]['status']=$response_sftp_arr['status'];
        }
        if(empty($return_array) && !empty($response_sftp_arr) && ($response_sftp_arr['status']=='SUCCESS')){       // If successfully connect to SFTP Server
            /*--------------- For upload plan document ---------------------*/
            $ext = pathinfo($_FILES[$upload]['name'], PATHINFO_EXTENSION);
            $name_plan_doc = pathinfo($_FILES[$upload]['name'], PATHINFO_FILENAME);
            $file_name=$type.'_'.time().'_'.strtolower($name_plan_doc).".".$ext;
            $folder_name=PLAN_DOC;
            $response=sftp_file_upload($upload, $folder_name, $file_name,$response_sftp_arr);       
            //$response=array('status'=>'SUCCESS','file_name'=>$file_name);        // For Testing 
            if(isset($response['status']) && $response['status']=='SUCCESS'){       //Check if no error occurs in other report file uploading
                $return_array=$response;
            } else {
                $return_array[02]['msg']=$response['msg'];
                $return_array[02]['code']=$response['code'];
                $return_array[02]['status']=$response['status'];
            }
        }                   // End of connection establishment to SFTP
    }       // End of plan document input field empty check
    unset($conn_id);
    return $return_array;
}


/**
 * 
 * @author Sanjeev Kumar
 * @todo This function written  for upload endorsement document.
 * @global type $conn_id
 * @global type $conn
 * @param type $upload
 * @param type $policy_number
 * @return type
 * Code written on 14-Feb-2017
 * In upload_endorsement_doc() function receive name of upload input field and policy number
 * 
 */
function upload_endorsement_doc($upload='',$policy_number){
    global $conn_id;            //SFTP Connection
    global $conn;  
    $return_array=array();                                      // Initialize empty array for returning message and status
    if(empty($_FILES[$upload]['name'])){                        // Check if plan document field is empty
        $return_array[00]['msg']=SFTP0014;
        $return_array[00]['code']='SFTP0014';          
        $return_array[00]['status']='ERROR';          
    }
    if(empty($policy_number)){
        $return_array[01]['msg']=SFTP0013;
        $return_array[01]['code']='SFTP0013';
        $return_array[01]['status']='ERROR';
    }
    if(empty($return_array)){
        $response_sftp_arr=sftp_connection();                   // Connect to SFTP Server.
        //$response_sftp_arr=array('status'=>'SUCCESS','response_sftp'=>'5'); // For Testing
        if(!empty($response_sftp_arr) && ($response_sftp_arr['status']=='ERROR')){      // Check if successfully connect to SFTP Server.
            $return_array[02]['msg']=$response_sftp_arr['msg'];
            $return_array[02]['code']=$response_sftp_arr['code'];
            $return_array[02]['status']=$response_sftp_arr['status'];
        }
        if(empty($return_array) && !empty($response_sftp_arr) && ($response_sftp_arr['status']=='SUCCESS')){       // If successfully connect to SFTP Server
            /*--------------- For upload endorsement document ---------------------*/
            $ext = pathinfo($_FILES[$upload]['name'], PATHINFO_EXTENSION);
            $name_endorsement_doc = pathinfo($_FILES[$upload]['name'], PATHINFO_FILENAME);
            $file_name=$policy_number.'_'.time().'_'.strtolower($name_endorsement_doc).".".$ext;
            $folder_name=ENDORSEMENT_DOC;
            $response=sftp_file_upload($upload, $folder_name, $file_name,$response_sftp_arr);       
            //$response=array('status'=>'SUCCESS','file_name'=>$file_name);        // For Testing 
            if(isset($response['status']) && $response['status']=='SUCCESS'){       //Check if no error occurs in other report file uploading
                $return_array=$response;
            } else {
                $return_array[03]['msg']=$response['msg'];
                $return_array[03]['code']=$response['code'];
                $return_array[03]['status']=$response['status'];
            }
        }   // End of checking SFTP Response
    }   // End of check for empty policy number
    unset($conn_id);
    return $return_array;
}



/**
 * @author Sanjeev Kumar
 * @todo This function written for upload product document using SFTP.
 * @global type $conn_id
 * @global type $conn
 * @param type $upload
 * @param type $applicable_for
 * @return type
 * Code written on 14-Feb-2017
 * In upload_product_doc() function receive upload input field name and product added for Retail or Corporate or Both.
 * 
 */
function upload_product_doc($upload='',$applicable_for){
    global $conn_id;            //SFTP Connection
    global $conn;  
    $return_array=array();                                      // Initialize empty array for returning message and status
    $applicable_for_array=array('R'=>'RETAIL','C'=>'CORPORATE','B'=>'BOTH');      // Define array for plan type
    if(empty($_FILES[$upload]['name'])){                        // Check if plan document field is empty
        $return_array[00]['msg']=SFTP0014;
        $return_array[00]['code']='SFTP0014';          
        $return_array[00]['status']='ERROR';          
    }
    
    if(empty($applicable_for) || !in_array($applicable_for,$applicable_for_array)){
        $return_array[01]['msg']=SFTP0020;
        $return_array[01]['code']='SFTP0020';          
        $return_array[01]['status']='ERROR';
    }
    if(empty($return_array)){                                   //  Check if no error occurs
        if($applicable_for=='RETAIL'){
            $type='R';
        }elseif($applicable_for=='CORPORATE'){
            $type='C';
        }elseif($applicable_for=='BOTH'){
            $type='B';
        }
        $response_sftp_arr=sftp_connection();                   // Connect to SFTP Server.
        //$response_sftp_arr=array('status'=>'SUCCESS','response_sftp'=>'5'); // For Testing
        if(!empty($response_sftp_arr) && ($response_sftp_arr['status']=='ERROR')){      // Check if successfully connect to SFTP Server.
            $return_array[01]['msg']=$response_sftp_arr['msg'];
            $return_array[01]['code']=$response_sftp_arr['code'];
            $return_array[01]['status']=$response_sftp_arr['status'];
        }
        if(empty($return_array) && !empty($response_sftp_arr) && ($response_sftp_arr['status']=='SUCCESS')){       // If successfully connect to SFTP Server
            /*--------------- For upload plan document ---------------------*/
            $ext = pathinfo($_FILES[$upload]['name'], PATHINFO_EXTENSION);
            $name_product_doc = pathinfo($_FILES[$upload]['name'], PATHINFO_FILENAME);
            $file_name=$type.'_'.time().'_'.strtolower($name_product_doc).".".$ext;
            $folder_name=PRODUCT_DOC;
            $response=sftp_file_upload($upload, $folder_name, $file_name,$response_sftp_arr);       
            //$response=array('status'=>'SUCCESS','file_name'=>$file_name);        // For Testing 
            if(isset($response['status']) && $response['status']=='SUCCESS'){       //Check if no error occurs in other report file uploading
                $return_array=$response;
            } else {
                $return_array[02]['msg']=$response['msg'];
                $return_array[02]['code']=$response['code'];
                $return_array[02]['status']=$response['status'];
            }
        }                   // End of connection establishment to SFTP
    }       // End of plan document input field empty check
    unset($conn_id);
    return $return_array;
}




/**
 * 
 * @author Sanjeev Kumar
 * @todo This function written for upload reimbursement document using SFTP.
 * @global type $conn_id
 * @global type $conn
 * @global type $response_sftp_arr
 * @global string $response_sftp
 * @param type $upload
 * @param type $policy_number
 * @return type
 * Code created on 16-Feb-2017
 * In upload_reimbursement_doc() function receive file upload input field name and policy number.
 * 
 */
function upload_reimbursement_doc($upload='', $policy_number=''){
    global $conn_id;            // SFTP Connection ResoutceID
    global $conn;               // Database connection
    global $response_sftp_arr;  // SFTP Response array which contains status, Message code and SFTP Subsystem ResourceID.
    global $response_sftp;      // SFTP subsystem ResourceId
    $return_array=array();                                      // Initialize empty array for returning message and status
    if(empty($_FILES[$upload]['name'])){    // Check if file upload input field is empty
        $return_array[00]['msg']=SFTP0014;
        $return_array[00]['code']='SFTP0014';          
        $return_array[00]['status']='ERROR';
    }
    if(empty($policy_number)){
        $return_array[01]['msg']=SFTP0021;
        $return_array[01]['code']='SFTP0021';          
        $return_array[01]['status']='ERROR';
    }
    if(empty($return_array)){
        if(!$response_sftp && empty($response_sftp_arr['response_sftp'])){              // Check if already connected to SFTP
            $response_sftp_arr=sftp_connection();                 // Connect to SFTP Server.
        }
        //$response_sftp_arr=array('status'=>'SUCCESS','response_sftp'=>'5'); // For Testing
        if(!empty($response_sftp_arr) && ($response_sftp_arr['status']=='ERROR')){      // Check if successfully connect to SFTP Server.
            $return_array[03]['msg']=$response_sftp_arr['msg'];
            $return_array[03]['code']=$response_sftp_arr['code'];
            $return_array[03]['status']=$response_sftp_arr['status'];
        }
        if(empty($return_array) && !empty($response_sftp_arr) && ($response_sftp_arr['status']=='SUCCESS')){       // If successfully connect to SFTP Server
            /*--------------- For upload reimbursement document ---------------------*/
            if(!empty($_FILES[$upload]['name'])){
                $ext = pathinfo($_FILES[$upload]['name'], PATHINFO_EXTENSION);
                $name_reimbursement_doc = pathinfo($_FILES[$upload]['name'], PATHINFO_FILENAME);
                $file_name=time().'_'.strtolower($name_reimbursement_doc).".".$ext;
                $folder_name=str_replace('##POLICY_NO##',$policy_number,REIMBURSEMENT_DOC);
                $response=sftp_file_upload($upload, $folder_name, $file_name,$response_sftp_arr);    
                //$response=array('status'=>'SUCCESS','file_name'=>$file_name);        // For Testing 
                if(isset($response['status']) && $response['status']=='SUCCESS'){       //Check if no error occurs in other report file uploading
                    $return_array=$response;
                } else {                        
                    $return_array[04]['msg']=$response['msg'];
                    $return_array[04]['code']=$response['code'];
                    $return_array[04]['status']=$response['status'];
                }
            }
        }                   // End of connection establishment to SFTP
    }                       // End of policy document input field check
    //unset($conn_id);
    return $return_array;
}




/**
 * 
 * @author Sanjeev Kumar
 * @todo This function create for upload corporate document.
 * @global type $conn_id
 * @global type $conn
 * @global type $response_sftp_arr
 * @global string $response_sftp
 * @param type $upload
 * @param type $company_id
 * @return type
 * Code written on 16-Feb-2017
 * In upload_corporate_doc() function receive name of file upload input field and companyid
 * 
 */
function upload_corporate_doc($upload='',$company_id){
    global $conn_id;            // SFTP Connection ResoutceID
    global $conn;               // Database connection
    global $response_sftp_arr;  // SFTP Response array which contains status, Message code and SFTP Subsystem ResourceID.
    global $response_sftp;      // SFTP subsystem ResourceId
    $return_array=array();                                      // Initialize empty array for returning message and status
    if(empty($_FILES[$upload]['name'])){    // Check if file upload input field is empty
        $return_array[00]['msg']=SFTP0014;
        $return_array[00]['code']='SFTP0014';          
        $return_array[00]['status']='ERROR';
    }
    if(empty($company_id)){
        $return_array[01]['msg']=SFTP0023;
        $return_array[01]['code']='SFTP0023';          
        $return_array[01]['status']='ERROR';
    }
    if(empty($return_array)){
        if(!$response_sftp && empty($response_sftp_arr['response_sftp'])){              // Check if already connected to SFTP
            $response_sftp_arr=sftp_connection();                 // Connect to SFTP Server.
        }
        //$response_sftp_arr=array('status'=>'SUCCESS','response_sftp'=>'5'); // For Testing
        if(!empty($response_sftp_arr) && ($response_sftp_arr['status']=='ERROR')){      // Check if successfully connect to SFTP Server.
            $return_array[02]['msg']=$response_sftp_arr['msg'];
            $return_array[02]['code']=$response_sftp_arr['code'];
            $return_array[02]['status']=$response_sftp_arr['status'];
        }
        if(empty($return_array) && !empty($response_sftp_arr) && ($response_sftp_arr['status']=='SUCCESS')){       // If successfully connect to SFTP Server
            /*--------------- For upload reimbursement document ---------------------*/
            if(!empty($_FILES[$upload]['name'])){
                $ext = pathinfo($_FILES[$upload]['name'], PATHINFO_EXTENSION);
                $name_corporate_doc = pathinfo($_FILES[$upload]['name'], PATHINFO_FILENAME);
                $file_name=time().'_'.strtolower($name_corporate_doc).".".$ext;
                $folder_name=str_replace('##COMPANY_ID##',$company_id,CORPORATE_DOC);
                $response=sftp_file_upload($upload, $folder_name, $file_name,$response_sftp_arr);    
                //$response=array('status'=>'SUCCESS','file_name'=>$file_name);        // For Testing 
                if(isset($response['status']) && $response['status']=='SUCCESS'){       //Check if no error occurs in other report file uploading
                    $return_array=$response;
                } else {                        
                    $return_array[03]['msg']=$response['msg'];
                    $return_array[03]['code']=$response['code'];
                    $return_array[03]['status']=$response['status'];
                }
            }
        }                   // End of connection establishment to SFTP
    }                       // End of policy document input field check
    unset($conn_id);
    return $return_array;
}



/**
 * 
 * @author Sanjeev Kumar
 * @todo This function written for upload Scope healthtime document using SFTP.
 * @global type $conn_id
 * @global type $conn
 * @global type $response_sftp_arr
 * @global string $response_sftp
 * @param type $upload
 * @param type $month_year
 * @return type
 * Code written on 17-Feb-2017
 * In upload_healthtime_doc() function receive name of file upload input field and month year.
 * 
 */
function upload_healthtime_doc($upload='',$month_year){
    global $conn_id;            // SFTP Connection ResoutceID
    global $conn;               // Database connection
    global $response_sftp_arr;  // SFTP Response array which contains status, Message code and SFTP Subsystem ResourceID.
    global $response_sftp;      // SFTP subsystem ResourceId
    $return_array=array();                                      // Initialize empty array for returning message and status
    if(empty($_FILES[$upload]['name'])){    // Check if file upload input field is empty
        $return_array[00]['msg']=SFTP0014;
        $return_array[00]['code']='SFTP0014';          
        $return_array[00]['status']='ERROR';
    }
    if(empty($month_year)){
        $return_array[01]['msg']=SFTP0024;
        $return_array[01]['code']='SFTP0024';          
        $return_array[01]['status']='ERROR';
    }
    if(empty($return_array)){
        if(!$response_sftp && empty($response_sftp_arr['response_sftp'])){              // Check if already connected to SFTP
            $response_sftp_arr=sftp_connection();                 // Connect to SFTP Server.
        }
        //$response_sftp_arr=array('status'=>'SUCCESS','response_sftp'=>'5'); // For Testing
        if(!empty($response_sftp_arr) && ($response_sftp_arr['status']=='ERROR')){      // Check if successfully connect to SFTP Server.
            $return_array[02]['msg']=$response_sftp_arr['msg'];
            $return_array[02]['code']=$response_sftp_arr['code'];
            $return_array[02]['status']=$response_sftp_arr['status'];
        }
        if(empty($return_array) && !empty($response_sftp_arr) && ($response_sftp_arr['status']=='SUCCESS')){       // If successfully connect to SFTP Server
            /*--------------- For upload reimbursement document ---------------------*/
            if(!empty($_FILES[$upload]['name'])){
                $ext = pathinfo($_FILES[$upload]['name'], PATHINFO_EXTENSION);
                $name_healthtime_doc = pathinfo($_FILES[$upload]['name'], PATHINFO_FILENAME);
                $file_name=time().'_'.strtolower($name_healthtime_doc).".".$ext;
                $folder_name=str_replace('##MONTH_YEAR##',strtoupper($month_year),HEALTHTIME_DOC);
                $response=sftp_file_upload($upload, $folder_name, $file_name,$response_sftp_arr);    
                //$response=array('status'=>'SUCCESS','file_name'=>$file_name);        // For Testing 
                if(isset($response['status']) && $response['status']=='SUCCESS'){       //Check if no error occurs in other report file uploading
                    $return_array=$response;
                } else {                        
                    $return_array[03]['msg']=$response['msg'];
                    $return_array[03]['code']=$response['code'];
                    $return_array[03]['status']=$response['status'];
                }
            }
        }                   // End of connection establishment to SFTP
    }                       // End of policy document input field check
    unset($conn_id);
    return $return_array;
}






/**
 * 
 * @author Sanjeev Kumar
 * @todo This function write to display messages of file upload
 * @param type $message_details
 * @return string
 * Date: 02-Feb-2017
 * In sftp_message_display() function pass message array as argument
 * 
 */
function sftp_message_display($message_details=array()) { 
    $message_data='';
    $message_data.= ' <style>
                    .sftp_error_div .fade.in {
                        opacity: 1;
                    }
                    .sftp_error_div .alert-danger {
                        background-image: linear-gradient(to bottom, #f2dede 0px, #e7c3c3 100%);
                        background-repeat: repeat-x;
                        border-color: #dca7a7 !important;
                    }
                    .sftp_error_div .alert {
                        box-shadow: 0 1px 0 rgba(255, 255, 255, 0.25) inset, 0 1px 2px rgba(0, 0, 0, 0.05);
                        text-shadow: 0 1px 0 rgba(255, 255, 255, 0.2);
                        margin:5px !important;
                    }
                     .sftp_error_div .alert-danger {
                        background-color: #f2dede;
                        border-color: #ebccd1;
                        color: #a94442;
                        font-size:12px !important;
                    }
                    .sftp_error_div .alert {
                        border: 1px solid transparent;
                        border-radius: 4px;
                        padding: 5px !important;
                    }
                    .sftp_error_div .fade {
                        opacity: 0;
                        transition: opacity 0.15s linear 0s;
                    }
                    .sftp_error_div {
                        margin:5px !important;
                    }
                    .sftp_error_div .close {
                        color: #000;
                        float: right;
                        font-size: 16px;
                        font-weight: 700;
                        line-height: 1;
                        opacity: 0.2;
                        text-shadow: 0 1px 0 #fff;
                    }
                    .cl {
                        clear: both;
                    }
                    .sftp_error_div .alert-danger a{
                        text-decoration:none;
                    }
                    .sftp_error_div .close:focus,.sftp_error_div .close:hover {
                        color: #000;
                        cursor: pointer;
                        opacity: 0.5;
                        text-decoration: none;
                    }
                    .sftp_error_div .alert-success {
                        background-color: #dff0d8;
                        border-color: #aec7a4;
                        color: #3c763d !important;
                        font-size:12px;
                    }
                     .sftp_error_div .alert-success a{
                        text-decoration:none;
                    }
                    .sftp_error_div span{
                    padding:2px;
                    /*display:list-item;*/
                    }
                    </style>                   
                    <script>
                        $(document).ready(function(){
                            $(".sftp_error_close").click(function(){
                            var id=$(this).attr("id");
                                 $("#SFTP_ERROR_MESSAGE_DIV_"+id).fadeTo("slow", 0);
                                 setTimeout(function(){ $("#SFTP_ERROR_MESSAGE_DIV_"+id).hide();}, 500);           
                             }); 
                             $(".sftp_success_close").click(function(){
                             var id=$(this).attr("id");
                                 $("#SFTP_SUCCESS_MESSAGE_DIV_"+id).fadeTo("slow", 0);
                                 setTimeout(function(){ $("#SFTP_SUCCESS_MESSAGE_DIV_"+id).hide();}, 500);           
                             });
                        });        
                    </script>';
    if(!empty($message_details)){
        $count=1;
        foreach($message_details as $key => $message){
            
            if($message['status']=='SUCCESS'){
                $message_data.='<div class="cl"></div>
                                <div class="sftp_error_div bs-example SFTP_SUCCESS_MESSAGE_DIV" id="SFTP_SUCCESS_MESSAGE_DIV_'.$count.'">
                                    <div class="alert alert-success fade in">
                                        <a href="#" class="close sftp_success_close" id="'.$count.'" data-dismiss="alert">&times;</a> 
                                        <!--<strong>Success!</br></strong>-->';
                $message_data.= '<span><strong>'.$message['code'].' : </strong>'.$message['msg'].'</span>';            
                $message_data.='</div></div>';
            }elseif($message['status']=='ERROR'){
                $message_data.='<div class="cl"></div>
                                <div class="sftp_error_div bs-example SFTP_ERROR_MESSAGE_DIV" id="SFTP_ERROR_MESSAGE_DIV_'.$count.'">
                                    <div class="alert alert-danger fade in">
                                        <a href="#" class="close sftp_error_close" id="'.$count.'" data-dismiss="alert">&times;</a>                           <!--<strong>Error!</br></strong>-->'; 
                $message_data.= '<span><strong>'.$message['code'].' : </strong>'.$message['msg'].'</span>';
                $message_data.='</div></div>';
            }
            $count++;
            }
    }
    return $message_data;
}