<script>
var datelist = '';
var ptype = $('#ptype').val();
var appointment_status = {};
appointment_status = {
	"PENDING":[{"VIEW" : "View Details"},{"CANCELLED" : "Cancel"},{"CONFIRM" : "Confirm"},{"RESCHEDULE" : "Re-schedule"}],
	"CONFIRM":[{"VIEW" : "View Details"},{"CANCELLED" : "Cancel"},{"DONE" : "Mark As Done"},{"RESCHEDULE" : "Re-schedule"},{"CONFIRM" : "Re-Send Authorization Letter"}], 
	"CANCELLED":[{"VIEW" : "View Details"}],
	"RESCHEDULE":[{"VIEW" : "View Details"}],
	"DONE":[{"VIEW" : "View Details"},{"UPLOADREPORT" : "Upload Report"}]
};
mindate = 1;
if (ptype == 'customerlogin' || ptype == 'hr') {
	mindate = 3;
}
$(document).ready(function() {
	var appointment_errorss = {0:'', 1: 'Gender Mismatch', 2: 'Age Mismatch', 4:'SI Mismatch' , 8:'Relation Mismatch', 16:'Appointment is already requested/booked', 32:'Not Applicable for limited Plan', 64:'Not Applicable for purchased plan', 128:''};
	var centerdetails = '';
	var eventDates = {};
	var HolidayDates = ''; 
	var slotdetails = {};
	var planandmembers = {};
	var memberlist = {};
	$( "#policynumber,#customerid" ).keyup(function() {
		var policynumber = $('#policynumber').val();
		var customerId = $('#customerid').val();
		var PolicyLength = policynumber.length;
		var customerIdLength = customerId.length;
		var ajaxhit = false;
		var valtype = $(this).attr('id');
		$(".successdiv").html('');
		$('#empname').val('');
		$('#email').val('');
		$('#number').val('');
		$("#pname").hide();
		$("#caseid").hide();
		$('#memberlist').html('<td class="apperror" colspan="6" align="center" class="col-border-1">Select Plan first.</td>');
		$("#planid").html('<option value="">Select Plan</option>');
		if (valtype == 'customerid') {
			if (PolicyLength == 8 && customerIdLength == 8) {
				ajaxhit = true;
			}
		} else {
			$('#customerid').val('');
			if (PolicyLength == 8) {
				ajaxhit = true;
			}	
		}
		var subtype = $('#portal').val();
		if (ptype == 'mscrm'){
			$("#policylist").html('<td class="apperror" colspan="8" align="center">Please Enter Policy Number </td>');
		}
		if (ajaxhit) {			
			$(".errordiv").html('');
			$(".warningdiv").text('');
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: 'ajax/GetAppointmentDetails.php',
				data: {'type': 'CHECKPOLICYEXIST', 'policyNumber': policynumber, 'customerId': customerId, 'subtype': subtype},
				beforeSend: function () {
					//$("#loadingimage").show();
					$.LoadingOverlay('show');
				},
				success: function (res) {
					//console.log(res.planlist);
					if(res.success != 1){
						if (res.errors!='') {
							$(".errordiv").html(res.errors);				   
						}
						if (res.message!='') {
							$(".warningdiv").text(res.message);				   
							$(".warningdiv").show(); 
							$('#customerid').attr('disabled', false);
						} 					
					} else {				
						$("#planmsg").text('');
						$("#planmsg").hide();
						$("#policyid").val(res.policyid);
						$("#customerid").val(res.customerId);
						$("#productid").val(res.productId);
						$("#productname").val(res.productname);
						$("#productcode").val(res.productcode);
						$("#companyid").val(res.companyid);
						$("#companyname").val(res.companyname);
						if (res.productId!=''){
							$('#customerid').attr('disabled', true);
							$("#pname").show();
							$("#caseid").show();
							$("#cid").prop("readonly", false); 
						}
						planandmembers =  res.planlist;
						$("#planid").html('');	
						var num = 1;
						if (ptype == 'hr') {  
							finalplanlist =  planandmembers.filter(function (planandmembers) { return planandmembers.PLANTYPE == 'FREE' });
							var num = 0;
						} else {
							finalplanlist = res.planlist;
						}
						if (finalplanlist.length > num) {
							for (var i = num;i<finalplanlist.length;i++) {
								var plantype = ((finalplanlist[i]['PLANTYPE'])?finalplanlist[i]['PLANTYPE']:finalplanlist[i]['PAYMENTOPTIONS']);
								var planname = ((finalplanlist[i]['PLANNAME2'])?finalplanlist[i]['PLANNAME2']:finalplanlist[i]['PLANNAME'])+' - '+plantype;
								var option=$('<option value="'+finalplanlist[i]['PLANID']+'||'+plantype+'"></option>').text(planname);
								$('#planid').append(option);
							}
							$('#planid').change();
						} else {
							$("#planmsg").text('Plans not found for entered policy number');
							$("#planmsg").show();
						}
						if (ptype == 'mscrm') {
							if (res.policylist!=''){
								$("#policylist").html(res.policylist);
							} else {
								$("#policylist").html('<td class="apperror" colspan="8" align="center">No Record Found.</td>');
							}
						}
					}
					//$("#loadingimage").hide();
					$.LoadingOverlay('hide');
				}

			});
		}
	});
	$("#planid" ).change(function() {
		if ($.isEmptyObject(planandmembers) && ptype == 'customerlogin') {
			planandmembers = csplanmember;
		}
		memberlist = planandmembers[0];
		 var Planid = $(this).val();			 
		 if  (Planid!='') {
			var type      = 'GETPLANDETAILS';
			var companyid = $('#companyid').val();
			var policyid  = $('#policyid').val();
			var productid = $('#productid').val();
			$('#cityid').html('<option value="">Select City</option>');
			$('#stateid').html('<option value="">Select State</option>');
			$('#centerid').html('<option value="">Select Center</option>');
			$('#center_autocomp').val('');
			$("#planmsg").hide();
			$('#memberlist').val('');
			$('#empname').val('');
			$('#email').val('');
			$('#number').val('');
			var pidsplit = Planid.split('||');
			var pid = pidsplit[0];							
			plandetails = planandmembers.filter(function (planandmembers) { return planandmembers.PLANID == pid });	
			memberlist = memberlist.MEMBERS;
			//console.log(memberlist);
			$('#empname').val(memberlist[0]['QC-MEMNAME']);
			$('#email').val(memberlist[0]['QC-EMAIL']);
			$('#numbers').val(memberlist[0]['QC-CLTPHONE']?memberlist[0]['QC-CLTPHONE']:memberlist[0]['QC-CLTPHONE02']);
			planmember = plandetails[0]['MEMBERSDETAILS'];
			var getmemberhtml = CreateMemberhtml(memberlist,plandetails);
			$('#memberlist').html(getmemberhtml);
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: 'ajax/GetAppointmentDetails.php',
				async: false,
				data: {'type': type, 'policyid': policyid, 'planid': Planid, 'companyid': companyid, 'productid': productid},
				beforeSend: function () {
					//$("#loadingimage1").show();
					$.LoadingOverlay('show');
				},
				success: function (obj) {
					//ChangeStateCityCenter(obj);
					if (obj.error == '') { 
						centerdetails = obj;						
						for (var i=0;i<obj.statelist.length;i++) {
						  var option=$('<option value="'+obj.statelist[i]['STATEID']+'"></option>').text(obj.statelist[i]['STATENAME']);
						  $('#stateid').append(option);
						}							
					} else {
						$("#planmsg").text(obj.error);
						$("#planmsg").show();	
					}
					//$("#loadingimage1").hide(); 
					$.LoadingOverlay('hide');					
				}
			});
		} else {
			$("#planmsg").text('Please select Plan.');
			$("#planmsg").show();
		}
	});
	$(".centerdetail" ).change(function(){
		var id = $(this).attr('id');
		var selectedid = $("#"+id+" option:selected").val();
		if (id == 'stateid') {
			$('#cityid').html('<option value="">Select City</option>');
			$('#centerid').html('<option value="">Select Center</option>');
			$('#center_autocomp').val("");
			if (selectedid!='') {
				for (var i=0;i<centerdetails.centerlist.length;i++) {
					if (centerdetails.centerlist[i]['STATEID'] == selectedid) {
						var option=$('<option value="'+centerdetails.centerlist[i]['CENTERID']+'"></option>').text(centerdetails.centerlist[i]['CENTERNAME']+ ' '+centerdetails.centerlist[i]['ADDRESS1']+ ' '+centerdetails.centerlist[i]['ADDRESS2']);
						$('#centerid').append(option);
					}
				} 
				for(var i=0;i < centerdetails.citylist.length; i++) {
					if (centerdetails.citylist[i]['STATEID'] == selectedid) {
						var option=$('<option value="'+centerdetails.citylist[i]['CITYID']+'"></option>').text(centerdetails.citylist[i]['CITYNAME']);
						$('#cityid').append(option);
					}
				}
			}
		}
		if (id == 'cityid') {
		   var stateid = $("#stateid option:selected").val();
		   if (stateid != ''){
			   $('#centerid').html('<option value="">Select Center</option>');
			   $('#center_autocomp').val("");
			   if (selectedid!='') {
					for (var i=0;i<centerdetails.centerlist.length;i++) {
						if (centerdetails.centerlist[i]['CITYID'] == selectedid) {
							var option=$('<option value="'+centerdetails.centerlist[i]['CENTERID']+'"></option>').text(centerdetails.centerlist[i]['CENTERNAME']+ ' '+centerdetails.centerlist[i]['ADDRESS1']+ ' '+centerdetails.centerlist[i]['ADDRESS2']);
							$('#centerid').append(option);
						}
					 }
				}
		   } else {
				example5("\nPlease select State.", 'stateid');
				$("#cityid").val("");  
				$('#centerid').val("");
				$('#center_autocomp').val("");
		   }
		}
		if (id == 'centerid') {
		   var cityid = $("#cityid option:selected").val();
		   if (cityid != ''){
				$('#center_autocomplete').val('');
				if (selectedid!='') {	
					var centername = $("#"+id+" option:selected").text();	
					$('#center_autocomplete').val(centername);
					var type = 'GETAVAILABLEDATES';
					$.LoadingOverlay('show');
					setTimeout(function() {
						$.ajax({
							type: 'POST',
							dataType: 'json',
							url: 'ajax/GetAppointmentDetails.php',	
							data: {'type': type, 'centerid': selectedid},
							async: false,
							success: function (HolidayDates) {	
								datelist = HolidayDates.datelist;
								CreateEventList(HolidayDates,''); 
								$.LoadingOverlay('hide');
							}
						});
					}, 200);
					$.LoadingOverlay('hide');
				}
		   } else {
				example5("\nPlease select City.", 'cityid');   
				$('#'+id).val("");
		   }    
		}
	});
	$(".rescenterdetail" ).change(function(){
		var id = $(this).attr('id');
		var selectedid = $("#"+id+" option:selected").val();
		if (selectedid!='') {	
			var centername = $("#"+id+" option:selected").text();	
			var type = 'GETAVAILABLEDATES';
			$.LoadingOverlay('show');
			$.ajax({
				type: 'POST',
				dataType: 'json',
				url: 'ajax/GetAppointmentDetails.php',
				data: {'type': type, 'centerid': selectedid},
				async: false,
				beforeSend: function () {
				   //$("#dateloadingimage").show();
				},
				success: function (HolidayDates) {	
					datelist = HolidayDates.datelist;
					CreateEventList(HolidayDates,'reschedule'); 
				}
			});
			//$("#dateloadingimage").hide();
			$.LoadingOverlay('hide');
		 }
	});
	function CreateEventList(HolidayDates,apptype) {
		var checkmembercount = $('.membercheckbox:checked').length;
		if (apptype == 'reschedule'){
			checkmembercount = 1;
		}
		if (checkmembercount>0) {
			eventDates = {};
			appavailcolor = {};
			$('#appointmentdate').val('');
			 $('#timeslot').html('<option value="">Select Slot</option>');
			 $('#rescheduledate').val('');
			 $('#timeslotres').html('<option value="">Select Slot</option>');
			for (var i=0;i<HolidayDates.holidaylist.length;i++) { 
				var newDate = ChangeDateFormat(HolidayDates.holidaylist[i]['HOLIDAYDATE']);
				eventDates[ new Date(newDate)] = HolidayDates.holidaylist[i]['HOLIDAYNAME'];
				appavailcolor[ new Date( newDate )] = 'orange';
			}
			var alredychecked = {};
			var calendardate  = [];
			var datelistfinal = [];
			var datelistforcheck = HolidayDates.datelist ;
			if (!$.isEmptyObject(datelistforcheck)) {
				datelistfinal =  datelistforcheck.filter(function (datelistforcheck) { return datelistforcheck.AVAILAPPOINTMENT >= checkmembercount });
				for (var i=0;i<datelistfinal.length;i++) {
					if(!alredychecked[datelistfinal[i]['CALDATE']]) {					
						COLOR = 'green';						
						var newDate = ChangeDateFormat(datelistfinal[i]['CALDATE']);
						eventDates[ new Date(newDate)] = (newDate).toString();
						appavailcolor[ new Date( newDate )] = COLOR;   
					}
				}
			}
		} else {
			 example5("\nPlease select member first.", 'memberid');
			 $('#centerid').val('').change();
		}
	}
	$( "#appointmentdate" ).datepicker({
		beforeShowDay: function( date ) { 
		     //$.LoadingOverlay('show');
			//$('#appointmentdateimg').show();
			var highlight = eventDates[date];
			if( highlight ) {  
				var appointment = appavailcolor[date];
				var event = ((appointment == 'green')?'event_green':(appointment == 'red')?'event_red':(appointment == 'orange')?'event_orange':(appointment == 'yellow')?'event_yellow':(appointment == 'lightred')?'event_lightred':'');
				if (appointment == 'green' || appointment == 'yellow' || appointment == 'lightred'){
					return [true, event, highlight];
				} else {  
					return [false, event, highlight];
				}
				
			} else {
				return [false, '', ''];
			}
		},
		minDate:mindate,
		dateFormat: 'dd-M-yy',
		onSelect:function(selected){
			var memberscount = $('.membercheckbox:checked').length;
			$('#timeslot').html('<option value="">Select Slot</option>');
			for (var i=0;i<datelist.length;i++) {  
				 var newDate = ChangeDateFormat(datelist[i]['CALDATE'],'dd-M-yy');
				if (newDate == selected && datelist[i]['AVAILAPPOINTMENT']>=memberscount) {
					var option=$('<option value="'+datelist[i]['SLOTID']+'"></option>').text(datelist[i]['STARTTIMENAME']);
					$('#timeslot').append(option);
					slotdetails[datelist[i]['SLOTID']] = datelist[i];
				}
			}  
		},
	});
	$( "#rescheduledate" ).datepicker({
		beforeShowDay: function( date ) { 
			//$('#rescheduledateimg').show();
			//$.LoadingOverlay('show');
			var highlight = eventDates[date];
			if( highlight ) {  
				var appointment = appavailcolor[date];
				var event = ((appointment == 'green')?'event_green':(appointment == 'red')?'event_red':(appointment == 'orange')?'event_orange':(appointment == 'yellow')?'event_yellow':(appointment == 'lightred')?'event_lightred':'');
				if (appointment == 'green' || appointment == 'yellow' || appointment == 'lightred'){
					return [true, event, highlight];
				} else {  
					return [false, event, highlight];
				}	
			} else {
				return [false, '', ''];
			}
		},
		minDate:3,
		dateFormat: 'dd-M-yy',
		onSelect:function(selected){
			$('#timeslotres').html('<option value="">Select Slot</option>');
			var memberscount = $('.membercheckbox:checked').length;
			for (var i=0;i<datelist.length;i++) {  	
				 var newDate = ChangeDateFormat(datelist[i]['CALDATE'],'dd-M-yy');
				if (newDate == selected && datelist[i]['AVAILAPPOINTMENT']>=memberscount) {
					var option=$('<option value="'+datelist[i]['SLOTID']+'"></option>').text(datelist[i]['STARTTIMENAME']);
					$('#timeslotres').append(option);
					slotdetails[datelist[i]['SLOTID']] = datelist[i];
				}
			}  
		},
	});
	$("#timeslot" ).change(function(){
		var slotid =  $("#timeslot option:selected").val();
		var members = $('.membercheckbox:checked').length;
		if (slotid!=''){
			if (members>0) {
				if( slotdetails[slotid]['AVAILAPPOINTMENT'] < members ) {
					example5("\nYou can book appointment for only "+slotdetails[slotid]['AVAILAPPOINTMENT']+" Members in selected slot. Please select slot again.", 'memberid');
					$('#timeslot').val('').attr("selected", "selected");
				}
			} else {
				example5("\nPlease select member.", 'memberid');
				$('#timeslot').val('').attr("selected", "selected");
			}
		}  
	});    
	function CreateMemberhtml(memberlist,planmembers) {
		var planmembersdetail= planmembers[0]['MEMBERSDETAILS']
		var memberlsithtml = '';	
		var foundmember = false;
		for (i=0; i<memberlist.length; i++) {
			var errorcode = planmembersdetail[i]['QC-ERROR'];
			var errorval = '';
			if (errorcode!=0){
				if (ptype!='customerlogin' && ptype!='hr'){
					for(j=0;Math.pow(2, j)<= errorcode;j++){	
						if (errorcode & Math.pow(2, j)) {
							errorval += ((errorval!='')? '|':'')+appointment_errorss[Math.pow(2, j)]; 
						}	
					}
					foundmember = true;
					memberlsithtml += 	'<tr><td  class="apperror" colspan="6" align="center">'+errorval+': '+memberlist[i]['QC-MEMNAME'] + '<hr>' +$.trim(planmembersdetail[i]['QC-DEBUG'],'|')+'</td></tr>';
				}
			} else {
				foundmember = true;
				memberlsithtml += '<tr>';
					memberlsithtml += '<td align="center">'+memberlist[i]['QC-GENDER']+'</td>';
					memberlsithtml += '<td align="center">'+memberlist[i]['QC-RELATIONNAME']+'</td>';
					memberlsithtml += '<td align="center">'+memberlist[i]['QC-MEMNAME']+'</td>';
					memberlsithtml += '<td align="center">'+memberlist[i]['QC-AGE']+'</td>';
					memberlsithtml += '<td align="center">'+memberlist[i]['QC-CLNTNM']+'</td>';
					memberlsithtml += '<td align="center"><input class="membercheckbox" type="checkbox" name="memeberids[]" value="'+memberlist[i]['QC-CLNTNM']+'" onchange = "checkMemberCheckedbox();"></td>';
				memberlsithtml += '</tr>';
			}
		}
		if (foundmember === false) {
			memberlsithtml += 	'<tr><td  class="apperror" colspan="6" align="center">Members not found.</td></tr>';
		}
		return memberlsithtml;
	}
	$('#AddAppointmentform').click (function(){
		validate_New_appointment();
	});
	$('#RescheduleAppointmentform').click (function(){
		validate_Reschedule_appointment();
	});
	
});
function checkMemberCheckedbox(){
  var cid =  $("#centerid").val();
  if (cid!='') {
	  $("#centerid").val(cid).change();
  }
}
function ChangeDateFormat( date, format) {
	var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ];
	var date    = new Date(date);                        	
	yr          = date.getFullYear();
	month       = date.getMonth();
	
	day         = date.getDate()  < 10 ? '0' + date.getDate()  : date.getDate();
	if (format != null) {
		//month   = month < 10 ? '0' + month : month;
		newDate = day + '-' + monthNames[month].slice(0, 3) + '-' + yr;
	} else {
		month   = parseInt(date.getMonth()) +1;
		month   = month < 10 ? '0' + month : month;
		newDate = yr + '/' + month + '/' + day;
	}
	return newDate;
}
function  validate_New_appointment()
{ 
	var policynumber = $.trim($("#policynumber").val());
	var companyid 	 = $.trim($("#companyid").val());
	var productid 	 = $.trim($("#productid").val());
	var planid 		 = $.trim($("#planid").val());
	var customerid 	 = $.trim($("#customerid").val());
	var empname 	 = $.trim($("#empname").val());
	var atLeastOneIsChecked = $('input[name="memeberids[]"]:checked').length;
	var email 		 = $.trim($("#email").val());
	var numbers 	 = $.trim($("#numbers").val());
	var caseid 	 	 = $.trim($("#cid").val());
	var stateid 	 = $.trim($("#stateid").val());
	var cityid 		 = $.trim($("#cityid").val());
	var centerid 	 = $.trim($("#centerid").val());
	var appointmentdate  = $.trim($("#appointmentdate").val());
	var timeslot 	 = $.trim($("#timeslot").val());
	var centername 	 = $.trim($("#center_autocomplete").val());
	
	if (companyid == '' && productid == '') {
		example5("\nEssential data is missing please try again.", 'policynumber');
		return false;
	}
	if (policynumber == '') {
		example5("\nPolicy Number can not be blank.", 'policynumber');
		return false;
	}
	if (productid == '' && customerid == '') {
		example5("\nCustomer id can not be blank.", 'customerid');
		return false;
	}
	if (planid == '') {
		example5("\nPlease Select Plan For Appointment.", 'planid');
		return false;
	}
	if (empname == ''){
		example5("\nPlease enter employee Name.", 'empname');
		return false;
	}
	
	if (caseid != '') {
		if ((isNaN(caseid))) {
			example5("\nCase id should be numeric.", 'cid');
			return false;
		}
	}
	if (email == ''){
		example5("\nPlease enter emailid.", 'email');
		return false;
	}
	if (email != '') {
		emailReg = /^([\w-'\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		if (!emailReg.test(email)) {
			example5('Please enter a valid Email address', 'email');
			return false;
		} 	
	}
	if (numbers == ''){
		 example5("\nPlease enter Mobile Number.", 'numbers');
		 return false;
	}
	intRegex = /[0-9 -()+]+$/;
	if (numbers != '') {
		if ((!intRegex.test(numbers))) {
			example5("\nMobile Number Should be Numeric.", 'numbers');
			return false;
		}
		if (numbers.length < 10 || numbers.length > 12) {
			example5("\nMobile Number Not Valid.", 'numbers');
			return false;
		}
	}
	
	if (atLeastOneIsChecked == 0 || atLeastOneIsChecked == null) {
		example5("\nPlease Select Member For Appointment.", 'memberc');
		return false;
	}
	if (stateid == ''){
		example5("\nPlease select a state.", 'stateid');
		return false;
	}
	if (cityid == ''){
		example5("\nPlease select a city.", 'cityid');
		return false;
	}
	if (centerid == ''){
		example5("\nPlease select a center.", 'centerid');
		return false;
	}
	if (centername == ''){
		example5("\nPlease select a center.", 'center_autocomplete');
		return false;
	}
	if (appointmentdate == ''){
		example5("\nPlease select a date for appointment.", 'appointmentdate');
		return false;
	}
	if (timeslot == ''){
		example5("\nPlease select a timeslot for appointment.", 'timeslot1');
		return false;
	}
	var type = 'SAVEAPPOINTMENTDATA';
	var data = $('#appointmentForm').serializeArray();
	//var centerid = $('#centerid').val();//added by sumit to send the customer id on page
	data.push({name: 'type', value: type});
	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: 'ajax/GetAppointmentDetails.php',
		async: true,
		data: data, 
		beforeSend: function () {
			$("#AddAppointmentform").hide();
			//$("#apploader").show();
			$.LoadingOverlay('show');
			//$(".successdiv").html('<div  style="text-align:center; background-color: blanchedalmond;"><img src="images/loading-new.gif"></div>');
		},
		success: function (res) {	
			if ( res.sflag == 1) {
				if (ptype == 'customerlogin') {
					location = "existing_appointments.php?message="+res.success+"&tab=ZXhpc3RpbmdhcHA=";
				} else {
					$(".errordiv").html('');
					$(".successdiv").html(res.success);
					$("#planid").html('<option value="">Select Plan</option>');	
					$('#memberlist').html('<td class="apperror" colspan="6" align="center" class="col-border-1">Select Plan first.</td>');
					$('#customerid').val('');
					$( "#resetform" ).trigger( "click" );
				}
			} else {
				if (ptype == 'customerlogin') {
					//location = "existing_appointments.php?error=Please try again.";
					//$(".errordiv").html('<li>Appointment not booked. Please try again or contact to customer care.</li>');
					$(".errordiv").html(res.error);
				} else {
					$(".successdiv").html('');
					$(".errordiv").html(res.error);
				}
			}
			//$("#apploader").hide();
			$.LoadingOverlay('hide');
			$("#AddAppointmentform").show();
		}
	});
	$('html,body').animate({
		scrollTop: $(".rightBody_head").offset().top - 500},
	'slow');
}

function RescheduleAppointment(appointmentid, type){
	$.ajax({
		type: 'POST',
		dataType: "json",
		url: 'ajax/GetAppointmentDetails.php',
		data: {'id': appointmentid, 'type': 'VIEWAPPOINTMENTDETAIL', 'subtype': type},
		async: true,
		beforeSend: function () {
			//$("#AddAppointmentform").hide();
			//$(".loadingimageedit").show();
			$.LoadingOverlay('show');
		},
		success: function (response) {			
			if ( response.sflag == 1) {
				var appdetails = response.appointmentdetail;
				var msdetails = response.mscrmdetail;
				//$(".loadingimageedit").hide();
				$.LoadingOverlay('hide');
				var productid= '';
				var Planid = '';
				var policyid = '';
				var companyid = '';
				var Planid = appdetails['HEALTHCHECKUPID'];
				if (type == 'corporate') {	
					/*var response = getcompanyandpolicydetails(appdetails['POLICYNUMBER'], appdetails['COMPANYID']);
					//console.log(response);
					$('.comp_name').html(response['COMPANYNAME']);
					$('#empid_text').html(appdetails['EMPLOYEEID']);
					policyid  =  response['POLICYID'];
					companyid =  response['COMPANYID'];*/
				} else {
					productid = appdetails['PRODUCTID'];
				}
				$('#id').val(appdetails['APPOINTMENTID']);
				$('#app_box_id').val(appdetails['APPOINTMENTID']);
				$('#cityidres').val(appdetails['CITYID']);
				$('#centername_text').html(appdetails['CENTERNAME']);
				$('#plan_text').html(appdetails['HEALTHCHECKUPPLAN']);
				$('#empname_text').html(appdetails['FIRSTNAME']);
				$('#age_text').html(appdetails['DOB']);
				$('#email_text').html(appdetails['EMAILID']);
				$('#customerid_text').html(appdetails['CUSTOMERID']);
				$('#mobile_text').html(appdetails['MOBILENO']);
				$('#emplocation_text').html(appdetails['EMPLOYEELOCATION']);
				$('#unique_text').html(appdetails['APPOINTMENTID']);
				$('#relation_text').html(appdetails['MEMBERRELATION']);
				$('#gender_text').html(appdetails['GENDER']);
				$("#status").val(appdetails['STATUS']).change();
				$("#displaydate").html(appdetails['DATE1'] + ' at ' + appdetails['TIMESLOT1'] + ' '+ appdetails['TIMESLOT1AM']);
				$("#appdate_text").html(appdetails['DATE1'] + ' at ' + appdetails['TIMESLOT1'] + ' '+ appdetails['TIMESLOT1AM']);
				$('#mscrm_casetyperes').val(msdetails['CRM_CASETYPE']);
				$('#zentrix_session_idres').val(msdetails['SESSION_ID']);
				$("#ms_originres").val(msdetails['CASE_ORIGIN']).change();
				$("#mscrm_req_byres").val(msdetails['REQUESTED_BY']).change();					
				$('#mscrm_req_by_idres').val(msdetails['REQUESTED_BY_ID']);
				$('#mscrm_app_numres').val(msdetails['APPLICATION_NO']);
				$('#mscrm_srres').val(msdetails['CRM_SR']);
				$('#mscrm_subsrres').val(msdetails['CRM_SUBSR']);
				$.ajax({
					type: 'POST',
					dataType: 'json',
					url: 'ajax/GetAppointmentDetails.php',
					async: false,
					data: {'type': 'GETPLANDETAILS', 'policyid': policyid, 'planid': Planid + '||R', 'companyid': companyid, 'productid': productid},
					beforeSend: function () {
						$("#loadingimage1").show();
						$.LoadingOverlay('show');
					},
					success: function (obj) {
						//ChangeStateCityCenter(obj);
						//console.log(obj);
						if (obj.error == '') {
							centerarray = obj.centerlist;
							centerlist = centerarray.filter(function (centerarray) { return centerarray.CITYID == appdetails['CITYID'] });
							for (var i=0;i<centerlist.length;i++) {
							  var option=$('<option value="'+centerlist[i]['CENTERID']+'"></option>').text(centerlist[i]['CENTERNAME']+ ' '+centerlist[i]['ADDRESS1']+ ' '+centerlist[i]['ADDRESS2']);
							  $('#centeridres').append(option);
							}
							$("#centeridres").val(appdetails['CENTERID']).change();	
						} else {
							$("#planmsg").text(obj.error);
							$("#planmsg").show();	
						}
						$("#loadingimage1").hide();  
						$.LoadingOverlay('hide');
					}
				});
			} else {
				$(".errordiv").html(response.error);
			}
		}
	});
	
}	
function closeboxreset(num) {
	var id = document.getElementById('app_box_id').value;
	document.getElementById('filter').style.display='none';
	document.getElementById('box'+num).style.display='none';
	document.getElementById("RescheduleAppointmentdata").reset();
	document.getElementById('action_'+id).selectedIndex = '';
}	

function validate_Reschedule_appointment(){
	var id 					=	$.trim($("#id").val());
	var centeridres 		=	$.trim($("#centeridres").val());
	var rescheduledate 		=	$.trim($("#rescheduledate").val());
	var timeslotres 		=	$.trim($("#timeslotres").val());
	var reschedulestatus 	=	$.trim($("#status").val());
	if (id=='') {
		example5("\nEssential data is missing. Please try again.", 'centeridres');
		return false;
	}
	if (centeridres == ''){
		example5("\nPlease select a center.", 'centeridres');
		return false;
	}
	if (rescheduledate == ''){
		example5("\nPlease select a reschedule appointment date.", 'rescheduledate');
		return false;
	}
	if (timeslotres == ''){
		example5("\nPlease select a reschedule time slot.", 'timeslotres');
		return false;
	}
	var type = 'RESCHEDULEAPPOINTMENTDATA';
	var data = $('#RescheduleAppointmentdata').serializeArray();	
	data.push({name: 'type', value: type});
	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: 'ajax/GetAppointmentDetails.php',
		data: data, 
		beforeSend: function () {
			$.LoadingOverlay('show');
			$("#RescheduleAppointmentform").hide();
			//$("#loadingimage3").show();
		},
		success: function (res) {	
			if ( res.sflag == 1) {
				$(".errordiv").html('');
				$("#policynumber").trigger('keyup');
				$(".successdiv").html(res.success);
				$( "#cancellbutton" ).trigger( "click" );
                location.reload(); 
			} else {
				$(".successdiv").html('');
				$(".errordiv").html(res.error);
			}
			$('html,body').animate({
				scrollTop: $(".rightBody_head").offset().top - 100},
			'slow');
			$("#RescheduleAppointmentform").show();
			//$("#loadingimage3").hide();
			$.LoadingOverlay('hide');
		}
	});
}
function appointmentActionChange(currentaction, appointmentid, type, viewform, bnum) 
{
	var reschedule = true;
	var filelink = true;
	if (type == 'retail') {
		var actiontype = 'CHANGESTATUS';
		var uploadtype = 'UPLOADRETAILPOSTREPORT';
	} else {
		var actiontype = 'CORPORATECHANGESTATUS';
		var uploadtype = 'UPLOADCORPORATEPOSTREPORT';
		reschedule = false;
		filelink = false;
	}
    var statuslist  = eval('appointment_status.'+currentaction.toString());
    if (currentaction == 'RESCHEDULE') {	
       if (confirm("Are you sure want to Reschedule appointment?")) {
			if (viewform == 'viewform'){
				closeboxreset(bnum);
			}
            openbox5('', 2);
            RescheduleAppointment(appointmentid,type);	
            $('html,body').animate({
                scrollTop: $(".rightBody_head").offset().top - 500},
            'slow');
        } else {
                $("#action_" + appointmentid).val('');
        }
    } else if (currentaction == 'VIEW') {	
        if (confirm("Are you sure want to View appointment Details?")) {
			if (viewform == 'viewform'){
				closeboxreset(bnum);
			}
            openbox5('', 3);
            ViewAppointmentDetails(appointmentid,type);	
            $('html,body').animate({
                scrollTop: $(".rightBody_head").offset().top - 500},
            'slow');
        } else {
            $("#action_" + appointmentid).val('');
        }
    } else if (currentaction == 'CONFIRM' || currentaction == 'CANCELLED') {
        if (confirm("Are you sure want to change status?")) {
			if (viewform == 'viewform'){
				closeboxreset(bnum);
			}
            $.ajax({
                type: 'POST',
                dataType: "json",
				async:true,
                url: 'change_appointment_status.php',
                data: {'status_id': appointmentid, 'status': currentaction, 'type': actiontype},
				beforeSend: function () {
					$.LoadingOverlay('show');
				},
                success: function (response) { 
                    $('#changestatus_' + appointmentid).html(response.status);
                    var options = '';
                    $("#action_" + appointmentid).html('<option value="">Action</option>');
                    $.each(statuslist, function () {					
                        $.each(this, function (name, value) {
							if (value!='Re-Send Authorization Letter') {
								if (name != 'RESCHEDULE') {
									var option = $('<option value="'+name+'"></option>').text(value);
									$("#action_" + appointmentid).append(option);
								} else if (reschedule === true){
									var option = $('<option value="'+name+'"></option>').text(value);
									$("#action_" + appointmentid).append(option);
								}
							}
                        });
                    });
					$.LoadingOverlay('hide');					
                }
            });
        } else {
            $("#action_" + appointmentid).val('');
        }	
    } else if (currentaction == 'DONE' || currentaction == 'UPLOADREPORT') {
		var cmsg = "Are you sure want to change status?";
		if (currentaction == 'UPLOADREPORT') {
			var cmsg = "Are you sure want to upload reports?";
		}
        if (confirm(cmsg)) {
			if (viewform == 'viewform'){
				closeboxreset(bnum);
			}
			$("#markasdoneApp").val(0);
			if (currentaction == 'DONE') {
				 $("#markasdoneApp").val(1);
			}
            openbox5('', 1);
			$('html,body').animate({
                scrollTop: $(".rightBody_head").offset().top - 500},
            'slow');
            $.ajax({
                type: 'POST',
                dataType: "json",
                url: 'change_appointment_status.php',
                data: {'status_id': appointmentid, 'type': uploadtype},
				beforeSend: function () {
					$.LoadingOverlay('show');
				},
                success: function (response) {
					$('#changestatus_' + appointmentid).html(response.status);
					$('#AppointmentId').val(response.AppointmentId);
                    $('.bloodReportfile').html(response.bloodReportfile);
                    $('.urineReportfile').html(response.urineReportfile);
                    $('.Reportfile').html(response.Reportfile);
                    $('#updatereportfile').val(response.updatereportfile);
                    $('#bloodupdatereportfile').val(response.bloodupdatereportfile);
                    $('#urineupdatereportfile').val(response.urineupdatereportfile);
                    $('.EmpfirstName').html(response.EmpfirstName);
                    $('.compId').val(response.companyId);
					if (filelink) {	
						document.getElementById("bloodReportfile").href=response.url_blood_report_file;
						document.getElementById("urineReportfile").href=response.url_urine_report_file;
						document.getElementById("Reportfile").href=response.url_other_report_file;	
					}
                }
            });
			$.LoadingOverlay('hide');	
        } else {
            $("#action_" + appointmentid).val('');
        }
    } else {
		$('.cancelbtn').trigger('click');	
		$("#action_" + appointmentid).val('');
	}
}
function ViewAppointmentDetails(appointmentid,subtype) {
	$('#ViewUpdateAppointmentform').hide();
	$.ajax({
		type: 'POST',
		dataType: "json",
		url: 'ajax/GetAppointmentDetails.php',
		data: {'id': appointmentid, 'type': 'VIEWAPPOINTMENTDETAIL', 'extraparam': 'APPHISTORY', 'subtype': subtype},
		beforeSend: function () {
			$.LoadingOverlay('show');
		},
		success: function (response) {
			var appdetail = response.appointmentdetail;			
			if (subtype == 'retail') {
				$('.comp_name').html(appdetail.PRODUCTNAME);
				$('.productname-view').html(appdetail.PRODUCTNAME);
				$('.caseid-view').html(appdetail.CASEID);
			} else {
				$('.comp_name').html(response.companyname);
				$('#productname-lable').html('Company Name');
				$('.productname-view').html(response.companyname);
				$('.caseid-view-div').hide();
			}
			$('#app_id').val(appdetail.APPOINTMENTID);
			$('.policynumber-view').html(appdetail.POLICYNUMBER);
			$('.healthPlan-view').html(appdetail.HEALTHCHECKUPPLAN);
			$('.customerid-view').html(appdetail.CUSTOMERID);
			$('.name-view').html(appdetail.FIRSTNAME);
			$('.age-view').html(appdetail.DOB);
			$('.email-view').html(appdetail.EMAILID);
			$('.appointmentid-view').html(appdetail.APPOINTMENTID);
			$('#app_box_id').val(appdetail.APPOINTMENTID);
			$('.location-view').html(appdetail.EMPLOYEELOCATION);
			$('.centername-view').html(appdetail.CENTERNAME);
			$('.appointment-date-view').html(appdetail.DATE1);
			$('.appointment-time-view').html(appdetail.TIMESLOT1+' '+appdetail.TIMESLOT1AM);
			if (ptype == 'mscrm') {
				var option = $('<option value="'+appdetail.STATUS+'"></option>').text(appdetail.STATUS);
				$("#status_update").append(option);
			} else { 
				var status = appdetail.STATUS;
				if (appdetail.RESCHEDULED == 'YES') {
					status = 'RESCHEDULE';
				}	
				$("#status_update").html('');
				$("#statusname").text('');
				var statuslist  = eval('appointment_status.'+status.toString());
				$("#statusname").text(status);
				var option = $('<option value=""></option>').text('Select Status');
				$("#status_update").append(option);
				var showbutton = false;
				$.each(statuslist, function () {					
					$.each(this, function (name, value) {
						if (name!='VIEW') {
							if (subtype == 'retail') {
								var option = $('<option value="'+name+'"></option>').text(value);
								$("#status_update").append(option);
							} else {
								if (name!='RESCHEDULE') {
									var option = $('<option value="'+name+'"></option>').text(value);
									$("#status_update").append(option);
								}
							}
							
						}
					});
				});
			}
			var apphistory = response.apphistory;
			strrecord = '';
			if (apphistory !=''){
				for (i=0; i< apphistory.length;i++){
					strrecord += '<tr>';
						strrecord += '<td>'+apphistory[i]['ID']+'</td>';
						strrecord += '<td>'+apphistory[i]['CREATEDON']+'</td>';
						strrecord += '<td>'+apphistory[i]['CREATEDBY']+'</td>';
						strrecord += '<td>'+apphistory[i]['STATUS']+'</td>';
						strrecord += '<td>'+apphistory[i]['REPORTSTATUS']+'</td>';
					strrecord += '</tr>';
				}
			} else {
					strrecord = '<tr><td colspan="6" align="center">No History Record Found..</td></tr>';
			}
			$("#appointmentHistory").html(strrecord);
			$.LoadingOverlay('hide');
		}
    });
}
function UploadHckReports() {
    var uploadFile 				= $.trim($("#upload").val());
    var blooduploadFile 		= $.trim($("#bloodupload").val());
    var urineuploadFile 		= $.trim($("#urineupload").val());
    var updatereportfile 		= $.trim($("#updatereportfile").val());
    var bloodupdatereportfile 	= $.trim($("#bloodupdatereportfile").val());
    var urineupdatereportfile 	= $.trim($("#urineupdatereportfile").val());
    if (blooduploadFile == '' && bloodupdatereportfile == '' && urineuploadFile == '' && urineupdatereportfile == '' && uploadFile == '' && updatereportfile == '') {
        example5("\nPlease Select at least one file.", "blooduploadFile");
        return false;
    }
	var data = new FormData($('#uploadReportForm')[0]);
	$.ajax({
		type: 'POST',
		dataType: 'json',
		url: 'ajax/GetAppointmentDetails.php',
		data: data,
		aync: true,
		processData: false,
		contentType: false,
		beforeSend: function () {
			$.LoadingOverlay('show');
		},
		success: function (resp) {
			if (resp.status == 'ERROR'){
				$('#errordivfile').text('Please Try again.');
			} else {
				//$('#successdivfile').text('Uploaded Successfuly.');
				//$('#successdivfile').show();
				location=''; 
			}
			$.LoadingOverlay('hide');					
		}
	});
}
function CheckSubmitButton(val){
	if (val!='') {
		$('#ViewUpdateAppointmentform').show();
	} else {
		$('#ViewUpdateAppointmentform').hide();
	}
}
<?php $_SESSION['appointmentCSRF']=md5(rand(1, 99999999));?>
$.ajaxSetup({
headers: {  'appointmentCSRF' : '<?php echo $_SESSION['appointmentCSRF']; ?>' }
});
</script>