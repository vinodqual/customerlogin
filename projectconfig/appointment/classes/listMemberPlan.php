<?php
/**********************************************************************************************
***********************************************************************************************
***********************************************************************************************
***********************************************************************************************
**CREATED ON : 4Feb, 2017**********************************************************************
**CREATED BY : Chandan Kumar Singh*************************************************************
**COMPANY	 : Qualtech Consultants Pvt Ltd****************************************************
**DESCRIPTION: This pange is used for list all healthchec-up plans*****************************
***********************************************************************************************
***********************************************************************************************
***********************************************************************************************
***********************************************************************************************
***********************************************************************************************/
error_reporting(E_ALL && ~E_NOTICE);
include_once(__DIR__.'/../../../religarehrcrm/api/api.php');

class listMemberPlan
{
	public function listMemberPlan()
	{
		global $conn, $relationArray;
		$this->dbConnect		=	$conn;
		$this->appointmentDate	=	array();
		$this->appointmentId	=	array();
		$this->relationArr		=	$relationArray;
		$this->isSystemLogedIn	=	(isset($_SESSION['userName']) && (strtolower($_SESSION['userName'])=='system'))?1:0;
	}
	private function fetchDataAsAccoc($query, $autocommit="1")
	{
		$parseResource 			=	oci_parse($this->dbConnect, $query);
		if($autocommit=="1")
			oci_execute($parseResource);
		else
			oci_execute($parseResource, OCI_NO_AUTO_COMMIT);
		$fetchResource 			=	oci_fetch_assoc($parseResource);
		return $fetchResource;
	}
	private function fetchDataAsResource($query, $autocommit="1")
	{
		$parseResource 			=	oci_parse($this->dbConnect, $query);
		if($autocommit=="1")
			oci_execute($parseResource);
		else
			oci_execute($parseResource, OCI_NO_AUTO_COMMIT);
		return $parseResource;
	}
	private function getWebServiceResponse($query, $webServiceType)
	{
		$response		=	"";
		switch($webServiceType)
		{
			case	"1"	:	$response		=	getRenewalMemberDetails($query, true);break;
			
			case	"2"	:	$response		=	getPolicyEnquiry($query, true);break;
			
			default		:	/*Some Error Message*/$response		=	"default";break;
		}
		return $response;
	}
	private function isAppointmentAvailable($planId,$companyId, $empId, $policyNumber, $custId, $type, $policyStartDate='', $policyEndDate='') {
		$appointmentCheckMemberArr	=	array();
		if($type=='CORPORATE')
		{
			$i							=	0;
			$appointmentCheck_q			=	"SELECT APPOINTMENTID,FIRSTNAME, DATE1 FROM CRMCOMPANYPOSTAPPOINTMENT where HEALTHCHECKUPID='$planId' AND  EMPLOYEEID='$empId' AND  COMPANYID='$companyId'  AND STATUS !='CANCELLED'";
			$appointmentCheck_f			=	$this->fetchDataAsResource($appointmentCheck_q, 0);
			while($appointmentCheck_v 	= oci_fetch_assoc($appointmentCheck_f))
			{
				$appointmentCheckMemberArr[$i]=	stripslashes(strtolower($appointmentCheck_v['FIRSTNAME']));
				$this->appointmentDate[strtolower($appointmentCheck_v['FIRSTNAME'])]	=	$appointmentCheck_v['DATE1'];
				$this->appointmentId[strtolower($appointmentCheck_v['FIRSTNAME'])]	=	$appointmentCheck_v['APPOINTMENTID'];
				$i++;
			}
		}else{
			$appointmentCheck_q				=	"SELECT APPOINTMENTID,DATE1,CREATEDON,FIRSTNAME FROM CRMRETAILPOSTAPPOINTMENT WHERE POLICYNUMBER='$policyNumber'  AND HEALTHCHECKUPID='$planId' AND STATUS !='CANCELLED' AND RESCHEDULED is null";
			$appointmentCheck_f				=	$this->fetchDataAsResource($appointmentCheck_q, 0);//echo COUNT($appointmentCheck_f);die;
			if(COUNT($appointmentCheck_f)>0) {
				//CHECK FOR APPOINTMENT IS ALREADY BOOKED OR NOT
				//$bookingStartDate			=	date('d-m', strtotime($policyStartDate)).'-'.date('Y', time());
				$policystartDate 	= date('d-m-Y', strtotime($policyStartDate));
				$policyEndDate 		= date('d-m-Y', strtotime($policyEndDate));
				$date_diff = strtotime($policyEndDate) - strtotime($policystartDate);
				$date_diff = ceil(($date_diff)/(60*60*24*365));
				$policystartTime 	= date('d-m-Y', strtotime($policyStartDate));
				$datenew 			= date('d-m-Y', strtotime('+1 year', strtotime($policystartTime)));
				$policyEndTime 		= date('d-m-Y', strtotime('-1 day', strtotime($datenew)));
				if ($date_diff > 1 ) {
					///For Multiyear Policy
					$currentDate = date('d-m-Y');
					for ($d=0;$d<$date_diff;$d++) {
						//echo $currentDate."=====".$policystartTime."=====".$policyEndTime."<br />";
						if (strtotime($policystartTime)<=strtotime($currentDate) && strtotime($policyEndTime)>=strtotime($currentDate)) {
							break;
						} else {
							$policystartTime 			= date('d-m-Y', strtotime('+1 year', strtotime($policystartTime)));
							$newdate 					= date('d-m-Y', strtotime('+1 year', strtotime($policystartTime)));
							$policyEndTime 				= date('d-m-Y', strtotime('-1 day', strtotime($newdate)));
						}
					}	
				}
				$i							=	0;
				while($appointmentCheck_v 	= oci_fetch_assoc($appointmentCheck_f))
				{
					$bookeddate = '';
					$bookeddate = date('d-m-Y',strtotime($appointmentCheck_v['CREATEDON']));
					//echo $bookeddate."=====".$policystartTime."=====".$policyEndTime."<br />";
					if (strtotime($bookeddate) >= strtotime($policystartTime) && strtotime($bookeddate) <= strtotime($policyEndTime)) {
						$appointmentCheckMemberArr[$i]=	stripslashes(strtolower($appointmentCheck_v['FIRSTNAME']));
						$this->appointmentDate[strtolower($appointmentCheck_v['FIRSTNAME'])]	=	$appointmentCheck_v['DATE1'];
						$this->appointmentId[strtolower($appointmentCheck_v['FIRSTNAME'])]	=	$appointmentCheck_v['APPOINTMENTID'];
						$i++;
					}
				}
			}
		}
		return $appointmentCheckMemberArr;
	}
	private function isAplicableForRelation($planId, $jsonVal, $type)
	{
		$aplicableForArr			=	json_decode($jsonVal, true);
		$applicableForArr			=	array();
		if($type=='FREE')
		{
			$aplicableForStr 			= 	implode(',', $aplicableForArr);
			$applicableFor_q			=	"SELECT TITLE FROM CRMCORPMEMBERTYPE WHERE MEMBERTYPEID IN($aplicableForStr) AND STATUS='ACTIVE'";
			$applicableFor_resource		=	$this->fetchDataAsResource($applicableFor_q, 0);
			$i							=	0;
			while($applicableFor_fetchArr=	oci_fetch_assoc($applicableFor_resource))
			{
				$applicableFor_title 	= 	strtolower($applicableFor_fetchArr['TITLE']);
                $applicableFor_title 	= 	preg_replace('/\s+/', '', $applicableFor_title);
				if ($applicableFor_title== 'self') {
                    $applicableFor_title= 'primarymember';
                }
				$applicableForArr[$i]	=	$applicableFor_title;
				$i++;
			}
		}else{
			
			foreach ($aplicableForArr as $aplicableForArrkey => $aplicableForArrValues)
			{
				$applicableFor_q		=	"SELECT TITLE FROM CRMCORPMEMBERTYPE WHERE MEMBERTYPEID ='$aplicableForArrkey'";
				$applicableFor_fetchArr	=	$this->fetchDataAsAccoc($applicableFor_q, 0);
				$applicableFor_title 	= 	strtolower($applicableFor_fetchArr['TITLE']);
                $applicableFor_title 	= 	preg_replace('/\s+/', '', $applicableFor_title);
				if ($applicableFor_title == 'self')
				{
                    $applicableFor_title = 'primarymember';
                }
				$applicableForArr[$i]	=	$applicableFor_title;
				$i++;
			}
		}
		return $applicableForArr;
	}
	public function getMemberDetail($policyNo, $customerId, $memberDetailArr=array(), $productCode, $planId='', $isCustomerLoginCallingMe=false,$policyStartDate = '',$policyEndDate='')
	{
		
		$query['policyNo']		=	$policyNo;
		$query['CLNTNUM']	 	=	$customerId;
		$query['CHDRNUM'] 		=	$policyNo; 
		//$policyStartDate		=	'';
		//$policyEndDate			=	'';
		if($policyStartDate == '' || $policyEndDate=='') {
			$policyDetail		=	$this->getWebServiceResponse($query, 1);
			//echo "<pre>";print_r($policyDetail);die;
			$policyStartDate 	= 	$policyDetail[0]['policyCommencementDt']['#text'];
			$policyEndDate		=	$policyDetail[0]['policyMaturityDt']['#text'];
		}
		$userType				=	'';
		$returnMemberArr		=	true;
		//$policyDetail			=	$this->getWebServiceResponse($query, 1);
		if(count($memberDetailArr)<=0){
			$memberDetailArr	=	$this->getWebServiceResponse($query, 2);
			$policyStartDate	=	$memberDetailArr['POLICYSTARTDATE'];
			$policyEndDate		=	$memberDetailArr['POLICYENDDATE'];
			$memberDetailArr	=	$memberDetailArr['dataArray'];
			//echo $policyStartDate."===".$policyEndDate;die;
			$currentdate = strtotime(date("Y-m-d"));
			$policyExpiryPlusOneday = strtotime("+1 day", strtotime($policyEndDate));
			if ($currentdate <= $policyExpiryPlusOneday) {
				//Do Nothing
				/*if ($currentdate < strtotime($policyStartDate)) {
					return array('ERROR'=>"You can't schedule appointment for this policy,policy start-Date is " . date('d-m-Y',trim(strtotime($policyStartDate))));
				}*/
			} else {
				return array('ERROR'=>"Your policy is expired.Please contact customer care !");
			}	
		} else {
			$returnMemberArr		=	false;
		}
		if(isset($memberDetailArr[0]['BGEN-CLNTNM']['#text']) && trim($memberDetailArr[0]['BGEN-CLNTNM']['#text'])=='')
		{
			return array('ERROR'=>"Client Number Is Invalid.");
		}elseif(!isset($memberDetailArr[0]['BGEN-CLNTNM']['#text']) )
		{
			return array('ERROR'=>"Server Is Down. Please Try After Some Time.");
		}
		if($productCode!="")
		{
			$userType			=	'Retail';
			return $this->getRetailPlanDetail($policyNo,$customerId, $memberDetailArr, $productCode, $policyStartDate, $policyEndDate, $planId, $returnMemberArr, $isCustomerLoginCallingMe);
		}else{
			$userType			=	'Corporate';
			return $this->getCorporatePlanDetail($policyNo,$customerId, $memberDetailArr, $planId, $returnMemberArr, $isCustomerLoginCallingMe);
		}
		//echo "<pre>";print_r($memberDetailArr);die;//print_r($memberDetail[0]['customerId']['#text']);		
	}
	private function getCorporatePlanDetail($policyNo, $customerId, $memberDetailArr, $planId, $returnMemberArr, $isCustomerLoginCallingMe)
	{//GETTING PLAN DETAIL AND MEMBER DETAIL FOR CORPORATE USER
		global $relationArray;
		$planArr				=	array();
		$policyDetail_quer		=	"SELECT * FROM CRMPOLICY where POLICYNUMBER='$policyNo' AND STATUS='ACTIVE'";
		$policyDetail_fetchArr	=	$this->fetchDataAsAccoc($policyDetail_quer, 0);//print_r($policyDetail_fetchArr);
		$policyId				=	$policyDetail_fetchArr['POLICYID'];
		$companyId				=	$policyDetail_fetchArr['COMPANYID'];
		$policyStartDate		=	$policyDetail_fetchArr['POLICYSTARTDATE'];
		$policyEndDate			=	$policyDetail_fetchArr['POLICYENDDATE'];
		$planIdCond				=	($planId)?"AND CCPP.PLANID='$planId'":"";
		$planDetail_query		=	"SELECT CCPP.*,CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANNAME AS PLANNAME2, CRMCORPHEALTHCHECKUP.UPLOADPDF AS UPLOADPDF2 FROM CRMCOMPANYHEALTHCHECKUPPLAN CCPP LEFT JOIN CRMCORPHEALTHCHECKUP ON CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CCPP.PLANNAMEID where CCPP.POLICYID='$policyId' AND (((CCPP.PAYMENTOPTIONS='FREE' OR CCPP.PLANTYPE='LIMITED') AND CCPP.STATUS='ACTIVE') OR CCPP.PAYMENTOPTIONS='BUY') $planIdCond ";//remove  (and planid='3442'(BUY)) (and planid='5643'(FREE))
		$planDetail_resource	=	$this->fetchDataAsResource($planDetail_query, 0);
		$i						=	0;
		if($returnMemberArr)
		{
			$memberListArr			=	array();
			$m						=	0;
			foreach($memberDetailArr as $memberDetailVal)
			{
				$memberListArr[$m]['QC-MEMNAME']	=	$memberDetailVal['BGEN-MEMNAME']['#text'];
				$memberListArr[$m]['QC-EMPNO']		=	$memberDetailVal['BGEN-EMPNO']['#text'];
				$memberListArr[$m]['QC-DPNTTYP']	=	$memberDetailVal['BGEN-DPNTTYP']['#text'];
				$memberListArr[$m]['QC-RELATIONNAME']=	ucwords($relationArray[$memberDetailVal['BGEN-DPNTTYP']['#text']]);
				$memberListArr[$m]['QC-PLANNO']		=	$memberDetailVal['BGEN-PLANNO']['#text'];
				$memberListArr[$m]['QC-AGE']		=	$memberDetailVal['BGEN-AGE']['#text'];
				$memberListArr[$m]['QC-DOB']		=	$memberDetailVal['BGEN-DOB']['#text'];
				$memberListArr[$m]['QC-GENDER']		=	$memberDetailVal['BGEN-GENDER']['#text'];
				$memberListArr[$m]['QC-EMAIL']		=	$memberDetailVal['BGEN-EMAIL']['#text'];
				$memberListArr[$m]['QC-CLNTNM']		=	$memberDetailVal['BGEN-CLNTNM']['#text'];
				//$memberListArr[$m]['QC-STDCODE01']	=	$memberDetailVal['BGEN-STDCODE01']['#text'];
				$memberListArr[$m]['QC-CLTPHONE']	=	$memberDetailVal['BGEN-CLTPHONE']['#text'];
				//$memberListArr[$m]['QC-STDCODE02']	=	$memberDetailVal['BGEN-STDCODE02']['#text'];
				$memberListArr[$m]['QC-CLTPHONE02']	=	$memberDetailVal['BGEN-CLTPHONE02']['#text'];
				//$memberListArr[$m]['QC-STDCODE03']	=	$memberDetailVal['BGEN-STDCODE03']['#text'];
				$memberListArr[$m]['QC-CLTPHONE03']	=	$memberDetailVal['BGEN-CLTPHONE03']['#text'];
				$memberListArr[$m]['QC-ZBALCURR']	=	$memberDetailVal['BGEN-ZBALCURR']['#text'];
				$m++;
			}
			$planArr[0]['MEMBERS']	=	$memberListArr;
			$i++;
		}
		
		while($planDetail_fetchArr=	oci_fetch_assoc($planDetail_resource))
		{
			$AllowForCustomerSupport = false;
			$switchCond					=	($planDetail_fetchArr['PLANTYPE']=='LIMITED')?$planDetail_fetchArr['PLANTYPE']:$planDetail_fetchArr['PAYMENTOPTIONS'];
			$planArr[$i]				=	$planDetail_fetchArr;
			$responseArr				=	array();
			$planArr['UNAPLICABLEPLAN']	=	0;
			$isAllMembersAreUnAplicable	=	0;
			switch($switchCond)
			{
				CASE	"FREE"		:	
										$planArr[$i]['PLANTYPE']	=	"FREE";
										$j							=	0;
										$applicableForArr			=	$this->isAplicableForRelation($planArr[$i]['PLANNAMEID'], $planArr[$i]['FREEMAPPEDMEMBERTYPEIDS'], 'FREE');
										$appointmentCheckMemberArr	=	$this->isAppointmentAvailable($planArr[$i]['PLANID'],$planArr[$i]['COMPANYID'], $memberDetailArr[$j]['BGEN-EMPNO']['#text'], $policyNo, $customerId, 'CORPORATE');
										$getSi_query				=	"SELECT max(CAST(SI AS integer)) MA, min(CAST(SI AS integer)) MI FROM CRMSI WHERE SIID in(".$planArr[$i]['SI'].")";
										$getSi_fetch				=	$this->fetchDataAsAccoc($getSi_query, 0);//echo '<pre>';print_r($appointmentCheckMemberArr);die;
										while($j<count($memberDetailArr))
										{
											$allowedArr				=	0;
											$debugStr				=	'';
											if(trim($memberDetailArr[$j]['BGEN-GENDER']['#text'])==trim($planArr[$i]['GENDER'][0]) || strtoupper(trim($planArr[$i]['GENDER']))=='BOTH')
											{
												//do nothing
											}else{
												$allowedArr			=	1;//FOR GENDER MISMATCH
												$debugStr			.=	' | GENDER MISMATCH';
											}
											if(trim($memberDetailArr[$j]['BGEN-GENDER']['#text'])=='' )
											{
												$allowedArr			=	1;//FOR GENDER MISMATCH
												$debugStr			.=	' | GENDER MISMATCH';
											}
											$ageArr					=	explode(',', $planArr[$i]['AGEGROUPID']);
											$memberAge				=	($memberDetailArr[$j]['BGEN-AGE']['#text'][1].$memberDetailArr[$j]['BGEN-AGE']['#text'][2]);
											if(!in_array($memberAge, $ageArr) || ($memberAge=='' || $memberAge=="0"))
											{
												$allowedArr			=	$allowedArr	+	2;//FOR AGE MISMATCH
												$debugStr			.=	' | AGE MISMATCH---Member Age is '.$memberAge;
											}
											if ((($getSi_fetch["MA"] + 1) <= (int)$memberDetailArr[$j]['BGEN-ZBALCURR']['#text'] && ($getSi_fetch["MI"] - 1) >= (int)$memberDetailArr[$j]['BGEN-ZBALCURR']['#text']) || ((int)$memberDetailArr[$j]['BGEN-ZBALCURR']['#text'] == "0" || (int)$memberDetailArr[$j]['BGEN-ZBALCURR']['#text']==''))
											{
												$allowedArr			=	$allowedArr	+	4;//FOR SI MISMATCH
												$debugStr			.=	' | SI MISMATCH---Member SI is '.$memberDetailArr[$j]['BGEN-ZBALCURR']['#text'];
											}
											if(!in_array(strtolower(str_replace(' ', '', $this->relationArr[$memberDetailArr[$j]['BGEN-DPNTTYP']['#text']])), $applicableForArr))
											{
												$allowedArr			=	8;
												$debugStr			.=	' | RELATIONSHIP MISMATCH---Member Relation is '.$this->relationArr[$memberDetailArr[$j]['BGEN-DPNTTYP']['#text']];
											} else {
												$AllowForCustomerSupport = true;
											}
											if(in_array(strtolower($memberDetailArr[$j]['BGEN-MEMNAME']['#text']), $appointmentCheckMemberArr))
											{
												$allowedArr			=	$allowedArr	+	16;//FOR BOOKED
												$debugStr			.=	' | APPOINTMENT ALREADY BOOKED [Appointment Id : '.$this->appointmentId[strtolower($memberDetailArr[$j]['BGEN-MEMNAME']['#text'])].' Appointment Date : '.$this->appointmentDate[strtolower($memberDetailArr[$j]['BGEN-MEMNAME']['#text'])].']';
											}
											$responseArr[$j]['QC-PURCHASEDID'] 	= 	"";
											$responseArr[$j]['QC-ERROR']			=	$allowedArr;
											$responseArr[$j]['QC-DEBUG']			=	trim($debugStr, '|');
											$responseArr[$j]['QC-TOTALAMOUNT']	=	'';
											$responseArr[$j]['QC-MEMBERID']		=	'';
											if($memberDetailArr[0]['BGEN-CLNTNM']['#text'] != $customerId && $memberDetailArr[$j]['BGEN-CLNTNM']['#text']!=$customerId && $isCustomerLoginCallingMe)
											{
												$responseArr[$j]['QC-ERROR']			=	$allowedArr + 128;//CASE OF DEPENTENT LOGIN FROM CUSTOMER PORTAL
											}
											if($allowedArr>0) {
												$isAllMembersAreUnAplicable++;
												
											}
												
										$j++;
										$planArr[$i]['MEMBERSDETAILS']		=	$responseArr;
										}
										if($isAllMembersAreUnAplicable==count($memberDetailArr))
											$planArr['UNAPLICABLEPLAN']			=	1;				
									Break;
				CASE	"BUY"		:	$planArr[$i]['ERROR']		=	'';
										$planArr[$i]['PLANTYPE']	=	"BUY";
										$buy_query					=	"SELECT * FROM CRMEMPLOYEEPURCHASEPLAN WHERE PLANID='".$planDetail_fetchArr['PLANID']."' AND POLICYNUMBER='$policyNo'  AND COMPANYID='$companyId' AND CLIENTID='$customerId' AND PAYMENTRECEIVED='YES' ORDER BY PURCHASEPLANID DESC";
										$buy_resource				=	$this->fetchDataAsResource($buy_query, 0);//FETCHING PURCHASE PLAN DETAILS
										$purchasePlanIdArr			=	array();
										$purchasePlanTotalAmountArr	=	array();
										$k=0;
										while($buy_fetch			=	oci_fetch_assoc($buy_resource))
										{
											$purchasePlanIdArr[$k]	=	$buy_fetch['PURCHASEPLANID'];//FETCHING PURCHASE PLAN DETAIL
											$purchasePlanTotalAmountArr[$k]		=	$buy_fetch['TOTALAMOUNT'];
											$k++;
										}
										//if(count($purchasePlanIdArr)>0)
										//{//CHECK FOR PLAN EXISTES OR NOT
											$purchasePlanId				=	implode(',', $purchasePlanIdArr);
											$purchasePlanMembr_query	=	"SELECT * FROM CRMEMPLOYEEPLANMEMBER where PURCHASEPLANID in($purchasePlanId)";
											$purchasePlanMembr_fetch	=	$this->fetchDataAsResource($purchasePlanMembr_query, 0);//FETCHING PURCHASE PLAN MEMBERS DETAILS
											$applicableForArr			=	$this->isAplicableForRelation($planArr[$i]['PLANNAMEID'], $planArr[$i]['EDITEDPRICE'], 'BUY');
											$j							=	0;
											$purchasePlanMembrClientIdArr=	array();
											$k							=	0;
											$purchasePlanMemberIdArr	=	array();
											while($purchasePlanMembr_f	=	oci_fetch_assoc($purchasePlanMembr_fetch))
											{
												$purchasePlanMembrClientIdArr[$k]		=	trim($purchasePlanMembr_f['CLIENTID']);//FETCHING PURCHASE MEMBER DETAIL
												$purchasePlanMemberIdArr[$purchasePlanMembr_f['CLIENTID']]		=	$purchasePlanMembr_f['MEMBERID'];
												$purchasePlanIdArr1[$purchasePlanMembr_f['CLIENTID']]			=	$purchasePlanMembr_f['PURCHASEPLANID'];
												$k++;
											}
											$appointmentCheckMemberArr	=	$this->isAppointmentAvailable($planArr[$i]['PLANID'],$planArr[$i]['COMPANYID'], $memberDetailArr[$j]['BGEN-EMPNO']['#text'], $policyNo, $customerId, 'CORPORATE');
											while($j<count($memberDetailArr))
											{//CHECK FOR CLIENT MATCHED OR NOT AND IS PLAN APLICABLE FOR THE MEMBER ?
												$allowedArr				=	0;
												$debugStr				=	'';
												if(!in_array(strtolower(str_replace(' ', '', $this->relationArr[$memberDetailArr[$j]['BGEN-DPNTTYP']['#text']])), $applicableForArr))
												{
													$allowedArr			=	8;
													$debugStr			=	' | RELATIONSHIP MISMATCH';
												} else {
													$AllowForCustomerSupport = true;
												}
												if(!in_array(trim($memberDetailArr[$j]['BGEN-CLNTNM']['#text']), $purchasePlanMembrClientIdArr))
												{
													$allowedArr			=	$allowedArr	+	64;
													$debugStr			=	' | MEMBER NOT FOUND';
												}
												if(in_array(strtolower($memberDetailArr[$j]['BGEN-MEMNAME']['#text']), $appointmentCheckMemberArr))
												{
													$allowedArr			=	$allowedArr	+	16;//FOR BOOKED
													$debugStr			=	' | APPOINTMENT ALREADY BOOKED [ Appointment Id : '.$this->appointmentId[strtolower($memberDetailArr[$j]['BGEN-MEMNAME']['#text'])].' Appointment Date : '.$this->appointmentDate[strtolower($memberDetailArr[$j]['BGEN-MEMNAME']['#text'])].']';
												}
												$responseArr[$j]['QC-PURCHASEDID'] 	= 	$purchasePlanIdArr1[$memberDetailArr[$j]['BGEN-CLNTNM']['#text']];
												$responseArr[$j]['QC-ERROR']			=	$allowedArr;
												$responseArr[$j]['QC-DEBUG']			=	trim($debugStr, '| ');
												$responseArr[$j]['QC-TOTALAMOUNT']	=	$purchasePlanTotalAmountArr[$j];
												$responseArr[$j]['QC-MEMBERID']		=	$purchasePlanMemberIdArr[$memberDetailArr[$j]['BGEN-CLNTNM']['#text']];
												if($memberDetailArr[0]['BGEN-CLNTNM']['#text'] != $customerId && $memberDetailArr[$j]['BGEN-CLNTNM']['#text']!=$customerId && $isCustomerLoginCallingMe)
												{
													$responseArr[$j]['QC-ERROR']			=	$allowedArr + 128;//CASE OF DEPENTENT LOGIN FROM CUSTOMER PORTAL
												}
												
											$j++;
											$planArr[$i]['MEMBERSDETAILS']		=	$responseArr;
											if($allowedArr>0)
												$isAllMembersAreUnAplicable++;
											}
											if($isAllMembersAreUnAplicable==count($memberDetailArr))
												$planArr['UNAPLICABLEPLAN']			=	1;
										//}else{
											//$planArr[$i]['ERROR']		=	'MEMBER NOT FOUND';
										//}
									Break;
				CASE	"LIMITED"	:	$planArr[$i]['ERROR']		=	'';
										$AllowForCustomerSupport 	= 	true;
										$planArr[$i]['PLANTYPE']	=	"LIMITED";
										$limitedPlanDetail_query	=	"SELECT * FROM CRMLIMITEDEMPLOYEE  WHERE POLICYID = '$policyId' AND (EMPLOYEENUMBER like '$customerId' OR CLIENTNUMBER like '$customerId'  )  AND STATUS='ACTIVE'";
										$limitedPlanDetail_resource	=	$this->fetchDataAsResource($limitedPlanDetail_query, 0);//FETCHING LIMITED PLAN DETAIL
										$limitedPlanEmpNoArr		=	array();
										$limitedPlanClientNoArr		=	array();
										$limitedPlanIdArr			=	array();
										$limitedappointmentAvail	=	array();
										$k=0;
										while($limitedPlanDetail_fetch	=	oci_fetch_assoc($limitedPlanDetail_resource))
										{
											$limitedPlanEmpNoArr[$k]	=	$limitedPlanDetail_fetch['EMPLOYEENUMBER'];
											$limitedPlanClientNoArr[$k]	=	$limitedPlanDetail_fetch['CLIENTNUMBER'];//FETCHING PURCHASE PLAN DETAIL
											$limitedPlanIdArr[$limitedPlanDetail_fetch['CLIENTNUMBER']]				=	$limitedPlanDetail_fetch['LIMITEDID'];
											$limitedappointmentAvail[$limitedPlanDetail_fetch['CLIENTNUMBER']]		=	$limitedPlanDetail_fetch['LIMITEDAPPOINTMENT'];
											$k++;
										}
										//if((count($limitedPlanEmpNoArr)+count($limitedPlanClientNoArr))>0)
										//{//CHECK FOR PLAN EXISTES OR NOT
											$j							=	0;
											//$applicableForArr			=	$this->isAplicableForRelation($planArr[$i]['PLANNAMEID'], $planArr[$i]['EDITEDPRICE'], 'BUY');
											$appointmentCheckMemberArr	=	$this->isAppointmentAvailable($planArr[$i]['PLANID'],$planArr[$i]['COMPANYID'], $memberDetailArr[$j]['BGEN-EMPNO']['#text'], $policyNo, $customerId, 'CORPORATE');
											//echo '<pre>'.$memberDetailArr[$j]['BGEN-CLNTNM']['#text'];print_r($limitedPlanDetailArr);die;
											while($j<count($memberDetailArr))
											{//CHECK FOR CLIENT MATCHED OR NOT AND IS PLAN APLICABLE FOR THE MEMBER ?
												$allowedArr				=	0;
												$debugStr				=	'';
												if(in_array(strtolower($memberDetailArr[$j]['BGEN-MEMNAME']['#text']), $appointmentCheckMemberArr))
												{
													$allowedArr			=	$allowedArr	+	16;//FOR BOOKED
													$debugStr			=	' | APPOINTMENT ALREADY BOOKED [Appointment Id : '.$this->appointmentId[strtolower($memberDetailArr[$j]['BGEN-MEMNAME']['#text'])].' Appointment Date : '.$this->appointmentDate[strtolower($memberDetailArr[$j]['BGEN-MEMNAME']['#text'])].']';
												}
												if(in_array(trim($memberDetailArr[$j]['BGEN-CLNTNM']['#text']), $limitedPlanClientNoArr) || in_array(trim($memberDetailArr[$j]['BGEN-EMPNO']['#text']), $limitedPlanEmpNoArr))
												{
													//do nothing
												}else{
													$allowedArr			=	$allowedArr	+	32;
													$debugStr			=	' | MEMBER NOT FOUND';
												}
												if($limitedappointmentAvail[$memberDetailArr[$j]['BGEN-CLNTNM']['#text']]=='YES')
												{
													$allowedArr			=	$allowedArr	+	120;
													$debugStr			=	' | MEMBER NOT ALLOWED';
												}
												$responseArr[$j]['QC-PURCHASEDID'] 	= 	$limitedPlanIdArr[$memberDetailArr[$j]['BGEN-CLNTNM']['#text']];
												$responseArr[$j]['QC-ERROR']			=	$allowedArr;
												$responseArr[$j]['QC-DEBUG']			=	trim($debugStr, '|');
												$responseArr[$j]['QC-TOTALAMOUNT']	=	'';
												$responseArr[$j]['QC-MEMBERID']		=	'';
												if($memberDetailArr[0]['BGEN-CLNTNM']['#text'] != $customerId && $memberDetailArr[$j]['BGEN-CLNTNM']['#text']!=$customerId && $isCustomerLoginCallingMe)
												{
													$responseArr[$j]['QC-ERROR']			=	$allowedArr + 128;//CASE OF DEPENTENT LOGIN FROM CUSTOMER PORTAL
												}
											$j++;
											$planArr[$i]['MEMBERSDETAILS']		=	$responseArr;
											if($allowedArr>0)
												$isAllMembersAreUnAplicable++;
											}
											if($isAllMembersAreUnAplicable==count($memberDetailArr))
												$planArr['UNAPLICABLEPLAN']			=	1;
									Break;
			}
			if (($planArr['UNAPLICABLEPLAN']=="1" && $isCustomerLoginCallingMe==true) || ($AllowForCustomerSupport === false && $_SESSION['userName']!='system')) 
			{
				unset($planArr[$i]);
				$i--;
			}
			$i++;
		}//echo '44';die;
		unset($planArr['UNAPLICABLEPLAN']);
		return $planArr;
	}
	
	private function getRetailPlanDetail($policyNo,$customerId, $memberDetailArr, $productCode, $policyStartDate, $policyEndDate, $planId, $returnMemberArr, $isCustomerLoginCallingMe)
	{//GETTING PLAN DETAIL AND MEMBER DETAIL FOR RETAIL USER
		//echo $policyNo."===".$customerId."===".$memberDetailArr."===".$productCode."===".$policyStartDate."===".$policyEndDate."===".$planId."===".$returnMemberArr."===".$isCustomerLoginCallingMe;die;
		//10082575===50334855===Array===10001101===============1
		global $relationArray;
		$planArr				=	array();
		$policyDetail_quer		=	"select PRODUCTID,PRODUCTCODE,TITLE from CRMRETAILPRODUCT where PRODUCTCODE='$productCode'";
		$policyDetail_fetchArr	=	$this->fetchDataAsAccoc($policyDetail_quer, 0);//print_r($policyDetail_fetchArr);
		$productId				=	$policyDetail_fetchArr['PRODUCTID'];
		$productCode			=	$policyDetail_fetchArr['PRODUCTCODE'];
		$productTitle			=	$policyDetail_fetchArr['TITLE'];
		$planIdCond				=	($planId)?"AND CRMRETAILHEALTHCHECKUPPLAN.PLANID='$planId'":"";
		$planDetail_query		=	"SELECT CRMRETAILHEALTHCHECKUPPLAN.*,CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANNAME AS PLANNAME2, CRMCORPHEALTHCHECKUP.UPLOADPDF AS UPLOADPDF2 FROM CRMRETAILHEALTHCHECKUPPLAN LEFT JOIN CRMCORPHEALTHCHECKUP ON CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CRMRETAILHEALTHCHECKUPPLAN.PLANNAMEID where CRMRETAILHEALTHCHECKUPPLAN.PRODUCTID='$productId' AND CRMRETAILHEALTHCHECKUPPLAN.STATUS='ACTIVE'  $planIdCond ";//remove  (and planid='3442'(BUY)) (and planid='5643'(FREE))////retail  AND CRMRETAILHEALTHCHECKUPPLAN.PLANID='171'
		$planDetail_resource	=	$this->fetchDataAsResource($planDetail_query, 0);
		$i						=	0;
		if($returnMemberArr)
		{
			$memberListArr			=	array();
			$m						=	0;
			foreach($memberDetailArr as $memberDetailVal)
			{
				$memberListArr[$m]['QC-MEMNAME']	=	$memberDetailVal['BGEN-MEMNAME']['#text'];
				$memberListArr[$m]['QC-EMPNO']		=	$memberDetailVal['BGEN-EMPNO']['#text'];
				$memberListArr[$m]['QC-DPNTTYP']	=	$memberDetailVal['BGEN-DPNTTYP']['#text'];
				$memberListArr[$m]['QC-RELATIONNAME']=	ucwords($relationArray[$memberDetailVal['BGEN-DPNTTYP']['#text']]);
				$memberListArr[$m]['QC-PLANNO']		=	$memberDetailVal['BGEN-PLANNO']['#text'];
				$memberListArr[$m]['QC-AGE']		=	$memberDetailVal['BGEN-AGE']['#text'];
				$memberListArr[$m]['QC-DOB']		=	$memberDetailVal['BGEN-DOB']['#text'];
				$memberListArr[$m]['QC-GENDER']		=	$memberDetailVal['BGEN-GENDER']['#text'];
				$memberListArr[$m]['QC-EMAIL']		=	$memberDetailVal['BGEN-EMAIL']['#text'];
				$memberListArr[$m]['QC-CLNTNM']		=	$memberDetailVal['BGEN-CLNTNM']['#text'];
				//$memberListArr[$m]['QC-STDCODE01']	=	$memberDetailVal['BGEN-STDCODE01']['#text'];
				$memberListArr[$m]['QC-CLTPHONE']	=	$memberDetailVal['BGEN-CLTPHONE']['#text'];
				//$memberListArr[$m]['QC-STDCODE02']	=	$memberDetailVal['BGEN-STDCODE02']['#text'];
				$memberListArr[$m]['QC-CLTPHONE02']	=	$memberDetailVal['BGEN-CLTPHONE02']['#text'];
				//$memberListArr[$m]['QC-STDCODE03']	=	$memberDetailVal['BGEN-STDCODE03']['#text'];
				$memberListArr[$m]['QC-CLTPHONE03']	=	$memberDetailVal['BGEN-CLTPHONE03']['#text'];
				$memberListArr[$m]['QC-ZBALCURR']	=	$memberDetailVal['BGEN-ZBALCURR']['#text'];
				$m++;
			}
			$planArr[0]['MEMBERS']	=	$memberListArr;
			$i++;
		}
		while($planDetail_fetchArr=	oci_fetch_assoc($planDetail_resource))
		{
			$AllowForCustomerSupport = false;
			$planArr[$i]				=	$planDetail_fetchArr;
			$responseArr				=	array();
			$planArr['UNAPLICABLEPLAN']	=	"0";
			$isAllMembersAreUnAplicable	=	0;
			switch($planDetail_fetchArr['PAYMENTOPTIONS'])
			{
				CASE	"FREE"		:	$planArr[$i]['ERROR']		=	'';
										$planArr[$i]['PLANTYPE']	=	"FREE";
										$j							=	0;
										$applicableForArr			=	$this->isAplicableForRelation($planArr[$i]['PLANNAMEID'], $planArr[$i]['FREEMAPPEDMEMBERTYPEIDS'], 'FREE');
										$appointmentCheckMemberArr	=	$this->isAppointmentAvailable($planArr[$i]['PLANID'],'', $memberDetailArr[$j]['BGEN-EMPNO']['#text'], $policyNo, $customerId, 'RETAIL', $policyStartDate,$policyEndDate);
										$getSi_query				=	"SELECT max(CAST(SI AS integer)) MA, min(CAST(SI AS integer)) MI FROM CRMSI WHERE SIID in(".$planArr[$i]['SI'].")";
										$getSi_fetch				=	$this->fetchDataAsAccoc($getSi_query, 0);//echo '<pre>';print_r($applicableForArr);die;							
										while($j<count($memberDetailArr))
										{
											$allowedArr				=	0;
											$debugStr				=	'';
											if(trim($memberDetailArr[$j]['BGEN-GENDER']['#text'])==trim($planArr[$i]['GENDER'][0]) || strtoupper(trim($planArr[$i]['GENDER']))=='BOTH')
											{
												//do nothing
											}else{
												$allowedArr			=	1;//FOR GENDER MISMATCH
												$debugStr			.=	' | GENDER NOT FOUND IN WEB-SERVICE';
											}
											if(trim($memberDetailArr[$j]['BGEN-GENDER']['#text'])=='' )
											{
												$allowedArr			=	1;//FOR GENDER MISMATCH
												$debugStr			.=	' | GENDER MISMATCH';
											}
											$ageArr					=	explode(',', $planArr[$i]['AGEGROUPID']);
											$memberAge				=	($memberDetailArr[$j]['BGEN-AGE']['#text'][1].$memberDetailArr[$j]['BGEN-AGE']['#text'][2]);
											$memberAge				=	(int)$memberAge;//print_r($ageArr);
											if(!in_array("$memberAge", $ageArr))
											{
												$allowedArr			=	$allowedArr	+	2;//FOR AGE MISMATCH
												$debugStr			.=	$memberAge.' | AGE MISMATCH---Member Age is '.$memberAge;
											}
											
											if (((((int)$getSi_fetch["MA"] + 1 ) > (int)$memberDetailArr[$j]['BGEN-ZBALCURR']['#text']) && (((int)$getSi_fetch["MI"] - 1) < (int)$memberDetailArr[$j]['BGEN-ZBALCURR']['#text'])))
											{
												
											}else{
												$allowedArr			=	$allowedArr	+	4;//FOR SI MISMATCH
												$debugStr			.=	' | SI MISMATCH---Member SI is '.(int)$memberDetailArr[$j]['BGEN-ZBALCURR']['#text'].'---'.$getSi_fetch["MA"].'----'.$getSi_fetch["MI"];
											}
											
											if(!in_array(strtolower(str_replace(' ', '', $this->relationArr[$memberDetailArr[$j]['BGEN-DPNTTYP']['#text']])), $applicableForArr))
											{
												$allowedArr			=	8;
												$debugStr			.=	' | RELATIONSHIP MISMATCH---Member Relation is '.$this->relationArr[$memberDetailArr[$j]['BGEN-DPNTTYP']['#text']];
											} else {
												$AllowForCustomerSupport = true;
											}
											if(in_array(strtolower($memberDetailArr[$j]['BGEN-MEMNAME']['#text']), $appointmentCheckMemberArr))
											{
												$allowedArr			=	$allowedArr	+	16;//FOR BOOKED
												$debugStr			.=	' | APPOINTMENT ALREADY BOOKED [Appointment Id : '.$this->appointmentId[strtolower($memberDetailArr[$j]['BGEN-MEMNAME']['#text'])].' Appointment Date : '.$this->appointmentDate[strtolower($memberDetailArr[$j]['BGEN-MEMNAME']['#text'])].']';
											
											}
											$responseArr[$j]['QC-PURCHASEDID'] 	= 	"";
											$responseArr[$j]['QC-ERROR']			=	$allowedArr;
											$responseArr[$j]['QC-DEBUG']			=	trim($debugStr);
											$responseArr[$j]['QC-TOTALAMOUNT']	=	'';
											$responseArr[$j]['QC-MEMBERID']		=	'';
											if($memberDetailArr[0]['BGEN-CLNTNM']['#text'] != $customerId && $memberDetailArr[$j]['BGEN-CLNTNM']['#text'] != $customerId && $isCustomerLoginCallingMe)
											{
												$responseArr[$j]['QC-ERROR']			=	$allowedArr + 128;//CASE OF DEPENTENT LOGIN FROM CUSTOMER PORTAL
											}
										$j++;
										$planArr[$i]['MEMBERSDETAILS']		=	$responseArr;
										if($allowedArr>0)
												$isAllMembersAreUnAplicable++;
										}
									if($isAllMembersAreUnAplicable==count($memberDetailArr))
											$planArr['UNAPLICABLEPLAN']			=	1;
									Break;
				CASE	"BUY"		:	$planArr[$i]['ERROR']		=	'';
										$planArr[$i]['PLANTYPE']	=	"BUY";
										$buy_query					=	"SELECT * FROM CRMRETEMPLOYEEPURCHASEPLAN WHERE PLANID='".$planDetail_fetchArr['PLANID']."' AND POLICYNUMBER='$policyNo' AND CLIENTID='$customerId' AND PAYMENTRECEIVED='YES' ORDER BY PURCHASEPLANID DESC";
										$buy_resource				=	$this->fetchDataAsResource($buy_query, 0);//FETCHING PURCHASE PLAN DETAILS
										$purchasePlanIdArr			=	array();
										$k=0;
										while($buy_fetch	=	oci_fetch_assoc($buy_resource))
										{
											$purchasePlanIdArr[$k]		=	$buy_fetch['PURCHASEPLANID'];//FETCHING PURCHASE PLAN DETAIL
											$k++;
										}
										//if(count($purchasePlanIdArr)>0)
										//{//CHECK FOR PLAN EXISTES OR NOT
											$purchasePlanId				=	implode(',', $purchasePlanIdArr);
											$purchasePlanMembr_query	=	"SELECT * FROM CRMRETEMPLOYEEPLANMEMBER where PURCHASEPLANID in($purchasePlanId)";
											$purchasePlanMembr_fetch	=	$this->fetchDataAsResource($purchasePlanMembr_query, 0);//FETCHING PURCHASE PLAN MEMBERS DETAILS
											$applicableForArr			=	$this->isAplicableForRelation($planArr[$i]['PLANNAMEID'], $planArr[$i]['EDITEDPRICE'], 'BUY');
											$j							=	0;
											$purchasePlanMembrClientIdArr=	array();
											$k							=	0;
											while($purchasePlanMembr_f	=	oci_fetch_assoc($purchasePlanMembr_fetch))
											{
												$purchasePlanMembrClientIdArr[$k]		=	trim($purchasePlanMembr_f['CLIENTID']);//FETCHING PURCHASE MEMBER DETAIL
												$k++;
											}

											$appointmentCheckMemberArr	=	$this->isAppointmentAvailable($planArr[$i]['PLANID'],$planArr[$i]['COMPANYID'], $memberDetailArr[$j]['BGEN-EMPNO']['#text'], $policyNo, $customerId, 'RETAIL', $policyStartDate,$policyEndDate);
											while($j<count($memberDetailArr))
											{//CHECK FOR CLIENT MATCHED OR NOT AND IS PLAN APLICABLE FOR THE MEMBER ?
												$allowedArr				=	0;
												$debugStr				=	'';
												if(!in_array(strtolower(str_replace(' ', '', $this->relationArr[$memberDetailArr[$j]['BGEN-DPNTTYP']['#text']])), $applicableForArr))
												{
													$allowedArr			=	8;
													$debugStr			=	' | RELATIONSHIP MISMATCH';
												} else {
													$AllowForCustomerSupport = true;
												}
												if(!in_array(trim($memberDetailArr[$j]['BGEN-CLNTNM']['#text']), $purchasePlanMembrClientIdArr))
												{
													$allowedArr			=	$allowedArr	+	64;
													$debugStr			=	' | MEMBER NOT FOUND';
												}
												if(in_array(strtolower($memberDetailArr[$j]['BGEN-MEMNAME']['#text']), $appointmentCheckMemberArr))
												{
													$allowedArr			=	$allowedArr	+	16;//FOR BOOKED
													$debugStr			=	' | APPOINTMENT ALREADY BOOKED [Appointment Id :'.$this->appointmentId[strtolower($memberDetailArr[$j]['BGEN-MEMNAME']['#text'])].'  Appointment Date : '.$this->appointmentDate[strtolower($memberDetailArr[$j]['BGEN-MEMNAME']['#text'])].']';
												}
												$responseArr[$j]['QC-PURCHASEDID'] 	= 	"";
												$responseArr[$j]['QC-ERROR']			=	$allowedArr;
												$responseArr[$j]['QC-DEBUG']			=	trim($debugStr, '|');
												$responseArr[$j]['QC-TOTALAMOUNT']	=	'';
												$responseArr[$j]['QC-MEMBERID']		=	'';
												if($memberDetailArr[0]['BGEN-CLNTNM']['#text'] != $customerId && $memberDetailArr[$j]['BGEN-CLNTNM']['#text']!=$customerId && $isCustomerLoginCallingMe)
												{
													$responseArr[$j]['QC-ERROR']			=	$allowedArr + 128;//CASE OF DEPENTENT LOGIN FROM CUSTOMER PORTAL
												}
											$j++;
											$planArr[$i]['MEMBERSDETAILS']		=	$responseArr;
											if($allowedArr>0)
												$isAllMembersAreUnAplicable++;												
											}
											if($isAllMembersAreUnAplicable==count($memberDetailArr))
												$planArr['UNAPLICABLEPLAN']			=	1;
											
									Break;
			}
			if (($planArr['UNAPLICABLEPLAN']=="1" && $isCustomerLoginCallingMe==true) || ($AllowForCustomerSupport === false && $_SESSION['userName']!='system')) {
				unset($planArr[$i]);
				$i--;
			}
			$i++;
		}//echo '44';die;
		unset($planArr['UNAPLICABLEPLAN']);
		return $planArr;
	}
}
?>