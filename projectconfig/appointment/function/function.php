<?php
/**
 * Get center's mapped to plan.
 * also get state and city list
 * 
 * @author Amit Choyal <choyalamit1432@gmail.com>
 * @param int planid,policyid,companyid, productid for get mapped center.
 * @return array center list with statename and city name
 *
 */
if (! function_exists('getCenterstateCitydetails')) {
	function getCenterstateCitydetails ($planid, $policyid = null, $companyid = null, $productid = null, $cid = null){
		global $conn;		
		if ( $planid != '' ) {
			$StrWhere = '';
			if ($cid!='') {
				$StrWhere = " AND LWCENTERMASTER.CENTERID = '".$cid."'";
			}
			if ($productid!='') {
				$sql = "SELECT LWCENTERMASTER.CENTERID,LWCENTERMASTER.CENTERNAME,LWCENTERMASTER.ADDRESS2,LWCENTERMASTER.ADDRESS1,LWCENTERMASTER.CONTACTPERSONMOBILE1,LWCENTERMASTER.STATEID,LWCENTERMASTER.CITYID,LWSTATEMASTER.STATENAME,LWCITYMASTER.CITYNAME FROM LWCENTERMASTER JOIN CRMRETAILCENTERMAPPING  ON  CRMRETAILCENTERMAPPING.CENTERID=LWCENTERMASTER.CENTERID LEFT JOIN LWSTATEMASTER ON LWCENTERMASTER.STATEID = LWSTATEMASTER.STATEID LEFT JOIN LWCITYMASTER 
	ON LWCENTERMASTER.CITYID = LWCITYMASTER.CITYID WHERE LWCENTERMASTER.STATUS='ACTIVE' AND CRMRETAILCENTERMAPPING.PLANID='".$planid."' AND CRMRETAILCENTERMAPPING.PRODUCTID='".$productid."'".$StrWhere."";
			} else {
				$sql = "SELECT LWCENTERMASTER.CENTERID,LWCENTERMASTER.CENTERNAME,LWCENTERMASTER.ADDRESS2,LWCENTERMASTER.ADDRESS1,LWCENTERMASTER.CONTACTPERSONMOBILE1,LWCENTERMASTER.STATEID,LWCENTERMASTER.CITYID,LWSTATEMASTER.STATENAME,LWCITYMASTER.CITYNAME FROM LWCENTERMASTER JOIN CRMCOMPANYCENTERMAPPING ON  CRMCOMPANYCENTERMAPPING.CENTERID = LWCENTERMASTER.CENTERID 
	LEFT JOIN LWSTATEMASTER ON LWCENTERMASTER.STATEID = LWSTATEMASTER.STATEID LEFT JOIN LWCITYMASTER 
	ON LWCENTERMASTER.CITYID = LWCITYMASTER.CITYID  WHERE LWCENTERMASTER.STATUS='ACTIVE' AND LWSTATEMASTER.STATUS='ACTIVE' AND LWCITYMASTER.STATUS='ACTIVE' AND 
	CRMCOMPANYCENTERMAPPING.POLICYID='".$policyid."' AND CRMCOMPANYCENTERMAPPING
	.PLANID='".$planid."' AND CRMCOMPANYCENTERMAPPING.COMPANYID = '".$companyid."'".$StrWhere."";
			}
			$acenterFound = array();	
			dbSelect($sql, $bcenterFound, $acenterFound, $Countrows);
			return $acenterFound;
		}
	}
}

/**
 * Get policy details
 * check start and expiry date
 * 
 * @author Amit Choyal <choyalamit1432@gmail.com>
 * @param int policy number
 * @return array with policy details
 *
 */
if (! function_exists('GetPolicyDetailsByPolicyNumber')) {
	function GetPolicyDetailsByPolicyNumber($policyNumber,$subtype = null) 
	{
		global $conn;
		$query['policyNo'] = $policyNumber;
		$policyDetailsArray = array();
		$foundDetails = false;
		if ($subtype != 'corporate') {
			for ($i =0; $i<3; $i++ ){
				$renewalArray = getRenewalMemberDetails($query);
				if (count($renewalArray)>0){
					$foundDetails = true;
					break;
				}
			}
		}
		if ($foundDetails) {
			//print_r($renewalArray);die;
			$foundDetails = false;
			if ($renewalArray[0]['policyMaturityDt']['#text']!='' && $renewalArray[0]['policyCommencementDt']['#text']!=''){
				$policyDetailsArray['policyNumber'] 	= trim($renewalArray[0]['policyNum']['#text']);
				$policyDetailsArray['policyId'] 		= '';
				$policyDetailsArray['policyStartdate'] 	= strtotime(trim($renewalArray[0]['policyCommencementDt']['#text']));
				$policyDetailsArray['policyExpirydate'] = strtotime(trim($renewalArray[0]['policyMaturityDt']['#text']));
				$policyDetailsArray['productId'] 		= trim($renewalArray[0]['baseProductId']['#text']);
				$policyDetailsArray['customerId'] 		= trim($renewalArray[0]['customerId']['#text']);
				$policyDetailsArray['companyId'] 		= '';
				$foundDetails = true;
			}
		}
		else if ($subtype == 'corporate') {
			$query="SELECT CRMPOLICY.*,CRMCOMPANY.COMPANYNAME FROM CRMPOLICY LEFT JOIN CRMCOMPANY ON CRMPOLICY.COMPANYID = CRMCOMPANY.COMPANYID WHERE CRMPOLICY.POLICYNUMBER='".$policyNumber."' AND CRMPOLICY.STATUS='ACTIVE'";
			dbSelect($query, $bPolicyDetail, $dataArray, $Countrows);		
			if ($bPolicyDetail) {				
				$policyDetailsArray['policyNumber'] 	= trim($dataArray[0]['POLICYNUMBER']);
				$policyDetailsArray['policyId'] 		= trim($dataArray[0]['POLICYID']);
				$policyDetailsArray['policyExpirydate'] = strtotime(trim($dataArray[0]['POLICYENDDATE']));
				$policyDetailsArray['policyStartdate'] 	= strtotime(trim($dataArray[0]['POLICYSTARTDATE']));
				$policyDetailsArray['productId']		= '';
				$policyDetailsArray['customerId']		= '';
				$policyDetailsArray['companyId']		= (isset($dataArray[0]['COMPANYID'])?trim($dataArray[0]['COMPANYID']):'');
				$policyDetailsArray['companyname']		= (isset($dataArray[0]['COMPANYNAME'])?trim($dataArray[0]['COMPANYNAME']):'');
				$policyDetailsArray['error']            = '';
				$foundDetails = true;
			}
		}
		if ($foundDetails) {
			//Check Policy start date and expiry date.	
			$currentdate = strtotime(date("Y-m-d"));
			$policyExpiryPlusOneday = strtotime("+1 day", $policyDetailsArray['policyExpirydate']);
			if ($currentdate <= $policyExpiryPlusOneday) {
				if ($currentdate < $policyDetailsArray['policyStartdate']) {
					$policyDetailsArray['error'] = '<li>You can\'t schedule appointment for this policy,policy start-Date is ' . date('d-m-Y',trim($policyDetailsArray['policyStartdate'])) . '</li>';
				} 
			} else {
				$policyDetailsArray['error'] = '<li>Your policy is expired.Please contact customer care !</li>';
			}	
		} else {
			$policyDetailsArray['error'] = '<li>Policy does not exist or Inactive.Please contact customer care !</li>';
		}
		return $policyDetailsArray;
	}
}
/**
 * Get Available slots for centern on selected date wise or latter dates.
 * 
 * @author Amit Choyal <choyalamit1432@gmail.com>
 * @param  int Required. Specifies the multi-dimensional array (record-set) to use
 * @param  date Optionnal. An date to filter the slots. This parameter can also be NULL to return complete arrays (useful together when required specific datewise slots array)
 * @return array dates and holiday list with slots 
 *
 */
if (! function_exists('GetAvailableSlotsForCenterOnSelectedDate')) {
	function GetAvailableSlotsForCenterOnSelectedDate($centerid,$date = null){
		global $conn;
		$ymd = date('Y-m-d');
		$holidaylist = '';
		$datelist = '';
		$strWhere = " WHERE CRMCALENDERSLOTMAPPING.CALDATE > '".$ymd."'";
		$strWhereHoliday = " AND HOLIDAYDATE > '".$ymd."'";
		if ($date!='') {
			$date = date('Y-m-d',strtotime($date));
			$strWhere = " WHERE CRMCALENDERSLOTMAPPING.CALDATE = '".$date."'"	;
			$strWhereHoliday = " AND HOLIDAYDATE = '".$date."'";
		}
		if ($centerid!='' ) {
			$holidaysql = "select HOLIDAYDATE,HOLIDAYNAME from CRMHOLIDAYS WHERE CENTERID = '" . $centerid . "' AND STATUS='ACTIVE' ".$strWhereHoliday."";
			dbSelect($holidaysql, $bHolidayFound, $aHolidaylist, $HolidayCount);
			if ($bHolidayFound) {
				$holidaylist = $aHolidaylist;
			}
			$datequery = "SELECT CRMSLOTMASTER.STARTTIMENAME,CRMCALENDER.CENTERID,CRMCALENDER.CALID,CRMSLOTMASTER.SLOTID,CRMCALENDERSLOTMAPPING
	.ID,CRMCALENDERSLOTMAPPING.CALDATE,CRMCALENDERSLOTMAPPING.MAXAPPOINTMENT,CRMCALENDERSLOTMAPPING.NOOFAPPOINTMENT, (CRMCALENDERSLOTMAPPING.MAXAPPOINTMENT - CRMCALENDERSLOTMAPPING.NOOFAPPOINTMENT) as availappointment FROM CRMSLOTMASTER LEFT JOIN CRMCALENDERSLOTMAPPING ON CRMCALENDERSLOTMAPPING
	.SLOTID=CRMSLOTMASTER.SLOTID LEFT JOIN CRMCALENDER ON CRMCALENDERSLOTMAPPING.CALID=CRMCALENDER.CALID
	 ".$strWhere." AND CRMCALENDER.CENTERID='".$centerid."' AND CRMCALENDERSLOTMAPPING
	.ISAVAIL='YES'";
			dbSelect($datequery, $bDatesFound, $aDatesFound, $DatesCount);
			if ($bDatesFound) {
				$datelist = $aDatesFound;
			}
		}
		$response = array(
			'holidaylist' => $holidaylist,
			'datelist'    => $datelist,  
		);
		return $response;
	}
}

/**
 * Check Appointment booking form server side validations.
 * 
 * @author Amit Choyal <choyalamit1432@gmail.com>
 * @param  Post array
 * @return array final array to book appointment and errors.
 *
 */
if (! function_exists('AppointmentFormValidation')) {
	function AppointmentFormValidation($variables) {
		global $conn;
		$appointment_details = array();
		$strErrorMessage ='';
		$success = '';
		$sflag = 0;
		$subtype 		= req_param('portal','','',$variables['portal']);
		$policynumber 	= req_param('policynumber','','',$variables['policynumber']);
		$policyid 		= req_param('policyid','','',$variables['policyid']);
		$companyid 		= req_param('companyid','','',$variables['companyid']);
		$companyname 	= req_param('companyid','','',$variables['companyname']);
		$productid 		= req_param('productid','','',$variables['productid']);
		$customerid 	= req_param('customerid','','',$variables['customerid']);
		$planidtype 	= req_param('planid','','',$variables['planid']);
		if ($planidtype!='') {
			$planidtype 	= explode('||', $planidtype);
			$plantype 		= $planidtype[1];
			$planid 		= $planidtype[0];		
		}
		$empname 		= req_param('empname','','',$variables['empname']);
		$emailid 		= req_param('email','','',$variables['email']);
		$mobilenumber 	= req_param('numbers','','',$variables['numbers']);
		$stateid		= req_param('stateid','','',$variables['stateid']);
		$cityid 		= req_param('cityid','','',$variables['cityid']);
		$centerid 		= req_param('centerid','','',$variables['centerid']);
		$centername  	= req_param('centername','','',$variables['centername']);
		$date1 			= req_param('date1','','',$variables['date1']);
		$timeslot 		= req_param('timeslot','','',$variables['timeslot']);
		$status 		= req_param('status','','',$variables['status']);
		///define null variablr for mscrm///
		$mscrm_zentrix_sessionid= req_param('mscrm_zentrix_sessionid');
		$mscrm_origin 			= req_param('mscrm_origin');
		$mscrm_req_by 			= req_param('mscrm_req_by');
		$mscrm_req_by_id 		= req_param('mscrm_req_by_id');
		$mscrm_app_num 			= req_param('mscrm_app_num');
		$mscrm_sr 				= req_param('mscrm_sr');
		$mscrm_subsr 			= req_param('mscrm_subsr');
		$mscrm_casetype 		= req_param('mscrm_casetype');
		/// End of mscrm///
		
		$isavail 				= 'YES';
		$memeberids 			= req_param('memeberids','','',$variables['memeberids']);
		if ($companyid == '' && $productid == '') {
			$strErrorMessage .= "<li>Essential data is missing please try again.</li>"; 
		}
		if ($companyid != '' && $policyid == '') {
			$strErrorMessage .= "<li>Essential data is missing please try again.</li>"; 
		}
		$usertype  = 'corporate';
		if ($productid!='' && $companyid==''){
			$usertype = 'retail';
			$productcode = req_param('productcode','','',$variables['productcode']);
		}	
		if ($policynumber == '') {
			$strErrorMessage .= "<li>Policy number can not be blank.</li>"; 
		}
		if ($usertype == 'corporate' && $customerid == '') {
			$strErrorMessage .= "<li>Customer id can not be blank.</li>"; 
		}
		if ($planid == '') {
			$strErrorMessage .= "<li>Please Select Plan For Appointment.</li>";  
		} 
		if ($empname ==''){
			$strErrorMessage .= "<li>Employee name can not be blank. Please try again.</li>";  
		}
		if ($emailid=='') {
			$strErrorMessage .= "<li>Email ID can not be empty.</li>"; 
		} elseif (filter_var($emailid, FILTER_VALIDATE_EMAIL) == false) {
			$strErrorMessage .= "<li>The email address is not valid.</li>";
		}	
		if ($mobilenumber=='') {
			$strErrorMessage .= "<li>Mobile Number can not be empty.</li>";   
		} elseif (!is_numeric($mobilenumber)) {
			$strErrorMessage .= "<li>Mobile Number Should be only numeric.</li>";       
		} elseif (strlen($mobilenumber) < 10 || strlen($mobilenumber)>10) {
			$strErrorMessage .= "<li>Mobile Number Should have only 10 digits.</li>";   
		}
		if ($stateid=='') {
			$strErrorMessage .=  "<li>State Name can not be empty.</li>";
		} elseif (!is_numeric($stateid)) {
			$strErrorMessage .= "<li>Selected State id Should be numeric.</li>";
		}
		
		if ($cityid=='') {
			$strErrorMessage .=  "<li>City Name can not be empty.</li>";
		} elseif (!is_numeric($cityid)) {
			$strErrorMessage .= "<li>Selected City id Should be numeric.</li>";
		}	
		
		if ($centername=='') {
			$strErrorMessage .= "<li>Center name can not be empty.</li>"; 
		}
		if ($centerid=='') {
			$strErrorMessage .= "<li>Center ID can not be empty.</li>"; 
		} elseif (!is_numeric($centerid)) {
			$strErrorMessage .= "<li>Selected Center id Should be numeric.</li>";
		}
		if (count($memeberids)==0) {
			$strErrorMessage .= "<li>Please select atleast a member for  book appointment.</li>";
		}
		$datePattern = '/^[0-3][0-9]-[a-zA-Z]{3}-[0-9]{4}$/';
		if (trim($date1)=='') {
			$strErrorMessage .= "<li>Appointment date can not be empty.</li>";
		} elseif (!preg_match($datePattern, $date1)) {
			$strErrorMessage .= "<li>Appointment Date ($date1) is not in correct format. (DD-MMM-YYYY | 26-Jan-2016).</li>";
		}
		if ($timeslot=='') {
			$strErrorMessage .= "<li>Please select a slot to book appointment.</li>";
		} elseif (!is_numeric($timeslot)) {
			$strErrorMessage .= "<li>Selected time slot id should be only numeric.</li>";
		}
		$mscrm = false;
		if (isset($_SESSION["portalaccess"]) && $_SESSION["portalaccess"] == 'FULLACCESS') {
		//if (isset($_SESSION["from_mscrm"])) {
			$mscrm_zentrix_sessionid= req_param('mscrm_zentrix_sessionid','','',$variables['mscrm_zentrix_sessionid']);
			$mscrm_origin 			= req_param('mscrm_origin','','',$variables['mscrm_origin']);
			$mscrm_req_by 			= req_param('mscrm_req_by','','',$variables['mscrm_req_by']);
			$mscrm_req_by_id 		= req_param('mscrm_req_by_id','','',$variables['mscrm_req_by_id']);
			$mscrm_app_num 			= req_param('mscrm_app_num','','',$variables['mscrm_app_num']);
			$mscrm_sr 				= req_param('mscrm_sr','','',$variables['mscrm_sr']);
			$mscrm_subsr 			= req_param('mscrm_subsr','','',$variables['mscrm_subsr']);
			$mscrm_casetype 		= req_param('mscrm_casetype','','',$variables['mscrm_casetype']);	
			$mscrm = true;
			if ($mscrm_zentrix_sessionid==''){
				$strErrorMessage .= "<li>Zentrix Session id Not found. please try again.</li>";
			}
			if ($mscrm_origin==''){
				$strErrorMessage .= "<li>Touch Point / Origin Not found. please try again.</li>";
			}
			if ($mscrm_req_by==''){
				$strErrorMessage .= "<li>Requested By not found. please try again.</li>";
			}
			if ($mscrm_req_by_id==''){
				$strErrorMessage .= "<li>Requested By ID not found. please try again.</li>";
			}
			if ($mscrm_app_num==''){
				$strErrorMessage .= "<li>Application No not found. please try again.</li>";
			}
		    if ($mscrm_sr==''){
				$strErrorMessage .= "<li>Sr not found. please try again.</li>";
			}
			if ($mscrm_subsr==''){
				$strErrorMessage .= "<li>Sub Sr not found. please try again.</li>";
			}
			if ($mscrm_casetype==''){
				$strErrorMessage .= "<li>Case type not found. please try again.</li>";
			}
		}
	    
		// prepare final array to insert
		$appointment_details['usertype'] 		= $usertype;
		$appointment_details['policynumber'] 	= $policynumber;
		$appointment_details['policyid']		= $policyid;
		$appointment_details['companyid'] 		= $companyid;	
		$appointment_details['productid'] 		= $productid;
		$appointment_details['customerid'] 		= $customerid;		
		$appointment_details['empname'] 		= $empname;
		$appointment_details['emailid'] 		= $emailid;
		$appointment_details['mobilenumber'] 	= $mobilenumber;
		$appointment_details['stateid'] 		= $stateid;
		$appointment_details['cityid'] 			= $cityid;
		$appointment_details['centername'] 		= $centername;
		$appointment_details['centerid'] 		= $centerid;
		$appointment_details['date1'] 			= $date1;
		$appointment_details['status'] 			= ($status!='')?$status:'PENDING';
		$appointment_details['slotid'] 			= $timeslot;
		$appointment_details['plantype'] 		= $plantype;
		$appointment_details['planid'] 			= $planid;
		if ($mscrm) {
			$appointment_details['mscrm_zentrix_sessionid'] = $mscrm_zentrix_sessionid;
			$appointment_details['mscrm_origin'] 	= $mscrm_origin;
			$appointment_details['mscrm_req_by']	= $mscrm_req_by;
			$appointment_details['mscrm_req_by_id'] = $mscrm_req_by_id;
			$appointment_details['mscrm_app_num'] 	= $mscrm_app_num;
			$appointment_details['mscrm_sr'] 		= $mscrm_sr;
			$appointment_details['mscrm_subsr'] 	= $mscrm_subsr;
			$appointment_details['mscrm_casetype'] 	= $mscrm_casetype;
		}
		if ($strErrorMessage == '') {
			$policyDetails 	= GetPolicyDetailsByPolicyNumber($policynumber,$subtype);
			
			if ($policyDetails['error']=='') {
				if ($usertype == 'retail') {
					$appointment_details['policystartdate'] = date('d-M-Y',$policyDetails['policyStartdate']);
					$appointment_details['policyendtdate']  = date('d-M-Y',$policyDetails['policyExpirydate']);
					$appointment_details['customerid'] 	 	= $policyDetails['customerId'];
					$customerid 							= $appointment_details['customerid'];
					$appointment_details['caseid']  		= req_param('caseid','','',$variables['caseid']);
					if ($appointment_details['caseid']!='' && !is_numeric($appointment_details['caseid'])) {
						$strErrorMessage .= "<li>Case id should be only numeric.</li>";       
					}
					$appointment_details['productname']  	= req_param('productname','','',$variables['productname']);
					if ($appointment_details['productname']==''){
						$appointment_details['productname'] 	=  $_SESSION['productName'];
					}
					if ($appointment_details['productname']==''){
						$strErrorMessage .= "<li>Product name can not be blank, Please try again.</li>";
					}
					$appointment_details['productcode']  	= req_param('productcode','','',$variables['productcode']);
					if ($appointment_details['customerid'] == '') {
						$strErrorMessage .= "<li>Customer id can not be blank.</li>";
					}
				} else {
					$appointment_details['companyname']  = $policyDetails['companyname'];
				}
			} else {
				$strErrorMessage .= $policyDetails['error'];
			}
		}
		if ($strErrorMessage == '') {
			$centerlist = getCenterstateCitydetails($planid, $policyid, $companyid, $productid, $centerid);
			if (count($centerlist)>0 ){
				$cityname  = $centerlist[0]['CITYNAME'];
				$statename 	= $centerlist[0]['STATENAME'];
				$centerphone = $centerlist[0]['CONTACTPERSONMOBILE1'];
				if ($cityname == '' || $statename == '') {
					$strErrorMessage .= "<li>Selected State or City name not found in database. Please try again.</li>";
				}
				$appointment_details['location'] = $cityname." - ".$statename;
				$appointment_details['centerphone']	= $centerphone;			
			}
			else {
				$strErrorMessage .= "<li>Selected Center details not mapped in database. Please try again.</li>";
			}
		}
		
		if ($strErrorMessage == '') {
			$pmlist = new listMemberPlan();
			//echo $policynumber."===".$customerid."===".$productcode."===".$planid;die;
			$planandmembersarray = $pmlist->getMemberDetail($policynumber, $customerid, array(), $productcode, $planid);
			
				$memberfound = false;
				if ( count($planandmembersarray) > 0 ) {
					$appointment_details['planname'] = 	$planandmembersarray[1]['PLANNAME2']?$planandmembersarray[1]['PLANNAME2']:$planandmembersarray[1]['PLANNAME2'];
					$appointment_details['UPLOADPDF'] = $planandmembersarray[1]['UPLOADPDF'];
					$appointment_details['UPLOADPDF2'] = $planandmembersarray[1]['UPLOADPDF2'];
					$memberdetails = array();
					$memberlist = $planandmembersarray[0]['MEMBERS'];
					$memberextradetail = $planandmembersarray[1]['MEMBERSDETAILS'];
					for ($i=0;$i<count($memberlist);$i++){
						$memberdetails[$i] = array_merge($memberlist[$i],$memberextradetail[$i]);
					}
					//print_r($memberlist);die;
					if (count($memberdetails)>0) {
						for ($i=0;$i<count($memberdetails);$i++){	
							if (in_array($memberdetails[$i]['QC-CLNTNM'],$memeberids)) {
								if ($memberdetails[$i]['QC-ERROR'] == 0) {
									$appointment_details['members'][] = $memberdetails[$i];
									$memberfound = true;
								} else{
									$strErrorMessage .= "<li>Somethingh went wrong. Please try again.</li>";
								}
							}
						}
					} else {
						$strErrorMessage .= "<li>Customer Not Found. Please try again.</li>";
					}
					if ($memberfound === false) {
						$strErrorMessage .= "<li>Customer detail not found in database. Please try again.</li>";
					}
					else{
						$appointment_details['primarymember'] = $memberdetails[0];
					}
				} else {
					$strErrorMessage .= "<li>Selected Plan details not mapped in database. Please try again.</li>";
				}
		}
		if ($strErrorMessage == '') {	
			$response = GetAvailableSlotsForCenterOnSelectedDate($centerid,$date1);
			if (count($response['datelist'])>0){	
				$found = false;
				if ($strErrorMessage == '') {
					for ($i=0;$i<count($response['datelist']);$i++) {
						if ($timeslot == $response['datelist'][$i]['SLOTID']) {
							$found = true;
							$slotmappingid 				= $response['datelist'][$i]['ID'];
							$availableappointment 		= $response['datelist'][$i]['AVAILAPPOINTMENT'];
							$maxappointment 			= $response['datelist'][$i]['MAXAPPOINTMENT'];
							$noofbookedappointment 		= $response['datelist'][$i]['NOOFAPPOINTMENT'];
							if (count($memeberids) > $availableappointment){
								$strErrorMessage .= "<li>You can book appointment only for ".$availableappointment." members in selected slot. Please try again.</li>";
							}
							$time = $response['datelist'][$i]['STARTTIMENAME'];
							if ($time != date('h:i A', strtotime($time)) && $time != date('g:i A', strtotime($time))) {
								$strErrorMessage .='<li>Invalid Time Slot. Please try again.</li>'; 
							}
							$timeslot1      = date('h:i', strtotime($time));
							$timeslot1am    = date('A', strtotime($time));
							if (trim($timeslot1)=='' || !isset($timeslot1)) {         
								$strErrorMessage .= '<li>Invalid Time Slot. Please try again.</li>'; 
							}
							if (trim($timeslot1am)=='' || !isset($timeslot1am)) { 
								$strErrorMessage .= '<li>Invalid Time Slot. Please try again.</li>'; 
							}
							$appointment_details['slotmappingid'] 			= $slotmappingid;
							$appointment_details['availableappointment'] 	= $availableappointment;
							$appointment_details['maxappointment'] 			= $maxappointment;
							$appointment_details['noofbookedappointment'] 	= $noofbookedappointment;
							$appointment_details['timeslot1'] 				= $timeslot1;
							$appointment_details['timeslot1am'] 			= $timeslot1am;
							break;
						}
					}
				}
				if ($found === false) {
					$strErrorMessage .= "<li>Selected date and slot details not mapped in database. Please try again.</li>";
				}
			} else {
				$strErrorMessage .= "<li>Selected Center date details not mapped in database. Please try again.</li>";
			}
		}
		if ($strErrorMessage=='') {
			//print_r($appointment_details);die;
			$sflag = 1;
		}
		$response = array(
			'sflag' 	=> $sflag,
			'success' 	=> $success,
			'error'    	=> $strErrorMessage,
			'appointment_details' => $appointment_details	
		);
		return $response;
	}
}
/**
 * Check Appointment booking form server side validations.
 * 
 * @author Amit Choyal <choyalamit1432@gmail.com>
 * @param  Post array
 * @return array final array to book appointment and errors.
 *
 */
if (! function_exists('SaveAppointmentData')) {
	function SaveAppointmentData($postdata){
		global $relationArray;
		global $conn;
		$sflag = 0;
		$success ='';
		$mode = $postdata['type'];
		//echo $mode;die;
		if ($mode=='RESCHEDULEAPPOINTMENTDATA'){
			$res = RescheduleAppointmentFormValidation($postdata);
		} else {
			$res = AppointmentFormValidation($postdata);
		}
		$ipaddress 		= $_SERVER['REMOTE_ADDR'];
		$finalarray = array();
		$plantype = $res['appointment_details']['plantype'];
		
		if ($res['sflag']!=1){
			return $res;
		} else {
			$healthcheckupplantype = 'GENERAL';
			if ($plantype == 'LIMITED') {
				$healthcheckupplantype = 'LIMITED';
			}
			$finalarray = $res['appointment_details'];
			//print_r($finalarray);die;
			$members = $finalarray['members'];
			if ( count($members)>0 ) {
				$entrytime = date('d-M-Y h:i:s A',time());	
				$noofbookedappointment 	= $finalarray['noofbookedappointment'];
				//print_r($finalarray['primarymember']);
				for ($i=0; $i<count($members);$i++) {
					$customerid 	= $members[$i]['QC-CLNTNM'];
					$fname 			= $members[$i]['QC-MEMNAME'];
					$age 			= $members[$i]['QC-AGE'];
					$si 			= $members[$i]['QC-ZBALCURR'];
					$gender 		= $members[$i]['QC-GENDER'];
					if (trim($gender)=='MALE' || $gender=='M') {
						$gender='M';
					} elseif (trim($gender)=='FEMALE' || $gender=='F') {
						$gender='F';
					} else {
						$strErrorMessage .= "<li>Gender Mismatch for customer ".$members[$i]['QC-MEMNAME'].".</li>"; 
					}
					if ($mode == 'RESCHEDULEAPPOINTMENTDATA'){
						$finalarray['primarymember']['QC-EMPNO'] = $members[$i]['QC-EMPNO'];
					} 
					$employeenumber = $finalarray['primarymember']['QC-EMPNO'];
					 
					$purchaseid 	= $members[$i]['QC-PURCHASEDID'];
					//echo $purchaseid."==".$plantype;die;
					if ($plantype == "BUY" && $purchaseid =='') {
						$strErrorMessage .= "<li>Purchased Id can not be blank. Please try again.</li>";
					} 
					$relation = $members[$i]['QC-DPNTTYP'];
					if ($relation=='' || !isset($relation)) {
						$strErrorMessage .= "<li>Customer Relation can not be empty.</li>";								
					} elseif (!array_key_exists($relation, $relationArray)) {
						$strErrorMessage .= "<li>Invalid Relation. Please try again.</li>";
					}
					$memberid =	$members[$i]['QC-MEMBERID'];
					if (($plantype == "BUY") && ($memberid=='' || !isset($memberid))) {
						$strErrorMessage .= "<li>Member ID can not be empty.</li>";
					} elseif (($plantype == "BUY") && !is_numeric($memberid)) {
						$strErrorMessage .= "<li>Member ID should be numeric.</li>";
					}
					$rollback = false;
					
					if ($strErrorMessage == ''){
						if ($finalarray['usertype'] == 'corporate') {
							/*$prevappointmentid = '';
							if ($mode=='RESCHEDULEAPPOINTMENTDATA'){
								$prevappointmentid = $finalarray['appointmentid'];
								$sql_resch = "UPDATE CRMCOMPANYPOSTAPPOINTMENT SET RESCHEDULED='NO',UPDATEDON=TO_DATE('".$entrytime."','DD-MON-YYYY HH:MI:SS AM'),UPDATEDBY='" . $_SESSION['userName'] . "' WHERE APPOINTMENTID=" . $prevappointmentid . "";
								$stdid_resch = oci_parse($conn, $sql_resch);
								$rs = oci_execute($stdid_resch, OCI_NO_AUTO_COMMIT);
								if (!$rs){
									$rollback = true;
									break;
								} 	
							}*/
							//get next seq.
							$check = oci_parse($conn, 'SELECT CRMCOMPANYPREAPPOINTMENT_SEQ.nextval FROM DUAL');
							oci_execute($check, OCI_NO_AUTO_COMMIT);
							$rest = oci_fetch_assoc($check);
							$rowid = $rest['NEXTVAL'];
							if ($rowid!='') {
								$finalarray['members'][$i]['QC-APPID'] =  $rowid;
								//Prepare for insert data
								 $sql="INSERT INTO CRMCOMPANYPOSTAPPOINTMENT (APPOINTMENTID,COMPANYID,CUSTOMERID,FIRSTNAME,DOB,EMAILID,MOBILENO,EMPLOYEELOCATION,CENTERNAME,DATE1,TIMESLOT1,TIMESLOT1AM,EMPLOYEEID,POLICYNUMBER,STATUS,REPORTSTATUS,CREATEDON,CREATEDBY,UPDATEDON,UPDATEDBY,HEALTHCHECKUPID,PURCHASEID,MEMBERID,PLANTYPE,HEALTHCHECKUPTYPE,HEALTHCHECKUPPLAN,SLOTID,SELECTEDSLOT,SLOTMAPPINGID,CENTERID,CITYID,STATEID,ADDRESS,IPADDRESS,GENDER,RELATION) values('".$rowid."','".$finalarray['companyid']."','".$customerid."',".NullifyVal($fname).",'".$age."',".NullifyVal($finalarray['emailid']).",'".$finalarray['mobilenumber']."',".NullifyVal($finalarray['location']).",".NullifyVal($finalarray['centername']).",to_date('".$finalarray['date1']."', 'DD-Mon-YYYY'),'".$finalarray['timeslot1']."',
								'".$finalarray['timeslot1am']."','".$employeenumber."','".$finalarray['policynumber']."','".$finalarray['status']."','NOTUPLOADED', TO_DATE('".$entrytime."', 'DD-Mon-YYYY HH:MI:SS AM'),".NullifyVal($_SESSION['userName']).",TO_DATE('".$entrytime."', 'DD-Mon-YYYY HH:MI:SS AM'),".NullifyVal($_SESSION['userName']).",'".$finalarray['planid']."','".$purchaseid."','".$memberid."','GENERAL','".$healthcheckupplantype."',".NullifyVal($finalarray['planname']).",'".$finalarray['slotid']."','slot1','".$finalarray['slotmappingid']."',
								'".$finalarray['centerid']."','".$finalarray['cityid']."','".$finalarray['stateid']."',".NullifyVal($address).",'".$ipaddress."','".$gender."','".$relation."') "; 
								$stdid = oci_parse($conn, $sql);
								$r = oci_execute($stdid, OCI_NO_AUTO_COMMIT);
								if (!$r) {
									$rollback = true;
								}   
								if ($rollback === false) {
									$sql2="INSERT INTO CRMCOMPANYAPPHISTORY(ID,APPOINTMENTID,STATUS,CREATEDON,CREATEDBY) 
									VALUES(CRMCOMPANYAPPHISTORY_SEQ.nextval,".$rowid.",'".$finalarray['status']."',TO_DATE('".$entrytime."', 'DD-Mon-YYYY HH:MI:SS AM'),'".$_SESSION['userName']."') ";  
									//query to update status based on ID
									$stdid2 = oci_parse($conn, $sql2);
									$r2 = oci_execute($stdid2, OCI_NO_AUTO_COMMIT);
									if (!$r2) {
										$rollback = true;
										break;
									}
								}
								if ($rollback === false && $plantype == 'LIMITED') {
									 $limitedid = $purchaseid;
									 $sql3="UPDATE  CRMLIMITEDEMPLOYEE SET LIMITEDAPPOINTMENT='YES',UPDATEDON='".$entrytime."',UPDATEDBY='".$_SESSION['userName']."' WHERE LIMITEDID='".$limitedid."'";						//query to insert records
									 $stdid3 = @oci_parse($conn, $sql3);
									 $r3 = @oci_execute($stdid3, OCI_NO_AUTO_COMMIT);
									 if (!$r3) {
                                        $rollback = true;
										break;
                                    }
								}
							} else {
								$strErrorMessage .= "<li class='1'>Something went wrong please try again.</li>";
								$rollback = true;
								break;
							}
						}
						elseif ($finalarray['usertype'] == 'retail') {
							$prevappointmentid = '';
							if ($mode == 'RESCHEDULEAPPOINTMENTDATA'){
								$prevappointmentid = $finalarray['appointmentid'];
								
								$sql_resch = "UPDATE CRMRETAILPOSTAPPOINTMENT SET RESCHEDULED='YES',UPDATEDON=TO_DATE('".$entrytime."', 'DD-MON-YYYY HH:MI:SS AM'),UPDATEDBY='" . $_SESSION['userName'] . "' WHERE APPOINTMENTID=" . $prevappointmentid . "";
								$stdid_resch = oci_parse($conn, $sql_resch);
								$rs = oci_execute($stdid_resch, OCI_NO_AUTO_COMMIT);
								if (!$rs){
									$rollback = true;
									break;
								} 	
							}
							$check1 = oci_parse($conn, 'SELECT CRMRETAILPOSTAPPOINTMENT_SEQ.nextval FROM DUAL');
							oci_execute($check1, OCI_NO_AUTO_COMMIT);
							$res = oci_fetch_assoc($check1);
							$rowid=$res['NEXTVAL'];
							if ($rowid!='')	{
								$finalarray['members'][$i]['QC-APPID'] =  $rowid;
$sql="INSERT INTO CRMRETAILPOSTAPPOINTMENT (APPOINTMENTID,PRODUCTID,PRODUCTNAME,CUSTOMERID,FIRSTNAME,DOB,EMAILID,MOBILENO,EMPLOYEELOCATION,CENTERNAME,DATE1,TIMESLOT1,TIMESLOT1AM,EMPLOYEEID,POLICYNUMBER,STATUS,REPORTSTATUS,CREATEDON,CREATEDBY,UPDATEDON,UPDATEDBY,HEALTHCHECKUPID,PURCHASEID,PLANTYPE,HEALTHCHECKUPTYPE,HEALTHCHECKUPPLAN,REFERENCENUMBER,SELECTEDSLOT,SLOTID,SLOTMAPPINGID,CENTERID,CITYID,STATEID,IPADDRESS,MEMBERRELATION,GENDER,SI,WSPOLICYSTARTDATE,WSPOLICYENDDATE, CASEID, PREVAPPOINTMENTID) values('".$rowid."','".$finalarray['productid']."',".NullifyVal($finalarray['productname']).",'".$customerid."',".NullifyVal($fname).",'".$age."',".NullifyVal($finalarray['emailid']).",'".$finalarray['mobilenumber']."',".NullifyVal($finalarray['location']).",".NullifyVal($finalarray['centername']).",TO_DATE('".$finalarray['date1']."', 'DD-Mon-YYYY'),'".$finalarray['timeslot1']."','".$finalarray['timeslot1am']."','".$finalarray['customerid']."','".$finalarray['policynumber']."','".$finalarray['status']."','NOTUPLOADED',TO_DATE('".$entrytime."', 'DD-Mon-YYYY HH:MI:SS AM'),".NullifyVal($_SESSION['userName']).",TO_DATE('".$entrytime."', 'DD-Mon-YYYY HH:MI:SS AM'),".NullifyVal($_SESSION['userName']).",'".$finalarray['planid']."','".$purchaseid."','GENERAL','".$healthcheckupplantype."',".NullifyVal($finalarray['planname']).",'','slot1','".$finalarray['slotid']."','".$finalarray['slotmappingid']."','".$finalarray['centerid']."','".$finalarray['cityid']."','".$finalarray['stateid']."','".$ipaddress."','".$relation."','".$gender."','".$si."',TO_DATE('".$finalarray['policystartdate']."', 'DD-Mon-YYYY'),TO_DATE('".$finalarray['policyendtdate']."', 'DD-Mon-YYYY'), '".$finalarray['caseid']."', '".$prevappointmentid."')";
								$stdid = oci_parse($conn, $sql);
								$r = oci_execute($stdid, OCI_NO_AUTO_COMMIT);
								if (!$r) {
									$rollback = true;
									break;
								}
								if ($_SESSION["portalaccess"] == 'FULLACCESS' && !empty($finalarray['mscrm_zentrix_sessionid'])) {
									$mscrm_zentrix_sessionid 	= $finalarray['mscrm_zentrix_sessionid'];
									$mscrm_origin 			 	= $finalarray['mscrm_origin'];
									$mscrm_req_by 			 	= $finalarray['mscrm_req_by'];
									$mscrm_sr 			 		= $finalarray['mscrm_sr'];
									$mscrm_subsr 			 	= $finalarray['mscrm_subsr'];
									$mscrm_casetype 			= $finalarray['mscrm_casetype'];
									$mscrm_app_num 				= $finalarray['mscrm_app_num'];
									$mscrm_req_by_id 			= $finalarray['mscrm_req_by_id'];
									
									$query_ms = "INSERT INTO CRMAPPOINTMENT_MS_CREATION (ID,APPOINTMENTID,SESSION_ID,CASE_ORIGIN,REQUESTED_BY,CRM_SR,CRM_SUBSR,CRM_CASETYPE,CREATED_ON,APPLICATION_NO,REQUESTED_BY_ID) VALUES (CRMAPPOINTMENT_MS_CREATION_SEQ.nextval,'" . $rowid . "','" . $mscrm_zentrix_sessionid. "','" . $mscrm_origin  . "','" . $mscrm_req_by  . "','" . $mscrm_sr . "','" . $mscrm_subsr . "','" . $mscrm_casetype . "','" . $entrytime . "','" . $mscrm_app_num . "','" . $mscrm_req_by_id  . "')";
									$query_parse = oci_parse($conn, $query_ms);
									$r = oci_execute($query_parse, OCI_NO_AUTO_COMMIT);
									if (!$r) {
										$rollback  = true;
									}
								}
								if ($mode == 'RESCHEDULEAPPOINTMENTDATA'){// Slot Free //
									$sql = "SELECT RA.SLOTMAPPINGID,CC.NOOFAPPOINTMENT,CC.MAXAPPOINTMENT,CC.ISAVAIL FROM CRMRETAILPOSTAPPOINTMENT RA LEFT JOIN CRMCALENDERSLOTMAPPING CC ON RA.SLOTMAPPINGID = CC.ID WHERE RA.APPOINTMENTID='".$prevappointmentid."'";
									dbSelect($sql, $bappdetail, $aappdetail, $count, true);
									if ($bappdetail) {
										$avail = '';
										$noofbookedappointment 	= $aappdetail[0]['NOOFAPPOINTMENT'] - 1;
										if ($aappdetail[0]['ISAVAIL'] == 'YES' ) {
											$avail = "ISAVAIL='NO' ,";
										}
										$sqlres="UPDATE  CRMCALENDERSLOTMAPPING SET NOOFAPPOINTMENT='".$noofbookedappointment."',$avail UPDATEDON = '".$entrytime."',UPDATEDBY=".NullifyVal($_SESSION['userName'])." WHERE ID='".$aappdetail[0]['SLOTMAPPINGID']."'";	
										$stdidres = oci_parse($conn, $sqlres);
										$r4 = oci_execute($stdidres, OCI_NO_AUTO_COMMIT);
									}
								}/// slot Free End////
									// Slot Free //
									$sql = "SELECT RA.SLOTMAPPINGID,CC.NOOFAPPOINTMENT,CC.MAXAPPOINTMENT,CC.ISAVAIL FROM CRMRETAILPOSTAPPOINTMENT RA LEFT JOIN CRMCALENDERSLOTMAPPING CC ON RA.SLOTMAPPINGID = CC.ID WHERE RA.APPOINTMENTID='".$prevappointmentid."'";
									dbSelect($sql, $bappdetail, $aappdetail, $count, true);
									if ($bappdetail) {
										$avail = '';
										$noofbookedappointment 	= $aappdetail[0]['NOOFAPPOINTMENT'] - 1;
										if ($aappdetail[0]['ISAVAIL'] == 'YES' ) {
											$avail = "ISAVAIL='NO' ,";
										}
										$sqlres="UPDATE  CRMCALENDERSLOTMAPPING SET NOOFAPPOINTMENT='".$noofbookedappointment."',$avail UPDATEDON = '".$entrytime."',UPDATEDBY=".NullifyVal($_SESSION['userName'])." WHERE ID='".$aappdetail[0]['SLOTMAPPINGID']."'";	
										$stdidres = oci_parse($conn, $sqlres);
										$r4 = oci_execute($stdidres, OCI_NO_AUTO_COMMIT);
									}
									/// slot Free End////
								}
								if ($insert_mscrm === true) {
									$query_ms = "INSERT INTO CRMAPPOINTMENT_MS_CREATION (ID,APPOINTMENTID,SESSION_ID,CASE_ORIGIN,REQUESTED_BY,CRM_SR,CRM_SUBSR,CRM_CASETYPE,CREATED_ON,APPLICATION_NO,REQUESTED_BY_ID) VALUES (CRMAPPOINTMENT_MS_CREATION_SEQ.nextval,'" . $rowid . "','" . $mscrm_zentrix_sessionid. "','" . $mscrm_origin  . "','" . $mscrm_req_by  . "','" . $mscrm_sr . "','" . $mscrm_subsr . "','" . $mscrm_casetype . "','" . $entrytime . "','" . $mscrm_app_num . "','" . $mscrm_req_by_id  . "')";
									$query_parse = oci_parse($conn, $query_ms);
									$r = oci_execute($query_parse, OCI_NO_AUTO_COMMIT);
									if (!$r) {
										$rollback  = true;
									}
								}						
								if ($rollback === false) {
									$sql2="INSERT INTO CRMRETAILAPPHISTORY(ID,APPOINTMENTID,STATUS,CREATEDON,CREATEDBY) 
									VALUES(CRMRETAILAPPHISTORY_SEQ.nextval,".$rowid.",'".$finalarray['status']."',TO_DATE('".$entrytime."', 'DD-Mon-YYYY HH:MI:SS AM'),'".$_SESSION['userName']."') ";  
									//query to update status based on ID
									$stdid2 = oci_parse($conn, $sql2);
									$r2 = oci_execute($stdid2, OCI_NO_AUTO_COMMIT);
									if (!$r2) {
										$rollback = true;
										break;
									}
								}
							} else {
								$strErrorMessage .= "<li class='2'>Something went wrong please try again.</li>";
								$rollback = true;
								break;
							}
						}
					
					else {	
						$strErrorMessage .= "<li class='3'>Something went wrong please try again.</li>";
						$rollback = true;
						break;
					} 
					$noofbookedappointment 	= $noofbookedappointment+1;
					$maxappointment 		= $finalarray['maxappointment'];
					$bookedapp 				= $noofbookedappointment;
					$appfull = '';
					if ($bookedapp  == $maxappointment){
						$appfull = "ISAVAIL='NO' ,";
					}
					$sql4="UPDATE  CRMCALENDERSLOTMAPPING SET NOOFAPPOINTMENT='".$bookedapp."',$appfull UPDATEDON = '".$entrytime."',UPDATEDBY='".$_SESSION['userName']."' WHERE ID='".$finalarray['slotmappingid']."'";	
					$stdid4 = oci_parse($conn, $sql4);
					$r4 = oci_execute($stdid4, OCI_NO_AUTO_COMMIT);
				}
				//$rollback = false;
				if ($rollback === false) {	
					oci_commit($conn);
					//oci_rollback($conn);
					//print_r($finalarray);
					$sflag = 1;
					$qwe= new sendMailForAppointment();
					if ($mode=='RESCHEDULEAPPOINTMENTDATA'){
						$success ='Rescheduled Appointment successfully.';
						$qwe->SendRetailAppointmentReschedulemail($finalarray);
					} else {
						$success ='Appointment Booked successfully.';
						$qwe->sendMailAfterAppointmentBooking($finalarray);
					}
				} else {					
					$strErrorMessage .= "<li class='4'>Something went wrong please try again.</li>";
					oci_rollback($conn);
				}
			}	
		}
		$response = array(
			'sflag' 	=> $sflag,
			'success' 	=> $success,
			'error'    	=> $strErrorMessage,
		);
		return $response;
	}
}



/**
 * Reschedule Appointment Validation.
 * 
 * @author Amit Choyal <choyalamit1432@gmail.com>
 * @param Post Appointment Details array
 * @return final array to insert data.
 *
 */

 
if (! function_exists('RescheduleAppointmentFormValidation')) {
	function RescheduleAppointmentFormValidation($postvariables) {
		global $conn;		
		$appointment_details = array();
		$aAppointmentFound['type'] = 'reschedule';
		$strErrorMessage ='';
		$success = '';
		$sflag = 0;
		$portal 	= req_param('portal','','',$postvariables['portal']);
		$centerid 	= req_param('centeridres','','',$postvariables['centerid']);
		$date1 		= req_param('rescheduledate','','',$postvariables['rescheduledate1']);
		$timeslot 	= req_param('timeslotres','','',$postvariables['timeslot']);
		$status 	= req_param('statusres','','',$postvariables['status_update']);
		$mscrm_zentrix_sessionid= req_param('mscrm_zentrix_sessionid');
		$mscrm_origin 			= req_param('mscrm_origin');
		$mscrm_req_by 			= req_param('mscrm_req_by');
		$mscrm_req_by_id 		= req_param('mscrm_req_by_id');
		$mscrm_app_num 			= req_param('mscrm_app_num');
		$mscrm_sr 				= req_param('mscrm_sr');
		$mscrm_subsr 			= req_param('mscrm_subsr');
		$mscrm_casetype 		= req_param('mscrm_casetype');
		
		if ( $portal == 'corporate' ) {
			$usertype = 'corporate';
			$sql = "SELECT * FROM CRMCOMPANYPOSTAPPOINTMENT WHERE APPOINTMENTID = '".$postvariables['id']."'";
		} else {
			$usertype = 'retail';
			$sql = "SELECT * FROM CRMRETAILPOSTAPPOINTMENT WHERE APPOINTMENTID = '".$postvariables['id']."'";	
		}
		
		dbSelect($sql, $bAppointmentFound, $aAppointmentFound);
		if ( !$bAppointmentFound ) {	
			$strErrorMessage .= "<li>Essential data is missing please try again.</li>"; 
		}

		if ($strErrorMessage == '') {
			if ($centerid=='') {
				$strErrorMessage .= "<li>Center ID can not be empty.</li>"; 
			} elseif (!is_numeric($centerid)) {
				$strErrorMessage .= "<li>Center id Should be only numeric.</li>";
			}
			$datePattern = '/^[0-3][0-9]-[a-zA-Z]{3}-[0-9]{4}$/';
			if (trim($date1)=='') {
				$strErrorMessage .= "<li>Date1 can not be empty.</li>";
			} elseif (!preg_match($datePattern, $date1)) {
				$strErrorMessage .= "<li>Appointment Date ($date1) is not in correct format. (DD-MMM-YYYY | 26-Jan-2016).</li>";
			}
			if ($timeslot=='') {
				$strErrorMessage .= "<li>Please select a slot to book appointment.</li>";
			} elseif (!is_numeric($timeslot)) {
				$strErrorMessage .= "<li>Time slot id should be only numeric.</li>";
			}
		}
		$mscrm = false;
		if ($portal == 'mscrm') {
			$mscrm = true;
			$mscrm_zentrix_sessionid= req_param('mscrm_zentrix_sessionid','','',$variables['mscrm_zentrix_sessionid']);
			$mscrm_origin 			= req_param('mscrm_origin','','',$variables['mscrm_origin']);
			$mscrm_req_by 			= req_param('mscrm_req_by','','',$variables['mscrm_req_by']);
			$mscrm_req_by_id 		= req_param('mscrm_req_by_id','','',$variables['mscrm_req_by_id']);
			$mscrm_app_num 			= req_param('mscrm_app_num','','',$variables['mscrm_app_num']);
			$mscrm_sr 				= req_param('mscrm_sr','','',$variables['mscrm_sr']);
			$mscrm_subsr 			= req_param('mscrm_subsr','','',$variables['mscrm_subsr']);
			$mscrm_casetype 		= req_param('mscrm_casetype','','',$variables['mscrm_casetype']);	
			if ($mscrm_zentrix_sessionid==''){
				$strErrorMessage .= "<li>Zentrix Session id Not found. please try again.</li>";
			}
			if ($mscrm_origin==''){
				$strErrorMessage .= "<li>Touch Point / Origin Not found. please try again.</li>";
			}
			if ($mscrm_req_by==''){
				$strErrorMessage .= "<li>Requested By not found. please try again.</li>";
			}
			if ($mscrm_req_by_id==''){
				$strErrorMessage .= "<li>Requested By ID not found. please try again.</li>";
			}
			if ($mscrm_app_num==''){
				$strErrorMessage .= "<li>Application No not found. please try again.</li>";
			}
		    if ($mscrm_sr==''){
				$strErrorMessage .= "<li>Sr not found. please try again.</li>";
			}
			if ($mscrm_subsr==''){
				$strErrorMessage .= "<li>Sub Sr not found. please try again.</li>";
			}
			if ($mscrm_casetype==''){
				$strErrorMessage .= "<li>Case type not found. please try again.</li>";
			}
		}
		// prepare final array to insert
		$appointmentdetails = $aAppointmentFound[0];
		//print_r($appointmentdetails);die;
		// prepare final array to insert
		$appointment_details['usertype'] 		= $usertype;
		$appointment_details['appointmentid'] 	= $appointmentdetails['APPOINTMENTID'];
		$appointment_details['policynumber'] 	= $appointmentdetails['POLICYNUMBER'];
		$appointment_details['policyid']		= '';
		$appointment_details['companyid'] 		= '';
		$appointment_details['companyname'] 	= '';
		$appointment_details['productid'] 		= '';
		$appointment_details['customerid'] 		= $appointmentdetails['CUSTOMERID'];
		$appointment_details['planid'] 			= $appointmentdetails['HEALTHCHECKUPID'];
		$appointment_details['empname'] 		= $appointmentdetails['FIRSTNAME'];
		$appointment_details['emailid'] 		= $appointmentdetails['EMAILID'];
		$appointment_details['mobilenumber'] 	= $appointmentdetails['MOBILENO'];
		$appointment_details['stateid'] 		= $appointmentdetails['STATEID'];
		$appointment_details['cityid'] 			= $appointmentdetails['CITYID'];
		$appointment_details['centerid'] 		= $centerid;
		$appointment_details['date1'] 			= $date1;
		$appointment_details['status'] 			= ($status!='')?$status:'PENDING';
		$appointment_details['slotid'] 			= $timeslot;
		$appointment_details['planname'] 		= $appointmentdetails['HEALTHCHECKUPPLAN'];
		if ($mscrm) {
			$appointment_details['mscrm_zentrix_sessionid'] = $mscrm_zentrix_sessionid;
			$appointment_details['mscrm_origin'] 	= $mscrm_origin;
			$appointment_details['mscrm_req_by']	= $mscrm_req_by;
			$appointment_details['mscrm_req_by_id'] = $mscrm_req_by_id;
			$appointment_details['mscrm_app_num'] 	= $mscrm_app_num;
			$appointment_details['mscrm_sr'] 		= $mscrm_sr;
			$appointment_details['mscrm_subsr'] 	= $mscrm_subsr;
			$appointment_details['mscrm_casetype'] 	= $mscrm_casetype;
		}
		
		if ($strErrorMessage == '') {
			$policyDetails 	= GetPolicyDetailsByPolicyNumber($appointment_details['policynumber'],$usertype);
			if (!isset($policyDetails['error']) && $policyDetails['error']=='') {
				if ($usertype == 'retail') {
					$appointment_details['policystartdate'] = date('d-M-y',$policyDetails['policyStartdate']);
					$appointment_details['policyendtdate']  = date('d-M-y',$policyDetails['policyExpirydate']);
					$customerid 							= $appointmentdetails['CUSTOMERID'];
					$appointment_details['caseid']  		= $appointmentdetails['CASEID'];
					$appointment_details['productid']  		= $appointmentdetails['PRODUCTID'];
					$appointment_details['productname']  	= $appointmentdetails['PRODUCTNAME'];
					//$appointment_details['productcode']  	= req_param('productcode','','',$variables['productcode']);
				} else{
					$appointment_details['policyid'] = $policyDetails['policyid'];
					$appointment_details['companyid'] = $policyDetails['companyid'];
					$appointment_details['companyname'] = $policyDetails['companyname'];
				}	
			} else {
				$strErrorMessage .= $policyDetails['error'];
			}
		}
		
		if ($strErrorMessage == '') {
			$centerlist = getCenterstateCitydetails($appointment_details['planid'] , $appointment_details['policyid'] , $appointment_details['companyid'], $appointment_details['productid'], $appointment_details['centerid']);
			if (count($centerlist)>0 ){
				$cityname  = $centerlist[0]['CITYNAME'];
				$statename 	= $centerlist[0]['STATENAME'];
				$centerphone = $centerlist[0]['CONTACTPERSONMOBILE1'];
				if ($cityname == '' || $statename == '') {
					$strErrorMessage .= "<li>Selected State or City name not found in database. Please try again.</li>";
				}
				$appointment_details['centername'] = $centerlist[0]['CENTERNAME'];
				$appointment_details['location'] = $cityname." - ".$statename;
				$appointment_details['centerphone']	= $centerphone;			
			}
			else {
				$strErrorMessage .= "<li>Selected Center details not mapped in database. Please try again.</li>";
			}
		}
		
		if ($strErrorMessage == '') {	
			$response = GetAvailableSlotsForCenterOnSelectedDate($centerid,$date1);
			if (count($response['datelist'])>0){	
				$found = false;
				if ($strErrorMessage == '') {
					for ($i=0;$i<count($response['datelist']);$i++) {		
						if ($timeslot == $response['datelist'][$i]['SLOTID']) {
							$found = true;
							$slotmappingid 				= $response['datelist'][$i]['ID'];
							$availableappointment 		= $response['datelist'][$i]['AVAILAPPOINTMENT'];
							$maxappointment 			= $response['datelist'][$i]['MAXAPPOINTMENT'];
							$noofbookedappointment 		= $response['datelist'][$i]['NOOFAPPOINTMENT'];
							if ($availableappointment < 1 ){
								$strErrorMessage .= "<li>Slot is already full. Please try again.</li>";
							}
							$time = $response['datelist'][$i]['STARTTIMENAME'];
							if ($time != date('h:i A', strtotime($time)) && $time != date('g:i A', strtotime($time))) {
								$strErrorMessage .='<li>Invalid Time Slot..</li>'; 
							}
							$timeslot1      = date('h:i', strtotime($time));
							$timeslot1am    = date('A', strtotime($time));
							if (trim($timeslot1)=='' || !isset($timeslot1)) {         
								$strErrorMessage .= '<li>Invalid Time Slot</li>'; 
							}
							if (trim($timeslot1am)=='' || !isset($timeslot1am)) { 
								$strErrorMessage .= '<li>Invalid Time Slot AM</li>'; 
							}
							$appointment_details['slotmappingid'] 			= $slotmappingid;
							$appointment_details['availableappointment'] 	= $availableappointment;
							$appointment_details['maxappointment'] 			= $maxappointment;
							$appointment_details['noofbookedappointment'] 	= $noofbookedappointment;
							$appointment_details['timeslot1'] 				= $timeslot1;
							$appointment_details['timeslot1am'] 			= $timeslot1am;
							break;
						}
					}
				}
				if ($found === false) {
					$strErrorMessage .= "<li>Selected date and slot details not mapped in database. Please try again.</li>";
				}
			} else {
				$strErrorMessage .= "<li>Selected Center date details not mapped in database. Please try again.</li>";
			}
		}
		if ($strErrorMessage=='') {
			$members[0]['QC-CLNTNM'] 	= $customerid; 
			$members[0]['QC-MEMNAME'] 	= $appointmentdetails['FIRSTNAME'];
			$members[0]['QC-AGE'] 		= $appointmentdetails['DOB'];
			if ($usertype == 'retail') {
				$members[0]['QC-ZBALCURR'] 	= $appointmentdetails['SI'];
			}
			$members[0]['QC-GENDER'] 		= $appointmentdetails['GENDER'];
			$members[0]['QC-PURCHASEDID'] 	= $appointmentdetails['PURCHASEDID'];
			$members[0]['QC-DPNTTYP'] 		= $appointmentdetails['MEMBERRELATION'];
			$members[0]['QC-MEMBERID'] 		= $appointmentdetails['MEMBERID'];
			$members[0]['QC-EMPNO'] 		= $appointmentdetails['EMPLOYEEID'];
			
			if ($appointmentdetails['HEALTHCHECKUPTYPE'] == 'LIMITED') {
				$appointment_details['plantype'] = 'LIMITED';
			} else {
				if ($appointmentdetails['PURCHASEDID']!='') {
					$appointment_details['plantype'] = 'BUY';
				} else {
					$appointment_details['plantype'] = 'FREE';
				}
			}				
			$sflag = 1;
		}
		$appointment_details['members'] = $members;
		//$appointment_details['prim'] = $members;
		//print_r($appointment_details);die;
		$response = array(
			'sflag' 	=> $sflag,
			'success' 	=> $success,
			'error'    	=> $strErrorMessage,
			'appointment_details' => $appointment_details	
		);
		return $response;
	}
}

/**
 * Get booked Retail appointment list by policy number.
 * 
 * @author Amit Choyal <choyalamit1432@gmail.com>
 * @param policy number
 * @return array contains list of booked appointment
 *
 */

 
if (! function_exists('getBookedAppointmentdetailbypolicynumber')) {
	function getBookedAppointmentdetailbypolicynumber($policynumber) {
		$aapplist = array();
		$sql = "SELECT CA.PRODUCTID, CA.HEALTHCHECKUPID, CA.APPOINTMENTID, CA.RESCHEDULED, CA.PREVAPPOINTMENTID,  CA.FIRSTNAME, 
CA.EMAILID, CA.DATE1, CA.DATE2, CA.STATUS, CRA.TITLE, CCHP.HEALTHCHECKUPPLANNAME AS PLANNAME2, CRHP.PLANNAME FROM CRMRETAILPOSTAPPOINTMENT CA  LEFT JOIN CRMRETAILPRODUCT CRA ON CRA.PRODUCTID = CA.PRODUCTID LEFT JOIN  CRMRETAILHEALTHCHECKUPPLAN CRHP ON CRHP.PLANID = CA.HEALTHCHECKUPID LEFT JOIN CRMCORPHEALTHCHECKUP CCHP ON CCHP.HEALTHCHECKUPPLANID = CRHP.PLANNAMEID WHERE CA.POLICYNUMBER = '".$policynumber."' ORDER BY CA.CREATEDON DESC";
		dbSelect($sql, $bapplist, $aapplist);
		return $aapplist;
	}
}

function CreateHtmltableforBookedAppointmentList($applist = array(),$type = 'retail') {
	
	$stringPolicy = '';
	if (count($applist)>0) {
		for ($i=0; $i<count($applist);$i++) {
			$optionvalue = '';
			if ($applist[$i]['RESCHEDULED'] == 'YES') {
				$optionvalue = '';
			} else if (($applist[$i]['STATUS'] == 'PENDING') || ($applist[$i]['STATUS'] == 'CONFIRM')) {
				$optionvalue = '<option value="RESCHEDULE">Re-schedule</option>';
			} else if ($applist[$i]['STATUS'] == 'DONE') {
				$optionvalue = '';
			} else {
				$optionvalue = '';
			}
			$action = '<select class="txtfield_185 status_drop_down" style="width:70px;" id="action_' . $applist[$i]['APPOINTMENTID'] . '" onchange="return appointmentActionChange(this.value,' . $applist[$i]['APPOINTMENTID'] . ',\''.$type.'\')">
				<option value="">Action</option>
				<option value="VIEW">View Detail</option>
			' . $optionvalue . '
			</select>';
				if (trim($applist[$i]['RESCHEDULED']) == 'YES') {
					$statusDisplay = 'RESCHEDULED';
				} else if (trim($applist[$i]['PREVAPPOINTMENTID']) != '' && trim($applist[$i]['STATUS']) == 'PENDING') {
					$statusDisplay = 'Pending For Rescheduling';
				} else {
					$statusDisplay = trim($applist[$i]['STATUS']);
				}
				$stringPolicy.='<tr>';
				$stringPolicy.='<td>' . trim($applist[$i]['APPOINTMENTID'] ? stripslashes($applist[$i]['APPOINTMENTID']) : 'NA') . '</td>';
				$stringPolicy.='<td>' . trim($applist[$i]['PREVAPPOINTMENTID'] ? stripslashes($applist[$i]['PREVAPPOINTMENTID']) : 'NA') . '</td>';
			   // $stringPolicy.='<td>' . trim($productName[0]['TITLE'] ? stripslashes($productName[0]['TITLE']) : 'NA') . '</td>';
				$stringPolicy.='<td>' . trim($applist[$i]['FIRSTNAME'] ? stripslashes($applist[$i]['FIRSTNAME']) : 'NA') . '</td>';
				$stringPolicy.='<td>' . trim($applist[$i]['EMAILID'] ? stripslashes($applist[$i]['EMAILID']) : 'NA') . '</td>';
				$stringPolicy.='<td>' . wordwrap($applist[$i]['DATE1'], 17, "\n", true) . "<br/>" . wordwrap($applist[$i]['DATE2'], 17, "\n", true) . '</td>';
				$stringPolicy.='<td>' . trim($applist[$i]['PLANNAME2']). '</td>';
				$stringPolicy.='<td id="changestatus_' . $applist[$i]['APPOINTMENTID'] . '">' . $statusDisplay . '</td>';
				$stringPolicy.='<td>' . trim($action) . '</td>';
				$stringPolicy.='</tr>';
		}
	}
	return $stringPolicy;
}
function checkAppointmentCSRF(){
	$customHeaders =	apache_request_headers();
	if($customHeaders['X-Requested-With'] == 'XMLHttpRequest')
	{
		if($customHeaders['appointmentCSRF']!=$_SESSION['appointmentCSRF']) {
			die("Unauthorized Access");
		}
	}
	/*if (isset($_REQUEST['appcsrf'])){
		if($_REQUEST['appcsrf']!=$_SESSION['appcsrf']) {
			die("Unauthorized Access");
		}
	}*/
}
