<style>
.event_green a {
    background-color: #42B373 !important;
    background-image :none !important;
    color: #ffffff !important;
}

.event_orange a {
    background-color:#FF6400 !important;
    background-image :none !important;
    color: #ffffff !important;
}
.event_orange span {
    background-color:#FF6400 !important;
    background-image :none !important;
    color: #ffffff !important;
}
.event_yellow a {
    background-color: #FFBD00 !important;
    background-image :none !important;
    color: #ffffff !important;
}
.event_red a {
    background-color: #FF0000 !important;
    background-image :none !important;
    color: #FF0000 !important;
}

.event_red span {
    background-color:#FF0000 !important;
    background-image :none !important;
    color: #FF0000 !important;   
}
.event_lightred a {
    background-color: #FF474d !important;
    background-image :none !important;
    color: #FFFFFF !important;
}
.warningdiv{
	color: #316FE5;
    margin-bottom: 10px;
    margin-left: 10px;
    font-weight: bold;
}
.errordiv{
	color:#a94442;
	margin-bottom:10px;
}
.errordiv li{
	list-style: none;
	margin-left: -40px;
}
.alert-message-error {
	border: 1px solid transparent;
    border-radius: 0.25rem;
    margin-bottom: 1rem;
    padding: 0.75rem 1.25rem;
    background-color: #f2dede;
    border-color: #ebccd1;
    color: #a94442;;
}
.alert-message {
	border: 1px solid transparent;
    border-radius: 0.25rem;
    margin-bottom: 1rem;
    padding: 0.75rem 1.25rem;
    background-color: #D9EDF7;
    border-color: #BCDFF1;
    color: #31708F;
}
.membertable table {
	float:left;
	border: 1px solid #CCC;
	font-weight : normal;

}

.membertable {
    border-bottom: 1px solid #dadada;
    border-left: 1px solid #dadada;
    margin: 0;
    padding: 0;
}
.membertable th {
	border-right: 1px solid #DADADA;
	font-family: Tahoma;
	font-size: 10px;
	font-size-adjust: none;
	font-stretch: normal;
	font-style: normal;
	font-variant: normal;
	font-weight: bold;
	line-height: 22px !important;
	padding: 5px 10px;
	text-align: center;
	text-transform: uppercase;
}
.membertable td {
    background: #ffffff none repeat scroll 0 0;
    border-right: 1px solid #dadada;
    border-top: 1px solid #dadada;
    color: #222222;
    font-family: Tahoma;
    font-feature-settings: normal;
    font-kerning: auto;
    font-language-override: normal;
    font-size: 12px;
    font-size-adjust: none;
    font-stretch: normal;
    font-style: normal;
    font-synthesis: weight style;
    font-variant: normal;
    font-weight: normal;
    line-height: 22px !important;
    padding: 5px 10px;
    text-align: center;
}
.apperror{
	background: #F4EDF2 !important;
}
.apperror hr{
	color:#FFFFFF;
}
.successdiv{
	color:green;
	margin-bottom:10px;
	/*border: 1px solid transparent;
    border-radius: 0.25rem;
    margin-bottom: 1rem;
    padding: 0.75rem 1.25rem;
    background-color: #dff0d8;
    border-color: #b2dba1;
    color: #3c763d;*/
}
.displaydate {
	padding-left : 10px;
}
</style>