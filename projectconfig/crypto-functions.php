<?php
function encrypt_data($data, $key='z5yK1lw7XYt6YKdP7Pne2Jw3zRkMAziH', $iv="i0kbCAlFTlDXshYVCT2IxLOdJ0iWEwqK"){
					//String		// Encryption Key			// IV Key
	
    $ciphertext_encrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key,$data, MCRYPT_MODE_CBC, $iv);
    //$ciphertext_encrypt = $iv . $ciphertext_encrypt;
    $ciphertext_base64 = base64_encode($ciphertext_encrypt);
    return $ciphertext_base64;
}
function decrypt_data($data, $key='z5yK1lw7XYt6YKdP7Pne2Jw3zRkMAziH', $iv="i0kbCAlFTlDXshYVCT2IxLOdJ0iWEwqK"){
					//Encrypted string		// Encryption Key			// IV Key

	$ciphertext = base64_decode($data);
	//$iv_size=strlen($iv);
    //$ciphertext = substr($ciphertext_base64_decode, $iv_size);
    $plaintext_decrypt = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key,$ciphertext, MCRYPT_MODE_CBC, $iv);
    return $plaintext_decrypt;
}
function encryptUrlData($data, $key='z5yK1lw7XYt6YKdP7Pne2Jw3zRkMAziH', $iv="i0kbCAlFTlDXshYVCT2IxLOdJ0iWEwqK"){
	$ciphertext_base64=encrypt_data($data,$key,$iv);
	$ciphertext_base64=str_replace(array('+','/'),array('-','_'),$ciphertext_base64);
    return $ciphertext_base64;
}
function decryptUrlData($data, $key='z5yK1lw7XYt6YKdP7Pne2Jw3zRkMAziH', $iv="i0kbCAlFTlDXshYVCT2IxLOdJ0iWEwqK"){
	$plaintext_decrypt = decrypt_data(str_replace(array('-','_'),array('+','/'),$data),$key,$iv);
	
    return $plaintext_decrypt;
}