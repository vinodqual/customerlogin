<?php

/**
  Used to connect with the Database

  @category Partner-Payment
  @package  PHP
  @author   sumit kumar <sumit.kumar@qualtech-consultants.com>
  @license  2016 Qualtech-consultants pvt ltd.
  @link     http://tractus.religarehealthinsurance.com/
 * */

/**
 * Used to connect with database
 *  
 * @return boolean
 */
function db_connect() 
{

    // Production DB Connection
      $db_hostName="10.216.6.71"; //defined host name
	  $db_hostPort="2529"; //defined host name
	  $db_serviceName="rhprod"; //defined db name
	  $db_username="rhicl";  // define db username
	  $db_password="rhicl"; // define db password

    // Connection  string with Oracle
    return oci_connect($db_username, $db_password, "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=" . $db_hostName . ")(PORT=" . $db_hostPort . "))(CONNECT_DATA=(SERVICE_NAME=" . $db_serviceName . ")))");
}

?>
