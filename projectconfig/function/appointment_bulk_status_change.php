<?php 
function appoint_bulk_status_change($userType, $appColNo, $statusColNo, $new_status, $row, $checkValidation, &$final_arr, $appointment_id, $k, &$error_empty, &$error)
{
	global $conn;
	$status_array = array("CONFIRM", "CANCELLED", "PENDING", "DONE");
	$new_status=strtoupper($new_status);
	if(!in_array($new_status, $status_array))
	{
		$error_empty = "Invalid Status";
		$error[$k] = "Invalid Status";
		return false;
	}
				if($userType=='RETAIL')
				{
					$table='CRMRETAILPOSTAPPOINTMENT';
					$whereVar=' and RESCHEDULED is null';
				}else
				{
					$table='CRMCOMPANYPOSTAPPOINTMENT';
					$whereVar='';
				}
				if($appColNo>0 && $statusColNo>0)
				{
					if($checkValidation)
					{
						if($final_arr[$k]['curr_status']!=$new_status && ($new_status!="PENDING FOR RESCHEDULING" || $final_arr[$k]['curr_status']!="PENDING")){
							switch($new_status)
							{
								case "CONFIRM" :    $cond = " and upper(STATUS) != 'DONE' and upper(STATUS) != 'CANCELLED'";
													$confirm=true;
									break;
								case "PENDING" :    $cond = " and upper(STATUS)!='CONFIRM' and upper(STATUS) != 'DONE' and upper(STATUS) != 'CANCELLED'";
									break;
								case "CANCELLED" :  $cond = " and upper(STATUS) != 'DONE'";
													$cancel=true;
									break;
								default: $cond ="";
									$error_empty = "Invalid Status";
									$error[$k] = "Invalid Status";
							}
							if($cond!=""){
								$sql = "Select upper(trim(STATUS)) from $table where APPOINTMENTID='" . $appointment_id . "' $whereVar ". $cond;
								$res = oci_parse($conn, $sql);
								oci_execute($res);
								$result = oci_fetch_row($res);
								
								if($result){
									if($confirm && $result[0]=='PENDING') {
										$final_arr[$k]["conMail"]=1;
									}
									else if($cancel && ($result[0]=='PENDING' || $result[0]=='CONFIRM')) {
										$final_arr[$k]["canMail"]=1;
									}
								}
								else 
								{
									$error_empty = "Status cannot be changed";
									$error[$k] = "Status cannot be changed";
								}
							}
						}
						else if($new_status == "DONE") {
							$error_empty = "Invalid Status";
							$error[$k] = "Invalid Status";
						}
					}
				}
				else{
					$error_empty = "All fields are mandatory";
					$error[$k] = "All fields are mandatory";
					break;
				}
	if($error_empty=='')
		return true;
	else
		return false;
}
?>