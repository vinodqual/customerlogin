<?php
/**
 * grab contents of _GET or _POST variables
 * also cleans inputs of any nasty stuff
 * to prevent spamming
 * 
 * @author Amit Choyal <choyalamit1432@gmail.com>
 * @param string variable that you would like to check
 * @param bool choose whether you want to clean the input
 * @param bool choose whether you want to really clean it
 * @param string the return value if the variable is not set
 * @return string
 *
 */

if (! function_exists('req_param')) {
	function req_param($request,$clean=false,$all=true,$else='')
	{
		$patterns = array('/content\-type:/i','/to:/i','/bcc:/i','/cc:/i');
		if ( $all ) array_push($patterns,'/\r/i','/\n/i','/%0d/i','/%0a/i');
		if ( isset($_POST[$request]) ) {
			if ( $clean ) {
				if ( is_array($_POST[$request]) ) {
					$aReturn_Array = array();
					foreach($_POST[$request] as $value) {
						$aReturn_Array[] = stripslashes(preg_replace($patterns,'',trim($value)));
					}
					return $aReturn_Array;
				}
				else {
					return stripslashes(preg_replace($patterns,'',trim( $_POST[$request] )));
				}
			} else {
				if ( is_array($_POST[$request]) ) {
					$aReturn_Array = array();
					foreach($_POST[$request] as $value) {
						$aReturn_Array[] = stripslashes(trim($value));
					}
					return $aReturn_Array;
				} else {
					return stripslashes(trim( $_POST[$request] ));
				}
			}
		} elseif ( isset($_GET[$request]) ) {
			if ( $clean ) {
				if ( is_array($_GET[$request]) ) {
					$aReturn_Array = array();
					foreach($_GET[$request] as $value) {
						$aReturn_Array[] = stripslashes(preg_replace($patterns,'',trim($value)));
					}
					return $aReturn_Array;
				}
				else {
					if ( is_array($_GET[$request]) ) {
						$aReturn_Array = array();
						foreach($_GET[$request] as $value) {
							$aReturn_Array[] = stripslashes(trim($value));
						}
						return $aReturn_Array;
					}
					else {
						return stripslashes(preg_replace($patterns,'',trim( $_GET[$request] )));
					}
				}
			} else {
				return stripslashes(trim( $_GET[$request] ));
			}
		} else {
			return $else;
		}
	}
}
/**
 * checks to see if a variable is empty
 *
 * @author Amit Choyal <choyalamit1432@gmail.com>
 * @param string
 * @return bool
 *
 */
if (! function_exists('CheckNullValue')) {
	function CheckNullValue($str)
	{
		if ( $str == "0" ) {
			return(false);
		} else {
			if ( is_null($str) || $str == "" || empty($str) || strlen($str) <= 0 ) {
				return(true);
			}
		}
	}
}

/**
 * checks to see if a variable is null or empty returns (string) "null" if so
 * if not, cleans the variable and returns it quoted 'var'
 *
 * @author Amit Choyal <choyalamit1432@gmail.com>
 * @param string variable to be checked
 * @return string
 *
 */
 
if (! function_exists('NullifyVal')) {
	function NullifyVal($varIn)
	{
		if ( CheckNullValue($varIn) ) {
			return "NULL";
		}
		$varIn = stripslashes($varIn);
		$varIn = addslashes( $varIn );
		$varIn = str_replace("\'", "\''", $varIn);//added by faisal
		return "'" . $varIn . "'";
	}
}


/**
 * select the data from database.
 * 
 * @author Amit Choyal <choyalamit1432@gmail.com>
 * @param sql query that you would like to run
 * @param bool return true if records found in database
 * @param array data array row wise
 * @param int the return number of rows found in database.
 * @return array, bool, int
 *
 */
if (! function_exists('dbSelect')) {
	function dbSelect($strQuery, &$bTrigger, &$aArray, &$Countrows = null, $nocommit = false) {
		global $conn;   
		//global $strQry;
		$strQry = $strQuery;	
		if ($strQry) { //if query string exists then continue
			$sql = oci_parse($conn, $strQry);
			// Execute Query 
			if ($nocommit === true) {
				oci_execute($sql, OCI_NO_AUTO_COMMIT);
			} else {
				oci_execute($sql);
			}
			$i=0;
			while (($row = @oci_fetch_assoc($sql))) {
				foreach ($row as $key=>$value) {
					$aArray[$i][$key] = $value;				
				}
				$i++;
			}
			//print_r($aArray);die;
			$Countrows = count($aArray);
			if ( $Countrows > 0 ) {
				 $bTrigger = true;
			} else {
				$bTrigger = false;
			}
		} else {
			$bTrigger = false;
		}
	}
}

/**
 * Get center's mapped to plan.
 * also get state and city list
 * 
 * @author Amit Choyal <choyalamit1432@gmail.com>
 * @param  array Required. Specifies the multi-dimensional array (record-set) to use
 * @param  string Required. An integer key or a string key name of the column of values to return. This parameter can also be NULL to       return complete arrays (useful together with index_key to re-index the array)
 * @param  string Optional. The column to use as the index/keys for the returned array
 * @return array center list with statename and city name
 *
 */
if (! function_exists('array_column')) {
    function array_column(array $input, $columnKey, $indexKey = null) {
		global $conn;
        $array = array();
        foreach ($input as $value) {
            if ( !array_key_exists($columnKey, $value)) {
                trigger_error("Key \"$columnKey\" does not exist in array");
                return false;
            }
            if (is_null($indexKey)) {
                $array[] = $value[$columnKey];
            }
            else {
                if ( !array_key_exists($indexKey, $value)) {
                    trigger_error("Key \"$indexKey\" does not exist in array");
                    return false;
                }
                if ( ! is_scalar($value[$indexKey])) {
                    trigger_error("Key \"$indexKey\" does not contain scalar value");
                    return false;
                }
                $array[$value[$indexKey]] = $value[$columnKey];
            }
        }
        return $array;
    }
}

?>