<?php $data=@$_REQUEST['data'];
$success=false;
$error=0;
$email='';
if(isset($data) && !empty($data)){
	include_once("conf/conf.php");    //include configuration file to access database connectivity
	include_once("conf/common_functions.php"); //include common_functions file to access defined function
	
	$decryptData=simple_decrypt($data,$pwdsalt); 
	parse_str($decryptData,$output);
	$email=strtolower($output['email']);
	$date=$output['date']+(72*60*60);
	$query=fetchcolumnListCond("USERNAME,USERID,ISPASSWORDCHANGED,USERSTATUS","RETUSERAUTH","WHERE LOWER(USERNAME)='".@$email."' AND USERSTATUS='ACTIVE' ");
	if(count($query) > 0){
		$success=true;
		$currenttime=time();
		if($date >= $currenttime){
			$newpassword=@$_POST['newpassword'];
			$cpassword=@$_POST['cpassword'];
			
			if(($newpassword==$cpassword) && !empty($cpassword) && !empty($newpassword) && isset($newpassword) && isset($cpassword) && (sanitize_data($_POST['change'])!='')){
				require_once 'passwordverify/PkcsKeyGenerator.php';
				require_once 'passwordverify/DesEncryptor.php';
				require_once 'passwordverify/PbeWithMd5AndDes.php';		
				$encryptNewPass = PbeWithMd5AndDes::encrypt($newpassword, $keystring);
				$sql="UPDATE RETUSERAUTH SET PASSWORD='".$encryptNewPass."',ISPASSWORDCHANGED='YES',UNSUCCESSFULATTEMPTS=0,STATUS='ACTIVE' WHERE USERNAME='".@$email."'"; 
				$stdid = @oci_parse($conn, $sql);
				$r = @oci_execute($stdid);
				// redirect to dashboard page
					$_SESSION["userName"] = trim($query[0]['USERNAME']);
					$_SESSION["USERID"] = trim($query[0]['USERID']);
					$_SESSION["LOGINTYPE"] = 'RETAIL';
					$_SESSION['validateLogin'] = 1;
					$_SESSION['ISPASSWORDCHANGED']=trim($query[0]['ISPASSWORDCHANGED']);
					$loginTime = time();
					$_SESSION["lastLoginTime"] = time();
					$sql = "INSERT INTO RETUSERLOGIN (USERLOGINSEQ,USERID,LASTLOGINDT,FIRSTLOGINDATE,LOGINSTATUS,IP) VALUES (RETUSERLOGIN_SEQ.nextval,'" . trim(@$_SESSION['USERID']) . "','" . $loginTime . "','" . $loginTime . "','" . $query[0]['USERSTATUS']. "','" . @$_SERVER['REMOTE_ADDR'] . "')"; //insert query to insert records 
					$stdid = @oci_parse($conn, $sql);
					$r = @oci_execute($stdid);
					
					$stid = @oci_parse($conn, "SELECT LASTLOGINDT FROM RETUSERLOGIN WHERE USERID='" . $_SESSION["USERID"] . "' order by LASTLOGINDT desc");
					// Execute Query
					@oci_execute($stid);
					$logindetails = @oci_fetch_all($stid, $res);
					if ($logindetails == 1 || $logindetails == 0) {
						$_SESSION["lastLogin"] = @$loginTime;
					} else {
						$_SESSION["lastLogin"] = $res['LASTLOGINDT'][1];
					}
					header("location:index.php?message=Please login!");
					exit;

				//end of redirect to dashboard page				
				
				$success=true;
			} else {
				$error=4;
				$success=false;	
			}
		} else {
			$error=3;
			$success=false;	
		}
	} else {
	$error=2;
	$success=false;	
	}
} else {
$error=1;
$success=false;	
}
if($success==false){
	//header("location:index.php?data=".base64_encode($error));
	//exit;
}
//echo $error;
?>

<!DOCTYPE html>
<html lang="en">
    <head>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
        
            <!--[if IE]>
            <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
        <![endif]-->
        <!--[if lt IE 9]>
              <script src="js/html5.js"></script>
            <![endif]-->    
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Religare Register</title>
        <meta name="description" content="{$METADESC}">
        <meta name="keywords" content="{$METAKEYWORDS}"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="">
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css" >
		
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="css/stylesheet.css" >
        <link rel="stylesheet" type="text/css" href="css/styles.css" >
		<link rel="stylesheet" type="text/css" href="css/jquery-ui.css" >
		<link rel="stylesheet" type="text/css" href="css/style.css" >
<?php $browser = get_browser(null, true);
if ($browser['browser'] == 'IE' && $browser['version'] == '8.0') {
    ?>
    <script type="text/javascript" src="js/jquery.js"></script> 	
<?php } else { ?>
    <script type="text/javascript" src="js/jquery.min.js"></script>
<?php } ?>
		
		
		<link href="css/owl.carousel.css" rel="stylesheet">
		<link href="css/owl.theme.css" rel="stylesheet">	
		<script language="javascript" src="js/encrypt.js"></script>
		<script src="js/common.js"></script>
		  
<script type="text/javascript"> 
var aliceImageHost = '';
var globalConfig = {};     
</script>		
<script type="text/javascript">
function ranvir_tab(en)
{
    var tabcont_id  = $(en).attr("id");
    
    $(".claimdisplayContent").hide();
    $("#"+tabcont_id+"tab, .service_tabs").show();
    
    $(".claimdisplaynav ul li a").removeClass("activetab");
    $(en).addClass("activetab");

}

function ranvir_tabS(en)
{
    var tabcont_id  = $(en).attr("id");
    
    $(".service_tabs").hide();
    $("#"+tabcont_id+"tab").show();
    
    $(".claimdisplaynav1 ul li a").removeClass("activetab");
    $(en).addClass("activetab");

} 
function regeneratePassword(email,type){
	$.ajax({
		type:'post',
		url:'ajax/get_data.php',
		data:{'type':type,'email':email},
		success:function(res){
			if(res.status==1){
				alert('Passwoed reset Link has been emailed to youe email id.');
			} else {
				alert("Error: Please try after some time.");	
			}
		}
		
		});
}
</script>
<script>
	$(document).ready(function() {
	$(".rightnavBox ul li a.claimcl").click(function(){
	$('#clickarrowDivServices, .clickarrow').hide();
	$(this).parent().children("div").show();	
	$('#clickarrowDivClaims').show();
});

$(".rightnavBox ul li a.servicecl").click(function(){
	$('#clickarrowDivClaims, .clickarrow').hide();
	$('.service_tabs').hide();
	$(this).parent().children("div").show();	
	$('#clickarrowDivServices, #tax_receiptstab').show();
        
    $(".claimdisplaynav1 ul li a").removeClass("activetab");
    $("#tax_receipts").addClass("activetab");
});

$(".grCross").click(function(event){
    $('.clickarrow, .clickarrowDiv').hide();
//    event.stopPropagation();
});

$("#loginNav").click(function(){
	$('#clickarrowDivServices,#clickarrowDivClaims, .clickarrow, .service_tabs').hide();
});
$('.navigation, .nav1').click(function(event) {
  $('html').one('click',function() {
	$('.nav1').hide();
  });
  event.stopPropagation();
});

$(".rightnavBox ul li #loginNav").click(function(){
	$('.loginDiv').toggle();
});

$('#loginNav, .loginDiv').click(function(event) {
  $('html').one('click',function() {
	$('.loginDiv').hide();
  });

  event.stopPropagation();
});
	});
</script>
</head>
<body>

<!-- header -->
<div class="headerMain">
</div>
        <!-- body container -->
        <section id="body_container"> 
            <div class="right-Cont" id="login_section">
                <h1>Login to your Account</h1>
                <!-- Heding top  closed-->
                <div class="clearfix"></div>
                <div class="spacer4"></div>
                <!-- Step start -->
                <div class="right-Cont-step1">
				 <form action="" name="resetForm" id="resetForm" method="post">
                    <div class="middleContainerBox">
						<h2>Please Choose Your New Password</h2>
                        <?php if(isset($error) && $error==3){ ?>
							<h3><div class="error2">Your temporary password reset link has been expired. Please <a href="javascript:void(0);" onClick="regeneratePassword('<?php echo $email; ?>','REGENERATEPASSWORD');">Click here</a> to regenerate it.</div></h3>
                        <?php } else if($error > 0 && @$_POST['submit']!=''){?>
                        <h3><div class="error2">Unauthorised access.</div></h3>
                        <?php } else if($error!=4) { ?>
						<h3><div class="error2">something went wrong.</div></h3>
                        <?php } else {  }?>
					 <div class="middleContainerBox">
                     <div id="errorMsg1" class="customerrormsg"></div>
                     
                      <h4>New Password</h4>
                            <div class="graytxtBox3 space-marging marginspl" >
                           
                            <span class="txtIconnew"></span>
                                <div class="txtMrgn ">
								<input type="password" value="" placeholder="" class="txtfieldFull" id="newpassword" name="newpassword">
								</div>
                            </div>
                            </div>
                            <div class="middleContainerBox">
                            <h4>Confirm Password</h4>
                            <div class="graytxtBox3" style="margin-bottom:5%;">
                            
                            <span class="txtIconnew"></span>
                                <div class="txtMrgn">
									<input type="password" value="" placeholder="" class="txtfieldFull" id="cpassword" name="cpassword">
                                    <input type="Submit" name="" style="display:none"  />
                                    <input type="hidden" name="change" value="submit">
                                </div>
                            </div>
                       
						</div>
                    </div>
					</form>
                    <div class="right-Cont-step1-button">
					<a href="javascript:void(0);" onClick="$('#resetForm').submit();">Continue<img src="images/arrow.png"  border="0" alt="" title=""></a>  
                    </div>
				</div>
            </div>
   <!-- Forgot password section ends -->
<!-- Activate Account section starts-->
	
   <!-- Activate Account section ends-->
            <!-- Left container -->
            <div class="Left-Cont">
			  <?php include("inc/inc.left-registration.php");?>
			</div>
        </div>
    </section>
    <div class="clearfix"></div>
    <!-- body container --> 
    <!-- footer -->
    <footer class="footer">
     <?php include("inc/inc.ft-home.php"); ?> 	
    </footer>
    <!-- footer closed -->
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/additional-methods.min.js" type="text/javascript"></script>
<script src="js/owl.carousel.js" type="text/javascript"></script>
	
<script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true

      });
    });
    </script>	
<!-- Demo --> 
<script>
		$("#resetForm").validate({
			ignore: [], // this will allow to validate any hidden field inside a form
		rules :{
			"newpassword" : {
				required : true,
			    minlength: 8,
			    custompassvalidate: true,
			},
			"cpassword" : {
			   required : true,
			   equalTo: "#newpassword"
			}
		},
		messages :{
			"newpassword" : {
				required : 'Please enter new password'
			},
			"cpassword" : {
				required : 'Please enter confirm password'
			}
		},
		errorElement: "div"		
});
// add extra custom validation method created by amit kumar dubey on 19 january 2016 at 10:28 AM
$.validator.addMethod('custompassvalidate', function(value, element) {
        return this.optional(element) || (value.match(/[a-zA-Z]/) && value.match(/[0-9]/));
    },
    'Password must contain at least one numeric and one alphabetic character.');

</script>	
<style>
div.error {
	position:absolute;
	margin-top:3px;
	color:red;
	padding:3px;
	text-align:left;
	z-index:1;
	margin-left: -47px;
}
div.error2 {
	position:absolute;
	margin-top:3px;
	color:red;
	padding:3px;
	text-align:left;
	z-index:1;
	margin-left: 0px;
}
.owl-theme .owl-controls {
margin-top: -152px;
}
</style>

</div>
</body>
</html>