<?php
$isHttps = !empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) != 'off';

if ($isHttps) {
    header('Strict-Transport-Security: max-age=31536000'); // FF 4 Chrome 4.0.211 Opera 12
}

include_once("conf/conf.php");    //include configuration file to access database connectivity
include_once("conf/common_functions.php"); //include common_functions file to access defined function
$policynum = sanitize_data(base64_decode(@$_REQUEST['policy']));
//include_once("securimage/securimage.php");
include("inc/inc-header-registration.php");
$browser = get_browser(null, true);
?>
<script src="js/bootstrap.min.js"></script>
<style>
    div.error {
        position:absolute;
        margin-top:3px;
        color:red;
        padding:3px;
        text-align:left;
        z-index:1;
        margin-left: -47px;
    }
    div.error2 {
        position:absolute;
        margin-top:3px;
        color:red;
        padding:3px;
        text-align:left;
        z-index:1;
    }
    #Password-error{
        width:260px;
    }
    .owl-theme .owl-controls {
        margin-top: -146px;
    }
</style>
</head>
<body>
    <!-- header -->
    <div class="headerMain">
        <?php include('pphcheader.php'); ?>
    </div>
    <link rel="stylesheet" type="text/css" href="css/datepiker.css" >
    <!-- header -->

    <?php
    if ($_SERVER["REQUEST_METHOD"] == "POST") {   //condition to insert or update data into/from database
//$img = new Securimage(); 
//$valid = $img->check(sanitize_data(@$_POST['code'])); 
//$message = '';
        $valid = true;
        $message = '';

        if (@$valid == true) {

// start of captcha validation true	

            @$empname = sanitize_name($_POST['empname']);

            @$empid = sanitize_data($_POST['empid']);
            if (!empty($empid)) {
                $empId = str_pad($empid, 10, "0", STR_PAD_LEFT);
            }
            @$companyId = sanitize_data($_POST['companyId']);

            @$policyNumber = sanitize_data($_POST['policyNumber']);
            @$MobileNo = sanitize_data($_POST['Numbers']);
            @$CustomerId = sanitize_data($_POST['CustomerId']);
            if (@$CustomerId == 'Corporate Id') {
                @$CustomerId = '';
            } else {
                @$CustomerId = @$CustomerId;
            }

            //@$Password = sanitize_email($_POST['Password']);
            @$Password = sanitize_password($_POST['Password']);
            if($Password !=$_POST['Password']){
                $message .="<span style='color:red' >Invalid characters in password.</span>"."<br />";
            }
            @$Password1 = md5($Password);
            @$email = sanitize_email($_POST['email']);
            $entryTime = date('d-m-Y H:i');
 	    if (strlen(@$empId) !='10') {
                    $message .="<span style='color:red' >Please enter correct employee id.</span>"."<br />";
                   
                   }
                if (strlen(@$MobileNo) !='10') {
                    $message .="<span style='color:red' >Please enter correct mobile No.</span>"."<br />";
                   
                   }
                if (!filter_var(@$email, FILTER_VALIDATE_EMAIL)) {
                    $message .="<span style='color:red' >Please enter a valid email address.</span>"."<br />";
                   
                    }
            $array_exist = fetchcustempid("CRMEMPLOYEELOGIN", " EMAIL ='" . @$email . "' AND STATUS = 'ACTIVE'");

            $polciy_deatils = fetchListByemailId("CRMPOLICY", "POLICYNUMBER", @$policyNumber);
            $policyActive = fetchcolumnListCond("POLICYID", "CRMPOLICY", "WHERE POLICYNUMBER='" . @$policyNumber . "' AND STATUS='ACTIVE' ");
            $policyId = @$polciy_deatils[0]['POLICYID'];
            @$companyId = @$polciy_deatils[0]['COMPANYID'];
            $array_existempcustid = fetchcustempid("CRMEMPLOYEELOGIN", " (EMPLOYEEID='$empid' OR CUSTOMERID='$CustomerId') and POLICYID = " . @$policyId . "");

            $query['CHDRNUM'] = $policyNumber; 
            $query['EMPNO'] = $empId;
            $dataArray=getPolicyCorporateEnquiry($query); 
            $dataRecord = $dataArray['dataArray'][0];
            if (!empty($policyId) && isset($policyId)) {
                if (!empty($policyActive) && isset($policyActive)) {
                    if (count($array_exist) == 0 && count($array_existempcustid) == 0) {
                        if(@$dataRecord['BGEN-MEMNAME']['#text'] || @$dataRecord['BGEN-EMPNO']['#text']){ 

                        //check password not exist with same emailid
                        $passwordQuery = fetchcolumnListCond("EMAIL,LOGINID,ISPASSWORDCHANGED,STATUS", "CRMEMPLOYEELOGIN", "WHERE EMAIL='" . @$email . "' AND ENCRYPTPASSWORD='" . $Password1 . "' ");
                        if (count($passwordQuery) == 0) {
                            $sql = "INSERT INTO CRMEMPLOYEELOGIN ( LOGINID,COMPANYID,EMPLOYEENAME,EMPLOYEEID,POLICYNUMBER,POLICYID,MOBILENUMBER,CUSTOMERID,PASSWORD,ENCRYPTPASSWORD,EMAIL,CREATIONDATE,CREATEDBY,STATUS)values(CRMEMPLOYEELOGIN_SEQ.nextval,'" . @$companyId . "',q'[" . $empname . "]','" . @$empid . "','" . @$policyNumber . "','" . $policyId . "','" . @$MobileNo . "','" . @$CustomerId . "','" . @$Password . "','" . @$Password1 . "','" . @$email . "','" . @$entryTime . "','" . @$_SESSION['userName'] . "','ACTIVE')";                         //query to insert records                            		 
                            $stdid = @oci_parse($conn, $sql);
                            $r = @oci_execute($stdid);
                            /* $email='';
                              $_POST['email']=''; */
                            $compseoName = fetchListByColumnName('CRMCOMPANY', 'SEOTITLE', 'COMPANYID', $companyId);
                            if (count($compseoName) > 0) {
                                $compTitle = $compseoName[0]['SEOTITLE'];
                            } else {
                                $compTitle = '';
                            }
                            SendMailToEmployee($email, $Password, $compTitle);
                            $message .="<span style='color:green' >Inserted Successfully</span>";
                            //header("location:company.php?msg='success'&company-".@$_SESSION['companyame1']);	
                        } else {
                            $message .="<span style='color:red' >You have used this password recently, Please choose a different one.</span>" . "<br />";
                        }
                        ?>
                        <!--locate to employee_list.php page -->
                        <?php } else { 
                            $message .="<span style='color:red' >Entered Employee no and Policy no is not matched with the record.</span>" . "<br />";
                        }
                        
                        } else {
                        if (count($array_existempcustid) > 0) {

                            if ($array_existempcustid[0]['EMPLOYEEID'] == @$empid) {
                                $message .="<span style='color:red' >Employee id already exist</span>" . "<br />";
                            }
                            if ($array_existempcustid[0]['CUSTOMERID'] == @$CustomerId) {
                                $message .="<span style='color:red' >Customer id already exist</span>" . "<br />";
                            }
                           
                        }

                        if (count($array_exist) > 0) {
                            $message .="<span style='color:red' >Email id already exist</span>" . "<br />";
                            ?>

                            <?php
                        }
                    }
                    // end of captcha validation true
                } else {
                    $message .="<span style='color:red' >Policy No entered is expired. Please register with active Policy No.</span>" . "<br />";
                }
            } else {
                $message .="<span style='color:red' >Policy Number Not Exists</span>" . "<br />";
            }
        } else {
            $message .="<span style='color:red' >Incorrect Code</span>" . "<br />";
        }
    }
    ?>
    <!-- body container -->
    <section id="body_container"> 
        <div class="right-Cont">
            <h1>Corporate Policy Registration</h1>
            <!-- Heding top  closed-->
            <div class="clearfix"></div>
            <div class="spacer4"></div> 
            <!-- Step start --> 
            <div class="right-Cont-step1 spacer-margin-top"> 
                <h2>Please enter your details</h2>
                <script>
                    function submit_form()
                    {
                        $("#submit_forms_id").trigger("click");
                    }
                </script>
                <form method="post" action="" name="form1" id="corpRegForm"> <div style="text-align: center;">  <?php echo @$message; ?></div> 
                    <div class="middleContainerBox">
                        
                        <div class="graytxtBox3 space-marging "> 
                            <span class="txtIcon3"></span>
                            <div class="txtMrgn "><img src="images/question_mark.png" class="ques_mark" id="policy_no_help">
                                <input type="text" value="<?php echo sanitize_data(@$_POST['policyNumber']) ? sanitize_data(@$_POST['policyNumber']) : $policynum; ?>" name="policyNumber" class="txtfieldFull"  maxlength="8" id="policyNumber" AUTOCOMPLETE="OFF" placeholder="Policy No *" >
                            </div>
                        </div>

                        <div class="graytxtBox3">
                            <span class="txtIcon1"></span>			
                            <div class="txtMrgn"><input type="text" value="<?php echo sanitize_data(@$_POST['empname']) ?>" name="empname" id="empname" class="txtfieldFull" AUTOCOMPLETE="OFF" placeholder="Employee Name*">
                            </div>                  
                        </div>

                        <div class="graytxtBox3"> 
                            <span class="txtIcon1"></span>
                            <div class="txtMrgn"><input type="text" value="<?php echo sanitize_data(@$_POST['empid']) ?>" name="empid" class="txtfieldFull"  id="empid" AUTOCOMPLETE="OFF" placeholder="Employee ID *" maxlength="10" >
                            </div>
                        </div>

                        <div class="graytxtBox3">
                            <span class="txtIcon5"></span>			
                            <div class="txtMrgn"><input type="text" value="<?php echo @$_POST['email'] ?>" name="email" class="txtfieldFull"  id="email" AUTOCOMPLETE="OFF" placeholder="Email *" >
                            </div>
                        </div>
                        
                        <div class="graytxtBox3">
                            <span class="txtIcon1"></span>			
                            <div class="txtMrgn"><img src="images/question_mark.png" class="ques_mark" id="customer_id_help">
                                <input type="text" value="<?php echo sanitize_data(@$_POST['CustomerId']) ?>" name="CustomerId" class="txtfieldFull"  id="CustomerId" AUTOCOMPLETE="OFF" placeholder="Customer ID" readonly="readonly">
                            </div>
                        </div>

                        <div class="graytxtBox3"> 
                            <span class="txtIcon7"></span>
                            <div class="txtMrgn"><input type="text" value="<?php echo sanitize_data(@$_POST['Numbers']) ?>" name="Numbers" class="txtfieldFull"  id="Numbers" AUTOCOMPLETE="OFF" maxlength="10" minlength="10" placeholder="Mobile No*">
                            </div>
                        </div>
                        <div class="graytxtBox3 passMarginMob passMargin"> 
                            <span class="txtIconnew"></span>
                            <div class="txtMrgn">

                                <input type="password" value="" name="Password" class="txtfieldFull" id="Password" AUTOCOMPLETE="OFF" placeholder="Password *">
                            </div>
                        </div>

                        <div class="graytxtBox3 conPassMarginMob"> 
                            <span class="txtIconnew"></span>
                            <div class="txtMrgn">

                                <input type="password" value="" name="CPassword" class="txtfieldFull" id="CPassword" AUTOCOMPLETE="OFF" placeholder="Confirm Password *">
                            </div>
                        </div>
                    </div>
                    <input name="submit" type="submit" id="submit_forms_id" border="0" alt="submit" class="submit_pp" value="Submit" style="display:none;">
                    <div class="right-Cont-step1-button space-marging1">

                        <a style="margin-right:10px;" href="register.php" onClick="$('#login_section').show();
                                $('#forgot_pass_section').hide();"><img src="images/arrowleft.png"  border="0" alt="" title=""> Back </a>

                        <a onClick="submit_form();" >Submit <img src="images/arrow.png"  border="0" alt="" title=""></a>

                    </div>
                </form>

                <div class="spacer4"></div>
            </div>
        </div>

        <!-- Left container -->
        <div class="Left-Cont">
            <?php include("inc/inc.left-registration.php"); ?>
        </div>

    </div>
</section>
<div class="clearfix"></div>
<!-- body container -->
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/additional-methods.min.js" type="text/javascript"></script>
<script type="text/javascript">
                            $("#corpRegForm").validate({
                                ignore: [], // this will allow to validate any hidden field inside a form
                                rules: {
                                    "policyNumber": {
                                        required: true,
                                        minlength: 8,
                                        maxlength: 8,
                                        //noStartEndWhiteSpaces: true
                                    },
                                    "empname": {
                                        required: true,
                                    },
                                    "empid": {
                                        required: true,
                                    },
                                    "email": {
                                        required: true,
                                        email: true,
                                    },
                                    /*"CustomerId" : {
                                     number: true,
                                     minlength: 8,
                                     maxlength: 8, 
                                     },*/
                                    "Numbers": {
				        required : true,
                                        
                                    },
                                    "Password": {
                                        required: true,
                                        minlength: 8,
                                        custompassvalidate: true,
                                    },
                                    "CPassword": {
                                        required: true,
                                        equalTo: "#Password"
                                    }


                                },
                                messages: {
                                    "policyNumber": {
                                        required: 'Please enter your 8 digit policy no.',
                                        number: 'Please enter your 8 digit policy no.',
                                    },
                                    "empname": {
                                        required: 'Please enter employee name'
                                    },
                                    "empid": {
                                        required: ''
                                    },
                                    "email": {
                                        required: 'Please enter a valid Email ID'
                                    },
                                    /*"CustomerId" :{
                                     required:'Please enter Customer ID' 
                                     },*/
                                    "Numbers": {
                                        required: 'Please enter your 10 digits Mobile No'
                                       
                                    },
                                    "Password": {
                                        required: 'Please enter a valid password'
                                    },
                                    "CPassword": {
                                        required: 'Please enter confirm password',
                                        equalTo: 'Please enter the same password again.'
                                    },
                                },
                                errorElement: "div"
                            });
// custom rule for checking . and character after . for email validation
                            jQuery.validator.addMethod("email", function (value, element) {
                                return this.optional(element) || (/^[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}$/.test(value) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(value));
                            }, 'Please enter valid email address.');

                            $.validator.addMethod('custompassvalidate', function (value, element) {
                                return this.optional(element) || (value.match(/[a-zA-Z]/) && value.match(/[0-9]/));
                            },
                                    'Password must contain at least one numeric and one alphabetic character.');
$.validator.addMethod('validatename', function(value, element) {
        return this.optional(element) || (/^[a-z][a-z\s]+$/i.test(value));
    },
    'Please enter alphabet only');
 
    

</script>
<script src="js/owl.carousel.js" type="text/javascript"></script>	
<script>
                            $(document).ready(function () {
                                $("#owl-demo").owlCarousel({
                                    navigation: true,
                                    slideSpeed: 300,
                                    paginationSpeed: 400,
                                    singleItem: true
                                });
                            });
</script>

<?php if ($browser['browser'] == 'IE' && $browser['version'] == '8.0') { ?>
    <script type="text/javascript" src="js/jquery.js"></script>   
<?php } else { ?>
    <script type="text/javascript" src="js/jquery.min.js"></script>
<?php } ?>
<script src="js/datepiker.js"></script> 
<script>
                            var DT = jQuery.noConflict();
                            DT(function () {
                                DT('#datepicker').datepicker({
                                    changeMonth: true,
                                    changeYear: true,
                                    yearRange: "-99:-0",
                                    dateFormat: "dd/mm/yy"

                                });
                                DT('#dob').datepicker({
                                    changeMonth: true,
                                    changeYear: true,
                                    yearRange: "-99:-0",
                                    dateFormat: "yy-mm-dd"

                                });

                            });
                            
$('#policyNumber').bind('keyup blur',function(){
    var Pnum = $(this);
    Pnum.val(Pnum.val().replace(/[^0-9]/g,'') );
})                     
$('#empname').bind('keyup blur',function(){ 
    var empname = $(this);
    empname.val(empname.val().replace(/[^a-zA-Z ]/g,''));
    var match=/[a-zA-Z]+( [a-zA-Z]*)*/g.exec(empname.val());
    var s="";
    if(match!=null) if(match.length>0){ 
        s=match[0].replace(/[ ][ ]+/,' '); 
    }
    empname.val( s ); }
);
$('#Numbers').bind('keyup blur',function(){ 
    var mobile = $(this);
    mobile.val(mobile.val().replace(/[^0-9]/g,'') ); }
);

$('#empid').bind('blur',function(){     
    var emp = $("#empid").val();  
    if(emp && emp!=0){
     var empval = emp.replace(/[^a-zA-Z0-9_-]/g,'');
     $("#empid").val(empval.length < 10 ? pad(10, empid, "0" + empval) : empval); 
    }
        }
);

</script>
<style>
    .popover{max-height: 170px;height: auto!important; border-radius: 6px;padding: 0;width:auto!important; max-width: none; left:auto !important;}
    .popover .popover-title {max-height: 170px;height: auto!important;padding: 0;width:auto!important; max-width: none; left:auto !important;}
</style>
<script>
$(document).ready(function(){
    $('#customer_id_help').popover({
        placement : 'auto',
        trigger : 'click hover focus',
        html : true,
        title : '<img src="images/customer-id-help.png">',
    }); 
    $('#policy_no_help').popover({
        placement : 'auto',
        trigger : 'click hover focus',
        html : true,
        title : '<img src="images/policy-no-help.png">',
    });
});
</script>
<?php include("inc/inc.ft-registration.php"); ?>
</div>
</body>
</html>
