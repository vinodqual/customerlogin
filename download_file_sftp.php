<?php

/**
 * 
 * @author Sanjeev Kumar
 * @todo This file created to download file from SFTP.
 * @uses sftp_connect.php. This file created in projectconfig folder.
 * @version 1.0
 * File created on 06-Feb-2017
 * 
 */

require_once("conf/conf.php");				//include conf file
//include_once("conf/session.php");				//include session file
require_once(__DIR__.'/classes/sessionHandlerDB.php');
$handler = new sessionHandlerDB();
session_start();
if ((trim(@$_SESSION["userName"])=="") || (@$_SESSION["userName"]=="0")  || (@$_SESSION["USERID"]==""))	{
    session_destroy();
    $url=base_url(CUSTOMERLOGIN);
    if($url!==FALSE){
        header("Location: ".$url);
        exit;
    }else{
        echo 'Invalid Redirection';die;
    }
    
}else{
    if(isset($_GET['folder_name'],$_GET['file_name'])){     // These variables defines in .htaccess
    $folder_name=$_GET['folder_name'];
    $file_name=$_GET['file_name']; 
    
    $file_name=decode_data($file_name);
    $folder_name=decode_data($folder_name);
    $response_sftp_arr=sftp_connection();               // Connect to SFTP Server
    if(!empty($response_sftp_arr) && ($response_sftp_arr['status']=='SUCCESS')){
        $response_sftp=$response_sftp_arr['response_sftp'];
        $filepath=UPLOAD_FOLDER.$folder_name.'/';
        if(file_exists("ssh2.sftp://{$response_sftp}{$filepath}{$file_name}")){
            $data = file_get_contents("ssh2.sftp://{$response_sftp}{$filepath}{$file_name}");
            file_put_contents($file_name,$data);
            header('Content-type: application/force-download');
            header('Content-Disposition: attachment; filename="'.$file_name.'"');
            header('Content-Length: ' . filesize($file_name));
            readfile($file_name);
            unlink($file_name);
        }else{
            echo 'File does not exist on server.';
        }
    }else{
        echo $response_sftp_arr['msg']; 
    }
}
else{
    echo "Unable to Find related File.";   
}
}

?>
