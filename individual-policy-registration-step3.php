﻿<?php  $isHttps = !empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) != 'off';
 
if ($isHttps)
{
  header('Strict-Transport-Security: max-age=31536000'); // FF 4 Chrome 4.0.211 Opera 12
}
include_once("conf/conf.php");
include_once("conf/common_functions.php");

$successSameAccount ='';
$newpolicyNewAccount ='';
if(isset($_SESSION['successSameAccount']) && $_SESSION['successSameAccount'] !='')
{
	$successSameAccount = 'successSameAccount';
}
if(isset($_SESSION['newpolicyNewAccount']) && $_SESSION['newpolicyNewAccount'] !='')
{
	$newpolicyNewAccount = 'newpolicyNewAccount';
}
include("inc/inc-header-registration.php");
$browser = get_browser(null, true);
?>

<style>
div.error {
	position:absolute;
	margin-top:3px;
	color:red;
	padding:3px;
	text-align:left;
	z-index:1;
	margin-left: -47px;
}
#chkbox-error{
	float: left;
    margin-left: 12px;
    margin-top: 45px;
}
.owl-theme .owl-controls {
    margin-top: -146px;
	}
</style>
</head>
<body>
<?php $Error_msg = $_GET['error'];?>
<!-- header -->
 <div class="headerMain">
<?php include('pphcheader.php'); ?>
</div>
<link rel="stylesheet" type="text/css" href="css/datepiker.css" >
<!-- body container -->
<section id="body_container">
  <div class="right-Cont">
    <h1>Individual Policy Registration</h1>
    
    <!-- Heding top  closed-->
    <div class="clearfix"></div>
    <div class="spacer4"></div>
    
    <!-- Step start -->
    
    <div class="right-Cont-step1 ">
      <div class="spacer3"></div>
	  <div class="otp-errors"></div>
	  <form id="otpForm" name="otpForm" method="post">
      <div class="middleContainerBox space-marging-1 " id="otpDiv">
	  <?php if (isset($Error_msg) && $Error_msg == 'fail') { ?>
      <div style="height:20px; margin-bottom:10px; color: red;">Account can not be created this time !</div>
        <?php }  ?>
        <div class="graytxtBox3 space-marging "><span class="txtIcon1"></span>
		
          <div class="txtMrgn ">
            <input type="text" value="OTP *" maxlength="7" class="txtfieldFull" id="otp" name="otp" onFocus="if (this.value == 'OTP *') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'OTP *';}">
			
          </div>
        </div>
				
		<?php 
			if($successSameAccount == 'successSameAccount'){
		?>	
		<div class="right-Cont-step1-button-2 space-marging1" id="successSameAccount" > <a href="#">Confirm <img src="images/arrow.png"  border="0" alt="" title=""></a>
		
		<?php	}
		else if($newpolicyNewAccount == 'newpolicyNewAccount'){
		?>
		<div class="right-Cont-step1-button-2 space-marging1" id="newpolicyNewAccount" > <a href="#">Confirm <img src="images/arrow.png"  border="0" alt="" title=""></a>
		 <?php 
			}	
			else if($successSameAccount != 'successSameAccount' && $newpolicyNewAccount != 'newpolicyNewAccount')
		{	
		?>	
		 <div class="right-Cont-step1-button-2 space-marging1" id="confirmOTP"> <a href="#">Confirm <img src="images/arrow.png"  border="0" alt="" title=""></a>
		<?php
		}		
		?>
		  
		<div id="ajaxLoading" style="display:none; float:right; position:absolute; margin-left:10px;">
		Please wait...<img class="doc_load_img" alt="Loading.." src="images/ajax-loader_12.gif">
		</div>
		</div>
		
          </div>
		  
		  </form>
      <div class="middleContainerBox space-marging-1" id="registrationDiv" style="display: none">
          
      <form action="#" name="registrationForm1" id="registrationForm1" method="post">
        <div class="clearfix"></div>
        <h2>Set your account password</h2>        
        <div class="graytxtBox3"><span class="txtIconnew"></span>
                <div class="txtMrgn">
                <input type="text" value="Password *" class="txtfieldFull" id="fake_Password" style="display:none"  name="fake_Password" onFocus="if (this.value == 'Password *') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Password *';document.registrationForm1.password.value = '';}">
              <input type="password" value="" class="txtfieldFull" id="password" name="password" AUTOCOMPLETE="OFF" maxlength="25" placeholder="Password*">  
                </div>
            </div>
        
			<div class="graytxtBox3"><span class="txtIconnew"></span>
                <div class="txtMrgn">
                <input type="text" value="Confirm Password *" class="txtfieldFull" id="fake_cnfPassword" style="display:none"  name="fake_cnfPassword" onFocus="if (this.value == 'Confirm Password *') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Confirm Password *';document.registrationForm1.cnfpassword.value = '';}">
              <input type="password" value="" class="txtfieldFull" id="cnfpassword" name="cnfpassword" AUTOCOMPLETE="OFF" maxlength="25" placeholder="Confirm Password*">  
                </div>
            </div>
        
        <div class="clearfix"></div>
        <div class="right-Cont-step1-button-2 space-marging1"> </div>
        <div class="right-Cont-step1-button space-marging1" style="padding-top:2%;" id="createAccount2"> <a href="javascript:void(0)">Submit & Create my Account <img src="images/arrow.png"  border="0" alt="" title=""></a> 
		<div id="ajaxLoading2" style="display:none; float:right; position:absolute; margin-left:10px;">
		Please wait...<img class="doc_load_img" alt="Loading.." src="images/ajax-loader_12.gif">
		</div>
		</div>
		
        <div class="spacer"></div>
        <div class="insuredQuestioncheckBox" style="margin:0; padding:0; margin-left:-15px">
          <div class="insuredQuestioncheckRight">
            <div class="insuredQuestioncheck">
              <input type="checkbox" id="test1" name="chkbox">
              <label for="test1"></label>
            </div>
            <div class="insuredQuestionc-left"> T&C </div>
			 <div class="errorTxt" id="customError"></div>
          </div>
        </div>
        
        </form>
        </div>
        
        <div class="spacer3"></div>
      </div>
      <div class="spacer4"></div>
    </div>
  </div>
  
  <!-- Left container -->
   <div class="Left-Cont">
       <?php include("inc/inc.left-registration.php");?>
    </div>
  
  </div>
</section>
<div class="clearfix"></div>
<!-- body container --> 
<!-- footer -->
<!-- footer closed --> 
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/additional-methods.min.js" type="text/javascript"></script>
 <script type="text/javascript">
		$("#registrationForm1").validate({
			ignore: [], // this will allow to validate any hidden field inside a form
			// this condition will work to display error message for check box at custom place
			errorPlacement: function(error, element) {
				if (element.attr("name") == "chkbox" )
					error.insertAfter(".errorTxt");
				else
					error.insertAfter(element);
			},
			
		rules :{
			"password" : {
			   required : true,
			   minlength: 8,
			   custompassvalidate: true,
			},
			"cnfpassword" : {
			   required: true,
			   equalTo: "#password"
			},
			"chkbox" : {
			   required: true,
			}
			
		},
		messages :{
			"password":{
				required:'Please enter password'
			},
			"cnfpassword":{
				required:'Please enter confirm password',
				equalTo: 'Please enter the same password again.'
			},
			"chkbox":{
				required:'Please check on T&C to agree.',
			},
		},	
		errorElement: "div",	
});
$.validator.addMethod('custompassvalidate', function(value, element) {
        return this.optional(element) || (value.match(/[a-zA-Z]/) && value.match(/[0-9]/));
    },
    'Password must contain at least one numeric and one alphabetic character.');
</script>	
<?php require_once 'conf/conf.php'; ?>
<script type="text/javascript">
$("#otp").keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
		$('#confirmOTP').trigger('click');   
    }
});
 $("#cnfpassword").keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
		$('#createAccount2').trigger('click');   
    }
}); 

$(function() {
 $('#createAccount2').click(function(){
	$('#registrationForm1').validate();
	if ($('#registrationForm1').valid()) // check if form is valid
	{
		var password = $("#password").val();
	   $.ajax({
		type: 'POST',
		url : 'createaccount.php',
		data: {password: password},
		beforeSend: function () {
		$("#ajaxLoading2").css("display", "inline");
		$("#ajaxLoading2").show();
	   },
		success: function (data)
			{
			if(data == 'success')
			{               
				window.location.href="index.php?accountcreated=yes";
			}
			},
			complete: function(){
			$('#ajaxLoading2').hide();
			}
	}); 
	}
	else 
	{
		return false;
	}
});
});
</script>
<script>

$(".editQuote, .editNav").click(function (evt) {
    $(".selectParent select").removeAttr('disabled');
	$(".selectParent").addClass('selectParentEnable');
	$("#mobid").show();
	$(".tooltipgr").hide();
	$(".editQuote").hide();
	$("#submitid").show();
	$(".yourQuoteSummary").show();
});

$(".maleIcon").click(function () {
	$(".maleIcon").toggleClass('maleIconActive');
	$(".femaleIcon").removeClass('femaleIconActive');
});
$(".femaleIcon").click(function () {
	$(".maleIcon").removeClass('maleIconActive');
	$(".femaleIcon").toggleClass('femaleIconActive');
});


$("#confirmOTP").click(function () {
    var otp = $("#otp").val();
    if(otp == 'OTP *' || otp =='')
    {
		$(".otp-errors").text("Please fill OTP.").show();
        
        return false;
    }
    if(!$.isNumeric(otp) || otp.length!=7)
    {
      
		$(".otp-errors").text("OTP is not correct.").show();
        return false;
    }
    if($.isNumeric(otp) && otp.length==7)
    {
        $.ajax({
			type: 'POST',
			data: {'code': otp},
			url: 'validateOTP.php',
			beforeSend: function () {
			$("#ajaxLoading").css("display", "inline");
			$("#ajaxLoading").show();
		   },
			success: function(data){
				if(data == 'success')
				{
					$("#otpDiv").hide();
					$(".otp-errors").hide();
					$("#registrationDiv").show();
				}
				else if(data = 'incorrectOTP')
				{
					$(".otp-errors").text("OTP is not correct.").show();
					
				}
			},
			complete: function(){
			$('#ajaxLoading').hide();
			}
        })
    }
});
</script> 
<script>
$("#successSameAccount").click(function () {
    var otp = $("#otp").val();
    if(otp == 'OTP *' || otp =='')
    {
		$(".otp-errors").text("Please fill OTP.").show();
        
        return false;
    }
    if(!$.isNumeric(otp) || otp.length!=7)
    {
       // alert("OTP is.");
		$(".otp-errors").text("OTP is not correct.").show();
        return false;
    }
    if($.isNumeric(otp) && otp.length==7)
    {
        $.ajax({
			type: 'POST',
			data: {'code': otp,'successSameAccount' : '<?php echo 'successSameAccount'; ?>' },
			url: 'validateOTP.php',
			beforeSend: function () {
			$("#ajaxLoading").css("display", "inline");
			$("#ajaxLoading").show();
		   },
			success: function(data){
				
				if(data == 'policymapped')
				{
					window.location.href="index.php?policymapped=yes";
				}
				else if(data = 'incorrectOTP')
				{
					$(".otp-errors").text("OTP is not correct.").show();
					//alert("OTP is not correct.");
				}
			},
			complete: function(){
			$('#ajaxLoading').hide();
			}
        })
    }
});
</script>
<script>
$("#newpolicyNewAccount").click(function () {
    var otp = $("#otp").val();
    if(otp == 'OTP *' || otp =='')
    {
		$(".otp-errors").text("Please fill OTP.").show();
        //alert("Please fill OTP.");
        return false;
    }
    if(!$.isNumeric(otp) || otp.length!=7)
    {
       // alert("OTP is.");
		$(".otp-errors").text("OTP is not correct.").show();
        return false;
    }
    if($.isNumeric(otp) && otp.length==7)
    {
        $.ajax({
			type: 'POST',
			data: {'code': otp,'newpolicyNewAccount' : '<?php echo 'newpolicyNewAccount'; ?>' },
			url: 'validateOTP.php',
			beforeSend: function () {
			$("#ajaxLoading").css("display", "inline");
			$("#ajaxLoading").show();
		   },
			success: function(data){
				
				if(data == 'newpolicynewac')
				{
					window.location.href="<?php echo _SITEURL_CUSTOMER.'index.php?newpolicynewac=yes';?>";
				}
				else if(data = 'incorrectOTP')
				{
					$(".otp-errors").text("OTP is not correct.").show();
					//alert("OTP is not correct.");
				}
			},
			complete: function(){
			$('#ajaxLoading').hide();
			}
        })
    }
});
</script>
<script src="js/owl.carousel.js" type="text/javascript"></script>	
<script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
		  navigation : true,
		  slideSpeed : 300,
		  paginationSpeed : 400,
		  singleItem : true
      });
    });
</script>

<?php 
if($browser['browser'] == 'IE' && $browser['version'] == '8.0'){ ?>
<script type="text/javascript" src="js/jquery.js"></script>   
<?php } else {  ?>
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<?php } ?>
<script src="js/datepiker.js"></script> 
<script>
 var DT = jQuery.noConflict();
DT(function() {
DT('#datepicker').datepicker( {
	changeMonth: true,
	changeYear: true,
	yearRange: "-99:-0",
	dateFormat: "dd/mm/yy"
 
    });
});
</script>
<?php include("inc/inc.ft-registration.php"); ?>
<style>
#password-error{
width:254px;
}
</style>
</div>
</body>
</html>