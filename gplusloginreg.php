<?php
/**
 * Google plus user authentication by Google. On successfull authentication
 * Insert user record into RETUSERAUTH
 * Insert user record into RETUSERMASTER
 * Redirect to dashboard.php
 * 
 * @author : Shakti Rana <shakti@catpl.co.in> 01 Dec 2015.
 */
require_once 'conf/conf.php'; 
require_once 'conf/common_functions.php';
require_once 'passwordverify/PkcsKeyGenerator.php';
require_once 'passwordverify/DesEncryptor.php';
require_once 'passwordverify/PbeWithMd5AndDes.php';
include_once("conf/staticFunctions.php");

error_reporting(E_ALL);
ini_set("display_errors", 1);


/* Google authentication */
require_once 'googleplus/src/Google_Client.php';
require_once 'googleplus/src/contrib/Google_Oauth2Service.php';

$client = new Google_Client();
$client->setApplicationName("Religare");
$authUrl = $client->createAuthUrl();
$oauth2 = new Google_Oauth2Service($client);
if (isset($_GET['code'])) {
  $client->authenticate($_GET['code']);
  $_SESSION['token'] = $client->getAccessToken();
  //$redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];  
  //header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
  //return;
}

if (isset($_SESSION['token'])) {
 $client->setAccessToken($_SESSION['token']);
}

if (isset($_REQUEST['logout'])) {
  unset($_SESSION['token']);
  $client->revokeToken();
}

if ($client->getAccessToken()) {
    $user = $oauth2->userinfo->get();
    $username = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
    $username = $email = strtolower($username);
    $fname = filter_var($user['given_name'], FILTER_SANITIZE_EMAIL);
    $lname = filter_var($user['family_name'], FILTER_SANITIZE_EMAIL);
    $gender = filter_var($user['gender'], FILTER_SANITIZE_EMAIL);
    //$img = filter_var($user['picture'], FILTER_VALIDATE_URL);
    $content = $user;
    $_SESSION['OTPDetail']['email'] = $email;
    if($gender=='male')
    {
    $_SESSION['OTPDetail']['gender']='M';
    }
    $_SESSION['OTPDetail']['firstName'] = $fname;
    $_SESSION['OTPDetail']['lastName'] = $lname;
    $_SESSION['token'] = $client->getAccessToken();
} 
else {
  $authUrl = $client->createAuthUrl();
}


//$password = $orgpassword =base64_decode($_POST['password']);
$password = $orgpassword = 'Welcome1$';
$password = PbeWithMd5AndDes::encrypt($password, $keystring); 

$_SESSION['userName'] = $username;
$_SESSION['LOGINTYPE'] = 'RETAIL';
$dob = @$_SESSION['OTPDetail']['dob'] ? $_SESSION['OTPDetail']['dob'] : date('y-m-d');
$title = (@$_SESSION['OTPDetail']['gender']=='M') ? 'Mr' : 'Mrs';
$firstName = @$_SESSION['OTPDetail']['firstName'];
$lastName = @$_SESSION['OTPDetail']['lastName'];
$mobile = @$_SESSION['OTPDetail']['mobile'];
$contactNum = '';
$stdCode = '';
$policyNo = @$_SESSION['OTPDetail']['policyNo'];
$customerId = @$_SESSION['OTPDetail']['customerId'];

$memberType = '';
$createdOn = date('y-m-d');

global $conn;
$gplusUser = fetchcolumnListCond('USERID,ISPASSWORDCHANGED', 'RETUSERAUTH', "where LOWER(USERNAME)='".strtolower($username)."'");
$_SESSION['ISPASSWORDCHANGED']=$gplusUser[0]['ISPASSWORDCHANGED'];
$_SESSION['USERID'] = $gplusUser[0]['USERID'];
if(!$gplusUser[0]['USERID'])
{
    $userAuthSql = "INSERT INTO RETUSERAUTH (USERID,USERNAME,PASSWORD,USERSTATUS,SOURCE,UNSUCCESSFULATTEMPTS,ISPASSWORDCHANGED) VALUES (RETUSERAUTH_SEQ.NEXTVAL,'$username','$password','ACTIVE','GPLUS',0,'YES')";
    $stdid1 = @oci_parse($conn, $userAuthSql) or die(mysql_error());

    if(@oci_execute($stdid1))
    {
        $check1 = @oci_parse($conn, 'SELECT RETUSERAUTH_SEQ.CURRVAL FROM DUAL');
        @oci_execute($check1);
        $res=@oci_fetch_assoc($check1);
        $rowid=@$res['CURRVAL'];
        $userMasterSql = "INSERT INTO RETUSERMASTER(USERID, EMAILID,BIRTHDT,TITLE,FIRSTNAME,LASTNAME,CONTACTNUM,CONTACTPHONENUM,STDCODE,ACCOUNTSTARTDT) VALUES ($rowid,'$email','$dob','$title','$firstName','$lastName','$mobile','','','$createdOn')";
        $stdid2 = @oci_parse($conn, $userMasterSql) or die(mysql_error());
        if(@oci_execute($stdid2))
        {
			$_SESSION['ISPASSWORDCHANGED']='YES';
			$_SESSION['USERID'] = $rowid;
            $emailmessage ='';
            $to      = $email;
            $subject = "You have successfully registered";
            $emailmessage .= "Dear, ".@$firstName.' '.@$lastName."<br /><br />";
            $emailmessage .="You have successfully registered.<br />";
            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
            $headers .= 'From: wellness@religare.com' . "\r\n" .
                            'Reply-To: wellness@religare.com' . "\r\n" .
                            'X-Mailer: PHP/' . phpversion();
            mail($to, $subject, $emailmessage, $headers);
            /*        
            if(mail($to, $subject, $emailmessage, $headers))
            { ?>
                <script>
                    sendMAil();
                    function sendMail(){
                        alert ("You have successfully registered.");
                    }
                </script>
            }
            */
        }
    }
/*
    function GetContentBySMSTemplateName($templateName)
    {
        global $conn;
        $dataArray=array();
        $query="SELECT TEMPLATEID,SUBJECT,CONTENT,FROMEMAIL,FROMNAME,TEMPLATENAME from CRMSMSTEMPLTE where TEMPLATENAME='".$templateName."' ";

            $sql = @oci_parse($conn, $query);
            @oci_execute($sql);
            $i=0;
            while (($row = oci_fetch_assoc($sql))) {
                    foreach($row as $key=>$value){
                            $dataArray[$i][$key] = $value;
                    }
                    $i++;
            }
        return $dataArray;
    }
*/
}
//@SHAKTIRANA 04/05/2016 15:31
CustomerLoginFunction::mapPolicyWithProposal($username);
CustomerLoginFunction::mapPolicyWithPolicymap($username);
//@SHAKTIRANA 04/05/2016 15:31

header("Location: dashboard.php");
?>
