<!--
/*****************************************************************************
* COPYRIGHT
* Copyright 2013 Catabatic Automation Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: sitedown.php,v 1.0 2013/04/02 12:30:17 ashish gupta Exp $
* $Author: ashish gupta $
*
****************************************************************************/
-->


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Religare</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link media="screen" rel="stylesheet" href="css/colorbox.css" />
<script src="js/jquery.min.js"></script>
</head>
<body style="background:#b7db82;">
<script src="js/wz_tooltip.js"></script>
<div class="Tractus">
  <div class="TractusCount fl">
    <div class="TractusCount_top fl">
      <div class="TractusCount_top_left fl"><img src="images/tactus_logo.jpg" alt="" title="" border="0" /></div>
      <div class="TractusCount_top_right fr"><img src="images/tractus_employee_interface_logo.jpg" alt="" title="" border="0" /></div>
    </div>
    <div class="TractusCount_mid fl">
      <div class="TractusCount_mid_bot fl">
        <h1 style="text-align:center"><strong>Site Down For Maintenance</strong></h1>
       <p style="min-height:150px">&nbsp;</p>
      </div>
    </div>
    <div class="TractusCount_bot fl">
      <p> Insurance is the subject matter of solicitation | IRDA Registration No. 148<br />
        Copyrights <?php echo date('Y');?>, All right reserved by Religare Health Insurance Company Ltd.<br />
        <span>This site is best viewed on Internet Explorer 7/8 and Fire Fox 3.x</span> </p>
    </div>
  </div>
</div>
<!-- footer End-->
</body>
</html>
