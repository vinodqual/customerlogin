<?php
/*****************************************************************************
* COPYRIGHT
* Copyright 2013 Catabatic Automation Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: common_functions.php,v 1.0 2013/04/01 17:30:17 ashish gupta Exp $
* $Author: ashish gupta $
*
****************************************************************************/
//function to sanitize data
function sanitize_data($input_data) {
	$searchArr=array("document","write","alert","%","@","$",";","+","|","#","<",">",")","(","'","\'",",");
	$input_data= str_replace("script","",$input_data);
	$input_data= str_replace("iframe","",$input_data);
	$input_data= str_replace($searchArr,"",$input_data);
	return htmlentities(stripslashes($input_data), ENT_QUOTES);
}
function sanitize_name($input_data) {
	$searchArr=array("document","write","alert","%","$",";","+","|","#","<",">",")","(");
	$input_data= str_replace("script","",$input_data);
	$input_data= str_replace("iframe","",$input_data);
	$input_data= str_replace($searchArr,"",$input_data);
    $input_data=trim($input_data);
	return htmlentities(stripslashes($input_data), ENT_QUOTES);
}

//function to sanitize input username created by amit kumar dubey on 25/september/2015 at 1:55PM
function sanitize_username($input_data) {
	$searchArr=array("document","write","alert","%","$",";","+","|","#","<",">",")","(","'","\'",",");
	$input_data= str_replace("script","",$input_data);
	$input_data= str_replace("iframe","",$input_data);
	$input_data= str_replace($searchArr,"",$input_data);
    $input_data=trim($input_data);
	return htmlentities(stripslashes($input_data), ENT_QUOTES);
}
function sanitize_email($input_data) {
	$searchArr=array("document","write","alert","%","$",";","+","|","#","<",">",")","(","'","\'",",");
	$input_data= str_replace("script","",$input_data);
	$input_data= str_replace("iframe","",$input_data);
	$input_data= str_replace($searchArr,"",$input_data);
	$input_data=trim($input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}
function sanitize_simple($input_data) {
	$searchArr=array("document","write","alert");
	$input_data= str_replace("script","",$input_data);
	$input_data= str_replace("iframe","",$input_data);
	$input_data= str_replace($searchArr,"",$input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}

function sanitize_title($input_data) {
	$searchArr=array("document","write","alert","%","$",";","+","|","#","<",">",")","(","'","\'",",");
	$input_data= str_replace("script","",$input_data);
	$input_data= str_replace("iframe","",$input_data);
	$input_data= str_replace($searchArr,"",$input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}
function sanitize_center($input_data) {
	$searchArr=array("document","write","alert","%","$",";","+","|","#","<",">",")","(","'","\'");
	$input_data= str_replace("script","",$input_data);
	$input_data= str_replace("iframe","",$input_data);
	$input_data= str_replace($searchArr,"",$input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}
function sanitize_data_email($input_data) {
	$searchArr=array("document","write","alert","%","$",";","+","|","#","<",">",")","(","'","\'");
	$input_data= str_replace("script","",$input_data);
	$input_data= str_replace("iframe","",$input_data);
	$input_data= str_replace($searchArr,"",$input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}
function sanitize_description($input_data) {
	$searchArr=array("write","alert","$");
	$input_data= str_replace("script","",$input_data);
	$input_data= str_replace("iframe","",$input_data);
	$input_data= str_replace($searchArr,"",$input_data);
    $input_data=trim($input_data);
	return $input_data;
}
if (version_compare(PHP_VERSION, '5.3.5') <= 0) {
// function to use verify password decryption on 25/september/2015 at 4:23 PM

function hex2bin($hexstr){
         $n = strlen($hexstr);
         $sbin="";
         $i=0;
         while($i<$n)
         {
             $a =substr($hexstr,$i,2);
             $c = pack("H*",$a);
             if ($i==0){$sbin=$c;}
             else {$sbin.=$c;}
             $i+=2;
         }
         return $sbin;
     }
 }
 
 function fetchcolumnListCond($col,$table,$cond){
	global $conn;
	$dataArray=array();
		 $query="SELECT $col FROM $table $cond";
				$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
//this function is used for fetch app healthcheckup plan with retail product id  by amit kumar dubey on 29 september 2015 at 11:37 PM
function fetchRetailhealthPlanList($cond){
	global $conn;
	$dataArray=array();
	$query="SELECT CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANNAME AS PLANNAME2,CRMRETAILHEALTHCHECKUPPLAN.PLANNAME,CRMRETAILHEALTHCHECKUPPLAN.AGEGROUPID,CRMRETAILHEALTHCHECKUPPLAN.STATUS,CRMRETAILHEALTHCHECKUPPLAN.SI,CRMRETAILHEALTHCHECKUPPLAN.PLANID,CRMRETAILHEALTHCHECKUPPLAN.GENDER,CRMRETAILHEALTHCHECKUPPLAN.PAYMENTOPTIONS,CRMCORPHEALTHCHECKUP.PLANDETAILS,CRMCORPHEALTHCHECKUP.TERMSANDCONDITION,CRMRETAILHEALTHCHECKUPPLAN.COST,CRMRETAILHEALTHCHECKUPPLAN.EDITEDPRICE FROM CRMRETAILHEALTHCHECKUPPLAN LEFT JOIN CRMCORPHEALTHCHECKUP ON CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CRMRETAILHEALTHCHECKUPPLAN.PLANNAMEID ".@$cond."";
		$sql = @oci_parse($conn, $query);
		// Execute Query
	@oci_execute($sql);
		
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
//this function is used for fetch term and condition and how to avail healthcheckup plan with retail product id  by amit kumar dubey on 29 september 2015 at 1:18 PM
function fetchRetailhealthPlanListById($cond){
	global $conn;
	$dataArray=array();
	$query="SELECT CRMCORPHEALTHCHECKUP.EXCLUSION,CRMCORPHEALTHCHECKUP.TERMSANDCONDITION,CRMCORPHEALTHCHECKUP.UPLOADPDF FROM CRMRETAILHEALTHCHECKUPPLAN LEFT JOIN CRMCORPHEALTHCHECKUP ON CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CRMRETAILHEALTHCHECKUPPLAN.PLANNAMEID ".@$cond."";
		$sql = @oci_parse($conn, $query);
		// Execute Query
	@oci_execute($sql);
		
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function getRetHealChkupPlanDetailsConf($productId,$planId){
	global $conn;
	$dataArray=array();
	if(@$productId>0){
		$query="SELECT CRMRETAILHEALTHCHECKUPPLAN.*,CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANNAME AS PLANNAME2 FROM CRMRETAILHEALTHCHECKUPPLAN LEFT JOIN CRMCORPHEALTHCHECKUP ON  CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CRMRETAILHEALTHCHECKUPPLAN.PLANNAMEID  where CRMRETAILHEALTHCHECKUPPLAN.PRODUCTID='".$productId."' AND CRMRETAILHEALTHCHECKUPPLAN.PLANID='".@$planId."' AND CRMRETAILHEALTHCHECKUPPLAN.STATUS='ACTIVE' "; 
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function getRetPurchasedPlanDetails($PURCHASEPLANID,$policyNumber){
	global $conn;
	$dataArray=array();
	if(@$policyNumber!=''){
		 $query="SELECT CRMRETEMPLOYEEPURCHASEPLAN.*,CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANNAME AS PLANNAME2 FROM CRMRETEMPLOYEEPURCHASEPLAN LEFT JOIN CRMCORPHEALTHCHECKUP ON CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CRMRETEMPLOYEEPURCHASEPLAN.PLANNAMEID  where CRMRETEMPLOYEEPURCHASEPLAN.PURCHASEPLANID='".$PURCHASEPLANID."' AND CRMRETEMPLOYEEPURCHASEPLAN.POLICYNUMBER='".@$policyNumber."' ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function getRetHealthCheckupPlanMemberDetails($PURCHASEPLANID,$PLANID){
	global $conn;
	$dataArray=array();
	if(@$PURCHASEPLANID>0){
		$query="SELECT * FROM CRMRETEMPLOYEEPLANMEMBER where PURCHASEPLANID='".$PURCHASEPLANID."' AND PLANID='".@$PLANID."' ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function getRet2PurchasedPlanDetails($PURCHASEPLANID,$policyNumber,$PRODUCTID){
	global $conn;
	$dataArray=array();
	if(@$policyNumber!=''){
		 $query="SELECT CRMRETEMPLOYEEPURCHASEPLAN.*,CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANNAME AS PLANNAME2 FROM CRMRETEMPLOYEEPURCHASEPLAN LEFT JOIN CRMCORPHEALTHCHECKUP ON CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CRMRETEMPLOYEEPURCHASEPLAN.PLANNAMEID  where CRMRETEMPLOYEEPURCHASEPLAN.PURCHASEPLANID='".$PURCHASEPLANID."' AND CRMRETEMPLOYEEPURCHASEPLAN.POLICYNUMBER='".@$policyNumber."' AND CRMRETEMPLOYEEPURCHASEPLAN.PRODUCTID='".@$PRODUCTID."' ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function sendMailToRetempForPurchasePlan($email){
		$_SESSION['PURCHASEPLANIDNEW'] = $_SESSION['PURCHASEPLANIDNEW']?$_SESSION['PURCHASEPLANIDNEW']:$_SESSION['PURCHASEPLANID'];
		
		$plandetail = fetchPlanwithProductDetails(@$_SESSION['PURCHASEPLANIDNEW']);
		
		//$empUrl = "https://uattractus.religarehealthinsurance.com:9443/retail/";
		global $conn;
		global $empUrl;
		$todayDate = date('d-M-Y',time());
		$todayDate = strtoupper($todayDate);
		$certificatefilenane = create80DCertificateForRet($plandetail);
		
		$Mailtemplates=GetTemplateContentByTemplateNameForConfirm('EMP_PURCHASE_PLAN_MAIL');
	
		$from=stripslashes($Mailtemplates[0]['FROMEMAIL']);
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
	    $bodytext  = stripslashes($Mailtemplates[0]['CONTENT']);
	    $bodytext  = str_replace("##Name",@$plandetail[0]['EMPNAME'],$bodytext);	
		$bodytext  = str_replace("##healthcheckupplanname",@$plandetail[0]['PLANNAME'],$bodytext);	
		$bodytext  = str_replace("##emploginLink",@$empUrl,$bodytext);
		$bodytext  = str_replace("##today ",@$todayDate,$bodytext);
        $bodytext  = str_replace("##policyEndDate ",@$plandetail[0]['POLICYENDDATE'],$bodytext);	
/////simple mail with attactch
		$uploadfilename = @$certificatefilenane;
		$upload_temp='template/'.@$certificatefilenane;
		$file_to_attach = $upload_temp;
		$to = $email; //Recipient Email Address
		$headers = "From: ".$from."\r\nReply-To: ".$from."\r\nCc:wellness@religare.com";
		$random_hash = md5(date('r', time()));
		$headers .= "\r\nContent-Type: multipart/mixed; boundary=\"PHP-mixed-".$random_hash."\"";
		$attachment = chunk_split(base64_encode(file_get_contents($file_to_attach))); // Set your file path here
		$message = "--PHP-mixed-$random_hash\r\n"."Content-Type: multipart/alternative; boundary=\"PHP-alt-$random_hash\"\r\n\r\n";
		$message .= "--PHP-alt-$random_hash\r\n"."Content-Type: text/html; charset=\"iso-8859-1\"\r\n"."Content-Transfer-Encoding: 7bit\r\n\r\n";
		$message .= $bodytext;
		$message .="\r\n\r\n--PHP-alt-$random_hash--\r\n\r\n";
		$message .= "--PHP-mixed-$random_hash\r\n"."Content-Type: application/zip; name=\"$uploadfilename\"\r\n"."Content-Transfer-Encoding: base64\r\n"."Content-Disposition: attachment\r\n\r\n";
		$message .= $attachment;
		$message .= "/r/n--PHP-mixed-$random_hash--";
		if($mail = mail( $to, $subject , $message, $headers ))
			{
				$reasontomail = 'RETAIL_EMP_PURCHASE_PLAN_MAIL';
				$sendby = '';
				$entryTime=date('d-M-Y');
				$status = '';
				$sendfrom = WELLNESSEMAILID;
				$ipAddress = get_client_ip();
				$userType2 = '';
				//query to insert records FOR EMAIL LOG
				$sqlLog="INSERT INTO PPHCMAILLOG (MAILID,MAILTO,SUBJECT,MESSAGE,USERTYPE,REASONTOMAIL,SENDBY,SENDDATE,STATUS,SENDFROM,IPADDRESS,CASEID) values(PPHCMAILLOG_SEQ.nextval,q'[".$to."]','".@$subject."','".@$message."','".@$userType2."','".@$reasontomail."','".@$sendby."','".@$entryTime."','".@$status."','".@$sendfrom."','".@$ipAddress."','".@$caseid."') ";		
				$sqlLogExe = @oci_parse($conn, $sqlLog);
				$r = @oci_execute($sqlLogExe);	
			}
		//$mail ? "Mail senttyry try rt yrty" : "Mail failed";
}
function fetchPlanwithProductDetails($purchasePlanId){
	global $conn;
	$dataArray=array();
		 $query="SELECT CRMRETEMPLOYEEPURCHASEPLAN.PURCHASEPLANID,CRMRETEMPLOYEEPURCHASEPLAN.CLIENTID,CRMRETEMPLOYEEPURCHASEPLAN.PURCHASEDATE,CRMRETEMPLOYEEPURCHASEPLAN.EMPNAME,CRMRETEMPLOYEEPURCHASEPLAN.PLANNAME,CRMRETEMPLOYEEPURCHASEPLAN.POLICYNUMBER,CRMRETEMPLOYEEPURCHASEPLAN.TOTALAMOUNT,CRMRETEMPLOYEEPURCHASEPLAN.ADDRESS FROM CRMRETEMPLOYEEPURCHASEPLAN left join CRMRETAILPRODUCT on CRMRETAILPRODUCT.PRODUCTID = CRMRETEMPLOYEEPURCHASEPLAN.PRODUCTID WHERE CRMRETEMPLOYEEPURCHASEPLAN.PURCHASEPLANID=$purchasePlanId";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}

function create80DCertificateForRet($plandetailpre){

			require_once 'PHPWord.php';

			$PHPWord = new PHPWord();
			$section = $PHPWord->createSection();
			$_SESSION['PURCHASEPLANIDNEW'] = $_SESSION['PURCHASEPLANIDNEW']?$_SESSION['PURCHASEPLANIDNEW']:1;
			$plandetail = fetchPlanwithProductDetails(@$_SESSION['PURCHASEPLANIDNEW']);
		
			$document = $PHPWord->loadTemplate('PHPWord/Invoice_Template.docx');
			$document->setValue('Value1', @$plandetail[0]['POLICYNUMBER']);
			$document->setValue('Value2', @$plandetail[0]['CLIENTID']);
			$document->setValue('Value3', @$plandetail[0]['EMPNAME']);
			$document->setValue('Value4', @$plandetail[0]['COMPANYNAME']);
			$document->setValue('Value5', @$plandetail[0]['POLICYSTARTDATE']);
			$document->setValue('Value6', @$plandetail[0]['POLICYENDDATE']);
			$document->setValue('Value7', @$plandetail[0]['TOTALAMOUNT']);
			$document->setValue('Value8', @$plandetail[0]['TOTALAMOUNT']);
			$document->setValue('Value9', @$plandetail[0]['TOTALAMOUNT']);
			$document->setValue('Value10',@$plandetail[0]['EMPNAME']);
			$purchasetime = strtotime($plandetail[0]['PURCHASEDATE']);
			$purchasedate = date('d-M-Y',$purchasetime);
			$document->setValue('Value11',$purchasedate);
			include_once("invoiceReport.php");
			return $certificatefilenane;
}
function getRetHealthCheckupPlansFree($productId){
	global $conn;
	$dataArray=array();
	if(@$productId>0){
		 $query="SELECT CRMRETAILHEALTHCHECKUPPLAN.*,CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANNAME AS PLANNAME2 FROM CRMRETAILHEALTHCHECKUPPLAN LEFT JOIN CRMCORPHEALTHCHECKUP ON CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CRMRETAILHEALTHCHECKUPPLAN.PLANNAMEID where CRMRETAILHEALTHCHECKUPPLAN.PRODUCTID='".$productId."' AND CRMRETAILHEALTHCHECKUPPLAN.STATUS='ACTIVE'  AND CRMRETAILHEALTHCHECKUPPLAN.PAYMENTOPTIONS='FREE'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function getRetHealthCheckupPurchasePlan($policyNumber,$clientNumber,$productId){
	global $conn;
	$dataArray=array();
	if(@$policyNumber!=''){
		$query="SELECT CEP.*,CCCP.PLANID,CCCP.PLANNAME,CCCHP.HEALTHCHECKUPPLANNAME AS PLANNAME2,CCCP.AGEGROUPID,CCCP.SI,CCCP.PLANLIMITS,CCCP.COST,CCCP.PAYMENTOPTIONS,CCCP.GENDER as PLANGENDER FROM CRMRETEMPLOYEEPURCHASEPLAN CEP LEFT JOIN CRMRETAILHEALTHCHECKUPPLAN CCCP ON CEP.PLANID=CCCP.PLANID LEFT JOIN CRMCORPHEALTHCHECKUP CCCHP ON   CCCHP.HEALTHCHECKUPPLANID=CCCP.PLANNAMEID where CEP.POLICYNUMBER='".$policyNumber."'  AND CEP.PRODUCTID='".$productId."' AND CEP.CLIENTID='".$clientNumber."' AND CEP.PAYMENTRECEIVED='YES'  ORDER BY CEP.PURCHASEPLANID DESC";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function checkLimitedPlanForRetEmployee($productId,$empNo,$clientNumber){
	global $conn;
	$dataArray=array();
	if($productId!=''){
		 $query1=sprintf("SELECT * FROM CRMRETLIMITEDEMPLOYEE  WHERE PRODUCTID = '%s' AND (EMPLOYEENUMBER like '%s' OR CLIENTNUMBER like '%s'  )  AND LIMITEDAPPOINTMENT='NO' ",
				@$productId,
				@$empNo,
				@$clientNumber);
		$sql = @oci_parse($conn, $query1);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function get_balance_retmember_list_for_appointment($purchaseId,$planId,$productId){
	global $conn;
	$dataArray=array();
		 //$query="SELECT * FROM CRMRETEMPLOYEEPLANMEMBER where PURCHASEPLANID='".$purchaseId."' AND  PLANID='".@$planId."' AND  PRODUCTID='".@$productId."' AND MEMBERID NOT IN (SELECT MEMBERID FROM CRMRETAILPOSTAPPOINTMENT WHERE  PURCHASEID='".$purchaseId."' AND STATUS !='CANCELLED')";
		 $query="SELECT * FROM CRMRETEMPLOYEEPLANMEMBER where PURCHASEPLANID='".$purchaseId."' AND  PLANID='".@$planId."' AND  PRODUCTID='".@$productId."' AND CLIENTID NOT IN (SELECT CUSTOMERID FROM CRMRETAILPOSTAPPOINTMENT WHERE  PURCHASEID='".$purchaseId."' AND CUSTOMERID is NOT NULL AND STATUS !='CANCELLED')";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function get_particular_retplan_details_for_policy($planId,$productId){
	global $conn;
	$dataArray=array();
	if($productId>0){
		 $query="SELECT CRMRETAILHEALTHCHECKUPPLAN.*,CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANNAME AS PLANNAME2,CRMCORPHEALTHCHECKUP.UPLOADPDF AS UPLOADPDF2 FROM CRMRETAILHEALTHCHECKUPPLAN LEFT JOIN CRMCORPHEALTHCHECKUP ON CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CRMRETAILHEALTHCHECKUPPLAN.PLANNAMEID where CRMRETAILHEALTHCHECKUPPLAN.PRODUCTID='".$productId."' and CRMRETAILHEALTHCHECKUPPLAN.PLANID='".@$planId."'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function checkRetMemberAvailAppointment($planId,$policyNumber,$name,$customerId,$age){
		global $conn;
		$dataArray=array();
		$query="SELECT * FROM CRMRETAILPOSTAPPOINTMENT where   HEALTHCHECKUPID='".@$planId."' AND  POLICYNUMBER='".@$policyNumber."' AND  (EMPLOYEEID='".@$_SESSION['empId']."' or CUSTOMERID='".@$customerId."') AND  LOWER(FIRSTNAME) like '".trim(strtolower(@$name))."'  AND STATUS !='CANCELLED' ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}

function get_retmember_list_for_free_appointment($responseArray,$relationArray){
	$purchaseId=$responseArray['purchaseId'];
	$planId=$responseArray['planId'];
	$productId=$responseArray['productId'];
	$policyNumber=$responseArray['policyNumber'];
	$currenttime=time();
	//$planDetails=get_particular_retplan_details_for_policy($planId,@$productId);// fetch plan details for plan id
	$planDetails= fetchcolumnListCond('CRMRETAILHEALTHCHECKUPPLAN.PLANLIMITS,CRMRETAILHEALTHCHECKUPPLAN.GENDER,CRMRETAILHEALTHCHECKUPPLAN.AGEGROUPID,CRMRETAILHEALTHCHECKUPPLAN.FREEMAPPEDMEMBERTYPEIDS,CRMRETAILHEALTHCHECKUPPLAN.SI','CRMRETAILHEALTHCHECKUPPLAN'," WHERE CRMRETAILHEALTHCHECKUPPLAN.PRODUCTID='".$productId."' AND CRMRETAILHEALTHCHECKUPPLAN.PLANID='".@$planId."'");
	$memberList=array();
	$l=0;
	if(@$planDetails[0]['PLANLIMITS']=="ONLYEMPLOYEES"){ 
		// check employee valid for appointment
		for($i=0;$i<count($responseArray);$i++){
		if($i == 0 ){//if primary member
			$planDisplay=0;
			if(trim($responseArray[$i]['BGEN-GENDER']['#text'])=="F"){$gender="Female";	}
			if(trim($responseArray[$i]['BGEN-GENDER']['#text'])=="M"){$gender="Male";	}
			if(strtolower($gender)==strtolower(@$planDetails[0]['GENDER']) || strtolower(@$planDetails[0]['GENDER'])=="both"){
							$planDisplay++;
			}
				$pieces = explode(',' , @$planDetails[0]['AGEGROUPID']);
				$policyHolderAge=str_replace(",","",number_format(trim($responseArray[$i]['BGEN-AGE']['#text'])));
				for($j=0;$j<count($pieces);$j++) {
					if($policyHolderAge == $pieces[$j]){
								$planDisplay++;
								break;
					}
				}
				$si_list = explode(',' , @$planDetails[0]['SI']);
				$policyHolderSI=str_replace(",","",number_format(@$_SESSION['policyHolderSI']));
				//$policyHolderSI=100000;
				for($j=0;$j<count($si_list);$j++) {
						$siName=fetchListById('CRMSI','SIID',$si_list[$j]); 
						if($siName[0]['SI']==$policyHolderSI){
							$planDisplay++;
						}
				}
				
				if($planDisplay==3){
					
					$memberAvail=checkRetMemberAvailAppointment($planId,@$policyNumber,trim(@$responseArray[$i]['BGEN-MEMNAME']['#text']),trim(@$responseArray[$i]['BGEN-CLNTNM']['#text']),$policyHolderAge);

                       	// start query for check policy expired and appointment for current year
						//$fetchAppByThisPlanId=fetchListCondsWithColumn("CREATEDON","CRMRETAILPOSTAPPOINTMENT"," WHERE HEALTHCHECKUPID=".$planId."");
						   if($memberAvail[0]['CREATEDON']!=''){
							   $yearDiff=date('Y',strtotime($_SESSION['POLICYENDDATE']))-date('Y',time());
							   $yearRange=getYearRange($_SESSION['POLICYSTARTDATE'],$_SESSION['POLICYENDDATE']);
							  // print_r($yearRange);
							   for($j=0;$j<count($yearRange);$j++){
								   $matchstartdate=strtotime($yearRange[$j]['STARTDATE']);
								   $matchenddate=strtotime($yearRange[$j]['ENDDATE']);
								   if(($currenttime >= $matchstartdate) && ($currenttime <= $matchenddate)){
									   $matchstartdate=$matchstartdate;
									   $matchenddate=$matchenddate;
									   break;
								   } else {
									   $matchstartdate='';
									   $matchenddate='';
								   }
								}
								 $strCreatedOn=strtotime($memberAvail[0]['CREATEDON']);
								// echo $strCreatedOn;
								// echo "</br>";
								// echo $matchstartdate;
								// echo "</br>";
								// echo $matchenddate;
								// echo "</br>";
								   if((($strCreatedOn >= $matchstartdate) && ($strCreatedOn <= $matchenddate)) || ($yearDiff < 0)){
								   		$bookApp=false;
								   } else {
								   		$bookApp=true;
								   }
						   }
						  
						  // echo count($memberAvail);
						  // end of query for check policy expired 
					if(count($memberAvail) >=0){
					 if($bookApp==true){
			$allvalues=trim($responseArray[$i]['BGEN-GENDER']['#text']).":".trim($relationArray[$i]['BGEN-DPNTTYP']['#text']).":".trim($relationArray[$i]['BGEN-MEMNAME']['#text']).":".trim($relationArray[$i]['BGEN-AGE']['#text']).":".trim($relationArray[$i]['BGEN-ZBALCURR']['#text']).":".trim($relationArray[$i]['BGEN-CLNTNM']['#text']).":".trim($policyNumber);
						$memberList[$l]['NAME']=trim($responseArray[$i]['BGEN-MEMNAME']['#text']);
						$memberList[$l]['AGE']=trim($responseArray[$i]['BGEN-AGE']['#text']);
						$memberList[$l]['RELATION']=trim($responseArray[$i]['BGEN-DPNTTYP']['#text']);
						$memberList[$l]['CUSTOMERTD']=trim($responseArray[$i]['BGEN-CLNTNM']['#text']);
						$memberList[$l]['GENDER']=@$gender;
						$memberList[$l]['ALLADATA']=@$allvalues;
						$l++;
						}
					}
				}
			}
		}
	}else if(@$planDetails[0]['PLANLIMITS']=="FAMILYALLOWED"){
		//start family allowed
		// check employee valid for appointment
		for($i=0;$i<count($responseArray);$i++){
		if($i > 0 ){//if primary member
			$planDisplay=0;
			if(trim(@$responseArray[$i]['BGEN-GENDER']['#text'])=="F"){$gender="Female";	}
			if(trim(@$responseArray[$i]['BGEN-GENDER']['#text'])=="M"){$gender="Male";	}
			if(strtolower($gender)==strtolower(@$planDetails[0]['GENDER']) || strtolower(@$planDetails[0]['GENDER'])=="both"){
							$planDisplay++;
			}
				$pieces = explode(',' , @$planDetails[0]['AGEGROUPID']);
				$policyHolderAge=str_replace(",","",@number_format(@$responseArray[$i]['BGEN-AGE']['#text']));
				for($j=0;$j<count($pieces);$j++) {
					if($policyHolderAge == $pieces[$j]){
								$planDisplay++;
								break;
					}
				}
				$si_list = explode(',' , @$planDetails[0]['SI']);
				$policyHolderSI=str_replace(",","",@number_format(@$_SESSION['policyHolderSI']));
				for($j=0;$j<count($si_list);$j++) {
						$siName=fetchListById('CRMSI','SIID',$si_list[$j]); 
						if($siName[0]['SI']==$policyHolderSI){
							$planDisplay++;
						}
				}
				if($planDisplay==3){
					$memberAvail=checkRetMemberAvailAppointment($planId,@$policyNumber,trim(@$responseArray[$i]['BGEN-MEMNAME']['#text']),trim(@$responseArray[$i]['BGEN-CLNTNM']['#text']),$policyHolderAge);
                       	// start query for check policy expired and appointment for current year
						//$fetchAppByThisPlanId=fetchListCondsWithColumn("CREATEDON","CRMRETAILPOSTAPPOINTMENT"," WHERE HEALTHCHECKUPID=".$planId."");
						   if($memberAvail[0]['CREATEDON']!=''){
							   $yearDiff=date('Y',strtotime($_SESSION['POLICYENDDATE']))-date('Y',time());
							   $yearRange=getYearRange($_SESSION['POLICYSTARTDATE'],$_SESSION['POLICYENDDATE']);
							  // print_r($yearRange);
							   for($j=0;$j<count($yearRange);$j++){
								   $matchstartdate=strtotime($yearRange[$j]['STARTDATE']);
								   $matchenddate=strtotime($yearRange[$j]['ENDDATE']);
								   if(($currenttime >= $matchstartdate) && ($currenttime <= $matchenddate)){
									   $matchstartdate=$matchstartdate;
									   $matchenddate=$matchenddate;
									   break;
								   } else {
									   $matchstartdate='';
									   $matchenddate='';
								   }
								}
								 $strCreatedOn=strtotime($memberAvail[0]['CREATEDON']);
								// echo $strCreatedOn;
								// echo "</br>";
								// echo $matchstartdate;
								// echo "</br>";
								// echo $matchenddate;
								// echo "</br>";
								   if((($strCreatedOn >= $matchstartdate) && ($strCreatedOn <= $matchenddate)) || ($yearDiff < 0)){
								   		$bookApp=false;
								   } else {
								   		$bookApp=true;
								   }
						   }
						  
						  // echo count($memberAvail);
						  // end of query for check policy expired 
					if(count($memberAvail) >=0){
					 if($bookApp==true){
			$allvalues=trim($responseArray[$i]['BGEN-GENDER']['#text']).":".trim($relationArray[$i]['BGEN-DPNTTYP']['#text']).":".trim($relationArray[$i]['BGEN-MEMNAME']['#text']).":".trim($relationArray[$i]['BGEN-AGE']['#text']).":".trim($relationArray[$i]['BGEN-ZBALCURR']['#text']).":".trim($relationArray[$i]['BGEN-CLNTNM']['#text']).":".trim($policyNumber);
						$memberList[$l]['NAME']=trim($responseArray[$i]['BGEN-MEMNAME']['#text']);
						$memberList[$l]['AGE']=trim($responseArray[$i]['BGEN-AGE']['#text']);
						$memberList[$l]['RELATION']=trim($responseArray[$i]['BGEN-DPNTTYP']['#text']);
						$memberList[$l]['CUSTOMERTD']=trim($responseArray[$i]['BGEN-CLNTNM']['#text']);
						$memberList[$l]['GENDER']=@$gender;
						$memberList[$l]['ALLADATA']=@$allvalues;
						$l++;
					 }
					}
				}
			}
		}
		//end family allowed
	}else if(@$planDetails[0]['PLANLIMITS']=="BOTH"){
		//start both
		for($i=0;$i<count($responseArray);$i++){
			$planDisplay=0;
			if(trim(@$responseArray[$i]['BGEN-GENDER']['#text'])=="F"){$gender="Female";	}
			if(trim(@$responseArray[$i]['BGEN-GENDER']['#text'])=="M"){$gender="Male";	}
			if(strtolower($gender)==strtolower(@$planDetails[0]['GENDER']) || strtolower(@$planDetails[0]['GENDER'])=="both"){
							$planDisplay++;
			}
				$pieces = explode(',' , @$planDetails[0]['AGEGROUPID']);
				$policyHolderAge=str_replace(",","",@number_format(trim(@$responseArray[$i]['BGEN-AGE']['#text'])));
				for($j=0;$j<count($pieces);$j++) {
					if($policyHolderAge == $pieces[$j]){
								$planDisplay++;
								break;
					}
				}
				$si_list = explode(',' , @$planDetails[0]['SI']);
				$policyHolderSI=str_replace(",","",@number_format(@$_SESSION['policyHolderSI']));
				for($j=0;$j<count($si_list);$j++) {
						$siName=fetchListById('CRMSI','SIID',$si_list[$j]); 
						if($siName[0]['SI']==$policyHolderSI){
							$planDisplay++;
						}
				}
				if($planDisplay==3){
					$bookApp=true;
					$memberAvail=checkRetMemberAvailAppointment($planId,@$policyNumber,trim(@$responseArray[$i]['BGEN-MEMNAME']['#text']),trim(@$responseArray[$i]['BGEN-CLNTNM']['#text']),$policyHolderAge);
					
                       	// start query for check policy expired and appointment for current year
						//$fetchAppByThisPlanId=fetchListCondsWithColumn("CREATEDON","CRMRETAILPOSTAPPOINTMENT"," WHERE HEALTHCHECKUPID=".$planId."");
						   if($memberAvail[0]['CREATEDON']!=''){
							   $yearDiff=date('Y',strtotime($_SESSION['POLICYENDDATE']))-date('Y',time());
							   $yearRange=getYearRange($_SESSION['POLICYSTARTDATE'],$_SESSION['POLICYENDDATE']);
							  // print_r($yearRange);
							   for($j=0;$j<count($yearRange);$j++){
								   $matchstartdate=strtotime($yearRange[$j]['STARTDATE']);
								   $matchenddate=strtotime($yearRange[$j]['ENDDATE']);
								   if(($currenttime >= $matchstartdate) && ($currenttime <= $matchenddate)){
									   $matchstartdate=$matchstartdate;
									   $matchenddate=$matchenddate;
									   break;
								   } else {
									   $matchstartdate='';
									   $matchenddate='';
								   }
								}
								 $strCreatedOn=strtotime($memberAvail[0]['CREATEDON']);
								// echo $strCreatedOn;
								// echo "</br>";
								// echo $matchstartdate;
								// echo "</br>";
								// echo $matchenddate;
								// echo "</br>";
								   if((($strCreatedOn >= $matchstartdate) && ($strCreatedOn <= $matchenddate)) || ($yearDiff < 0)){
								   		$bookApp=false;
								   } else {
								   		$bookApp=true;
								   }
						   }
						  
						  // echo count($memberAvail);
						  // end of query for check policy expired 
					if(count($memberAvail) >=0){
					 if($bookApp==true){
						$applicatefor=@$planDetails[0]['FREEMAPPEDMEMBERTYPEIDS'];
						$title='';
						if(isset($applicatefor) && !empty($applicatefor)){
							$appres=json_decode($applicatefor,true);
							$appres=implode(',',$appres);
							$membersname= fetchcolumnListCond('TITLE','CRMCORPMEMBERTYPE','WHERE MEMBERTYPEID IN('.$appres.') ');
							$f=0;
							$titles=array();
							while($f<count($membersname)){
								$titles[]=$membersname[$f]['TITLE'];
							$f++; }
							if(count($titles)>0){
								$title=$titles;
							} else {
								$title='';
							}
						}
						$title = str_ireplace('self', 'primary member', $title);
						$array1=$title;
						$array2=$_SESSION['membersarray'];
						

						if(count($title)>0){
						
							 /*$relationArray = array(
											'MMBR' =>  'primary member'  , 	'UDTR' => 'daughter', 	'BOTH' =>  'brother' , 
											'CHD2' =>  'Child'           , 	'CHD3' =>  'Child'  , 	'CHLD' => 'Child',
											'DLAW' =>  'Daughter In Law' , 	'EFAT' => 'father'  , 	'EFLW' =>  'father In Law' , 
											'EMLW' =>  'mother In Law'   ,   'EMOT' =>  'mother' , 	'FATH' => 'father', 
											'FLAW' =>  'father In Law'   ,   'GDAU' => 'Grand Daughter', 'GFAT' =>  'grand father' , 
											'GMOT' =>  'grand mother'    , 	'GSON' =>  'grand son', 'MLAW' => 'mothe in law',
											'MOTH' =>  'mother'         , 	'SIST' => 'sister'    , 'SLAW' =>  'son in law' , 
											'SONM' =>  'son'             ,	'SPSE' => 'Spouse'    ,	'UDTR' => 'daughter',
										);*/
						//echo strtolower(@$relationArray[trim(@$responseArray[$i]['BGEN-DPNTTYP']['#text'])]); die;
						$resultmapped=array_intersect(array_map('strtolower', $array1), array_map('strtolower', $array2));
						 if(in_array(strtolower(@$relationArray[trim(@$responseArray[$i]['BGEN-DPNTTYP']['#text'])]),$resultmapped)){
							$allvalues=trim($responseArray[$i]['BGEN-GENDER']['#text']).":".trim($relationArray[$i]['BGEN-DPNTTYP']['#text']).":".trim($relationArray[$i]['BGEN-MEMNAME']['#text']).":".trim($relationArray[$i]['BGEN-AGE']['#text']).":".trim($relationArray[$i]['BGEN-ZBALCURR']['#text']).":".trim($relationArray[$i]['BGEN-CLNTNM']['#text']).":".trim($policyNumber);
							$memberList[$l]['NAME']=trim(@$responseArray[$i]['BGEN-MEMNAME']['#text']);
							$memberList[$l]['AGE']=trim(@$responseArray[$i]['BGEN-AGE']['#text']);
							$memberList[$l]['RELATION']=trim(@$responseArray[$i]['BGEN-DPNTTYP']['#text']);
							$memberList[$l]['CUSTOMERID']=trim($responseArray[$i]['BGEN-CLNTNM']['#text']);
							$memberList[$l]['GENDER']=@$gender;
							$memberList[$l]['ALLADATA']=@$allvalues;
							$l++;
						 }
						} else {
							$allvalues=trim($responseArray[$i]['BGEN-GENDER']['#text']).":".trim($relationArray[$i]['BGEN-DPNTTYP']['#text']).":".trim($relationArray[$i]['BGEN-MEMNAME']['#text']).":".trim($relationArray[$i]['BGEN-AGE']['#text']).":".trim($relationArray[$i]['BGEN-ZBALCURR']['#text']).":".trim($relationArray[$i]['BGEN-CLNTNM']['#text']).":".trim($policyNumber);
							$memberList[$l]['NAME']=trim(@$responseArray[$i]['BGEN-MEMNAME']['#text']);
							$memberList[$l]['AGE']=trim(@$responseArray[$i]['BGEN-AGE']['#text']);
							$memberList[$l]['RELATION']=trim(@$responseArray[$i]['BGEN-DPNTTYP']['#text']);
							$memberList[$l]['CUSTOMERID']=trim($responseArray[$i]['BGEN-CLNTNM']['#text']);
							$memberList[$l]['GENDER']=@$gender;
							$memberList[$l]['ALLADATA']=@$allvalues;
							$l++;
						}
						
					
					}
					}
				}
			}
		//end both allowed
	}
	return $memberList;
}

function fetchpurchaseRetPlanDetailsByplanId($purchaseplanId,$planId,$productId){
	global $conn;
	$dataArray=array();
	if($productId>0){
		$query="SELECT CRMRETEMPLOYEEPLANMEMBER.* FROM CRMRETEMPLOYEEPLANMEMBER LEFT JOIN CRMRETEMPLOYEEPURCHASEPLAN ON CRMRETEMPLOYEEPURCHASEPLAN.PURCHASEPLANID=CRMRETEMPLOYEEPLANMEMBER.PURCHASEPLANID  where CRMRETEMPLOYEEPURCHASEPLAN.PRODUCTID=".$productId." AND CRMRETEMPLOYEEPURCHASEPLAN.PLANID=".@$planId." AND CRMRETEMPLOYEEPLANMEMBER.PLANID=".@$planId." AND CRMRETEMPLOYEEPLANMEMBER.PURCHASEPLANID=".@$purchaseplanId." ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function checkPlanForRetEmployee($productId,$empNo,$clientNumber){
	global $conn;
	$dataArray=array();
	$countPlan=0;
	if($productId!=''){
		$query="SELECT * FROM CRMRETAILHEALTHCHECKUPPLAN where PRODUCTID='".$productId."' AND STATUS='ACTIVE' AND PAYMENTOPTIONS='FREE' ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
		 $query1=sprintf("SELECT * FROM CRMRETEMPLOYEEPURCHASEPLAN  WHERE POLICYID = '%s' AND (EMPLOYEEID like '%s' OR CLIENTID like '%s'  )  AND PAYMENTSTATUS='CONFIRMED' AND PAYMENTRECEIVED='YES' ",
				@$productId,
				@$empNo,
				@$clientNumber);
		$sql = @oci_parse($conn, $query1);
		// Execute Query
		@oci_execute($sql);
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return count($dataArray);
}
//this function is used for get state where center exists in retail ui portal by amit dubey on 05 october 2015
function GetRetStateWhereCenterExist($productId,$policyNumber,$planId){
	global $conn;
	$sqlresult=array();
		$query=" SELECT LWSTATEMASTER.STATEID,LWSTATEMASTER.STATENAME FROM LWSTATEMASTER WHERE LWSTATEMASTER.STATUS ='ACTIVE' AND LWSTATEMASTER.STATEID IN ( SELECT LWCENTERMASTER.STATEID from LWCENTERMASTER JOIN CRMRETAILCENTERMAPPING  ON  CRMRETAILCENTERMAPPING.CENTERID=LWCENTERMASTER.CENTERID WHERE LWCENTERMASTER.STATUS='ACTIVE' AND CRMRETAILCENTERMAPPING.PLANID='".@$planId."' AND CRMRETAILCENTERMAPPING.PRODUCTID='".@$productId."') ";

		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i = 0;
		while (($row = oci_fetch_assoc($sql))) {
				$dataArray[] = $row;
			$i++;
		}
		return $dataArray;
}
function fetchRetCenterListByPlanId($stateId,$cityId,$planId){
		global $conn;
		$dataArray=array();
		//echo $query="SELECT LWCENTERMASTER.CENTERNAME,LWCENTERMASTER.CENTERID,LWCENTERMASTER.ADDRESS1,LWCENTERMASTER.ADDRESS2,LWCITYMASTER.CITYNAME from LWCENTERMASTER LEFT JOIN  LWCITYMASTER ON LWCITYMASTER.CITYID=LWCENTERMASTER.CITYID where CENTERID IN (SELECT CENTERID from CRMRETAILCENTERMAPPING where CRMRETAILCENTERMAPPING.PRODUCTID='".@$_SESSION['productId']."' AND CRMRETAILCENTERMAPPING.PLANID=".@$planId." AND CRMRETAILCENTERMAPPING.POLICYNUMBER='".@$_SESSION['policyNumber']."') AND LWCENTERMASTER.CITYID='".@$cityId."' AND LWCENTERMASTER.STATEID='".@$stateId."' AND LWCENTERMASTER.STATUS='ACTIVE' order by UPPER(LWCENTERMASTER.CENTERNAME) ASC ";	
		$query="SELECT LWCENTERMASTER.CENTERNAME,LWCENTERMASTER.CENTERID,LWCENTERMASTER.ADDRESS1,LWCENTERMASTER.ADDRESS2,LWCITYMASTER.CITYNAME from LWCENTERMASTER LEFT JOIN  LWCITYMASTER ON LWCITYMASTER.CITYID=LWCENTERMASTER.CITYID where CENTERID IN (SELECT CENTERID from CRMRETAILCENTERMAPPING where CRMRETAILCENTERMAPPING.PRODUCTID='".@$_SESSION['productId']."' AND CRMRETAILCENTERMAPPING.PLANID=".@$planId." ) AND LWCENTERMASTER.CITYID='".@$cityId."' AND LWCENTERMASTER.STATEID='".@$stateId."' AND LWCENTERMASTER.STATUS='ACTIVE' order by UPPER(LWCENTERMASTER.CENTERNAME) ASC ";	
	
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
	while (($row = oci_fetch_assoc($sql))) {
		foreach($row as $key=>$value){
			$dataArray[$i][$key] = $value;				
		}
		$i++;
	 }
	return $dataArray;

}	
//this function is used for autocomplete center search in retail ui by amit dubey on 06 october 2015
function SearchRetAutoCenter($q,$table,$name,$compId,$policyId,$conds,$stateId,$cityId,$planId){
	global $conn;
	$sqlresult=array();
	if($q!=''){
		$query="select LWCENTERMASTER.CENTERNAME,LWCENTERMASTER.CENTERID,LWCENTERMASTER.ADDRESS1,LWCENTERMASTER.ADDRESS2,LWCITYMASTER.CITYNAME AS CITY from CRMRETAILCENTERMAPPING LEFT JOIN LWCENTERMASTER ON  CRMRETAILCENTERMAPPING.CENTERID=LWCENTERMASTER.CENTERID LEFT JOIN LWSTATEMASTER ON  LWSTATEMASTER.STATEID=LWCENTERMASTER.STATEID LEFT JOIN LWCITYMASTER ON LWCITYMASTER.CITYID=LWCENTERMASTER.CITYID WHERE   (regexp_like(LWCENTERMASTER.CENTERNAME, '".@$q."', 'i') OR regexp_like(LWCENTERMASTER.ADDRESS1, '".@$q."', 'i') or regexp_like(LWSTATEMASTER.STATENAME, '".@$q."', 'i') or regexp_like(LWCITYMASTER.CITYNAME, '".@$q."', 'i')) AND LWCENTERMASTER.STATUS='ACTIVE' AND LWSTATEMASTER.STATUS='ACTIVE' AND LWCITYMASTER.STATUS='ACTIVE' AND CRMRETAILCENTERMAPPING.PLANID='".@$planId."' AND LWCENTERMASTER.STATEID='".@$stateId."' AND LWCENTERMASTER.CITYID='".@$cityId."' order by LWCENTERMASTER.CENTERID DESC";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while(($row2 = oci_fetch_assoc($sql))) {
		 
		 	$address1 = $row2['ADDRESS1']?",".$row2['ADDRESS1']:'';
		 	$address2 = $row2['ADDRESS2']?",".$row2['ADDRESS2']:'';
		 
		$sqlresult[$i]=$row2['CENTERNAME'].$address1.$address2.",".$row2['CITY'].":".$row2['CENTERID'];
		$i++;
		}
		
	}
	return $sqlresult;
}
//this function is used for autocomplete center search in religarehrcrm by amit dubey on 18 august 2015
function SearchAutoCenter($q,$table,$name,$compId,$policyId,$conds,$stateId,$cityId,$planId){
		global $conn;
	$sqlresult=array();
	if($q!=''){
		$query="select LWCENTERMASTER.CENTERNAME,LWCENTERMASTER.CENTERID,LWCENTERMASTER.ADDRESS1,LWCENTERMASTER.ADDRESS2,LWCITYMASTER.CITYNAME AS CITY from CRMCOMPANYCENTERMAPPING LEFT JOIN LWCENTERMASTER ON  CRMCOMPANYCENTERMAPPING.CENTERID=LWCENTERMASTER.CENTERID LEFT JOIN LWSTATEMASTER ON  LWSTATEMASTER.STATEID=LWCENTERMASTER.STATEID LEFT JOIN LWCITYMASTER ON LWCITYMASTER.CITYID=LWCENTERMASTER.CITYID WHERE   (regexp_like(LWCENTERMASTER.CENTERNAME, '".@$q."', 'i') OR regexp_like(LWCENTERMASTER.ADDRESS1, '".@$q."', 'i') or regexp_like(LWSTATEMASTER.STATENAME, '".@$q."', 'i') or regexp_like(LWCITYMASTER.CITYNAME, '".@$q."', 'i')) AND LWCENTERMASTER.STATUS='ACTIVE' AND LWSTATEMASTER.STATUS='ACTIVE' AND LWCITYMASTER.STATUS='ACTIVE' AND CRMCOMPANYCENTERMAPPING.PLANID='".@$planId."' AND LWCENTERMASTER.STATEID='".@$stateId."' AND LWCENTERMASTER.CITYID='".@$cityId."' order by LWCENTERMASTER.CENTERID DESC";


		//$query="SELECT CRMHEALTHCENTER.CENTERNAME,CRMHEALTHCENTER.CENTERID,CRMHEALTHCENTER.ADDRESS,CRMHEALTHCENTER.CITY from CRMHEALTHCENTER where CENTERID IN (SELECT CENTERID from CRMCOMPANYCENTERMAPPING where (regexp_like(CRMHEALTHCENTER.CENTERNAME, '".@$q."', 'i') OR regexp_like(CRMHEALTHCENTER.ADDRESS, '".@$q."', 'i') or regexp_like(CRMHEALTHCENTER.STATE, '".@$q."', 'i') or regexp_like(CRMHEALTHCENTER.CITY, '".@$q."', 'i')) AND COMPANYID='".@$_SESSION['COMPANYID']."' AND PLANID=".@$planId." AND POLICYNUMBER='".@$_SESSION['policyNumber']."') AND CRMHEALTHCENTER.CITYID='".@$cityId."' AND CRMHEALTHCENTER.STATEID='".@$stateId."' AND CRMHEALTHCENTER.STATUS='ACTIVE' order by UPPER(CRMHEALTHCENTER.CENTERNAME) ASC ";	
	
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while(($row2 = oci_fetch_assoc($sql))) {
		 
		 	$address1 = $row2['ADDRESS1']?",".$row2['ADDRESS1']:'';
		 	$address2 = $row2['ADDRESS2']?",".$row2['ADDRESS2']:'';
		 
		$sqlresult[$i]=$row2['CENTERNAME'].$address1.$address2.",".$row2['CITY'].":".$row2['CENTERID'];
		$i++;
		}
		
	}
	return $sqlresult;

}

// function to fetch slotlist by date . created by amit kumar dubey on 6 october 2015 at 01:18:32 PM
function fetchSlotListByDate($date){
	global $conn;
	$dataArray=array();
		$query="SELECT CRMSLOTMASTER.STARTTIMENAME,CRMSLOTMASTER.SLOTID,CRMCALENDERSLOTMAPPING.ID FROM CRMSLOTMASTER LEFT JOIN CRMCALENDERSLOTMAPPING ON CRMCALENDERSLOTMAPPING.SLOTID=CRMSLOTMASTER.SLOTID WHERE CRMCALENDERSLOTMAPPING.CALDATE='".$date."' AND CRMCALENDERSLOTMAPPING.ISAVAIL='YES'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
	@oci_execute($sql);
		
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
// function used for fetch slot list by center and date created by amit kumar dubey on 11 feb 2016 at 11:28 PM
function fetchSlotListByDateWithCenter($date,$centerId){
	global $conn;
	$dataArray=array();
		$query="SELECT CRMSLOTMASTER.STARTTIMENAME,CRMCALENDER.CENTERID,CRMCALENDER.CALID,CRMSLOTMASTER.SLOTID,CRMCALENDERSLOTMAPPING.ID,CRMCALENDERSLOTMAPPING.CALDATE FROM CRMSLOTMASTER LEFT JOIN CRMCALENDERSLOTMAPPING ON CRMCALENDERSLOTMAPPING.SLOTID=CRMSLOTMASTER.SLOTID LEFT JOIN CRMCALENDER ON CRMCALENDERSLOTMAPPING.CALID=CRMCALENDER.CALID WHERE CRMCALENDERSLOTMAPPING.CALDATE='".$date."' AND CRMCALENDER.CENTERID='".$centerId."'  AND CRMCALENDERSLOTMAPPING.ISAVAIL='YES'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
            @oci_execute($sql);
		
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
// end of fetch list

function fetchSlotListByDateUpdate($slotId){
	global $conn;
	$dataArray=array();
		$query="SELECT CRMSLOTMASTER.STARTTIMENAME,CRMSLOTMASTER.SLOTID,CRMCALENDERSLOTMAPPING.ID FROM CRMSLOTMASTER LEFT JOIN CRMCALENDERSLOTMAPPING ON CRMCALENDERSLOTMAPPING.SLOTID=CRMSLOTMASTER.SLOTID WHERE CRMCALENDERSLOTMAPPING.SLOTID='".$slotId."' AND CRMCALENDERSLOTMAPPING.ISAVAIL='YES'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
	@oci_execute($sql);
		
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function fetchAppListBySlot($mappid){
	global $conn;
	$dataArray=array();
		$query="SELECT CRMCALENDERSLOTMAPPING.ID,CRMCALENDERSLOTMAPPING.MAXAPPOINTMENT,CRMCALENDERSLOTMAPPING.SLOTID,CRMCALENDERSLOTMAPPING.NOOFAPPOINTMENT FROM CRMCALENDERSLOTMAPPING WHERE CRMCALENDERSLOTMAPPING.ID='".$mappid."'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
	@oci_execute($sql);
		
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function getRetReimbursementList($employeeId,$policyNumber){
	global $conn;
	$dataArray=array();
	if($policyNumber!=''){
		$query="SELECT * FROM CRMRETAILREIMBURSEMENTREQUEST where POLICYNO='".$policyNumber."' AND EMPLOYEEID='".$employeeId."' order by REQUESTID DESC ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function checkRetReimbursementRequest($requestId,$policyNumber){
	global $conn;
	$dataArray=array();
		$query="SELECT * FROM CRMRETAILREIMBURSEMENTREQUEST where REQUESTID='".@$requestId."' and POLICYNO='".@$policyNumber."'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function fetchRetReimbursementDocList($requestId){
	global $conn;
	$dataArray=array();
		$query="SELECT * FROM CRMRETAILREIMBURSEMENTDOCS where REQUESTID='".@$requestId."'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}




















































////////end of functions/////////////////////
//function to check Employee Policy OR Company Activated
function checkEmployeePolicyORCompanyActivated($policyId){
	global $conn;
	$dataArray=array();
	 $query=sprintf("SELECT CRMPOLICY.STATUS as POLICYSTATUS,(SELECT CRMCOMPANY.STATUS FROM CRMCOMPANY WHERE CRMCOMPANY.COMPANYID=CRMPOLICY.COMPANYID) AS COMPANYSTATUS FROM CRMPOLICY  WHERE POLICYID ='%s' ",
				@$policyId);
		$sql = oci_parse($conn, $query);
		// Execute Query
		oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
		return $dataArray;
}
//function to check User In Employee Table

function checkUserInEmployeeTable($policyNumber,$employeeId,$customerId){
	global $conn;
	$dataArray=array();
	$query=sprintf("SELECT * FROM CRMEMPLOYEELOGIN  WHERE POLICYNUMBER like '%s' AND (EMPLOYEEID like '%s' OR CUSTOMERID like '%s'  ) ",
				@$policyNumber,
				@$employeeId,
				@$customerId);
		$sql = oci_parse($conn, $query);
		// Execute Query
		oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
		return @$dataArray[0];
}
//function to update Number Of Login
function updateNoOfLogin($policyNumber,$employeeId,$customerId){
	global $conn;
	$query=sprintf("UPDATE  CRMEMPLOYEELOGIN SET NOOFLOGIN=1 WHERE POLICYNUMBER like '%s' AND (EMPLOYEEID like '%s' OR CUSTOMERID like '%s'  ) ",
				@$policyNumber,
				@$employeeId,
				@$customerId);
	$stdid = @oci_parse($conn, $query);
	$r = @oci_execute($stdid);
}
//function to sent Email
function sentMail($policyNumber,$employeeId,$customerId){
	global $conn;
	updateNoOfLogin($policyNumber,$employeeId,$customerId);
	$dataArray=array();
	$query=sprintf("SELECT * FROM CRMEMPLOYEELOGIN  WHERE POLICYNUMBER like '%s' AND (EMPLOYEEID like '%s' OR CUSTOMERID like '%s'  ) ",
				@$policyNumber,
				@$employeeId,
				@$customerId);
		$sql = oci_parse($conn, $query);
		// Execute Query
		oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
		if($dataArray[0]['EMAIL']!=''){
				 $body1='<style type="text/css">
				<!--
				.style1 {
				color: #ffffff;
				font-weight: bold;
				}
				-->
				</style>
				<table width="650" border="0" cellspacing="2" cellpadding="2">
				<tr>
				<td height="30" colspan="2" bgcolor="#086634"><p class="style1">Login Details</p></td>
				</tr>
				<tr>
				<td width="157" align="left">User Name: </td>
				<td width="429" align="left">'.@$dataArray[0]['EMAIL'].'</td>
				</tr>
				<tr>
				<td align="left">Password: </td>
				<td align="left">'.@$dataArray[0]['PASSWORD'].'</td>
				</tr>
				
				</table>
				';
				// To send HTML mail, the Content-type header must be set
				$headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				
				// Additional headers
				$headers .= 'Bcc: ashish.gupta@catabatic.co.in' . "\r\n";
				$headers .= 'From: customerfirst@religarehealthinsurance.com'.' \r\n';
				$subject="Login Details";
				// Mail it
				mail(@$dataArray[0]['EMAIL'], @$subject, $body1, $headers);
		}
}
function fetchListByemailId($tblname,$column1,$value1){
	global $conn;
	$dataArray=array();
	 $query="SELECT * FROM $tblname where $column1='$value1'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function fetchcustempid($tblname,$cond){
	global $conn;
	$dataArray=array();
    $query="SELECT EMPLOYEEID,CUSTOMERID FROM $tblname where $cond ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while ((@$row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function fetchListByColumnName($table,$column,$condColumn,$condColumnVal){
	global $conn;
	$dataArray=array();
	if(isset($table)){
		 $query="SELECT $column FROM $table WHERE $condColumn='$condColumnVal'";
		$sql = oci_parse($conn, $query);
		// Execute Query
		oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function SendMailToEmployee($email,$Password,$compTitle)
{
$to =  $email;  
$password=$Password;  		
		$Mailtemplates=GetTemplateContentByTemplateName('EMPLOYEE_TEMP');
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
	    $message  = stripslashes($Mailtemplates[0]['CONTENT']);
	    $message  = str_replace("##email",@$email,$message);			
	    $message  = str_replace("##password",@$password,$message);			
	   	$message  = str_replace("##comptitle",@$compTitle,$message);
		
		$headers  = 'From:'.stripslashes($Mailtemplates[0]['FROMNAME']).' <'.stripslashes($Mailtemplates[0]['FROMEMAIL']). "> \r\n" .
            'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1';
					// Mail it
					if(mail($to, $subject, $message, $headers))
					{
					$to=addslashes($to);
				 	$subject=addslashes($subject);
					$message=addslashes($message);
				 	$sql="INSERT INTO CRMSENTMAIL (MAILID,MAILTO,SUBJECT,MESSAGE,MAILDATE,TYPE) values(CRMSENTMAIL_SEQ.nextval,'".@$to."','".@$subject."','".@$message."','".@$entryTime."','".@$type."') ";
				$stdid = @oci_parse($conn, $sql);
				$r = @oci_execute($stdid);
					}


}
function GetTemplateContentByTemplateName($templateName){
	global $conn;
	$dataArray=array();
		 $query="SELECT TEMPLATEID,SUBJECT,CONTENT,LARGECONTENT,FROMEMAIL,FROMNAME,TEMPLATENAME from CRMEMAILTEMPLTE where TEMPLATENAME='".$templateName."' ";
		$sql = @oci_parse($conn, $query);
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function fetchListCondsWithColumn($column,$table,$cond){
	global $conn;
	$dataArray=array();
	$query="SELECT $column FROM $table $cond";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while ((@$row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function fetchpolicyDocList($policyNumber){
	global $conn;
	$dataArray=array();
		$query="SELECT CRMPOLICYDOCS.ACCESSHR,CRMPOLICYDOCS.ACCESSEMPLOYEE,CRMPOLICYDOCS.ACCESSAGENT FROM CRMPOLICYDOCS LEFT JOIN CRMPOLICY ON CRMPOLICYDOCS.POLICYID=CRMPOLICY.POLICYID WHERE CRMPOLICY.POLICYNUMBER='".$policyNumber."'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
	@oci_execute($sql);
		
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function fetchDocumentList($policyNumber){
	global $conn;
	$dataArray=array();
	 $query="SELECT CRMPOLICYDOCS.FILENAME,CRMPOLICYDOCS.TITLE,CRMPOLICYDOCS.STATUS FROM CRMPOLICYDOCS LEFT JOIN CRMPOLICY ON CRMPOLICY.POLICYID=CRMPOLICYDOCS.POLICYID WHERE POLICYNUMBER='".@$policyNumber."' AND CRMPOLICYDOCS.STATUS='ACTIVE' AND CRMPOLICYDOCS.ACCESSEMPLOYEE='Yes' ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function scriptname()
{
                $script_name=substr(strrchr($_SERVER['SCRIPT_NAME'],'/'),1);
                return $script_name;
}

function getReimbursementList($employeeId,$policyNumber){
	global $conn;
	$dataArray=array();
	if($policyNumber!=''){
		$query="SELECT * FROM CRMCOMPANYREIMBURSEMENTREQUEST where POLICYNO='".$policyNumber."' AND EMPLOYEEID='".$employeeId."' order by REQUESTID DESC ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function checkReimbursementRequest($requestId,$policyNumber){
	global $conn;
	$dataArray=array();
		$query="SELECT * FROM CRMCOMPANYREIMBURSEMENTREQUEST where REQUESTID='".@$requestId."' and POLICYNO='".@$policyNumber."'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function fetchReimbursementDocList($requestId){
	global $conn;
	$dataArray=array();
		$query="SELECT * FROM CRMCOMPANYREIMBURSEMENTDOCS where REQUESTID='".@$requestId."'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function getHealthCheckupPlansFree($policyId){
	global $conn;
	$dataArray=array();
	if(@$policyId>0){
		 $query="SELECT CRMCOMPANYHEALTHCHECKUPPLAN.*,CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANNAME AS PLANNAME2 FROM CRMCOMPANYHEALTHCHECKUPPLAN LEFT JOIN CRMCORPHEALTHCHECKUP ON CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CRMCOMPANYHEALTHCHECKUPPLAN.PLANNAMEID where CRMCOMPANYHEALTHCHECKUPPLAN.POLICYID='".$policyId."' AND CRMCOMPANYHEALTHCHECKUPPLAN.STATUS='ACTIVE'  AND CRMCOMPANYHEALTHCHECKUPPLAN.PAYMENTOPTIONS='FREE'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function fetchPolicyDetails($policyNumber){
	global $conn;
	$dataArray=array();
	if($policyNumber!=''){
		$query="SELECT * FROM CRMPOLICY where POLICYNUMBER='".$policyNumber."' AND STATUS='ACTIVE' ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function getHealthCheckupPlans($policyId){
	global $conn;
	$dataArray=array();
	if(@$policyId>0){
		$query="SELECT * FROM CRMCOMPANYHEALTHCHECKUPPLAN where POLICYID='".$policyId."' AND STATUS='ACTIVE' AND PAYMENTOPTIONS='BUY' ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function fetchhealthPlanList($cond){
	global $conn;
	$dataArray=array();
		$query="SELECT CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANNAME AS PLANNAME2,CRMCOMPANYHEALTHCHECKUPPLAN.PLANNAME,CRMCOMPANYHEALTHCHECKUPPLAN.AGEGROUPID,CRMCOMPANYHEALTHCHECKUPPLAN.SI,CRMCOMPANYHEALTHCHECKUPPLAN.PLANID,CRMCOMPANYHEALTHCHECKUPPLAN.PLANNAMEID,CRMCOMPANYHEALTHCHECKUPPLAN.GENDER,CRMCOMPANYHEALTHCHECKUPPLAN.PAYMENTOPTIONS,CRMCOMPANYHEALTHCHECKUPPLAN.COST,CRMCOMPANYHEALTHCHECKUPPLAN.EDITEDPRICE FROM CRMCOMPANYHEALTHCHECKUPPLAN LEFT JOIN CRMCORPHEALTHCHECKUP ON CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CRMCOMPANYHEALTHCHECKUPPLAN.PLANNAMEID ".@$cond."";
		$sql = @oci_parse($conn, $query);
		// Execute Query
	@oci_execute($sql);
		
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function fetchPlanDetailsById($cond){
	global $conn;
	$dataArray=array();
		$query="SELECT CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANNAME AS PLANNAME2,CRMCORPHEALTHCHECKUP.UPLOADPDF AS UPLOADPDF2,CRMCORPHEALTHCHECKUP.PLANDETAILS,CRMCORPHEALTHCHECKUP.TERMSANDCONDITION,CRMCORPHEALTHCHECKUP.EXCLUSION,CRMCOMPANYHEALTHCHECKUPPLAN.PLANNAME FROM CRMCOMPANYHEALTHCHECKUPPLAN LEFT JOIN CRMCORPHEALTHCHECKUP ON CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CRMCOMPANYHEALTHCHECKUPPLAN.PLANNAMEID ".@$cond."";
		$sql = @oci_parse($conn, $query);
		// Execute Query
	@oci_execute($sql);
		
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function fetchListById($tblname,$column1,$value1){
	global $conn;
	$dataArray=array();
	 $query="SELECT * FROM $tblname where $column1='$value1'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function fetchListCond($table,$cond){
	global $conn;
	$dataArray=array();
	  $query="SELECT * FROM $table $cond";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function fetchListByColumnNameCond($table,$column,$condColumn,$condColumnVal,$cond){
	global $conn;
	$dataArray=array();
	if(isset($table)){
	$query="SELECT $column FROM $table WHERE regexp_like($condColumn, '^".@$condColumnVal."$', 'i') $cond";
		$sql = oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function getHealthCheckupPlanDetails($policyId,$planId){
	global $conn;
	$dataArray=array();
	if(@$policyId>0){
		$query="SELECT * FROM CRMCOMPANYHEALTHCHECKUPPLAN where POLICYID='".$policyId."' AND PLANID='".@$planId."' AND STATUS='ACTIVE' ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function getHealChkupPlanDetailsConf($policyId,$planId){
	global $conn;
	$dataArray=array();
	if(@$policyId>0){
		$query="SELECT CRMCOMPANYHEALTHCHECKUPPLAN.*,CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANNAME AS PLANNAME2 FROM CRMCOMPANYHEALTHCHECKUPPLAN LEFT JOIN CRMCORPHEALTHCHECKUP ON  CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CRMCOMPANYHEALTHCHECKUPPLAN.PLANNAMEID  where CRMCOMPANYHEALTHCHECKUPPLAN.POLICYID='".$policyId."' AND CRMCOMPANYHEALTHCHECKUPPLAN.PLANID='".@$planId."' AND CRMCOMPANYHEALTHCHECKUPPLAN.STATUS='ACTIVE' "; 
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function getPurchasedPlanDetails($PURCHASEPLANID,$policyNumber){
	global $conn;
	$dataArray=array();
	if(@$policyNumber!=''){
		 $query="SELECT CRMEMPLOYEEPURCHASEPLAN.*,CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANNAME AS PLANNAME2 FROM CRMEMPLOYEEPURCHASEPLAN LEFT JOIN CRMCORPHEALTHCHECKUP ON CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CRMEMPLOYEEPURCHASEPLAN.PLANNAMEID  where CRMEMPLOYEEPURCHASEPLAN.PURCHASEPLANID='".$PURCHASEPLANID."' AND CRMEMPLOYEEPURCHASEPLAN.POLICYNUMBER='".@$policyNumber."' ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function getHealthCheckupPlanMemberDetails($PURCHASEPLANID,$PLANID){
	global $conn;
	$dataArray=array();
	if(@$PURCHASEPLANID>0){
		$query="SELECT * FROM CRMEMPLOYEEPLANMEMBER where PURCHASEPLANID='".$PURCHASEPLANID."' AND PLANID='".@$PLANID."' ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function getExistingAppointmentList($employeeId,$policyNumber){
	global $conn;
	$dataArray=array();
	if($policyNumber!=''){
	$query="SELECT CRMCOMPANYPOSTAPPOINTMENT.* FROM CRMCOMPANYPOSTAPPOINTMENT where CRMCOMPANYPOSTAPPOINTMENT.POLICYNUMBER='".$policyNumber."' ORDER BY CRMCOMPANYPOSTAPPOINTMENT.APPOINTMENTID DESC ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function getExistingAppointmentListByEmp($employeeId,$policyNumber){
	global $conn;
	$dataArray=array();
	if($policyNumber!=''){
	$query="SELECT CRMCOMPANYPOSTAPPOINTMENT.* FROM CRMCOMPANYPOSTAPPOINTMENT where CRMCOMPANYPOSTAPPOINTMENT.POLICYNUMBER='".$policyNumber."' AND CRMCOMPANYPOSTAPPOINTMENT.EMPLOYEEID='".$employeeId."'  ORDER BY CRMCOMPANYPOSTAPPOINTMENT.APPOINTMENTID DESC ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function getRetExistingAppointmentListByEmp($employeeId,$policyNumber){
	global $conn;
	$dataArray=array();
	if($policyNumber!=''){
	$query="SELECT CRMRETAILPOSTAPPOINTMENT.* FROM CRMRETAILPOSTAPPOINTMENT where CRMRETAILPOSTAPPOINTMENT.POLICYNUMBER='".$policyNumber."' AND (CRMRETAILPOSTAPPOINTMENT.CUSTOMERID='".@$_SESSION['customerId']."' OR CRMRETAILPOSTAPPOINTMENT.EMPLOYEEID='".@$_SESSION['customerId']."') AND CRMRETAILPOSTAPPOINTMENT.PRODUCTID='".@$_SESSION['productId']."'  ORDER BY CRMRETAILPOSTAPPOINTMENT.APPOINTMENTID DESC ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function printr($value)
{
    echo "<pre>";
   print_r($value);
   exit;

}	
function getHealthCheckupPurchasePlan($policyNumber,$clientNumber,$companyId){
	global $conn;
	$dataArray=array();
	if(@$policyNumber!=''){
		$query="SELECT CEP.*,CCCP.PLANID,CCCP.PLANNAME,CCCHP.HEALTHCHECKUPPLANNAME AS PLANNAME2,CCCP.AGEGROUPID,CCCP.SI,CCCP.PLANLIMITS,CCCP.COST,CCCP.PAYMENTOPTIONS,CCCP.GENDER as PLANGENDER FROM CRMEMPLOYEEPURCHASEPLAN CEP LEFT JOIN CRMCOMPANYHEALTHCHECKUPPLAN CCCP ON CEP.PLANID=CCCP.PLANID LEFT JOIN CRMCORPHEALTHCHECKUP CCCHP ON   CCCHP.HEALTHCHECKUPPLANID=CCCP.PLANNAMEID where CEP.POLICYNUMBER='".$policyNumber."'  AND CEP.COMPANYID='".$companyId."' AND CEP.CLIENTID='".$clientNumber."' AND CEP.PAYMENTRECEIVED='YES'  ORDER BY CEP.PURCHASEPLANID DESC";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function getHealthCheckupPurchasePlanAll($policyNumber,$clientNumber,$companyId){
	global $conn;
	$dataArray=array();
	if(@$policyNumber!=''){
		 $query="SELECT CEP.*,CCCHP.HEALTHCHECKUPPLANNAME AS PLANNAME2,CCCP.PLANID,CCCP.PLANNAME,CCCP.AGEGROUPID,CCCP.SI,CCCP.PLANLIMITS,CCCP.COST,CCCP.PAYMENTOPTIONS FROM CRMEMPLOYEEPURCHASEPLAN CEP LEFT JOIN CRMCOMPANYHEALTHCHECKUPPLAN CCCP ON CEP.PLANID=CCCP.PLANID LEFT JOIN CRMCORPHEALTHCHECKUP CCCHP ON   CCCHP.HEALTHCHECKUPPLANID=CCCP.PLANNAMEID where CEP.POLICYNUMBER='".$policyNumber."'  AND CEP.COMPANYID='".$companyId."' AND CEP.CLIENTID='".$clientNumber."'   ORDER BY CEP.PURCHASEPLANID DESC";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function checkLimitedPlanForEmployee($policyId,$empNo,$clientNumber){
	global $conn;
	$dataArray=array();
	if($policyId!=''){
		  $query1=sprintf("SELECT * FROM CRMLIMITEDEMPLOYEE  WHERE POLICYID = '%s' AND (EMPLOYEENUMBER like '%s' OR CLIENTNUMBER like '%s'  )  AND LIMITEDAPPOINTMENT='NO' AND STATUS='ACTIVE' ",
				@$policyId,
				@$empNo,
				@$clientNumber);
		$sql = @oci_parse($conn, $query1);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function get_particular_plan_details($planId,$policyId){
	global $conn;
	$dataArray=array();
	if($policyId>0){
		$query="SELECT CRMCOMPANYHEALTHCHECKUPPLAN.*,CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANNAME AS PLANNAME2 FROM CRMCOMPANYHEALTHCHECKUPPLAN LEFT JOIN CRMCORPHEALTHCHECKUP ON  CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CRMCOMPANYHEALTHCHECKUPPLAN.PLANNAMEID where CRMCOMPANYHEALTHCHECKUPPLAN.POLICYID='".$policyId."' AND CRMCOMPANYHEALTHCHECKUPPLAN.STATUS='ACTIVE' and CRMCOMPANYHEALTHCHECKUPPLAN.PLANID='".@$planId."'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function get_particular_plan_detailsall($planId,$policyId){
	global $conn;
	$dataArray=array();
	if($policyId>0){
		$query="SELECT CRMCOMPANYHEALTHCHECKUPPLAN.*,CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANNAME AS PLANNAME2,CRMCORPHEALTHCHECKUP.UPLOADPDF AS UPLOADPDF2 FROM CRMCOMPANYHEALTHCHECKUPPLAN LEFT JOIN CRMCORPHEALTHCHECKUP ON  CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CRMCOMPANYHEALTHCHECKUPPLAN.PLANNAMEID where CRMCOMPANYHEALTHCHECKUPPLAN.POLICYID='".$policyId."' AND CRMCOMPANYHEALTHCHECKUPPLAN.PLANID='".@$planId."'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function checkPlanForEmployee($policyId,$empNo,$clientNumber){
	global $conn;
	$dataArray=array();
	$countPlan=0;
	if($policyId!=''){
		$query="SELECT * FROM CRMCOMPANYHEALTHCHECKUPPLAN where POLICYID='".$policyId."' AND STATUS='ACTIVE' AND PAYMENTOPTIONS='FREE' ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
		 $query1=sprintf("SELECT * FROM CRMEMPLOYEEPURCHASEPLAN  WHERE POLICYID = '%s' AND (EMPLOYEEID like '%s' OR CLIENTID like '%s'  )  AND PAYMENTSTATUS='CONFIRMED' AND PAYMENTRECEIVED='YES' ",
				@$policyId,
				@$empNo,
				@$clientNumber);
		$sql = @oci_parse($conn, $query1);
		// Execute Query
		@oci_execute($sql);
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return count($dataArray);
}
function get_particular_plan_details_for_policy($planId,$policyId){
	global $conn;
	$dataArray=array();
	if($policyId>0){
		 $query="SELECT CRMCOMPANYHEALTHCHECKUPPLAN.*,CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANNAME AS PLANNAME2 FROM CRMCOMPANYHEALTHCHECKUPPLAN LEFT JOIN CRMCORPHEALTHCHECKUP ON CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=CRMCOMPANYHEALTHCHECKUPPLAN.PLANNAMEID where CRMCOMPANYHEALTHCHECKUPPLAN.POLICYID='".$policyId."' and CRMCOMPANYHEALTHCHECKUPPLAN.PLANID='".@$planId."'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function fetchpurchasePlanDetailsByplanId($purchaseplanId,$planId,$policyId){
	global $conn;
	$dataArray=array();
	if($policyId>0){
		$query="SELECT CRMEMPLOYEEPLANMEMBER.* FROM CRMEMPLOYEEPLANMEMBER LEFT JOIN CRMEMPLOYEEPURCHASEPLAN ON CRMEMPLOYEEPURCHASEPLAN.PURCHASEPLANID=CRMEMPLOYEEPLANMEMBER.PURCHASEPLANID  where CRMEMPLOYEEPURCHASEPLAN.POLICYID=".$policyId." AND CRMEMPLOYEEPURCHASEPLAN.PLANID=".@$planId." AND CRMEMPLOYEEPLANMEMBER.PLANID=".@$planId." AND CRMEMPLOYEEPLANMEMBER.PURCHASEPLANID=".@$purchaseplanId." ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function fetchStateNameId($tblname,$column,$value){
	global $conn;
	$dataArray=array();
		$query="SELECT STATENAME FROM $tblname where $column='$value'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function fetchExistcanceledPlan($table,$column1,$Columnvalue1,$column2,$Columnvalue2){
	global $conn;
	$dataArray=array();
	if(isset($table)){
		$query="SELECT $column1,$column2 FROM $table WHERE $column1='".$Columnvalue1."' AND $column2=$Columnvalue2 AND STATUS!='CANCELLED' ";
		$sql = oci_parse($conn, $query);
		// Execute Query
		oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function SendMailToEmp($EMAILID,$centerName,$EMPLOYEELOCATION,$appointmentDate,$tmslot1,$appointmentDate2,$tmslot2){
 		$to =  $EMAILID;   
		$Mailtemplates=GetTemplateContentByTemplateName('EMPPOSTAPPOINTMENT_SCH');
		$from=stripslashes($Mailtemplates[0]['FROMEMAIL']);
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
	    $message  = stripslashes($Mailtemplates[0]['CONTENT']);
	    $message  = str_replace("##center",@$centerName,$message);	
		$message  = str_replace("##address",@$EMPLOYEELOCATION,$message);	
		$message  = str_replace("##Date1",@$appointmentDate,$message);
		$message  = str_replace("##Date2",@$appointmentDate2,$message);
        $message  = str_replace("##timeslot1",@$tmslot1,$message);
	    $message  = str_replace("##timeslot2",@$tmslot2,$message);
		$headers  = 'From:'.stripslashes($Mailtemplates[0]['FROMEMAIL'])."\r\n" .
					'Cc:  wellness@religare.com' . "\r\n" .
			    'MIME-Version: 1.0' . "\r\n" .
				'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				
			if(mail($to, $subject, $message, $headers)){
				 $to=addslashes($to);
				 $subject=addslashes($subject);
				 $message=addslashes($message);
				 	$sql="INSERT INTO CRMSENTMAIL (MAILID,MAILTO,SUBJECT,MESSAGE,MAILDATE,TYPE) values(CRMSENTMAIL_SEQ.nextval,'".@$to."','".@$subject."','".@$message."','".@$entryTime."','".@$type."') ";
				$stdid = @oci_parse($conn, $sql);
				$r = @oci_execute($stdid);
			}			
				

}
function SendMailToEmpRetail($EMAILID,$centerName,$EMPLOYEELOCATION,$appointmentDate,$tmslot1,$appointmentDate2,$tmslot2){
 		$to =  $EMAILID;   
		$Mailtemplates=GetTemplateContentByTemplateName('EMPPOSTAPPOINTMENT_SCH_RETAIL');
		$from=stripslashes($Mailtemplates[0]['FROMEMAIL']);
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
	    $message  = $Mailtemplates[0]['LARGECONTENT']?stripslashes($Mailtemplates[0]['LARGECONTENT']->load()):stripslashes($Mailtemplates[0]['CONTENT']);
	    $message  = str_replace("##center",@$centerName,$message);	
		$message  = str_replace("##address",@$EMPLOYEELOCATION,$message);	
		$message  = str_replace("##Date1",@$appointmentDate,$message);
		$message  = str_replace("##Date2",@$appointmentDate2,$message);
        $message  = str_replace("##timeslot1",@$tmslot1,$message);
	    $message  = str_replace("##timeslot2",@$tmslot2,$message);
		$headers  = 'From:'.stripslashes($Mailtemplates[0]['FROMEMAIL'])."\r\n" .
					'Cc:  healthcheckup@religare.com' . "\r\n" .
			    'MIME-Version: 1.0' . "\r\n" .
				'Content-type: text/html; charset=iso-8859-1' . "\r\n";
				
			if(mail($to, $subject, $message, $headers)){
				 $to=addslashes($to);
				 $subject=addslashes($subject);
				 $message=addslashes($message);
				 	$sql="INSERT INTO CRMSENTMAIL (MAILID,MAILTO,SUBJECT,MESSAGE,MAILDATE,TYPE) values(CRMSENTMAIL_SEQ.nextval,'".@$to."','".@$subject."','".@$message."','".@$entryTime."','".@$type."') ";
				$stdid = @oci_parse($conn, $sql);
				$r = @oci_execute($stdid);
			}			
				

}
function get_member_list_for_free_appointment($responseArray,$relationArray){
	$purchaseId=$responseArray['purchaseId'];
	$planId=$responseArray['planId'];
	$companyId=$responseArray['companyId'];
	$policyId=$responseArray['policyId'];
	$planDetails=get_particular_plan_details_for_policy($planId,@$policyId);// fetch plan details for plan id
	$memberList=array();
	$l=0;
	
	if(@$planDetails[0]['PLANLIMITS']=="ONLYEMPLOYEES"){ 
		// check employee valid for appointment
		for($i=0;$i<count($responseArray);$i++){
		if($i == 0 ){//if primary member
			$planDisplay=0;
			if(trim($responseArray[$i]['BGEN-GENDER']['#text'])=="F"){$gender="Female";	}
			if(trim($responseArray[$i]['BGEN-GENDER']['#text'])=="M"){$gender="Male";	}
			if(strtolower($gender)==strtolower(@$planDetails[0]['GENDER']) || strtolower(@$planDetails[0]['GENDER'])=="both"){
							$planDisplay++;
			}
			
				$pieces = explode(',' , @$planDetails[0]['AGEGROUPID']);
				$policyHolderAge=str_replace(",","",number_format(trim($responseArray[$i]['BGEN-AGE']['#text'])));
				//$policyHolderAge=24;
				for($j=0;$j<count($pieces);$j++) {
				// age group according to new
					$ageName1.=$pieces[$j].", ";
					if($policyHolderAge == $pieces[$j]){
								$planDisplay++;
								break;
					}
			   }
				
				$si_list = explode(',' , @$planDetails[0]['SI']);
				$policyHolderSI=str_replace(",","",number_format(@$_SESSION['policyHolderSI']));
				//$policyHolderSI=100000;
				for($j=0;$j<count($si_list);$j++) {
						$siName=fetchListById('CRMSI','SIID',$si_list[$j]); 
						if($siName[0]['SI']==$policyHolderSI){
							$planDisplay++;
						}
				}
				if($planDisplay==3){
					$memberAvail=checkMemberAvailAppointment($planId,@$companyId,sanitize_title(trim(@$responseArray[$i]['BGEN-MEMNAME']['#text'])),$policyHolderAge);
					if(count($memberAvail)==0){
						$memberList[$l]['NAME']=trim($responseArray[$i]['BGEN-MEMNAME']['#text']);
						$memberList[$l]['AGE']=trim($responseArray[$i]['BGEN-AGE']['#text']);
						$memberList[$l]['RELATION']=trim($responseArray[$i]['BGEN-DPNTTYP']['#text']);
						$memberList[$l]['GENDER']=@$gender;
						$l++;
					}
				}
			}
		}
	}else if(@$planDetails[0]['PLANLIMITS']=="FAMILYALLOWED"){
		//start family allowed
		// check employee valid for appointment
		for($i=0;$i<count($responseArray);$i++){
		if($i > 0 ){//if primary member
			$planDisplay=0;
			if(trim(@$responseArray[$i]['BGEN-GENDER']['#text'])=="F"){$gender="Female";	}
			if(trim(@$responseArray[$i]['BGEN-GENDER']['#text'])=="M"){$gender="Male";	}
			if(strtolower($gender)==strtolower(@$planDetails[0]['GENDER']) || strtolower(@$planDetails[0]['GENDER'])=="both"){
							$planDisplay++;
			}
				$pieces = explode(',' , @$planDetails[0]['AGEGROUPID']);
				$policyHolderAge=str_replace(",","",@number_format(@$responseArray[$i]['BGEN-AGE']['#text']));
				for($j=0;$j<count($pieces);$j++) {
				// age group according to new
					$ageName1.=$pieces[$j].", ";
					if($policyHolderAge == $pieces[$j]){
								$planDisplay++;
								break;
					}
			   }
				$si_list = explode(',' , @$planDetails[0]['SI']);
				$policyHolderSI=str_replace(",","",@number_format(@$_SESSION['policyHolderSI']));
				for($j=0;$j<count($si_list);$j++) {
						$siName=fetchListById('CRMSI','SIID',$si_list[$j]); 
						if($siName[0]['SI']==$policyHolderSI){
							$planDisplay++;
						}
				}
				if($planDisplay==3){
					$memberAvail=checkMemberAvailAppointment($planId,@$companyId,sanitize_title(trim(@$responseArray[$i]['BGEN-MEMNAME']['#text'])),$policyHolderAge);
					if(count($memberAvail)==0){
						$memberList[$l]['NAME']=trim($responseArray[$i]['BGEN-MEMNAME']['#text']);
						$memberList[$l]['AGE']=trim($responseArray[$i]['BGEN-AGE']['#text']);
						$memberList[$l]['RELATION']=trim($responseArray[$i]['BGEN-DPNTTYP']['#text']);
						$memberList[$l]['GENDER']=@$gender;
						$l++;
					}
				}
			}
		}
		//end family allowed
	}else if(@$planDetails[0]['PLANLIMITS']=="BOTH"){
		//start both
		for($i=0;$i<count($responseArray);$i++){
			$planDisplay=0;
			if(trim(@$responseArray[$i]['BGEN-GENDER']['#text'])=="F"){$gender="Female";	}
			if(trim(@$responseArray[$i]['BGEN-GENDER']['#text'])=="M"){$gender="Male";	}
			if(strtolower($gender)==strtolower(@$planDetails[0]['GENDER']) || strtolower(@$planDetails[0]['GENDER'])=="both"){
							$planDisplay++;
			}
				$pieces = explode(',' , @$planDetails[0]['AGEGROUPID']);
				$policyHolderAge=str_replace(",","",@number_format(trim(@$responseArray[$i]['BGEN-AGE']['#text'])));
				for($j=0;$j<count($pieces);$j++) {
				// age group according to new
					$ageName1.=$pieces[$j].", ";
					if($policyHolderAge == $pieces[$j]){
								$planDisplay++;
								break;
					}
			   }
				$si_list = explode(',' , @$planDetails[0]['SI']);
				$policyHolderSI=str_replace(",","",@number_format(@$_SESSION['policyHolderSI']));
				for($j=0;$j<count($si_list);$j++) {
						$siName=fetchListById('CRMSI','SIID',$si_list[$j]); 
						if($siName[0]['SI']==$policyHolderSI){
							$planDisplay++;
						}
				}
				if($planDisplay==3){
					$memberAvail=checkMemberAvailAppointment($planId,@$companyId,sanitize_title(trim(@$responseArray[$i]['BGEN-MEMNAME']['#text'])),$policyHolderAge);
					if(count($memberAvail)==0){

						$applicatefor=@$planDetails[0]['FREEMAPPEDMEMBERTYPEIDS'];
						$title='';
						if(isset($applicatefor) && !empty($applicatefor)){
							$appres=json_decode($applicatefor,true);
							$appres=implode(',',$appres);
							$membersname= fetchcolumnListCond('TITLE','CRMCORPMEMBERTYPE','WHERE MEMBERTYPEID IN('.$appres.') ');
							$f=0;
							$titles=array();
							while($f<count($membersname)){
								$titles[]=$membersname[$f]['TITLE'];
							$f++; }
							if(count($titles)>0){
								$title=$titles;
							} else {
								$title='';
							}
						}
						$title = str_ireplace('self', 'primary member', $title);
						$array1=$title;
						$array2=$_SESSION['membersarray'];
						

						if(count($title)>0){
						
							 /*$relationArray = array(
											'MMBR' =>  'primary member'  , 	'UDTR' => 'daughter', 	'BOTH' =>  'brother' , 
											'CHD2' =>  'Child'           , 	'CHD3' =>  'Child'  , 	'CHLD' => 'Child',
											'DLAW' =>  'Daughter In Law' , 	'EFAT' => 'father'  , 	'EFLW' =>  'father In Law' , 
											'EMLW' =>  'mother In Law'   ,   'EMOT' =>  'mother' , 	'FATH' => 'father', 
											'FLAW' =>  'father In Law'   ,   'GDAU' => 'Grand Daughter', 'GFAT' =>  'grand father' , 
											'GMOT' =>  'grand mother'    , 	'GSON' =>  'grand son', 'MLAW' => 'mothe in law',
											'MOTH' =>  'mother'         , 	'SIST' => 'sister'    , 'SLAW' =>  'son in law' , 
											'SONM' =>  'son'             ,	'SPSE' => 'Spouse'    ,	'UDTR' => 'daughter',
										);*/
						//echo strtolower(@$relationArray[trim(@$responseArray[$i]['BGEN-DPNTTYP']['#text'])]); die;
						$resultmapped=array_intersect(array_map('strtolower', $array1), array_map('strtolower', $array2));
						 if(in_array(strtolower(@$relationArray[trim(@$responseArray[$i]['BGEN-DPNTTYP']['#text'])]),$resultmapped)){
							$memberList[$l]['NAME']=trim(@$responseArray[$i]['BGEN-MEMNAME']['#text']);
							$memberList[$l]['AGE']=trim(@$responseArray[$i]['BGEN-AGE']['#text']);
							$memberList[$l]['RELATION']=trim(@$responseArray[$i]['BGEN-DPNTTYP']['#text']);
							$memberList[$l]['GENDER']=@$gender;
							$l++;
						 }
						} else {
							$memberList[$l]['NAME']=trim(@$responseArray[$i]['BGEN-MEMNAME']['#text']);
							$memberList[$l]['AGE']=trim(@$responseArray[$i]['BGEN-AGE']['#text']);
							$memberList[$l]['RELATION']=trim(@$responseArray[$i]['BGEN-DPNTTYP']['#text']);
							$memberList[$l]['GENDER']=@$gender;
							$l++;
						}
						
					}
				}
			}
		//end both allowed
	}
	return $memberList;
}
function get_balance_member_list_for_appointment($purchaseId,$planId,$companyId){
	global $conn;
	$dataArray=array();
		  $query="SELECT * FROM CRMEMPLOYEEPLANMEMBER where PURCHASEPLANID='".$purchaseId."' AND  PLANID='".@$planId."' AND  COMPANYID='".@$companyId."' AND MEMBERID NOT IN (SELECT MEMBERID FROM CRMCOMPANYPOSTAPPOINTMENT WHERE  PURCHASEID='".$purchaseId."' AND STATUS !='CANCELLED' AND MEMBERID IS NOT NULL)";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function checkMemberAvailAppointment($planId,$companyId,$name,$age){
		global $conn;
		$dataArray=array();
		$query="SELECT * FROM CRMCOMPANYPOSTAPPOINTMENT where   HEALTHCHECKUPID='".@$planId."' AND  COMPANYID='".@$companyId."' AND  EMPLOYEEID='".@$_SESSION['empId']."' AND  LOWER(FIRSTNAME) like '".trim(strtolower(@$name))."'  AND STATUS !='CANCELLED' ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function fetchCityListById($tblname,$cond){
	global $conn;
	$dataArray=array();
		$query="SELECT * FROM $tblname $cond";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}	
function fetchCenterListByPlanId($stateId,$cityId,$planId){
		global $conn;
		$dataArray=array();
		$query="SELECT LWCENTERMASTER.CENTERNAME,LWCENTERMASTER.CENTERID,LWCENTERMASTER.ADDRESS1,LWCENTERMASTER.ADDRESS2,LWCITYMASTER.CITYNAME from LWCENTERMASTER LEFT JOIN  LWCITYMASTER ON LWCITYMASTER.CITYID=LWCENTERMASTER.CITYID where CENTERID IN (SELECT CENTERID from CRMCOMPANYCENTERMAPPING where COMPANYID='".@$_SESSION['COMPANYID']."' AND PLANID=".@$planId." AND POLICYNUMBER='".@$_SESSION['policyNumber']."') AND LWCENTERMASTER.CITYID='".@$cityId."' AND LWCENTERMASTER.STATEID='".@$stateId."' AND LWCENTERMASTER.STATUS='ACTIVE' order by UPPER(LWCENTERMASTER.CENTERNAME) ASC ";	

		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
	while (($row = oci_fetch_assoc($sql))) {
		foreach($row as $key=>$value){
			$dataArray[$i][$key] = $value;				
		}
		$i++;
	 }
	return $dataArray;
}	
function touploadItem($filedName ,$target_path,$filename)
{
	$target_path = $target_path.$filename;
	  if(move_uploaded_file($_FILES[$filedName]['tmp_name'], $target_path)) 		{
			chmod($target_path,0777); 			
		}
		return $filename;
}
//function to Handle Space
function toHAndleSpace($string)
{
   $string = @ereg_replace("[ \t\n\r]+", " ", $string); 
   $text = @str_replace(" ","_", $string);
	return $text;  
	}
	
// start for all mails///

// this function is used when rembersment request submit. page:reimbursement_request.php	
function FunSendReimbursementMail($email,$policyNumber,$name,$phone,$enquiry){

		$to =  $email; 
		$Mailtemplates=GetTemplateContentByTemplateName('EMPREIMBURSEMENTREQUEST');
		$from=stripslashes($Mailtemplates[0]['FROMEMAIL']);
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
		$subject  = str_replace("##policyNumber",@$policyNumber,$subject);
		$subject  = str_replace("##Name",@$name,$subject);
	    $message  = stripslashes($Mailtemplates[0]['CONTENT']);		
	    $message  = str_replace("##policyNumber",@$policyNumber,$message);	
		$message  = str_replace("##Name",@$name,$message);	
		$message  = str_replace("##Email",@$email,$message);
		$message  = str_replace("##Phone",@$phone,$message);
        $message  = str_replace("##Enquiry",@$enquiry,$message);	
		$headers  = 'From:'.@$from."\r\n" .'MIME-Version: 1.0' . "\r\n" .'Content-type: text/html; charset=iso-8859-1' . "\r\n";

			if(mail($to, $subject, $message, $headers)){
				 $to=addslashes($to);
				 $subject=addslashes($subject);
				 $message=addslashes($message);
				 	$sql="INSERT INTO CRMSENTMAIL (MAILID,MAILTO,SUBJECT,MESSAGE,MAILDATE,TYPE) values(CRMSENTMAIL_SEQ.nextval,'".@$to."','".@$subject."','".@$message."','".@$entryTime."','".@$type."') ";
				$stdid = @oci_parse($conn, $sql);
				$r = @oci_execute($stdid);
			}
		

}
function sanitize_search($input_data) {
	$searchArr=array("document","write","alert","%","@","$",";","+","|","#","<",">",")","(","'","\'",",","http://");
	$input_data= str_replace("script","",$input_data);
	$input_data= str_replace("iframe","",$input_data);
	$input_data= str_replace($searchArr,"",$input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}
function xml2array($contents, $get_attributes=1, $priority = 'tag')
    {
        if(!$contents) return array();
   
        if(!function_exists('xml_parser_create')) {
            //print "'xml_parser_create()' function not found!";
            return array();
        }
   
        //Get the XML parser of PHP - PHP must have this module for the parser to work
        $parser = xml_parser_create('');
        xml_parser_set_option($parser, XML_OPTION_TARGET_ENCODING, "UTF-8"); # http://minutillo.com/steve/weblog/2004/6/17/php-xml-and-character-encodings-a-tale-of-sadness-rage-and-data-loss
        xml_parser_set_option($parser, XML_OPTION_CASE_FOLDING, 0);
        xml_parser_set_option($parser, XML_OPTION_SKIP_WHITE, 1);
        xml_parse_into_struct($parser, trim($contents), $xml_values);
        xml_parser_free($parser);
   
        if(!$xml_values) return;//Hmm...
   
        //Initializations
        $xml_array = array();
        $parents = array();
        $opened_tags = array();
        $arr = array();
   
        $current = &$xml_array; //Refference
   
        //Go through the tags.
        $repeated_tag_index = array();//Multiple tags with same name will be turned into an array
        foreach($xml_values as $data) {
            unset($attributes,$value);//Remove existing values, or there will be trouble
   ini_set('memory_limit', '-1');
            //This command will extract these variables into the foreach scope
            // tag(string), type(string), level(int), attributes(array).
            extract($data);//We could use the array by itself, but this cooler.
   
            $result = array();
            $attributes_data = array();
           
            if(isset($value)) {
                if($priority == 'tag') $result = $value;
                else $result['value'] = $value; //Put the value in a assoc array if we are in the 'Attribute' mode
            }
   
            //Set the attributes too.
            if(isset($attributes) and $get_attributes) {
                foreach($attributes as $attr => $val) {
                    if($priority == 'tag') $attributes_data[$attr] = $val;
                    else $result['attr'][$attr] = $val; //Set all the attributes in a array called 'attr'
                }
            }
   
            //See tag status and do the needed.
            if($type == "open") {//The starting of the tag '<tag>'
                $parent[$level-1] = &$current;
                if(!is_array($current) or (!in_array($tag, array_keys($current)))) { //Insert New tag
                    $current[$tag] = $result;
                    if($attributes_data) $current[$tag. '_attr'] = $attributes_data;
                    $repeated_tag_index[$tag.'_'.$level] = 1;
   
                    $current = &$current[$tag];
   
                } else { //There was another element with the same tag name
   
                    if(isset($current[$tag][0])) {//If there is a 0th element it is already an array
                        $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
                        $repeated_tag_index[$tag.'_'.$level]++;
                    } else {//This section will make the value an array if multiple tags with the same name appear together
                        $current[$tag] = array($current[$tag],$result);//This will combine the existing item and the new item together to make an array
                        $repeated_tag_index[$tag.'_'.$level] = 2;
                       
                        if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
                            $current[$tag]['0_attr'] = $current[$tag.'_attr'];
                            unset($current[$tag.'_attr']);
                        }
   
                    }
                    $last_item_index = $repeated_tag_index[$tag.'_'.$level]-1;
                    $current = &$current[$tag][$last_item_index];
                }
   
            } elseif($type == "complete") { //Tags that ends in 1 line '<tag />'
                //See if the key is already taken.
                if(!isset($current[$tag])) { //New Key
                    $current[$tag] = $result;
                    $repeated_tag_index[$tag.'_'.$level] = 1;
                    if($priority == 'tag' and $attributes_data) $current[$tag. '_attr'] = $attributes_data;
   
                } else { //If taken, put all things inside a list(array)
                    if(isset($current[$tag][0]) and is_array($current[$tag])) {//If it is already an array...
   
                        // ...push the new element into that array.
                        $current[$tag][$repeated_tag_index[$tag.'_'.$level]] = $result;
                       
                        if($priority == 'tag' and $get_attributes and $attributes_data) {
                            $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
                        }
                        $repeated_tag_index[$tag.'_'.$level]++;
   
                    } else { //If it is not an array...
                        $current[$tag] = array($current[$tag],$result); //...Make it an array using using the existing value and the new value
                        $repeated_tag_index[$tag.'_'.$level] = 1;
                        if($priority == 'tag' and $get_attributes) {
                            if(isset($current[$tag.'_attr'])) { //The attribute of the last(0th) tag must be moved as well
                               
                                $current[$tag]['0_attr'] = $current[$tag.'_attr'];
                                unset($current[$tag.'_attr']);
                            }
                           
                            if($attributes_data) {
                                $current[$tag][$repeated_tag_index[$tag.'_'.$level] . '_attr'] = $attributes_data;
                            }
                        }
                        $repeated_tag_index[$tag.'_'.$level]++; //0 and 1 index is already taken
                    }
                }
   
            } elseif($type == 'close') { //End of tag '</tag>'
                $current = &$parent[$level-1];
            }
        }
		
        return($xml_array);
    }
	function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
    $sort_col = array();
    foreach ($arr as $key=> $row) {
        $sort_col[$key] = $row[$col];
    }

    return array_multisort($sort_col, $dir, $arr);
}
function static_page_array($pageListArray1){
	$m =0;
	$dataArr=array();
	if(is_array($pageListArray1['page'][0]) )	{
		$pageListArray = $pageListArray1['page'];
		for($i=0;$i <sizeof($pageListArray);$i++)
		{
			if(trim($pageListArray[$i]['isDeleted']['value']) =='no'){
				@$dataArr[$m]['staticId'] 		  = trim($pageListArray[$i]['staticId']['value']);
				@$dataArr[$m]['staticTitle'] 	  = $pageListArray[$i]['staticTitle']['value'];
				@$dataArr[$m]['pageCode'] 		  = $pageListArray[$i]['pageCode']['value'];
				@$dataArr[$m]['link'] 		  		= $pageListArray[$i]['link']['value'];
				@$dataArr[$m]['linkType'] 		  	= $pageListArray[$i]['linkType']['value'];
				@$dataArr[$m]['staticDescription'] = $pageListArray[$i]['staticDescription']['value'];
				$dataArr[$m]['isDeleted']         = trim($pageListArray[$i]['isDeleted']['value']);
				$m++;
			}
		}
	}else	{
			$pageListArray = $pageListArray1['page'];	
			if(trim($pageListArray['isDeleted']['value']) =='no') 	{
					$dataArr[$m]['staticId'] 		= trim($pageListArray['staticId']['value']);
					$dataArr[$m]['staticTitle'] 	= $pageListArray['staticTitle']['value'];
					$dataArr[$m]['pageCode']		= $pageListArray['pageCode']['value'];
					$dataArr[$m]['link']			= $pageListArray['link']['value'];
					$dataArr[$m]['linkType']		= $pageListArray['linkType']['value'];
					$dataArr[$m]['staticDescription'] = $pageListArray['staticDescription']['value'];
					$dataArr[$m]['isDeleted']		 = trim($pageListArray[$i]['isDeleted']['value']);			
					$m++;
				}
	}
//sort by
	if (count ($dataArr) > 0) {
    foreach ($dataArr as $key => $row) {
        @$order_in_category[$key]  = $row[0];//[0] is @$order_in_category
    }
    	@array_multisort(@$order_in_category, SORT_ASC, $dataArr);
	}
return $dataArr;
}
function cities_page_array($pageListArray1){
	$m =0;
	$dataArr=array();
	if(is_array(@$pageListArray1['page'][0]) )	{
		$pageListArray = $pageListArray1['page'];
		for($i=0;$i <sizeof($pageListArray);$i++)
		{
			if(trim($pageListArray[$i]['isDeleted']['value']) =='no'){
				$dataArr[$m]['stateId'] 		  		= @$pageListArray[$i]['stateId']['value'];
				$dataArr[$m]['cityId'] 		  		= @$pageListArray[$i]['cityId']['value'];
				$dataArr[$m]['cityTitle'] 	 		 = @$pageListArray[$i]['cityTitle']['value'];
				$dataArr[$m]['isDeleted']         = trim(@$pageListArray[$i]['isDeleted']['value']);
				$m++;
			}
		}
	}else	{
			$pageListArray = @$pageListArray1['page'];	
			if(trim($pageListArray['isDeleted']['value']) =='no') 	{
					$dataArr[$m]['stateId'] 		= @$pageListArray['stateId']['value'];
					$dataArr[$m]['cityId'] 			= $pageListArray['cityId']['value'];
					$dataArr[$m]['cityTitle'] 		= @$pageListArray['cityTitle']['value'];
					$dataArr[$m]['isDeleted']		= trim(@$pageListArray['isDeleted']['value']);			
					$m++;
				}
	}
//sort by
/*	if (count ($dataArr) > 0) {
    foreach ($dataArr as $key => $row) {
        @$order_in_category[$key]  = $row[0];//[0] is @$order_in_category
    }
    	@array_multisort(@$order_in_category, SORT_ASC, $dataArr);
	}*/
return $dataArr;
}
function hospital_page_array($pageListArray1,$city,$advanceSearch){
	$m =0; 
	if(is_array(@$pageListArray1['page'][0]) )	{
		$pageListArray = $pageListArray1['page'];  
       
		for($i=0;$i < sizeof($pageListArray);$i++)
		{
			if($city!='' && $advanceSearch==''){    
					if($city==@$pageListArray[$i]['city']['value'] ){                                  
				@$dataArr[$m]['hospitalId'] 		  = trim($pageListArray[$i]['hospitalId']['value']);
				@$dataArr[$m]['hospitalName'] 	  = @$pageListArray[$i]['hospitalName']['value'];
				@$dataArr[$m]['hospitalCode'] 	  = @$pageListArray[$i]['hospitalCode']['value'];
				@$dataArr[$m]['address1'] 		  = @$pageListArray[$i]['address1']['value'];
				@$dataArr[$m]['address2'] 		  = @$pageListArray[$i]['address2']['value'];
				@$dataArr[$m]['state'] 			  = @$pageListArray[$i]['state']['value'];
				@$dataArr[$m]['city'] 			  = @$pageListArray[$i]['city']['value'];
				@$dataArr[$m]['cityTitle'] 			  = @$pageListArray[$i]['cityTitle']['value'];
				@$dataArr[$m]['location'] 		  = @$pageListArray[$i]['location']['value'];
				@$dataArr[$m]['pincode'] 		  = @$pageListArray[$i]['pincode']['value'];
				@$dataArr[$m]['phone'] 			  = @$pageListArray[$i]['phone']['value'];
                $dataArr[$m]['fax']               = @$pageListArray[$i]['fax']['value'];
				$dataArr[$m]['hospitalType'] 	  = @$pageListArray[$i]['hospitalType']['value'];
				$dataArr[$m]['email'] 			  = @$pageListArray[$i]['email']['value'];
				$dataArr[$m]['website'] 		  = @$pageListArray[$i]['website']['value'];
				$dataArr[$m]['longitude'] 		  = @$pageListArray[$i]['longitude']['value'];
				$dataArr[$m]['lattitude'] 		  = @$pageListArray[$i]['lattitude']['value'];
				$dataArr[$m]['hospitalType'] 	  = @$pageListArray[$i]['hospitalType']['value'];
				$dataArr[$m]['isDeleted']         = trim(@$pageListArray[$i]['isDeleted']['value']);
				$m++;        
				}
			}else if($city!='' && $advanceSearch!=''){ 
                 $orignal = trim(strtolower(base64_decode($pageListArray[$i]['hospitalName']['value']))) ;
                 $countText  = strlen($advanceSearch);
                 $match = substr($orignal,0,$countText);					 		 
				 $str = explode(" ",$orignal);
				 $match2 = substr($str[1],0,$countText);
				 if(count(explode(" ",$orignal))>3)
				 $match3 = substr($str[2],0,$countText);
				 if(count(explode(" ",$orignal))>4)
				 $match4 = substr($str[3],0,$countText);
				   $location = trim(strtolower(base64_decode($pageListArray[$i]['location']['value']))) ;
				   $match1 = substr($location,0,$countText);        
                  if($city==@$pageListArray[$i]['city']['value'] && (($match==strtolower($advanceSearch)) || ($match2==strtolower($advanceSearch)) || ($match1==strtolower($advanceSearch)) || (count(explode(" ",$orignal))>3 && ($match3==strtolower($advanceSearch))) || (count(explode(" ",$orignal))>4 && ($match4==strtolower($advanceSearch))))) {
                              
				$dataArr[$m]['hospitalId'] 		  = trim($pageListArray[$i]['hospitalId']['value']);
				$dataArr[$m]['hospitalName'] 	  = @$pageListArray[$i]['hospitalName']['value'];
				$dataArr[$m]['hospitalCode'] 	  = @$pageListArray[$i]['hospitalCode']['value'];
				$dataArr[$m]['address1'] 		  = @$pageListArray[$i]['address1']['value'];
				$dataArr[$m]['address2'] 		  = @$pageListArray[$i]['address2']['value'];
				$dataArr[$m]['state'] 			  = @$pageListArray[$i]['state']['value'];
				$dataArr[$m]['city'] 			  = @$pageListArray[$i]['city']['value'];
				@$dataArr[$m]['cityTitle'] 			  = @$pageListArray[$i]['cityTitle']['value'];
				$dataArr[$m]['location'] 		  = @$pageListArray[$i]['location']['value'];
				$dataArr[$m]['pincode'] 		  = @$pageListArray[$i]['pincode']['value'];
				$dataArr[$m]['phone'] 			  = @$pageListArray[$i]['phone']['value'];
				$dataArr[$m]['fax'] 			  = @$pageListArray[$i]['fax']['value'];
                $dataArr[$m]['hospitalType']      = @$pageListArray[$i]['hospitalType']['value']; 
				$dataArr[$m]['email'] 			  = @$pageListArray[$i]['email']['value'];
				$dataArr[$m]['website'] 		  = @$pageListArray[$i]['website']['value'];
				$dataArr[$m]['longitude'] 		  = @$pageListArray[$i]['longitude']['value'];
				$dataArr[$m]['lattitude'] 		  = @$pageListArray[$i]['lattitude']['value'];
				$dataArr[$m]['hospitalType'] 	  = @$pageListArray[$i]['hospitalType']['value'];
				$dataArr[$m]['match'] 	  = @$match;
				$dataArr[$m]['isDeleted']         = trim(@$pageListArray[$i]['isDeleted']['value']);
                $m++;
			} }
		}
	}
//sort by
	/*if (count (@$dataArr) > 0) {
    foreach ($dataArr as $key => $row) {
        @$order_in_category[$key]  = $row[0];//[0] is @$order_in_category
    }
    	array_multisort(@$order_in_category, SORT_ASC, $dataArr);
	}*/
return @$dataArr;
}
function states_page_array($pageListArray1){
	$m =0;
	$dataArr=array();
	if(is_array(@$pageListArray1['page'][0]) )	{
		$pageListArray = $pageListArray1['page'];
		for($i=0;$i <sizeof($pageListArray);$i++)
		{
			if(trim($pageListArray[$i]['isDeleted']['value']) =='no'){
				$dataArr[$m]['stateId'] 		  		= @$pageListArray[$i]['stateId']['value'];
				$dataArr[$m]['stateTitle'] 	 		 = @$pageListArray[$i]['stateTitle']['value'];
				$dataArr[$m]['isDeleted']         = trim(@$pageListArray[$i]['isDeleted']['value']);
				$m++;
			}
		}
	}else	{
			$pageListArray = @$pageListArray1['page'];	
			if(trim($pageListArray['isDeleted']['value']) =='no') 	{
					$dataArr[$m]['stateId'] 			= $pageListArray['stateId']['value'];
					$dataArr[$m]['stateTitle'] 		= @$pageListArray['stateTitle']['value'];
					$dataArr[$m]['isDeleted']		 = trim(@$pageListArray['isDeleted']['value']);			
					$m++;
				}
	}
//sort by
	/*if (count ($dataArr) > 0) {
    foreach ($dataArr as $key => $row) {
        @$order_in_category[$key]  = $row[0];//[0] is @$order_in_category
    }
    	@array_multisort(@$order_in_category, SORT_ASC, $dataArr);
	}*/
return $dataArr;
}
function hospital_particular_details($pageListArray1,$id){
	$dataArr=array();
	$m =0; 
	if(is_array(@$pageListArray1['page'][0]) )	{
		$pageListArray = $pageListArray1['page'];  
       
		for($i=0;$i < sizeof($pageListArray);$i++)
		{
			if(@$id==trim(@$pageListArray[$i]['hospitalId']['value'])){                          
				@$dataArr[$m]['hospitalId'] 		  = trim($pageListArray[$i]['hospitalId']['value']);
				@$dataArr[$m]['hospitalName'] 	  = @$pageListArray[$i]['hospitalName']['value'];
				@$dataArr[$m]['hospitalCode'] 	  = @$pageListArray[$i]['hospitalCode']['value'];
				@$dataArr[$m]['address1'] 		  = @$pageListArray[$i]['address1']['value'];
				@$dataArr[$m]['address2'] 		  = @$pageListArray[$i]['address2']['value'];
				@$dataArr[$m]['state'] 			  = @$pageListArray[$i]['state']['value'];
				@$dataArr[$m]['city'] 			  = @$pageListArray[$i]['city']['value'];
				@$dataArr[$m]['cityTitle'] 			  = @$pageListArray[$i]['cityTitle']['value'];
				@$dataArr[$m]['location'] 		  = @$pageListArray[$i]['location']['value'];
				@$dataArr[$m]['pincode'] 		  = @$pageListArray[$i]['pincode']['value'];
				@$dataArr[$m]['phone'] 			  = @$pageListArray[$i]['phone']['value'];
                $dataArr[$m]['fax']               = @$pageListArray[$i]['fax']['value'];
				$dataArr[$m]['hospitalType'] 	  = @$pageListArray[$i]['hospitalType']['value'];
				$dataArr[$m]['email'] 			  = @$pageListArray[$i]['email']['value'];
				$dataArr[$m]['website'] 		  = @$pageListArray[$i]['website']['value'];
				$dataArr[$m]['longitude'] 		  = @$pageListArray[$i]['longitude']['value'];
				$dataArr[$m]['lattitude'] 		  = @$pageListArray[$i]['lattitude']['value'];
				$dataArr[$m]['hospitalType'] 	  = @$pageListArray[$i]['hospitalType']['value'];
				$dataArr[$m]['isDeleted']         = trim(@$pageListArray[$i]['isDeleted']['value']);
				$m++; 
				break;       
				}
			 }
		}
return @$dataArr;
}
function download_page_array($pageListArray1){
	$m =0;
	$dataArr=array();
	if(is_array(@$pageListArray1['download'][0]) )	{
		$pageListArray = $pageListArray1['download'];
		for($i=0;$i <sizeof($pageListArray);$i++)
		{
			if(trim($pageListArray[$i]['isDeleted']['value']) =='no'){
					$dataArr[$m]['downloadId'] 	  	  = $pageListArray[$i]['downloadId']['value'];
					$dataArr[$m]['downloadTitle']	  = @$pageListArray[$i]['downloadTitle']['value'];
					$dataArr[$m]['fileName']	  		 = @$pageListArray[$i]['fileName']['value'];
					$dataArr[$m]['fileType']	  		 = @$pageListArray[$i]['fileType']['value'];
					$dataArr[$m]['isDeleted']        = trim($pageListArray[$i]['isDeleted']['value']);
					$m++;
			}
		}
	}else	{
			$pageListArray = @$pageListArray1['download'];	
			if(trim($pageListArray['isDeleted']['value']) =='no') 	{
					$dataArr[$m]['downloadId'] 		 = $pageListArray['downloadId']['value'];
					$dataArr[$m]['downloadTitle']	 = $pageListArray['downloadTitle']['value'];
					$dataArr[$m]['fileName']	  		= @$pageListArray['fileName']['value'];
					$dataArr[$m]['fileType']	  		= @$pageListArray['fileType']['value'];
					$dataArr[$m]['isDeleted']		 = trim($pageListArray[$i]['isDeleted']['value']);			
					$m++;
				}
	}
//sort by
	if (count ($dataArr) > 0) {
    foreach ($dataArr as $key => $row) {
        @$order_in_category[$key]  = $row[0];//[0] is @$order_in_category
    }
    	array_multisort(@$order_in_category, SORT_ASC, $dataArr);
	}
return $dataArr;
}
function tools_page_array($pageListArray1){
	$m =0;
	$dataArr=array();
	if(is_array(@$pageListArray1['tools'][0]) )	{
		$pageListArray = $pageListArray1['tools'];
		for($i=0;$i <sizeof($pageListArray);$i++)
		{
			if(trim($pageListArray[$i]['isDeleted']['value']) =='no'){
					$dataArr[$m]['toolsId'] 	  	  = $pageListArray[$i]['toolsId']['value'];
					$dataArr[$m]['downloadTitle']	  = @$pageListArray[$i]['downloadTitle']['value'];
					$dataArr[$m]['fileName']	  		 = @$pageListArray[$i]['fileName']['value'];
					$dataArr[$m]['fileType']	  		 = @$pageListArray[$i]['fileType']['value'];
					$dataArr[$m]['isDeleted']        = trim($pageListArray[$i]['isDeleted']['value']);
					$m++;
			}
		}
	}else	{
			$pageListArray = @$pageListArray1['tools'];	
			if(trim($pageListArray['isDeleted']['value']) =='no') 	{
					$dataArr[$m]['toolsId'] 		 = $pageListArray['toolsId']['value'];
					$dataArr[$m]['downloadTitle']	 = $pageListArray['downloadTitle']['value'];
					$dataArr[$m]['fileName']	  		= @$pageListArray['fileName']['value'];
					$dataArr[$m]['fileType']	  		= @$pageListArray['fileType']['value'];
					$dataArr[$m]['isDeleted']		 = trim($pageListArray[$i]['isDeleted']['value']);			
					$m++;
				}
	}
//sort by
	if (count ($dataArr) > 0) {
    foreach ($dataArr as $key => $row) {
        @$order_in_category[$key]  = $row[0];//[0] is @$order_in_category
    }
    	array_multisort(@$order_in_category, SORT_ASC, $dataArr);
	}
return $dataArr;
}

function claim_forms_page_array($pageListArray1){
	$m =0;
	$dataArr=array();
	if(is_array(@$pageListArray1['claimforms'][0]) )	{
		$pageListArray = $pageListArray1['claimforms'];
		for($i=0;$i <sizeof($pageListArray);$i++)
		{
			if(trim($pageListArray[$i]['isDeleted']['value']) =='no'){
					$dataArr[$m]['claimformsId'] 	  	  = $pageListArray[$i]['claimformsId']['value'];
					$dataArr[$m]['downloadTitle']	  = @$pageListArray[$i]['downloadTitle']['value'];
					$dataArr[$m]['fileName']	  		 = @$pageListArray[$i]['fileName']['value'];
					$dataArr[$m]['fileType']	  		 = @$pageListArray[$i]['fileType']['value'];
					$dataArr[$m]['isDeleted']        = trim($pageListArray[$i]['isDeleted']['value']);
					$m++;
			}
		}
	}else	{
			$pageListArray = @$pageListArray1['claimforms'];	
			if(trim($pageListArray['isDeleted']['value']) =='no') 	{
					$dataArr[$m]['claimformsId'] 		 = $pageListArray['claimformsId']['value'];
					$dataArr[$m]['downloadTitle']	 = $pageListArray['downloadTitle']['value'];
					$dataArr[$m]['fileName']	  		= @$pageListArray['fileName']['value'];
					$dataArr[$m]['fileType']	  		= @$pageListArray['fileType']['value'];
					$dataArr[$m]['isDeleted']		 = trim($pageListArray[$i]['isDeleted']['value']);			
					$m++;
				}
	}
//sort by
	if (count ($dataArr) > 0) {
    foreach ($dataArr as $key => $row) {
        @$order_in_category[$key]  = $row[0];//[0] is @$order_in_category
    }
    	array_multisort(@$order_in_category, SORT_ASC, $dataArr);
	}
return $dataArr;
}

function fetchByManyIds($column,$tblname,$cond){
	global $conn;
	$dataArray=array();
		$query="SELECT $column FROM $tblname $cond";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i] = strtolower($value);				
			}
			$i++;
		}
	return $dataArray;
}	
function in_array_multidimensional($needle, $haystack, $strict = false) {
    foreach ($haystack as $item) {
        if (($strict ? $item === $needle : $item == $needle) || (is_array($item) && in_array_multidimensional($needle, $item, $strict))) {
            return true;
        }
    }

    return false;
}
function find ($string, $array = array ())
{       
    foreach ($array as $key => $value) {
        unset ($array[$key]);
        if (strpos($value, $string) !== false) {
            $array[$key] = $value;
        }
    }       
    return $array;
}
function get_static_details($staticId){
	global $conn;
	$dataArray=array();
	$query="SELECT * FROM CRMSTATICPAGES where STATUS='ACTIVE' AND  STATICID='".@$staticId."'";
	$sql = @oci_parse($conn, $query);
	// Execute Query
	@oci_execute($sql);
	$i=0;
	while (($row = @oci_fetch_assoc($sql))) {
		foreach($row as $key=>$value){
			$dataArray[$i][$key] = $value;				
		}
		$i++;
	}
	return $dataArray;
}
function getStaticPages(){
	global $conn;
	$dataArray=array();
		$query="SELECT * FROM CRMSTATICPAGES where STATUS='ACTIVE'";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function SendMail($email,$from,$subject,$message,$type){
 
 		if($type=='HR'){
		$to = 'customerfirst@religarehealthinsurance.com';  
		$Mailtemplates=GetTemplateContentByTemplateName('APPOINTMENTREQUEST_HR');
		$from=stripslashes($Mailtemplates[0]['FROMEMAIL']);
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
	    $message  = stripslashes($Mailtemplates[0]['CONTENT']);
	   	$headers  = 'From:'.stripslashes($Mailtemplates[0]['FROMEMAIL'])."\r\n" .
				'Cc:  wellness@religare.com' . "\r\n" .
			    'MIME-Version: 1.0' . "\r\n" .
				'Content-type: text/html; charset=iso-8859-1' . "\r\n";		
		}
		if($type=='EmployeeBuy'){
		$to = $email;  
		$Mailtemplates=GetTemplateContentByTemplateName('EMPLOYEE_PURCHASED_PLAN');
		$from=stripslashes($Mailtemplates[0]['FROMEMAIL']);
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
	    $message  = stripslashes($Mailtemplates[0]['CONTENT']);
	   	$headers  = 'From:'.stripslashes($Mailtemplates[0]['FROMEMAIL'])."\r\n" .
					'Cc:  wellness@religare.com' . "\r\n" .
			    'MIME-Version: 1.0' . "\r\n" .
				'Content-type: text/html; charset=iso-8859-1' . "\r\n";	
		}
		if($type=='HRBY'){
		$to = $email;  
		$Mailtemplates=GetTemplateContentByTemplateName('PLANPURCHASED_HR');
		$from=stripslashes($Mailtemplates[0]['FROMEMAIL']);
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
	    $message  = stripslashes($Mailtemplates[0]['CONTENT']);
	   	$headers  = 'From:'.stripslashes($Mailtemplates[0]['FROMEMAIL'])."\r\n" .
					'Cc:  wellness@religare.com' . "\r\n" .
			    'MIME-Version: 1.0' . "\r\n" .
				'Content-type: text/html; charset=iso-8859-1' . "\r\n";	
		}
		if($type!='HR'){
		if(mail($to, $subject, $message, $headers)){
		 $to=addslashes($to);
				 $subject=addslashes($subject);
				 $message=addslashes($message);
				 	$sql="INSERT INTO CRMSENTMAIL (MAILID,MAILTO,SUBJECT,MESSAGE,MAILDATE,TYPE) values(CRMSENTMAIL_SEQ.nextval,'".@$to."','".@$subject."','".@$message."','".@$entryTime."','".@$type."') ";
				$stdid = @oci_parse($conn, $sql);
				$r = @oci_execute($stdid);
		 return "Sent SuccessFully";
		}
		}

}
function SendMailRetail($email,$from,$subject,$message,$type){
 
 		if($type=='HR'){
		$to = 'customerfirst@religarehealthinsurance.com';  
		$Mailtemplates=GetTemplateContentByTemplateName('APPOINTMENTREQUEST_HR_RETAIL');
		$from=stripslashes($Mailtemplates[0]['FROMEMAIL']);
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
	    $message  = $Mailtemplates[0]['LARGECONTENT']?stripslashes($Mailtemplates[0]['LARGECONTENT']->load()):stripslashes($Mailtemplates[0]['CONTENT']);
	   	$headers  = 'From:'.stripslashes($Mailtemplates[0]['FROMEMAIL'])."\r\n" .
				'Cc:  healthcheckup@religare.com' . "\r\n" .
			    'MIME-Version: 1.0' . "\r\n" .
				'Content-type: text/html; charset=iso-8859-1' . "\r\n";		
		}
		if($type=='EmployeeBuy'){
		$to = $email;  
		$Mailtemplates=GetTemplateContentByTemplateName('EMPLOYEE_PURCHASED_PLAN');
		$from=stripslashes($Mailtemplates[0]['FROMEMAIL']);
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
	    $message  = stripslashes($Mailtemplates[0]['CONTENT']);
	   	$headers  = 'From:'.stripslashes($Mailtemplates[0]['FROMEMAIL'])."\r\n" .
					'Cc:  wellness@religare.com' . "\r\n" .
			    'MIME-Version: 1.0' . "\r\n" .
				'Content-type: text/html; charset=iso-8859-1' . "\r\n";	
		}
		if($type=='HRBY'){
		$to = $email;  
		$Mailtemplates=GetTemplateContentByTemplateName('PLANPURCHASED_HR');
		$from=stripslashes($Mailtemplates[0]['FROMEMAIL']);
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
	    $message  = stripslashes($Mailtemplates[0]['CONTENT']);
	   	$headers  = 'From:'.stripslashes($Mailtemplates[0]['FROMEMAIL'])."\r\n" .
					'Cc:  wellness@religare.com' . "\r\n" .
			    'MIME-Version: 1.0' . "\r\n" .
				'Content-type: text/html; charset=iso-8859-1' . "\r\n";	
		}
		if($type!='HR'){
		if(mail($to, $subject, $message, $headers)){
		 $to=addslashes($to);
				 $subject=addslashes($subject);
				 $message=addslashes($message);
				 	$sql="INSERT INTO CRMSENTMAIL (MAILID,MAILTO,SUBJECT,MESSAGE,MAILDATE,TYPE) values(CRMSENTMAIL_SEQ.nextval,'".@$to."','".@$subject."','".@$message."','".@$entryTime."','".@$type."') ";
				$stdid = @oci_parse($conn, $sql);
				$r = @oci_execute($stdid);
		 return "Sent SuccessFully";
		}
		}

}
function sendMailToempForPurchasePlan($email){
		$_SESSION['PURCHASEPLANIDNEW'] = $_SESSION['PURCHASEPLANIDNEW']?$_SESSION['PURCHASEPLANIDNEW']:$_SESSION['PURCHASEPLANID'];
		
		$plandetail = fetchPlanwithPolicyDetails(@$_SESSION['PURCHASEPLANIDNEW']);
		
		//$empUrl = "https://uattractus.religarehealthinsurance.com/employee_interface/company-".@$_COOKIE['company'];
		global $conn;
		global $empUrl;
		$todayDate = date('d-M-Y',time());
		$todayDate = strtoupper($todayDate);
		$certificatefilenane = create80DCertificate($plandetail);
		
		$Mailtemplates=GetTemplateContentByTemplateNameForConfirm('EMP_PURCHASE_PLAN_MAIL');
	
		$from=stripslashes($Mailtemplates[0]['FROMEMAIL']);
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
	    $bodytext  = stripslashes($Mailtemplates[0]['CONTENT']);
	    $bodytext  = str_replace("##Name",@$plandetail[0]['EMPNAME'],$bodytext);	
		$bodytext  = str_replace("##healthcheckupplanname",@$plandetail[0]['PLANNAME'],$bodytext);	
		$bodytext  = str_replace("##emploginLink",@$empUrl,$bodytext);
		$bodytext  = str_replace("##today ",@$todayDate,$bodytext);
        $bodytext  = str_replace("##policyEndDate ",@$plandetail[0]['POLICYENDDATE'],$bodytext);	
		/*$upload_temp='template/80DCertificate.docx';
		$upload_size=filesize('template/80DCertificate.docx');
		$upload_name='80DCertificate.docx';*/
		
		/*$upload_temp='template/'.@$certificatefilenane.'';
		$upload_size=filesize('template/'.@$certificatefilenane.'');
		$upload_name=@$certificatefilenane;*/





/////simple mail with attactch
		$uploadfilename = @$certificatefilenane;
		$upload_temp='template/'.@$certificatefilenane;
		
		

		//$file_to_attach = $_SERVER["DOCUMENT_ROOT"] . 'siteupload/doc/' . $fileName;
		$file_to_attach = $upload_temp;
		$to = $email; //Recipient Email Address
		 //Email Subject
		//$headers = "From: customerfirst@religarehealthinsurance.com\r\nReply-To: customerfirst@religarehealthinsurance.com\r\nCc:wellness@religare.com";
		$headers = "From: ".$from."\r\nReply-To: ".$from."\r\nCc:wellness@religare.com";
		//$headers = "rhiclportaladmin@religarehealthinsurance.com";
		$random_hash = md5(date('r', time()));
		$headers .= "\r\nContent-Type: multipart/mixed; boundary=\"PHP-mixed-".$random_hash."\"";
		$attachment = chunk_split(base64_encode(file_get_contents($file_to_attach))); // Set your file path here
		$message = "--PHP-mixed-$random_hash\r\n"."Content-Type: multipart/alternative; boundary=\"PHP-alt-$random_hash\"\r\n\r\n";
		$message .= "--PHP-alt-$random_hash\r\n"."Content-Type: text/html; charset=\"iso-8859-1\"\r\n"."Content-Transfer-Encoding: 7bit\r\n\r\n";
		$message .= $bodytext;
		$message .="\r\n\r\n--PHP-alt-$random_hash--\r\n\r\n";
		$message .= "--PHP-mixed-$random_hash\r\n"."Content-Type: application/zip; name=\"$uploadfilename\"\r\n"."Content-Transfer-Encoding: base64\r\n"."Content-Disposition: attachment\r\n\r\n";
		$message .= $attachment;
		$message .= "/r/n--PHP-mixed-$random_hash--";
		if($mail = mail( $to, $subject , $message, $headers ))
			{
				$reasontomail = 'CORP_EMP_PURCHASE_PLAN_MAIL';
				$sendby = '';
				$entryTime=date('d-M-Y');
				$status = '';
				$sendfrom = WELLNESSEMAILID;
				$ipAddress = get_client_ip();
				$userType2 = '';
				//query to insert records FOR EMAIL LOG
				$sqlLog="INSERT INTO PPHCMAILLOG (MAILID,MAILTO,SUBJECT,MESSAGE,USERTYPE,REASONTOMAIL,SENDBY,SENDDATE,STATUS,SENDFROM,IPADDRESS,CASEID) values(PPHCMAILLOG_SEQ.nextval,q'[".$to."]','".@$subject."','".@$message."','".@$userType2."','".@$reasontomail."','".@$sendby."','".@$entryTime."','".@$status."','".@$sendfrom."','".@$ipAddress."','".@$caseid."') ";		
				$sqlLogExe = @oci_parse($conn, $sqlLog);
				$r = @oci_execute($sqlLogExe);	
			}
		
////// simple mail with attacht
		





		/*$fp = fopen($upload_temp, "rb");
		$file = fread($fp, $upload_size);
		
		$file = chunk_split(base64_encode($file));
		$num = md5(time());
		
		//Normal headers

		$headers  = "From: ".@$from."\r\n";
		$headers  .= "Cc:  wellness@religare.com"."\r\n";
		$headers  .= "MIME-Version: 1.0\r\n";
		$headers  .= "Content-Type: multipart/mixed; ";
		$headers  .= "boundary=".$num."\r\n";
		$headers  .= "--$num\r\n";
		
		// This two steps to help avoid spam
		
		$headers .= "Message-ID: <".gettimeofday()." TheSystem@".$_SERVER['SERVER_NAME'].">\r\n";
		$headers .= "X-Mailer: PHP v".phpversion()."\r\n";
		
		// With message
		
		$headers .= "Content-Type: text/html; charset=iso-8859-1\r\n";
		$headers .= "Content-Transfer-Encoding: 8bit\r\n";
		$headers .= "".$message."\n";
		$headers .= "--".$num."\n";
		
		// Attachment headers
		
		$headers  .= "Content-Type:".$upload_type." ";
		$headers  .= "name=\"".$upload_name."\"r\n";
		$headers  .= "Content-Transfer-Encoding: base64\r\n";
		$headers  .= "Content-Disposition: attachment; ";
		$headers  .= "filename=\"".$upload_name."\"\r\n\n";
		$headers  .= "".$file."\r\n";
		$headers  .= "--".$num."--";
		$to=@$email;
		if(mail($to, $subject, $message, $headers)) {
		//echo "sent";
		} else {
		//echo "not sent";
		}
		fclose($fp);	*/		
}
function fetchPlanwithPolicyDetails($purchasePlanId){
	global $conn;
	$dataArray=array();
		 $query="SELECT CRMEMPLOYEEPURCHASEPLAN.PURCHASEPLANID,CRMEMPLOYEEPURCHASEPLAN.CLIENTID,CRMEMPLOYEEPURCHASEPLAN.PURCHASEDATE,CRMEMPLOYEEPURCHASEPLAN.EMPNAME,CRMEMPLOYEEPURCHASEPLAN.PLANNAME,CRMEMPLOYEEPURCHASEPLAN.POLICYNUMBER,CRMEMPLOYEEPURCHASEPLAN.TOTALAMOUNT,CRMEMPLOYEEPURCHASEPLAN.ADDRESS,CRMPOLICY.POLICYSTARTDATE,CRMPOLICY.POLICYENDDATE,CRMCOMPANY.COMPANYNAME FROM CRMEMPLOYEEPURCHASEPLAN left join CRMPOLICY on CRMPOLICY.POLICYNUMBER = CRMEMPLOYEEPURCHASEPLAN.POLICYNUMBER left join CRMCOMPANY on  CRMEMPLOYEEPURCHASEPLAN.COMPANYID =CRMCOMPANY.COMPANYID WHERE CRMEMPLOYEEPURCHASEPLAN.PURCHASEPLANID=$purchasePlanId";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}	
function create80DCertificate($plandetailpre){

			require_once 'PHPWord.php';

			$PHPWord = new PHPWord();
			$section = $PHPWord->createSection();
			$_SESSION['PURCHASEPLANIDNEW'] = $_SESSION['PURCHASEPLANIDNEW']?$_SESSION['PURCHASEPLANIDNEW']:1;
			$plandetail = fetchPlanwithPolicyDetails(@$_SESSION['PURCHASEPLANIDNEW']);
		
			$document = $PHPWord->loadTemplate('PHPWord/Invoice_Template.docx');
			$document->setValue('Value1', @$plandetail[0]['POLICYNUMBER']);
			$document->setValue('Value2', @$plandetail[0]['CLIENTID']);
			$document->setValue('Value3', @$plandetail[0]['EMPNAME']);
			$document->setValue('Value4', @$plandetail[0]['COMPANYNAME']);
			$document->setValue('Value5', @$plandetail[0]['POLICYSTARTDATE']);
			$document->setValue('Value6', @$plandetail[0]['POLICYENDDATE']);
			$document->setValue('Value7', @$plandetail[0]['TOTALAMOUNT']);
			$document->setValue('Value8', @$plandetail[0]['TOTALAMOUNT']);
			$document->setValue('Value9', @$plandetail[0]['TOTALAMOUNT']);
			$document->setValue('Value10',@$plandetail[0]['EMPNAME']);
			$purchasetime = strtotime($plandetail[0]['PURCHASEDATE']);
			$purchasedate = date('d-M-Y',$purchasetime);
			$document->setValue('Value11',$purchasedate);
			/*$section = $PHPWord->createSection();
			$section->addText('80D Certificate!');
			$section->addTextBreak(2);
			
			$document->setValue('weekday', date('l'));
			$document->setValue('time', date('H:i'));
			//$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');
			$document->save('template/80DCertificate.docx');*/
			
			include_once("invoiceReport.php");
			
			return $certificatefilenane;
			
}
function GetTemplateContentByTemplateNameForConfirm($templateName){
	global $conn;
	$dataArray=array();
	    $query="SELECT TEMPLATEID,SUBJECT,CONTENT,FROMEMAIL,FROMNAME,TEMPLATENAME from CRMEMAILTEMPLTE where TEMPLATENAME='".$templateName."' ";
		
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
function Sendmailwithattatchment($email,$time,$AmPm,$date,$center,$tempName,$name,$centerPhone,$outputfile){

		$Mailtemplates=GetTemplateContentByTemplateNameForConfirm($tempName);
		$from=stripslashes($Mailtemplates[0]['FROMEMAIL']);
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
	    $bodytext  = stripslashes($Mailtemplates[0]['CONTENT']);
	    $bodytext  = str_replace("##FIRSTNAME",@$name,$bodytext);	
		$bodytext  = str_replace("##TIMESLOT",@$time,$bodytext);	
		$bodytext  = str_replace("##AMPM",@$AmPm,$bodytext);
		$bodytext  = str_replace("##DATE",@$date,$bodytext);
        $bodytext  = str_replace("##CENTERNAME",@$center,$bodytext);	
        $bodytext  = str_replace("##phoneno",@$centerPhone,$bodytext);
		$uploadfilename = @$outputfile.'.pdf';
		$upload_temp='template/'.@$uploadfilename;
		
		

		//$file_to_attach = $_SERVER["DOCUMENT_ROOT"] . 'siteupload/doc/' . $fileName;
		$file_to_attach = $upload_temp;
		$to = $email; //Recipient Email Address
		 //Email Subject
		//$headers = "From: customerfirst@religarehealthinsurance.com\r\nReply-To: customerfirst@religarehealthinsurance.com\r\nCc:wellness@religare.com";
		$headers = "From: ".$from."\r\nReply-To: ".$from."\r\nCc:wellness@religare.com";
		//$headers = "rhiclportaladmin@religarehealthinsurance.com";
		$random_hash = md5(date('r', time()));
		$headers .= "\r\nContent-Type: multipart/mixed; boundary=\"PHP-mixed-".$random_hash."\"";
		$attachment = chunk_split(base64_encode(file_get_contents($file_to_attach))); // Set your file path here
		$message = "--PHP-mixed-$random_hash\r\n"."Content-Type: multipart/alternative; boundary=\"PHP-alt-$random_hash\"\r\n\r\n";
		$message .= "--PHP-alt-$random_hash\r\n"."Content-Type: text/html; charset=\"iso-8859-1\"\r\n"."Content-Transfer-Encoding: 7bit\r\n\r\n";
		$message .= $bodytext;
		$message .="\r\n\r\n--PHP-alt-$random_hash--\r\n\r\n";
		$message .= "--PHP-mixed-$random_hash\r\n"."Content-Type: application/zip; name=\"$uploadfilename\"\r\n"."Content-Transfer-Encoding: base64\r\n"."Content-Disposition: attachment\r\n\r\n";
		$message .= $attachment;
		$message .= "/r/n--PHP-mixed-$random_hash--";
		$mail = mail( $to, $subject , $message, $headers );
		 $mail ? "" : "";
				
}
function fetchPolicyDetailsWithAgent($policyNumber){
	global $conn;
	$dataArray=array();
	if($policyNumber!=''){
		$query="SELECT CRMPOLICY.POLICYID,CRMPOLICY.POLICYNUMBER,CRMAGENT.AGENTID,CRMAGENT.AGENTCODE FROM CRMPOLICY LEFT JOIN CRMAGENT ON CRMPOLICY.AGENTID=CRMAGENT.AGENTID where CRMPOLICY.POLICYNUMBER='".$policyNumber."' AND CRMPOLICY.STATUS='ACTIVE' AND CRMAGENT.STATUS='ACTIVE' ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function goodnesswallpapers_page_array($pageListArray1){
	$m =0;
	$dataArr=array();
	if(is_array(@$pageListArray1['goodnesswallpapers'][0]) )	{
		$pageListArray = $pageListArray1['goodnesswallpapers'];
		for($i=0;$i <sizeof($pageListArray);$i++)
		{
			if(trim($pageListArray[$i]['isDeleted']['value']) =='no'){
					$dataArr[$m]['goodnesswallpapersId'] 	  	  = $pageListArray[$i]['goodnesswallpapersId']['value'];
					$dataArr[$m]['downloadTitle']	  = @$pageListArray[$i]['downloadTitle']['value'];
					$dataArr[$m]['fileName']	  		 = @$pageListArray[$i]['fileName']['value'];
					$dataArr[$m]['fileType']	  		 = @$pageListArray[$i]['fileType']['value'];
					$dataArr[$m]['isDeleted']        = trim($pageListArray[$i]['isDeleted']['value']);
					$m++;
			}
		}
	}else	{
			$pageListArray = @$pageListArray1['goodnesswallpapers'];	
			if(trim($pageListArray['isDeleted']['value']) =='no') 	{
					$dataArr[$m]['goodnesswallpapersId'] 		 = $pageListArray['goodnesswallpapersId']['value'];
					$dataArr[$m]['downloadTitle']	 = $pageListArray['downloadTitle']['value'];
					$dataArr[$m]['fileName']	  		= @$pageListArray['fileName']['value'];
					$dataArr[$m]['fileType']	  		= @$pageListArray['fileType']['value'];
					$dataArr[$m]['isDeleted']		 = trim($pageListArray[$i]['isDeleted']['value']);			
					$m++;
				}
	}
//sort by
	if (count ($dataArr) > 0) {
    foreach ($dataArr as $key => $row) {
        @$order_in_category[$key]  = $row[0];//[0] is @$order_in_category
    }
    	array_multisort(@$order_in_category, SORT_ASC, $dataArr);
	}
return $dataArr;
}
 
//function fetchcolumnListCond($col,$table,$cond){
//	global $conn;
//	$dataArray=array();
//		 $query="SELECT $col FROM $table $cond";
//				$sql = @oci_parse($conn, $query);
//		// Execute Query
//		@oci_execute($sql);
//		$i=0;
//		while ((@$row = oci_fetch_assoc($sql))) {
//			foreach($row as $key=>$value){
//				$dataArray[$i][$key] = $value;				
//			}
//			$i++;
//		}
//	return $dataArray;
//}
 
//this function is used for get state where center exists in employee portal by amit dubey on 25 august 2015
function GetStateWhereCenterExist($compId,$policyId,$planId){
	global $conn;
	$sqlresult=array();
		$query=" SELECT LWSTATEMASTER.STATEID,LWSTATEMASTER.STATENAME FROM LWSTATEMASTER WHERE LWSTATEMASTER.STATUS ='ACTIVE' AND LWSTATEMASTER.STATEID IN ( SELECT LWCENTERMASTER.STATEID from LWCENTERMASTER JOIN CRMCOMPANYCENTERMAPPING  ON  CRMCOMPANYCENTERMAPPING.CENTERID=LWCENTERMASTER.CENTERID WHERE LWCENTERMASTER.STATUS='ACTIVE' AND CRMCOMPANYCENTERMAPPING.POLICYID='".@$policyId."' AND CRMCOMPANYCENTERMAPPING.PLANID='".@$planId."' AND CRMCOMPANYCENTERMAPPING.COMPANYID='".@$compId."') ";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i = 0;
		while (($row = oci_fetch_assoc($sql))) {
				$dataArray[] = $row;
			$i++;
		}
		return $dataArray;
}

//this function is used for get corporate city where center exists in crmadmin by amit dubey on 30 november 2015 at 06:41 PM
function GetCityWhereCenterExist($planId,$stateId){
	global $conn;
	$dataArray=array();
		 $query=" SELECT LWCITYMASTER.CITYID,LWCITYMASTER.CITYNAME FROM LWCITYMASTER WHERE LWCITYMASTER.STATUS ='ACTIVE' AND LWCITYMASTER.CITYID IN ( SELECT DISTINCT LWCENTERMASTER.CITYID from LWCENTERMASTER JOIN CRMCOMPANYCENTERMAPPING  ON  CRMCOMPANYCENTERMAPPING.CENTERID=LWCENTERMASTER.CENTERID WHERE LWCENTERMASTER.STATUS='ACTIVE' AND CRMCOMPANYCENTERMAPPING.PLANID='".@$planId."'  AND LWCENTERMASTER.STATEID='".@$stateId."') ";

		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i = 0;
		while (($row = oci_fetch_assoc($sql))) {
				$dataArray[] = $row;
			$i++;
		}
		return $dataArray;
}

function fetchDoctorFromService($service_id,$city_condition)
{
    global $conn;
    $dataArray = array();
    //Fetch Hospital with respective Service , (Service is comma seprated so we are Using INSTR) thn fetch doctor acc to hospital ID USING JOINS
   $query = "SELECT LWCENTERMASTER.CENTERID,LWDOCTORNEWMASTER.DOCTORID,LWDOCTORNEWMASTER.DOCTORNAME  ,LWDOCTORNEWMASTER.SPECIALITYID ,INSTR (LWCENTERMASTER.SERVICES, '".$service_id."',1,1) AS ISSERVICE FROM    LWCENTERMASTER INNER JOIN LWDOCTORNEWMASTER ON LWCENTERMASTER.CENTERID=LWDOCTORNEWMASTER.CENTERID   Where INSTR (LWCENTERMASTER.SERVICES, '".$service_id."',1,1)>0".$city_condition;
 //$query = "SELECT LWCENTERMASTER.CENTERID, INSTR (LWCENTERMASTER.SERVICES, '".$service_id."',1,1) AS ISSERVICE FROM    LWCENTERMASTER    Where INSTR (LWCENTERMASTER.SERVICES, '".$service_id."',1,1)>0";
  $sql = @oci_parse($conn, $query);
    @oci_execute($sql);
    $i = 0;
    while ((@$row = oci_fetch_assoc(@$sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}
function fetchDoctorFromSecialty($spec_id,$city_condition) {
    global $conn;
    $dataArray = array();
    $query = "SELECT DOCTORID,DOCTORNAME,CENTERID  ,SPECIALITYID ,INSTR (SPECIALITYID, '" . $spec_id . "',1,1) AS ISSPECIALTY FROM    LWDOCTORNEWMASTER Where INSTR (SPECIALITYID, '" . $spec_id . "',1,1)>0".$city_condition;
   
    //echo $query;
    $sql = @oci_parse($conn, $query);
    @oci_execute($sql);
    $i = 0;
    while ((@$row = oci_fetch_assoc(@$sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function fetchDoctorFromAilment($ailment_id,$city_condition) {
    global $conn;
    $dataArray = array();
    $specialty_ids = fetchListCondsWithColumn("SPECAILTYID", "LWSPECIALTY_AILMENTMASTER", " where AILMENTID=" . $ailment_id);
   
    foreach ($specialty_ids as $spec_id) {
        $data = fetchDoctorFromSecialty($spec_id['SPECAILTYID'],$city_condition);
    }
    $dataArray = array_merge($dataArray, $data);
    return $dataArray;
}


function FetchJoinResultINNER($firstcolumn,$secondcolumn,$table1,$table2,$cond1,$where){
	global $conn;
	$dataArray=array();
	if($table1!=''){
 	 
		 $query="select $firstcolumn,$secondcolumn  from $table1 INNER JOIN $table2 ON  $cond1  $where";
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function FetchJoinResultINNER2($firstcolumn,$secondcolumn,$table1,$table2,$cond1,$where){
	global $conn;
	$dataArray=array();
	if($table1!=''){
 	 
		 $query="select $firstcolumn,$secondcolumn  from $table1 INNER JOIN $table2 ON  $cond1  $where";
                 ECHO $query;EXIT;
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function fetchHospitalDetailOfDoctor($hosp_id)
{
    global $conn;
	$dataArray=array();
	if($hosp_id!=''){
 	        $query="select LWCENTERMASTER.CENTERNAME,LWCENTERMASTER.DISCOUNTOFFER,LWCITYMASTER.CITYNAME,LWSTATEMASTER.STATENAME,LWCENTERMASTER.DISCOUNTDESC  from LWCENTERMASTER INNER JOIN LWCITYMASTER ON  LWCENTERMASTER.CITYID=LWCITYMASTER.CITYID INNER JOIN LWSTATEMASTER   ON LWCENTERMASTER.STATEID=LWSTATEMASTER.STATEID   where LWCENTERMASTER.CENTERID=".$hosp_id;
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
    
}
function fetchNewDoctorFromService($service_id,$city_condition)
{
    global $conn;
    $dataArray = array();
    //Fetch Hospital with respective Service , (Service is comma seprated so we are Using INSTR) thn fetch doctor acc to hospital ID USING JOINS
   $query = "SELECT LWCENTERMASTER.CENTERID,LWDOCTORNEWMASTER.DOCTORID,LWDOCTORNEWMASTER.DOCTORNAME  ,LWDOCTORNEWMASTER.SPECIALITYID ,INSTR (LWCENTERMASTER.SERVICES, '".$service_id."',1,1) AS ISSERVICE FROM    LWCENTERMASTER INNER JOIN LWDOCTORNEWMASTER ON LWCENTERMASTER.CENTERID=LWDOCTORNEWMASTER.CENTERID   Where LWDOCTORNEWMASTER.SERVICEID= '".$service_id."' ".$city_condition;
 
   //$query = "SELECT LWCENTERMASTER.CENTERID, INSTR (LWCENTERMASTER.SERVICES, '".$service_id."',1,1) AS ISSERVICE FROM    LWCENTERMASTER    Where INSTR (LWCENTERMASTER.SERVICES, '".$service_id."',1,1)>0";
  $sql = @oci_parse($conn, $query);
    @oci_execute($sql);
    $i = 0;
    while ((@$row = oci_fetch_assoc(@$sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}
function fetchListBySql($sql){
    global $conn;
	$dataArray=array();
	if($sql!=''){
 	     $query=$sql;
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){ 
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;

}
function sendMailToGenerateCoupon($filename,$email,$mobile,$hosp_id,$finalRefNo) {
    global $conn;
    $todayDate = date('d-M-Y', time());
    $todayDate = strtoupper($todayDate);
    $Mailtemplates = GetTemplateContentByTemplateNameForConfirmCoupon('COUPON');
//    $from = stripslashes(COUPON_EMAIL_FROM);
//    $subject = stripslashes(COUPON_EMAIL_SUBJECT);
    
    
    
		$from=stripslashes($Mailtemplates[0]['FROMEMAIL']);
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
                
    $bodytext = stripslashes($Mailtemplates[0]['CONTENT']);
 
	$uploadfilename = @$filename.'.pdf';
	$upload_temp="coupon/".@$uploadfilename;
	$to = $email; //Recipient Email Address
       
	$email = new PHPMailer();
          
	$email->From      = @$from;
	$email->FromName  = 'Religare Health Insurance';
	$email->Subject   = $subject;
	$email->Body      = $bodytext;
	$email->AddAddress($to);
	$file_to_attach = $upload_temp;
	$email->IsHTML(true);  
	$email->AddAttachment($file_to_attach , $uploadfilename);
        
	if(!$email->Send()) {
	 //'Message was not sent.';
	echo 'Mailer error: ' . $email->ErrorInfo;
	} else {
	  'Message has been sent.';
          $reasontomail = 'WECARE_DISCOUNT_COUPON';
				$sendby = '';
				$entryTime=date('d-M-Y');
				$status = 'ACTIVE';
				$sendfrom = $from;
				$ipAddress = get_client_ip();
				$userType2 = '';
			 
                                        $sqlLog="INSERT INTO PPHCMAILLOG (MAILID,MAILTO,SUBJECT,MESSAGE,USERTYPE,REASONTOMAIL,SENDBY,SENDDATE,STATUS,SENDFROM,IPADDRESS) values(PPHCMAILLOG_SEQ.nextval,q'[".$to."]','".@$subject."',:largecontent,'".@$userType2."','".@$reasontomail."','".@$sendby."','".@$entryTime."','".@$status."','".@$sendfrom."','".@$ipAddress."') ";             
                                    
                                        $sqlLogExe = @oci_parse($conn, $sqlLog);
                                        oci_bind_by_name($sqlLogExe, ':largecontent', $bodytext);
                                        $r = @oci_execute($sqlLogExe);
	 sendSMS($mobile,$hosp_id,$finalRefNo);
	}

		
}

function sendSMS($mobile,$hosp_id,$finalRefNo){
if(strlen(trim(sanitize_data(@$mobile)))>9){
         $hospitals=fetchListCond("LWCENTERMASTER"," where CENTERID=".$hosp_id);
         
         $state_info=fetchListCond("LWSTATEMASTER"," where STATEID=".$hospitals[0]['STATEID']);
         $city_info=fetchListCond("LWCITYMASTER"," where CITYID=".$hospitals[0]['CITYID']);
         $address=@$hospitals[0]['ADDRESS1']." ".@$hospitals[0]['ADDRESS2']." ".@$city_info[0]['CITYNAME']." ".@$state_info[0]['STATENAME'];
        
        
         
         $membername="User";
         if($_SESSION["livewell_employee_name"]){
            $membername=@$_SESSION["livewell_employee_name"];
         }
         
         $service_msg="";
         if($_SESSION["member_service_id"]){
         $service_info=fetchListCond("LWSERVICEMASTER"," where SERVICEID=".$_SESSION["member_service_id"]);
         $service_msg=" ".$service_info[0]['NAME']; 
         }
         
         $center="your selected hospital";
         if(isset($hospitals[0]["CENTERNAME"]) && !empty($hospitals[0]["CENTERNAME"])){
                 $center=@$hospitals[0]["CENTERNAME"];
         }
         
        $discount="";
        if(isset($hospitals[0]["DISCOUNTOFFER"]) && !empty($hospitals[0]["DISCOUNTOFFER"]))
        {
           $discount= $hospitals[0]["DISCOUNTOFFER"];
        }
        
         
       $message="Dear ".$membername.",".$service_msg." Discount Code for ".$center." is ".$finalRefNo.". Please present your ID proof & LiveWell discount coupon (mailed on your registered email ID) to avail  ".$discount." discount on selected services. Team RHI.";
         //$message='Coupon has been sent to your email id!';
	$mnumber=sanitize_data(@$mobile);
	$stringURL="&mnumber=91".trim($mnumber)."&message=".urlencode($message);
	 $apiUrl= SMSURL.@$stringURL;
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, $apiUrl);
		curl_setopt($ch, CURLOPT_HTTP_VERSION, 1.0);
		curl_setopt($ch, CURLOPT_HEADER, false);
                 curl_setopt($ch, CURLOPT_NOBODY, 1);	
		$response = curl_exec ( $ch );
		$ch_info=curl_getinfo($ch);
		curl_close($ch);
		//echo $response;
} else {
	echo "mobile length error";
}

}
function GetTemplateContentByTemplateNameForConfirmCoupon($templateName){
	global $conn;
	$dataArray=array();
	    $query="SELECT TEMPLATEID,SUBJECT,CONTENT,FROMEMAIL,FROMNAME,TEMPLATENAME from LWEMAILTEMPLTE where TEMPLATENAME='".$templateName."' ";
		
		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
//this function is used for get retail city where center exists in crmadmin by amit dubey on 19 november 2015 at 1:09 PM
function GetRetCityWhereCenterExist($planId,$stateId){
	global $conn;
	$dataArray=array();
		 $query=" SELECT LWCITYMASTER.CITYID,LWCITYMASTER.CITYNAME FROM LWCITYMASTER WHERE LWCITYMASTER.STATUS ='ACTIVE' AND LWCITYMASTER.CITYID IN ( SELECT DISTINCT LWCENTERMASTER.CITYID from LWCENTERMASTER JOIN CRMRETAILCENTERMAPPING  ON  CRMRETAILCENTERMAPPING.CENTERID=LWCENTERMASTER.CENTERID WHERE LWCENTERMASTER.STATUS='ACTIVE' AND CRMRETAILCENTERMAPPING.PLANID='".@$planId."'  AND LWCENTERMASTER.STATEID='".@$stateId."') ";

		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i = 0;
		while (($row = oci_fetch_assoc($sql))) {
				$dataArray[] = $row;
			$i++;
		}
		return $dataArray;
}
//this function is used for get corporate sub module mapped with policy by Er amit kumar dubey on 20 november 2015 at 11:28 AM
function GetCorpServiceListByPolicy($policyId,$ispaid){
	global $conn;
	$dataArray=array();
	if($policyId!=''){
		$query=" SELECT CRMSUBMODULEMASTER.ID,CRMSUBMODULEMASTER.DESCRIPTION,CRMSUBMODULEMASTER.SUBMODULENAME,CRMSUBMODULEMASTER.FILENAME,CRMSUBMODULEMASTER.LINK FROM CRMSUBMODULEMASTER LEFT JOIN CRMCORPSUBMODMAP ON CRMCORPSUBMODMAP.SUBMODULEID=CRMSUBMODULEMASTER.ID WHERE CRMSUBMODULEMASTER.STATUS='ACTIVE' AND CRMSUBMODULEMASTER.AF_CORPORATEPORTAL='YES' AND CRMSUBMODULEMASTER.ISPAID='".$ispaid."' AND CRMCORPSUBMODMAP.POLICYID=".$policyId."";

		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i = 0;
		while (($row = oci_fetch_assoc($sql))) {
				$dataArray[] = $row;
			$i++;
		}
	}
		return $dataArray;
}
//this function is used for fetch range wise sequence value like an age range Ex array(1,2,3,4,8,9,10) => 1-2,8-10 created by amit kumar dubey on 27 november 2015 at 4:21 PM

function getRanges( $nums )
{
    $ranges = array();

    for ( $i = 0, $len = count($nums); $i < $len; $i++ )
    {
        $rStart = $nums[$i];
        $rEnd = $rStart;
        while ( isset($nums[$i+1]) && $nums[$i+1]-$nums[$i] == 1 )
            $rEnd = $nums[++$i];

        $ranges[] = $rStart == $rEnd ? $rStart : $rStart.'-'.$rEnd;
    }

    return $ranges;
}
//this function is used for get retail sub module mapped with product by Er amit kumar dubey on 1 december 2015 at 12:11 PM
function GetRetServiceListByProduct($productid,$ispaid){
	global $conn;
	$dataArray=array();
	if($productid!=''){
		$query=" SELECT CRMSUBMODULEMASTER.ID,CRMSUBMODULEMASTER.DESCRIPTION,CRMSUBMODULEMASTER.SUBMODULENAME,CRMSUBMODULEMASTER.FILENAME,CRMSUBMODULEMASTER.LINK FROM CRMSUBMODULEMASTER LEFT JOIN CRMRETPRODSETUPSUBMODMAP ON CRMRETPRODSETUPSUBMODMAP.SUBMODULEID=CRMSUBMODULEMASTER.ID WHERE CRMSUBMODULEMASTER.STATUS='ACTIVE' AND CRMSUBMODULEMASTER.AF_CORPORATEPORTAL='YES' AND CRMSUBMODULEMASTER.ISPAID='".$ispaid."' AND CRMRETPRODSETUPSUBMODMAP.PRODUCTID=".$productid."";

		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i = 0;
		while (($row = oci_fetch_assoc($sql))) {
				$dataArray[] = $row;
			$i++;
		}
	}
		return $dataArray;
}
function fetchSlotListByCond($cond){
	global $conn;
	$dataArray=array();
	if($cond!=''){
		$query="SELECT CRMSLOTMASTER.STARTTIMENAME,CRMSLOTMASTER.SLOTID,CRMCALENDERSLOTMAPPING.ID,CRMCALENDERSLOTMAPPING.NOOFAPPOINTMENT,CRMCALENDERSLOTMAPPING.MAXAPPOINTMENT,CRMCALENDERSLOTMAPPING.ISAVAIL FROM CRMSLOTMASTER LEFT JOIN CRMCALENDERSLOTMAPPING ON CRMCALENDERSLOTMAPPING.SLOTID=CRMSLOTMASTER.SLOTID $cond";
		$sql = @oci_parse($conn, $query);
		// Execute Query
	@oci_execute($sql);
		
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if(getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if(getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if(getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if(getenv('HTTP_FORWARDED'))
       $ipaddress = getenv('HTTP_FORWARDED');
    else if(getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}
function fetchCorporateProposalsListByCond($Email){
	global $connProposal;
	$dataArray=array();
	if($Email!=''){
		echo $query="SELECT * from reference_data where EAIL_ID = '".$Email."'";
		$sql = @oci_parse($connProposal, $query);
		// Execute Query
	$res=@oci_execute($sql);
	if(!$res){
		print_r(oci_error($sql));
	} 
		$i=0;
		while (($row = @oci_fetch_assoc($sql))) { //printr($row); die;
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	}
	return $dataArray;
}
//this function is used for get xol product list where corporate employee exists created by Er. amit kumar dubey on 12 december 2015 at 05:17 PM
function fetchXolProdListForEmployee($policyNumber,$employeeid){
	global $conn;
	$dataArray=array();
		$query=" SELECT CRMCORPORATE.CORPORATEID,CRMCORPORATE.CORPORATEUNIQUECODE,CRMCORPORATE.RELATION,CRMTOPUPPLANEMPLOYEE.SUPERTOPUP,CRMTOPUPPLANEMPLOYEE.EMAILID,CRMTOPUPPLANEMPLOYEE.TOPUPPLANEMPLOYEEID,CRMTOPUPPLANEMPLOYEE.PREVENTIVE,CRMTOPUPPLANEMPLOYEE.PARENTPOLICY,CRMTOPUPPLANEMPLOYEE.OPD FROM CRMCORPORATE LEFT JOIN CRMTOPUPPLANEMPLOYEE ON CRMTOPUPPLANEMPLOYEE.CORPORATEID=CRMCORPORATE.CORPORATEID WHERE CRMCORPORATE.RELATION='YES' AND CRMCORPORATE.POLICYNUMBER='".$policyNumber."' AND CRMTOPUPPLANEMPLOYEE.EMPLOYEEID='".$employeeid."'";

		$sql = @oci_parse($conn, $query);
		// Execute Query
		@oci_execute($sql);
		$i = 0;
		while (($row = oci_fetch_assoc($sql))) {
				$dataArray[] = $row;
			$i++;
		}
		return $dataArray;
}
function random_password( $length = 8 ) {
    $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?";
    $password = substr( str_shuffle( $chars ), 0, $length );
    return $password;
}

// this function is used for proposal db . created by amit kumar dubey on 18 january 2016 at 12:10 PM
function fetchListCondsWithColumnProposal($column,$table,$cond){
	global $connProposal;
	$dataArray=array();
	 $query="SELECT $column FROM $table $cond";
		$sql = @oci_parse($connProposal, $query);
		// Execute Query
		@oci_execute($sql);
		$i=0;
		while ((@$row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}
// Encrypt Function
function simple_encrypt($encrypt, $key){
    $encrypt = serialize($encrypt);
    $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
    $key = pack('H*', $key);
    $mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
    $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt.$mac, MCRYPT_MODE_CBC, $iv);
    $encoded = base64_encode($passcrypt).'|'.base64_encode($iv);
    return $encoded;
}
// Decrypt Function
function simple_decrypt($decrypt, $key){
    $decrypt = explode('|', $decrypt.'|');
    $decoded = base64_decode($decrypt[0]);
    $iv = base64_decode($decrypt[1]);
    if(strlen($iv)!==mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)){ return false; }
    $key = pack('H*', $key);
    $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
    $mac = substr($decrypted, -64);
    $decrypted = substr($decrypted, 0, -64);
    $calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
    if($calcmac!==$mac){ return false; }
    $decrypted = unserialize($decrypted);
    return $decrypted;
}
// function added by Dinesh chandra 20-01-2016 1.32 PM for non preferred hospital search starts
function non_preferred_cities_page_array($pageListArray1){
	$m =0;
	$dataArr=array();
	if(is_array(@$pageListArray1['page'][0]) )	{
		$pageListArray = $pageListArray1['page'];
		for($i=0;$i <sizeof($pageListArray);$i++)
		{
			if(trim($pageListArray[$i]['isDeleted']['value']) =='no'){
			    $dataArr[$m]['stateId'] 		  		= @$pageListArray[$i]['stateId']['value'];
				$dataArr[$m]['cityId'] 		  		= @$pageListArray[$i]['cityId']['value'];
				$dataArr[$m]['cityTitle'] 	 		 = @$pageListArray[$i]['cityTitle']['value'];
				$dataArr[$m]['isDeleted']         = trim(@$pageListArray[$i]['isDeleted']['value']);
				$m++;
			}
		}
	}else	{
			$pageListArray = @$pageListArray1['page'];	
			if(trim($pageListArray['isDeleted']['value']) =='no') 	{
			        $dataArr[$m]['stateId'] 		  		= @$pageListArray[$i]['stateId']['value'];
					$dataArr[$m]['cityId'] 			= $pageListArray['cityId']['value'];
					$dataArr[$m]['cityTitle'] 		= @$pageListArray['cityTitle']['value'];
					$dataArr[$m]['isDeleted']		= trim(@$pageListArray['isDeleted']['value']);			
					$m++;
				}
	}
return $dataArr;
}

function non_preferred_hospital_page_array($pageListArray1,$city,$advanceSearchString){  

    $advanceSearch = trim($advanceSearchString);
	$m =0; 
	if(is_array(@$pageListArray1['page'][0]) )	{ 
		$pageListArray = $pageListArray1['page'];  
       
		for($i=0;$i < sizeof($pageListArray);$i++)
		{
			if($city!='' && $advanceSearch==''){   
			  if($city==@$pageListArray[$i]['city']['value'] ){                                  
				@$dataArr[$m]['hospitalId'] 	       = trim($pageListArray[$i]['hospitalId']['value']);
				@$dataArr[$m]['state'] 			       = @$pageListArray[$i]['state']['value'];
				@$dataArr[$m]['city'] 			       = @$pageListArray[$i]['city']['value'];
				@$dataArr[$m]['hospitalName'] 	       = @$pageListArray[$i]['hospitalName']['value'];
				@$dataArr[$m]['address'] 		       = @$pageListArray[$i]['address']['value'];
				@$dataArr[$m]['pincode'] 		       = @$pageListArray[$i]['pincode']['value'];
				@$dataArr[$m]['alternateHospitalName'] = @$pageListArray[$i]['alternateHospitalName']['value'];
				$m++;        
				}
			} else if($city!='' && $advanceSearch!=''){ 
                 $orignal = trim(strtolower(base64_decode($pageListArray[$i]['hospitalName']['value'])));
                 $countText  = strlen($advanceSearch);
                 $match = substr($orignal,0,$countText);			 		 
				 $str = explode(" ",$orignal);
				 $match2 = substr($str[1],0,$countText);
				 if(count(explode(" ",$orignal))>3){
				   $match3 = substr($str[2],0,$countText);
				 }
				 if(count(explode(" ",$orignal))>4) { 
				   $match4 = substr($str[3],0,$countText);
				 }    
                if(($city==@$pageListArray[$i]['city']['value']) && (($match==strtolower($advanceSearch)) || ($match2==strtolower($advanceSearch)) || ($match1==strtolower($advanceSearch)) || (count(explode(" ",$orignal))>3 && ($match3==strtolower($advanceSearch))) || (count(explode(" ",$orignal))>4 && ($match4==strtolower($advanceSearch))))) {
                     
				@$dataArr[$m]['hospitalId'] 	       = trim($pageListArray[$i]['hospitalId']['value']);
				@$dataArr[$m]['state'] 			       = @$pageListArray[$i]['state']['value'];
				@$dataArr[$m]['city'] 			       = @$pageListArray[$i]['city']['value'];
				@$dataArr[$m]['hospitalName'] 	       = @$pageListArray[$i]['hospitalName']['value'];
				@$dataArr[$m]['address'] 		       = @$pageListArray[$i]['address']['value'];
				@$dataArr[$m]['pincode'] 		       = @$pageListArray[$i]['pincode']['value'];
				@$dataArr[$m]['alternateHospitalName'] = @$pageListArray[$i]['alternateHospitalName']['value'];
				$dataArr[$m]['match'] 	               = @$match;
                $m++;
			} }
		}
		
	}
return @$dataArr;
}
function non_preferred_hospital_particular_details($pageListArray1,$id){
	$dataArr=array();
	$m =0; 
	if(is_array(@$pageListArray1['page'][0]) )	{
		$pageListArray = $pageListArray1['page'];  
       
		for($i=0;$i < sizeof($pageListArray);$i++)
		{
			if(@$id==trim(@$pageListArray[$i]['hospitalId']['value'])){                          
				@$dataArr[$m]['hospitalId'] 		    = trim($pageListArray[$i]['hospitalId']['value']);
				@$dataArr[$m]['state'] 			        = @$pageListArray[$i]['state']['value'];
				@$dataArr[$m]['city'] 			        = @$pageListArray[$i]['city']['value'];
				@$dataArr[$m]['hospitalName'] 	        = @$pageListArray[$i]['hospitalName']['value'];
				@$dataArr[$m]['address'] 		        = @$pageListArray[$i]['address']['value'];
				@$dataArr[$m]['pincode'] 		        = @$pageListArray[$i]['pincode']['value'];
				@$dataArr[$m]['alternateHospitalName']  = @$pageListArray[$i]['alternateHospitalName']['value'];
				$dataArr[$m]['isDeleted']               = trim(@$pageListArray[$i]['isDeleted']['value']);
				$m++; 
				break;       
				}
			 }
		}
return @$dataArr;
}
// function added by Dinesh chandra 20-01-2016 1.32 PM for non preferred hospital search ends

//function used for masking mobile for registraton for retail created by amit kumar dubey on 21 january 2016 at 12:45 PM
function mobileMasking($number, $maskingCharacter = 'X') {
    //return substr($number, 0, 3) . str_repeat($maskingCharacter, strlen($number) - 6) . substr($number, -3);
    return str_repeat($maskingCharacter, strlen($number) - 4) . substr($number, -4);
}

//function used for masking email for registraton for retail created by amit kumar dubey on 21 january 2016 at 12:45 PM
function emailMasking($email)
{

$prop=2;
    $domain = substr(strrchr($email, "@"), 1);
    $mailname=str_replace($domain,'',$email);
    $name_l=strlen($mailname);
    $domain_l=strlen($domain);
        for($i=0;$i<=$name_l/$prop-1;$i++)
        {
        $start.='x';
        }

        for($i=0;$i<=$domain_l/$prop-1;$i++)
        {
        $end.='x';
        }

    return substr_replace($mailname, $start, 2, $name_l/$prop).substr_replace($domain, $end, 2, $domain_l/$prop);
}

// function used for difference in year between two dates . format Y-m-d like 2015-05-21. variable method value can be 1-M OR 2-Y OR 3-D OR 4-TODALDAYS OR 5-TOTALMONTH
function getDateDifference($date1,$date2,$method){
	$d1 = new DateTime($date1);
	$d2 = new DateTime($date2);
	$diff = $d2->diff($d1);
	if($method=='Y'){
	 return $diff->y;
	}
	if($method=='M'){
	 return $diff->m;
	}
	if($method=='D'){
	 return $diff->d;
	}
	if($method=='TODALDAYS'){
	 return $diff->days;
	}
	if($method=='TOTALMONTH'){
	 return (12*$diff->y)+$diff->m;
	}
}
function getYearRange($StaringDate,$EndDate){
	$diff=getDateDifference($EndDate,$StaringDate,'Y');
	$DateRange=array();
for($i=0;$i<=$diff;$i++){
		if(strtotime($EndDate) > strtotime($StaringDate)){
			if($i==0){
				$StaringDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($StaringDate)) . " + 0 year"));
			} else {
				$StaringDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($StaringDate)) . " + 1 year"));
			}
			$newEndingDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($StaringDate)) . " + 1 year"));
			$newEndingDate = date("Y-m-d", strtotime(date("Y-m-d", strtotime($newEndingDate)) . " -1 days"));
			$res['STARTDATE']=$StaringDate;
			$res['ENDDATE']=$newEndingDate;
		$DateRange[]=$res;	
	   }
	}
	return $DateRange;
}

?>