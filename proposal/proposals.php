<?php
/* * ***************************************************************************
 * COPYRIGHT
 * Copyright 2013 Catabatic Technology Pvt Ltd.
 * All rights reserved
 *
 * DISCLAIMER
 *
 *
 * AUTHOR
 *
 * $Id: proposals.php,v 1.0 2015/12/09 03:55:0 Pooja Choudhary $
 * $Author: Pooja Choudhary $
 *
 * ************************************************************************** */

include("inc/inc.hd2.php");     //include header file to show header on page
include_once('../../religarehrcrm/api/api.php');
if (isset($_SESSION['response_error'])) {
    $responseError = $_SESSION['response_error'];
} else {
    $responseArray = $_SESSION['dataArray'];
}
if (@$TPAManagedchkdis == 'Yes') {
    echo "<script>window.location.href='dashboard.php';</script>";
    exit;
}
$proposalsData = fetchCorporateProposalsListByCond($_SESSION['userName']);
?>
<style>
    div.error {
        position:absolute;
        margin-top:14px;
        color:red;
        background-color:#fff;
        padding:3px;
        text-align:left;
        z-index:1;
        margin-left: -17px;
        font-size:14px;
    }
</style>
<style>
    div #upload1-error {
        margin-left: 231px;
        margin-top: 52px;
    }
    div #upload2-error {
        margin-left: 231px;
        margin-top: 52px;
    }
    div #upload3-error {
        margin-left: 231px;
        margin-top: 52px;
    }
    div #upload4-error {
        margin-left: 231px;
        margin-top: 52px;
    }
    div #upload5-error {
        margin-left: 231px;
        margin-top: 52px;
    }
</style>
<section id="middleContainer">
    <div class="container-fluid">
        <div class="middlebox">
            <div class="col-md-9">
                <div class="middlebox-left">

                    <div class="tab-content responsive">
                                    <div class="tab-pane <?php if(empty($tab) || $tab=="claims"){ echo "active"; } ?>" id="buyPlan">


                            <div class="colfullBot">
                                <div class="tab-paneIn">
                                    <div class="col-md-12">

                                        <div class="title-bg textleft">
                                            <h1>Proposals List</h1>
                                        </div>

                                        <div class="myPlanForm">
                                            <table class="responsive" width="100%">
                                                <?php
                                                $r = 0;
                                                if(count($proposalsData) > 0){
                                                foreach ($proposalsData as $key => $response) {
                                                    $r++;
//                                                        echo "<pre>"; print_r($response['AGENT_ID'] ); die;
//                                                        for ($c = 0; $c < count($response); $c++) {
                                                    if ($r == 1) {
                                                        ?>
                                                        <tr>
                                                            <th>Application No.</th>
                                                            <th>Cover Type</th>
                                                            <th>Product Code</th>
                                                            <th>Policy No.</th>
                                                            <th>Premium</th>
                                                        </tr>
                                                    <?php }
													if($response['POLICY_NUMBER']==""){
													 ?>
                                                    
                                                    <tr>
                                                        <td>
                                                        <?php foreach($productCodeNameArray as $code=>$page)
																{
																	if($response['PRODUCTCODE'] == $code) {?>
   <a href="<?php echo 'https://buyuat.religare.com:7443/newdesign/'.$page.'.php?id='.$response['REFERENCE_ID'].'&code='.hash('sha256','proposal-'.$response['REFERENCE_ID']); ?>" style="text-decoration:underline" target="_blank"> <?php echo $response['PROPOSAL_ID'] ?></a> 
                                                        <?php } // if END
																}// Foreach END?>
                                                        </td>
                                                        <td><?php echo $response['COVERTYPE'] ?></td>
                                                        <td><?php echo $response['PRODUCTCODE'] ?></td>
                                                        <td><?php echo $response['POLICY_NUMBER'] ?></td>
                                                        <td><?php echo $response['PREMIUM_AMOUNT'] ?></td>


                                                    </tr>
                                                    <?php  
													}   //if($response['POLICY_NUMBER']!="") END
//                                                        }
                                                }
                                                } else  {
                                                    ?>
                                                    <tr>
                                                        <td colspan="10" align="center" style="text-align:-moz-center;border:none;"><font color="#cc0000"><b>No record(s) found.</b></font></td>
                                                    </tr>
                                                    <?php
                                                }
                                                ?>
                                            </table>
                                        </div>                     
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <?php include("inc/inc.right.php"); ?>
            </div> 
        </div>
    </div>
</section>
<script>
    $(function () {
        $('.closeBox').click(function () {
            $('#exampleModal1').modal('hide');
        });
    });
</script>

<script type='text/javascript'>
    $(window).load(function () {
        $(function () {
            $(".dropdown-menu").on("click", "li", function (event) {
                console.log("You clicked the drop downs", event)
            })
        })
    });
</script>

<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="model-header" id="exampleModalLabel">Reimbursement Doc List</div>
            </div>
            Please Wait....

        </div>
    </div>
</div>
<?php include("inc/inc.ft.php"); ?>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/additional-methods.min.js" type="text/javascript"></script>
<script src="js/owl.carousel.js" type="text/javascript"></script>
<script type="text/javascript">
    $.validator.addMethod("noStartEndWhiteSpaces", function (value, element) {
        return this.optional(element) || /^[a-zA-Z0-9](?:[a-zA-Z0-9 ]*[a-zA-Z0-9])?$/.test(value);
    }, "Spaces or special characters not allowed.");
    $.validator.addMethod("noStartEndWhiteSpacesAlpha", function (value, element) {
        return this.optional(element) || /^[a-zA-Z](?:[a-zA-Z ]*[a-zA-Z])?$/.test(value);
    }, "Number not allowed.");

    jQuery.validator.addMethod("specialChars", function (value, element) {
        var regex = new RegExp("^[a-zA-Z0-9]+$");
        var key = value;

        if (!regex.test(key)) {
            return false;
        }
        return true;
    }, "Special characters not allowed");

    $("#DocRequestForm").validate({
        rules: {
            "txtEmail": {
                required: true,
                email: true
                        //noStartEndWhiteSpaces: true
            },
            "txtPhone": {
                required: true,
                number: true,
                maxlength: 10,
                minlength: 10
            },
            "docTitle1": {
                required: true,
            },
            "docTitle2": {
                required: true,
            },
            "docTitle3": {
                required: true,
            },
            "docTitle4": {
                required: true,
            },
            "docTitle5": {
                required: true,
            },
            "txtEnquiry": {
                required: true,
            },
            "upload1": {
                extension: "jpg|doc|pdf|xls|"
            },
            "upload2": {
                extension: "jpg|doc|pdf|xls|"
            },
            "upload3": {
                extension: "jpg|doc|pdf|xls|"
            },
            "upload4": {
                extension: "jpg|doc|pdf|xls|"
            },
            "upload5": {
                extension: "jpg|doc|pdf|xls|"
            }


        },
        messages: {
            "txtEmail": {
                required: 'Please enter email'
            },
            "txtPhone": {
                required: 'Please enter contact no'
            },
            "docTitle1": {
                required: 'Please enter doc title'
            },
            "docTitle2": {
                required: 'Please enter doc title'
            },
            "docTitle3": {
                required: 'Please enter doc title'
            },
            "docTitle4": {
                required: 'Please enter doc title'
            },
            "docTitle5": {
                required: 'Please enter doc title'
            },
            "upload1": {
                extension: 'Please choose correct file'
            },
            "upload2": {
                extension: 'Please choose correct file'
            },
            "upload3": {
                extension: 'Please choose correct file'
            },
            "upload4": {
                extension: 'Please choose correct file'
            },
            "upload5": {
                extension: 'Please choose correct file'
            }
        },
        errorElement: "div"
    });

// custom rule for checking . and character after . for email validation
    jQuery.validator.addMethod("email", function (value, element) {
        return this.optional(element) || (/^[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}$/.test(value) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(value));
    }, 'Please enter valid email address.');
</script>


