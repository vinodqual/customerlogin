<?php
/**
 * CustomerLoginFunction Class. Contains useful static functions.
 * 
 * @author : Shakti Rana <shakti@catpl.co.in> 03 May 2016 18:37.
 */
require_once 'conf/conf.php'; 
require_once 'conf/common_functions.php';

class CustomerLoginFunction {

    static function mapPolicyWithProposal($username) {
        ///added condition for mapping policy with proposals from proposal database table RETUSERPOLICYMAP cretaed by amit kumar dubey on 18 january 2016 at 11:56 AM
        $proposalPolicyList = fetchListCondsWithColumnProposal("POLICY_NUMBER,DOB", "reference_data_final", " WHERE POLICY_NUMBER IS NOT NULL AND LOWER(EMAIL_ID) ='" . strtolower($username) . "'");

        if (count($proposalPolicyList) > 0) {
			global $conn;
            $k = 0;
            while ($k < count($proposalPolicyList)) {
                $queryCheckWrongAttempts = fetchcolumnListCond("POLICYNUM", "RETUSERPOLICYMAP", "WHERE POLICYNUM='" . @$proposalPolicyList[$k]['POLICY_NUMBER'] . "' ");

                if (count($queryCheckWrongAttempts) == 0) {
                    $sqlInsertMapping = "INSERT INTO RETUSERPOLICYMAP (USERPOLICYMAPSEQ,USERID,POLICYNUM,CUSTOMERID,EMAILID,DOB,MEMBERTYPE) VALUES (RETUSERPOLICYMAP_SEQ.nextval,'" . trim(@$_SESSION['USERID']) . "','" . $proposalPolicyList[$k]['POLICY_NUMBER'] . "','','" . @$username . "','" . $proposalPolicyList[$k]['DOB'] . "','')"; //insert query to insert records 

                    $stdidsMap = @oci_parse($conn, $sqlInsertMapping);
                    $rsdMap = @oci_execute($stdidsMap);
                }
                $k++;
            }
        }
        ///end added condition for mapping policy with proposals from proposal database table RETUSERPOLICYMAP
    }

    static function mapPolicyWithPolicymap($username) {
        ///added condition for mapping policy with policymap from rhstage database table USERPOLICYMAP cretaed by Er amit kumar dubey on 28 april 2016 at 01:24 PM
        $rhstagePolicyList = fetchListCondsWithColumnRhstage("POLICYNUM,CUSTOMERID,DOB", "USERPOLICYMAP", " WHERE POLICYNUM IS NOT NULL AND LOWER(EMAILID) ='" . strtolower($username) . "'");
		
        if (count($rhstagePolicyList) > 0) {
			global $conn;
            $k = 0;
			while ($k < count($rhstagePolicyList)) {
                $queryCheckWrongAttempts = fetchcolumnListCond("POLICYNUM", "RETUSERPOLICYMAP", "WHERE POLICYNUM='" . @$rhstagePolicyList[$k]['POLICYNUM'] . "' ");
				if(count($queryCheckWrongAttempts) == 0) {
					
                  $sqlInsertMapping2 = "INSERT INTO RETUSERPOLICYMAP (USERPOLICYMAPSEQ,USERID,POLICYNUM,CUSTOMERID,EMAILID,DOB,MEMBERTYPE) VALUES (RETUSERPOLICYMAP_SEQ.nextval,'" . trim(@$_SESSION['USERID']) . "','" . $rhstagePolicyList[$k]['POLICYNUM'] . "','" . $rhstagePolicyList[$k]['CUSTOMERID'] . "','" . @$username . "','" . $rhstagePolicyList[$k]['DOB'] . "','')"; //insert query to insert records 

                    
					$stdidsMap2 = @oci_parse($conn, $sqlInsertMapping2);
                    $rsdMap2 = @oci_execute($stdidsMap2);
				
                }

                $k++;
            }
			
        }
        ///end added condition for mapping policy with proposals from rhstage database table USERPOLICYMAP
    }
}

?>