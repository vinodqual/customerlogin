﻿<?php 
include_once("../conf/session.php");
include 'inc/conf.php';
$propasalPage = "0";
if (isset($_REQUEST['id'])) {
    $requestedId = sanitize_data($_REQUEST['id']);
    $checkCode = sanitize_data($_REQUEST['code']);
    $checkId = hash('sha256', "proposal-$requestedId");
    if ($checkCode != $checkId) {
        $propasalPage = "1";
    }
    $resultAllData = fetchListByColumnName('REFERENCE_DATA', $requestedId); 
    if (!empty($resultAllData)) {
        $resultData = $resultAllData[0];
        $cont = $resultData['QNA']->load();
        $t = base64_decode($cont);
        $questionArrayResult = json_decode($t, true);
        $UTM_SOURCE = json_decode($resultData['UTM_SOURCE'], true);
        $jsonDependentDetail = json_decode($resultData['DEPENDENT_DETAIL']);
    } else {
        $propasalPage = "1";
    }
} else {
    $requestedId = md5("NEW");
}

$ValidCityName = '';
$occupation = '';
	if(isset($resultData['UTM_SOURCE'])){
	$utmSource = json_decode($resultData['UTM_SOURCE'], true);    
	$occupation = $utmSource['occupation'];	
	$ValidCityName = $utmSource['ValidCityName'];
	}
    
  

if (isset($resultData['INITIALS'])) {
    $INITIALS = sanitize_data($resultData['INITIALS']);
} else {
    $INITIALS = '0';
}
if (isset($resultData['FIRST_NAME'])) {
    $FIRST_NAME = sanitize_data($resultData['FIRST_NAME']);
} else {
    $FIRST_NAME = '';
}
if (isset($resultData['LAST_NAME'])) {
    $LAST_NAME = sanitize_data($resultData['LAST_NAME']);
} else {
    $LAST_NAME = '';
}
if (isset($resultData['DOB'])) {
    $DOB = sanitize_data($resultData['DOB']);
} else {
    $DOB = '';
}

if (isset($resultData['ADDRESS_1'])) {
    $ADDRESS_1 = sanitize_data($resultData['ADDRESS_1']);
} else {
    $ADDRESS_1 = '';
}


if (isset($resultData['STATE'])) {
    $CValidStateName = sanitize_data($resultData['STATE']);
} else {
    $CValidStateName = '';
}

if (isset($resultData['ADDRESS_2'])) {
    $ADDRESS_2 = sanitize_data($resultData['ADDRESS_2']);
} else {
    $ADDRESS_2 = '';
}
if (isset($resultData['EMAIL_ID'])) {
    $EMAIL_ID = sanitize_data_email($resultData['EMAIL_ID']);
} else {
    $EMAIL_ID = '';
}
if (isset($resultData['PIN_CODE'])) {
    $PIN_CODE = sanitize_data($resultData['PIN_CODE']);
} else {
    $PIN_CODE = '';
}
if (isset($resultData['PANCARD_NUMBER']) && $resultData['PANCARD_NUMBER'] != '') {
    $PANCARD_NUMBER = sanitize_data($resultData['PANCARD_NUMBER']);
} else {
    $PANCARD_NUMBER = '';
}
if (isset($UTM_SOURCE['quotationReferenceNum'])) {
    $quotationReferenceNum = sanitize_data($UTM_SOURCE['quotationReferenceNum']);
} else {
    $quotationReferenceNum = time();
}
if (isset($resultData['SUMINSURED'])) {
    $sumInsured = sanitize_data($resultData['SUMINSURED']);
} else {
    $sumInsured = '';
}

if (isset($resultData['INSURANCE_AMT'])) {
    $sInsuredAmount = sanitize_data($resultData['INSURANCE_AMT']);
} else {
    $sInsuredAmount = '';
}
if (isset($resultData['PREMIUM_AMOUNT'])) {
    $finalPremiumCheck = sanitize_data($resultData['PREMIUM_AMOUNT']);
} else {
    $finalPremiumCheck = '';
}
//echo '<pre>';print_r($resultData);exit;

if (isset($resultData['ELDEST_MEMBER_AGE'])) {
    $ageGroupOfEldestMember = sanitize_data($resultData['ELDEST_MEMBER_AGE']);
} else {
    $ageGroupOfEldestMember = '';
}
if (isset($resultData['INSURANCE_AMT'])) {
    $individualInsuredAmountAssure = sanitize_data($resultData['INSURANCE_AMT']);
    $sumInsuredValue1 = '500000';
} else {
    $individualInsuredAmountAssure = '100000';
    $sumInsuredValue1 = '500000';
}
if (isset($resultData['TENURE'])) {
    $tenure = sanitize_data($resultData['TENURE']);
} else {
    $tenure = '1';
}

if (isset($UTM_SOURCE['rmCode'])) {
    $rmCode = sanitize_data($UTM_SOURCE['rmCode']);
} else {
    $rmCode = '';
}

if (isset($UTM_SOURCE['branchCode'])) {
    $branchCode = sanitize_data($UTM_SOURCE['branchCode']);
} else {
    $branchCode = '';
}


if (isset($UTM_SOURCE['nomineeRelation'])) {
    $nomineeRelation = sanitize_data($UTM_SOURCE['nomineeRelation']);
} else {
    $nomineeRelation = '';
}

if (isset($UTM_SOURCE['NomineeName'])) {
    $NomineeName = sanitize_data($UTM_SOURCE['NomineeName']);
} else {
    $NomineeName = '';
}
if (isset($resultData['AGENT_ID'])) {
    $agentId = sanitize_data($resultData['AGENT_ID']);
} else {
    $agentId = $_SESSION['username'];
}
if (isset($resultData['MOBILE_NO'])) {
    $mobileNum = sanitize_data($resultData['MOBILE_NO']);
} else {
    $mobileNum = '';
}

$coverType = "INDIVIDUAL";
//$productCode = "20001004";
$productCode = "30001006";
$source = "FAVEOOTC";
$productFamily = "SECURE";
$numberOfAdult = "1";
$numberOfChildren = "0";

if ($propasalPage == "1") {
    header("LOCATION:error.php");
    exit;
}


include 'inc/topScript.php';
?>
<script type="text/javascript">


function chequeddvalidate() {
        var policypagebankname = $("#policypagebankname").val();
        var paymentType = $("#paymentType").val();
        var transactiondate = $("#transactiondate").val();
        var micr = $("#micr").val();
        var policypageBranchName = $("#policypageBranchName").val();
        var transactionID = $("#transactionID").val();

        if (micr == '' && $('input[name=micrHide]:checked').val() == 'show') {
            alert("Please enter micr number");
            $("#micr").focus();
            return false;
        }
        if (policypagebankname == '') {
            alert("Please enter bank name");
            $("#policypagebankname").focus();
            return false;
        }
        if (paymentType == '') {
            alert("Please enter bank name");
            $("#paymentType").focus();
            return false;
        }
        if (transactiondate == '') {
            alert("Please enter transaction date");
            $("#transactiondate").focus();
            return false;
        }
        if (policypageBranchName == '') {
            alert("Please enter bank branch name");
            $("#policypageBranchName").focus();
            return false;
        }
        if (transactionID == '') {
            alert("Please enter transaction id");
            $("#transactionID").focus();
            return false;
        }
//	$("#submit").attr("src","images/logo_loader.gif");
        $.ajax({
            url: "chequeDDResp.php",
            type: "POST",
//            async: false,
            data: $('#thankPage').serialize(),
            beforeSend: function() {
//                $("#loader").show();
//                $("#submit").hide();
                $("#submit").attr("src","images/logo_loader.gif");
            },
            complete: function() {
//                $("#loader").hide();
//                $("#submit").show();
            },
            success: function(data) {
                var splitData = data.split('##');
                if (splitData[0] == 'error') {
                    $('#checkddErr').html(splitData[1]);
                    $("#submit").attr("src","images/Faveo-OTC_submit.jpg");
                } else {
                    $('#pdfPath').val(splitData[2]);
                    $('#receiptNum').val(splitData[3]);
                    $('#inwardStatus').val(splitData[4]);                   
		    $('#proposalStatus').val(splitData[1]);		
                    $('#mircSubmit').trigger('click');
                    return false;
                }
            }
        });

        //document.getElementById("thankPage").submit();
    }



</script>
<body>
    <script type="text/javascript" src="js/wz_tooltip.js"></script>
    <?php include '../inc/header.php'; ?>
    <?php include '../inc/navigation.php'; ?>
    <div class="page_nav">
        <a href="home.php">Home</a> » Quotation » Secure</div>

    <?php include 'inc/editSecure.php'; ?>



    <div class="mid_inner_container_otc" id="proposal_form" >
        <div class="quoteBoxgreenUp fl"><a href="javascript://"  id="editquote">Edit Quote</a></div>
        <form action="savePagesecure.php" method="post" class="AdvancedForm" name="savePageassureForm" id="savePageassureForm">

            <input type="hidden" name="proposalRequestId" id="proposalRequestId" value="<?php echo $requestedId; ?>" />
            <input type="hidden" name="proposalSumInsured" id="proposalSumInsured" value="<?php echo $sumInsured; ?>" />
            <input type="hidden" name="proposalageGroupOfEldestMember" id="proposalageGroupOfEldestMember" value="<?php echo $ageGroupOfEldestMember; ?>" />
            <input type="hidden" name="totalAdultMember" id="totalAdultMember" value="<?php echo $numberOfAdult; ?>"/>
            <input type="hidden" name="totalChildMember" id="totalChildMember" value="<?php echo $numberOfChildren; ?>"/>
            <input type="hidden" name="proposalProductCode" id="proposalProductCode" value="<?php echo $productCode; ?>"/>
            <input type="hidden" name="proposalDummySi" id="proposalDummySi" value="<?php echo $sInsuredAmount; ?>"/>
            <input type="hidden" name="saveandcontinueemail"  id="saveandcontinueemail" value=""/>
            <input type="hidden" name="agentId"  value="<?php echo $agentId; ?>"/>
            <input type="hidden" name="proposalTenourCode" id="proposalTenourCode" value="<?php echo $tenure; ?>"/>
            <input type="hidden" name="rmCode"  value="<?php echo $rmCode; ?>"/>
            <input type="hidden" name="permiumamountValid" id="permiumamountValid" value="<?php echo $finalPremiumCheck; ?>"/>
            <input type="hidden" name="branchCode"  value="<?php echo $branchCode; ?>"/>
            <input type="hidden" name="quotationReferenceNum"  value="<?php echo $quotationReferenceNum; ?>"/>
            <input type="hidden" name="productFamily"  value="<?php echo $productFamily; ?>"/>
            <input type="hidden" name="source"  value="<?php echo $source; ?>"/>
            <input type="hidden" name="productCode"  value="<?php echo $productCode; ?>"/>
            <input type="hidden" name="coverType"  value="<?php echo $coverType; ?>"/>       


            <div class="quotetopbarotc fl">Your Proposal Form</div>
            <div class="quoteresultmidotc fl">
                <div class="proposerDetailBox fl">
                    <h1>Proposer’s Details</h1>

                    <div class="proposerDetailBoxForm fl">
                        <table width="949" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="40" valign="middle"><table width="949" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="119" valign="top"> 
                                                    <select id="ValidTitle" name="ValidTitle" onChange="changeTitleList();"  class="selectPad" style="width: 80px;">
                                                        <option value="0" <?php if ($INITIALS == '0') { ?> selected="selected" <?php } ?>>Title</option>
                                                        <option value="MR" <?php if ($INITIALS == 'MR') { ?> selected="selected" <?php } ?>>Mr</option>
                                                        <option value="MS" <?php if ($INITIALS == 'MS') { ?> selected="selected" <?php } ?>>Ms</option>
                                                    </select>
                                                
</td>
                                            <td width="188" valign="top"><input name="ValidFName" id="ValidFName" type="text" onBlur="changeTitleList();" placeholder="First name"  class="txtfield_OTC" style="width:170px;" value="<?php echo $FIRST_NAME; ?>"/></td><td width="18" valign="top"><span class="mandatorytxt">*</span></td>
                                            <td width="188" valign="top"><input type="text" name="ValidLName" id="ValidLName" onBlur="changeTitleList();" class="txtfield_OTC" style="width:170px;" placeholder="Last Name" value="<?php echo $LAST_NAME; ?>" /></td><td width="18" valign="top"><span class="mandatorytxt">*</span></td>
                                            <td width="198" valign="top"><input type="text" name="DOB" id="datepicker" readonly="true" autocomplete="off" class="txtfield_OTCDate" onClick="scwShow(scwID('datepicker'), event, '01/01/1975');" onBlur="changeTitleList();" style="width:170px;" placeholder="DOB - DD/MM/YYYY" value="<?php echo $DOB; ?>" /></td>
                                            <td width="206" valign="top"><input type="text" name="ValidMobileNumber" maxlength="10"  id="ValidMobileNumber" class="txtfield_OTC" style="width:200px;" placeholder="Mobile No." value="<?php echo $mobileNum; ?>" /></td><td width="18" valign="top"><span class="mandatorytxt">*</span></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="40" valign="middle"><table width="949" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="356" valign="top"><input type="text" name="ValidAddressOne" id="ValidAddressOne" class="txtfield_OTC" style="width:350px;" placeholder="Address Line 1" value="<?php echo $ADDRESS_1; ?>" /></td><td width="18" valign="top"><span class="mandatorytxt">*</span></td>
                                            <td width="374" valign="top"><input type="text" name="ValidAddressTwo" id="ValidAddressTwo" class="txtfield_OTC" style="width:350px;" placeholder="Address Line 2" value="<?php echo $ADDRESS_2; ?>" /></td>
                                            <td width="183" valign="top"><input type="text" name="ValidEmail" id="ValidEmail" class="txtfield_OTC" style="width:177px;" placeholder="Email Id" value="<?php echo $EMAIL_ID; ?>" /></td><td width="18" valign="top"><span class="mandatorytxt">*</span></td>
                                        </tr>
                                    </table></td>
                            </tr>
                            <tr>
                                <td height="40" valign="middle"><table width="949" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="263" valign="top"><input type="text" name="NomineeName" id="NomineeName" class="txtfield_OTC" style="width:245px;" placeholder="Nominee Name" value="<?php echo $NomineeName; ?>" /></td><td width="18" valign="top"><span class="mandatorytxt">*</span></td>
                                            <td width="110" valign="top"> 
                             <select id="nomineeRelation"  name="nomineeRelation" class="selectPad" style="font-weight:normal;">
                                                        <option value="0" <?php if ($nomineeRelation == '0') { ?> selected="selected" <?php } ?>>Relation</option>
                                                        <option value="WIFE" <?php if ($nomineeRelation == 'WIFE') { ?> selected="selected" <?php } ?>>Wife</option>
                                                        <option value="MOTHER" <?php if ($nomineeRelation == 'MOTHER') { ?> selected="selected" <?php } ?>>Mother</option>
                                                        <option value="HUSBAND" <?php if ($nomineeRelation == 'HUSBAND') { ?> selected="selected" <?php } ?>>Husband</option>
                                                        <option value="DAUGHTER" <?php if ($nomineeRelation == 'DAUGHTER') { ?> selected="selected" <?php } ?>>Daughter</option>
                                                        <option value="SON" <?php if ($nomineeRelation == 'SON') { ?> selected="selected" <?php } ?>>Son</option>
                                                        <option value="FATHER" <?php if ($nomineeRelation == 'FATHER') { ?> selected="selected" <?php } ?>>Father</option>

                                                        <option value="SIBLING" <?php if ($nomineeRelation == 'SIBLING') { ?> selected="selected" <?php } ?>>Sibling</option>
                                                        <option value="GRANDMOTHER" <?php if ($nomineeRelation == 'GRANDMOTHER') { ?> selected="selected" <?php } ?>>Grand Mother</option>
                                                        <option value="GRANDFATHER" <?php if ($nomineeRelation == 'GRANDFATHER') { ?> selected="selected" <?php } ?>>Grand Father</option>

                                                        <option value="SISTERINLAW" <?php if ($nomineeRelation == 'SISTERINLAW') { ?> selected="selected" <?php } ?>>Sister-In-Law</option>
                                                        <option value="BROTHERINLAW" <?php if ($nomineeRelation == 'BROTHERINLAW') { ?> selected="selected" <?php } ?>>Brother-In-Law</option>
                                                        <option value="BROTHER" <?php if ($nomineeRelation == 'BROTHER') { ?> selected="selected" <?php } ?>>Brother</option>
                                                        <option value="SISTER" <?php if ($nomineeRelation == 'SISTER') { ?> selected="selected" <?php } ?>>Sister</option>
                                                        <option value="MOTHERINLAW" <?php if ($nomineeRelation == 'MOTHERINLAW') { ?> selected="selected" <?php } ?>>Mother In Law</option>
                                                    </select>
                                                </td>
                                            <td width="109" valign="top"><input type="text" name="ValidPinCode" id="ValidPinCode" onBlur="getCityName();" class="txtfield_OTC" style="width:90px; margin-left:10px;" placeholder="Pincode" value="<?php echo $PIN_CODE; ?>" /></td><td width="18" valign="top"><span class="mandatorytxt">*</span></td>
                                            <td width="230" valign="top">
                                                    
	 <select name="ValidCityName" id="ValidCityName" class="selectPad" style="width:185px; font-weight:normal;">
                                                        <option value="<?php if ($ValidCityName != '') {
        echo $ValidCityName;
    } ?>"><?php if ($ValidCityName != '') {
        echo $ValidCityName;
    } else { ?>City (Auto on Pincode) <?php } ?> </option>
                                                    </select>
                                                </td>
                                            <td width="201" valign="top"><input type="text"  value="<?php echo $ValidCityName; ?>" name="ValidStateName" id="ValidStateName" class="txtfield_OTC" style="width:177px;" placeholder="State" /></td>
                                        </tr>
                                    </table></td>
                            </tr>
                        </table>
                    </div>

                    <h1>Details of person (s) to be insured</h1>
                </div>

                <?php
                $self = 0;
                $dep = 0;
                for ($i = 1; $i <= $numberOfAdult; $i++) {
                    ?>

                    <div class="proposerDetailBoxFormgray fl" style="background:none;">
                        <table width="949" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="115" valign="top">
                                        <select name="relationCd[]" id="relationCd-<?php echo $i; ?>" class="selectPad" onChange="changeTitleCd(this,<?php echo $i; ?>);" style="width:80px; font-weight:normal;">
                                            <option value="0" <?php if (@$jsonDependentDetail[$dep]->relationCd == '0') { ?> selected="selected" <?php } ?>>Relation</option>
                                            <option value="SELF" <?php if (@$jsonDependentDetail[$dep]->relationCd == 'SELF') { ?> selected="selected" <?php } ?>>Self</option>
      

                                        </select>
                                    </td>
                                <td width="115" valign="top">                                        <select name="titleCd[]" id="titleCd-<?php echo $i; ?>" class="selectPad" style="width:80px; font-weight:normal;">
                                                <?php if (isset($jsonDependentDetail[$dep]->titleCd) && ($jsonDependentDetail[$dep]->titleCd != '')) { ?>
                                                <option value="<?php echo $jsonDependentDetail[$dep]->titleCd; ?>"  selected="selected" ><?php
                                                    if ($jsonDependentDetail[$dep]->titleCd == '0') {
                                                        echo "Title";
                                                    } else {
                                                        echo ucfirst(strtolower($jsonDependentDetail[$dep]->titleCd));
                                                    }
                                                    ?></option>
    <?php } else { ?>
                                                <option value="0" >Title</option>
                                                <option value="MR">Mr</option>
                                                <option value="MS">Ms</option>
    <?php } ?>
                                        </select>
                                    </td>
                                <td width="175" valign="top"><input type="text" name="firstNameCd[]" id="firstNamecd-<?php echo $i; ?>" class="txtfield_OTC" style="width:170px;" placeholder="First Name" value="<?php echo @$jsonDependentDetail[$dep]->firstNameCd; ?>" /></td><td width="45" valign="top"><span class="mandatorytxt">*</span></td>
                                <td width="175" valign="top"><input type="text" name="lastNameCd[]" id="lastNamecd-<?php echo $i; ?>" class="txtfield_OTC" style="width:170px;" placeholder="Last Name" value="<?php echo @$jsonDependentDetail[$dep]->lastNameCd; ?>" />
                                    </td><td width="45" valign="top"><span class="mandatorytxt">*</span></td>
                                <td width="289" valign="top"><input type="text" name="dOBCd[]" autocomplete="off" readonly="true" id="datepickerCD-<?php echo $i; ?>" class="txtfield_OTCDate" style="width:170px;" placeholder="DOB - DD/MM/YYYY" value="<?php echo @$jsonDependentDetail[$dep]->dOBCd; ?>" onClick="var retval = checkDateVal(<?php echo $i; ?>);
                                                                if (retval == false) {
                                                                    return false;
                                                                }
                                                                scwShow(scwID('datepickerCD-<?php echo $i; ?>'), event, '<?php echo date('d/m/Y',strtotime('-20 years')); //echo $curentData; ?>');" /></td>
                            </tr>
<tr><td colspan="5">&nbsp;</td></tr>
<tr><td colspan="5">

       <select class="selectPad" name="occupation" onchange="questionnairedatashow();" id="occupation-1">
                                                                <option value="0">Occupation</option>
		<option value="I003" <?php if($occupation == 'I003'){ echo "selected"; } ?> >Professionals having no exposure to work outside office ( Consultant/ Engineer/ Doctor/ Dentist/ Lawyer/ CA)</option>
                                                                <option value="I004" <?php if($occupation == 'I004'){ echo "selected"; } ?> >Teacher/ Architect/ ITES employee/ BPO employee</option>
                                                                <option value="I005" <?php if($occupation == 'I005'){ echo "selected"; }?> >Management &amp; Administration staff having no exposure to work outside office</option>
                                                                <option value="I006" <?php if($occupation == 'I006'){ echo "selected"; }?>>Sales &amp; Marketing professionals</option> 
                                                                <option value="I007" <?php if($occupation == 'I007'){ echo "selected"; }?>>Business Owners ( Excluding Chemicals/ Arms &amp; Ammunition/ Explosives/ Fireworks shop owners)</option>
                                                                <option value="I009" <?php if($occupation == 'I009'){ echo "selected"; }?> >Technician ( Excluding- Electricians/ Nuclear &amp; Chemical lab technicians)</option>
                                                                <option value="P009" <?php if($occupation == 'P009'){ echo "selected"; }?>>Other</option>
                                                            </select>      
     



</td></tr>
                        </table>

                    </div>
<?php } ?>

                

                <div class="proposerDetailBox">
                    <h1><img src="images/minus_green.jpg" class="fl" onClick="questionnairedatahide()" id="questionnairedatahideid"><img src="images/plus_green.jpg" class="fl" id="questionnairedatashowid" onClick="questionnairedatashow()" style="display:none;">&nbsp;Health Questionnaire</h1>
<div id="questionnairedata">
                    
                        <div class="questionBar fl">
 <h2 id="healthclck1" >Any existing Disability/Deformity (physical or mental impairment/infirmity or any condition hampering vision, hearing or mobility) ?</h2>
                      <div class="yes_no_box" id="health_question1">
                                                                                                                    <input  name="questions[P001]" onClick="openSubHealthQuestion();" id="healthquestion1-1" type="radio" value="1" 
<?php
if (isset($questionArrayResult['P001'])) {
    if (@$questionArrayResult['P001'] == 1) {
        ?> checked="checked" <?php
                                                                                                                        }
                                                                                                                    }
                                                                                                                    ?>       
                                                                                                                            />
                                                                                                                    Yes
                                                                                                                    <input name="questions[P001]" onClick="openSubHealthQuestion();" id="healthquestion1-2" type="radio" value="0" 
<?php
if (isset($questionArrayResult['P001'])) {
    if (@$questionArrayResult['P001'] == 0) {
        ?> checked="checked" <?php
                                                                                                                        }
                                                                                                                    }
                                                                                                                    ?>       

                                                                                                                           />
                                                                                                                    No </div>
                                                                                                                <div class="cl"></div>
                                                                                                            </div>



                                                                                                            <div class="questionBar fl">
<h2 id="healthclck2">Does your job require you to engage in significant manual labor or hazardous activities or requires handling hazardous material or working at significant heights or with high voltage ?
<!--<img src="images/questionsmark.png"  onmouseout="UnTip()" onMouseOver="Tip('Please do not include treatments for common cold, flu fever, regular medical check-ups.')" border="0" class="qicon"> -->
</h2>
                                                                                                                <div class="yes_no_box" id="health_question2">
                                                                                                                    <input name="questions[P007]" id="healthquestion2-1" type="radio" value="1" 
                                                                                                                    <?php
                                                                                                                    if (isset($questionArrayResult['P007'])) {
                                                                                                                        if (@$questionArrayResult['P007'] == 1) {
                                                                                                                            ?> checked="checked" <?php
                                                                                                                               }
                                                                                                                           }
                                                                                                                           ?>       

                                                                                                                           />
                                                                                                                    Yes
                                                                                                                    <input name="questions[P007]" id="healthquestion2-2" type="radio" value="0" 
                                                                                                                    <?php
                                                                                                                    if (isset($questionArrayResult['P007'])) {
                                                                                                                        if (@$questionArrayResult['P007'] == 0) {
                                                                                                                            ?> checked="checked" <?php
                                                                                                                               }
                                                                                                                           }
                                                                                                                           ?>       

                                                                                                                           />
                                                                                                                    No </div>
                                                                                                                <div class="cl"></div>
                                                                                                            </div>

                                                                                                           <div class="questionBar fl">
<h2 id="healthclck3" >Has any company ever declined to issue/renew a Personal Accident policy for the proposed person? 
<!--<img src="images/questionsmark.png" border="0" class="qicon" onMouseOver="Tip('Please do not include treatments for common cold, flu fever, regular medical check-ups.');" onMouseOut="UnTip()">-->
</h2>
                                                                                                                <div class="yes_no_box" id="health_question3">
                                                                                                                    <input name="questions[P003]" id="healthquestion3-1" type="radio" value="1" 
                                                                                                                    <?php
                                                                                                                    if (isset($questionArrayResult['P003'])) {
                                                                                                                        if (@$questionArrayResult['P003'] == 1) {
                                                                                                                            ?> checked="checked" <?php
                                                                                                                               }
                                                                                                                           }
                                                                                                                           ?>       

                                                                                                                           />
                                                                                                                    Yes
                                                                                                                    <input name="questions[P003]" id="healthquestion3-2" type="radio" value="0" 
                                                                                                                    <?php
                                                                                                                           if (isset($questionArrayResult['P003'])) {
                                                                                                                               if (@$questionArrayResult['P003'] == 0) {
                                                                                                                                   ?> checked="checked" <?php
                                                                                                                       }
                                                                                                                   }
                                                                                                                           ?>       

                                                                                                                           />
                                                                                                                    No </div>
                                                                                                                <div class="cl"></div>
                                                                                                            </div>
      <div class="questionBar fl">
  <h2  id="healthclck6"> Have you ever been diagnosed or are under treatment for any terminal illness or any illness/disease restricting your activities (e.g. Epilepsy/Seizure disorder) ?
<!--<img src="images/questionsmark.png" border="0" onMouseOut="UnTip();" onMouseOver="Tip('Please select Yes if any person(s) to be insured has Diabetes, Hypertension, Liver Diseases, Cancer, Cardiac Disease, Joint Pain, Kidney Disease, Paralysis, Congenital Disorder, HIV/ AIDS. This is vital for correct Claim disbursal.');" class="qicon"/> -->
</h2>
                                                                                                                <div class="yes_no_box" id="health_question6">
                                                                                                                    <input name="questions[P008]" id="healthquestion6-1" type="radio" value="1" 
<?php
if (isset($questionArrayResult['P008'])) {
    if (@$questionArrayResult['P008'] == 1) {
        ?> checked="checked" <?php
                                                                                                                        }
                                                                                                                    }
                                                                                                                    ?>       

                                                                                                                           />
                                                                                                                    Yes
                                                                                                                    <input name="questions[P008]" id="healthquestion6-2" type="radio" value="0" 
<?php
if (isset($questionArrayResult['P008'])) {
    if (@$questionArrayResult['P008'] == 0) {
        ?> checked="checked" <?php
    }
}
?>       

                                                                                                                           />
                                                                                                                    No </div>
                                                                                                                <div class="cl"></div>
                                                                                                            </div>
																											</div>
                    
                    
                    
                    
                    
                    
                    
                    
                    
                    









                    <div class="termBox fl" id="validTermCondition">
                        <input type="checkbox" value="1" <?php if (@$resultData['TNC_AGREED'] == 1) { ?> checked="checked" <?php } ?> id="validTermCondition-1" name="validTermCondition" />
                        I here by agree to the <a href="javascript:void(0)" onClick="window.open('http://rhicluat.religare.com/proposalterms.html','_blank','width=700,height=515')">term & conditions</a> of the purchase of this policy. *<br />

                    </div>
                    <div class="termBox">
                       		 <input type="checkbox"  id="recivedSms" name="recivedSms" />
                        Receive Service SMS and E-mail alerts
                    </div>

                    <div class="proceedBox fl">
<!--	<div style="margin-bottom: 25px; color: rgb(221, 0, 0);" class="redtxtBottom" id="errordisplayPrashant">Kindly fill the boxes highlighted in red.</div> -->
					
                        <table border="0" align="center" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                     <input type="hidden" id="checkclickid" name="checkclickid" value="1">
                                    <input type="image" id="FormSubmit"  onclick="checkClickEvent(1);" src="images/preceed_btn.jpg" style="border: none;"/></td>
                                <td>
		<input type="image" name="save" src="images/save_emailBtn.jpg" value="" style="border: none; cursor: pointer;" onClick="checkClickEvent(2);" class="saveContinue savebtn"/>
				</td>								
                            </tr>
                        </table>

                    </div>
                </div>
            </div></form>
        <!-- <img src="images/grayotcBot.jpg" class="fl">-->
        <div class="cl"></div></div>

                            <?php include_once "../inc/footer.php"; ?>


 <!--    <div class="feedback_thumb"><a class='example2' href="#"><img src="images/feedback_thumb.png" border="0" /></a></div> -->



   <div  style='display:none'>
        <div id='inline_example2'>

            <div class="enquiry_popup">   


                <div class="EnquiryCounty fl">
                    <h1>Your Feedback</h1>
                    <ul>
                        <li>
                            <input type="text" class="TextBoxCount" value="Your Full Name">
                        </li>
                        <li><input type="text" class="TextBoxCount" value="Email Address"></li>
                        <li><input type="text" class="TextBoxCount" value="Contact No."></li>
                        <li><textarea cols="" rows="2" class="TextAreaCount"></textarea> </li>
                        <li><div class="cl" align="right" style="padding:5px 12px 0 0;"><a href="#"><img src="images/submit_btn.png" border="0" alt="" title=""></a></div></li>
                    </ul>

                </div>

                <div class="cl"></div></div>

        </div>
        <div class="cl"></div>
</div> 

    <div  style="display:none;">
        <div id='inline_proceedpay'>
            <div class="pop_proceed fl">
<?php if ($resultAllData[0]['PREMIUM_AMT'] != $resultAllData[0]['PREMIUM_AMOUNT']) { ?>
                    <h3>The DOB of eldest member to be insured is different from the age range you selected while taking quote. Hence, the total premium is revised from <br/>
                        <img alt="rupees" height="10" src="images/rupeesymbol_gr.png"/> <?php echo $resultAllData[0]['PREMIUM_AMT']; ?> to <img alt="rupees" height="10" src="images/rupeesymbol_gr.png"/> <?php echo $resultAllData[0]['PREMIUM_AMT']; ?>.</h3>
                        <?php } ?>
                <!--                                                                                                    <form action="pmt.php" method="POST" name="submitPMT" id="submitPMT">-->
                <div class="pop_proceedContainer">
                    <img src="images/yourProposalSummary.jpg" alt="your proposal" class="fl" style="margin:-1px 0 0 -1px;" />

                    <div class="proceedTable">
                        <table width="810" border="0" cellspacing="0" cellpadding="10">
                            <tr>
                                <td width="225" align="left" bgcolor="#ededed"><strong>Application No</strong></td>
                                <td width="264" align="center" bgcolor="#ededed"><strong>Plan Type</strong></td>
                                <td width="259" align="center" bgcolor="#ededed"><strong>Policy Period</strong></td>
                            </tr>
                            <tr>
                                <td align="left"><a href="javascript://"><input type="hidden" name="proposalNumberOnsnapshot" value="<?php echo $resultAllData[0]['PROPOSAL_ID']; ?>"/><?php echo $resultAllData[0]['PROPOSAL_ID']; ?></a></td>

                                <td align="center">Secure-1</td>

                                <td align="center"><?php echo $resultAllData[0]['TENURE']; ?> Year</td>
                            </tr>
                        </table>
                    </div>

                    <table width="100%" border="0" cellspacing="0" cellpadding="0" class="fl">
                        <tr>
                            <td width="74%" height="25" align="right" valign="middle" class="pdingTop"><strong>Sum Insured : </strong>
                                <label><strong style="color:#5c9a1b;"><img alt="rupees" height="10" src="images/rupeesymbol_gr.png"/><?php echo moneyFormatIndia($resultAllData[0]['INSURANCE_AMT']); ?></strong></label>&nbsp;</td>

                            <td width="26%"><table width="199" border="0" align="right" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td class="bgimg_large tdpad">Your premium is: <img alt="rupees" height="10" src="images/rupeesymbol_wh.png"/> <?php echo moneyFormatIndia($resultAllData[0]['PREMIUM_AMT']); ?></td>
                                    </tr>
                                </table></td>
                        </tr>
                    </table>


                </div>

                <div class="pop_proceedContainerbelow">
                    <div class="proceedTable">
                        <table width="810" border="0" cellspacing="0" cellpadding="10">
                            <tr>
                                <td width="225" align="left" bgcolor="#ededed"><strong>Person(s) Covered</strong></td>
                                <td width="264" align="center" bgcolor="#ededed"><strong>Date Of Birth</strong></td>
                                <td width="259" align="center" bgcolor="#ededed"><strong>Your Contact Address</strong></td>
                            </tr>
<?php
$dep1 = 0;
for ($i1 = 1; $i1 <= $numberOfAdult; $i1++) {
    ?>
                                <tr>
                                    <td align="left"><a href="javascript://"><?php echo $jsonDependentDetail[$dep1]->titleCd . '&nbsp;' . $jsonDependentDetail[$dep1]->firstNameCd . '&nbsp;' . $jsonDependentDetail[$dep1]->lastNameCd ?></a></td>
                                    <td align="center"><?php echo $jsonDependentDetail[$dep1]->dOBCd; ?></td>
                                    <td align="center"><?php echo $resultAllData[0]['ADDRESS_1'] . ',&nbsp;' . $resultAllData[0]['STATE'] . ',&nbsp;' . $resultAllData[0]['PIN_CODE']; ?> </td>
                                </tr>
    <?php
    $dep1++;
}
?>

                        </table>
                    </div>

                    <div class="proceedTable">
                        <table width="810" border="0" cellspacing="0" cellpadding="0" class="fl" style="border:0px;">
                            <tr>
                                 <td width="290" align="right">
<!-- <input type="image" class="payddandcheck"   src="images/preceedOnline_btn.jpg" style="border:none"> -->
<input class="payddandcheck cboxElement" src="images/preceed_dd_btn.jpg" style="border:none" type="image">

</td>
								 <td>
 <!-- <a href="#" class="savebtn"><img src="images/preceedOnline_btn.jpg" border="0" /></a> -->
</td>								
                                <td width="318" align="right"><a href="javascript://" onClick="hideColorBox();" class="backEdit"><< Back to Edit</a></td>
                            </tr>
                        </table>
                    </div>
                </div>
                <!--                                                                                                    </form>-->
                <div class="cl"></div>
            </div>


        </div>
    </div>
    <div style="display:none">
  <div class="pop1 fl" id="errormassage">
    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="fl">
      <tr>
        <td height="30"><?php
echo $_SESSION['errorMSGE'];
unset($_SESSION['errorMSGE']);
?>
        </td>
      </tr>
    </table>
    <div class="cl"></div>
  </div>
</div>
<div  style="display:none;">
        <div id='inline_saveContinue' style="text-align:center;">
            <div class="pop1 fl" id="savebtn">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="fl">
                    <tr>
                        <td height="30">Your Proposal will be saved and sent as it is to your id</td>
                    </tr>
                    <tr>
                        <td height="30"><input name="confirmMail" id="confirmMail"  type="text" class="mailid" value="" /></td>
                    </tr>
                    <tr>
                        <td height="30">So that you may continue later from it.</td>
                    </tr>
                    <tr>
                        <td height="50" align="center"><input name="image" type="image" src="images/proposalbutton.png" onClick="saveform();" style="border:none;"  /></td>
                    </tr>
                </table>
                <div class="cl"></div>
            </div>
        </div>
    </div>
    
    
    
    
    <div  style="display:none;">
        <div id='inline_saveContinueLater' style="text-align:center;">
            <div class="pop1 fl" id="savebtn">
                <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" class="fl">
                    <tr>
                        <td height="30">Once If you choose save & continue later the proposal.You can view the proposal from "Proposal Draft screen".</td>
                    </tr>
<!--                    <tr>
                        <td height="30"><input name="confirmMail" id="confirmMail"  type="text" class="mailid" value="" /></td>
                    </tr>-->
<!--                    <tr>
                        <td height="30">So that you may continue later from it.</td>
                    </tr>-->
                    <tr>
                        <td height="50" align="center"><input name="image" type="image" src="images/Save-to-Proposal-History.jpg" onClick="saveContinueLater();" style="border:none;"  /></td>
                    </tr>
                </table>
                <div class="cl"></div>
            </div>


        </div>
    </div>




    <div  style="display:none;">
        <div id='inline_proceedpay_dd'>
            <div class="pop_otc fl">
                <h3>Issue Policy Page</h3>
                <form action="thank.php" id="thankPage" method="POST" name="thankPage" class="thankPage">
                    <div class="pop_proceedContainer">
                        <img src="images/yourProposalSummary.jpg" alt="your proposal" class="fl" style="margin:-1px 0 0 -1px;" />
                        
                        <div style="padding:10px 5px 0 220px; font:bold 12px arial; color:#ff0000; text-align:right;"><span style="color:black;">Application no.:</span> <?php echo $resultAllData[0]['PROPOSAL_ID']; ?></div>
                        <div id="checkddErr" style="padding:10px 0 0 220px; font:bold 12px arial; color:#ff0000; text-align:center;"></div>

                        <div class="proceedTable">  
                            <div class="feveo_otc_left">

                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="borderno">

                                    <tr>
                                        <td width="41%" height="40">Payment Method <img src="images/questionsmark.png" border="0"></td>
                                        <td width="8%" height="0">:</td>
                                        <td width="51%" height="0">
                                            <table width="90%" border="0" cellspacing="0" cellpadding="0" class="borderno" ALIGN="right">
                                                <tr>
                                                    <td width="55%" align="left"><input name="policypage[paymentMethod]" type="radio" value="CHEQUE" checked="checked" /> Cheque</td>
                                                    <td width="45%" align="left"><input name="policypage[paymentMethod]" type="radio" value="DD" /> DD</td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="0">Amount <img src="images/questionsmark.png" border="0"></td>
                                        <td height="0">:</td>
                                        <td height="0"><input name="policypage[amount]" type="text" value="<?php echo $resultAllData[0]['PREMIUM_AMT']; ?>" readonly="true" class="medium_inputHealth mob_inputHealth gapB" /></td>
                                    </tr>
                                    <tr>
                                        <td height="0">Bank Name <img src="images/questionsmark.png" border="0"></td>
                                        <td height="0">:</td>
                                        <td height="0"><input name="policypage[bankname]" id="policypagebankname" type="text" value="" class="medium_inputHealth mob_inputHealth gapB" /></td>
                                    </tr>
                                    <tr>
                                        <td height="0">Payment Type <img src="images/questionsmark.png" border="0"></td>
                                        <td height="0">:</td>
                                        <td height="0">

                                            <select style="width:185px;" name="policypage[paymenttype]" id="paymentType">
                                                <option value="">Select</option>
                                               <option value="HOMEBANK">House</option>
                                                <option value="LOCALCHEQUE">Local</option>
                                                <option value="OUTSTATIONCHEQUE">Outstation</option>
                                            </select>
                                         <input type="hidden" id="proposalStatus" name="policypage[proposalStatus]" />
                                        <input type="hidden" id="pdfPath" name="policypage[pdfPath]" />
                                        <input type="hidden" id="receiptNum" name="policypage[receiptNum]" />
                                        <input type="hidden" id="inwardStatus" name="policypage[inwardStatus]" />        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="0">Transaction Date <img src="images/questionsmark.png" border="0"></td>
                                        <td height="0">:</td>
                                        <td height="0"><input id="transactiondate" name="policypage[transactiondate]" type="text" autocomplete="off"  onclick="scwShow(scwID('transactiondate'), event, '<?php echo $curentData; ?>');" class="medium_inputHealth mob_inputHealth gapB" /></td>
                                    </tr>
                                </table>



                            </div>
                            <div class="feveo_otc_left">

                                <table width="100%" border="0" cellspacing="0" cellpadding="0" class="borderno">
                                    <tr>
                                        <td width="49%" height="40" colspan="2"></td>                                        
                                        <td width="51%" height="0">
                                            <table width="90%" border="0" cellspacing="0" cellpadding="0" class="borderno" ALIGN="right">
                                                <tr>
                                                    <td width="55%" align="left"><input name="micrHide" class="micrHide" type="radio" value="show" checked="checked" /> Micr
                                                    </td>
                                                    <td width="45%" align="left"><input name="micrHide" class="micrHide" type="radio" value="hide" />Non-Micr
                                                    </td>
                                                </tr>
                                            </table>

                                        </td>
                                    </tr>
                                    <tr class="micrHideShow">
                                        <td width="41%" height="0">MICR <img src="images/questionsmark.png" border="0"></td>
                                        <td width="8%" height="0">:</td>
                                        <td width="51%" height="0"><input name="policypage[MICR]" onBlur="checkMicr();" id="micr" type="text" value=""  class="medium_inputHealth mob_inputHealth gapB" /></td>
                                    </tr>
                                    <tr>
                                        <td height="0" width="41%">Branch Name <img src="images/questionsmark.png" border="0"></td>
                                        <td height="0" width="8%">:</td>
                                        <td height="0" width="51%"><input name="policypage[BranchName]" id="policypageBranchName" type="text" value=""  class="medium_inputHealth mob_inputHealth gapB" /></td>
                                    </tr>
                                    <tr>
                                        <td height="0">Transaction ID  <img src="images/questionsmark.png" border="0"></td>
                                        <td height="0">:</td>
                                        <td height="0">
                                            <input name="policypage[TransactionID]" type="text" id="transactionID" class="medium_inputHealth mob_inputHealth gapB" maxlength="8"/>
                                            <input name="policypage[proposalNum]" type="hidden" value="<?php echo $resultAllData[0]['PROPOSAL_ID']; ?>"  />
                                            <input name="policypage[pageName]" type="hidden" value="secure1"  />
                                            <input name="policypage[redirectTo]" type="hidden" value="secure_pdf"  />
                                            <input name="policypage[print]" type="hidden" value="securePrint"  />
                                            <input name="policypage[id]" type="hidden" value="<?php
                            if (isset($_REQUEST['id'])) {
                                echo $_REQUEST['id'];
                            }
                            ?>"  />
                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </div>

                        <div class="proceedTable">
                            <table width="810" border="0" cellspacing="0" cellpadding="0" class="fl" style="border:0px;">
                                <tr>
                                    <td width="490" align="right">
<!--                                        <img src="images/logo_loader.gif" id="loader" style="cursor:pointer; display:none;"/>-->
                                        <img id="submit" src="images/Faveo-OTC_submit.jpg" onClick="chequeddvalidate()" style="cursor:pointer;"/>
                                                                     <!--<input name="" type="image" img src="images/Faveo-OTC_submit.jpg" border="0" class="otc_submit" />--></td>								
                                    <td width="318" align="right"> <a href="javascript:void(0);" id="backToPrevScreen" class="backEdit"> << Back to Previous Page </a> </td>
                                <input type="submit" id="mircSubmit" style="display:none;">
                                </tr>
                            </table>
                        </div>


                    </div>
                </form>
                <div class="cl"></div>
            </div>


        </div>
    </div>
    <script type="text/javascript">

	 $(document).ready(function() {
            $("#proposal_form").show();
            $("#clickbuy").click(function() {
                $("#getSearch").hide();
                $("#proposal_form").show();
            });
            $("#editquote").click(function() {
                $("#getSearch").show();
                $("#proposal_form").hide();
                //assuresearch();
            });


            /*   Vary Micr and Non-Micr */

            $('.micrHide').change(function() {
                if ($(this).val() === 'hide') {
                    $('.micrHideShow').fadeOut('slow');
                    $('#policypageBranchName').val('');
                    $('#policypagebankname').val('');
                    $('#micr').val('');
                }
                else {
                    $('.micrHideShow').fadeIn('slow');
                }
            });

        });

                                                        
    </script>

<?php if (isset($_REQUEST['status']) && $_REQUEST['status'] == 1) { ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $.colorbox({width: "auto", height: "auto", inline: true, href: "#inline_proceedpay"});
                $(".payddandcheck").colorbox({width: "auto", height: "auto", inline: true, href: "#inline_proceedpay_dd"});
            });
            $("#getSearch").hide();
            $("#proposal_form").show();
        </script>
<?php
} else if ((isset($_REQUEST['code'])) || (isset($_REQUEST['checkPageOpen']) && $_REQUEST['checkPageOpen'] == 9)) {


    
    ?>
        <script type="text/javascript">
            $(document).ready(function() {
                $("#getSearch").hide();
                $("#proposal_form").show();
            });
        </script>
<?php } else { ?>
        <script type="text/javascript">


            $(document).ready(function() {
                $("#proposal_form").hide();
                $("#getSearch").show();
                //assuresearch();
            });
        </script>
<?php } ?>
    <script type="text/javascript">


        $(document).ready(function() {
           secureResult();
		$("#backToPrevScreen").click(function(){
			window.location.reload();
		});
});
    </script>
    <script src="js/secure_know.js" type="text/javascript"></script>
    <script src="js/jquery.validation.functions_secure.js" type="text/javascript"></script>
	<script type="text/javascript">
$(document).ready(function() {
	$(".termBox").css({'border-top':'0'});
	$( "#questionnairedata").hide();
 	$( "#questionnairedatahideid").hide();
  	$( "#questionnairedatashowid").show();
});
function questionnairedatahide(){	 
	  $( "#questionnairedata" ).slideUp( "slow", function() {
		// Animation complete.slideDown
	  });
	  $(".termBox").css({'border-top':'0'});
	  $( "#questionnairedatahideid").hide();
	  $( "#questionnairedatashowid").show();
}
function questionnairedatashow(){	 
  $( "#questionnairedata" ).slideDown( "slow", function() {
    // Animation complete.slideDown
  });
  $(".termBox").css({'border-top':'1px dotted #191919'});
  $( "#questionnairedatashowid").hide();
  $( "#questionnairedatahideid").show();
}
$( "#datepickerCD-<?php echo $numberOfAdult?>" ).focusout(function() {	
 	questionnairedatashow();
});
</script>
</body>
</html>
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               