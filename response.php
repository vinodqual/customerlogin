<?php include("inc/inc.hd.php");?>
<section id="middleContainer">
  <div class="container-fluid">
    <div class="middlebox">
      <div class="col-md-9">
        <div class="middlebox-left">
          <?php include('inc/inc.healthplan_tab.php'); ?>
          <div class="tab-content ">
            <div class="tab-pane active" id="buyPlan">
            <div class="tab-paneIn">
              <div class="col-md-12">
			  <?php
			  if(empty($_REQUEST)){
			  	echo "<script>window.location.href='dashboard.php';</script>";
			  }
               $totalData=json_encode($_REQUEST);
			   $createdTime=date('d:m:Y h:i:s',time());
			   	  if($_SESSION['LOGINTYPE']=='CORPORATE'){
				  $sqlAll="INSERT INTO CRMCORPORATEPURCHASE (PURCHASEID,DATA,CREATEDON,CREATEDBY) VALUES ('".$_SESSION['PURCHASEPLANID']."','".$totalData."','".$createdTime."','".@$_SESSION['userName']."') ";				//query to update records 
				  }
			   	  if($_SESSION['LOGINTYPE']=='RETAIL'){
				  $sqlAll="INSERT INTO CRMRETAILPURCHASE (PURCHASEID,DATA,CREATEDON,CREATEDBY) VALUES ('".$_SESSION['PURCHASEPLANID']."','".$totalData."','".$createdTime."','".@$_SESSION['userName']."') ";				//query to update records 
				  }
                  $stdidAll = @oci_parse($conn, $sqlAll);
                  $rAll = @oci_execute($stdidAll);


               $txnid1=explode("_",@$_REQUEST['txnid']);
            
              $payuId_mihpayid = @$_REQUEST['mihpayid'];
              $Order_Id=@$txnid1[1];
                $_SESSION['PURCHASEPLANID']=@$Order_Id;
				if($_SESSION['LOGINTYPE']=='CORPORATE'){
                	$purchasePlanDetails=getPurchasedPlanDetails(@$_SESSION['PURCHASEPLANID'],@$_SESSION['policyNumber']);
				}
				if($_SESSION['LOGINTYPE']=='RETAIL'){
                	$purchasePlanDetails=getRet2PurchasedPlanDetails(@$_SESSION['PURCHASEPLANID'],@$_SESSION['policyNumber'],@$_SESSION['productId']);
				}
				@$policy=@$_SESSION['policyNumber'];
                $amount = @$purchasePlanDetails[0]['TOTALAMOUNT'];
                $productinfo=@$_POST['productinfo'];
                $Firstname=@$purchasePlanDetails[0]['EMPNAME'];
                $Lastname=@$purchasePlanDetails[0]['EMPNAME'];
                $address1=@$purchasePlanDetails[0]['ADDRESS'];
                $address2="";
                $City="Delhi";
                $State='Delhi';
                $Country='India';
                $Zipcode='111111';
                $Email=@$purchasePlanDetails[0]['EMPEMAILID'];
                //check checksum
            
                $txnid=@$_REQUEST['txnid'];
                $param=trim($salt)."|".trim(@$_REQUEST['status'])."|".trim(@$_REQUEST['udf10'])."|".trim(@$_REQUEST['udf9'])."|".trim(@$_REQUEST['udf8'])."|".trim(@$_REQUEST['udf7'])."|".trim(@$_REQUEST['udf6'])."|".trim(@$_REQUEST['udf5'])."|".trim(@$_REQUEST['udf4'])."|".trim(@$_REQUEST['udf3'])."|".trim(@$_REQUEST['udf2'])."|".trim(@$_REQUEST['udf1'])."|".trim(@$_REQUEST['email'])."|".trim($Firstname)."|".trim($productinfo)."|".trim(@$_REQUEST['amount'])."|".trim($_REQUEST['txnid'])."|".trim($merchantId);
                $Hash=strtolower(hash("sha512", $param));
                if((@$_REQUEST['status']=="success"))	{
                    $check1 = @oci_parse($conn, 'SELECT CRMPURCHASEREF_SEQ.nextval FROM DUAL');
                    @oci_execute($check1);
                    $res=@oci_fetch_assoc($check1);
                    $refNO="RHICL".@$res['NEXTVAL'];
				if($_SESSION['LOGINTYPE']=='CORPORATE'){
                    $purchasePlanDetails=getPurchasedPlanDetails(@$_SESSION['PURCHASEPLANID'],@$_SESSION['policyNumber']); //function to fetch records
                    $healthCheckPlans=getHealChkupPlanDetailsConf(@$wellnessList[0]['POLICYID'],$purchasePlanDetails[0]['PLANID']); //function to fetch records
					  $sql="UPDATE CRMEMPLOYEEPURCHASEPLAN SET AUTHDESC='".@$_REQUEST['status']."',PAYUORDERNO='".@$param."',PAYMENTSTATUS='CONFIRMED',STATUS='CONFIRMED',PAYMENTRECEIVED='YES',REFNO='".@$refNO."',MIHPAYID='".@$payuId_mihpayid."' WHERE PURCHASEPLANID=".$_SESSION['PURCHASEPLANID'];					//query to update records 
				}
				if($_SESSION['LOGINTYPE']=='RETAIL'){
                    $purchasePlanDetails=getRet2PurchasedPlanDetails(@$_SESSION['PURCHASEPLANID'],@$policy,@$_SESSION['productId']); //function to fetch records
                    $healthCheckPlans=getRetHealChkupPlanDetailsConf(@$_SESSION['productId'],$purchasePlanDetails[0]['PLANID']); //function to fetch records
                      $sql="UPDATE CRMRETEMPLOYEEPURCHASEPLAN SET AUTHDESC='".@$_REQUEST['status']."',PAYUORDERNO='".@$param."',PAYMENTSTATUS='CONFIRMED',STATUS='CONFIRMED',PAYMENTRECEIVED='YES',REFNO='".@$refNO."',MIHPAYID='".@$payuId_mihpayid."' WHERE PURCHASEPLANID=".$_SESSION['PURCHASEPLANID'];					//query to update records 
				}
                    $stdid = @oci_parse($conn, $sql);
                    $r = @oci_execute($stdid);
                    
                     if($healthCheckPlans[0]['PAYMENTTYPE']=="PAYFROMSALARY"){
					 
					 echo '<div class="transFailBox"><span>Payment for this plan will get deducted from your salary. Please contact HR Department for more details.<br> Your Reference No. is '.@$purchasePlanDetails[0]['REFNO'].'</div>'; } 
					 if($_SESSION['LOGINTYPE']=='CORPORATE'){
						 $res = sendMailToempForPurchasePlan(@$purchasePlanDetails[0]['EMPEMAILID']);
					 } 
					 if($_SESSION['LOGINTYPE']=='RETAIL'){
						 $res = sendMailToRetempForPurchasePlan(@$purchasePlanDetails[0]['EMPEMAILID']);
					 } 
                    $plannames = @$purchasePlanDetails[0]['PLANNAME']?stripslashes(@$purchasePlanDetails[0]['PLANNAME']):@$purchasePlanDetails[0]['PLANNAME2'];
                            echo '<div class="transFailBox">You have successfully purchased "'.@$plannames.'" health checkup plan. Please refer the details below and schedule
                  your appointment by clicking on "Schedule Appointment" button or to
                  view all other free and purchased plans click on "View Plans"</div>';
                
                }else{
				if($_SESSION['LOGINTYPE']=='CORPORATE'){
                        $sql="UPDATE CRMEMPLOYEEPURCHASEPLAN SET AUTHDESC='".@$_REQUEST['status']."',PAYUORDERNO='".@$param."',MIHPAYID='".@$payuId_mihpayid."',PAYMENTSTATUS='FAILED',PAYMENTRECEIVED='NO' WHERE PURCHASEPLANID=".$_SESSION['PURCHASEPLANID']; //query to update records 
				}
				if($_SESSION['LOGINTYPE']=='RETAIL'){
                        $sql="UPDATE CRMRETEMPLOYEEPURCHASEPLAN SET AUTHDESC='".@$_REQUEST['status']."',PAYUORDERNO='".@$param."',MIHPAYID='".@$payuId_mihpayid."',PAYMENTSTATUS='FAILED',PAYMENTRECEIVED='NO' WHERE PURCHASEPLANID=".$_SESSION['PURCHASEPLANID']; //query to update records 
				}
                    $stdid = @oci_parse($conn, $sql);
                    $r = @oci_execute($stdid);
				if($_SESSION['LOGINTYPE']=='CORPORATE'){
					$purchasePlanDetails=getPurchasedPlanDetails(@$_SESSION['PURCHASEPLANID'],@$_SESSION['policyNumber']); //function to fetch records
					$healthCheckPlans=getHealthCheckupPlanDetails(@$wellnessList[0]['POLICYID'],$purchasePlanDetails[0]['PLANID']); //function to fetch records
				}
				if($_SESSION['LOGINTYPE']=='RETAIL'){
                    $purchasePlanDetails=getRet2PurchasedPlanDetails(@$_SESSION['PURCHASEPLANID'],@$policy,@$_SESSION['productId']); //function to fetch records
                    $healthCheckPlans=getRetHealChkupPlanDetailsConf(@$wellnessList[0]['POLICYID'],@$purchasePlanDetails[0]['PLANID']); //function to fetch records
				}
				    ?>
                   <?php if(@$healthCheckPlans[0]['PAYMENTTYPE']=="PAYFROMSALARY"){
				   echo "Payment for this plan will get deducted from your salary. Please contact HR Department for more details.<br> Your Reference No. is ".@$purchasePlanDetails[0]['REFNO']; } 
                    
                            echo '<div class="transFailBox"><span>Transaction Failure !</span><br />
Your transaction has been failed. Please note the transaction ID below.<br />
<strong>Rhicl_</strong><br />
Please retry after some time</div>';
                    
                }
                $_SESSION['PURCHASEPLANIDNEW']=$_SESSION['PURCHASEPLANID'];
                ?>
              </div>
              <div class="clearfix"></div>
              </div>
              <div class="colfullBot">
              <div class="tab-paneIn">
              <div class="col-md-12">
                <div class="title-bg textleft">
                <h1>Plan Details</h1>
                </div>
                <div class="myPlanForm">
                        <table class="responsive" width="100%">
                  <tr>
                    <td>Plan Name</td>
                    <td><strong><?php if(isset($purchasePlanDetails[0]['PLANNAME']) or isset($purchasePlanDetails[0]['PLANNAME2'])){ echo @$purchasePlanDetails[0]['PLANNAME']?stripslashes(@$purchasePlanDetails[0]['PLANNAME']):@$purchasePlanDetails[0]['PLANNAME2']; } else { echo "NA"; } ?></strong></td>
                  </tr>
                  <tr>                    
                    <td>Total Price</td>
                    <td><strong><?php echo @$purchasePlanDetails[0]['COST']?stripslashes(@$purchasePlanDetails[0]['COST']):'NA'; ?></strong></td>
                  </tr>
                  <tr>                    
                    <td>Purchased For</td>
                    <td>
					  <?php 
					if($_SESSION['LOGINTYPE']=='CORPORATE'){                     
						  $memberList=getHealthCheckupPlanMemberDetails(@$_SESSION['PURCHASEPLANID'],@$purchasePlanDetails[0]['PLANID']);
					}
					if($_SESSION['LOGINTYPE']=='RETAIL'){                     
						  $memberList=getRetHealthCheckupPlanMemberDetails(@$_SESSION['PURCHASEPLANID'],@$purchasePlanDetails[0]['PLANID']);
					}
                        $memberName = array();
                         $m=0;
                          while($m<count($memberList)){
                            $memberName[] = ucwords($relationArray[$memberList[$m]['RELATION']]);
                         $m++; }
                        
                       ?>
                     <input name="textfield" type="text" class="txtfield_app" id="textfield" value="<?php echo (count(@$memberName) > 0)?implode(',',@$memberName):'NA'; ?>">
                    </td>
                  </tr>    
                </table>
                    </div>
                    <div class="myPlanBtn topMrgn text-right">
                <a href="javascript:void(0);" onClick="window.location.href='health_checkup.php?tab=bXlwbGFu'" class="submit-btn">View Plan &gt;</a>
                <?php if(@$_REQUEST['status']=='success'){ ?>
                <a href="javascript:void(0);" onClick="window.location='appointment_request_scheduler.php?id=<?php echo base64_encode(@$purchasePlanDetails[0]['PLANID']); ?>&pId=<?php echo base64_encode(@$_SESSION['PURCHASEPLANID']); ?>'" class="submit-btn">Schedule Appointment Now &gt;</a>
                <?php } ?>
              <div class="clearfix"></div></div>  
              </div>
              <div class="clearfix"></div>
              </div>
              </div>
            </div>
            
            
          </div>
        </div>
      </div>
        <div class="col-md-3">
            <?php include("inc/inc.right.php"); ?>
        </div> 
    </div>
  </div>
</section>
<?php include("inc/inc.ft.php"); ?>