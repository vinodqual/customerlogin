<?php /*****************************************************************************
* COPYRIGHT
* Copyright 2013 Catabatic Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: buy_plan.php,v 1.0 2015/09/30 11:11:0 Amit Kumar Dubey Exp $
* $Author: Amit Kumar Dubey $
*
****************************************************************************/
ob_clean();
include("inc/inc.hd.php");  			//include header file to show header on page

if(isset($_SESSION['response_error'])){
	$responseError  = $_SESSION['response_error'];
}else{
	$responseArray = $_SESSION['dataArray'];
}

if($_SESSION['LOGINTYPE']=='CORPORATE'){
	$selectedPlanDetails =   fetchListCondsWithColumn('TOTALPRICE,HEALTHCHECKUPPLANID',"CRMEMPLOYEESELECTEDPLAN","where CRMEMPLOYEESELECTEDPLAN.POLICYID='".@$wellnessList[0]['POLICYID']."' AND CRMEMPLOYEESELECTEDPLAN.ID=".@$_SESSION["CURRENTPLANID"]." AND CRMEMPLOYEESELECTEDPLAN.EMAILID='".@$_SESSION["userName"]."' ");
	$planId=sanitize_data(base64_decode(@$_REQUEST['id']));
	$healthCheckPlans=getHealChkupPlanDetailsConf(@$wellnessList[0]['POLICYID'],$planId);
} else {
	$selectedPlanDetails =   fetchListCondsWithColumn('TOTALPRICE,HEALTHCHECKUPPLANID',"CRMRETEMPLOYEESELECTEDPLAN","where CRMRETEMPLOYEESELECTEDPLAN.PRODUCTID='".@$_SESSION['productId']."' AND CRMRETEMPLOYEESELECTEDPLAN.ID=".@$_SESSION["CURRENTPLANID"]." AND CRMRETEMPLOYEESELECTEDPLAN.EMAILID='".@$_SESSION["userName"]."' ");
	$planId=sanitize_data(base64_decode(@$_REQUEST['id']));
	$healthCheckPlans=getRetHealChkupPlanDetailsConf(@$_SESSION['productId'],$planId);
}



$AddPlan=sanitize_title(@$_POST['AddPlan']);

if($_SESSION['LOGINTYPE']=='CORPORATE'){
	if($AddPlan!='' && isset($AddPlan)){
	$emailId=sanitize_title(@$_POST['emailId']);
	$contactNumber=sanitize_title(@$_POST['contactNumber']);
	$totalSumInputval=sanitize_title(@$_POST['totalSumInputval']);
	$address=sanitize_title(@$_POST['address']);
	$entryTime=date('d-m-Y H:i');
	 		 $sql1w="UPDATE CRMEMPLOYEESELECTEDPLAN SET TOTALPRICE='".@$totalSumInputval."' WHERE ID=".$_SESSION["CURRENTPLANID"]."";
			$stdid1w = @oci_parse($conn, $sql1w);
			$rw = @oci_execute($stdid1w);
		if(@$healthCheckPlans[0]['PLANLIMITS']=="ONLYEMPLOYEES"){
			$cost=@$healthCheckPlans[0]['COST']?@$healthCheckPlans[0]['COST']:$totalSumInputval;
	 		$sql1="INSERT INTO CRMEMPLOYEEPURCHASEPLAN (PURCHASEPLANID,COMPANYID,PLANID,PLANNAMEID,PLANTYPEID,EMPLOYEEID,CLIENTID,PURCHASEDATE,PLANNAME,GENDER,IPADDRESS,POLICYID,POLICYNUMBER,PAYMENTSTATUS,PAYMENTTYPE,EMPEMAILID,EMPNAME,EMPMOBILE,COST,CREATEDON,ADDRESS,TOTALAMOUNT,PAYMENTRECEIVED) values(CRMEMPLOYEEPURCHASEPLAN_SEQ.nextval,'".@$_SESSION['COMPANYID']."','".@$healthCheckPlans[0]['PLANID']."',".@$healthCheckPlans[0]['PLANNAMEID'].",".@$healthCheckPlans[0]['PLANTYPEID'].",'".@$_SESSION['empId']."','".@$_SESSION['clientNumber']."','".date('d-M-Y')."','".@$healthCheckPlans[0]['PLANNAME2']."','".@$_SESSION['GENDER']."','".@$_SERVER['REMOTE_ADDR']."','".@$wellnessList[0]['POLICYID']."','".@$_SESSION['policyNumber']."','PENDING','".@$healthCheckPlans[0]['PAYMENTTYPE']."','".@$emailId."','".@$_SESSION['policyHolderName']."','".@$contactNumber."','".@$totalSumInputval."','".@$entryTime."','".addslashes($address)."','".$cost."','NO')";
			$stdid1 = @oci_parse($conn, $sql1);
			$r = @oci_execute($stdid1);
			
			$check1 = @oci_parse($conn, 'SELECT CRMEMPLOYEEPURCHASEPLAN_SEQ.currval FROM DUAL');
			@oci_execute($check1);
			$res=@oci_fetch_assoc($check1);
			$rowid=@$res['CURRVAL'];
			$_SESSION['PURCHASEPLANID']=$rowid;
			//$res = sendMailToempForPurchasePlan($emailId);
			$sqlfamily="INSERT INTO CRMEMPLOYEEPLANMEMBER (MEMBERID,PURCHASEPLANID,GENDER,RELATION,TITLE,NAME,AGE,PLANID,EMPLOYEEID,CLIENTID,COMPANYID) values(CRMEMPLOYEEPLANMEMBER_SEQ.nextval,'".@$rowid."','".@$_SESSION['GENDER']."','".trim($_SESSION['policyHolderMember'])."','','".@$_SESSION['policyHolderName']."','".@$_SESSION['policyHolderAge']."','".@$healthCheckPlans[0]['PLANID']."','".@$_SESSION['empNo']."','".@$_SESSION['clientNumber']."','".@$COMPANYID."')";
			$stdidF = @oci_parse($conn, $sqlfamily);
			$r1 = @oci_execute($stdidF);
			echo "<script>window.location.href='confirm_plan.php'</script>";
			//header("Location: confirm_plan.php");
			exit;
		}else if(@$healthCheckPlans[0]['PLANLIMITS']=="FAMILYALLOWED" || @$healthCheckPlans[0]['PLANLIMITS']=="BOTH" ){
		
					if(count(@$_POST['member'])>0){
			  $TOTALAMOUNT=@$healthCheckPlans[0]['COST']?@$healthCheckPlans[0]['COST']*count(@$_POST['member']):$totalSumInputval;
		 	  $sql21="INSERT INTO CRMEMPLOYEEPURCHASEPLAN (PURCHASEPLANID,COMPANYID,PLANID,PLANNAMEID,PLANTYPEID,EMPLOYEEID,CLIENTID,PURCHASEDATE,PLANNAME,GENDER,IPADDRESS,POLICYID,POLICYNUMBER,PAYMENTSTATUS,PAYMENTTYPE,EMPEMAILID,EMPNAME,EMPMOBILE,COST,CREATEDON,ADDRESS,TOTALAMOUNT,PAYMENTRECEIVED) values(CRMEMPLOYEEPURCHASEPLAN_SEQ.nextval,'".@$_SESSION['COMPANYID']."','".@$healthCheckPlans[0]['PLANID']."',".@$healthCheckPlans[0]['PLANNAMEID'].",".@$healthCheckPlans[0]['PLANTYPEID'].",'".@$_SESSION['empNo']."','".@$_SESSION['clientNumber']."','".date('d-M-Y')."','".@$healthCheckPlans[0]['PLANNAME']."','".@$_SESSION['GENDER']."','".@$_SERVER['REMOTE_ADDR']."','".@$wellnessList[0]['POLICYID']."','".@$_SESSION['policyNumber']."','PENDING','".@$healthCheckPlans[0]['PAYMENTTYPE']."','".@$emailId."','".@$_SESSION['policyHolderName']."','".@$contactNumber."','".@$totalSumInputval."','".@$entryTime."','".addslashes($address)."','".$TOTALAMOUNT."','NO')"; 
			$stdid21 = @oci_parse($conn, $sql21);
			@oci_execute($stdid21);
			
			$check1 = @oci_parse($conn, 'SELECT CRMEMPLOYEEPURCHASEPLAN_SEQ.currval FROM DUAL');
			@oci_execute($check1);
			$res=@oci_fetch_assoc($check1);
			$rowid=@$res['CURRVAL'];
			$_SESSION['PURCHASEPLANID']=$rowid;
			//$res = sendMailToempForPurchasePlan($emailId);
				$rf1=0;
				foreach($responseArray as $key => $response){
					for($m=0;$m<count(@$_POST['member']);$m++){
						if($rf1==@$_POST['member'][$m]){				
								if(trim($response['BGEN-GENDER']['#text'])=="F"){	$gender="Female";		}
								if(trim($response['BGEN-GENDER']['#text'])=="M"){	$gender="Male";			}
									$sqlfamily="INSERT INTO   CRMEMPLOYEEPLANMEMBER (MEMBERID,PURCHASEPLANID,GENDER,RELATION,TITLE,NAME,AGE,PLANID,EMPLOYEEID,CLIENTID,COMPANYID) values(CRMEMPLOYEEPLANMEMBER_SEQ.nextval,'".@$rowid."','".@$gender."','".trim($response['BGEN-DPNTTYP']['#text'])."','','".ucwords(strtolower(trim($response['BGEN-MEMNAME']['#text'])))."','".ucwords(strtolower(trim($response['BGEN-AGE']['#text'])))."','".@$healthCheckPlans[0]['PLANID']."','".@$_SESSION['empNo']."','".trim($response['BGEN-CLNTNM']['#text'])."','".@$_SESSION['COMPANYID']."')";
									$stdidF = @oci_parse($conn, $sqlfamily);
									@oci_execute($stdidF);
						}				
					}
					$rf1++;				
				}
				echo "<script>window.location.href='confirm_plan.php'</script>";
				//header("Location: confirm_plan.php");
			exit;
		}else{
			$msg="try after sometimes";
		}
		}else{
		}
	}

} else {
	if($AddPlan!='' && isset($AddPlan)){
	$emailId=sanitize_title(@$_POST['emailId']);
	$contactNumber=sanitize_title(@$_POST['contactNumber']);
	$totalSumInputval=sanitize_title(@$_POST['totalSumInputval']);
	$address=sanitize_title(@$_POST['address']);
	$entryTime=date('d-m-Y H:i');


	 		  $sql1w="UPDATE CRMRETEMPLOYEESELECTEDPLAN SET TOTALPRICE='".@$totalSumInputval."' WHERE ID=".$_SESSION["CURRENTPLANID"].""; 
			$stdid1w = @oci_parse($conn, $sql1w);
			$rw = @oci_execute($stdid1w);

		if(@$healthCheckPlans[0]['PLANLIMITS']=="ONLYEMPLOYEES"){
		$cost=@$healthCheckPlans[0]['COST']?@$healthCheckPlans[0]['COST']:$totalSumInputval;
	 		 $sql1="INSERT INTO CRMRETEMPLOYEEPURCHASEPLAN (PURCHASEPLANID,PRODUCTID,PLANID,PLANNAMEID,PLANTYPEID,EMPLOYEEID,CLIENTID,PURCHASEDATE,PLANNAME,GENDER,IPADDRESS,POLICYID,POLICYNUMBER,PAYMENTSTATUS,PAYMENTTYPE,EMPEMAILID,EMPNAME,EMPMOBILE,COST,CREATEDON,ADDRESS,TOTALAMOUNT,PAYMENTRECEIVED) values(CRMRETEMPLOYEEPURCHASEPLAN_SEQ.nextval,'".@$_SESSION['productId']."','".@$healthCheckPlans[0]['PLANID']."',".@$selectedPlanDetails[0]['HEALTHCHECKUPPLANID'].",'','".@$_SESSION['empId']."','".@$_SESSION['customerId']."','".date('d-M-Y')."','".@$healthCheckPlans[0]['PLANNAME2']."','".@$_SESSION['GENDER']."','".@$_SERVER['REMOTE_ADDR']."','".@$wellnessList[0]['POLICYID']."','".@$_SESSION['policyNumber']."','PENDING','".@$healthCheckPlans[0]['PAYMENTTYPE']."','".@$emailId."','".@$_SESSION['policyHolderName']."','".@$contactNumber."','".@$totalSumInputval."','".@$entryTime."','".addslashes($address)."','".@$cost."','NO')"; 
			$stdid1 = @oci_parse($conn, $sql1);
			$r = @oci_execute($stdid1);
			
			$check1 = @oci_parse($conn, 'SELECT CRMRETEMPLOYEEPURCHASEPLAN_SEQ.currval FROM DUAL');
			@oci_execute($check1);
			$res=@oci_fetch_assoc($check1);
			$rowid=@$res['CURRVAL'];
			$_SESSION['PURCHASEPLANID']=$rowid;
			//$res = sendMailToempForPurchasePlan($emailId);
			$sqlfamily="INSERT INTO CRMRETEMPLOYEEPLANMEMBER (MEMBERID,PURCHASEPLANID,GENDER,RELATION,TITLE,NAME,AGE,PLANID,EMPLOYEEID,CLIENTID,PRODUCTID) values(CRMEMPLOYEEPLANMEMBER_SEQ.nextval,'".@$rowid."','".@$_SESSION['GENDER']."','".trim($_SESSION['policyHolderMember'])."','','".@$_SESSION['policyHolderName']."','".@$_SESSION['policyHolderAge']."','".@$healthCheckPlans[0]['PLANID']."','".@$_SESSION['empNo']."','".@$_SESSION['customerId']."','".@$_SESSION['productId']."')";
			$stdidF = @oci_parse($conn, $sqlfamily);
			$r1 = @oci_execute($stdidF);
			echo "<script>window.location.href='confirm_plan.php'</script>";
			//header("Location: confirm_plan.php");
			exit;
		}else if(@$healthCheckPlans[0]['PLANLIMITS']=="FAMILYALLOWED" || @$healthCheckPlans[0]['PLANLIMITS']=="BOTH" ){
		
					if(count(@$_POST['member'])>0){
			  $TOTALAMOUNT=@$healthCheckPlans[0]['COST']?@$healthCheckPlans[0]['COST']*count(@$_POST['member']):$totalSumInputval;
		 	  $sql21="INSERT INTO CRMRETEMPLOYEEPURCHASEPLAN (PURCHASEPLANID,PRODUCTID,PLANID,PLANNAMEID,PLANTYPEID,EMPLOYEEID,CLIENTID,PURCHASEDATE,PLANNAME,GENDER,IPADDRESS,POLICYID,POLICYNUMBER,PAYMENTSTATUS,PAYMENTTYPE,EMPEMAILID,EMPNAME,EMPMOBILE,COST,CREATEDON,ADDRESS,TOTALAMOUNT,PAYMENTRECEIVED) values(CRMRETEMPLOYEEPURCHASEPLAN_SEQ.nextval,'".@$_SESSION['productId']."','".@$healthCheckPlans[0]['PLANID']."','".@$selectedPlanDetails[0]['HEALTHCHECKUPPLANID']."','','".@$_SESSION['empNo']."','".@$_SESSION['customerId']."','".date('d-M-Y')."','".@$healthCheckPlans[0]['PLANNAME']."','".@$_SESSION['GENDER']."','".@$_SERVER['REMOTE_ADDR']."','".@$wellnessList[0]['POLICYID']."','".@$_SESSION['policyNumber']."','PENDING','".@$healthCheckPlans[0]['PAYMENTTYPE']."','".@$emailId."','".@$_SESSION['policyHolderName']."','".@$contactNumber."','".@$totalSumInputval."','".@$entryTime."','".addslashes($address)."','".@$totalSumInputval."','NO')"; 
			$stdid21 = @oci_parse($conn, $sql21);
			@oci_execute($stdid21);
			
			$check1 = @oci_parse($conn, 'SELECT CRMRETEMPLOYEEPURCHASEPLAN_SEQ.currval FROM DUAL');
			@oci_execute($check1);
			$res=@oci_fetch_assoc($check1);
			$rowid=@$res['CURRVAL'];
			$_SESSION['PURCHASEPLANID']=$rowid;
			//$res = sendMailToempForPurchasePlan($emailId);
				$rf1=0;
				foreach($responseArray as $key => $response){
					for($m=0;$m<count(@$_POST['member']);$m++){
						if($rf1==@$_POST['member'][$m]){				
								if(trim($response['BGEN-GENDER']['#text'])=="F"){	$gender="Female";		}
								if(trim($response['BGEN-GENDER']['#text'])=="M"){	$gender="Male";			}
									$sqlfamily="INSERT INTO   CRMRETEMPLOYEEPLANMEMBER (MEMBERID,PURCHASEPLANID,GENDER,RELATION,TITLE,NAME,AGE,PLANID,EMPLOYEEID,CLIENTID,PRODUCTID) values(CRMRETEMPLOYEEPLANMEMBER_SEQ.nextval,'".@$rowid."','".@$gender."','".trim($response['BGEN-DPNTTYP']['#text'])."','','".ucwords(strtolower(trim($response['BGEN-MEMNAME']['#text'])))."','".ucwords(strtolower(trim($response['BGEN-AGE']['#text'])))."','".@$healthCheckPlans[0]['PLANID']."','".@$_SESSION['empNo']."','".trim($response['BGEN-CLNTNM']['#text'])."','".@$_SESSION['productId']."')";
									$stdidF = @oci_parse($conn, $sqlfamily);
									@oci_execute($stdidF);
						}				
					}
					$rf1++;				
				}
				echo "<script>window.location.href='confirm_plan.php'</script>";
				//header("Location: confirm_plan.php");
			exit;
		}else{
			$msg="try after sometimes";
		}
		}else{
		}
	}
}
$a=0;
?>
<script>
function validateBuyPlan(){
		if(document.buyplan.emailId.value==""){
			alert("Please Enter the Email Id");
			document.buyplan.emailId.focus();
			return false;
		}else{
		if (!isEmail(document.buyplan.emailId.value))
			{
				alert("Please enter the Email address in the proper Format");
				document.buyplan.emailId.focus( );	
				return false;
			}
		}
		if(document.buyplan.contactNumber.value==""){
			alert("Please enter the contact number");
			document.buyplan.contactNumber.focus();
			return false;
		}
		if(document.buyplan.address.value=="" || document.buyplan.address.value=="Enter Address"){
			alert("Please enter the address");
			document.buyplan.address.focus();
			return false;
		}
		var checkedLength=numberOfSelectedCheckbox('memberc');
		if(checkedLength==0){		
			alert("Please select the member");
			return false;
		}else{
			document.buyplan.submit();
		}
}
function addtotalprice(){
	
    var total = 0;
    $("input[type=checkbox]:checked").each(function() {
        total += parseFloat($(this).attr('sums'));
    });
	total=total.toFixed(1);
    $(".totalSum").html(total);
    $(".totalSumInputval").val(total);
	
}
function numberOfSelectedCheckbox(className)
 {
 var selector_checked = $("input[class="+className+"]:checked").length;
 return selector_checked;
 }

</script>
<section id="middleContainer">
  <div class="container-fluid">
    <div class="middlebox">
      <div class="col-md-9">
        <div class="middlebox-left">
          <?php include('inc/inc.healthplan_tab.php'); ?>
          <div class="tab-content">
            <div class="tab-pane active" id="buyPlan">
            
            <div class="tab-paneIn">
              <div class="col-md-6">
                <div class="tab-left-container">
                  <h1>Buy Plan (<?php echo @$healthCheckPlans[0]['PLANNAME']?@$healthCheckPlans[0]['PLANNAME']:@$healthCheckPlans[0]['PLANNAME2']; ?>)</h1>
                </div>
              </div>
              <div class="col-md-6">
                <div class="stepProgress">
                  <div class="stepProgressBar"></div>
                  <div class="stepProgressIn"><span class="stepactive">1</span><span class="stepTxt">Select Plan</span></div>
                  <div class="stepProgressIn"><span class="stepactive">2</span><span class="stepTxt">Enter Detail</span></div>
                  <div class="stepProgressIn"><span class="stepBar">3</span><span class="stepTxt">Review</span></div>
                  <div class="stepProgressIn"><span class="stepBar">4</span><span class="stepTxt">Payment</span></div>
                  <div class="cl"></div>
                </div>
              </div>
              
              <div class="col-md-12 topMrgn">
              <form  name="buyplan" method="post" id="buyplan" >
              	<div class="grayBorder">
                	<h2>User Details</h2>
                    <div class="myPlanForm">
                    
                    	<div class="myPlanformBox30per myPlanLeft mrgnMyPlan">
                        	<label>Client No./Emp No. <span class="greenTxt">*</span></label>
                            <div class="inputBox inputBoxcolor">
                            	<div class="inputBoxIn">
                            	  <?php 
     								if($_SESSION['LOGINTYPE']=='CORPORATE'){
									  echo @$_SESSION['clientNumber']?@$_SESSION['clientNumber']:'NA';
									} else {
									  echo @$_SESSION['customerId']?@$_SESSION['customerId']:'NA';
									}
								  ?>
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        
                        
                        <div class="myPlanformBox30per myPlanLeft">
                        	<label>Name <span class="greenTxt">*</span></label>
                            <div class="inputBox inputBoxcolor">
                            	<div class="inputBoxIn">
                                	<?php echo @$_SESSION['employeeName']?@$_SESSION['employeeName']:'NA';?>
                                </div>
                            </div>
                        <div class="clearfix"></div></div>
                        
                        
                        <div class="myPlanformBox30per myPlanRight">
                        	<label>Contact No <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                                	<input type="text" name="contactNumber" id="contactNumber" maxlength="10" AUTOCOMPLETE="OFF"  onkeypress="return kp_numeric(event)"  class="txtField" value="<?php echo @$_POST['contactNumber']?sanitize_user_name(@$_POST['contactNumber']):@$employeeDetails[0]['MOBILENUMBER']; ?>">
                                </div>
                            </div>
                        <div class="clearfix"></div></div>
                        
                        
                        <div class="myPlanformBox30per myPlanLeft">
                        	<label>Email Id <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                            	  <input type="text" name="emailId" id="emailId" maxlength="200" AUTOCOMPLETE="OFF" class="txtField" value="<?php echo @$_POST['emailId']?sanitize_user_name(@$_POST['emailId']):@$_SESSION['userName']; ?>" >
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        
                        
                        <div class="myPlanformBox60per myPlanRight">
                        	<label>Address <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                                	<input type="text" name="address" id="address" class="txtField" value="<?php echo @$_POST['address']?sanitize_data(stripslashes(@$_POST['address'])):''; ?>">
                                </div>
                            </div>
                        <div class="clearfix"></div></div>
                        
                        
                    <div class="clearfix"></div></div>
                    <h2>sELECT mEMBER(S)</h2>
                    <?php if($_SESSION['LOGINTYPE']=='CORPORATE'){?>
                    <div class="myPlanForm">
					  <?php
                     $membersprice = json_decode($healthCheckPlans[0]['EDITEDPRICE'],true);
					 $discountprice = @$healthCheckPlans[0]['DISCOUNTPRICE']?json_decode(@$healthCheckPlans[0]['DISCOUNTPRICE'],true):''; 
					 
                      if(count($membersprice)>0){
                        $membersprice = @array_filter($membersprice);
                        $discountprice = @array_filter($discountprice);
						if(count($discountprice) > 0){
                        $membersprice = $discountprice;
						}
                        $array_ke = array_keys($membersprice);
                        ksort($array_ke, SORT_NUMERIC);
                        
                        $membersids = (implode(',',$array_ke));
                        $membersname = fetchByManyIds('TITLE','CRMCORPMEMBERTYPE',' WHERE MEMBERTYPEID IN ('.@$membersids.') order by MEMBERTYPEID ASC');
                        $memberspricewithname = array_merge(array_values($membersprice),array_values($membersname));
                        $membersnamewithprice = array_combine($membersname,array_values($membersprice));
						// for doscount///\
                        $discountprice = @array_filter($discountprice);
						if(count($discountprice) > 0){
                        $discountarray_ke = array_keys($discountprice);
                        ksort($discountarray_ke, SORT_NUMERIC);
                        
                        $discountmembersids = (implode(',',$discountarray_ke));
                        $discountmembersname = fetchByManyIds('TITLE','CRMCORPMEMBERTYPE',' WHERE MEMBERTYPEID IN ('.@$membersids.') order by MEMBERTYPEID ASC');
                        @$discountmemberspricewithname = array_merge(array_values($discountmembersprice),array_values($discountmembersname));
                        @$discountmembersnamewithprice = array_combine($discountmembersname,array_values($discountmembersprice));
						}
						// for doscount///\
                        
                      }
					  
                      if(@$healthCheckPlans[0]['PLANLIMITS']=="ONLYEMPLOYEES"){
                            // for display price in each column
                            
                                if(strtolower($relationArray[trim($_SESSION['policyHolderMember'])]) == 'primary member'){
                                        $membersname[]='primary member';
                                        $membersnamewithprice['primary member']=$membersnamewithprice['self'];
                                        } 
                                
                                foreach($membersname as $key=>$values){
                                
                                 $membersnamewithprice[$values];
                                
                                }
                                if(array_key_exists(strtolower($relationArray[trim($_SESSION['policyHolderMember'])]),$membersnamewithprice)){
                                    $price = $membersnamewithprice[strtolower($relationArray[trim($_SESSION['policyHolderMember'])])];
                                } else {
                                $price = 0;
                                }
                        
                        // for display price in each column
                      
                     
                      ?>

				        <table class="responsive" width="100%">
                          <tr>
                            <th>Add</th>
                            <th>Gender</th>
                            <th>Relation</th>
                            <th>Name</th>
                            <th>Age</th>
                            <th>Price</th>
                          </tr>
                          <tr>
                            <td> 
                            <input sums="<?php echo floatval($price); ?>" onClick="addtotalprice(<?php echo floatval($price); ?>);"  type="checkbox" name="m" id="test<?php echo @$au; ?>" class="memberc" > 
                                   <label for="test<?php echo @$au; ?>"></label>
                                   </td>
                            <td><?php echo ucwords(@$_SESSION['GENDER']);?><input name="gender" readonly="yes" type="hidden" maxlength="40" class="txtfield_gender" id="textfield" value="<?php echo ucwords(@$_SESSION['GENDER']);?>"></td>
                            <td><?php echo ucwords($relationArray[trim($_SESSION['policyHolderMember'])]);?><input name="relationshipType"  readonly="yes" type="hidden" maxlength="100" class="txtfield_app" id="textfield" value="<?php echo ucwords($relationArray[trim($_SESSION['policyHolderMember'])]);?>" style="width:135px;"></td>
                            <td><strong class="greenGeneralTxt"><?php echo @$_SESSION['policyHolderName'];?><input name="name" readonly="yes" type="hidden" maxlength="3" class="txtfield_app" id="textfield" style="width:135px;" value="<?php echo @$_SESSION['policyHolderName'];?>"></strong></td>
                            <td><?php echo @$_SESSION['policyHolderAge'];?><input name="age" readonly="yes" type="hidden" maxlength="3" class="txtfield_gender" id="textfield" style="width:135px;" value="<?php echo @$_SESSION['policyHolderAge'];?>"></td>
                            <td><cite class="WebRupee">Rs.</cite> <?php echo @$price; ?>/-</td>
                          </tr>
                        </table>
				      <?php } else { ?>
				        <table class="responsive" width="100%">
                          <tr>
                            <th>Add</th>
                            <th>Gender</th>
                            <th>Relation</th>
                            <th>Name</th>
                            <th>Age</th>
                            <th>Price</th>
                          </tr>
						  <?php 
                        if((isset($responseArray[0]['BGEN-MEMNAME']['#text'])) && ($responseArray[0]['BGEN-MEMNAME']['#text'] != "")){
                        $g=0;
                        foreach($responseArray as $key => $response){
                                $planDisplay=0;
                                if(trim($response['BGEN-GENDER']['#text'])=="F"){
                                    $gender="Female";
                                }
                                if(trim($response['BGEN-GENDER']['#text'])=="M"){
                                    $gender="Male";
                                }
                        if(strtolower($gender)==strtolower(@$healthCheckPlans[$a]['GENDER']) || strtolower(@$healthCheckPlans[$a]['GENDER'])=="both"){
                                     $planDisplay++;
                                }
                                
                        if($key == 0 ){
							
                        if(@$healthCheckPlans[$a]['PLANLIMITS']=="BOTH"){				
                                $ageName1="";
								
                                $pieces = explode(',' , $healthCheckPlans[$a]['AGEGROUPID']);
                                $policyHolderAge=str_replace(",","",number_format(trim($response['BGEN-AGE']['#text'])));
                                for($j=0;$j<count(@$pieces);$j++) {
									// age group according to new
										$ageName1.=$pieces[$j].", ";
                                        if($policyHolderAge == $pieces[$j]){
                                                    $planDisplay++;
                                                    break;
                                        }
                                   }
                               
                                $siName1="";
                                $si_list = explode(',' , $healthCheckPlans[$a]['SI']);
                                 $policyHolderSI=str_replace(",","",number_format(@$_SESSION['policyHolderSI']));
                                for($j=0;$j<count($si_list);$j++) {
                                        $siName=fetchListById('CRMSI','SIID',$si_list[$j]); 
                                        $siName1.=$siName[0]['SI'].", ";
                                        if($siName[0]['SI']==$policyHolderSI){
                                            $planDisplay++;
                                        }
                                }
                                
                            if($planDisplay==3){
                            
                            // for display price in each column
                                if(strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]) == 'primary member'){
                                        $membersname[]='primary member';
                                        $membersnamewithprice['primary member']=$membersnamewithprice['self'];
                                        } 
                                
                                foreach($membersname as $key=>$values){
                                
                                 $membersnamewithprice[$values];
                                
                                }
              				
                                if(array_key_exists(strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]),$membersnamewithprice)){
                                    $price = $membersnamewithprice[strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])])];
                                } else {
                                $price = 0;
                                }
                          if($price>0){
                        // for display price in each column
                        ?>
                          <tr>
                            <td> 
                                   <input sums="<?php echo floatval($price); ?>" onClick="addtotalprice(<?php echo floatval($price); ?>);" type="checkbox" name="member[]" value="<?=$g;?>" class="memberc"  id="test<?php echo @$au; ?>" > 
                                   <label for="test<?php echo @$au; ?>"></label>
                                   
                                  </td>
                            <td><?php echo @$gender;?><input name="gender[]" readonly="yes" type="hidden" maxlength="40" class="txtfield_gender" id="textfield" value="<?php echo @$gender;?>" style="width:55px;"></td>
                            <td><?php echo ucwords($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);?> <input name="relationshipType[]"  readonly="yes" type="hidden" maxlength="100" class="txtfield_app" id="textfield" value="<?php echo ucwords($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);?>" style="width:135px;"></td>
                            <td><strong class="greenGeneralTxt"><?php echo ucwords(strtolower(trim($response['BGEN-MEMNAME']['#text'])));?><input name="name[]" readonly="yes" type="hidden" maxlength="3" class="txtfield_app" id="textfield" style="width:135px;" value="<?php echo ucwords(strtolower(trim($response['BGEN-MEMNAME']['#text'])));?>"></strong></td>
                            <td><?php echo(trim($response['BGEN-AGE']['#text']));?><input name="age[]" readonly="yes" type="hidden" maxlength="3" class="txtfield_gender" id="textfield" style="width:135px;" value="<?php echo(trim($response['BGEN-AGE']['#text']));?>"></td>
                            <td><cite class="WebRupee">Rs.</cite> <?php echo $price ?>/-</td>
                          </tr>
						  <?php } 
						  } } } else{ 
                                 $ageName1="";
                                $pieces = explode(',' , $healthCheckPlans[$a]['AGEGROUPID']);
                                $policyHolderAge=str_replace(",","",number_format(trim($response['BGEN-AGE']['#text'])));
                                for($j=0;$j<count(@$pieces);$j++) {
									// age group according to new
										$ageName1.=$pieces[$j].", ";
                                        if($policyHolderAge == $pieces[$j]){
                                                    $planDisplay++;
                                                    break;
                                        }
                                   }
                                $siName1="";
                                $si_list = explode(',' , $healthCheckPlans[$a]['SI']);
                                $policyHolderSI=str_replace(",","",number_format(@$_SESSION['policyHolderSI']));
                                for($j=0;$j<count($si_list);$j++) {
                                        $siName=fetchListById('CRMSI','SIID',$si_list[$j]); 
                                        $siName1.=$siName[0]['SI'].", ";
                                        if($siName[0]['SI']==$policyHolderSI){
                                            $planDisplay++;
                                        }
                                }
                                
                            if($planDisplay==3){
                            // for display price in each column
                                if(strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]) == 'primary member'){
                                        $membersname[]='primary member';
                                        $membersnamewithprice['primary member']=$membersnamewithprice['self'];
                                        } 
                                
                                foreach($membersname as $key=>$values){
                                
                                 $membersnamewithprice[$values];
                                
                                }
                                if(array_key_exists(strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]),$membersnamewithprice)){
                                    $price = $membersnamewithprice[strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])])];
                                } else {
                                $price = 0;
                                }
                        if($price>0){
                        // for display price in each column
                          ?>
                          <tr>
                            <td>
                            <input sums="<?php echo floatval($price); ?>" onClick="addtotalprice(<?php echo floatval($price); ?>);" type="checkbox" name="member[]" value="<?=$g;?>" class="memberc" id="test<?php echo @$au; ?>"> 
                                 <label for="test<?php echo @$au; ?>"></label>
                                 
                                   </td>
                            <td><?php echo @$gender;?> <input name="gender[]" readonly="yes" type="hidden" maxlength="40" class="txtfield_gender" id="textfield" value="<?php echo @$gender;?>" style="width:55px;"></td>
                            <td><?php echo ucwords($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);?><input name="relationshipType[]"  readonly="yes" type="hidden" maxlength="100" class="txtfield_app" id="textfield" value="<?php echo ucwords($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);?>" style="width:135px;"></td>
                            <td><strong class="greenGeneralTxt"><?php echo ucwords(strtolower(trim($response['BGEN-MEMNAME']['#text'])));?> <input name="name[]" readonly="yes" type="hidden" maxlength="3" class="txtfield_app" id="textfield" style="width:135px;" value="<?php echo ucwords(strtolower(trim($response['BGEN-MEMNAME']['#text'])));?>"></strong></td>
                            <td><?php echo(trim($response['BGEN-AGE']['#text']));?><input name="age[]" readonly="yes" type="hidden" maxlength="3" class="txtfield_gender" id="textfield" style="width:135px;" value="<?php echo(trim($response['BGEN-AGE']['#text']));?>"></td>
                            <td><cite class="WebRupee">Rs.</cite> <?php echo @$price; ?>/-</td>
                          </tr>
				   <?php }
				   }
				   }$g++;}} ?>
                        </table>
					   <?php } ?>

                    </div>
                    <?php } else { ?>
                    <div class="myPlanForm">
					  <?php
                     $membersprice = json_decode(@$healthCheckPlans[0]['EDITEDPRICE'],true);
					 $discountprice = @$healthCheckPlans[0]['DISCOUNTPRICE']?json_decode(@$healthCheckPlans[0]['DISCOUNTPRICE'],true):''; 
					
					 $totalprice = 0;
                      if(count($membersprice)>0){
                        $membersprice = array_filter($membersprice);

                        $discountprice = array_filter($discountprice);
						if(count($discountprice) > 0){
                        $membersprice = $discountprice;
						}
                        $array_ke = array_keys($membersprice);
                        ksort($array_ke, SORT_NUMERIC);
                        
                        $membersids = (implode(',',$array_ke));
                        $membersname = fetchByManyIds('TITLE','CRMCORPMEMBERTYPE',' WHERE MEMBERTYPEID IN ('.@$membersids.') order by MEMBERTYPEID ASC');
                        $memberspricewithname = array_merge(array_values($membersprice),array_values($membersname));
                        $membersnamewithprice = array_combine($membersname,array_values($membersprice));
						// for doscount///\
                        $discountprice = array_filter($discountprice);
						if(count($discountprice) > 0){
                        $discountarray_ke = array_keys($discountprice);
                        ksort($discountarray_ke, SORT_NUMERIC);
                        
                        $discountmembersids = (implode(',',$discountarray_ke));
                        $discountmembersname = fetchByManyIds('TITLE','CRMCORPMEMBERTYPE',' WHERE MEMBERTYPEID IN ('.@$membersids.') order by MEMBERTYPEID ASC');
                        @$discountmemberspricewithname = array_merge(array_values($discountmembersprice),array_values($discountmembersname));
                        @$discountmembersnamewithprice = array_combine($discountmembersname,array_values($discountmembersprice));
						}
						// for doscount///\
						                        
                      }
					  
					   //print_r($membersnamewithprice);die;
                      if(@$healthCheckPlans[0]['PLANLIMITS']=="ONLYEMPLOYEES"){
                            // for display price in each column
                            
                                if(strtolower($relationArray[trim($_SESSION['policyHolderMember'])]) == 'primary member'){
                                        $membersname[]='primary member';
                                        $membersnamewithprice['primary member']=$membersnamewithprice['self'];
                                        } 
										
                               
                                foreach($membersname as $key=>$values){
                                
                                 $membersnamewithprice[$values];
                                
                                }
                                if(array_key_exists(strtolower($relationArray[trim($_SESSION['policyHolderMember'])]),$membersnamewithprice)){
                                    $price = $membersnamewithprice[strtolower($relationArray[trim($_SESSION['policyHolderMember'])])];
									
                                } else {
                                $price = 0;
                                }
                        
                        // for display price in each column
                      
                      
                      ?>

				        <table class="responsive" width="100%">
                          <tr>
                            <th>Add</th>
                            <th>Gender</th>
                            <th>Relation</th>
                            <th>Name</th>
                            <th>Age</th>
                            <th>Price</th>
                          </tr>
                          <tr>
                            <td>
                                    <input sums="<?php echo floatval($price); ?>" onClick="addtotalprice(<?php echo floatval($price); ?>);"  type="checkbox" name="member[]" id="test3" class="memberc" checked="true"  />
                                   
                                    </td>
                            <td><?php echo ucwords(@$_SESSION['GENDER']);?><input name="gender" readonly="yes" type="hidden" maxlength="40" class="txtfield_gender" id="textfield" value="<?php echo ucwords(@$_SESSION['GENDER']);?>"></td>
                            <td><?php echo ucwords($relationArray[trim($_SESSION['policyHolderMember'])]);?><input name="relationshipType"  readonly="yes" type="hidden" maxlength="100" class="txtfield_app" id="textfield" value="<?php echo ucwords($relationArray[trim($_SESSION['policyHolderMember'])]);?>" style="width:135px;"></td>
                            <td><strong class="greenGeneralTxt"><?php echo @$_SESSION['policyHolderName'];?><input name="name" readonly="yes" type="hidden" maxlength="3" class="txtfield_app" id="textfield" style="width:135px;" value="<?php echo @$_SESSION['policyHolderName'];?>"></strong></td>
                            <td><?php echo @$_SESSION['policyHolderAge'];?><input name="age" readonly="yes" type="hidden" maxlength="3" class="txtfield_gender" id="textfield" style="width:135px;" value="<?php echo @$_SESSION['policyHolderAge'];?>"></td>
                            <td><cite class="WebRupee">Rs.</cite> <?php echo @$price; ?>/-</td>
                          </tr>
                        </table>
				      <?php } else { ?>
				        <table class="responsive" width="100%">
                          <tr>
                            <th>Add</th>
                            <th>Gender</th>
                            <th>Relation</th>
                            <th>Name</th>
                            <th>Age</th>
                            <th>Price</th>
                          </tr>
							  <?php 
                            if((isset($responseArray[0]['BGEN-MEMNAME']['#text'])) && ($responseArray[0]['BGEN-MEMNAME']['#text'] != "")){
                            $g=0;
							$au=1;
                            foreach($responseArray as $key => $response){
                                    $planDisplay=0;
                                    if(trim($response['BGEN-GENDER']['#text'])=="F"){
                                        $gender="Female";
                                    }
                                    if(trim($response['BGEN-GENDER']['#text'])=="M"){
                                        $gender="Male";
                                    }
                            if(strtolower($gender)==strtolower(@$healthCheckPlans[$a]['GENDER']) || strtolower(@$healthCheckPlans[$a]['GENDER'])=="both"){
                                         $planDisplay++;
                                    }
                                    
                            if($key == 0 ){
							if(@$healthCheckPlans[$a]['PLANLIMITS']=="BOTH"){				
                                    $ageName1="";
                                    $pieces = explode(',' , $healthCheckPlans[$a]['AGEGROUPID']);
									
                                    $policyHolderAge=str_replace(",","",number_format(trim($response['BGEN-AGE']['#text'])));
                                    for($j=0;$j<count($pieces);$j++) {
									// age group according to new
										$ageName1.=$pieces[$j].", ";
                                        if($policyHolderAge == $pieces[$j]){
                                                    $planDisplay++;
                                                    break;
                                        }
                                   }
                                   
                                    $siName1="";
                                    $si_list = explode(',' , $healthCheckPlans[$a]['SI']);
                                     $policyHolderSI=str_replace(",","",number_format(@$_SESSION['policyHolderSI']));
                                    for($j=0;$j<count($si_list);$j++) {
                                            $siName=fetchListById('CRMSI','SIID',$si_list[$j]); 
                                            $siName1.=$siName[0]['SI'].", ";
                                            if($siName[0]['SI']==$policyHolderSI){
                                                $planDisplay++;
                                            }
                                    }
                                    
                                if($planDisplay==3){
                                
                                // for display price in each column
                                    if(strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]) == 'primary member'){
                                            $membersname[]='primary member';
                                            $membersnamewithprice['primary member']=$membersnamewithprice['self'];
                                            } 
                                    
                                    foreach($membersname as $key=>$values){
                                    
                                     $membersnamewithprice[$values];
                                    
                                    }
                                    if(array_key_exists(strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]),$membersnamewithprice)){
                                        $price = $membersnamewithprice[strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])])];
                                    } else {
                                    $price = 0;
                                    }
                            
                            // for display price in each column
                            ?>
                          
                          
                          <tr>
                            <td>
                                    <input sums="<?php echo floatval($price); ?>" onClick="addtotalprice(<?php echo floatval($price); ?>);" type="checkbox" name="member[]" value="<?=$g;?>" class="memberc"  id="test<?php echo @$au; ?>">
                                    <label for="test<?php echo @$au; ?>"></label></td>
                            <td><?php echo @$gender;?><input name="gender[]" readonly="yes" type="hidden" maxlength="40" class="txtfield_gender" id="textfield" value="<?php echo @$gender;?>" style="width:55px;"></td>
                            <td><?php echo ucwords($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);?> <input name="relationshipType[]"  readonly="yes" type="hidden" maxlength="100" class="txtfield_app" id="textfield" value="<?php echo ucwords($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);?>" style="width:135px;"></td>
                            <td><strong class="greenGeneralTxt"><?php echo ucwords(strtolower(trim($response['BGEN-MEMNAME']['#text'])));?><input name="name[]" readonly="yes" type="hidden" maxlength="3" class="txtfield_app" id="textfield" style="width:135px;" value="<?php echo ucwords(strtolower(trim($response['BGEN-MEMNAME']['#text'])));?>"></strong></td>
                            <td><?php echo(trim($response['BGEN-AGE']['#text']));?><input name="age[]" readonly="yes" type="hidden" maxlength="3" class="txtfield_gender" id="textfield" style="width:135px;" value="<?php echo(trim($response['BGEN-AGE']['#text']));?>"></td>
                            <td><cite class="WebRupee">Rs.</cite> <?php echo $price ?>/-</td>
                          </tr>
					  <?php } } } else{ 
                             $ageName1="";
                            $pieces = explode(',' , @$healthCheckPlans[$a]['AGEGROUPID']);
                            $policyHolderAge=str_replace(",","",number_format(trim($response['BGEN-AGE']['#text'])));
                            for($j=0;$j<count($pieces);$j++) {
									// age group according to new
										$ageName1.=$pieces[$j].", ";
                                        if($policyHolderAge == $pieces[$j]){
                                                    $planDisplay++;
                                                    break;
                                        }
                                   }
                            $siName1="";
                            $si_list = explode(',' , @$healthCheckPlans[$a]['SI']);
                            $policyHolderSI=str_replace(",","",number_format(@$_SESSION['policyHolderSI']));
                            for($j=0;$j<count($si_list);$j++) {
                                    $siName=fetchListById('CRMSI','SIID',@$si_list[$j]); 
                                    $siName1.=@$siName[0]['SI'].", ";
                                    if(@$siName[0]['SI']==@$policyHolderSI){
                                        $planDisplay++;
                                    }
                            }
                            
                        if($planDisplay==3){
                        // for display price in each column
                            if(strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]) == 'primary member'){
                                    $membersname[]='primary member';
                                    $membersnamewithprice['primary member']=$membersnamewithprice['self'];
                                    } 
                            
                            foreach($membersname as $key=>$values){
                            
                             $membersnamewithprice[$values];
                            
                            }
                            if(array_key_exists(strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]),$membersnamewithprice)){
                                $price = $membersnamewithprice[strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])])];
                            } else {
                            $price = 0;
                            }
                    
                    // for display price in each column
                      ?>
                          <tr>
                            <td>
                                    <input sums="<?php echo floatval($price); ?>" onClick="addtotalprice(<?php echo floatval($price); ?>);" type="checkbox" name="member[]" value="<?=$g;?>" class="memberc" id="test<?php echo @$au; ?>">
                                    <label for="test<?php echo @$au; ?>"></label>
                                    </td>
                            <td><?php echo @$gender;?> <input name="gender[]" readonly="yes" type="hidden" maxlength="40" class="txtfield_gender" id="textfield" value="<?php echo @$gender;?>" style="width:55px;"></td>
                            <td><?php echo ucwords($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);?><input name="relationshipType[]"  readonly="yes" type="hidden" maxlength="100" class="txtfield_app" id="textfield" value="<?php echo ucwords($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);?>" style="width:135px;"></td>
                            <td><strong class="greenGeneralTxt"><?php echo ucwords(strtolower(trim($response['BGEN-MEMNAME']['#text'])));?> <input name="name[]" readonly="yes" type="hidden" maxlength="3" class="txtfield_app" id="textfield" style="width:135px;" value="<?php echo ucwords(strtolower(trim($response['BGEN-MEMNAME']['#text'])));?>"></strong></td>
                            <td><?php echo(trim($response['BGEN-AGE']['#text']));?><input name="age[]" readonly="yes" type="hidden" maxlength="3" class="txtfield_gender" id="textfield" style="width:135px;" value="<?php echo(trim($response['BGEN-AGE']['#text']));?>"></td>
                            <td><cite class="WebRupee">Rs.</cite> <?php echo @$price; ?>/-</td>
                          </tr>
					   <?php }}$g++; $au++; }} ?>
                        </table>
					   <?php } ?>
                    </div>
					<?php } ?>
                     <span id="membererror"></span>
                    <div class="myPlanBtn">
                    
                    <input type="hidden" name="totalSumInputval" id="totalSumInputval" class="totalSumInputval" value="<?php  if(@$healthCheckPlans[0]['PLANLIMITS']=="ONLYEMPLOYEES"){ echo @$price?@$price:0; } else { echo 0; } ?>">
                    <input type="hidden" name="AddPlan" id="button2" value="Buy Now" class="submitbtn">
                  <a href="javascript:void(0);" onClick="$('#buyplan').submit();" class="submit-btn">Buy Now ></a>
<a href="buy_plan_first.php?pid=<?php echo base64_encode(@$planId); ?>" class="cancel-btn">Cancel ></a>
                        
                    </div>
                   
                </div>
                
              </form>
              </div>
              <div class="clearfix"></div>
              </div>
            </div>
            
            
          </div>
        </div>
        
      </div>
    <div class="col-md-3">
        <?php include("inc/inc.right.php"); ?>
    </div> 
             
    </div>
  </div>
</section>
<?php include("inc/inc.ft.php"); ?>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/additional-methods.min.js" type="text/javascript"></script>
<script>
 $("#buyplan").validate({
    rules :{
		"contactNumber" : {
		   required : true,
		   number: true,
		   maxlength: 10,
		   minlength: 10
        },
        "emailId" : {
            required : true,
			email: true
			//noStartEndWhiteSpaces: true
        },
		"address" : {
		   required : true,
        },
		"member[]" :{
			required : true,
			//specialChars: true
			//noStartEndWhiteSpacesAlpha:true
		}
    },
    messages :{
        "contactNumber" : {
            required : 'Please Enter the Contact No.'
        },
        "emailId" : {
            required : 'Please Enter the Email Id'
        },
		"address" : {
            required : 'Please Enter Address'
        },
		"member[]":{
			required: 'Please select at least one member'
		}
    },
	errorElement: "div",
	errorPlacement: function(error, element) {
     if (element.attr("name") == "member[]") {

       // do whatever you need to place label where you want

         // an example
         error.insertBefore( $("#membererror") );

         // just another example
		 $("#membererror").show(); 
         $("#membererror").html( error );
		     

     } else {

         // the default error placement for the rest
         error.insertAfter(element);

     }
   }	
  }); 
</script>
<style>
div.error {
	position:absolute;
	margin-top:14px;
	color:red;
	background-color:#fff;
	padding:3px;
	text-align:left;
	z-index:1;
	margin-left: -17px;
}
div.membertypeerror .error{
	margin-top:-9px !important;
	margin-left:0px !important;
}
#membererror{
	margin-left:10px;
	margin-top:-45px;
	float:left;
}

</style>