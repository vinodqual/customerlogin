<?php include("inc/inc.hd.php");
?>
<section id="middleContainer">
   <div class="container-fluid">
        <div class="middlebox"> 
            <div class="col-md-9">
               <div class="middlebox-left">
           <?php include('inc/inc.healthplan_tab.php'); ?>
		  <?php if(isset($_GET['message']) && $_GET['message']!=''){?>
			   <div style="width: 100%; height:30px !important; padding:15px; text-align: center; color: green;"><?php echo $_GET['message']; ?></div>
		  <?php } ?>
          <div class="tab-content ">
            <div class="tab-pane active" id="existingAppointments"> 
            <div class="tab-paneIn" id="myPlanList" style="display:block;">
			<?php if($_SESSION['LOGINTYPE']=='CORPORATE'){ ?>
                    <table class="responsive" width="100%">
                      <tr>
                      <th>S&nbsp;No.</th>
                      <th>Plan&nbsp;Name</th>
                      <th>Name</th>
                      <th>Appointment&nbsp;Date&nbsp;/Time</th>
                      <th>Center&nbsp;Name</th>
                      <th>Status</th>
                      <th>Action</th>
                      </tr>
                      <tbody id="myTable">
            <?php $existingAppointmentList=getExistingAppointmentListByEmp(@$_SESSION['empId'],@$_SESSION['policyNumber']);
			//print_r($existingAppointmentList);
		$a=0;
		$k=1;
		if(count(@$existingAppointmentList)>0){
		while($a<count(@$existingAppointmentList)){
		$appointmentDate="";
		$appointmentTime="";
		$slottime = fetchListByColumnName('CRMSLOTMASTER','STARTTIMENAME','SLOTID',@$existingAppointmentList[$a]['SLOTID']);
		if(@$existingAppointmentList[$a]['SELECTEDSLOT']=="slot1"){
			$appointmentDate=@$existingAppointmentList[$a]['DATE1'];
			$appointmentTime=@$slottime[0]['STARTTIMENAME'];
			}
		if(@$existingAppointmentList[$a]['SELECTEDSLOT']=="slot2"){
			$appointmentDate=@$existingAppointmentList[$a]['DATE2'];
			$appointmentTime=$slottime[0]['STARTTIMENAME'];
			}
		$planNamesDet = fetchListByColumnName('CRMCOMPANYHEALTHCHECKUPPLAN','PLANNAMEID','PLANID',@$existingAppointmentList[$a]['HEALTHCHECKUPID']);
		if(isset($planNamesDet) && !empty($planNamesDet)){
			$planNamenew = fetchListByColumnName('CRMCORPHEALTHCHECKUP','HEALTHCHECKUPPLANNAME','HEALTHCHECKUPPLANID',@$planNamesDet[0]['PLANNAMEID']);
		$planNames = @$planNamenew[0]['HEALTHCHECKUPPLANNAME']?stripslashes(@$planNamenew[0]['HEALTHCHECKUPPLANNAME']):'NA';
		} else {
		$planNames = 'NA';
		}
		$reportfile=0;
		if(stripslashes(@$existingAppointmentList[$a]['BLOODREPORTFILE'])!='' || stripslashes(@$existingAppointmentList[$a]['URINEREPORTFILE'])!='' || stripslashes(@$existingAppointmentList[$a]['REPORTFILE'])!=''){
			$reportfile=1;
		}
		?>
            <tr>
              <td><?php echo $k;?>.</td>
              <td><?php echo @$existingAppointmentList[$a]['HEALTHCHECKUPPLAN']?stripslashes(@$existingAppointmentList[$a]['HEALTHCHECKUPPLAN']):$planNames; ?></td>
              <td><?php echo stripslashes(@$existingAppointmentList[$a]['FIRSTNAME']);?></td>
              <td><?php if(@$existingAppointmentList[$a]['STATUS']=="FIXED" || @$existingAppointmentList[$a]['STATUS']=="DONE"){echo @$appointmentDate;?> /<?php echo $appointmentTime; }else { echo @$existingAppointmentList[$a]['DATE1']."/".@$slottime[0]['STARTTIMENAME']; } ?></td>
              <td><?php echo stripslashes(htmlentities(stripslashes($existingAppointmentList[$a]['CENTERNAME']),ENT_QUOTES, "UTF-8"));?></td>
              <td><?php if(@$existingAppointmentList[$a]['STATUS']=='CANCELLED'){ ?>CANCELLED<?php } else { echo stripslashes(@$existingAppointmentList[$a]['STATUS']); } ?></td>
              
              <td title="Download">
			  <?php if($reportfile==1){?>
              <a data-toggle="modal" data-target="#exampleModal1_<?php echo @$existingAppointmentList[$a]['APPOINTMENTID']; ?>" href="view_healthreport_docs.php?id=<?php echo base64_encode(@$existingAppointmentList[$a]['APPOINTMENTID']);?>" class="view_nav"><img src="img/downloadIcon.png" /></a>
                  <div class="modal fade" id="exampleModal1_<?php echo @$existingAppointmentList[$a]['APPOINTMENTID']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <div class="model-header" id="exampleModalLabel">Download Health Report</div>
                      </div>
                     	Please Wait....
                    </div>
                  </div>
                </div>
			  <?php } if(@$existingAppointmentList[$a]['STATUS']=='CANCELLED') { 
			 
			  if($_SESSION['policyexpiredisables']['vas.existing_app.reschedule_appointment_icon']!='NO'){
				  //echo "<a target='_blank' href='reschedule_appointment.php?appId=".base64_encode(@$existingAppointmentList[$a]['APPOINTMENTID'])."&pId=".base64_encode(@$existingAppointmentList[$a]['HEALTHCHECKUPID'])."&tab=".base64_encode('existingapp')."' class=''><img src='img/timeIcon.png' /></a>";
			  } else {
				  //echo "<a href='javascript:void(0);' class=''><img src='img/timeIcon.png' /></a>";
			  }
			  }if(@$existingAppointmentList[$a]['STATUS']=='CANCELLED' && @$existingAppointmentList[$a]['RESCHEDULE']=='1'){ echo "Rescheduled"; } echo ""; ?> &nbsp;</td>
            </tr>
       <?php 
	    $a++;
	 	$k++;
		} 
	   
	   
	   }else{ ?>
       
     	  <tr>
         	 <td colspan="7"  style="text-align:center;">No Appointments</td>
		  </tr>
      <?php } ?>         
            </tbody>
                    </table>
			<?php } else { ?>
                    <table class="responsive" width="100%">
                      <tr>
                      <th>S&nbsp;No.</th>
                      <th>Plan&nbsp;Name</th>
                      <th>Name</th>
                      <th>Appointment&nbsp;Date&nbsp;/Time</th>
                      <th>Center&nbsp;Name</th>
                      <th>Status</th>
                      <th>Action</th>
                      </tr>
                      <tbody id="myTable">
            <?php 
			$existingAppointmentList=getRetExistingAppointmentListByEmp(@$_SESSION['empId'],@$_SESSION['policyNumber']);
		$a=0;
		$k=1;
		if(count(@$existingAppointmentList)>0){
		while($a<count(@$existingAppointmentList)){
		$appointmentDate="";
		$appointmentTime="";
		$slottime = fetchListByColumnName('CRMSLOTMASTER','STARTTIMENAME','SLOTID',@$existingAppointmentList[$a]['SLOTID']);
		if(@$existingAppointmentList[$a]['SELECTEDSLOT']=="slot1"){
			$appointmentDate=@$existingAppointmentList[$a]['DATE1'];
			$appointmentTime=@$slottime[0]['STARTTIMENAME'];
			}
		if(@$existingAppointmentList[$a]['SELECTEDSLOT']=="slot2"){
			$appointmentDate=@$existingAppointmentList[$a]['DATE2'];
			$appointmentTime=@$slottime[0]['STARTTIMENAME'];
			}
		$planNamesDet = fetchListByColumnName('CRMRETAILHEALTHCHECKUPPLAN','PLANNAMEID','PLANID',@$existingAppointmentList[$a]['HEALTHCHECKUPID']);
		if(isset($planNamesDet) && !empty($planNamesDet)){
			$planNamenew = fetchListByColumnName('CRMCORPHEALTHCHECKUP','HEALTHCHECKUPPLANNAME','HEALTHCHECKUPPLANID',@$planNamesDet[0]['PLANNAMEID']);
		$planNames = @$planNamenew[0]['HEALTHCHECKUPPLANNAME']?stripslashes(@$planNamenew[0]['HEALTHCHECKUPPLANNAME']):'NA';
		} else {
		$planNames = 'NA';
		}
		$reportfile=0;
		if(stripslashes(@$existingAppointmentList[$a]['BLOODREPORTFILE'])!='' || stripslashes(@$existingAppointmentList[$a]['URINEREPORTFILE'])!='' || stripslashes(@$existingAppointmentList[$a]['REPORTFILE'])!=''){
			$reportfile=1;
		}
		?>
            <tr>
              <td><?php echo $k;?>.</td>
              <td><?php echo @$existingAppointmentList[$a]['HEALTHCHECKUPPLAN']?stripslashes(@$existingAppointmentList[$a]['HEALTHCHECKUPPLAN']):$planNames; ?></td>
              <td><?php echo stripslashes(@$existingAppointmentList[$a]['FIRSTNAME']);?></td>
              <td><?php if(@$existingAppointmentList[$a]['STATUS']=="FIXED" || @$existingAppointmentList[$a]['STATUS']=="DONE"){echo @$appointmentDate;?> /<?php echo $appointmentTime; } else { echo @$existingAppointmentList[$a]['DATE1']."/".@$slottime[0]['STARTTIMENAME']; } ?></td>
              <td><?php echo stripslashes(htmlentities(stripslashes($existingAppointmentList[$a]['CENTERNAME']),ENT_QUOTES, "UTF-8"));?></td>

              <td><?php if(@$existingAppointmentList[$a]['STATUS']=='CANCELLED'){ ?>CANCELLED<?php } else { echo stripslashes(@$existingAppointmentList[$a]['STATUS']); } ?></td>
              <td title="Download">
               <?php if($reportfile==1){?>
              <a data-toggle="modal" data-target="#exampleModal1_<?php echo @$existingAppointmentList[$a]['APPOINTMENTID']; ?>" href="view_healthreport_docs.php?id=<?php echo base64_encode(@$existingAppointmentList[$a]['APPOINTMENTID']);?>" class="view_nav"><img src="img/downloadIcon.png" /></a>
                  <div class="modal fade" id="exampleModal1_<?php echo @$existingAppointmentList[$a]['APPOINTMENTID']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <div class="model-header" id="exampleModalLabel">Download Health Report</div>
                      </div>
                     	Please Wait....
                    </div>
                  </div>
                </div>
			  <?php } if(@$existingAppointmentList[$a]['STATUS']=='CANCELLED') { 
			  //echo "<a target='_blank' href='reschedule_appointment.php?appId=".base64_encode(@$existingAppointmentList[$a]['APPOINTMENTID'])."&pId=".base64_encode(@$existingAppointmentList[$a]['HEALTHCHECKUPID'])."' class=''><img src='img/timeIcon.png' /></a>";  
			  } 
				if(@$existingAppointmentList[$a]['STATUS']=='CANCELLED' && @$existingAppointmentList[$a]['RESCHEDULE']=='1') { 
					echo "Rescheduled"; 
				} 
				echo ""; 
			  ?> 
			  &nbsp;</td>
            </tr>
       <?php 
	    $a++;
	 	$k++;
		} 
	   
	   
	   }else{ ?>
       
     	  <tr>
         	 <td colspan="7"  style="text-align:center;">No Appointments</td>
		  </tr>
      <?php } ?>         
            </tbody>
                    </table>
	<?php } if(count(@$existingAppointmentList)>0){?>
    <ul class="pagination pagination-lg pager" id="myPager"></ul>
    <?php } ?>
                <div class="clearfix"></div>
                </div>
            
             </div>
            <div class="tab-pane" id="myPlans"> 
                <div class="tab-paneIn" id="myPlanList" style="display:block;">

<!-- end of new implementation for health plan list -->
                
                
                <div class="clearfix"></div>
                </div>
            </div>
          </div>
        </div>
        
            </div>  
            <div class="col-md-3">
                <?php include("inc/inc.right.php"); ?>
            </div> 
             
        </div>
    </div>
</section>

<?php include("inc/inc.ft.php"); ?>
 <script src="js/pagination.js"></script>
 <script>$('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:10});</script>
