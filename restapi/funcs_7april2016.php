<?php
define("_SITEURL_CUSTOMER","https://customeruat.religare.com:9443/");
$pwdsalt =md5('india@1947');
require("config_inc.php");
function email_verify($emailid){
	global $conn;
	$query = "SELECT count(*) as NUM_ROWS FROM RETUSERMASTER WHERE EMAILID = '$emailid'";
    $sql = @oci_parse($conn, $query) or die("Error! DB Connection. Please try again");
	@oci_execute($sql);
	$row = @oci_fetch_assoc($sql);
	#print_r($row);exit;
	if($row['NUM_ROWS'] > 0){
			return "YES";
	}else{
		    return "NO";
	}
}

function USERAUTH($username, $password){
	require("encryp_decryp_pwd.php");
	global $conn;
    $dataArray = array();
	$dataArray1 = array();
	
	$query = "SELECT USERID, PASSWORD FROM RETUSERAUTH WHERE USERNAME like '$username' and USERSTATUS = 'ACTIVE'";
 	$sql = @oci_parse($conn, $query);
    @oci_execute($sql);
	$row = oci_fetch_assoc($sql);
					if(decrypted_pwd($row['PASSWORD']) == $password){
						$query = "select USERID from RETUSERMASTER where USERID = '{$row['USERID']}'";     
						$sql = @oci_parse($conn, $query);
						@oci_execute($sql);	
						$row = oci_fetch_assoc($sql);
						 if(isset($row['USERID']) && $row['USERID'] != ""){
							$error_msg = "Success";
							return $error_msg;
							
						 }else{
							$error_msg = "Error! User does not exist in Master DB";
							 return $error_msg;
						 }
					}else{
						$error_msg = "Error! Invalid Credentials";
						return $error_msg;
					}
			
	   
	   oci_free_statement($stmt);
       oci_close($conn);
                     
} 

function USER_REGISTER($EMAILID,$PASSWORD,$BIRTHDT,$TITLE,$FIRSTNAME,$LASTNAME,$CONTACTNUM,$CONTACTPHONENUM,$STDCODE){
	require("encryp_decryp_pwd.php");
	global $conn;
	$PWD = encrypted_pwd($PASSWORD);       
    $date = date("Y-m-d");
	$dataArray = array();	
	$dataArray1 = array();
	$query = "SELECT count(*) as NUM_ROWS FROM RETUSERAUTH WHERE USERNAME = '$EMAILID'";
	$sql = @oci_parse($conn, $query) or die("Error! DB Connection. Please try again");
	oci_execute($sql);
	$row = @oci_fetch_assoc($sql);
	
 if($row['NUM_ROWS'] == 0){

	$insert2 = "INSERT INTO RETUSERAUTH (USERID,USERNAME,PASSWORD,USERSTATUS,SOURCE,UNSUCCESSFULATTEMPTS) VALUES (RETUSERAUTH_SEQ.NEXTVAL,'$EMAILID','$PWD','ACTIVE','PORTAL',0)";
	$stmt2 = oci_parse($conn, $insert2);
    if(oci_execute($stmt2)){
   	$check1 = @oci_parse($conn, "SELECT USERID FROM RETUSERAUTH WHERE USERNAME = '$EMAILID'");
	@oci_execute($check1);
	$res=@oci_fetch_assoc($check1);
	
	$USERID=$res['USERID'];
	$insert = "INSERT INTO RETUSERMASTER(USERID,EMAILID,BIRTHDT,TITLE,FIRSTNAME,LASTNAME,CONTACTNUM,CONTACTPHONENUM,STDCODE,ACCOUNTSTARTDT) 
	VALUES ('$USERID','$EMAILID','$BIRTHDT','$TITLE','$FIRSTNAME','$LASTNAME','$CONTACTNUM','$CONTACTPHONENUM','$STDCODE','$date')";
	
		$stmt = oci_parse($conn, $insert);
    if(oci_execute($stmt)){
	global $pwdsalt;
	// 	send email template for new user creation created by Er Amit Kumar Dubey on 19 january 2016 at 4:28 PM
		$to = sanitize_email($EMAILID);
		$text ='email='.$to.'&date='.time();
		
		$resetpwdlink=_SITEURL_CUSTOMER."resetpwd.php?data=".rawurlencode(simple_encrypt($text,$pwdsalt));
		$userDetails=fetchcolumnListCond("FIRSTNAME,LASTNAME","RETUSERMASTER"," WHERE EMAILID='".@$to."' ");
		
		$Mailtemplates=GetTemplateContentByTemplateName('RET_USER_CREATION');
		$lastname=@$userDetails[0]['LASTNAME']?@$userDetails[0]['LASTNAME']:'';
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
		$message  = $Mailtemplates[0]['LARGECONTENT']?stripslashes($Mailtemplates[0]['LARGECONTENT']->load()):'';
		$message  = str_replace("##USER",@$userDetails[0]['FIRSTNAME']." ".@$lastname,$message);			
		$message  = str_replace("##LOGINUSERNAME",@$to,$message);			
		$message  = str_replace("##PASSWORDLINK",@$resetpwdlink,$message);			
		
		$headers  = 'From:'.stripslashes($Mailtemplates[0]['FROMNAME']).' <'.stripslashes($Mailtemplates[0]['FROMEMAIL']). "> \r\n" .
			'MIME-Version: 1.0' . "\r\n" .
			'Content-type: text/html; charset=iso-8859-1';
					// Mail it
		
		mail($to, $subject, $message, $headers);
			
		
	// end of send email
		
		
 	 	   	$res = json_encode(array('USERID'=>$USERID,'EMAILID'=>$EMAILID,'BIRTHDT'=>$BIRTHDT,'TITLE'=>$TITLE,'FIRSTNAME'=>$FIRSTNAME,'LASTNAME'=>$LASTNAME,'CONTACTNUM'=>$CONTACTNUM,'CONTACTPHONENUM'=>$CONTACTPHONENUM,'STDCODE'=>$STDCODE,'ACCOUNTSTARTDT'=>$date,'PASSWORD'=>$PASSWORD));
			return $res;
	
	}else{
		$result = "Error! DB Connection. Please try again";
		return $result;	
	}
	}else{
	 $result = "Error! DB Connection. Please try again";
	 return $result;
	}
 }else{
	 $result = "Error! User is already exist";
	 return $result;
 }

return $result;
}


// functions created by Er amit kumar dubey	for send email template on 19 january 2016 at 4:30 PM
function sanitize_email($input_data) {
	$searchArr=array("document","write","alert","%","$",";","+","|","#","<",">",")","(","'","\'",",");
	$input_data= str_replace("script","",$input_data);
	$input_data= str_replace("iframe","",$input_data);
	$input_data= str_replace($searchArr,"",$input_data);
	$input_data=trim($input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}
// Encrypt Function
function simple_encrypt($encrypt, $key){
    $encrypt = serialize($encrypt);
    $iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
    $key = pack('H*', $key);
    $mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
    $passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt.$mac, MCRYPT_MODE_CBC, $iv);
    $encoded = base64_encode($passcrypt).'|'.base64_encode($iv);
    return $encoded;
}
// Decrypt Function
function simple_decrypt($decrypt, $key){
    $decrypt = explode('|', $decrypt.'|');
    $decoded = base64_decode($decrypt[0]);
    $iv = base64_decode($decrypt[1]);
    if(strlen($iv)!==mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)){ return false; }
    $key = pack('H*', $key);
    $decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
    $mac = substr($decrypted, -64);
    $decrypted = substr($decrypted, 0, -64);
    $calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
    if($calcmac!==$mac){ return false; }
    $decrypted = unserialize($decrypted);
    return $decrypted;
}
function fetchcolumnListCond($col,$table,$cond){
	global $conn;
	$dataArray=array();
	 $query="SELECT $col FROM $table $cond";
			$sql = @oci_parse($conn, $query);
	// Execute Query
	@oci_execute($sql);
	$i=0;
	while (($row = @oci_fetch_assoc($sql))) {
		foreach($row as $key=>$value){
			$dataArray[$i][$key] = $value;				
		}
		$i++;
	}
	return $dataArray;
}
function GetTemplateContentByTemplateName($templateName){
	global $conn;
	$dataArray=array();
		 $query="SELECT TEMPLATEID,SUBJECT,CONTENT,LARGECONTENT,FROMEMAIL,FROMNAME,TEMPLATENAME from CRMEMAILTEMPLTE where TEMPLATENAME='".$templateName."' ";
		$sql = @oci_parse($conn, $query);
		@oci_execute($sql);
		$i=0;
		while (($row = oci_fetch_assoc($sql))) {
			foreach($row as $key=>$value){
				$dataArray[$i][$key] = $value;				
			}
			$i++;
		}
	return $dataArray;
}


?>