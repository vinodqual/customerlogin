<?php /*****************************************************************************
* COPYRIGHT
* Copyright 2013 Catabatic Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: buy_plan.php,v 1.0 2015/09/30 04:55:0 Amit Kumar Dubey Exp $
* $Author: Amit Kumar Dubey $
*
****************************************************************************/

include("inc/inc.hd.php");  			//include header file to show header on page

if($_SESSION['LOGINTYPE']=='CORPORATE'){
	$purchasePlanDetails=getPurchasedPlanDetails(@$_SESSION['PURCHASEPLANID'],@$policy);
	$healthCheckPlans=getHealChkupPlanDetailsConf(@$wellnessList[0]['POLICYID'],@$purchasePlanDetails[0]['PLANID']);
	$memberList=getHealthCheckupPlanMemberDetails(@$_SESSION['PURCHASEPLANID'],@$purchasePlanDetails[0]['PLANID']);
} else {
	$purchasePlanDetails=getRetPurchasedPlanDetails(@$_SESSION['PURCHASEPLANID'],@$_SESSION['policyNumber']);
	$healthCheckPlans=getRetHealChkupPlanDetailsConf(@$_SESSION['productId'],@$purchasePlanDetails[0]['PLANID']);
	$memberList=getRetHealthCheckupPlanMemberDetails(@$_SESSION['PURCHASEPLANID'],@$purchasePlanDetails[0]['PLANID']);
}

if(@$healthCheckPlans[0]['PAYMENTTYPE']=="PAYFROMSALARY"){$actionUrl="pay_from_salary.php";}
if(@$healthCheckPlans[0]['PAYMENTTYPE']=="PAYONLINE"){
	$txnid="rhicl_".$_SESSION['PURCHASEPLANID'];
	$amount=$purchasePlanDetails[0]['COST'];
	$productinfo="Purchase Health Plan from Religare";
	$Firstname=$purchasePlanDetails[0]['EMPNAME'];
	$Lastname=$purchasePlanDetails[0]['EMPNAME'];
	$Email=$purchasePlanDetails[0]['EMPEMAILID'];
	$address=$purchasePlanDetails[0]['ADDRESS'];
	$address2="";
	$City="Delhi";
	$State='Delhi';
	$Country='India';
	$Zipcode='111111';
	$actionUrl=$paymentGatewayUrl;
	$param=trim($merchantId)."|".trim($txnid)."|".trim($amount)."|".trim($productinfo)."|".trim($Firstname)."|".trim($Email)."|||||||||||".trim($salt);
	$Hash=strtolower(hash("sha512", $param));
}
?>
<section id="middleContainer">
  <div class="container-fluid">
    <div class="middlebox">
      <div class="col-md-9">
        <div class="middlebox-left">
          <?php include('inc/inc.healthplan_tab.php'); ?>
          <div class="tab-content ">
            <div class="tab-pane active" id="buyPlan">
            
            <div class="tab-paneIn">
              <div class="col-md-6">
                <div class="tab-left-container">
                  <h1>Confirm Plan (<?php echo @$purchasePlanDetails[0]['PLANNAME']?@$purchasePlanDetails[0]['PLANNAME']:@$purchasePlanDetails[0]['PLANNAME2']; ?>)</h1>
                  
                </div>
              </div>
              <div class="col-md-6">
                <div class="stepProgress">
                  <div class="stepProgressBar"></div>
                  <div class="stepProgressIn"><span class="stepactive">1</span><span class="stepTxt">Select Plan</span></div>
                  <div class="stepProgressIn"><span class="stepactive">2</span><span class="stepTxt">Enter Detail</span></div>
                  <div class="stepProgressIn"><span class="stepactive">3</span><span class="stepTxt">Review</span></div>
                  <div class="stepProgressIn"><span class="stepBar">4</span><span class="stepTxt">Payment</span></div>
                  <div class="cl"></div>
                </div>
              </div>
              
              
              
              
              <div class="col-md-12 topMrgn">
              <form name="buyplan" method="post" id="buyplan" action="<?php echo @$paymentGatewayUrl;?>">
              	<div class="grayBorder">
                	<h2>User Details</h2>
                    <div class="myPlanForm">
                    
                    	<div class="myPlanformBox30per myPlanLeft mrgnMyPlan">
                        	<label>Client No./Emp No. <span class="greenTxt">*</span></label>
                            <div class="inputBox inputBoxcolor">
                            	<div class="inputBoxIn">
                            	  <?php 
     								if($_SESSION['LOGINTYPE']=='CORPORATE'){
									  echo @$_SESSION['clientNumber']?@$_SESSION['clientNumber']:'NA';
									} else {
									  echo @$_SESSION['customerId']?@$_SESSION['customerId']:'NA';
									}
								  ?>
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        
                        
                        <div class="myPlanformBox30per myPlanLeft">
                        	<label>Name <span class="greenTxt">*</span></label>
                            <div class="inputBox inputBoxcolor">
                            	<div class="inputBoxIn">
                                	<?php echo @$_SESSION['policyHolderName'];?>
                                </div>
                            </div>
                        <div class="clearfix"></div></div>
                        
                        
                        <div class="myPlanformBox30per myPlanRight">
                        	<label>Contact No <span class="greenTxt">*</span></label>
                            <div class="inputBox inputBoxcolor">
                            	<div class="inputBoxIn">
                                	<?php echo @$purchasePlanDetails[0]['EMPMOBILE'];?>
                                </div>
                            </div>
                        <div class="clearfix"></div></div>
                        
                        
                        <div class="myPlanformBox30per myPlanLeft">
                        	<label>Email Id <span class="greenTxt">*</span></label>
                            <div class="inputBox inputBoxcolor">
                            	<div class="inputBoxIn">
                            	 <?php echo @$purchasePlanDetails[0]['EMPEMAILID'];?>
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        <div class="myPlanformBox60per myPlanRight">
                        	<label>Address <span class="greenTxt">*</span></label>
                            <div class="inputBox inputBoxcolor">
                            	<div class="inputBoxIn">
                                	<?php echo stripslashes(@$purchasePlanDetails[0]['ADDRESS']);?>
                                </div>
                            </div>
                        <div class="clearfix"></div></div>
                    <div class="clearfix"></div></div>
                    <h2>sELECT mEMBER(S)</h2>
                    <div class="myPlanForm">
                        <table class="responsive" width="100%">
                  <tr>
                    <th>Gender</th>
                    <th>Relation</th>
                    <th>Name</th>
                    <th>Age</th>
                  </tr>
					<?php 
                      $m=0;
                      while($m<count($memberList)){
                    ?>
                  <tr>
                    <td><input name="textfield" type="text" class="txtfield_gender" id="textfield" readonly="yes" style="width:55px;" value="<?php echo @$memberList[$m]['GENDER'];?>"></td>
                    <td><input name="textfield" type="text" class="txtfield_app" id="textfield"  readonly="yes" style="width:135px;" value="<?php echo ucwords($relationArray[$memberList[$m]['RELATION']]);?>"></td>
                    <td><strong class="greenGeneralTxt"><input name="textfield" type="text" class="txtfield_app" id="textfield"  readonly="yes" style="width:135px;" value="<?php echo @$memberList[$m]['NAME'];?>"></strong></td>
                    <td><input name="textfield" type="text" class="txtfield_gender" id="textfield"  readonly="yes" style="width:45px;" value="<?php echo @$memberList[$m]['AGE'];?>"></td>
                  </tr>
	             <?php $m++;} ?>
                 
                </table>
                 <table class="responsive" width="100%">
                    <tr>
                      <td height="40"><strong>Total Amount to be Paid:</strong> INR <?php echo @$purchasePlanDetails[0]['COST'];?></td>
                    </tr>
                    <tr>
                      <td height="40">*Please review the above details and click buy
                        now.</td>
                    </tr>
                  </table>
                    </div>
                    <div class="myPlanBtn">
                              <input type="hidden" name="key" value="<?php echo @$merchantId;?>" />
                              <input type="hidden" name="txnid" value="<?php echo  @$txnid;?>" />
                              <input type="hidden" name="amount" value="<?php echo @$amount;?>" />
                              <input type="hidden" name="productinfo" value="<?php echo @$productinfo;?>" />
                              <input type="hidden" name="Firstname" value="<?php echo @$Firstname;?>" />
                              <input type="hidden" name="Lastname" value="<?php echo @$Lastname;?>" />
                              <input type="hidden" name="address1" value="<?php echo @$address;?>" />
                              <input type="hidden" name="City" value="<?php echo @$City;?>" />
                              <input type="hidden" name="State" value="<?php echo @$State;?>" />
                              <input type="hidden" name="Country" value="<?php echo @$Country;?>" />
                              <input type="hidden" name="Zipcode" value="<?php echo @$Zipcode;?>" />
                              <input type="hidden" name="Email" value="<?php echo @$Email;?>" />
                              <input type="hidden" name="surl" value="<?php echo @$surl;?>" />
                              <input type="hidden" name="furl" value="<?php echo @$furl;?>" />
                              <input type="hidden" name="curl" value="<?php echo @$curl;?>" />
                              <input type="hidden" name="Hash" value="<?php echo @$Hash;?>" />
                              <input type="hidden" name="Pg" value="CC" /> 
                    	<a href="javascript:void(0);" onClick="submitForm('buyplan');" class="submit-btn">Buy Now ></a><a href="javascript:void(0);" onClick="window.location='health_checkup.php'" class="cancel-btn">Cancel ></a>
                    </div>
                </div>
              </form>
              </div>
              <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </div>
    <div class="col-md-3">
        <?php include("inc/inc.right.php"); ?>
    </div> 
    </div>
  </div>
</section>
<?php include("inc/inc.ft.php"); ?>
