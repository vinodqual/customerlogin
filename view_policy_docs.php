<?php 
/*****************************************************************************
* COPYRIGHT
* Copyright 2015 Catabatic Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: view_reimbursement.php,v 1.0 2015/10/07 05:30:15 amit kumar dubey Exp $
* $Author: amit kumar dubey $
*
****************************************************************************/
include_once("conf/conf.php");           //include configuration file 
include_once("conf/common_functions.php");          // include function file
include_once("conf/session.php");		//including session file 
if(isset($_REQUEST['id'])&&!empty($_REQUEST['id'])){
    $policyNumber=sanitize_data(base64_decode($_REQUEST['id']));
}
if(isset($policyNumber) && !empty($policyNumber)){
    $list=fetchDocumentList($policyNumber);
}


?>
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="model-header" id="exampleModalLabel" align="left">Policy Document Details</div>
      </div>
      <div class="modal-middle" id="reimbursmentdoc" style="overflow:auto;">
			<table   width="100%">
			  <tr>
				<th>#</th>
				<th>Title</th>
				<th>File  Name</th>
				<th class="text-right">Download</th>
			  </tr>
				<?php  
				if(count($list) > 0){

				$w=0;
				$g=1;
				while($w<count($list)){
				$doclist =@$list[$w]['FILENAME'];
				if(isset($doclist) && !empty($doclist)){
				$res = explode(',',$doclist);
				}
				if(count(@$res)>0){
				$k=$g;
				for($i=0;$i<=count($res);$i++){ 
				if(@$res[$i]!=''){
				?>
			  <tr>
				<td><?php echo $k++;  ?></td>
				<td><?php echo stripslashes($list[$w]['TITLE']); ?></td>
				<td><?php echo stripslashes(@$res[$i]); ?></td>
                                
                                <!----------------- Code added for download file using SFTP ---------------->
                                <td class="text-right">
                                    <?php if(isset($res[$i])&&!empty($res[$i])){
                                        $folder_name=str_replace('##POLICY_NO##',$policyNumber,POLICY_DOC);
                                        $url=create_url($folder_name,$res[$i],CUSTOMERLOGIN);        // Create File Download Link
                                        if($url!==FALSE){
                                            echo '<a href="'.$url.'"><span class="downloadIcon"></span></a>';
                                        }else{
                                            echo SFTP0026;
                                        }
                                        
                                    }else{
                                        echo 'NA';
                                    } ?>
                                </td>
                          </tr> 
                                <!----------------- End of code -------------------------------------------->
                                
<!--				<td class="text-right">
                                    <a href="download.php?fileName=<?php //echo @$res[$i]; ?>"><span class="downloadIcon"></span></a></td>
                          </tr> -->
			  <?php } }
			  } 
			  $w++; $g++; }
			  } else {
			  ?>
			  <tr>
				<td colspan="4"><?php echo "No Document Uploaded";?></td>
			  </tr>  
				  <?php
				  }
				   ?>  
			</table>      
     </div>
      
    </div>
                     