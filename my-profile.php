<?php
include("inc/inc.hd.php");
include_once("conf/session.php");
include_once("conf/conf.php"); //include configuration file to use databse connectivity 
include_once("conf/common_functions.php");
$tab=base64_decode(sanitize_data(@$_REQUEST['tab']));
$successmsg='';
$rediret=0;

if($_SESSION['LOGINTYPE']=='RETAIL'){
	$fetchPolListByUserId=fetchListCond("RETUSERMASTER"," WHERE USERID=".@$_SESSION['USERID']."");//query 
	$name = ucfirst(@$fetchPolListByUserId[0]['TITLE']).' '.ucfirst(@$fetchPolListByUserId[0]['FIRSTNAME']).' '.ucfirst(@$fetchPolListByUserId[0]['LASTNAME']);
	$dob = date("d-m-Y", strtotime(@$fetchPolListByUserId[0]['BIRTHDT']));
	$emailid=$fetchPolListByUserId[0]['EMAILID'];
	$contactnumber=$fetchPolListByUserId[0]['CONTACTNUM'];
	$contactnumber=@$contactnumber?@$contactnumber:'NA';
	if(isset($_POST["newpass"]) && $_POST['newpass']!='') { //check condition to insert or update data into/database
	
		require_once 'passwordverify/PkcsKeyGenerator.php';
		require_once 'passwordverify/DesEncryptor.php';
		require_once 'passwordverify/PbeWithMd5AndDes.php';
		
	$newpass = $_POST['newpass'];
	$oldpass=base64_decode($_POST['oldpass']);
	
	$encryptOldPass = PbeWithMd5AndDes::encrypt($oldpass, $keystring);
	$decryptOldPass = PbeWithMd5AndDes::decrypt($encryptOldPass, $keystring);
	$encryptNewPass = PbeWithMd5AndDes::encrypt($newpass, $keystring);
	$decryptNewPass = PbeWithMd5AndDes::decrypt($encryptNewPass, $keystring);
	
		if($oldpass == $decryptOldPass && $newpass == $decryptNewPass){
			$id=@$_SESSION['USERID'];
			$check_password=fetchListCond("RETUSERAUTH"," WHERE USERID='".$id."'");
			$count = count($check_password[0]['PASSWORD']);
			$dbDecriptPass = PbeWithMd5AndDes::decrypt($check_password[0]['PASSWORD'], $keystring);
			if(($count>0) && ($dbDecriptPass == $_POST['oldpass'])){
				   $sql="UPDATE  RETUSERAUTH SET PASSWORD='".@$encryptNewPass."',ISPASSWORDCHANGED='YES' WHERE USERID='".@$id."'"; //query to Change password
							$stdid = @oci_parse($conn, $sql);
							$r = @oci_execute($stdid);
							$_SESSION['ISPASSWORDCHANGED']='YES';
						//$email=$check_password[0]['EMAILID'];
						//$username=$check_password[0]['USERNAME'];
						//SendMailToUser($email,$username,$passtosend);  //function to send mail to the user on updating password	
						$successmsg='<div><font color="green">Password Updated Successfully</font></div>'; 
						$tab = 'Y3Bhc3M=';		
						$tab=base64_decode(sanitize_data($tab));//error message when password entered wrong by user
					$rediret=1;
					}
					else
					{
						$successmsg='<div><font color="red">Wrong old password!</font></div>'; 
						$tab = 'Y3Bhc3M=';		
						$tab=base64_decode(sanitize_data($tab));//error message when password entered wrong by user
						//exit;
					}
					
		}else{
						$successmsg='<div><font color="red">Wrong old password!</font></div>';  
						$tab = 'Y3Bhc3M=';
						$tab=base64_decode(sanitize_data($tab));
					}
	}
}
if($_SESSION['LOGINTYPE']=='CORPORATE'){
	
	$fetchPolListByUserId=fetchListCond("CRMEMPLOYEELOGIN"," WHERE LOGINID=".@$_SESSION['USERID']."");//query 
	$name = ucfirst(@$fetchPolListByUserId[0]['EMPLOYEENAME']);
	$dob = '';
	$emailid=$fetchPolListByUserId[0]['EMAIL'];
	$contactnumber=$fetchPolListByUserId[0]['MOBILENUMBER'];
	if(isset($_POST["newpass"]) && $_POST['newpass']!='') {

		$newpassword=$_POST['newpass'];		//decode password
		$oldpassword=$_POST['oldpass'];
		$query1 = sprintf("SELECT * FROM CRMEMPLOYEELOGIN  WHERE EMAIL like '%s' AND ENCRYPTPASSWORD like '%s'",
		sanitize_username(@$_SESSION['userName']),
		md5(@$oldpassword));
		// Parse Query 
		$login = @oci_parse($conn, $query1);
		// Execute Query
		@oci_execute($login);
		// Fetch Data IN Associative Array
		$nrows1 = @oci_fetch_all($login, $res1);
		
		if($nrows1>0 && strlen($newpassword)>5)	{	
		//check password not exist with same emailid
		$passwordQuery=fetchcolumnListCond("EMAIL,LOGINID,ISPASSWORDCHANGED,STATUS","CRMEMPLOYEELOGIN","WHERE EMAIL='".$_SESSION['userName']."' AND ENCRYPTPASSWORD='".md5(@$newpassword)."' ");
		
        if(count($passwordQuery) == 0){
			$query=sprintf("UPDATE  CRMEMPLOYEELOGIN SET ENCRYPTPASSWORD='".md5(@$newpassword)."',PASSWORD='".@$newpassword."',ISPASSWORDCHANGED='YES' WHERE LOGINID=".$_SESSION['USERID']." AND EMAIL like '%s' ",sanitize_username(@$_SESSION['userName']));
			$stdid = @oci_parse($conn, $query);
			$r = @oci_execute($stdid);	
			$_SESSION['ISPASSWORDCHANGED']='YES';
			$successmsg='<div><font color="green">Password Updated Successfully</font></div>'; 
			$tab = 'Y3Bhc3M=';		
			$rediret=1;
		} else{
			$tab = 'Y3Bhc3M=';		
			$successmsg='<div><font color="red">You have used this password recently, Please choose a different one.</font></div>';  
		}
			$tab=base64_decode(sanitize_data($tab));//error message when password entered wrong by user
		
	}else{
		$successmsg='<div><font color="red">Wrong old password!</font></div>';  
		$tab = 'Y3Bhc3M=';
		$tab=base64_decode(sanitize_data($tab));
	}
	
	}
}
if($rediret==1){
	echo '<meta http-equiv="refresh" content="3;url=dashboard.php" />';
}
 ?>
<style>
div.error {
	position:absolute;
	margin-top:8px;
	color:red;
	padding:3px;
	text-align:left;
	z-index:1;
	margin-left: -18px;
	font-size:14px;
}
.demo{
	color:red; 
	padding-top:10px;
}
.demosuccess{
	color:#5e9d2d;
	padding-top:10px;
}
</style>

<section id="middleContainer">
  <div class="container-fluid">
    <div class="middlebox">
      <div class="col-md-9">
        <div class="middlebox-left">
          <ul class="nav nav-tabs responsive" id="myTab">
            <li class="test-class <?php if(empty($tab) || $tab=='' || $tab!='cpass'){ echo "active";  } ?>"><a class="deco-none misc-class" href="#buyPlan"> My Profile</a></li>
            <li class="<?php if(!empty($tab) && $tab!='' && $tab=='cpass'){ echo "active";  } ?>"><a class="deco-none" href="#myPlans">Change Password</a></li>
          </ul>
          <div class="tab-content responsive">
            <div class="tab-pane <?php if(empty($tab) || $tab=='' || $tab!='cpass'){ echo "active";  } ?>" id="buyPlan">            
            <div class="tab-paneIn">                          
              <div class="clearfix"></div>              
              <div class="tab-mid-container">
              <?php  echo $successmsg; ?>
                <table class="responsive" width="100%">
					<tr>
                    <th colspan="2">My Profile</th>
					</tr>
                      <?php if($_SESSION['LOGINTYPE']=='RETAIL'){ ?>
					<tr>
					<td>Name</td>
					<td><?php echo $_SESSION['renewalArray'][0]['firstName1']['#text']?@$_SESSION['renewalArray'][0]['titleCd']['#text']."&nbsp;".@$_SESSION['renewalArray'][0]['firstName1']['#text']."&nbsp;".@$_SESSION['renewalArray'][0]['lastName1']['#text']:$name;?></td>
					</tr>
					<tr>
					<td>DOB</td>
					<td><?php echo $_SESSION['renewalArray'][0]['birthDt']['#text']?@$_SESSION['renewalArray'][0]['birthDt']['#text']:'NA';?></td>
					</tr>
                     <?php } else { ?>
					<tr>
					<td>Name</td>
					<td><?php echo @$name?$name:'NA'; ?></td>
					</tr>
					<tr>
					<td>Employee-Id</td>
					<td><?php echo @$fetchPolListByUserId[0]['EMPLOYEEID']?@$fetchPolListByUserId[0]['EMPLOYEEID']:'NA';?></td>
					</tr>
					<tr>
                        <td>Email</td>
                        <td><?php echo @$emailid?$emailid:'NA';?></td>
					</tr>
					<tr>
                        <td>Mobile</td>
                        <td><?php echo @$contactnumber?$contactnumber:'NA';?></td>
					</tr>
                     <?php } if($_SESSION['LOGINTYPE']=='RETAIL'){ ?>
					<tr>
                        <td>Email</td>
                        <td><?php echo $_SESSION['renewalArray'][0]['emailAddress']['#text']?@$_SESSION['renewalArray'][0]['emailAddress']['#text']:$emailid;?></td>
					</tr>
					<tr>
                        <td>Mobile</td>
                        <td><?php echo $_SESSION['renewalArray'][0]['contactNum']['#text']?@$_SESSION['renewalArray'][0]['stdCode']['#text']."-".@$_SESSION['renewalArray'][0]['contactNum']['#text']:$contactnumber;?></td>
				    </tr>
                    <?php
					 for($f=0;$f<count($_SESSION['renewalArray']);$f++){ 
						$addressType=$_SESSION['renewalArray'][$f]['addressTypeCd']['#text'];
						$addressLine1=$_SESSION['renewalArray'][$f]['addressLine1Lang1']['#text'];
						$addressLine1=$addressLine1?$addressLine1:'';
						$addressLine2=$_SESSION['renewalArray'][$f]['addressLine1Lang2']['#text'];
						$addressLine2=$addressLine2?$addressLine2:'';
						$totaladdress=$addressLine1."&nbsp;".$addressLine2;
						$country=$_SESSION['renewalArray'][$f]['countryCd']['#text'];
						$state=$_SESSION['renewalArray'][$f]['stateCd']['#text'];
						$city=$_SESSION['renewalArray'][$f]['cityCd']['#text'];
						$pincode=$_SESSION['renewalArray'][$f]['pinCode']['#text'];
						$area=$_SESSION['renewalArray'][$f]['areaCd']['#text'];
					?>
					<tr>
                        <td><?php echo ucfirst(strtolower($addressType)); ?> Address</td>
                        <td><?php echo $totaladdress.",".$state.",".$city.",".$pincode."(".$country.")"; ?></td>
				    </tr>
                  <?php }
				  } ?>
                </table>
              </div>
              <div class="clearfix"></div>
              </div>            
              <div class="colfull">
			</div>
              <div class="clearfix"></div>
            </div>
			
            <div class="tab-pane <?php if(!empty($tab) && $tab!='' && $tab=='cpass'){ echo "active";  } ?>" id="myPlans">           
            	<div class="tab-paneIn" id="myplanForm">
				<form name="changePassForm" id="changePassForm" method="POST" action="">                
              	  <div class="col-sm-12">
            	<div class="title-bg textleft">
               <h1>Change Password <?php if($_SESSION['ISPASSWORDCHANGED']=='NO' || $_SESSION['ISPASSWORDCHANGED']=='') { echo "(Please reset your password first!)"; } ?></h1>
                </div>               
                <div class="grayBorder">
                	<h2>Fill Your Details</h2>
					 <div class="demo"><?php if(@$successmsg !=''){echo @$successmsg; @$successmsg=''; }?></div>
                    <div class="myPlanForm">                    
                    	<div class="myPlanformBox myPlanLeft">
                        	<label>Old Password<span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                            	  <input type="password" name="oldpass" id="oldpass" onkeyup="fillAllValue('oldpass','oldpasshidden');" class="txtField password" value="" autocomplete="off" >
                            	  <input type="text" style="display:none;" name="oldpasshidden" onkeyup="fillAllValue('oldpasshidden','oldpass');" id="oldpasshidden" class="txtField oldpasshidden" value="" autocomplete="off" >
                            	</div>
                            </div>
                        <div class="clearfix"></div></div> 
				
                        <div class="myPlanformBox">
                        	<label>New Password <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                            	  <input type="password" name="newpass" id="newpass" onkeyup="fillAllValue('newpass','newpasshidden');" class="txtField password" value="<?php echo @$_POST['newpass']; ?>" autocomplete="off" >
                            	  <input type="text" style="display:none;" name="newpasshidden" id="newpasshidden"  onkeyup="fillAllValue('newpasshidden','newpass');" class="txtField passwordhidden" value="<?php echo @$_POST['newpass']; ?>" autocomplete="off" >
                            	</div>
								
                            </div>
							
                        <div class="clearfix"></div><span id="alertpasstext"></span></div>
						
						<div class="myPlanformBox myPlanLeft">
                        	<label>Confirm Password <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                            	  <input type="password" name="confirmpass" onkeyup="fillAllValue('confirmpass','confirmpasshidden');" id="confirmpass" class="txtField password" value="<?php echo @$_POST['confirmpass']; ?>" autocomplete="off">
                            	  <input type="text" name="confirmpasshidden" id="confirmpasshidden" onkeyup="fillAllValue('confirmpasshidden','confirmpass');" class="txtField passwordhidden" value="<?php echo @$_POST['confirmpass']; ?>" autocomplete="off">
                            	</div>
                            </div>
							<div class="showpassword" style="float:right;"><input type="checkbox" id="showHide"> Show Password</div>
                        <div class="clearfix"></div><span id="alerttext"></span></div>
                          <div class="clearfix"></div>
					</div>
                        
                 
                    <!-- <div class="myPlanBtn"> -->
					<div class="">
					
					<input type="submit" name="submit" value="Submit" class="submit-btn"/>
					<input type="submit" name="submit" value="Cancel" class="cancel-btn"/>
                    	
                    </div>
                  
                 
                </div>   
                </div>
                  <div class="clearfix"></div>
                </div>
                  </form>
                
                <div class="tab-paneIn" id="myPlanList" style="display:none;">
                <div class="col-sm-12">
                	
                </div>
                <div class="clearfix"></div>
                </div>
                
                
            </div>
			</form>
          </div>
        </div>
        
      </div>
      <div class="col-md-3">
       <?php include("inc/inc.right.php"); ?> 
      </div>
	  
    </div>
  </div>
</section>
<?php include("inc/inc.ft.php"); ?>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/additional-methods.min.js" type="text/javascript"></script>
<script type="text/javascript">
function fillAllValue(primaryId,secondaryId){
	primaryvalue=$.trim($('#'+primaryId).val());
	$('#'+secondaryId).val(primaryvalue);
	
}
 $("#showHide").click(function () {
	 if($("#showHide").is(":checked")==true){
		 try{
			 $(".error").css({'padding-top':'28px'});
		 $("#oldpasshidden").val($("#oldpass").val());
		 $("#oldpass").hide();
		 $("#oldpasshidden").show();

		 $("#newpasshidden").val($("#newpass").val());
		 $("#newpass").hide();
		 $("#newpasshidden").show();


		 $("#confirmpasshidden").val($("#confirmpass").val());
		 $("#confirmpass").hide();
		 $("#confirmpasshidden").show();

		 } catch(e){
			alert(e); 
		 }
	 }else {
		 $(".error").css({'padding':'3px'});
   	     $("#oldpass").val($("#oldpasshidden").val());
		 $("#oldpass").show();
		 $("#oldpasshidden").hide();


   	     $("#newpass").val($("#newpasshidden").val());
		 $("#newpass").show();
		 $("#newpasshidden").hide();


   	     $("#confirmpass").val($("#confirmpasshidden").val());
		 $("#confirmpass").show();
		 $("#confirmpasshidden").hide();
	 }
 });
    $.validator.addMethod('passWithOneNumberOneAlphabatic', function(value, element) {
            return this.optional(element) || (value.match(/[a-zA-Z]/) && value.match(/[0-9]/));
        },
        'Password must contain at least one numeric and one alphabetic character.'); // additional method to get more secure password by user

	$("#changePassForm").validate({
	rules :{
		"oldpass" : {
			required : true
		},
		"newpass" : {
		   required : true,
		   minlength: 8,
		   passWithOneNumberOneAlphabatic: true
                        
		},
		"confirmpass" : {
			required: true,
			minlength: 8,
			equalTo: "#newpass"
		}		
	},
	messages :{
		"oldpass" : {
			required : 'Please enter old password'
		},
		"newpass" : {
			required : 'Please enter new password'
		},
		"confirmpass" : {
			required : 'Please enter confirm password',
			equalTo: 'Password does not match'
		}
	},
	errorElement: "div"		
});
$("#changePassForm").removeAttr("novalidate");

</script>

