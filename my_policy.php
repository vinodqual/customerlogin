<?php
include("inc/inc.hd.php");
$downloadToken = md5(uniqid(rand(), TRUE));
$_SESSION['download_token'] = $downloadToken;
?>
<section id="middleContainer">
    <div class="container-fluid">
        <div class="middlebox">
            <div class="col-md-9">
                <div class="valueaddedBox" style="overflow:hidden; clear:both;   background: #fff none repeat scroll 0 0; border: 1px solid #e0e2e3; padding-bottom:2%;">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-default" style="border-bottom:none;">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                        <strong><?php echo @$_SESSION['policyNumber'] ? @$_SESSION['policyNumber'] : "NA"; ?></strong> <?php if (isset($_SESSION['productName']) && !empty($_SESSION['productName'])) {
    $productname = $_SESSION['productName'];
} else {
    $productname = 'NA';
} if ($_SESSION['LOGINTYPE'] == 'RETAIL') {
    echo " [ " . $productname . " ] ";
} ?> 
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                <div class="panel-body" >
                                    <table class="responsive responsivedash" width="100%">
                                        <tr>
                                            <td><span style="font-weight:bold;">Name</span></td>
                                            <td><span style="font-weight:bold;">Type</span></td>
                                            <td><span style="font-weight:bold;">Age</span></td>

                                            <td class="text-right"> <div style="font-weight:bold;"  > Policy Documents</div> </td>

                                        </tr>

                                        <?php
                                        if (!empty($responseArray)) {
                                            if ((isset($responseArray[0]['BGEN-MEMNAME']['#text'])) && ($responseArray[0]['BGEN-MEMNAME']['#text'] != "")) {
                                                $i = 0;
                                                $membersarray = array();
                                                foreach ($responseArray as $key => $response) {
                                                    $dataPdf = array();
                                                    $queryPdf = '';
                                                    if ($key == 0) {
                                                        if (trim($response['BGEN-GENDER']['#text']) == "F") {
                                                            $_SESSION['GENDER'] = "Female";
                                                            $genderIcon = 'femaleIcon';
                                                        }
                                                        if (trim($response['BGEN-GENDER']['#text']) == "M") {
                                                            $_SESSION['GENDER'] = "Male";
                                                            $genderIcon = 'maleIcon';
                                                        }
                                                        /* line added by Rahul shrivastava 29-11-2016 for download health card without web-services request */
                                                        $dataPdf[] = base64_encode(trim($response['BGEN-MEMNAME']['#text']));
                                                        $dataPdf[] = base64_encode(trim(@$response['BGEN-CLNTNM']['#text']));
                                                        $dataPdf[] = base64_encode((int) (trim($response['BGEN-AGE']['#text'])));
                                                        $dataPdf[] = base64_encode(trim($response['BGEN-EMPNO']['#text']));
                                                        $queryPdf = array('cl' => json_encode($dataPdf));
                                                        ?>
                                                        <tr>
                                                            <td><span class="<?php echo $genderIcon; ?>"></span><?php echo ucwords(strtolower(trim($response['BGEN-MEMNAME']['#text']))); ?></td>
                                                            <td><?php echo ucwords($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]); ?></td>
                                                            <td><?php echo(trim($response['BGEN-AGE']['#text'])); ?></td>

                                                            <td class="text-right">
                                                                <?php
                                                                if ($_SESSION['LOGINTYPE'] == 'CORPORATE') {
                                                                    if ($wellnessList[0]['DISABLEDOWNLOADPOLICYDOC'] == "Yes" || empty($wellnessList[0]['DISABLEDOWNLOADPOLICYDOC'])) {
                                                                        ?>
                                                                        <a  <?php if ($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs'] != 'NO') { ?> data-toggle="modal"  data-target="#exampleModal1_<?php echo @$_SESSION['policyNumber']; ?>" href="view_policy_docs.php?id=<?php echo base64_encode(@$_SESSION['policyNumber']); ?>&token=<?php echo $downloadToken; ?>" <?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } ?>>   						
                                                                            <span class="editIcon" title="Policy Documents"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <?php } ?>

                                                                    <?php /* line commented by Rahul shrivastava 25-11-2016 
                                                                      <a  <?php if($wellnessList[0]['DISABLEDOWNLOADHEALTHCARD']!='YES'){ if($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs']!='NO'){ ?> onclick="javascript:window.open('download_pdf.php?clientNo=<?php echo base64_encode(trim($response['BGEN-CLNTNM']['#text']));?>','parentWindow','width=790,height=480,scrollbars=yes,menubar=no,status=yes,resizable=yes,directories=false,location=yes,left=0,top=0')" <?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } } ?> href="javascript:void(0)" class="pdf_nav">
                                                                      <span class="healthcardeditIcon" title="Health Card"></span></a> */ ?>
                                                                    <?php /* line added by Rahul shrivastava 25-11-2016 for download health card without web-services request */ ?> 
                    <?php if (strtolower($wellnessList[0]['DISABLEDOWNLOADHEALTHCARD']) == "yes" || empty($wellnessList[0]['DISABLEDOWNLOADHEALTHCARD'])) { ?>
                                                                        <a  <?php if ($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs'] != 'NO') { ?> onclick="javascript:window.open('download_pdf.php?clientId=<?php echo urlencode(implode('/', $queryPdf)); ?>&token=<?php echo $downloadToken; ?>', 'parentWindow', 'width=790,height=480,scrollbars=yes,menubar=no,status=yes,resizable=yes,directories=false,location=yes,left=0,top=0')" <?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } ?> href="javascript:void(0)" class="pdf_nav">
                                                                            <span class="healthcardeditIcon" title="Health Card"></span></a>
                                                                       <?php
                                                                    }

                                                                    if ($wellnessList[0]['DISABLEDOWNLOADPOLICYPDF'] == "Yes" || empty($wellnessList[0]['DISABLEDOWNLOADPOLICYPDF'])) {
                                                                        ?> 
                                                                        <a <?php if ($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs'] != 'NO') { ?> onclick="javascript:window.open('download_pdf.php?clientNo=<?php echo base64_encode(trim($response['BGEN-CLNTNM']['#text'])); ?>&policy=policy&token=<?php echo $downloadToken; ?>', 'parentWindow', 'width=790,height=480,scrollbars=yes,menubar=no,status=yes,resizable=yes,directories=false,location=yes,left=0,top=0')" <?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } ?> href="javascript:void(0)" class="pdf_nav">	
                                                                            <span class="umbIcon" title="Policy PDF"></span></a>

                    <?php
                    }
                } else {
                    if ($wellnessList[0]['DISABLEDOWNLOADPOLICYPDF'] == "Yes" || empty($wellnessList[0]['DISABLEDOWNLOADPOLICYPDF'])) {
                        ?>
                                                                        <a  <?php if ($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs'] != 'NO') { ?> onclick="javascript:window.open('download_pdf.php?clientNo=<?php echo base64_encode(trim($response['BGEN-CLNTNM']['#text'])); ?>&policy=policy&token=<?php echo $downloadToken; ?>', 'parentWindow', 'width=790,height=480,scrollbars=yes,menubar=no,status=yes,resizable=yes,directories=false,location=yes,left=0,top=0')" href="javascript:void(0)" class="pdf_nav" <?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } ?> class="pdf_nav">	
                                                                            <span class="umbIcon" title="Policy PDF"></span></a>
                    <?php }
                } ?>
                                                                <div class="modal fade" id="exampleModal1_<?php echo @$_SESSION['policyNumber']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <div class="model-header" id="exampleModalLabel">Reimbursement Doc List</div>
                                                                            </div>
                                                                            Please Wait....

                                                                        </div>
                                                                    </div>
                                                                </div>           

                                        <!--<span class="umbIcon" data-toggle="modal" data-target="#exampleModal1"></span>-->
                                                            </td>
                                                        </tr>
                                                    <?php
                                                    } else {
                                                        if (trim($response['BGEN-GENDER']['#text']) == "F") {
                                                            $genderIcon = 'femaleIcon';
                                                        }
                                                        if (trim($response['BGEN-GENDER']['#text']) == "M") {
                                                            $genderIcon = 'maleIcon';
                                                        }
                                                        /* line added by Rahul shrivastava 29-11-2016 for download health card without web-services request */
                                                        $dataPdf[] = base64_encode(trim($response['BGEN-MEMNAME']['#text']));
                                                        $dataPdf[] = base64_encode(trim(@$response['BGEN-CLNTNM']['#text']));
                                                        $dataPdf[] = base64_encode((int) (trim($response['BGEN-AGE']['#text'])));
                                                        $dataPdf[] = base64_encode(trim($response['BGEN-EMPNO']['#text']));
                                                        $queryPdf = array('cl' => json_encode($dataPdf));


                                                        //$membersarray[] = strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);
                                                        ?>
                                                        <tr>
                                                            <td><span class="<?php echo $genderIcon; ?>"></span><?php echo ucwords(strtolower(trim($response['BGEN-MEMNAME']['#text']))); ?></td>
                                                            <td><?php echo ucwords($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]); ?></td>
                                                            <td><?php echo(trim($response['BGEN-AGE']['#text'])); ?></td>
                                                            <td class="text-right">
                                                                <?php
                                                                if ($_SESSION['LOGINTYPE'] == 'CORPORATE') {
                                                                    if ($wellnessList[0]['DISABLEDOWNLOADPOLICYDOC'] == "Yes" || empty($wellnessList[0]['DISABLEDOWNLOADPOLICYDOC'])) {
                                                                        ?>
                                                                        <a data-toggle="modal"  <?php if ($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs'] != 'NO') { ?> data-target="#exampleModal1_<?php echo @$_SESSION['policyNumber']; ?>" href="view_policy_docs.php?id=<?php echo base64_encode(@$_SESSION['policyNumber']); ?>&token=<?php echo $downloadToken; ?>" <?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } ?>>   						
                                                                            <span class="editIcon" title="Policy Documents"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                                        <?php /* line commented by Rahul shrivastava 25-11-2016 
                                                                          <a  <?php if($wellnessList[0]['DISABLEDOWNLOADHEALTHCARD']!='YES'){ if($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs']!='NO'){ ?> onclick="javascript:window.open('download_pdf.php?clientNo=<?php echo base64_encode(trim($response['BGEN-CLNTNM']['#text']));?>','parentWindow','width=790,height=480,scrollbars=yes,menubar=no,status=yes,resizable=yes,directories=false,location=yes,left=0,top=0')" <?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } } ?> href="javascript:void(0)" class="pdf_nav">
                                                                          <span class="healthcardeditIcon" title="Health Card"></span></a> */ ?>
                                                                        <?php /* line added by Rahul shrivastava 25-11-2016 for download health card without web-services request */ ?>                     
                                                                    <?php } if (strtolower($wellnessList[0]['DISABLEDOWNLOADHEALTHCARD']) == "yes" || empty($wellnessList[0]['DISABLEDOWNLOADHEALTHCARD'])) { ?>                        
                                                                        <a  <?php if ($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs'] != 'NO') { ?> onclick="javascript:window.open('download_pdf.php?clientId=<?php echo urlencode(implode('/', $queryPdf)); ?>&token=<?php echo $downloadToken; ?>', 'parentWindow', 'width=790,height=480,scrollbars=yes,menubar=no,status=yes,resizable=yes,directories=false,location=yes,left=0,top=0')" <?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } ?> href="javascript:void(0)" class="pdf_nav">
                                                                            <span class="healthcardeditIcon" title="Health Card"></span></a>
                     <?php
                    }
                    if ($wellnessList[0]['DISABLEDOWNLOADPOLICYPDF'] == "Yes" || empty($wellnessList[0]['DISABLEDOWNLOADPOLICYPDF'])) {
                        ?>
                                                                        <a  <?php if ($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs'] != 'NO') { ?> onclick="javascript:window.open('download_pdf.php?clientNo=<?php echo base64_encode(trim($response['BGEN-CLNTNM']['#text'])); ?>&policy=policy&token=<?php echo $downloadToken; ?>', 'parentWindow', 'width=790,height=480,scrollbars=yes,menubar=no,status=yes,resizable=yes,directories=false,location=yes,left=0,top=0')" <?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } ?> href="javascript:void(0)" class="pdf_nav">
                                                                            <span class="umbIcon" title="Policy PDF"></span></a>
                    <?php
                    }
                } else {
                    if ($wellnessList[0]['DISABLEDOWNLOADPOLICYPDF'] == "Yes" || empty($wellnessList[0]['DISABLEDOWNLOADPOLICYPDF'])) {
                        ?>  
                                                                        <a <?php if ($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs'] != 'NO') { ?> onclick="javascript:window.open('download_pdf.php?clientNo=<?php echo base64_encode(trim($response['BGEN-CLNTNM']['#text'])); ?>&policy=policy&token=<?php echo $downloadToken; ?>', 'parentWindow', 'width=790,height=480,scrollbars=yes,menubar=no,status=yes,resizable=yes,directories=false,location=yes,left=0,top=0')" href="javascript:void(0)" class="pdf_nav" <?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } ?> class="pdf_nav">
                                                                            <span class="umbIcon" title="Policy PDF"></span></a>
                                                            <?php }
                                                        } ?>
                                                                <div class="modal fade" id="exampleModal1_<?php echo @$_SESSION['policyNumber']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <div class="model-header" id="exampleModalLabel">Reimbursement Doc List</div>
                                                                            </div>
                                                                            Please Wait....

                                                                        </div>
                                                                    </div>
                                                                </div>           

                                                            </td>
                                                        </tr>
            <?php
            }
            $i++;
        }
    } else {
        ?>
                                                <tr>
                                                    <td colspan="4">
        <?php if (isset($responseError)) { ?>
                                                            <font color="#cc0000"><b><?php echo $responseError; ?></b></font>
        <?php } else if (!empty($_SESSION['response_error'])) { ?>
                                                            <font color="#cc0000"><b><?php echo $_SESSION['response_error']; ?></b></font>
        <?php } else { ?>
                                                            <font color="#cc0000"><b>No record(s) found.</b></font>
        <?php } ?>
                                                    </td>
                                                </tr>    
                    <?php }
                } else { ?>
                                            <tr>
                                                <td colspan="10" align="center" style="text-align:-moz-center;border:none;"><font color="#cc0000"><b>Please Try Again or </b></font><a href="individual-policy-registration.php" target="_blank">Register your policy</a></td>
                                            </tr>
<?php } ?>                                         
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>

                </div>




            </div>
            <div class="col-md-3">
<?php include("inc/inc.right.php"); ?>
            </div>
        </div>
    </div>
</section>

<?php include("inc/inc.ft.php"); ?>
