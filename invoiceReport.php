<?php 
require('../religare1/religarehrcrm/oaq/pdf-Religare/fpdf.php');
class PDF extends FPDF{
	// Colored table
	function FancyTable($columnWidth,$columnData){
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(34);
		$this->SetFont('Helvetica','',10);
		// Data
		$fill = false;
		for($k=0;$k<count($columnData);$k++){
			for($i=0;$i<count($columnWidth);$i++){
				$this->Cell($columnWidth[$i],7,$columnData[$k][$i],1,0,'L',$fill);				
			}
			$this->Ln();
		}
		$this->Ln(6);
	}
	function FancyTable1($columnWidth,$columnData){
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(34);
		$this->SetFont('Helvetica','',10);
		// Data
		$fill = false;
		for($k=0;$k<count($columnData);$k++){
			for($i=0;$i<count($columnWidth);$i++){
				$this->Cell($columnWidth[$i],7,$columnData[$k][$i],1,0,'L',$fill);				
			}
			$this->Ln();
		}
		$this->Ln(0);
	}

	function column($columnName,$columnWidth){
		$this->SetFillColor(0,95,43);
		$this->SetTextColor(255);
		$this->SetDrawColor(218);
		$this->SetLineWidth(.2);
		$this->SetFont('Helvetica','b',9);
		for($i=0;$i<count($columnWidth);$i++)
			$this->Cell($columnWidth[$i],9,$columnName[$i],1,0,'L',true);
		$this->Ln();
	}

	function heading($heading){
		$this->SetDrawColor(218);
		$this->SetFillColor(247);
		$this->SetTextColor(7,101,51);
		$this->SetFont('Helvetica','',12);
		$this->Cell('',9,$heading,1,0,'L',true);
		$this->Ln();
	}
	function longrow($heading){
		$this->SetFillColor(224,235,255);
		$this->SetTextColor(34);
		$this->SetFont('Helvetica','',7);
		$fill = false;
		$this->Cell('',9,$heading,1,0,'L',$fill);
		$this->Ln(12);
	}
	function headingc($heading){
		$this->SetDrawColor(218);
		$this->SetFillColor(247);
		$this->SetTextColor(7,101,51);
		$this->SetFont('Helvetica','',12);
		$this->Cell('',9,$heading,1,0,'C',true);
		$this->Ln();
	}

	function logo(){
		$this->Image('images/religare_logo.jpg',10,6,70);
		//$this->Image('images/religarelogopdf.jpg',10,6,70);
		$this->Ln(12);
	}
	function signature(){
		$this->Image('images/pdfsingnature.jpg',13,160,13);
		$this->Ln(6);
	}
	function content($file){
		$this->SetTextColor(108);
		// Read text file
		$txt = file_get_contents($file);
		// Times 12
		$this->SetFont('Helvetica','',9);
		// Output justified text
		$this->MultiCell(0,5,$txt);
		// Line break
		$this->Ln(6);
	}
	function content1($txt){
		$this->SetTextColor(108);
		// Read text file
		//$txt = file_get_contents($file);
		// Times 12
		$this->SetFont('Helvetica','',9);
		// Output justified text
		$this->MultiCell(0,5,$txt);
		// Line break
		$this->Ln(6);
	}
	function content2($txt){
		$this->SetTextColor(200);
		// Read text file
		//$txt = file_get_contents($file);
		// Times 12
		$this->SetFont('Helvetica','',9);
		// Output justified text
		$this->MultiCell(0,5,$txt);
		// Line break
		$this->Ln(6);
	}
}

$pdf = new PDF();

$pdf->AddPage();
$pdf->logo();
//$pdf->signature();
foreach($_REQUEST as $key=>$value)
{
 $$key=$value;
}
$pdf->heading('Premium Acknowledgement');
$columnWidth = array(65,125);
$columnData = array(array('Policy No. :',@$plandetail[0]['POLICYNUMBER']),array('Client ID :',@$plandetail[0]['CLIENTID']),array('Policyholder :',$plandetail[0]['EMPNAME']),array('Corporate Name :',@$plandetail[0]['COMPANYNAME']),array('Policy Period	:',@$plandetail[0]['POLICYSTARTDATE'].' to '.@$plandetail[0]['POLICYENDDATE']));  
$pdf->FancyTable($columnWidth,$columnData);
/*******************************************/
$pdf->heading('Premium Details');
$columnWidth = array(95,95);
$columnData = array(array('Particulars','Amount (in Rs.)'),array('Gross Premium CARE (incl GST & Levies) :',@$plandetail[0]['TOTALAMOUNT']),array('Total :',@$plandetail[0]['TOTALAMOUNT']));  
$pdf->FancyTable1($columnWidth,$columnData);
$pdf->longrow('The Premium is rounded off to the nearest rupee.');
/*******************************************/
$pdf->heading('Eligibility of Premium for Deduction u/s 80D of the Income Tax Act, 1961');
include_once("invoicetext.php");
$pdf->content1($firststr);
$pdf->signature();
include_once("invoicetext2.php");
$pdf->content1($secondstr);
//$pdf->content2($secondstr);
$certificatefilenane = '80DCertificate-'.@$plandetail[0]['POLICYNUMBER'].'.pdf';
$pdf->Output('template/'.@$certificatefilenane,'F');
?>
