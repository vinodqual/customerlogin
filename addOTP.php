<?php 
include("conf/conf.php");
include("conf/common_functions.php");
include("inc/inc-header-registration.php"); 
?>
<style>
.otp-errors{
	margin-bottom: 13px;
	height:30px;
	color:red;
	margin-left:3px;
}
div.error {
	position:absolute;
	margin-top:3px;
	color:red;
	padding:3px;
	text-align:left;
	z-index:1;
	margin-left: -47px;
}
.owl-theme .owl-controls {
margin-top: -146px;
}
</style>
<script src="js/alice.js" type="text/javascript"></script>
</head>
<body>
<!-- header -->
<div class="headerMain">
<?php include('pphcheader.php'); ?>
<link rel="stylesheet" type="text/css" href="css/datepiker.css" >
</div>
<!-- body container -->
<section id="body_container">
  <div class="right-Cont">
    <h1>Activate Account Help</h1>   
    <!-- Heding top  closed-->
    <div class="clearfix"></div>
    <div class="spacer4"></div> 
    <!-- Step start --> 
    <div class="right-Cont-step1 ">
      <div class="spacer3"></div>
	  <div class="otp-errors"></div>
	  <div class="otp-success"></div>
	  <form id="otpForm" name="otpForm" method="post">
      <div class="middleContainerBox space-marging-1 " id="otpDiv">
        <div class="graytxtBox3 space-marging "><span class="txtIcon1"></span>		
          <div class="txtMrgn ">
            <input type="text" value="OTP *" maxlength="7" class="txtfieldFull" id="otp" name="otp" onFocus="if (this.value == 'OTP *') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'OTP *';}">			
          </div>
        </div>		
        <div class="right-Cont-step1-button-custom2" id="confirmOTP"> <a href="#">Confirm <img src="images/arrow.png"  border="0" alt="" title=""></a>
		<div id="ajaxLoading" style="display:none; float:right; position:absolute; margin-left:10px;">
		Please wait...<img class="doc_load_img" alt="Loading.." src="images/ajax-loader_12.gif">
		</div>
		</div>		
          </div>		  
		  </form>  
        <div class="spacer3"></div>
      </div>
      <div class="spacer4"></div>
    </div>
  </div> 
  <!-- Left container -->
  <div class="Left-Cont">
      <?php include("inc/inc.left-registration.php");?>
    </div>
  </div>
</section>
<div class="clearfix"></div>
<!-- body container --> 
<!-- footer -->
<?php include("inc/inc.ft-registration.php"); ?>
<!-- footer closed --> 
<script type="text/javascript">
$("#otp").keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
        //$("registrationForm").submit();
		$('#confirmOTP').trigger('click');   
    }
});
</script>
<script type="text/javascript">
$("#confirmOTP").click(function () {
    var otp = $("#otp").val();
    if(otp == 'OTP *' || otp =='')
    {
		$(".otp-errors").text("Please fill OTP.").show();
        return false;
    }
    if(!$.isNumeric(otp) || otp.length!=7)
    {
		$(".otp-errors").text("OTP is not correct.").show();
        return false;
    }
    if($.isNumeric(otp) && otp.length==7)
    {
        $.ajax({
			type: 'POST',
			data: {code: otp},
			url: 'validateAccountOTP.php',
			beforeSend: function () {
			$("#ajaxLoading").css("display", "inline");
			$("#ajaxLoading").show();
		   },
			success: function(data){
				if(data == 'success')
				{
					window.location.href="index.php?acactivated=yes";
				}
				else
				{
					$(".otp-errors").text("OTP is not correct.").show();
				}
			},
			complete: function(){
			$('#ajaxLoading').hide();
			}
        })
    }
});
</script> 
<script src="js/owl.carousel.js"></script> 
<!-- Demo --> 
<script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true
      });
    });
    </script>
	
<?php 
if($browser['browser'] == 'IE' && $browser['version'] == '8.0'){ ?>
<script type="text/javascript" src="js/jquery.js"></script>   
<?php } else {  ?>
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<?php } ?>
<script src="js/datepiker.js"></script> 
<script>
 var DT = jQuery.noConflict();
DT(function() {
DT('#datepicker').datepicker( {
        changeMonth: true,
        changeYear: true,
	    yearRange: "-99:-0",
	   dateFormat: "yy-mm-dd"
 
    });
});
</script>
</div>
</body>
</html>