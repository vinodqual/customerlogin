<?php include("inc/inc.hd.php"); 
//print_r($_SESSION['policyexpiredisables']); die;
?>
  <link rel="stylesheet" type="text/css" href="css/custom-scrollbar.css" >
<section id="middleContainer">
  <div class="container-fluid">
    <div class="middlebox">
      <div class="col-md-9">
      
      <div class="dashboard-leftTop">
      	<div class="grayBorderdashboard">
        <div class="dashboardName">
        	
        	
            <?php if(isset($_SESSION['policyNumber']) && !empty($_SESSION['policyNumber'])){ ?>
            <div class="colSm6">
                <strong>
                <?php echo @$_SESSION['policyNumber']?@$_SESSION['policyNumber']:"NA"; ?>
                </strong>
				<?php if(isset($_SESSION['productName']) && !empty($_SESSION['productName'])) { $productname= $_SESSION['productName']; } else { $productname='NA'; }  if($_SESSION['LOGINTYPE']=='RETAIL'){ echo " [ ".$productname." ] "; } ?>
                </div>
            <?php } else { ?>
            <div class="colSm6" style="width:100%">
                Welcome <?php if(@$_SESSION['OTPDetail'] != array()){
					echo @$_SESSION['OTPDetail'] ['firstName'] ."&nbsp;". @$_SESSION['OTPDetail']['lastName'];
				}
				else{
					echo @$_SESSION['firstName'] ."&nbsp;". @$_SESSION['lastName']; 
				}?>,<br />
                You currently do not have any policy registered with us, <a href="individual-policy-registration.php">Click here</a> to Register your policy or Click any of the below products to Buy a Policy First!
                </div>
                <?php } ?> 
            
            <!--<div class="colSm7"><a href="#" ><img src="images/icon-dashboard.jpg" /></a></div> -->
        <div class="clearfix"></div>
        </div>
        <?php if(isset($_SESSION['policyNumber']) && !empty($_SESSION['policyNumber'])){
		 if($_SESSION['LOGINTYPE']=='CORPORATE'){ ?>
	        <table class="responsive responsivedash" width="100%">
                    <tr>
                                <td><span style="font-weight:bold;">Name</span></td>
                                <td><span style="font-weight:bold;">Type</span></td>
                                <td><span style="font-weight:bold;">Age</span></td>
                                
                                <td class="text-right">  <div style="font-weight:bold;"  > Policy Documents</div>  </td>
                                
                     </tr>
             <?php if(!empty($responseArray)){
			if((isset($responseArray[0]['BGEN-MEMNAME']['#text'])) && ($responseArray[0]['BGEN-MEMNAME']['#text'] != "")){
			$i=0;
			$membersarray = array();
				foreach($responseArray as $key => $response){	
						if($key == 0){
						$_SESSION['memberId']=trim(@$response['BGEN-CLNTNM']['#text']);
						$_SESSION['corporateId']=trim(@$_SESSION['owner_number']);
						//$_SESSION['corporateId']=trim(@$response['BGEN-OWNERNUM']['#text']);
						$_SESSION['policyHolderName']=trim($response['BGEN-MEMNAME']['#text']);
						$_SESSION['policyHolderAge']=trim($response['BGEN-AGE']['#text']);
						$_SESSION['policyHolderMember']=trim($response['BGEN-DPNTTYP']['#text']);
						$_SESSION['policyHolderSI']=trim($response['BGEN-ZBALCURR']['#text']);
						if(trim($response['BGEN-GENDER']['#text'])=="F"){
							$_SESSION['GENDER']="Female";
						}
						if(trim($response['BGEN-GENDER']['#text'])=="M"){
							$_SESSION['GENDER']="Male";
						}
						if(trim($response['BGEN-GENDER']['#text'])=="F"){
							$genderIcon='femaleIcon';
						}
						if(trim($response['BGEN-GENDER']['#text'])=="M"){
							$genderIcon='maleIcon';
						}
						
						$membersarray[] = strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);
						
				?>

                  <tr>
                    <td><span class="<?php echo $genderIcon; ?>"></span><?php echo ucwords(strtolower(trim($response['BGEN-MEMNAME']['#text'])));?></td>
                    <td><?php echo ucwords($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);?></td>
                    <td><?php echo(trim($response['BGEN-AGE']['#text']));?></td>
                    
                    <td class="text-right">
                    	<a  <?php if($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs']!='NO'){ ?>   data-toggle="modal" data-target="#exampleModal1_<?php echo @$_SESSION['policyNumber']; ?>"  href="view_policy_docs.php?id=<?php echo base64_encode(@$_SESSION['policyNumber']);?>" <?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } ?>>   						
                        <span class="editIcon" title="Policy Documents"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <a  <?php if($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs']!='NO'){ ?> onclick="javascript:window.open('download_pdf.php?clientNo=<?php echo base64_encode(trim($response['BGEN-CLNTNM']['#text']));?>','parentWindow','width=790,height=480,scrollbars=yes,menubar=no,status=yes,resizable=yes,directories=false,location=yes,left=0,top=0')" href="javascript:void(0)" class="pdf_nav"<?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! '); return false;" <?php } ?> >
                        <span class="healthcardeditIcon" title="Health Card"></span></a>
                        
                        <a  <?php if($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs']!='NO'){ ?> onclick="javascript:window.open('download_pdf.php?clientNo=<?php echo base64_encode(trim($response['BGEN-CLNTNM']['#text']));?>&policy=policy','parentWindow','width=790,height=480,scrollbars=yes,menubar=no,status=yes,resizable=yes,directories=false,location=yes,left=0,top=0')" href="javascript:void(0)" class="pdf_nav" <?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } ?> >
						<span class="umbIcon" title="Policy PDF"></span></a>
										<div class="modal fade" id="exampleModal1_<?php echo @$_SESSION['policyNumber']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
										  <div class="modal-dialog" role="document">
											<div class="modal-content">
											  <div class="modal-header">
												<div class="model-header" id="exampleModalLabel">Policy Document Details</div>
											  </div>
											 Please Wait....
											  
											</div>
										  </div>
										</div>           

                        <!--<span class="umbIcon" data-toggle="modal" data-target="#exampleModal1"></span>-->
                        </td>
                       
                  </tr>
			<?php } else{ 
						$membersarray[] = strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);
					
						if(trim($response['BGEN-GENDER']['#text'])=="F"){
							$genderIcon='femaleIcon';
						}
						if(trim($response['BGEN-GENDER']['#text'])=="M"){
							$genderIcon='maleIcon';
						}
						
            //$membersarray[] = strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);
            ?>
                  <tr>
                    <td><span class="<?php echo $genderIcon; ?>"></span><?php echo ucwords(strtolower(trim($response['BGEN-MEMNAME']['#text'])));?></td>
                    <td><?php echo ucwords($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);?></td>
                    <td><?php echo(trim($response['BGEN-AGE']['#text']));?></td>
                   
                    <td class="text-right">
                    	<a data-toggle="modal"   <?php if($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs']!='NO'){ ?> data-target="#exampleModal1_<?php echo @$_SESSION['policyNumber']; ?>" href="view_policy_docs.php?id=<?php echo base64_encode(@$_SESSION['policyNumber']);?>" <?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } ?>>   						
                        <span class="editIcon" title="Policy Documents"></span></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                       
                        
                         <a   <?php if($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs']!='NO'){ ?> onclick="javascript:window.open('download_pdf.php?clientNo=<?php echo base64_encode(trim($response['BGEN-CLNTNM']['#text']));?>','parentWindow','width=790,height=480,scrollbars=yes,menubar=no,status=yes,resizable=yes,directories=false,location=yes,left=0,top=0')" href="javascript:void(0)" class="pdf_nav" <?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } ?> >
                        <span class="healthcardeditIcon" title="Health Card"></span></a>
                    
						<a   <?php if($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs']!='NO'){ ?> onclick="javascript:window.open('download_pdf.php?clientNo=<?php echo base64_encode(trim($response['BGEN-CLNTNM']['#text']));?>&policy=policy','parentWindow','width=790,height=480,scrollbars=yes,menubar=no,status=yes,resizable=yes,directories=false,location=yes,left=0,top=0')" href="javascript:void(0)" class="pdf_nav"<?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } ?> >
						<span class="umbIcon" title="Policy PDF"></span></a>
										<div class="modal fade" id="exampleModal1_<?php echo @$_SESSION['policyNumber']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
										  <div class="modal-dialog" role="document">
											<div class="modal-content">
											  <div class="modal-header">
												<div class="model-header" id="exampleModalLabel">Policy Document Details</div>
											  </div>
											 Please Wait....
											  
											</div>
										  </div>
										</div>           
						
						</td>
                        
                  </tr>
			<?php }?>
            <?php $i++; }
            $_SESSION['membersarray'] = $membersarray;
            } else {?>
                  <tr>
                    <td colspan="4">
					<?php if(isset($responseError)){?>
                    <font color="#cc0000"><b><?php echo $responseError;?></b></font>
                    <?php } else if(!empty($_SESSION['response_error'])){?>
                    <font color="#cc0000"><b><?php echo $_SESSION['response_error'];?></b></font>
                    <?php }else {?>
                    <font color="#cc0000"><b>No record(s) found.</b></font>
                    <?php }?>
                    </td>
                  </tr>    
			<?php } } else {?>
                    <tr>
                        <td colspan="10" align="center" style="text-align:-moz-center;border:none;"><font color="#cc0000"><b>Please Try Again or </b></font><a href="individual-policy-registration.php" target="_blank">Register your policy</a></td>
                    </tr>
			<?php } ?>                                         
                </table>
		<?php } else { ?>
       		<table class="responsive responsivedash" width="100%">
                <tr>
                    <td><span style="font-weight:bold;">Name</span></td>
                    <td><span style="font-weight:bold;">Type</span></td>
                    <td><span style="font-weight:bold;">Age</span></td>
                   
                    <td class="text-right"> <div style="font-weight:bold;"> Policy Documents</div> </td>
                   
                </tr>
			 <?php if(!empty($responseArray)){
                if((isset($responseArray[0]['BGEN-MEMNAME']['#text'])) && ($responseArray[0]['BGEN-MEMNAME']['#text'] != "")){
                $i=0;
                $membersarray = array();
                    foreach($responseArray as $key => $response){	
                            if($key == 0){
								$_SESSION['corporateId']=trim(@$_SESSION['owner_number']);
                            if(trim($response['BGEN-GENDER']['#text'])=="F"){
                                $_SESSION['GENDER']="Female";
								$genderIcon='femaleIcon';
                            }
                            if(trim($response['BGEN-GENDER']['#text'])=="M"){
                                $_SESSION['GENDER']="Male";
								$genderIcon='maleIcon';
                            }
                            
                            //$membersarray[] = strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);
            ?>

                  <tr>
                    <td><span class="<?php echo $genderIcon; ?>"></span><?php echo ucwords(strtolower(trim($response['BGEN-MEMNAME']['#text'])));?></td>
                    <td><?php echo ucwords($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);?></td>
                    <td><?php echo(trim($response['BGEN-AGE']['#text']));?></td>
                    
                    <td class="text-right">
                    	<a style="display:none"  <?php if($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs']!='NO'){ ?> onclick="javascript:window.open('download_pdf.php?clientNo=<?php echo base64_encode(trim($response['BGEN-CLNTNM']['#text']));?>','parentWindow','width=790,height=480,scrollbars=yes,menubar=no,status=yes,resizable=yes,directories=false,location=yes,left=0,top=0')" href="javascript:void(0)" class="pdf_nav" <?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } ?> >
                        <span class="healthcardeditIcon" title="Health Card"></span></a>
                        <a   onclick="javascript:window.open('download_pdf.php?clientNo=<?php echo base64_encode(trim($response['BGEN-CLNTNM']['#text']));?>&policy=policy','parentWindow','width=790,height=480,scrollbars=yes,menubar=no,status=yes,resizable=yes,directories=false,location=yes,left=0,top=0')" href="javascript:void(0)" class="pdf_nav">
                        <span class="umbIcon" title="Policy PDF"></span></a>
                        <!--<span class="umbIcon" data-toggle="modal" data-target="#exampleModal1"></span>-->
                        </td>
                       
                  </tr>
			<?php } else{ 
					if(trim($response['BGEN-GENDER']['#text'])=="F"){
						$genderIcon='femaleIcon';
					}
					if(trim($response['BGEN-GENDER']['#text'])=="M"){
						$genderIcon='maleIcon';
					}
            //$membersarray[] = strtolower($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);
            ?>
                  <tr>
                    <td><span class="<?php echo $genderIcon; ?>"></span><?php echo ucwords(strtolower(trim($response['BGEN-MEMNAME']['#text'])));?></td>
                    <td><?php echo ucwords($relationArray[trim($response['BGEN-DPNTTYP']['#text'])]);?></td>
                    <td><?php echo(trim($response['BGEN-AGE']['#text']));?></td>
                    
                    <td class="text-right">
                    	<a  style="display:none"  <?php if($_SESSION['policyexpiredisables']['mypolicy.disable_download_policy_docs']!='NO'){ ?> onclick="javascript:window.open('download_pdf.php?clientNo=<?php echo base64_encode(trim($response['BGEN-CLNTNM']['#text']));?>','parentWindow','width=790,height=480,scrollbars=yes,menubar=no,status=yes,resizable=yes,directories=false,location=yes,left=0,top=0')" href="javascript:void(0)" class="pdf_nav"<?php } else { ?>  onclick="alert('You can\'t download policy document as your policy is expired ! ')" <?php } ?>  >
                        <span class="healthcardeditIcon" title="Health Card"></span></a>
                        <a  onclick="javascript:window.open('download_pdf.php?clientNo=<?php echo base64_encode(trim($response['BGEN-CLNTNM']['#text']));?>&policy=policy','parentWindow','width=790,height=480,scrollbars=yes,menubar=no,status=yes,resizable=yes,directories=false,location=yes,left=0,top=0')" href="javascript:void(0)" class="pdf_nav">
                        <span class="umbIcon" title="Policy PDF"></span></a>
                        <!--<span class="umbIcon" data-toggle="modal" data-target="#exampleModal1"></span>-->
                        </td>
                  </tr>
			<?php }?>
            <?php $i++; }
           // $_SESSION['membersarray'] = $membersarray;
            } else {?>
                  <tr>
                    <td colspan="4">
					<?php if(isset($responseError)){?>
                    <font color="#cc0000"><b><?php echo $responseError;?></b></font>
                    <?php } else if(!empty($_SESSION['response_error'])){?>
                    <font color="#cc0000"><b><?php echo $_SESSION['response_error'];?></b></font>
                    <?php }else {?>
                    <font color="#cc0000"><b>No record(s) found.</b></font>
                    <?php }?>
                    </td>
                  </tr>    
			<?php } } else {?>
                    <tr>
                        <td colspan="10" align="center" style="text-align:-moz-center;border:none;"><font color="#cc0000"><b>Please Try Again or </b></font><a href="individual-policy-registration.php" target="_blank">Register your policy</a></td>
                    </tr>
             <?php } ?>
                </table>
			<?php } }?>
        </div>
      </div>
      <?php if(count($freeservices)>0){ ?>
        <div class="dashboard-left">
          <div id="Our_clients">
            <div class="border-box">
              <div class="title-bg textleft">
                <h1>Free Value Added Services</h1>
              </div>
              <!-- Logo Here -->
              <div>
                <div class="brands owl-carousel">
                <?php $f=0;
				while($f<count($freeservices)){ 
				
				$pattern1 = '/parentpolicy/i';
				$pattern2 = '/supertopup/i';
				$pattern3 = '/preventive/i';
				$pattern4 = '/opd/i';
				$removespaces = preg_replace('/\s+/', '',$freeservices[$f]['SUBMODULENAME']);
				if(preg_match($pattern1, $removespaces)){
					$xollink=1;	
				}
				if(preg_match($pattern2, $removespaces)){
					$xollink=1;	
				}
				if(preg_match($pattern3, $removespaces)){
					$xollink=1;	
				}
				if(preg_match($pattern4, $removespaces)){
					$xollink=1;	
				}
				if(@$xollink==1){
					$target='target="_blank"';
				}
				$employeeUrl=base64_encode("data:" . @$resxolaccess['EMAILID'] . ":" . @$resxolaccess['CORPORATEUNIQUECODE'] . ":" . @$resxolaccess['TOPUPPLANEMPLOYEEID']. ":" . @$resxolaccess['CORPORATEID'].":source-customer");
				$xollink = $xolcorpurl . $employeeUrl;
				?>
                  <div class="item dashboard-left_box"><a <?php echo @$target; ?> href="<?php echo $freeservices[$f]['LINK']?$freeservices[$f]['LINK']:''; ?>">
                    <div class="col-md-4 textCenter"><img src="<?php echo $imgURL.$freeservices[$f]['FILENAME']; ?>" class="img-responsive" title="" alt=""></div>
                    <div class="col-md-8 textbold" align="left" ><?php echo $freeservices[$f]['SUBMODULENAME']?stripslashes($freeservices[$f]['SUBMODULENAME']):'NA'; ?>
                    <p><?php echo substr(stripslashes($freeservices[$f]['DESCRIPTION']),0,35); ?></p>
                    
                    </div>
                    </a> </div>
                 <?php $f++; } ?>   
                </div>
              </div>
              <!-- Logo Here Closed --> 
            </div>
          </div>
        </div>
        <?php } if(count($paidservices)>0){ ?>
        <div class="dashboard-left">
          <div id="Our_clients">
            <div class="border-box">
              <div class="title-bg textleft">
                <h1>Paid Value Added Services</h1>
              </div>
              <!-- Logo Here -->
              <div>
                <div class="brands owl-carousel ">
                <?php $p=0;
				while($p<count($paidservices)){ 
				 $link=$paidservices[$p]['LINK']?$paidservices[$p]['LINK']:'';
				
				?>
                  <div class="item dashboard-left_box"><a href="<?php echo $link?$link:stripslashes($paidservices[$p]['DISPLAYHEALTHCHECKUPLINK']); ?>">
                    <div class="col-md-4 textCenter"><img src="<?php echo $imgURL.$paidservices[$p]['FILENAME']; ?>" class="img-responsive" title="" alt=""></div>
                    <div class="col-md-8 textbold" align="center"><?php echo $paidservices[$p]['SUBMODULENAME']?stripslashes($paidservices[$p]['SUBMODULENAME']):'NA'; ?>
                    <p><?php echo substr(stripslashes($paidservices[$p]['DESCRIPTION']),0,35); ?>
</p>
                    </div>
                    </a> </div>
                 <?php $p++; } ?>   
                </div>
              </div>
              <!-- Logo Here Closed --> 
            </div>
          </div>
        </div>
        <?php } if(count(@$productlist)>0){ ?>
        <div class="dashboard-left">
          <div id="Our_clients">
            <div class="border-box">
              <div class="title-bg textleft">
                <h2>Stay worry free..Hamesha! <span>with our products</span></h2>
              </div>
              <!-- Logo Here -->
              <div>
                <div class="brands owl-carousel ">
                   <?php $pr=0; if(count(@$productlist)>0){
					   
					while($pr<count($productlist)){ ?>
                    <div class="item dashboard-left_box1"><a target="_blank" href="<?php echo @$productlist[$pr]['EXTERNALLINK']; ?>">
                    <div class="col-md-12 " align="center"><img src="<?php echo $imgURL.$productlist[$pr]['BANNER']; ?>" title="" alt=""></div>
                    <div class="col-md-12 textboldProducts" align="center"><?php $desc=@$productlist[$pr]['DESCRIPTION']?stripslashes(@$productlist[$pr]['DESCRIPTION']):'NA'; $desc=str_replace("<p>","",$desc); $desc=str_replace("</p>","",$desc); echo $desc; ?></div>
                    </a> </div>
    	           <?php $pr++; } } ?>   
                </div>
              </div>
              <!-- Logo Here Closed --> 
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <div class="col-md-3">
         <?php include("inc/inc.right.php"); ?>
      </div>
    </div>
  </div>
</section>
<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="model-header" id="exampleModalLabel">Policy Documents</div>
      </div>
      <div class="modal-middle">
        <table class="responsive" width="100%">
                  <tr>
                    <th>#</th>
                    <th>Title</th>
                    <th>File  Name</th>
                    <th class="text-right">Download</th>
                  </tr>
                 <?php $list=fetchDocumentList($_SESSION['policyNumber']); 
					if(count($list) > 0){
					
					$w=0;
					$g=1;
					while($w<count($list)){
						$doclist =@$list[$w]['FILENAME'];
						if(isset($doclist) && !empty($doclist)){
						$res = explode(',',$doclist);
					}
					if(count(@$res)>0){
					$k=$g;
					for($i=0;$i<=count($res);$i++){ 
					if(@$res[$i]!=''){
					?>
  
                  <tr>
                    <td><?php echo $k++; ?></td>
                    <td><?php echo stripslashes($list[0]['TITLE']); ?></td>
                    <td><?php echo @$res[$i]; ?></td>
                    <td class="text-right"><a href="download.php?fileName=<?php echo @$res[$i]; ?>"><span class="downloadIcon"></span></a></td>
                  </tr> 
				  <?php } }
                  } 
                  $w++; $g++; }
                  } else {
                  ?>
                  <tr>
                    <td colspan="4">No Document Uploaded</td>
                  </tr>  
				  <?php
                  }
                   ?>  
                </table>
      </div>
      
    </div>
  </div>
</div>
<?php include("inc/inc.ft.php"); ?>

