<?php include("inc/inc.hd.php");
$productSetupDetail=fetchcolumnListCond("WELLNESSHRASTART,WELLNESSHRAEND","CRMPOLICY"," WHERE POLICYNUMBER='".@$_SESSION['policyNumber']."' ");
?>
<style>
.assessment{
	background: #5e9d2d none repeat scroll 0 0;
	border: medium none;
	color: #fff;
	float: right;
	font-size: 24px;
	font-weight: 600;
	margin: 0;
	padding: 5%;
	text-shadow: 1px 1px #6e6e6e;
	width: 100%;
}
</style>
<section id="middleContainer">
   <div class="container-fluid">
        <div class="middlebox"> 
            <div class="col-md-9">
               <div class="dashboard-leftTop">
                   <div class="topTittle">
                       <h1><img src="img/health-check-up.png" alt="" title="">Health Risk Assessment</h1>
                   </div>
                     <div class="myPlanForm" style="min-height:125px;">
                       
              <div class="clearfix"></div>
              <p>Health Risk Assessment is an online questionnaire based application, which empowers you analyze your health status and identify health risks early. HRA helps in early identification and management of risks, promotion of preventive healthcare, regular follow up and monitoring to ensure effective management of health status.</p>
               <div class="col-sm-3">
                <div class="tab-mid-container-buy"> 
                    <?php
                    $memberid="";
                    if(isset($_SESSION['customerId']) && !empty($_SESSION['customerId']))
                    {
                       $memberid= $_SESSION['customerId'];
                    }
                    else if(isset($_SESSION['memberId']) && !empty($_SESSION['memberId']))
                    {
                       $memberid= $_SESSION['memberId'];
                    }
                    else if(isset($_SESSION['customerIdCheck']) && !empty($_SESSION['customerIdCheck']))
                    {
                       $memberid= $_SESSION['customerIdCheck'];
                    }
                    if(isset($_SESSION['corporateId']) && !empty($_SESSION['corporateId'])) 
                  {
                  $corporateId=   $_SESSION['corporateId'];
                  }
                  else if(isset($_SESSION['owner_number']) && !empty($_SESSION['owner_number'])) 
                  {
                       $corporateId=   $_SESSION['owner_number'];
                  }
                  else
                  {
                      $corporateId=  "";
                  }
                    ?>
                      <form name="hra_form" id="hra_form"  target="_blank" method="post" action="<?php echo $hraURL; ?>">
            <input type="hidden" name="policy_id" value="<?php echo @$_SESSION['policyNumber'];?>" />
            <input type="hidden" name="member_id" value="<?php echo @$memberid?$memberid:'';  ?>" />
            <input type="hidden" name="corporate_id" value="<?php echo @$corporateId;?>" />
            <input type="hidden" name="enable_hra_form" value="Y" />
            <?php if($_SESSION['LOGINTYPE']=='CORPORATE'){ ?>
            <input type="hidden" name="start_date" value="<?php echo date("Y-m-d",strtotime(@$productSetupDetail[0]['WELLNESSHRASTART']));?>" />
            <input type="hidden" name="end_date" value="<?php echo date("Y-m-d",strtotime(@$productSetupDetail[0]['WELLNESSHRAEND']));?>" />
            <?php } else { ?>
            <input type="hidden" name="start_date" value="<?php echo date("Y-m-d",strtotime(@$_SESSION['POLICYSTARTDATE']));?>" />
            <input type="hidden" name="end_date" value="<?php echo date("Y-m-d",strtotime(@$_SESSION['POLICYENDDATE']));?>" />
            <?php } ?>
            <input type="hidden" name="gender" value="<?php echo @$_SESSION['GENDER'];?>" />
             <input type="submit" name="button2" id="button2" value="Assessment" class="assessment" style="float:right;" >
            </form>
                    
                     </div>
               </div>
                     </div>
         
        </div>
     
            </div>  
            <div class="col-md-3">
                <?php include("inc/inc.right.php"); ?>
            </div> 
        </div>
    </div>
</section>

<?php include("inc/inc.ft.php"); ?>
  