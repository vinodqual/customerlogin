<?php 
/*****************************************************************************
* COPYRIGHT
* Copyright 2015 Catabatic Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: view_reimbursement.php,v 1.0 2015/10/07 05:30:15 amit kumar dubey Exp $
* $Author: amit kumar dubey $
*
****************************************************************************/
include_once("conf/conf.php");           //include configuration file 
include_once("conf/common_functions.php");          // include function file
include_once("conf/session.php");		//including session file 
$requestId=sanitize_data(base64_decode(@$_REQUEST['id']));		//decode requested id

if($_SESSION['LOGINTYPE']=='CORPORATE'){
	$checkReimbursement=checkReimbursementRequest($requestId,@$_SESSION['policyNumber']);	
 } else {
	$checkReimbursement=checkRetReimbursementRequest($requestId,@$_SESSION['policyNumber']);	
 }
 

$reimbursementDoc=array();
if(count($checkReimbursement)>0){
	if($_SESSION['LOGINTYPE']=='CORPORATE'){
		$reimbursementDoc=fetchReimbursementDocList($requestId);// fetch reimbursement doc
	} else{
		$reimbursementDoc=fetchRetReimbursementDocList($requestId);// fetch reimbursement doc
	}
}else{
	$reimbursementDoc="";
	
	
}
?>
<script>
     $(function(){
         $('.closeBox').click(function(){
		//location.reload();
		  });
      });
 </script>


    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close closeBox" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="model-header" id="exampleModalLabel">Reimbursement Doc List</div>
      </div>
      <div class="modal-middle" id="reimbursmentdoc">
                    <table class="responsive" width="100%">
					  <tr>
						<th>#</th>
						<th>Title</th>
						<th>File  Name</th>
						<th class="text-right">Download</th>
					  </tr>
					 <?php if(count($reimbursementDoc)>0) {
					$r = 1; for($i=0;$i<count($reimbursementDoc);$i++){
							if($i%2==0){
							$class = 'class="whitebg" ';
							} else {
							$class='';
							}
					?>
					 
					
	  
					  <tr>
						<td><?php echo $r++;  ?></td>
						<td><?php echo stripslashes(@$reimbursementDoc[$i]['DOCTITLE']);?></td>
						<td><?php echo @$reimbursementDoc[$i]['FILENAME'];?></td>
<!--						<td class="text-right"><a href="download.php?fileName=<?php //echo @$reimbursementDoc[$i]['FILENAME'];?>"><span class="downloadIcon"></span></a></td>-->
                                                
                                                <!----------------------- Code added for download reimbursement document ---------->
                                                <td class="text-right">
                                                    <?php if(isset($reimbursementDoc[$i]['FILENAME']) && !empty($reimbursementDoc[$i]['FILENAME']) && !empty($_SESSION['policyNumber'])){
                                                        $folder_name=str_replace('##POLICY_NO##',$_SESSION['policyNumber'],REIMBURSEMENT_DOC);
                                                        $url=create_url($folder_name,$reimbursementDoc[$i]['FILENAME'],CUSTOMERLOGIN);        // Create File Download Link
                                                        if($url!==FALSE){
                                                            echo '<a href="'.$url.'"><span class="downloadIcon"></span></a>';
                                                        }else{
                                                            echo SFTP0026;
                                                        }
                                                        
                                                    } else {
                                                        echo 'NA';
                                                    } ?>
                                                </td>
                                                <!------------- End of Code ---------------->
                                                
                                                
					  </tr> 
					  <?php }
					  }else {?>
					  <tr>
						<td colspan="4"><?php echo "No Downloads Available";?></td>
					  </tr>  
					  <?php
					  }
					   ?>  
					</table>      
      </div>
      
    </div>
                     