<?php

/* * ***************************************************************************
 * COPYRIGHT
 * Copyright 2016 Qualtech Consultants Pvt Ltd.
 * All rights reserved
 *
 * DISCLAIMER
 *
 * $Id: download_pdf.php,v 1.0 25-11-2016$
 * $Author: Rahul Shrivastava $
 * $Desc: Download E-Card without web-services request $
 *
 * ************************************************************************** */
include_once("conf/conf.php");           //include configuration file 
if (!empty($_GET['file'])) {
    include_once("conf/session.php");
    include_once("../religare1/projectconfig/function/function_general.php");
    $file = base64_decode($_GET['file']);
    force_download($file);
    die();
}
if (isset($_SESSION['download_token']) && $_SESSION['download_token'] == $_GET['token']) {
    //unset($_SESSION['download_token']);
    include_once("conf/common_functions.php");          // include function file
    include_once("ajax/tcpdf/tcpdf.php");        // include function file
    include_once($apiurl);

    $policyNumber = $_SESSION['policyNumber'];
    $clientNumber = sanitize_data(base64_decode(@$_GET['clientNo']));

    if (isset($_GET['clientNo'])) {
        if ($_SESSION['LOGINTYPE'] == 'RETAIL') {
            $query2['policyNo'] = $policyNumber;
            $query['policyNo'] = $policyNumber . "-" . $clientNumber;
            if (isset($_GET['policy'])) {
                $pdfLink = getForRetail($query2);
                $fileName = "policy_retail_certificate.pdf";
            } else {
                $pdfLink = getPDFURL($query);
                $fileName = "health_card.pdf";
            }
        } else {
            $query2['policyNo'] = $policyNumber;
            $query['policyNo'] = $policyNumber . "-" . $clientNumber;
            if (isset($_GET['policy'])) {

                $pdfLink = getHealthCardForCorprate($query2);
                $fileName = "policy_certificate.pdf";
            } else {
                //$pdfLink=getHealthCardForCorprate($query2);
                $pdfLink = getPDFURL($query);
                $fileName = "health_card.pdf";
            }
        }

        if (@$pdfLink['Status'] == "SUCCESS") {
            file_put_contents("data/" . @$fileName, base64_decode(@$pdfLink['StreamData']));
//header("Location: display_pdf.php?fileName=".@$clientNumber);
//echo $fileName; die;
            $file = "data/$fileName";
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
            header("Content-Type: application/force-download");
            header("Content-Disposition: attachment; filename=" . basename($file));
            header("Content-Description: File Transfer");
            @readfile($file);
            exit;
        } else {
            echo "Please try after sometime.";
        }
    } else {

        $clientData = explode('/', urldecode(@$_GET['clientId']));
        $dataArr = json_decode($clientData[0]);
        $clientType = sanitize_data(base64_decode($dataArr[0]));
        $clientNumber = sanitize_data(base64_decode($dataArr[1]));
        $clientAge = sanitize_data(base64_decode($dataArr[2]));
        $empNo = sanitize_data(base64_decode($dataArr[3]));
//print_r($empNo);
// create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
        $pdf->SetCreator(PDF_CREATOR);

//$pdf->SetAuthor('Religare Health Insurance');
//$pdf->SetTitle('Health Card');
//$pdf->SetSubject('Health Card');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');
// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
// set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(8, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// add a page
        $pdf->AddPage();

//$pdf->SetFont('ufontscom_urwgothiclbook', '', 7, '', false);
        $pdf->setFontStretching('100%');
        $pdf->setFontSpacing('+0.180mm');
        $pdf->SetFont('helvetica', '', 7.5, '', false);
        $pdf->Image('../religare1/religarehrcrm/GROUP_CARE_Card.jpg', 3, 5, 202, 70);
        $pdf->SetTextColor(77, 77, 77);
        $pdf->Write(18, '', 10, 0, 'L', true, 1, false, false, 0);
        $pdf->Write(6, ltrim($clientType, ". "), 0, 0, 'L', true, 0, false, false, 0);
        $pdf->SetFont('helvetica', '', 6.5, '', false);
        $pdf->setFontStretching('100%');
        $pdf->setFontSpacing('+0.250mm');
        $pdf->Write(7, $clientNumber, 0, 0, 'L', true, 1, false, false, 0);
        $pdf->Write(7, $clientAge . ' yrs', 0, 0, 'L', true, 1, false, false, 0);
        $pdf->SetXY(30, 53);
        $pdf->Write(0, "$empNo");
//Close and output PDF document
        $pdf->Output('health_card.pdf', 'D');
        die;
    }
} else {
    echo "Unauthorised Access!";
    die;
}
?>
