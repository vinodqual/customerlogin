<?php 
/**
 * User authentication by Facebook. 
 * On successfull authentication 
 * 1. Insert user record into RETUSERAUTH (No user found)
 * 2. Insert user record into RETUSERMASTER (No user found).
 * 3. Redirect to dashboard.php (all cases).
 * 
 * @author : Shakti Rana <shakti@catpl.co.in> 08 Dec 2015.
 */

//error_reporting(E_ALL);
//ini_set('display_errors', 1);
require_once 'conf/conf.php'; 
require_once 'conf/common_functions.php';
require_once 'passwordverify/PkcsKeyGenerator.php';
require_once 'passwordverify/DesEncryptor.php';
require_once 'passwordverify/PbeWithMd5AndDes.php';
require_once 'conf/staticFunctions.php'; 
//session_start();

if(@$_POST['userdetail']){    
    $userDetail = json_decode($_POST['userdetail'], true);
    if (@$userDetail['id']) {
        $username = filter_var(@$userDetail['email'], FILTER_SANITIZE_EMAIL);
	$username = $email = strtolower($username);
        $name = explode(' ', @$userDetail['name']);
        $firstName = filter_var(@$name[0], FILTER_SANITIZE_EMAIL);
        $lastName = filter_var(@$name[1], FILTER_SANITIZE_EMAIL);
        //$gender = filter_var($user['gender'], FILTER_SANITIZE_EMAIL);
        //$img = filter_var($user['picture'], FILTER_VALIDATE_URL);

        //$password = $orgpassword =base64_decode($_POST['password']);
        $password = $orgpassword = 'Welcome1$';
        $password = PbeWithMd5AndDes::encrypt($password, $keystring); 

        $_SESSION['LOGINTYPE'] = 'RETAIL';
        $_SESSION['userName']  = $username;    
        $_SESSION['firstName'] = $firstName; 
        $_SESSION['lastName']  = $lastName;
        $createdOn = date('y-m-d');
        //$dob = $createdOn;
        $dob = date('y-m-d');
        $title = (@$userDetail['gender']=='male') ? 'Mr' : 'Mrs';   

        global $conn;
        $fbUser = fetchcolumnListCond('USERID,ISPASSWORDCHANGED', 'RETUSERAUTH', "where LOWER(USERNAME)='".strtolower($username)."'");             
        $rowid = @$fbUser[0]['USERID'] ? $fbUser[0]['USERID'] : '';
        if(@$fbUser[0]['USERID'])
        {
            $_SESSION['USERID'] = $rowid;  
            $_SESSION['ISPASSWORDCHANGED']=  @$fbUser[0]['ISPASSWORDCHANGED'];    
            //@SHAKTIRANA 04/05/2016 15:31
            CustomerLoginFunction::mapPolicyWithProposal($username);
            CustomerLoginFunction::mapPolicyWithPolicymap($username);
            //@SHAKTIRANA 04/05/2016 15:31
            echo 'success';
            exit;
        }
        else
        {
            $userAuthSql = "INSERT INTO RETUSERAUTH (USERID,USERNAME,PASSWORD,USERSTATUS,SOURCE,UNSUCCESSFULATTEMPTS,ISPASSWORDCHANGED) VALUES (RETUSERAUTH_SEQ.NEXTVAL,'$username','$password','ACTIVE','FACEBOOK',0,'YES')";            
            $stdid1 = @oci_parse($conn, $userAuthSql);

            if(@oci_execute($stdid1))
            {
                $check1 = @oci_parse($conn, 'SELECT RETUSERAUTH_SEQ.CURRVAL FROM DUAL');
                @oci_execute($check1);
                $res=@oci_fetch_assoc($check1);
                $rowid=@$res['CURRVAL'];
                $_SESSION['USERID'] = $rowid;
				$_SESSION['ISPASSWORDCHANGED']=  "YES";  
                $userMasterSql = "INSERT INTO RETUSERMASTER(USERID, EMAILID,BIRTHDT,TITLE,FIRSTNAME,LASTNAME,CONTACTNUM,CONTACTPHONENUM,STDCODE,ACCOUNTSTARTDT) VALUES ($rowid,'$email','$dob','$title','$firstName','$lastName','','','','$createdOn')";
                $stdid2 = @oci_parse($conn, $userMasterSql);
                if(@oci_execute($stdid2))
                {
                    $emailmessage ='';
                    $to      = $email;
                    $subject = "You have successfully registered";
                    $emailmessage .= "Dear, ".@$firstName.' '.@$lastName."<br /><br />";
                    $emailmessage .="You have successfully registered.<br />";
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    $headers .= 'From: wellness@religare.com' . "\r\n" .
                                    'Reply-To: wellness@religare.com' . "\r\n" .
                                    'X-Mailer: PHP/' . phpversion();
                    mail($to, $subject, $emailmessage, $headers);                    
                    //@SHAKTIRANA 04/05/2016 15:31
                    CustomerLoginFunction::mapPolicyWithProposal($username);
                    CustomerLoginFunction::mapPolicyWithPolicymap($username);
                    //@SHAKTIRANA 04/05/2016 15:31
                    echo 'success';
                    exit;
                }
                else
                {
                    $deleteAuth = "DELETE FROM RETUSERAUTH WHERE LOWER(USERNAME)='".strtolower($username)."'";
                    $deleteOnErrorAuth = @oci_parse($conn, $delete);
                    @oci_execute($deleteOnErrorAuth);
                    $deleteMaster = "DELETE FROM RETUSERMASTER WHERE LOWER(EMAILID)='".strtolower($username)."'";
                    $deleteOnErrorMaster = @oci_parse($conn, $delete);
                    @oci_execute($deleteOnErrorMaster);
                    unset($_SESSION);  
                    echo 'failureusermaster';
                    exit;
                }
            }
            else
            {                
                $deleteAuth = "DELETE FROM RETUSERAUTH WHERE LOWER(USERNAME)='".strtolower($username)."'";
                $deleteOnErrorAuth = @oci_parse($conn, $delete);
                @oci_execute($deleteOnErrorAuth);
                unset($_SESSION);
                echo 'failureuserauth';
                exit;
            }
        }        
    }   
    else{
        echo 'failureuser';
        exit;
    }
}
?>
