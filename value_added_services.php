<?php include("inc/inc.hd.php");
?>
<section id="middleContainer">
  <div class="container-fluid">
    <div class="middlebox">
      <div class="col-md-9">
      <?php if(count($freeservices)>0){ ?>
        <div class="dashboard-left">
          <div id="Our_clients">
            <div class="border-box">
              <div class="title-bg textleft">
                <h1>Free Value Added Services</h1>
              </div>
              <!-- Logo Here -->
              <div>
                <div class="brands owl-carousel">
                <?php $f=0;
				while($f<count($freeservices)){
				$pattern1 = '/parentpolicy/i';
				$pattern2 = '/supertopup/i';
				$pattern3 = '/preventive/i';
				$pattern4 = '/opd/i';
				$removespaces = preg_replace('/\s+/', '',$freeservices[$f]['SUBMODULENAME']);
				if(preg_match($pattern1, $removespaces)){
					$xollink=1;	
				}
				if(preg_match($pattern2, $removespaces)){
					$xollink=1;	
				}
				if(preg_match($pattern3, $removespaces)){
					$xollink=1;	
				}
				if(preg_match($pattern4, $removespaces)){
					$xollink=1;	
				}
				if(@$xollink==1){
					$target='target="_blank"';
				}
				$employeeUrl=base64_encode("data:" . @$resxolaccess['EMAILID'] . ":" . @$resxolaccess['CORPORATEUNIQUECODE'] . ":" . @$resxolaccess['TOPUPPLANEMPLOYEEID']. ":" . @$resxolaccess['CORPORATEID'].":source-customer");
				$xollink = $xolcorpurl . $employeeUrl;
					
					?>
                  <div class="item dashboard-left_box"><a <?php echo @$target; ?> href="<?php echo $freeservices[$f]['LINK']?$freeservices[$f]['LINK']:@$xollink; ?>">
                    <div class="col-md-4 "><img class="img-responsive" src="<?php echo $imgURL.$freeservices[$f]['FILENAME']; ?>" width="" height="" title="" alt=""></div>
                    <div class="col-md-8 textbold" align="left" ><?php echo $freeservices[$f]['SUBMODULENAME']?stripslashes($freeservices[$f]['SUBMODULENAME']):'NA'; ?> <p><?php echo substr(stripslashes($freeservices[$f]['DESCRIPTION']),0,35); ?></p></div>
                    </a> </div>
                 <?php $f++; } ?>   
                </div>
              </div>
              <!-- Logo Here Closed --> 
            </div>
          </div>
        </div>
        <?php } if(count($paidservices)>0){ ?>
        <div class="dashboard-left">
          <div id="Our_clients">
            <div class="border-box">
              <div class="title-bg textleft">
                <h1>Paid Value Added Services</h1>
              </div>
              <!-- Logo Here -->
              <div>
                <div class="brands owl-carousel ">
                <?php $p=0;
				while($p<count($paidservices)){
					 $link=$paidservices[$p]['LINK']?$paidservices[$p]['LINK']:''; ?>
                  <div class="item dashboard-left_box"><a href="<?php echo $link?$link:stripslashes($paidservices[$p]['DISPLAYHEALTHCHECKUPLINK']); ?>">
                    <div class="col-md-4 "><img src="<?php echo $imgURL.$paidservices[$p]['FILENAME']; ?>" width="" height="" title="" alt=""></div>
                    <div  class="col-md-8 textbold" align="left" ><?php echo $paidservices[$p]['SUBMODULENAME']?stripslashes($paidservices[$p]['SUBMODULENAME']):'NA'; ?> <p><?php echo substr(stripslashes(@$paidservices[$p]['DESCRIPTION']),0,35); ?></p></div>
                    </a> </div>
                 <?php $p++; } ?>   
                </div>
              </div>
              <!-- Logo Here Closed --> 
            </div>
          </div>
        </div>
        <?php } ?>
        
        <?php if(count($freeservices)==0 && count($paidservices)==0){ ?>
        <div class="dashboard-left">
          <div id="Our_clients">
            <div class="border-box">
              <div align="center">
                No <b>Value Added Services</b> are available in your policy
              </div>
              <!-- Logo Here Closed --> 
            </div>
          </div>
        </div>
        <?php } ?>
      </div>
      <div class="col-md-3">
         <?php include("inc/inc.right.php"); ?>
      </div>
    </div>
  </div>
</section>

<?php include("inc/inc.ft.php"); ?>
