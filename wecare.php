<?php  include("inc/inc.hd.php");  ?>
<style>
    .ui-menu-item
    {
        border-bottom: 1px solid #ccc;
    }
    
    .ui-autocomplete li
    {
        font-weight  :normal;
        font-size: 14px;
        padding: 5px !important;
        
    }
     .topStar{float:none !important;}
     .bottomNav{clear: both !important}
    
    .ui-autocomplete  
    {
        font-weight  :normal;
        font-size: 14px;
        padding: 5px !important;
        
    }
     .ui-menu-item:last-child {
   border-bottom: none;
}
.ui-menu{
    border:1px solid #ccc;
}
/*    .ui-menu .ui-menu-item
    {
        
    }*/
    div.cs-selectwidth
    {
        width:195px;
    }
    
/*    ::-webkit-scrollbar{
  width:0px;   
  background:transparent;
}*/
    ::-moz-scrollbar{
  width:0px;   
  background:transparent;
}
scrollbar {
/*  clear useragent default style*/
   -moz-appearance: none !important;
}
/* buttons at two ends */
scrollbarbutton {
   -moz-appearance: none !important;
}
/* the sliding part*/
thumb{
   -moz-appearance: none !important;
}
scrollcorner {
   -moz-appearance: none !important;
   resize:both;
}
/* vertical or horizontal */
scrollbar[orient="vertical"] {
    color:silver;
}
</style>
<section id="middleContainer">
    <div class="container-fluid">
        <div class="middlebox"> 
            <div class="col-md-9">
                 <div class="dashboard-leftTop">
                        <div class="topTittle"><img title="" alt="" src="img/wecareIcon.png">Discount Connect 
                            <form  class="sorting">
                          <select class="cs-select cs-selectwidth cs-skin-border fa arrow " name="sorting_id" id="sorting_id" style="float:right;" onclick="search_by_button();"> 
                                        <option value="DOCTORNAME ASC"  >Doctor Name(A-Z)</option>
                                        <option value="DOCTORNAME DESC" >Doctor Name(Z-A)</option>
                                        <option value="RATING ASC"       >Rating-Low </option>
                                        <option value="RATING DESC"      >Rating-High</option>
                                        <option value="DISCOUNTOFFER ASC" >Discount-Low </option>
                                        <option value="DISCOUNTOFFER DESC" >Discount-High</option> 
                            </select>
                                </form>
                            </div>
                        <div class="myPlanForm">  
                            <?php
                            
                        $city_name= sanitize_data(@$_POST['city_name']);
                        $city_id= sanitize_data_without_slash($_POST['city_id']);
                       
                         $keyword= sanitize_data($_POST['keyword']);
                         $search_keyword_virtual= sanitize_data_without_slash($_POST['search_keyword_virtual']);
                       
                     
                            if (@$city_name) {
                                $loc_name_care = $city_name;
                            } else if (@$loc_name) {
                                $loc_name_care = $loc_name;
                            }
                            if (@$city_id) {
                                $loc_id_care = $city_id;
                            } else if (@$loc_id) {
                                $loc_id_care = $loc_id;
                            }
                            ?> 
                            <input type="text" name="textfield" id="search_city" onkeyup="city_function()" class="locationtextfield" value="<?php echo @$loc_name_care?$loc_name_care:'Location*' ;?>" onFocus="if (this.value =='<?php echo @$loc_name_care?$loc_name_care:'Location*' ;?>') {this.value = '';}" onBlur="if (this.value == '') {this.value = '<?php echo @$loc_name_care?$loc_name_care:'Location*' ;?>';}" >
                            <input id="city_id" type="hidden" class="txtfield_185"  value="<?php echo @$loc_id_care; ?>"  />
                            <input type="text" name="textfield2"  id="search_keyword"    class="doctorsrchtextfield" value="<?php echo @$keyword?$keyword:'Doctors, Centers, Specialities, Services' ;?>" onFocus="if (this.value == 'Doctors, Centers, Specialities, Services') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Doctors, Centers, Specialities, Services';}" >
                            <input id="search_keyword_virtual" type="hidden" class="txtfield_185"  value="<?php echo @$search_keyword_virtual; ?>"  />
                             <input type="submit" class="greenSrchBtn"  onclick="search_by_button()" value="Submit" id="button" name="button"   style="line-height:20px;"  >
                        <div class="clearfix"></div>
                        
                        </div> 
                      
                </div>
                <div align="center">
                <img style="display:none;" id="doc_load_image" src="images/loading.gif" /> 
                </div>
                <div  id="doc_search_result" class="scrollable"  >
                  
                <br/> </div>
                        
            </div>  
            <div class="col-md-3">
                <?php include("inc/inc.right.php"); ?>
 <script> 
     $(document).ready(function(){
                search_by_button();
               }); 
               
 
</script>
            </div>  
        </div>
    </div>
</section>

<?php include("inc/inc.ft.php"); 
function sanitize_data_without_slash($input_data) {
	$searchArr=array("document","write","alert","%","@","$",";","+","#","<",">",")","(","'","\'",",");
	$input_data= str_replace("script","",$input_data);
	$input_data= str_replace("iframe","",$input_data);
	$input_data= str_replace($searchArr,"",$input_data);
	return htmlentities(stripslashes($input_data), ENT_QUOTES);
}

?>
  