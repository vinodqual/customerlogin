

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// -----------------------------------------------------------------------------


import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.PBEParameterSpec;
import javax.xml.bind.DatatypeConverter;

// StringEncrypter.java
// -----------------------------------------------------------------------------

/*
 * =============================================================================
 * Copyright (c) 1998-2011 Jeffrey M. Hunter. All rights reserved.
 * 
 * All source code and material located at the Internet address of
 * http://www.idevelopment.info is the copyright of Jeffrey M. Hunter and
 * is protected under copyright laws of the United States. This source code may
 * not be hosted on any other site without my express, prior, written
 * permission. Application to host any of the material elsewhere can be made by
 * contacting me at jhunter@idevelopment.info.
 *
 * I have made every effort and taken great care in making sure that the source
 * code and other content included on my web site is technically accurate, but I
 * disclaim any and all responsibility for any loss, damage or destruction of
 * data or any other property which may arise from relying on it. I will in no
 * case be liable for any monetary damages arising from such loss, damage or
 * destruction.
 * 
 * As with any code, ensure to test this code in a development environment 
 * before attempting to run it in production.
 * =============================================================================
 */
 
// CIPHER / GENERATORS

/**
 * -----------------------------------------------------------------------------
 * The following example implements a class for encrypting and decrypting
 * strings using several Cipher algorithms. The class is created with a key and
 * can be used repeatedly to encrypt and decrypt strings using that key.
 * Some of the more popular algorithms are:
 *      Blowfish
 *      DES
 *      DESede
 *      PBEWithMD5AndDES
 *      PBEWithMD5AndTripleDES
 *      TripleDES
 * 
 * @version 1.0
 * @author  pawan kumar
 *  
 * -----------------------------------------------------------------------------
 */

public class StringEncrypter {

    Cipher ecipher;
    Cipher dcipher;
    Cipher oneWayDCipher;
    Cipher oneWayCipher;
    Cipher desedeEncryptCipher;
    Cipher desedeDecryptCipher;

    /**
     * Constructor used to create this object.  Responsible for setting
     * and initializing this object's encrypter and decrypter Chipher instances
     * given a Secret Key and algorithm.
     * @param key        Secret Key used to initialize both the encrypter and
     *                   decrypter instances.
     * @param algorithm  Which algorithm to use for creating the encrypter and
     *                   decrypter instances.
     */
    StringEncrypter(SecretKey key, String algorithm) {
        try {
            try {
                ecipher = Cipher.getInstance(algorithm);
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(StringEncrypter.class.getName()).log(Level.SEVERE, null, ex);
            }
            dcipher = Cipher.getInstance(algorithm);
            try {
                ecipher.init(Cipher.ENCRYPT_MODE, key);
            } catch (InvalidKeyException ex) {
                Logger.getLogger(StringEncrypter.class.getName()).log(Level.SEVERE, null, ex);
            }
            dcipher.init(Cipher.DECRYPT_MODE, key);
        } catch (NoSuchPaddingException e) {
            System.out.println("EXCEPTION: NoSuchPaddingException");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("EXCEPTION: NoSuchAlgorithmException");
        } catch (InvalidKeyException e) {
            System.out.println("EXCEPTION: InvalidKeyException");
        }
    }


    /**
     * Constructor used to create this object.  Responsible for setting
     * and initializing this object's encrypter and decrypter Chipher instances
     * given a Pass Phrase and algorithm.
     * @param passPhrase Pass Phrase used to initialize both the encrypter and
     *                   decrypter instances.
     */
    StringEncrypter(String passPhrase) {

        // 8-bytes Salt
       final byte[] salt = {
            (byte)0xA9, (byte)0x9B, (byte)0xC8, (byte)0x32,
            (byte)0x56, (byte)0x34, (byte)0xE3, (byte)0x03
        };
	final byte[] salt2 = { (byte) 0x0B, (byte) 0x0A, (byte) 0x0B, (byte) 0x0E, (byte) 0x05, (byte) 0x0A, (byte) 0x0F, (byte) 0xE};
        // Iteration count
        int iterationCount = 25;

        try {

            KeySpec keySpec = new PBEKeySpec(passPhrase.toCharArray(), salt, iterationCount);
            SecretKey key = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec);

ecipher = Cipher.getInstance(key.getAlgorithm());
dcipher = Cipher.getInstance(key.getAlgorithm());

// Prepare the parameters to the ciphers
//
 AlgorithmParameterSpec paramSpec = new PBEParameterSpec(salt, iterationCount);
 ecipher.init(Cipher.ENCRYPT_MODE, key, paramSpec);
 dcipher.init(Cipher.DECRYPT_MODE, key, paramSpec);
 // One Way encrypt Cipher
 KeySpec keySpec2 = new PBEKeySpec(passPhrase.toCharArray(), salt2, iterationCount);
 SecretKey key2 = SecretKeyFactory.getInstance("PBEWithMD5AndDES").generateSecret(keySpec2);
 AlgorithmParameterSpec paramSpec2 = new PBEParameterSpec(salt2, iterationCount);
 oneWayCipher = Cipher.getInstance(key2.getAlgorithm());
 oneWayCipher.init(Cipher.ENCRYPT_MODE, key2, paramSpec2);
 oneWayDCipher = Cipher.getInstance(key2.getAlgorithm());
 oneWayDCipher.init(Cipher.DECRYPT_MODE,key2, paramSpec2);

// // 3DES Encryptor
//
 KeySpec keySpec3 = new PBEKeySpec(passPhrase.toCharArray(), salt2, iterationCount);
 SecretKey key3 = SecretKeyFactory.getInstance("PBEWithMD5AndTripleDES").generateSecret(keySpec3);
 desedeEncryptCipher = Cipher.getInstance(key3.getAlgorithm());
 desedeEncryptCipher.init(Cipher.ENCRYPT_MODE, key3, paramSpec2);
 desedeDecryptCipher = Cipher.getInstance(key3.getAlgorithm());
 desedeDecryptCipher.init(Cipher.DECRYPT_MODE, key3, paramSpec2);

        } catch (InvalidAlgorithmParameterException e) {
            System.out.println("EXCEPTION: InvalidAlgorithmParameterException");
        } catch (InvalidKeySpecException e) {
            System.out.println("EXCEPTION: InvalidKeySpecException");
        } catch (NoSuchPaddingException e) {
            System.out.println("EXCEPTION: NoSuchPaddingException");
        } catch (NoSuchAlgorithmException e) {
            System.out.println("EXCEPTION: NoSuchAlgorithmException");
        } catch (InvalidKeyException e) {
            System.out.println("EXCEPTION: InvalidKeyException");
        }


	


    }


    /**
     * Takes a single String as an argument and returns an Encrypted version
     * of that String.
     * @param str String to be encrypted
     * @return <code>String</code> Encrypted version of the provided String
     */
    public String encrypt(String str) {
    
        try {
            // Encode the string into bytes using utf-8
            byte[] utf8 = str.getBytes("UTF8");

            // Encrypt
//            byte[] enc = ecipher.doFinal(utf8);
            byte[] enc = null;
            try {
                enc = oneWayCipher.doFinal(utf8);
            } catch (IllegalBlockSizeException ex) {
                Logger.getLogger(StringEncrypter.class.getName()).log(Level.SEVERE, null, ex);
            } catch (BadPaddingException ex) {
                Logger.getLogger(StringEncrypter.class.getName()).log(Level.SEVERE, null, ex);
            }
//            byte[] enc = desedeEncryptCipher.doFinal(utf8);
            // Encode bytes to base64 to get a string
            String base64encodedString = DatatypeConverter.printBase64Binary(enc);
            return base64encodedString;
            
            
    
        } catch (UnsupportedEncodingException ex) {
            Logger.getLogger(StringEncrypter.class.getName()).log(Level.SEVERE, null, ex);
        }
                return null;
    }


    /**
     * Takes a encrypted String as an argument, decrypts and returns the 
     * decrypted String.
     * @param str Encrypted String to be decrypted
     * @return <code>String</code> Decrypted version of the provided String
     */
    public String decrypt(String str) {

      

        try {
            // Decode base64 to get bytes
            //byte[] dec = new sun.misc.BASE64Decoder().decodeBuffer(str);
            byte[] dec = DatatypeConverter.parseBase64Binary(str);              
            // Decrypt
//            byte[] utf8 = dcipher.doFinal(dec);
            byte[] utf8 = oneWayDCipher.doFinal(dec);
            try {
                //    	      byte[] utf8 = desedeDecryptCipher.doFinal(dec);
                // Decode using utf-8
                return new String(utf8, "UTF8");
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(StringEncrypter.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IllegalBlockSizeException ex) {
            Logger.getLogger(StringEncrypter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (BadPaddingException ex) {
            Logger.getLogger(StringEncrypter.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }


   /* 
     * The following method is used for testing the String Encrypter class.
     * This method is responsible for encrypting and decrypting a sample
     * String using using a Pass Phrase.
     */
    public static void testUsingPassPhrase(String desEncrypted) {
        String passPhrase   = "C2LBIZ Pass Phrase";

        // Create encrypter/decrypter class
        StringEncrypter desEncrypter = new StringEncrypter(passPhrase);

        // Encrypt the string
//        String desEncrypted       = desEncrypter.encrypt(secretString);
        // Decrypt the string
        String desDecrypted       = desEncrypter.decrypt(desEncrypted);

        // Print out values
		//System.out.print(desEncrypted + "  " + desDecrypted);
		System.out.print(desDecrypted);
        System.out.println();

    }


    /**
     * Sole entry point to the class and application used for testing the
     * String Encrypter class.
     * @param args Array of String arguments.
     */
	 
	public static void main(String[] args) {
		for(int i = 0; i < args.length; i++){
			System.out.println("Argument is: "+args[i]);
			testUsingPassPhrase(args[i]);
		}
		
    }
    
   
}



