<?php 
if (!isset($_POST)) {
    die('Invalid request');
}
$productName = trim($_POST['PRODUCT']);
$Corporatename = trim($_POST['CORPORATENAME']);
$Noofempolyee = trim($_POST['EMPLOYEENO']);
$IndustryType = trim($_POST['INDUSTRYID']);
$LocationName = trim($_POST['LOCATIONID']);
$FamilyStructure = trim($_POST['FAMILYDEFINITION']);
$Nooflives = trim($_POST['NOOFEMPLOYEE']);
$ContributionType = trim($_POST['CONTRIBUTORYTYPE']);
$CardType = trim($_POST['CARDTYPE']);
$Mandate = trim($_POST['MANDATETYPE']);
$PolicyStructure = trim($_POST['POLICYSTRUCTURE']);
$Policy_type = trim($_POST['POLICYTYPE']);
$PolicyStart = trim($_POST['POLICYSTARTDATE']);
$ExpiryInsurer = trim($_POST['EXPIRINGINSURER']);
$ExPremiumAmount = trim($_POST['EXPREMIUMAMOUNT']);
$ClaimAmount = trim($_POST['CLAIMAMOUNT']);
$ClaimDate = trim($_POST['CLAIMDATE']);
$SumInsuredType = trim($_POST['SUMINSUREDTYPE']);
$SumInsured = trim($_POST['SUMINSURED']);
$AdditionalRequirement = trim($_POST['ADDITIONALREQUIREMENT']);
$RiskCategory = trim($_POST['RISKCLASS']);
$Activity = trim($_POST['ACTIVITY']);
$policyBasis = trim($_POST['POLICYBASIS']);
$Ppd = trim($_POST['PPD']);
$Ptd = trim($_POST['PTD']);
$Death = trim($_POST['DEATH']);
$PtdImprovement = trim($_POST['PTDIMPROVEMENT']);
$PpdImprovement = trim($_POST['PPDIMPROVEMENT']);
$MortalRemains = trim($_POST['MORTALREMAINS']);
$ReconstructiveSurgary = trim($_POST['RECONSTRUCTIVESURGERY']);
$MarriageAllowanse = trim($_POST['MARRIAGEALLOWANCE']);
$MobilityExt = trim($_POST['MOBILTYEXTENSION']);
$HospitalCash = trim($_POST['HOSPITAL_CASH_ALLOWANCE']);
$FuneralExpanse = trim($_POST['FUNERALEXPENSES']);
$HomeModification = trim($_POST['HOME_MODIFICATION']);
$Fracture = trim($_POST['FRACTURE']);
$CompassionateVisit = trim($_POST['COMPASSIONATE_VISIT']);
$ChildrenEducation = trim($_POST['CHILDREN_EDUCATION']);
$Burns = trim($_POST['BURNS']);
$AmbulanceService = trim($_POST['AMBULANCE_SERVICE']);
$Ttd = trim($_POST['TTD']);
$Dissappearqnce = trim($_POST['Dissapprearance']);
$NormalMaternityAmount = trim($_POST['NORMALMATERNITY_AMOUNT']);
$CMaternityAmount = trim($_POST['CMATERNITY_AMOUNT']);
$CfAmount = trim($_POST['CFAMOUNT']);
$DiseaseLimit = trim($_POST['DISEASELIMIT']);
$PrePostHospital = trim($_POST['PREPOST_HOSPTALIZATION']);
$Ambulance = trim($_POST['AMBULANCE']);
$CoverType = trim($_POST['COVERTYPE']);
$PrePostNatal = trim($_POST['PREPOST_NATAL']);
$WaitingPeriod = trim($_POST['WAITINGPERIOD']);
$PedExclusion = trim($_POST['PED_EXCLUSION']);
$S_SumInsured = trim($_POST['S_SUMINSURED']);
$S_CoPay = trim($_POST['S_CO_PAY']);
$S_SuminsuredType = trim($_POST['S_SUMINSUREDTYPE']);
$HIGHESTSI = trim($_POST['HIGHESTSI']);
$TOTALSI = trim($_POST['TOTALSI']);
$TOPFIFTYSI = trim($_POST['TOPFIFTYSI']);
$GRADEBASISID = trim($_POST['GRADEBASISID']);
$S_GradeBasis = trim($_POST['S_GRADE_BASIS']);
$S_RoomRent = trim($_POST['S_ROOM_RENT']);
$S_IcuRent = trim($_POST['S_ICU_RENT']);
$S_Remark = trim($_POST['S_REMARK']);
$S_Age_1_35 = trim($_POST['S_AGE_1_35']);
$S_Age_36_45 = trim($_POST['S_AGE_36_45']);
$S_Age_46_55 = trim($_POST['S_AGE_46_55']);
$S_Age_56_65 = trim($_POST['S_AGE_56_65']);
$S_Age_66_70 = trim($_POST['S_AGE_66_70']);
$S_Age_71_75 = trim($_POST['S_AGE_71_75']);
$S_Age_76_80 = trim($_POST['S_AGE_76_80']);
$S_Age_81_90 = trim($_POST['S_AGE_81_90']);
$Opd_Consultationfee = trim($_POST['OPD_CONSULTATION_FEE']);
$Opd_suminsured = trim($_POST['OPD_SUMINSURED']);
$Opd_FamilyStructure = trim($_POST['OPD_FAMILY_STRUCTURE']);
$Opd_Noofemployee = trim($_POST['OPD_NOOFEMPLOYEE']);
$Opd_Noofmember = trim($_POST['OPD_NOOFMEMBER']);
$Opd_Copay = trim($_POST['OPD_COPAY']);
$Opd_ProposedType = trim($_POST['OPD_PROPOSED_TYPE']);
$ghi_covered = trim($_POST['GHICOVERED']);
$acc_hosp = trim($_POST['ACC_HOSP']);
$medex = trim($_POST['MEDEX']);
$policy_basis_desc = trim($_POST['POLICYBASISDESC']);
$new_born_baby_coverage = trim($_POST['BABYAMOUNT1']);
$health_checkup = trim($_POST['HEALTHCHECKUPID']);
$health_checkup_member = trim($_POST['HEALTHCHECKUPNUMBER']);
$proposal_form = trim($_POST['proposalform']);
$no_of_parents = trim($_POST['PARENTSNO']);
$brokerage = trim($_POST['BROKERAGE']);
$requestArray = array(
    'action' => 'GETRATE',
    'quote_data' => array(array(
            'corporate_name' => $Corporatename,
            'product' => $productName,
            'industry_type' => $IndustryType,
            'location_name' => $LocationName,
            'no_of_employees' => $Noofempolyee,
            'no_of_lives' => $Nooflives,
            'no_of_parents' => $no_of_parents,
            'family_strucure' => $FamilyStructure,
            'contribution_type' => $ContributionType,
            'card_type' => $CardType,
            'mandate_type' => $Mandate,
            'policy_structure' => $PolicyStructure,
            'policy_type' => $Policy_type,
            'policy_start_date' => $PolicyStart,
            'expiring_insurer' => $ExpiryInsurer,
            'expiring_premium' => $ExPremiumAmount,
            'claim_amount' => $ClaimAmount,
            'claim_date' => $ClaimDate,
            'sum_insured_type' => $SumInsuredType,
            'individual_si' => $SumInsured,
            'highest_si' => $HIGHESTSI,
            'total_si' => $TOTALSI,
            'top_50_live_si' => $TOPFIFTYSI,
            'gpa_grade_basis' => $GRADEBASISID,
            'proposal_form' => $proposal_form,
            'additional_requirement' => $AdditionalRequirement,
            'risk_class' => $RiskCategory,
            'activity' => $Activity,
            'policy_basis' => $policyBasis,
            'policy_basis_desc' => $policy_basis_desc,
            'caution_firm' => 'no',
            'ghi_covered' => $ghi_covered,
            'coverage' => array(
                'ppd' => $Ppd,
                'ptd' => $Ptd,
                'death' => $Death,
                'acc_hosp' => $acc_hosp,
                'medex' => $medex,
                'ptd_improvement' => $PtdImprovement,
                'ppd_improvement' => $PpdImprovement,
                'mortal_remains' => $MortalRemains,
                'reconstructive_surgery' => $ReconstructiveSurgary,
                'marriage_allowance' => $MarriageAllowanse,
                'mobility_extension' => $MobilityExt,
                'hospital_cash_allowance' => $HospitalCash,
                'funeral_expenses' => $FuneralExpanse,
                'home_modification' => $HomeModification,
                'fracture' => $Fracture,
                'compassionate_visit' => $CompassionateVisit,
                'children_education' => $ChildrenEducation,
                'burns' => $Burns,
                'ambulance_service' => $AmbulanceService,
                'ttd' => $Ttd,
                'disappearance' => $Dissappearqnce
            ),
            'normal_maternity_amount' => $NormalMaternityAmount,
            'c_section_maternity_amount' => $CMaternityAmount,
            'cf_amount' => $CfAmount,
            'disease_limit' => $DiseaseLimit,
            'pre_post_hospitalization' => $PrePostHospital,
            'ambulence' => $Ambulance,
            'cover_type' => $CoverType,
            'PrePost_Natal' => $PrePostNatal,
            'waiting_period' => $WaitingPeriod,
            'ped_exclusion' => $PedExclusion,
            'new_born_baby_coverage' => $new_born_baby_coverage,
            'health_checkup' => $health_checkup,
            'health_checkup_member' => $health_checkup_member,
            'slabs' => array(array(
                    'sum_insured' => $S_SumInsured,
                    'co_pay' => $S_CoPay,
                    'sum_insured_type' => $S_SuminsuredType,
                    'grade_basis' => $S_GradeBasis,
                    'room_rent' => $S_RoomRent,
                    'icu_rent' => $S_IcuRent,
                    'reason' => $S_Remark,
                    'member_age_group' => array(
                        '1-35' => $S_Age_1_35, '36-45' => $S_Age_36_45, '46-55' => $S_Age_46_55, '56-65' => $S_Age_56_65, '66-70' => $S_Age_66_70, '71-75' => $S_Age_71_75, '76-80' => $S_Age_76_80, '81-90' => $S_Age_81_90
                    ),
                ),
            ),
            'opd_coverage' => $Opd_Consultationfee,
            'opd_sum_insured' => $Opd_suminsured,
            'opd_family_strucure' => $Opd_FamilyStructure,
            'opd_no_of_employee' => $Opd_Noofemployee,
            'opd_no_of_member' => $Opd_Noofmember,
            'opd_co_pay' => $Opd_Copay,
            'opd_proposed_type' => $Opd_ProposedType,
            'brokerage' => $brokerage
        )),
    'api_key' => "GUMH62KEYZXVF7LCT415"
);
$requestData = json_encode($requestArray);
$enKey = "z5yK1lw7XYt6PKdP7Pne2Jw3zRkMAziH";
$enIV = "i0kbCAlFTlDXshGVCT2IxLOdJ0iWEwqK";
$encryp_Data = "data=" . urlencode(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $enKey, $requestData, MCRYPT_MODE_CBC, $enIV)));

$request_url = "https://uat-my.religarehealthinsurance.com/qtp/oaq-ws/do";

$headers = array(
    'Authorization: Basic ZmVkb3JhOnNoYWt0aTEyMw=='
);
$curl = curl_init();
curl_setopt($curl, CURLOPT_URL, $request_url);
curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
curl_setopt($curl, CURLOPT_POST, sizeof($encryp_Data));
curl_setopt($curl, CURLOPT_POSTFIELDS, $encryp_Data);
curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
$response = curl_exec($curl);
curl_close($curl);

echo '<hr>'.$response."<hr>";
$response=mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $enKey, base64_decode($response), MCRYPT_MODE_CBC, $enIV);
die($response);
?>