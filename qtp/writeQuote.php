<?php
include_once("conf/company_session.php");
include_once("conf/conf.php");
include_once("conf/fucts.php");

$_SESSION['quoteform'] = $_POST;
foreach ($_REQUEST as $key => $value) {
    $$key = $value;
}

$sourceminlives = @$_SESSION["sourceminlives"];
$sourceloading = @$_SESSION["sourceloading"];
//$array = array_map( 'addslashes', @$_REQUEST);
$requestData = str_replace("'", "''", @$_REQUEST);
$encodeData = json_encode($requestData);
$maxGroupSize = getMaxSize('CRMOAQGROUPSIZE', 'MAXIMUMNUMBER', 'ID', 'STATUS', 'ACTIVE', '', '');
//$maxClaimRatio	= getMaxSize('CRMOAQCLAIMRATIO','MAXIMUM','ID','STATUS','ACTIVE','','');
$entryTime = time();
$error = 0;
$serviceTaxArr = getServiceTax();
//print_r($serviceTaxArr);
$serviceTax = $serviceTaxArr[0]['TAX'];
//die();
$settingsArr = fetchListById('CRMOAQSETTINGS', 'ID', '3');
$OPDCARDAMOUNTVAL = $settingsArr[0]['OPDCARDAMOUNT'];
//print_r($settingsArr);
//echo '<pre>';

$CORPORATENAME = sanitize_quote_data(@$_POST['CORPORATENAME']);
$LEGALNAME = sanitize_data(@$_POST['LEGALNAME']);  // added by shailender
$PRODUCT = sanitize_data(@$_POST['PRODUCT']);  // added by shailender
$LOCATIONID = sanitize_data(@$_POST['LOCATIONID']);
$SOURCEID = sanitize_data(@$_POST['SELECTINTERMEDIARY']);
$EMPLOYEENO = sanitize_data(@$_POST['EMPLOYEENO']);
$EXPIRINGINSURER = sanitize_data(@$_POST['EXPIRINGINSURER']);
$PREMIUMAMOUNT = sanitize_data(round(str_replace(",", "", @$_POST['PREMIUMAMOUNT'])));
$INDUSTRYID = sanitize_data(@$_POST['INDUSTRYID']);
$INTERMEDIARYCOMMISSION = sanitize_data(@$_POST['INTERMEDIARYCOMMISSION']);
$CONTRIBUTORYTYPE = sanitize_data(@$_POST['CONTRIBUTORYTYPE']);
$FAMILYDEFINITION = sanitize_data(@$_POST['FAMILYDEFINITION']);
$RMNAME = sanitize_data(@$_POST['RMNAME']); // added by shailender
$RMEMAIL = sanitize_data(strtolower($_POST['RMEMAIL'])); // added by shailender
$MANDATETYPE = sanitize_data(@$_POST['MANDATETYPE']); // added by shailender
$POLICY_STRUCTURE = sanitize_data(@$_POST['POLICYSTRUCTURE']); // added by shailender
$POLICY_STRUCTURE_AR = explode("_", @$POLICY_STRUCTURE);           // added by shailender
$POLICYSTRUCTUREID = @$POLICY_STRUCTURE_AR[0];                  // added by shailender
$POLICYSTRUCTURE = @$POLICY_STRUCTURE_AR[1];                  // added by shailender

$PARENTSNO = sanitize_data(@$_POST['PARENTSNO']);
$NOOFEMPLOYEE = sanitize_data(@$_POST['NOOFEMPLOYEE']); // no of lives
$EXPIRINGTPA = sanitize_data(@$_POST['EXPIRINGTPA']);
$CLAIMAMOUNT = sanitize_data(round(str_replace(",", "", @$_POST['CLAIMAMOUNT'])));
$WAITINGPERIOD = sanitize_data(@$_POST['WAITINGPERIOD']);
$WAITINGPERIODPERCENT = round($settingsArr[0]['WAITINGPERIODDISCOUNT']);
$PEDEXCLUSION = sanitize_data(@$_POST['PEDEXCLUSION']);
$CARDTYPE = sanitize_data(@$_POST['CARDTYPE']);
$CARDAMOUNT = round(sanitize_data(@$_POST['CARDAMOUNT']));
$slabNo = sanitize_data(@$_POST['slabNo']);
//$REMARKS	 				= sanitize_data(@$_POST['REMARKS']);
$REMARKS = addslashes(@$_POST['REMARKS']);
$POLICYTYPE = sanitize_data(@$_POST['POLICYTYPE']);
$NORMALMATERNITY1 = sanitize_data(@$_POST['NORMALMATERNITY1']);
$NORMALMATERNITYTYPE1 = sanitize_data(@$_POST['NORMALMATERNITYTYPE1']);
$MATERNITY1 = sanitize_data(@$_POST['MATERNITY1']);
$MATERNITYTYPE1 = sanitize_data(@$_POST['MATERNITYTYPE1']);
$CORPORATEFLOATER1 = sanitize_data(@$_POST['CORPORATEFLOATER1']);
$CFMOUNT1 = sanitize_data(@$_POST['CFMOUNT1']);
$NEWBABY1 = sanitize_data(@$_POST['NEWBABY1']);
$BABYAMOUNT1 = sanitize_data(@$_POST['BABYAMOUNT1']);
$COVERTYPE1 = sanitize_data(@$_POST['COVERTYPE1']);
$GHICOVERTYPE1 = sanitize_data(@$_POST['GHICOVERTYPE1']);
$PREPOSTNATAL1 = sanitize_data(@$_POST['PREPOSTNATAL1']);
$GHIPREPOSTNATAL1 = sanitize_data(@$_POST['GHIPREPOSTNATAL1']);
$diseaseId1 = sanitize_data(@$_POST['diseaseId1']);
$prePostId1 = sanitize_data(@$_POST['prePostId1']);
$ambulanceId1 = sanitize_data(@$_POST['ambulanceId1']);
$CREATETYPE = sanitize_data(@$_POST['CREATETYPE']);
$COPYID = sanitize_data(@$_POST['COPYID']);
$EXPIRYNUMBER = sanitize_data(@$_POST['EXPIRYNUMBER']);
$NEWPOLICYTYPE = sanitize_data(@$_POST['NEWPOLICYTYPE']);
$clratioper = sanitize_data(@$_POST['clratioper']);
$firstQuoteCreateDate = sanitize_data(@$_POST['firstQuoteCreateDate']);
$proposalform = sanitize_data(@$_POST['proposalform']);
$SELECTINTERMEDIARY = sanitize_data(@$_POST['SELECTINTERMEDIARY']);
if ($SELECTINTERMEDIARY == 'other') {
    $SOURCENAME = sanitize_data(@$_POST['OTINTERMEDIARY']);
} else {
    $SOURCENAME = @$SELECTINTERMEDIARY;
}
$COVERTYPE1 = 'yes';
$ambulanceArr = explode("_", @$ambulanceId1);
$ambulanceId1 = @$ambulanceArr[0];
$AMBULANCEVAL = @$ambulanceArr[1];
$prepostArr = explode("_", @$prePostId1);
$prePostId1 = @$prepostArr[0];
$PREPOSTVAL = @$prepostArr[1];
$disesaseArr = explode("_", @$diseaseId1);
$diseaseId1 = @$disesaseArr[0];
$DISEASEVAL = @$disesaseArr[1];
if (@$PREPOSTNATAL1 == 'yes') {
    $prepostnatalArr = explode("_", @$GHIPREPOSTNATAL1);
    $GHIPREPOSTNATAL1 = @$prepostnatalArr[0];
    $PREPOSTNATALVAL = @$prepostnatalArr[1];
} else {
    $GHIPREPOSTNATAL1 = '';
    $PREPOSTNATALVAL = '';
}
if (@$COVERTYPE1 == 'yes') {
    $coverTypeArr = explode("_", @$GHICOVERTYPE1);
    $GHICOVERTYPE1 = @$coverTypeArr[0];
    $COVERTYPEVAL = @$coverTypeArr[1];
} else {
    $GHICOVERTYPE1 = '';
    $COVERTYPEVAL = '';
}
if (@$NEWBABY1 == 'yes') {
    $babyArr = explode("_", @$BABYAMOUNT1);
    $BABYAMOUNT1 = @$babyArr[0];
    $BABYVAL = @$babyArr[1];
} else {
    $BABYAMOUNT1 = '';
    $BABYVAL = '';
}
if (@$CORPORATEFLOATER1 == 'yes') {
    $cfArr = explode("_", @$CFMOUNT1);
    $CFMOUNT1 = @$cfArr[0];
    $CORPORATEFLOATERVAL = @$cfArr[1];
} else {
    $CFMOUNT1 = '';
    $CORPORATEFLOATERVAL = '';
}
if (@$MATERNITY1 == 'yes') {
    $maternityArr = explode("_", @$MATERNITYTYPE1);
    $MATERNITYTYPE1 = @$maternityArr[0];
    $MATERNITYVAL = @$maternityArr[1];
} else {
    $MATERNITYTYPE1 = '';
    $MATERNITYVAL = '';
}
if (@$NORMALMATERNITY1 == 'yes') {
    $normalmaternityArr = explode("_", @$NORMALMATERNITYTYPE1);
    $NORMALMATERNITYTYPE1 = @$normalmaternityArr[0];
    $NORMALMATERNITYVAL = @$normalmaternityArr[1];
} else {
    $NORMALMATERNITYTYPE1 = '';
    $NORMALMATERNITYVAL = '';
}
$policyTypeArr = explode("_", @$POLICYTYPE);
$POLICYTYPE = @$policyTypeArr[0];
$POLICYTYPELOADDISC = @$policyTypeArr[1];
$familyArr = explode("_", @$FAMILYDEFINITION);
$FAMILYDEFINITION = @$familyArr[0];
$familyChkRatio = @$familyArr[1];
$FAMILYTYPE = @$familyArr[2];
$locationArr = explode("_", @$LOCATIONID);
$locationTypeId = @$locationArr[2];
$LOCATIONNAME = @$locationArr[1];
$locationId = @$locationArr[0];
$industryArr = explode("_", @$INDUSTRYID);
$INDUSTRYID = @$industryArr[0];
$INDUSTRYNAME = @$industryArr[1];
$insurerArr = explode("_", @$EXPIRINGINSURER);
$EXPIRINGINSURER = @$insurerArr[0];
$EXPIRINGINSURERNAME = @$insurerArr[1];

$USERNAME = @$_SESSION['name'];
$USERBRANCH = @$_SESSION['branch'];
$USERZONEVERTICAL = @$_SESSION['vertical'];
$LASTYEARLIVES = sanitize_data(@$_POST['LASTYEARLIVES']);
$IS_WEB_REQUEST = @$_POST['IS_WEB_REQUEST']; // Field to detect whether it is a web service request or not
$POLICYBASIS = @$_POST['POLICYBASIS'];
$totalGroupMembers = '0';
/* Find total group member---START---- */
for ($i = 1; $i <= $slabNo; $i++) {
    $currentSlab = "slab" . $i;
    $cslab = $$currentSlab;
    for ($j = 0; $j < count($cslab); $j++) {
        $ageGroupId = $cslab[$j];
        $memberVar = $currentSlab . "_" . $ageGroupId;
        $groupMemberNo = $$memberVar;
        if ($groupMemberNo > 0) {
            $totalGroupMembers = $totalGroupMembers + $groupMemberNo;
        }
    }
}
/* * ++++++++++++++++++++++++++++++Total no members , Total no lives match condition+++++++++++++++++++++++++++++++++++++++ */
if ($totalGroupMembers != $NOOFEMPLOYEE) {
    $_SESSION['NoofLivesstatus'] = "Total member must be equal to total no. of lives. ";

    $redirectUrl = (@$copyQuoteId != '') ? 'editQuoteGHI.php?quoteId=' . @$copyQuoteId : 'createQuoteGHI.php';
    ?>
    <script type="text/javascript">document.location = '<?php echo $redirectUrl; ?>';</script>;
    <?php
    die();
}



if ($COPYID > 0) {
    if ($NEWPOLICYTYPE == 'Renewal') {
        $POLICYSTARTDATE = strtotime(@$_POST['NEWPOLICYSTARTDATE']);
        $CLAIMDATE = strtotime(@$_POST['NEWCLAIMDATE']);
    } else {
        $POLICYSTARTDATE = strtotime(@$_POST['POLICYSTARTDATE']);
        $CLAIMDATE = strtotime(@$_POST['CLAIMDATE']);
    }
} else {
    $POLICYSTARTDATE = strtotime(@$_POST['POLICYSTARTDATE']);
    $CLAIMDATE = strtotime(@$_POST['CLAIMDATE']);
}

$startDate = $POLICYSTARTDATE - 31536000;
$diffDays = ($CLAIMDATE - $startDate) / 86400;

$quoteArr = quoteDetails($COPYID);
/* OPD Code Start************************************************************************************************* */

$opdsiid = sanitize_data(@$_POST['OPDSIID']);
$opdsiarr = explode("_", @$opdsiid);
$opsSiId = @$opdsiarr[0];
$opsSiVal = @$opdsiarr[1];

$opdcoverageid = sanitize_data(@$_POST['OPDCOVERAGEID']);
$opdcoveragearr = explode("_", @$opdcoverageid);
$opdCoverageId = @$opdcoveragearr[0];
$opdCoverageVal = @$opdcoveragearr[1];

$opdlocationid = sanitize_data(@$_POST['OPDLOCATIONID']);
$opdlocationarr = explode("_", @$opdlocationid);
$opdLocationId = @$opdlocationarr[0];
$opdLocationVal = @$opdlocationarr[1];
$opdLocMultiplier = @$opdlocationarr[2];

$OPDEMPLOYEE = sanitize_data(@$_POST['OPDEMPLOYEE']);
$OPDMEMBERS = sanitize_data(@$_POST['OPDMEMBERS']);

$PROPOSEDTYPE = sanitize_data(@$_POST['PROPOSEDTYPE']);
if ($PROPOSEDTYPE == 'Cashless') {
    $opdcoveragemultiplier = fetchListCondsWithColumn('MULTIPLIER', 'OPDSICOVERAGEMULTIPLIER', " WHERE COVERAGEID  = '" . @$opdCoverageId . "' AND SIID  = '" . @$opsSiId . "' 	order by ID ASC ");
    $opdcovmultiplier = @$opdcoveragemultiplier[0]['MULTIPLIER'];
} else {
    $opdcoveragemultiplier = fetchListCondsWithColumn('MULTIPLIER', 'OPDSICOVMULREIMBURSEMENT', " WHERE COVERAGEID  = '" . @$opdCoverageId . "' AND SIID  = '" . @$opsSiId . "' 	order by ID ASC ");
    $opdcovmultiplier = @$opdcoveragemultiplier[0]['MULTIPLIER'];
}

$opdCardAmount = intval(@$OPDCARDAMOUNTVAL) * intval(@$OPDMEMBERS);

$opdfamilyid = sanitize_data(@$_POST['OPDFAMILYID']);
$opdfamilyarr = explode("_", @$opdfamilyid);
$opdFamilyId = @$opdfamilyarr[0];
$opdFamilyVal = @$opdfamilyarr[1];
$opdcovfamilymultiplierval = @$opdfamilyarr[2];

$opdCopayid = sanitize_data(@$_POST['OPDCOPAYID']);
$opdCopayarr = explode("_", @$opdCopayid);
$opdCopayId = @$opdCopayarr[0];
$opdCopayVal = @$opdCopayarr[1];
$opdcovcopaymultiplierval = @$opdCopayarr[2];

$opdPremium = @$opsSiVal * @$OPDEMPLOYEE * @$opdLocMultiplier * @$opdcovmultiplier * @$opdcovfamilymultiplierval * @$opdcovcopaymultiplierval;
$opdPremium = intval(@$opdPremium) + intval(@$opdCardAmount);
if ($opdPremium > 0) {
    $opdval = 'yes';
} else {
    $opdval = 'no';
}

$intermediacomm = $INTERMEDIARYCOMMISSION;
if ($intermediacomm > 0) {
    $brokerageMultiplier = 100 / (100 - @$intermediacomm);
    if ($brokerageMultiplier > 0) {
        $opdPremium = round($opdPremium * $brokerageMultiplier);
        //echo '<br/>After Commision---'.$premium = round($premium*$brokerageMultiplier);
    }
}

$HEALTHCHECKUPMEMBER = sanitize_data(@$_POST['HEALTHCHECKUPMEMBER']);
$healthchkupid = @$_POST['HEALTHCHECKUPID'];
$healthchkuparr = explode("|", @$healthchkupid);
$hlthchkupId = @$healthchkuparr[0];
$hlthchkupMultiplier = @$healthchkuparr[1];
$hlthchkupVal = @$healthchkuparr[2];
$hlthChkPremium = intval(@$hlthchkupMultiplier) * intval(@$HEALTHCHECKUPMEMBER);
if ($intermediacomm > 0) {
    $brokerageMultiplier = 100 / (100 - @$intermediacomm);
    if ($brokerageMultiplier > 0) {
        $hlthChkPremium = round($hlthChkPremium * $brokerageMultiplier);
        //echo '<br/>HEALTHCHECKUP premium---'.$hlthChkPremium;
    }
}
/* echo '<br/>HEALTHCHECKUP premium---'.$hlthChkPremium;
  die(); */
/* OPD Code End************************************************************************************************* */


if ($PEDEXCLUSION == 'waveoff') {
    $PEDEXCLUSIONPERCENT = $settingsArr[0]['PEDNOTCOVERED'];
}
if ($PEDEXCLUSION == 'applicable') {
    $PEDEXCLUSIONPERCENT = $settingsArr[0]['PEDCOVERED'];
}
if ($PEDEXCLUSION == 'pedWait1Year') {
    $PEDEXCLUSIONPERCENT = $settingsArr[0]['PEDWAIT1YEAR'];
}
$locId = $locationTypeId;
if ($POLICYTYPE == 'Renewal') {
    $claimAmt = $CLAIMAMOUNT == 0 ? '1' : $CLAIMAMOUNT;
    $annualClaimAmt = round($claimAmt * (365 / $diffDays), 2);
    $claimRatioPercent = round((@$annualClaimAmt / @$PREMIUMAMOUNT) * 100);
    $crId = fetchListCondsWithColumn('ID', 'CRMOAQCLAIMRATIO', " WHERE MINIMUM<=" . @$claimRatioPercent . " AND MAXIMUM>=" . @$claimRatioPercent . " AND STATUS = 'ACTIVE' ");
    $claimRatioId = @$crId[0]['ID'];
    //echo 'Claim ratio----'.$claimRatioId;	
}
if ($REMARKS != '') {
    $adminStatus = 'PENDING';
} else {
    $adminStatus = 'OPEN';
}
$adminStatus = 'OPEN'; // line inserted by wasim
$totalGroupMembers = '0';
/* Find total group member---START---- */
for ($i = 1; $i <= $slabNo; $i++) {
    $currentSlab = "slab" . $i;
    $cslab = $$currentSlab;
    for ($j = 0; $j < count($cslab); $j++) {
        $ageGroupId = $cslab[$j];
        $memberVar = $currentSlab . "_" . $ageGroupId;
        $groupMemberNo = $$memberVar;
        if ($groupMemberNo > 0) {
            $totalGroupMembers = $totalGroupMembers + $groupMemberNo;
        }
    }
}



$quoteTotal = '';
$totalMembers = '';
$PREMIUMEXCLUDINGTAX = '';
$cardval = $CARDAMOUNT;

$insertquery = "INSERT INTO CRMOAQQUOTE (ID,REFRENCENUMBER,CORPORATENAME,LOCATIONID,NEWSOURCEID,SOURCENAME,POLICYTYPE,EMPLOYEENO,EXPIRINGINSURER,PREMIUMAMOUNT,INDUSTRYID,POLICYSTARTDATE,INTERMEDIARYCOMMISSION,CONTRIBUTORYTYPE,FAMILYDEFINITION,PARENTSNO,EXPIRINGTPA,CLAIMAMOUNT,CLAIMDATE,WAITINGPERIOD,PEDEXCLUSION,CARDTYPE,CARDVAL,STATUS,REMARKS,ADMINSTATUS,SERVICETAX,USERID,USERTYPE,CLOSESTATUS,CREATETYPE,LASTYEARLIVES,LOCATIONNAME,INDUSTRYNAME,FAMILYTYPE,EXPIRINGINSURERNAME,USERNAME,USERBRANCH,USERZONEVERTICAL,MATERNITYID,MATERNITYVAL,CORPORATEFLOATERID,CORPORATEFLOATERVAL,BABYID,BABYVAL,COVERTYPEID,COVERTYPEVAL,PREPOSTNATALID,PREPOSTNATALVAL,DISEASEID,DISEASEVAL,PREPOSTID,PREPOSTVAL,NORMALMATERNITYID,NORMALMATERNITYVAL,AMBULANCEID,AMBULANCEVAL,WAITINGPERIODPERCENT,PEDEXCLUSIONPERCENT,OPD,OPDSIID,OPDSIVAL,OPDCOVERAGEID,OPDCOVERAGEVAL,OPDLOCATIONID,OPDLOCATIONVAL,OPDLOCMULTIPLIER,OPDSICOVERAGEMULTIPLIER,OPDEMPLOYEE,OPDMEMBERS,EXPIRYNUMBER,PROPOSEDTYPE,OPDFAMILYID,OPDFAMILYVAL,OPDFAMILYMULTIPLIER,OPDCOPAYID,OPDCOPAYVAL,OPDCOPAYMULTIPLIER,OPDCARDAMOUNT,OPDCARDAMOUNTTOTAL,HEALTHCHECKUPID,HEALTHCHECKUP,HEALTHCHECKUPMULTIPLIER,HEALTHCHECKUPPREMIUM,HEALTHCHECKUPMEMBER,PROPOSALFORM,JSONHISTORY,CREATEDBY,CREATEDON,UPDATEDBY,UPDATEDON,PRODUCT,MANDATETYPE,RMNAME,RMEMAIL,POLICYSTRUCTUREID,POLICYSTRUCTURE,QUOTEREQUESTNO,LEGALNAME,LOADINGDISC,NOOFLIVES,POLICYSELECTION) VALUES (CRMOAQQUOTE_SEQ.nextval,CRMOAQQUOTE_SEQ.nextval,'" . @$CORPORATENAME . "','" . @$locationId . "','" . @$SOURCEID . "','" . @$SOURCENAME . "','" . @$POLICYTYPE . "','" . @$EMPLOYEENO . "','" . @$EXPIRINGINSURER . "','" . @$PREMIUMAMOUNT . "','" . @$INDUSTRYID . "','" . @$POLICYSTARTDATE . "','" . @$INTERMEDIARYCOMMISSION . "','" . @$CONTRIBUTORYTYPE . "','" . @$FAMILYDEFINITION . "','" . @$PARENTSNO . "','" . @$EXPIRINGTPA . "','" . @$CLAIMAMOUNT . "','" . @$CLAIMDATE . "','" . @$WAITINGPERIOD . "','" . @$PEDEXCLUSION . "','" . @$CARDTYPE . "','" . @$cardval . "','" . @$adminStatus . "','" . @$REMARKS . "','" . @$adminStatus . "','" . @$serviceTax . "','" . @$_SESSION['userId'] . "','" . @$_SESSION['type'] . "','" . @$adminStatus . "','" . @$CREATETYPE . "','" . @$LASTYEARLIVES . "','" . @$LOCATIONNAME . "','" . @$INDUSTRYNAME . "','" . @$FAMILYTYPE . "','" . @$EXPIRINGINSURERNAME . "','" . @$USERNAME . "','" . @$USERBRANCH . "','" . @$USERZONEVERTICAL . "','" . @$MATERNITYTYPE1 . "','" . @$MATERNITYVAL . "','" . @$CFMOUNT1 . "','" . @$CORPORATEFLOATERVAL . "','" . @$BABYAMOUNT1 . "','" . @$BABYVAL . "','" . @$GHICOVERTYPE1 . "','" . @$COVERTYPEVAL . "','" . @$GHIPREPOSTNATAL1 . "','" . @$PREPOSTNATALVAL . "','" . @$diseaseId1 . "','" . @$DISEASEVAL . "','" . @$prePostId1 . "','" . @$PREPOSTVAL . "','" . @$NORMALMATERNITYTYPE1 . "','" . @$NORMALMATERNITYVAL . "','" . @$ambulanceId1 . "','" . @$AMBULANCEVAL . "','" . @$WAITINGPERIODPERCENT . "','" . @$PEDEXCLUSIONPERCENT . "','" . @$opdval . "','" . @$opsSiId . "','" . @$opsSiVal . "','" . @$opdCoverageId . "','" . @$opdCoverageVal . "','" . @$opdLocationId . "','" . @$opdLocationVal . "','" . @$opdLocMultiplier . "','" . @$opdcovmultiplier . "','" . @$OPDEMPLOYEE . "','" . @$OPDMEMBERS . "','" . @$EXPIRYNUMBER . "','" . @$PROPOSEDTYPE . "','" . @$opdFamilyId . "','" . @$opdFamilyVal . "','" . @$opdcovfamilymultiplierval . "','" . @$opdCopayId . "','" . @$opdCopayVal . "','" . @$opdcovcopaymultiplierval . "','" . @$OPDCARDAMOUNTVAL . "','" . @$opdCardAmount . "','" . @$hlthchkupId . "','" . @$hlthchkupVal . "','" . @$hlthchkupMultiplier . "','" . @$hlthChkPremium . "','" . @$HEALTHCHECKUPMEMBER . "','" . @$proposalform . "','" . @$encodeData . "','" . @$_SESSION['userName'] . "','" . @$entryTime . "','" . @$_SESSION['userName'] . "','" . @$entryTime . "','" . @$PRODUCT . "','" . @$MANDATETYPE . "','" . @$RMNAME . "','" . @$RMEMAIL . "','" . @$POLICYSTRUCTUREID . "','" . @$POLICYSTRUCTURE . "',CRMOAQQUOTE_SEQ.nextval,'" . @$LEGALNAME . "','" . $POLICYTYPELOADDISC . "','" . @$NOOFEMPLOYEE . "','" . @$POLICYBASIS . "')";
//echo $insertquery;die;
//writePremiumData($insertquery);//please remove. @SHAKTIRANA

$insertquery = oci_parse($conn, $insertquery);
if (oci_execute($insertquery) === false) {
    if ($IS_WEB_REQUEST) {
        $arr = array('msg' => 'Something went wrong', 'ref_no' => '');
        echo json_encode($arr);
        exit;
    }
}
$check1 = @oci_parse($conn, 'SELECT CRMOAQQUOTE_SEQ.currval FROM DUAL');
oci_execute($check1);
$res = @oci_fetch_assoc($check1);

$quoteId = @$res['CURRVAL'];
//die();
$currentSlab = '';
$cslab = '';
$memberVar = '';

$totalPremium = '';
$totalMembers = '';
$premium = '';
$PREMIUMEXCLUDINGTAX = '';

for ($i = 1; $i <= $slabNo; $i++) {
    $sumInsured = "SUMINSURED" . $i;
    $sumInsuredId = $$sumInsured;
    $siArr = fetchListCondsWithColumn('SI', 'CRMOAQSI', " WHERE SIID = " . @$sumInsuredId . " ");
    $sumInsuredAmount = @$siArr[0]['SI'];
    $copayDisc = "copay" . $i;
    $copayId = $$copayDisc;
    $ROOMTYPEDESC = "ROOMTYPE" . $i;
    $ROOMTYPEID = $$ROOMTYPEDESC;
    $ICULIMITDESC = "ICULIMIT" . $i;
    $ICULIMITID = $$ICULIMITDESC;
    $currentSlab = "slab" . $i;
    $cslab = $$currentSlab;
    $sitype = "SITYPE" . $i;
    $sitype = $$sitype;
    $sitypereason = "SITYPEREMARKS" . $i;
    $sitypereason = $$sitypereason;
    for ($j = 0; $j < count($cslab); $j++) {
        $ageGroupId = $cslab[$j];
        $mapVal = getMappingValues('CRMOAQLOCATIONSIMAPPING', 'LOCATIONID', @$locId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
        $premium = round(@$mapVal[0]['PREMIUM']);
        //echo '<br/>Premium-'.$premium = round($mapVal[0]['PREMIUM']);

        if ($POLICYTYPELOADDISC > 0) {
            $premium = round($premium * $POLICYTYPELOADDISC);
            //echo '<br/>After Policy Type----'.$premium = round($premium*$POLICYTYPELOADDISC);
        }

        if ($WAITINGPERIOD == 'applicable') {
            if ($WAITINGPERIODPERCENT > 0) {
                $premium = round($premium * $WAITINGPERIODPERCENT);
                //echo '<br/>After Waiting-'.$premium = round($premium*$WAITINGPERIODPERCENT);
            }
        }
        if ($PEDEXCLUSIONPERCENT > 0) {
            $premium = round($premium * $PEDEXCLUSIONPERCENT);
            //echo '<br/>After PED-'.$premium = round($premium*$PEDEXCLUSIONPERCENT);
        }
        //$prePostId = "prePostId1";
        $prePostId = @$prePostId1;
        //echo '<br>--'.$prePostId.'--'.$ageGroupId.'--'.$sumInsuredId;
        if ($prePostId != '') {
            $prePostVal = getMappingValues('CRMOAQPREPOSTSIMAPPING', 'PREPOSTID', @$prePostId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $prePostDIsc = @$prePostVal[0]['LOADINGDISCOUNT'];
            //print_r($prePostVal);
            if ($prePostDIsc > 0) {
                $premium = round($premium * $prePostDIsc);
                //echo '<br/>After PREPOST-'.$premium = round($premium*$prePostDIsc);
            }
        }
        //$ambulanceId1 	= "ambulanceId1";
        $ambulanceId = $ambulanceId1;
        if ($ambulanceId != '') {
            $ambulanceVal = getMappingValues('CRMOAQAMBULANCEMAPPING', 'AMBULANCEID', @$ambulanceId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $ambulanceDIsc = @$ambulanceVal[0]['LOADINGDISCOUNT'];
            //print_r($prePostVal);
            if ($ambulanceDIsc > 0) {
                $premium = round($premium * $ambulanceDIsc);
                //echo '<br/>After Ambulance-'.$premium = round($premium*$ambulanceDIsc);
            }
        }
        //$NORMALMATERNITY = "NORMALMATERNITY1";
        $NORMALMATERNITY = $NORMALMATERNITY1;
        if ($NORMALMATERNITY == 'yes') {
            //$NORMALMATERNITYTYPEID = "NORMALMATERNITYTYPE1";
            $NORMALMATERNITYTYPEID = $NORMALMATERNITYTYPE1;
            $normalmaternityVal = getMappingValues('CRMOAQNORMALMATMAPPING', 'NORMALID', @$NORMALMATERNITYTYPEID, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            //print_r($normalmaternityVal);
            $normalmaternityDIsc = @$normalmaternityVal[0]['LOADINGDISCOUNT'];
            if ($normalmaternityDIsc > 0) {
                $premium = round($premium * $normalmaternityDIsc);
                //echo '<br/>After Normal Maternity-'.$premium = round($premium*$normalmaternityDIsc);
            }
        }
        //$MATERNITY = "MATERNITY1";
        $MATERNITY = $MATERNITY1;
        if ($MATERNITY == 'yes') {
            //$MATERNITYTYPEID = "MATERNITYTYPE1";
            $MATERNITYTYPEID = $MATERNITYTYPE1;
            $maternityVal = getMappingValues('CRMOAQMATERNITYSIMAPPING', 'MATERNITYTYPEID', @$MATERNITYTYPEID, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            //print_r($maternityVal);
            $maternityDIsc = @$maternityVal[0]['LOADINGDISCOUNT'];
            if ($maternityDIsc > 0) {
                $premium = round($premium * $maternityDIsc);
                //echo '<br/>After Maternity-'.$premium = ($premium*$maternityDIsc);
            }
        }
        //$NEWBABY = "NEWBABY1";
        $NEWBABY = $NEWBABY1;
        if ($NEWBABY == 'yes') {
            $BABYAMOUNT = "BABYAMOUNT1";
            $BABYAMOUNTID = $$BABYAMOUNT;
            $babyVal = getMappingValues('CRMOAQNEWBORNBABYSIMAPPING', 'BABYID', @$BABYAMOUNTID, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $babyDIsc = @$babyVal[0]['LOADINGDISCOUNT'];
            if ($babyDIsc > 0) {
                $premium = round($premium * $babyDIsc);
                //echo '<br/>After BABY-'.$premium = ($premium*$babyDIsc);
            }
        }
        //$prepostNatalId = "GHIPREPOSTNATAL1";
        $prepostNatalId = $GHIPREPOSTNATAL1;
        if ($prepostNatalId != '') {
            $prepostNatalVal = getMappingValues('CRMOAQPREPOSTNATALMAPPING', 'PREPOSTNATALID', @$prepostNatalId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $prepostNatalDIsc = @$prepostNatalVal[0]['LOADINGDISCOUNT'];
            if ($prepostNatalDIsc > 0) {
                $premium = round($premium * $prepostNatalDIsc);
                //echo '<br/>After Pre Post Natal-'.$premium = ($premium*$prepostNatalDIsc);
            }
        }
        //$coverTypeId = "GHICOVERTYPE1";
        $coverTypeId = $GHICOVERTYPE1;
        if ($coverTypeId != '') {
            $coverTypeVal = getMappingValues('CRMOAQCOVERTYPESIMAPPING', 'COVERTYPEID', @$coverTypeId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $coverTypeDIsc = @$coverTypeVal[0]['LOADINGDISCOUNT'];
            if ($coverTypeDIsc > 0) {
                $premium = round($premium * $coverTypeDIsc);
                //echo '<br/>After Cover Type-'.$premium = ($premium*$coverTypeDIsc);
            }
        }
        //$diseaseId = "diseaseId1";
        $diseaseId = $diseaseId1;
        if ($diseaseId != '') {
            $diseaseVal = getMappingValues('CRMOAQDISEASEMAPPING', 'DISEASEID', @$diseaseId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $diseaseDIsc = @$diseaseVal[0]['LOADINGDISCOUNT'];
            if ($diseaseDIsc > 0) {
                $premium = round($premium * $diseaseDIsc);
                //echo '<br/>After DISEASE-'.$premium = ($premium*$diseaseDIsc);
            }
        }
        $CORPORATEFLOATER = $CORPORATEFLOATER1;
        if ($CORPORATEFLOATER == 'yes') {
            //$CFMOUNT 		 = "CFMOUNT1";
            $CFMOUNTID = $CFMOUNT1;
            $cfVal = getMappingValues('CRMOAQCFSIMAPPING', 'FLOATERID', @$CFMOUNTID, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $cfDIsc = @$cfVal[0]['LOADINGDISCOUNT'];
            if ($cfDIsc > 0) {
                $premium = round($premium * $cfDIsc);
                //echo '<br/>After CF-'.$premium = ($premium*$cfDIsc);
            }
        }
        if ($ROOMTYPEID != '') {
            $roomVal = getMappingValues('CRMOAQROOMSIMAPPING', 'ROOMTYPEID', @$ROOMTYPEID, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $roomDIsc = @$roomVal[0]['LOADINGDISCOUNT'];
            if ($roomDIsc > 0) {
                $premium = round($premium * $roomDIsc);
                //echo '<br/>After RoomType-'.$premium = ($premium*$roomDIsc);
            }
        }
        if ($ICULIMITID != '') {
            $icuVal = getMappingValues('CRMOAQICUMAPPING', 'ICULIMITID', @$ICULIMITID, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $icuDIsc = @$icuVal[0]['LOADINGDISCOUNT'];
            if ($icuDIsc > 0) {
                $premium = round($premium * $icuDIsc);
                //echo '<br/>After RoomType-'.$premium = ($premium*$icuDIsc);
            }
        }
        if ($copayId != '') {
            $copayVal = getMappingValues('CRMOAQCOPAYSIMAPPING', 'COPAYID', @$copayId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $copayDiscount = @$copayVal[0]['LOADINGDISCOUNT'];
            if ($copayDiscount > 0) {
                $premium = round($premium * $copayDiscount);
                //echo '<br/>After Copay-'.$premium = ($premium*$copayDiscount);
            }
        }
        if ($FAMILYDEFINITION != '') {
            $familyStaructureArr = fetchListCond('CRMOAQFAMILYSTRUCTURE', " WHERE ID =  " . @$FAMILYDEFINITION . " ");
            $familyMinMembers = $familyStaructureArr[0]['MINIMUMMEMBERS'];
            $familyMinPremium = $familyStaructureArr[0]['MINMEMPREMIUM'];
            $familyCalType = $familyStaructureArr[0]['CALCULATIONTYPE'];
            $familyVal = getMappingValues('CRMOAQFAMILYSIMAPPING', 'FAMILYID', @$FAMILYDEFINITION, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $familyDIsc = @$familyVal[0]['LOADINGDISCOUNT'];
            if ($familyDIsc > 0) {
                $premium = round($premium * $familyDIsc);
                //echo '<br/>After Family-'.$premium = ($premium*$familyDIsc);
            }
            if ($familyChkRatio == 'YES') {
                if ($EMPLOYEENO > 0) {
                    $peratio = round(@$PARENTSNO / @$EMPLOYEENO, 2);
                    //echo '<br/>Parents emp ratio:'.$peratio;
                }
                $ratioArr = fetchListCond('CRMOAQPARENTEMPRATIO', " WHERE STARTRATIO <=  " . @$peratio . " AND ENDRATIO >=  " . @$peratio . " AND STATUS = 'ACTIVE' ");
                $peRatioId = @$ratioArr[0]['ID'];
                if ($peRatioId != '') {
                    $peRatioVal = getMappingValues('CRMOAQPEMPRATIOMAPPING', 'PERATIOID', @$peRatioId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
                    $peRatioDIsc = @$peRatioVal[0]['LOADINGDISCOUNT'];
                    if ($peRatioDIsc > 0) {
                        $premium = round($premium * $peRatioDIsc);
                        //echo '<br/>'.$peRatioId.'--After Paret Employee Ratio-'.$premium = ($premium*$peRatioDIsc);
                    }
                }
            }
        }
        if ($INDUSTRYID != '') {
            $industryVal = getMappingValues('CRMOAQINDUSTRYSIMAPPING', 'INDUSTRYID', @$INDUSTRYID, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $industryDIsc = @$industryVal[0]['LOADINGDISCOUNT'];
            if ($industryDIsc > 0) {
                $premium = round($premium * $industryDIsc);
                //echo '<br/>After Industry-'.$premium = ($premium*$industryDIsc);
            }
        }
        /* $renewal = '1.10';
          if($renewal>0){
          echo '<br/>After Renewal-'.$premium = ($premium*$renewal);
          } */
        if (@$claimRatioId != '') {
            $claimRatioVal = '';
            $claimRatioVal = getMappingValues('CRMOAQCLAIMRATIOMAPPING', 'CLAIMRATIOID', @$claimRatioId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $renewal = '';
            $renewal = $claimRatioVal[0]['LOADINGDISCOUNT'];
            if ($renewal > 0) {
                $premium = round($premium * $renewal);
                //echo '<br/>'.$claimRatioId.'After Renewal-'.$premium = ($premium*$renewal);
            }
        }
        //$totalGroup = $totalGroupMembers;
        $groupSizeId = getGroupSizeId(@$totalGroupMembers);
        $groupSizeId = $groupSizeId[0]['ID'];
        //echo '<br/>'.$totalGroupMembers.'--'.$groupSizeId;	
        if ($groupSizeId != '') {
            $groupSizeVal = getMappingValues('CRMOAQGROUPSIZEMAPPING', 'GROUPSIZEID', @$groupSizeId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $groupSizeDIsc = @$groupSizeVal[0]['LOADINGDISCOUNT'];
            //print_r($groupSizeDIsc);
            if ($groupSizeDIsc > 0) {
                $premium = round($premium * $groupSizeDIsc);
                //echo '<br/>'.$totalGroupMembers.'--'.$groupSizeId.'After Group Size-'.$premium = ($premium*$groupSizeDIsc);
            }
        }
        if (@$sitype != '') {
            $siTypeRatioVal = getMappingValues('CRMOAQSITYPEMAPPING', 'SITYPE', @$sitype, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $sitypeloaddisc = $siTypeRatioVal[0]['LOADINGDISCOUNT'];
            if ($sitypeloaddisc > 0) {
                $premium = round($premium * $sitypeloaddisc);
                //echo '<br/>'.$claimRatioId.'After Renewal-'.$premium = ($premium*$renewal);
            }
        }
        $intermediacomm = $INTERMEDIARYCOMMISSION;
        if ($intermediacomm > 0) {
            $brokerageMultiplier = 100 / (100 - @$intermediacomm);
            if ($brokerageMultiplier > 0) {
                $premium = round($premium * $brokerageMultiplier);
                //echo '<br/>After Commision---'.$premium = $premium*$brokerageMultiplier;
            }
            //$premium = $premium/$intermediacomm;
            //echo '<br/>After Commision-'.$premium = $premium/$intermediacomm;
        }
        if ($CARDTYPE == 'physical') {
            if ($cardval > 0) {
                $premium = round($premium + $cardval);
                //echo '<br/>After Card-'.$premium = $premium+$cardval;
            }
        }
        $memberNo = '';
        //echo '<br/>Premium------'.$premium;
        $memberVar = $currentSlab . "_" . $ageGroupId;
        $memberNo = $$memberVar;
        //echo '<br>Member No---'.$memberNo = $$memberVar;
        if ($memberNo > 0) {
            $totalPremium = round(@$memberNo * @$premium);
            //echo '<br/>Total Premium----'.$totalPremium = $memberNo*$premium;
        } else {
            $totalPremium = '';
        }
        /* $quotesiquery = "INSERT INTO CRMOAQQUOTESUMINSURED (ID,QUOTEID,SIID,ROOMTYPEID,ICULIMITID,COPAY,COPAYID,AGEGROUPID,MEMBERNO,MATERNITYVAL,NORMALMATERNITYVAL,CORPORATEFLOATERVAL,BABYVAL,DISEASEVAL,PREPOSTVAL,AMBULANCEVAL,STATUS,PREMIUM,TOTALPREMIUM,CARDVAL,CREATETYPE,COPYID,COPYREFNUMBER,CREATEDBY,CREATEDON,UPDATEDBY,UPDATEDON) VALUES (CRMOAQQUOTESUMINSURED_SEQ.nextval,'".@$quoteId."','".@$sumInsuredId."','".@$ROOMTYPEID."','".@$ICULIMITID."','".@$copayDiscount."','".@$copayId."','".@$ageGroupId."','".@$memberNo."','".@$maternityDIsc."','".@$normalmaternityDIsc."','".@$cfDIsc."','".@$babyDIsc."','".@$diseaseDIsc."','".@$prePostDIsc."','".@$ambulanceDIsc."','ACTIVE','".@$premium."','".@$totalPremium."','".@$cardval."','".@$CREATETYPE."','".@$COPYID."','".@$COPYREFNUMBER."','".@$_SESSION['userName']."','".@$entryTime."','".@$_SESSION['userName']."','".@$entryTime."')";	 
          $quotesiquery = @oci_parse($conn,$quotesiquery);
          @oci_execute($quotesiquery); */
        if ($memberNo > 0) {
            $totalMembers = $totalMembers + $memberNo;
            //echo '<br/>Total Members----'.$totalMembers;	
            $PREMIUMEXCLUDINGTAX = round($PREMIUMEXCLUDINGTAX + @$totalPremium);
            //echo '<br/>Premium Exluding tax----'.$PREMIUMEXCLUDINGTAX;
        }
    }
}
if ($familyCalType == 'prorata') {
    if ($totalMembers < $familyMinMembers) {
        //75/50*2 Lac = 3 Lac
        //$PREMIUMEXCLUDINGTAX 	= round(@$PREMIUMEXCLUDINGTAX+@$familyMinPremium);
        $PREMIUMEXCLUDINGTAX = round((@$familyMinMembers / @$totalMembers) * $PREMIUMEXCLUDINGTAX);
    } else {
        $PREMIUMEXCLUDINGTAX = round(@$PREMIUMEXCLUDINGTAX);
        $familyMinPremium = '';
    }
} else {
    if ($totalMembers < $familyMinMembers) {
        $PREMIUMEXCLUDINGTAX = round(@$PREMIUMEXCLUDINGTAX + @$familyMinPremium);
    } else {
        $PREMIUMEXCLUDINGTAX = round(@$PREMIUMEXCLUDINGTAX);
        $familyMinPremium = '';
    }
}
/* if($totalMembers<$familyMinMembers){
  $PREMIUMEXCLUDINGTAX 	= round(@$PREMIUMEXCLUDINGTAX+@$familyMinPremium);
  } else {
  $PREMIUMEXCLUDINGTAX 	= round(@$PREMIUMEXCLUDINGTAX);
  $familyMinPremium 		= '';
  } */
//echo '<br/>Premium Exluding tax----'.$PREMIUMEXCLUDINGTAX;
//echo '<br/>Total members----'.$totalMembers.'---------'.@$familyMinMembers;
/* Health check-up */
$array_tblsite = fetchListById('CRMOAQSETTINGS', 'ID', '3');
$healthPercent = @$array_tblsite[0]['FREECHECKUPNO'];
$CHECKUPCOST = @$array_tblsite[0]['CHECKUPCOST'];
if ($healthPercent > 0) {
    $healthAmt = ($PREMIUMEXCLUDINGTAX * $healthPercent) / 100;
    $healthAmt = round($healthAmt);
    $freeChkUpNo = round($healthAmt / $CHECKUPCOST);
}
//$taxAmount 		= 	($PREMIUMEXCLUDINGTAX*$serviceTax)/100;
//$taxAmount 		= 	round($taxAmount);
//echo '<br/>taxAmount----'.$taxAmount;
//$quoteTotal 	= 	round($PREMIUMEXCLUDINGTAX+$taxAmount);
$quoteTotal = round($PREMIUMEXCLUDINGTAX);
//echo '<br/>Quote Total----'.$quoteTotal.'---------'.@$familyMinPremium;
$firstQuoteTotal = $quoteTotal;
//echo '<br/>firstQuoteTotal----'.$firstQuoteTotal;

if ($POLICYTYPE == 'Renewal') {
    $lastYearPremium = $PREMIUMAMOUNT;
    $brokerage = $INTERMEDIARYCOMMISSION;
    $lastYearLives = $LASTYEARLIVES;
    $livesNumber = $totalGroupMembers;
    $claimsRatio = @$claimRatioPercent;
    //$lastYearClaimRatio	=	((@$claimsRatio*@$lastYearPremium*1.1)/(1-@$brokerage)*(($livesNumber)/((@$livesNumber+@$lastYearLives)/2)));
    // Above formula in piecies
    $totlives = @$livesNumber + @$lastYearLives;
    $avglives = @$totlives / 2;
    $avglvs = @$livesNumber / @$avglives;
    $broVal = (100 - @$brokerage) / 100;

    $clmratiolastval = (@$claimsRatio * @$lastYearPremium * 1.1) / 100;
    $clmrtval = @$clmratiolastval / @$broVal;
    $lastYearClaimRatio = @$clmrtval * @$avglvs;
    $claimrateval = @$lastYearClaimRatio;
}
//echo '<br>lastYearClaimRatio---------'.@$lastYearClaimRatio;
$totalPremium = '';
$totalMembers = '';
$premium = '';
$PREMIUMEXCLUDINGTAX = '';
$taxAmount = '';
$quoteTotal = '';
//echo '------------------------------------------------------------------------------------------------------------------------------------------------------------';
for ($i = 1; $i <= $slabNo; $i++) {
    $sumInsured = "SUMINSURED" . $i;
    $sumInsuredId = $$sumInsured;
    $siArr = fetchListCondsWithColumn('SI', 'CRMOAQSI', " WHERE SIID = " . @$sumInsuredId . " ");
    $sumInsuredAmount = @$siArr[0]['SI'];
    $copayDisc = "copay" . $i;
    $copayId = $$copayDisc;
    $ROOMTYPEDESC = "ROOMTYPE" . $i;
    $ROOMTYPEID = $$ROOMTYPEDESC;
    $ICULIMITDESC = "ICULIMIT" . $i;
    $ICULIMITID = $$ICULIMITDESC;
    $currentSlab = "slab" . $i;
    $cslab = $$currentSlab;
    $sitype = "SITYPE" . $i;
    $sitype = $$sitype;
    $sitypereason = "SITYPEREMARKS" . $i;
    $sitypereason = $$sitypereason;
    $gradeBaisisId = "gradeBaisisId" . $i;
    $gradeBaisisId = $$gradeBaisisId;
    $gradeBasisArr = explode("_", @$gradeBaisisId);
    $gradeId = @$gradeBasisArr[0];
    $gradeVal = @$gradeBasisArr[1];

    for ($j = 0; $j < count($cslab); $j++) {
        $ageGroupId = $cslab[$j];
        $mapVal = getMappingValues('CRMOAQLOCATIONSIMAPPING', 'LOCATIONID', @$locId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
        $premium = round(@$mapVal[0]['PREMIUM']);
        //echo '<br/>Premium-'.$premium = $mapVal[0]['PREMIUM'];

        if ($POLICYTYPELOADDISC > 0) {
            $premium = round($premium * $POLICYTYPELOADDISC);
            //echo '<br/>After Policy Type----'.$premium = round($premium*$POLICYTYPELOADDISC);
        }

        if ($WAITINGPERIOD == 'applicable') {
            if ($WAITINGPERIODPERCENT > 0) {
                $premium = round($premium * $WAITINGPERIODPERCENT);
                //echo '<br/>After Waiting-'.$premium = round($premium*$WAITINGPERIODPERCENT);
            }
        }
        if ($PEDEXCLUSIONPERCENT > 0) {
            $premium = round($premium * $PEDEXCLUSIONPERCENT);
            //echo '<br/>After PED-'.$premium = round($premium*$PEDEXCLUSIONPERCENT);
        }
        //$prePostId = "prePostId1";
        $prePostId = @$prePostId1;
        //echo $prePostId.'--'.$ageGroupId.'--'.$sumInsuredId;
        if ($prePostId != '') {
            $prePostVal = getMappingValues('CRMOAQPREPOSTSIMAPPING', 'PREPOSTID', @$prePostId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $prePostDIsc = @$prePostVal[0]['LOADINGDISCOUNT'];
            //print_r($prePostVal);
            if ($prePostDIsc > 0) {
                $premium = round($premium * $prePostDIsc);
                //echo '<br/>After PREPOST-'.$premium = round($premium*$prePostDIsc);
            }
        }
        //$ambulanceId1 	= "ambulanceId1";
        $ambulanceId = $ambulanceId1;
        if ($ambulanceId != '') {
            $ambulanceVal = getMappingValues('CRMOAQAMBULANCEMAPPING', 'AMBULANCEID', @$ambulanceId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $ambulanceDIsc = @$ambulanceVal[0]['LOADINGDISCOUNT'];
            //print_r($prePostVal);
            if ($ambulanceDIsc > 0) {
                $premium = round($premium * $ambulanceDIsc);
                //echo '<br/>After Ambulance-'.$premium = round($premium*$ambulanceDIsc);
            }
        }
        //echo '<br>Last year claim ratio----'.$lastYearClaimRatio;	
        //echo '<br>firstQuoteTotal----'.$firstQuoteTotal;	
        if ($POLICYTYPE == 'Renewal') {
            if ($lastYearClaimRatio >= 0) {
                //echo '<br>Last year claim ratio----'.$lastYearClaimRatio;	
                //echo '<br>firstQuoteTotal----'.$firstQuoteTotal;	
                if ($COPYID > 0) {
                    if ($NEWPOLICYTYPE == 'Renewal') {
                        $lastYearClaimRatioVal = @$clratioper;
                    }
                } else {
                    $lastYearClaimRatioVal = @$lastYearClaimRatio / @$firstQuoteTotal;
                }
                if ($lastYearClaimRatioVal == '') {
                    $lastYearClaimRatioVal = '0';
                }
                //echo '<br>firstQuoteTotal----'.$lastYearClaimRatioVal;
                $ratioArr = fetchListCond('CRMOAQLASTYEARRATIO', " WHERE STARTRATIO <=  " . @$lastYearClaimRatioVal . " AND ENDRATIO >=  " . @$lastYearClaimRatioVal . " ");
                $ratioId = @$ratioArr[0]['ID'];
                if ($ratioId != '') {
                    $ratioVal = getMappingValues('CRMOAQLASTYEARRATIOMAPPING', 'RATIOID', @$ratioId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
                    $ratioDIsc = @$ratioVal[0]['LOADINGDISCOUNT'];
                    if ($ratioDIsc > 0) {
                        $premium = round($premium * $ratioDIsc);
                        //echo '<br/>'.$ratioId.'--After Last Year Ratio-'.$premium = round($premium*$ratioDIsc);
                    }
                }
            }
        }
        //$NORMALMATERNITY = "NORMALMATERNITY1";
        $NORMALMATERNITY = $NORMALMATERNITY1;
        if ($NORMALMATERNITY == 'yes') {
            //$NORMALMATERNITYTYPEID = "NORMALMATERNITYTYPE1";
            $NORMALMATERNITYTYPEID = $NORMALMATERNITYTYPE1;
            $normalmaternityVal = getMappingValues('CRMOAQNORMALMATMAPPING', 'NORMALID', @$NORMALMATERNITYTYPEID, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            //print_r($normalmaternityVal);
            $normalmaternityDIsc = @$normalmaternityVal[0]['LOADINGDISCOUNT'];
            if ($normalmaternityDIsc > 0) {
                $premium = round($premium * $normalmaternityDIsc);
                //echo '<br/>After Normal Maternity-'.$premium = round($premium*$normalmaternityDIsc);
            }
        }
        //$MATERNITY = "MATERNITY1";
        $MATERNITY = $MATERNITY1;
        if ($MATERNITY == 'yes') {
            //$MATERNITYTYPEID = "MATERNITYTYPE1";
            $MATERNITYTYPEID = $MATERNITYTYPE1;
            $maternityVal = getMappingValues('CRMOAQMATERNITYSIMAPPING', 'MATERNITYTYPEID', @$MATERNITYTYPEID, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            //print_r($maternityVal);
            $maternityDIsc = @$maternityVal[0]['LOADINGDISCOUNT'];
            if ($maternityDIsc > 0) {
                $premium = round($premium * $maternityDIsc);
                //echo '<br/>After Maternity-'.$premium = round($premium*$maternityDIsc);
            }
        }
        //$NEWBABY = "NEWBABY1";
        $NEWBABY = $NEWBABY1;
        if ($NEWBABY == 'yes') {
            $BABYAMOUNT = "BABYAMOUNT1";
            $BABYAMOUNTID = $$BABYAMOUNT;
            $babyVal = getMappingValues('CRMOAQNEWBORNBABYSIMAPPING', 'BABYID', @$BABYAMOUNTID, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $babyDIsc = @$babyVal[0]['LOADINGDISCOUNT'];
            if ($babyDIsc > 0) {
                $premium = round($premium * $babyDIsc);
                //echo '<br/>After BABY-'.$premium = round($premium*$babyDIsc);
            }
        }
        //$prepostNatalId = "GHIPREPOSTNATAL1";
        $prepostNatalId = $GHIPREPOSTNATAL1;
        if ($prepostNatalId != '') {
            $prepostNatalVal = getMappingValues('CRMOAQPREPOSTNATALMAPPING', 'PREPOSTNATALID', @$prepostNatalId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $prepostNatalDIsc = @$prepostNatalVal[0]['LOADINGDISCOUNT'];
            if ($prepostNatalDIsc > 0) {
                $premium = round($premium * $prepostNatalDIsc);
                //echo '<br/>After Pre Post Natal-'.$premium = ($premium*$prepostNatalDIsc);
            }
        }
        //$coverTypeId = "GHICOVERTYPE1";
        $coverTypeId = $GHICOVERTYPE1;
        if ($coverTypeId != '') {
            $coverTypeVal = getMappingValues('CRMOAQCOVERTYPESIMAPPING', 'COVERTYPEID', @$coverTypeId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $coverTypeDIsc = @$coverTypeVal[0]['LOADINGDISCOUNT'];
            if ($coverTypeDIsc > 0) {
                $premium = round($premium * $coverTypeDIsc);
                //echo '<br/>After Cover Type-'.$premium = ($premium*$coverTypeDIsc);
            }
        }
        //$diseaseId = "diseaseId1";
        $diseaseId = $diseaseId1;
        if ($diseaseId != '') {
            $diseaseVal = getMappingValues('CRMOAQDISEASEMAPPING', 'DISEASEID', @$diseaseId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $diseaseDIsc = @$diseaseVal[0]['LOADINGDISCOUNT'];
            if ($diseaseDIsc > 0) {
                $premium = round($premium * $diseaseDIsc);
                //echo '<br/>After DISEASE-'.$premium = round($premium*$diseaseDIsc);
            }
        }
        $CORPORATEFLOATER = $CORPORATEFLOATER1;
        if ($CORPORATEFLOATER == 'yes') {
            $CFMOUNTID = $CFMOUNT1;
            $cfVal = getMappingValues('CRMOAQCFSIMAPPING', 'FLOATERID', @$CFMOUNTID, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $cfDIsc = @$cfVal[0]['LOADINGDISCOUNT'];
            if ($cfDIsc > 0) {
                $premium = round($premium * $cfDIsc);
                //echo '<br/>After CF-'.$premium = round($premium*$cfDIsc);
            }
        }
        if ($ROOMTYPEID != '') {
            $roomVal = getMappingValues('CRMOAQROOMSIMAPPING', 'ROOMTYPEID', @$ROOMTYPEID, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $roomDIsc = @$roomVal[0]['LOADINGDISCOUNT'];
            if ($roomDIsc > 0) {
                $premium = round($premium * $roomDIsc);
                //echo '<br/>After RoomType-'.$premium = round($premium*$roomDIsc);
            }
        }
        if ($ICULIMITID != '') {
            $icuVal = getMappingValues('CRMOAQICUMAPPING', 'ICULIMITID', @$ICULIMITID, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $icuDIsc = @$icuVal[0]['LOADINGDISCOUNT'];
            if ($icuDIsc > 0) {
                $premium = round($premium * $icuDIsc);
                //echo '<br/>After RoomType-'.$premium = round($premium*$icuDIsc);
            }
        }
        if ($copayId != '') {
            $copayVal = getMappingValues('CRMOAQCOPAYSIMAPPING', 'COPAYID', @$copayId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $copayDiscount = @$copayVal[0]['LOADINGDISCOUNT'];
            if ($copayDiscount > 0) {
                $premium = round($premium * $copayDiscount);
                //echo '<br/>After Copay-'.$premium = round($premium*$copayDiscount);
            }
        }
        if ($FAMILYDEFINITION != '') {
            $familyStaructureArr = fetchListCond('CRMOAQFAMILYSTRUCTURE', " WHERE ID =  " . @$FAMILYDEFINITION . " ");
            $familyMinMembers = $familyStaructureArr[0]['MINIMUMMEMBERS'];
            $familyMinPremium = $familyStaructureArr[0]['MINMEMPREMIUM'];
            $familyCalType = $familyStaructureArr[0]['CALCULATIONTYPE'];
            $familyVal = getMappingValues('CRMOAQFAMILYSIMAPPING', 'FAMILYID', @$FAMILYDEFINITION, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $familyDIsc = @$familyVal[0]['LOADINGDISCOUNT'];
            if ($familyDIsc > 0) {
                $premium = round($premium * $familyDIsc);
                //echo '<br/>After Family-'.$premium = round($premium*$familyDIsc);
            }
            if ($familyChkRatio == 'YES') {
                if ($EMPLOYEENO > 0) {
                    $peratio = round(@$PARENTSNO / @$EMPLOYEENO, 2);
                    //echo '<br/>Parents emp ratio:'.$peratio;
                }
                $ratioArr = fetchListCond('CRMOAQPARENTEMPRATIO', " WHERE STARTRATIO <=  " . @$peratio . " AND ENDRATIO >=  " . @$peratio . " AND STATUS = 'ACTIVE' ");
                $peRatioId = @$ratioArr[0]['ID'];
                if ($peRatioId != '') {
                    $peRatioVal = getMappingValues('CRMOAQPEMPRATIOMAPPING', 'PERATIOID', @$peRatioId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
                    $peRatioDIsc = @$peRatioVal[0]['LOADINGDISCOUNT'];
                    if ($peRatioDIsc > 0) {
                        $premium = round($premium * $peRatioDIsc);
                        //echo '<br/>'.$peRatioId.'--After Paret Employee Ratio-'.$premium = round($premium*$peRatioDIsc);
                    }
                }
            }
        }
        if ($INDUSTRYID != '') {
            $industryVal = getMappingValues('CRMOAQINDUSTRYSIMAPPING', 'INDUSTRYID', @$INDUSTRYID, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $industryDIsc = @$industryVal[0]['LOADINGDISCOUNT'];
            if ($industryDIsc > 0) {
                $premium = round($premium * $industryDIsc);
                //echo '<br/>After Industry-'.$premium = round($premium*$industryDIsc);
            }
        }
        /* $renewal = '1.10';
          if($renewal>0){
          echo '<br/>After Renewal-'.$premium = ($premium*$renewal);
          } */
        if (@$claimRatioId != '') {
            $claimRatioVal = '';
            $claimRatioVal = getMappingValues('CRMOAQCLAIMRATIOMAPPING', 'CLAIMRATIOID', @$claimRatioId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $renewal = '';
            $renewal = $claimRatioVal[0]['LOADINGDISCOUNT'];
            if ($renewal > 0) {
                $premium = round($premium * $renewal);
                //echo '<br/>'.$claimRatioId.'After Renewal-'.$premium = round($premium*$renewal);
            }
        }
        //$totalGroup = $totalGroupMembers;
        $groupSizeId = getGroupSizeId(@$totalGroupMembers);
        $groupSizeId = $groupSizeId[0]['ID'];
        //echo '<br/>'.$totalGroupMembers.'--'.$groupSizeId;	
        if ($groupSizeId != '') {
            $groupSizeVal = getMappingValues('CRMOAQGROUPSIZEMAPPING', 'GROUPSIZEID', @$groupSizeId, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $groupSizeDIsc = @$groupSizeVal[0]['LOADINGDISCOUNT'];
            //print_r($groupSizeDIsc);
            if ($groupSizeDIsc > 0) {
                $premium = round($premium * $groupSizeDIsc);
                //echo '<br/>'.$totalGroupMembers.'--'.$groupSizeId.'After Group Size-'.$premium = round($premium*$groupSizeDIsc);
            }
        }
        if (@$sitype != '') {
            $siTypeRatioVal = getMappingValues('CRMOAQSITYPEMAPPING', 'SITYPE', @$sitype, 'AGEGROUPID', @$ageGroupId, 'SIID', @$sumInsuredId);
            $sitypeloaddisc = $siTypeRatioVal[0]['LOADINGDISCOUNT'];
            if ($sitypeloaddisc > 0) {
                $premium = round($premium * $sitypeloaddisc);
                //echo '<br/>'.$claimRatioId.'After Renewal-'.$premium = ($premium*$renewal);
            }
        }
        $intermediacomm = $INTERMEDIARYCOMMISSION;
        if ($intermediacomm > 0) {
            $brokerageMultiplier = 100 / (100 - @$intermediacomm);
            if ($brokerageMultiplier > 0) {
                $premium = round($premium * $brokerageMultiplier);
                //echo '<br/>After Commision---'.$premium = round($premium*$brokerageMultiplier);
            }
            //$premium = $premium/$intermediacomm;
            //echo '<br/>After Commision-'.$premium = $premium/$intermediacomm;
        }
        if ($CARDTYPE == 'physical') {
            if ($cardval > 0) {
                $premium = round($premium + $cardval);
                //echo '<br/>After Card-'.$premium = round($premium+$cardval);
            }
        }
        $memberNo = '';
        //echo '<br/>Premium------'.$premium;
        $memberVar = $currentSlab . "_" . $ageGroupId;
        $memberNo = $$memberVar;
        //echo '<br>Member No---'.$memberNo = $$memberVar;
        if ($memberNo > 0) {
            $totalPremium = round($memberNo * $premium);
            //echo '<br/>Total Premium----'.$totalPremium = round($memberNo*$premium);
        } else {
            $totalPremium = '';
        }

        $quotesiquery = "INSERT INTO CRMOAQQUOTESUMINSURED (ID,QUOTEID,SIID,ROOMTYPEID,ICULIMITID,COPAY,COPAYID,AGEGROUPID,MEMBERNO,MATERNITYVAL,NORMALMATERNITYVAL,CORPORATEFLOATERVAL,BABYVAL,DISEASEVAL,PREPOSTVAL,AMBULANCEVAL,COVERTYPEVAL,PREPOSTNATALVAL,STATUS,PREMIUM,TOTALPREMIUM,CARDVAL,CREATETYPE,COPYID,COPYREFNUMBER,SITYPE,SITYPEVAL,SITYPEREASON,GRADEBASISID,GRADEBASISVAL,CREATEDBY,CREATEDON,UPDATEDBY,UPDATEDON) VALUES (CRMOAQQUOTESUMINSURED_SEQ.nextval,'" . @$quoteId . "','" . @$sumInsuredId . "','" . @$ROOMTYPEID . "','" . @$ICULIMITID . "','" . @$copayDiscount . "','" . @$copayId . "','" . @$ageGroupId . "','" . @$memberNo . "','" . @$maternityDIsc . "','" . @$normalmaternityDIsc . "','" . @$cfDIsc . "','" . @$babyDIsc . "','" . @$diseaseDIsc . "','" . @$prePostDIsc . "','" . @$ambulanceDIsc . "','" . @$coverTypeDIsc . "','" . @$prepostNatalDIsc . "','ACTIVE','" . @$premium . "','" . @$totalPremium . "','" . @$cardval . "','" . @$CREATETYPE . "','" . @$COPYID . "','" . @$COPYREFNUMBER . "','" . @$sitype . "','" . @$sitypeloaddisc . "','" . @$sitypereason . "','" . @$gradeId . "','" . @$gradeVal . "','" . @$_SESSION['userName'] . "','" . @$entryTime . "','" . @$_SESSION['userName'] . "','" . @$entryTime . "')";

        writePremiumData($insertquery); //please remove. @SHAKTIRANA

        $quotesiquery = @oci_parse($conn, $quotesiquery);
        if (oci_execute($quotesiquery) === false) {
            if ($IS_WEB_REQUEST) {
                $arr = array('msg' => 'Something went wrong', 'ref_no' => '');
                echo json_encode($arr);
                exit;
            }
        }
        if ($memberNo > 0) {
            $totalMembers = $totalMembers + $memberNo;
            //echo '<br/>Total Members----'.$totalMembers;	
            $PREMIUMEXCLUDINGTAX = round($PREMIUMEXCLUDINGTAX + @$totalPremium);
            //echo '<br/>Premium Exluding tax----'.$PREMIUMEXCLUDINGTAX;
        }
    }
}
if ($familyCalType == 'prorata') {
    if ($totalMembers < $familyMinMembers) {
        //75/50*2 Lac = 3 Lac
        //$PREMIUMEXCLUDINGTAX 	= round(@$PREMIUMEXCLUDINGTAX+@$familyMinPremium);
        $PREMIUMEXCLUDINGTAX = round((@$familyMinMembers / @$totalMembers) * $PREMIUMEXCLUDINGTAX);
    } else {
        $PREMIUMEXCLUDINGTAX = round(@$PREMIUMEXCLUDINGTAX);
        $familyMinPremium = '';
    }
} else {
    if ($totalMembers < $familyMinMembers) {
        $PREMIUMEXCLUDINGTAX = round(@$PREMIUMEXCLUDINGTAX + @$familyMinPremium);
    } else {
        $PREMIUMEXCLUDINGTAX = round(@$PREMIUMEXCLUDINGTAX);
        $familyMinPremium = '';
    }
}
//echo '<br/>Premium Exluding tax----'.$PREMIUMEXCLUDINGTAX;
//echo '<br/>Total members----'.$totalMembers.'---------'.@$familyMinMembers;
/* Baljeet************************************************************************************************************************** */
$PREMIUMEXCLUDINGTAX = @$PREMIUMEXCLUDINGTAX + @$opdPremium + @$hlthChkPremium;
/* * ************************************************************************************************************************* */
/* Health check-up */
/* Partner Source Loading**************************************************************************************************** */
if ($sourceloading > 0) {
    $PREMIUMEXCLUDINGTAX = round($PREMIUMEXCLUDINGTAX * $sourceloading);
}
/* * ************************************************************************************************************************* */
$array_tblsite = fetchListById('CRMOAQSETTINGS', 'ID', '3');
$healthPercent = @$array_tblsite[0]['FREECHECKUPNO'];
$CHECKUPCOST = @$array_tblsite[0]['CHECKUPCOST'];
if ($healthPercent > 0) {
    $healthAmt = ($PREMIUMEXCLUDINGTAX * $healthPercent) / 100;
    $healthAmt = round($healthAmt);
    $freeChkUpNo = round($healthAmt / $CHECKUPCOST);
}
$taxAmount = ($PREMIUMEXCLUDINGTAX * $serviceTax) / 100;
$taxAmount = round($taxAmount);
//echo '<br/>PREMIUMEXCLUDINGTAX----'.$PREMIUMEXCLUDINGTAX;
//echo '<br/>taxAmount----'.$taxAmount;
$quoteTotal = round($PREMIUMEXCLUDINGTAX + $taxAmount);
//echo '<br/>Quote Total----'.$quoteTotal.'---------'.@$familyMinPremium;
/* if(@$CREATETYPE=='FRESH') {
  $strlength = strlen(@$quoteId);
  $concat='';
  for($len=@$strlength;$len<7;$len++){
  $concat.= '0';
  }
  $refNo = 'OAQ'.@$concat.''.@$quoteId;
  $REFRENCENUMBER = @$refNo;
  $COPYID = '';
  $COPYREFNUMBER = '';
  } else {
  $COPYID 		= @$COPYID;
  $REFRENCENUMBER = @$REFRENCENUMBER;
  $COPYREFNUMBER	= @$REFRENCENUMBER;
  $copyQuoteCount = countQuoteCopy(@$COPYID);
  $incrementCount = @$copyQuoteCount+1;
  $strlength = strlen(@$incrementCount);
  $concat='';
  for($len=0;$len<1;$len++){
  $concat.= '0';
  }
  $incrementCount = @$concat.''.@$incrementCount;
  $REFRENCENUMBER = @$REFRENCENUMBER.'_'.@$incrementCount;

  } */
$year = date('y', time());
$month = date('m', time());
if ($CREATETYPE == 'FRESH') {
    $firstQuoteCreateDate = @$entryTime;
    $strlength = strlen(@$quoteId);
    $concat = '';
    for ($len = @$strlength; $len < 6; $len++) {
        $concat.= '0';
    }
    $refNo = 'H' . @$year . @$month . @$concat . @$quoteId;
    $REFRENCENUMBER = @$refNo;
    $COPYID = '0';
} else {
    $firstQuoteCreateDate = @$firstQuoteCreateDate;
    $COPYID = @$copyQuoteId;

    $copyrefarr = explode("_", @$COPYREFNO);
    $COPYREFNO1 = @$copyrefarr[0];
    $copyQuoteCount = countQuoteCopyNew(@$COPYREFNO1);
    $incrementCount = @$copyQuoteCount;
    if ($COPYREFNO1 != '') {
        $COPYREFNOVAL = @$COPYREFNO1;
    } else {
        $COPYREFNOVAL = @$COPYREFNO;
    }
    $REFRENCENUMBER = @$COPYREFNOVAL . '_' . @$incrementCount;
}
if ($CREATETYPE == 'FRESH') {
    $QUOTEPARENT = $quoteId;
    $QUOTEPARENTNO = '0';
} else {
    $QUOTEPARENT = $QUOTEPARENT;
    $QUOTEPARENTNO = $QUOTEPARENTNO + 1;
}

$updatesql = "UPDATE CRMOAQQUOTE SET CHECKUPCOST = '" . @$CHECKUPCOST . "',RACKRATE = '" . @$firstQuoteTotal . "',CLAIMRATE = '" . @$claimrateval . "',CHECKUPPERCENT = '" . @$healthPercent . "',FREECHECKUPNO = '" . @$freeChkUpNo . "',TOTALPREMIUM = '" . @$quoteTotal . "',TOTALMEMBERS = '" . @$totalMembers . "',TAXAMOUNT = '" . @$taxAmount . "',PREMIUMEXCLUDINGTAX='" . @$PREMIUMEXCLUDINGTAX . "',FAMILYMINMEMBERADD='" . @$familyMinPremium . "',REFRENCENUMBER='" . @$REFRENCENUMBER . "',COPYID='" . @$COPYID . "',COPYREFNUMBER='" . @$COPYREFNUMBER . "',QUOTEPARENT='" . @$QUOTEPARENT . "',QUOTEPARENTNO='" . @$QUOTEPARENTNO . "',CALCULATIONTYPE='" . @$familyCalType . "',CLAIMRATIOPERCENT = '" . @$lastYearClaimRatioVal . "',OPDPREMIUM='" . @$opdPremium . "',FIRSTQUOTECREATION='" . @$firstQuoteCreateDate . "',SOURCELOADING='" . @$sourceloading . "',UPDATEDBY='" . $_SESSION['userName'] . "',UPDATEDON= '" . @$entryTime . "' WHERE ID = '" . @$quoteId . "' ";

writePremiumData($updatesql); //please remove. @SHAKTIRANA

$updatesql = @oci_parse($conn, $updatesql);
if (oci_execute($updatesql)) {
    if ($IS_WEB_REQUEST) {
        $arr = array('msg' => 'Quote created', 'ref_no' => $REFRENCENUMBER, 'premiumamount' => $quoteTotal);
        echo json_encode($arr);
        exit;
    }
} else {
    if ($IS_WEB_REQUEST) {
        $arr = array('msg' => 'Something went wrong', 'ref_no' => '');
        echo json_encode($arr);
        exit;
    }
}
//echo '<br>sql----'.@$updatesql;
if ($POLICYTYPE == 'Renewal') {
    //echo 'Last year claim ratio----'.$lastYearClaimRatio;		
    $lastYearClaimRatioVal = round(@$lastYearClaimRatio / @$firstQuoteTotal);
    $ratioArr = fetchListCond('CRMOAQLASTYEARRATIO', " WHERE STARTRATIO <=  " . @$lastYearClaimRatioVal . " AND ENDRATIO >=  " . @$lastYearClaimRatioVal . " ");
    $lastYearClaimRatioId = @$ratioArr[0]['ID'];
}
/* if($POLICYTYPE=='Renewal') {
  if(@$lastYearClaimRatioId=='') {
  $deletequotesql="DELETE FROM CRMOAQQUOTE WHERE ID = '".@$quoteId."' ";
  $deletequotesql = @oci_parse($conn,$deletequotesql);
  @oci_execute($deletequotesql);

  $deletequotesi="DELETE FROM CRMOAQQUOTESUMINSURED WHERE QUOTEID = '".@$quoteId."' ";
  $deletequotesi = @oci_parse($conn,$deletequotesi);
  @oci_execute($deletequotesi);
  //echo "Last year claim ratio is greater than defined value. Please route the case to U/W team.";
  $_SESSION['quotefailstatus'] = "Last year claim ratio is greater than defined value. Please route the case to U/W team.";
  ?>
  <script>
  document.location="quote_list.php?QUOTETYPE=GHI&POLICYTYPE=<?= $POLICYTYPE;?>"
  </script>
  <?php die();
  }
  } */
unset($_SESSION['quoteform']);
?>
<script type="text/javascript">
//var r = confirm('Are you sure you want to generate quotes now.');
//if (r==true){
    document.location = "quoteDetails.php?quoteId=<?php echo @$quoteId; ?>"
//} else {
//document.location="quote_list.php?QUOTETYPE=GHI&POLICYTYPE= $POLICYTYPE?PRODUCTNAME=GHI"
//} 
</script>