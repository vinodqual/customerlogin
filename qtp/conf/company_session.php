<?php

//error_reporting(E_ALL);
ini_set('display_errors', 0);
date_default_timezone_set("Asia/Kolkata");
require_once ('sessionHandlerDB.php');
$handler = new sessionHandlerDB();
$timeout = 100 * 60;

session_start();
if (isset($_SESSION['start_time'])) {
    //$elapsed_time = time() - $_SESSION['start_time'];
    if ($_SESSION['start_time'] + $timeout < time()) {
        session_unset();
        session_destroy(); // session timed out
    } else {
        $_SESSION['start_time'] = time();
    }
} else {
    $_SESSION['start_time'] = time();
}

if ((trim(@$_SESSION["userName"]) == "") || (@$_SESSION["userName"] == "0") || (trim($_SESSION["type"]) == "") || (trim(@$_SESSION["userId"]) == "")) {
    $logtext = "[DATE:" . date("d-m-Y h:i:sa", time()) . "][SESSION(userId:" . @$_SESSION['userId'] . ")(userName:" . @$_SESSION['userName'] . ")(type:" . @$_SESSION['type'] . ")][SERVER(SERVER_ADDR:" . @$_SERVER['SERVER_ADDR'] . ")(REMOTE_ADDR:" . @$_SERVER['REMOTE_ADDR'] . ")(REQUEST_URI:" . @$_SERVER['REQUEST_URI'] . ")(REQUEST_TIME:" . date("d-m-Y h:i:sa", @$_SERVER['REQUEST_TIME']) . ")][MESSAGE(session parameter break)]" . PHP_EOL;
    $_SESSION = array(); // Unset all of the session variables.
    setcookie('PHPSESSID', '', time() - 3600, '/', '', 0, 0); // Destroy the cookie.
    foreach (@$_COOKIE as $key => $value) {
        setcookie($key, '', 1, '/');
    }
    session_destroy(); // Finally, destroy the session.
    file_put_contents('conf/sessionlogin.log', @$logtext, FILE_APPEND | LOCK_EX);
    $url = "Location:  index.php";
    header($url);
    exit;
}

/**
 * include file. throw exception if there is any issue in file.
 * @param type $fileName
 */
function qtp_include_once($fileName, $testing = 1) {
    if ($testing) {
        try {
            set_error_handler('fileInclude_handler');
            include_once($fileName);
            restore_error_handler();
        } catch (Exception $e) {
            print $e;
        }
    } else {
        include_once($fileName);
    }
}

function fileInclude_handler($errno, $errstr, $fileName) {
    echo "File include error: " . basename($fileName) . "\n";
}

//Only for testing permium = 0. Please remove this code. 16/03/2016 12:39 @SHAKTIRANA //
function writePremiumData($quotesiquery) {
    date_default_timezone_set('Asia/Kolkata');
    $writeQuoteInsertQuery = fopen('writeQuoteInsertQuery.txt', 'a');
    fwrite($writeQuoteInsertQuery, date('d-m-y h:m:i', time()) . '/*** GPA -- ' . @$_SESSION['name'] . ' ***/');
    fwrite($writeQuoteInsertQuery, "\n");
    fwrite($writeQuoteInsertQuery, $quotesiquery);
    fwrite($writeQuoteInsertQuery, "\n");
    fclose($writeQuoteInsertQuery);
}

//Only for testing permium = 0. Please remove this code. 16/03/2016 12:39 @SHAKTIRANA //
?>