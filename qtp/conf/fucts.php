<?php

function sanitize_data($input_data) {
    $searchArr = array("document", "write", "alert", "%", "$", ";", "+", "|", "#", "<", ">", ")", "(", "'", "\'", ",", "and", "USER_NAME", "AND");
    $input_data = str_replace("script", "", $input_data);
    $input_data = str_replace("iframe", "", $input_data);
    $input_data = str_replace("javascript", "", $input_data);
    $input_data = str_replace($searchArr, "", $input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}

function sanitize_title($input_data) {
    $searchArr = array("document", "write", "alert", "%", "$", ";", "+", "|", "#", "<", ">", ")", "(", "'", "\'", "and", "USER_NAME", "AND");
    $input_data = str_replace("script", "", $input_data);
    $input_data = str_replace("iframe", "", $input_data);
    $input_data = str_replace("javascript", "", $input_data);
    $input_data = str_replace($searchArr, "", $input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}

function dateChange($date) {
    if ($date != "00000000") {
        return substr(@$date, 6, 2) . '-' . substr(@$date, 4, 2) . '-' . substr(@$date, 0, 4);
    } else {
        return '';
    }
}

function postEmployementCheck($companyId = '', $type) {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        if ($type == "GroupUser") {
            $query = "SELECT CRMPOLICY.* FROM CRMPOLICY LEFT JOIN CRMCOMPANY ON CRMPOLICY.COMPANYID=CRMCOMPANY.COMPANYID  WHERE  CRMPOLICY.POSTEMPLOYMENT='Yes' AND CRMPOLICY.STATUS='ACTIVE' AND  CRMPOLICY.COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE CRMCOMPANY.GROUPCOMPANYID=" . @$companyId . ")";
        } else {
            $query = "SELECT CRMPOLICY.* FROM CRMPOLICY LEFT JOIN CRMCOMPANY ON CRMPOLICY.COMPANYID=CRMCOMPANY.COMPANYID WHERE  CRMPOLICY.STATUS='ACTIVE'  AND CRMPOLICY.POSTEMPLOYMENT='Yes' AND CRMPOLICY.COMPANYID = " . @$companyId;
        }
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function preEmployementCheck($companyId = '', $type) {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        if ($type == "GroupUser") {
            $query = "SELECT CRMPOLICY.* FROM CRMPOLICY LEFT JOIN CRMCOMPANY ON CRMPOLICY.COMPANYID=CRMCOMPANY.COMPANYID  WHERE  CRMPOLICY.PREEMPLOYMENT='Yes' AND CRMPOLICY.STATUS='ACTIVE' AND  CRMPOLICY.COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE CRMCOMPANY.GROUPCOMPANYID=" . @$companyId . ")";
        } else {
            $query = "SELECT CRMPOLICY.* FROM CRMPOLICY LEFT JOIN CRMCOMPANY ON CRMPOLICY.COMPANYID=CRMCOMPANY.COMPANYID WHERE  CRMPOLICY.STATUS='ACTIVE'  AND CRMPOLICY.PREEMPLOYMENT='Yes' AND CRMPOLICY.COMPANYID = " . @$companyId;
        }
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function appointmentSchdByCompany($companyId = '', $type) {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        if ($type == "GroupUser") {
            $query = "SELECT CRMPOLICY.* FROM CRMPOLICY LEFT JOIN CRMCOMPANY ON CRMPOLICY.COMPANYID=CRMCOMPANY.COMPANYID  WHERE  CRMPOLICY.APPTSCHDBYCOMPANY='Yes' AND CRMPOLICY.STATUS='ACTIVE' AND  CRMPOLICY.COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE CRMCOMPANY.GROUPCOMPANYID=" . @$companyId . ")";
        } else {
            $query = "SELECT CRMPOLICY.* FROM CRMPOLICY LEFT JOIN CRMCOMPANY ON CRMPOLICY.COMPANYID=CRMCOMPANY.COMPANYID WHERE  CRMPOLICY.STATUS='ACTIVE'  AND CRMPOLICY.APPTSCHDBYCOMPANY='Yes' AND CRMPOLICY.COMPANYID = " . @$companyId;
        }
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function checkHealthCheckup($companyId = '', $type) {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        if ($type == "GroupUser") {
            $query = "SELECT CRMPOLICY.* FROM CRMPOLICY LEFT JOIN CRMCOMPANY ON CRMPOLICY.COMPANYID=CRMCOMPANY.COMPANYID  WHERE  CRMPOLICY.HEALTHCHECKUP='Yes' AND CRMPOLICY.STATUS='ACTIVE' AND  CRMPOLICY.COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE CRMCOMPANY.GROUPCOMPANYID=" . @$companyId . ")";
        } else {
            $query = "SELECT CRMPOLICY.* FROM CRMPOLICY LEFT JOIN CRMCOMPANY ON CRMPOLICY.COMPANYID=CRMCOMPANY.COMPANYID WHERE  CRMPOLICY.STATUS='ACTIVE'  AND CRMPOLICY.HEALTHCHECKUP='Yes' AND CRMPOLICY.COMPANYID = " . @$companyId;
        }
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function getCompanyUserdetails($companyId, $type) {
    global $conn;
    $dataArray = array();
    if ($companyId != '') {
        $query = "SELECT * FROM COMPANYUSERTABLE WHERE COMPANYID =" . @$companyId . " AND TYPE ='" . $type . "'";
        $sql = @oci_parse($conn, $query);
        // Execute Query
        @oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function getGroupPolicyReportList($companyId, $policyId) {
    global $conn;
    $dataArray = array();
    if ($policyId != '') {
        $query = 'SELECT POLICYREPORT.*, (SELECT POLICYNUMBER FROM POLICY WHERE "POLICYREPORT".POLICYID=POLICY.POLICYID ) as POLICYNUMBER FROM "CRMPOLICYREPORT" WHERE POLICYID=' . $policyId . ' AND COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE  GROUPCOMPANYID=' . @$companyId . ')';
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function getPolicyReportList($companyId, $policyId) {
    global $conn;
    $dataArray = array();
    if ($policyId != '') {
        $query = 'SELECT POLICYREPORT.*, (SELECT POLICYNUMBER FROM POLICY WHERE "POLICYREPORT".POLICYID=POLICY.POLICYID ) as POLICYNUMBER FROM "CRMPOLICYREPORT" WHERE POLICYID=' . $policyId . ' AND COMPANYID IN ' . @$companyId;
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function getGroupPolicyListDetails($companyId, $policyNumber) {
    global $conn;
    $dataArray = array();
    if ($policyNumber != '') {
        $query = "SELECT * FROM CRMPOLICY WHERE POLICYNUMBER='" . @$policyNumber . "' AND COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE  GROUPCOMPANYID=" . @$companyId . ")";
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function getPolicyListDetails($companyId, $policyNumber) {
    global $conn;
    $dataArray = array();
    if ($policyNumber != '') {
        $query = "SELECT * FROM CRMPOLICY WHERE POLICYNUMBER='" . @$policyNumber . "' AND COMPANYID =" . @$companyId;
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function fetchPolicyReportList($policyId = '') {
    global $conn;
    $dataArray = array();
    if ($policyId > 0) {
        $query = 'SELECT POLICYREPORT.*, (SELECT POLICYNUMBER FROM CRMPOLICY WHERE "POLICYREPORT".POLICYID=POLICY.POLICYID ) as POLICYNUMBER FROM "CRMPOLICYREPORT" WHERE POLICYID=' . $policyId;
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function fetchGropPolicyList($companyId = '') {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        $query = 'SELECT * FROM "CRMPOLICY" WHERE COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE  GROUPCOMPANYID=' . @$companyId . ')';
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function get_particular_appointment_post_details($appointmentId, $companyId) {
    global $conn;
    $dataArray = array();
    $query = "SELECT CRMCOMPANYPOSTAPPOINTMENT.* FROM CRMCOMPANYPOSTAPPOINTMENT WHERE  COMPANYID = " . @$companyId . " AND APPOINTMENTID=" . @$appointmentId;
    $sql = oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray[0];
}

function get_particular_appointment_details($appointmentId, $companyId) {
    global $conn;
    $dataArray = array();
    $query = "SELECT CRMCOMPANYPREAPPOINTMENT.* FROM CRMCOMPANYPREAPPOINTMENT WHERE  COMPANYID = " . @$companyId . " AND APPOINTMENTID=" . @$appointmentId;
    $sql = oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray[0];
}

function uploadReportStatusPost($companyId = '', $type) {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        if ($type == "GroupUser") {
            $query = "SELECT CRMCOMPANYPOSTAPPOINTMENT.*,CRMCOMPANY.COMPANYNAME FROM CRMCOMPANYPOSTAPPOINTMENT LEFT JOIN CRMCOMPANY ON CRMCOMPANYPOSTAPPOINTMENT.COMPANYID=CRMCOMPANY.COMPANYID  WHERE CRMCOMPANYPOSTAPPOINTMENT.STATUS='DONE' AND  CRMCOMPANYPOSTAPPOINTMENT.COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE  CRMCOMPANY.GROUPCOMPANYID=" . @$companyId . ")";
        } else {
            $query = "SELECT CRMCOMPANYPOSTAPPOINTMENT.*,CRMCOMPANY.COMPANYNAME FROM CRMCOMPANYPOSTAPPOINTMENT LEFT JOIN CRMCOMPANY ON CRMCOMPANYPOSTAPPOINTMENT.COMPANYID=CRMCOMPANY.COMPANYID WHERE  CRMCOMPANYPOSTAPPOINTMENT.STATUS='DONE' AND CRMCOMPANYPOSTAPPOINTMENT.COMPANYID = " . @$companyId;
        }
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function uploadReportStatus($companyId = '', $type) {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        if ($type == "GroupUser") {
            $query = "SELECT CRMCOMPANYPREAPPOINTMENT.*,CRMCOMPANY.COMPANYNAME FROM CRMCOMPANYPREAPPOINTMENT LEFT JOIN CRMCOMPANY ON CRMCOMPANYPREAPPOINTMENT.COMPANYID=CRMCOMPANY.COMPANYID  WHERE CRMCOMPANYPREAPPOINTMENT.STATUS='DONE' AND  CRMCOMPANYPREAPPOINTMENT.COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE  CRMCOMPANY.GROUPCOMPANYID=" . @$companyId . ")";
        } else {
            $query = "SELECT CRMCOMPANYPREAPPOINTMENT.*,CRMCOMPANY.COMPANYNAME FROM CRMCOMPANYPREAPPOINTMENT LEFT JOIN CRMCOMPANY ON CRMCOMPANYPREAPPOINTMENT.COMPANYID=CRMCOMPANY.COMPANYID WHERE  CRMCOMPANYPREAPPOINTMENT.STATUS='DONE' AND CRMCOMPANYPREAPPOINTMENT.COMPANYID = " . @$companyId;
        }
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function appointmentPostList($companyId = '', $type, $appointmentType = 'PENDING') {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        $strSql = "";
        if ($appointmentType == "FIXED") {
            $strSql = " AND CRMCOMPANYPOSTAPPOINTMENT.STATUS='FIXED'";
        }
        if ($appointmentType == "DONE") {
            $strSql = " AND CRMCOMPANYPOSTAPPOINTMENT.STATUS='DONE'";
        }
        if ($appointmentType == "REPORT") {
            $strSql = " AND CRMCOMPANYPOSTAPPOINTMENT.REPORTFILE!=' '";
        }
        if ($type == "GroupUser") {
            $query = "SELECT CRMCOMPANYPOSTAPPOINTMENT.*,CRMCOMPANY.COMPANYNAME FROM CRMCOMPANYPOSTAPPOINTMENT  LEFT JOIN CRMCOMPANY ON CRMCOMPANYPOSTAPPOINTMENT.COMPANYID=CRMCOMPANY.COMPANYID WHERE   CRMCOMPANYPOSTAPPOINTMENT.COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE  GROUPCOMPANYID=" . @$companyId . ") " . @$strSql . "  order by APPOINTMENTID DESC";
        } else {
            $query = "SELECT CRMCOMPANYPOSTAPPOINTMENT.*,CRMCOMPANY.COMPANYNAME FROM CRMCOMPANYPOSTAPPOINTMENT  LEFT JOIN CRMCOMPANY ON CRMCOMPANYPOSTAPPOINTMENT.COMPANYID=CRMCOMPANY.COMPANYID WHERE  CRMCOMPANYPOSTAPPOINTMENT.COMPANYID = " . @$companyId . "   " . @$strSql . "  order by APPOINTMENTID DESC";
        }
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function appointmentList($companyId = '', $type, $appointmentType = '') {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        $strSql = "";
        if ($appointmentType == "FIXED") {
            $strSql = " AND CRMCOMPANYPREAPPOINTMENT.STATUS='FIXED'";
        }
        if ($appointmentType == "DONE") {
            $strSql = " AND CRMCOMPANYPREAPPOINTMENT.STATUS='DONE'";
        }
        if ($appointmentType == "REPORT") {
            $strSql = " AND CRMCOMPANYPREAPPOINTMENT.REPORTFILE!=' '";
        }
        if ($type == "GroupUser") {
            $query = "SELECT CRMCOMPANYPREAPPOINTMENT.*,CRMCOMPANY.COMPANYNAME FROM CRMCOMPANYPREAPPOINTMENT  LEFT JOIN CRMCOMPANY ON CRMCOMPANYPREAPPOINTMENT.COMPANYID=CRMCOMPANY.COMPANYID WHERE   CRMCOMPANYPREAPPOINTMENT.COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE  GROUPCOMPANYID=" . @$companyId . ") " . @$strSql . "  order by APPOINTMENTID DESC";
        } else {
            $query = "SELECT CRMCOMPANYPREAPPOINTMENT.*,CRMCOMPANY.COMPANYNAME FROM CRMCOMPANYPREAPPOINTMENT  LEFT JOIN CRMCOMPANY ON CRMCOMPANYPREAPPOINTMENT.COMPANYID=CRMCOMPANY.COMPANYID WHERE  CRMCOMPANYPREAPPOINTMENT.COMPANYID = " . @$companyId . "   " . @$strSql . "  order by APPOINTMENTID DESC";
        }
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function getNewAppointment($companyId = '', $type) {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        if ($type == "GroupUser") {
            $query = "SELECT count(*)  FROM CRMCOMPANYPREAPPOINTMENT WHERE STATUS='FIXED' AND  COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE  GROUPCOMPANYID=" . @$companyId . ")";
        } else {
            $query = "SELECT count(*) FROM CRMCOMPANYPREAPPOINTMENT WHERE  STATUS='FIXED' AND COMPANYID = " . @$companyId;
        }
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $row = oci_fetch_row($sql);
    }
    return @$row[0];
}

function getNewAppointmentPost($companyId = '', $type) {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        if ($type == "GroupUser") {
            $query = "SELECT count(*)  FROM CRMCOMPANYPOSTAPPOINTMENT WHERE STATUS='FIXED' AND  COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE  GROUPCOMPANYID=" . @$companyId . ")";
        } else {
            $query = "SELECT count(*) FROM CRMCOMPANYPOSTAPPOINTMENT WHERE  STATUS='FIXED' AND COMPANYID = " . @$companyId;
        }
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $row = oci_fetch_row($sql);
    }
    return @$row[0];
}

function getNewAppointmentReport($companyId = '', $type) {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        if ($type == "GroupUser") {
            $query = "SELECT count(*) FROM CRMCOMPANYPREAPPOINTMENT WHERE REPORTFILE!=' ' AND  COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE  GROUPCOMPANYID=" . @$companyId . ")";
        } else {
            $query = "SELECT count(*) FROM CRMCOMPANYPREAPPOINTMENT WHERE  REPORTFILE!=' ' AND COMPANYID = " . @$companyId;
        }
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $row = oci_fetch_row($sql);
    }
    return @$row[0];
}

function getNewAppointmentReportPost($companyId = '', $type) {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        if ($type == "GroupUser") {
            $query = "SELECT count(*) FROM CRMCOMPANYPOSTAPPOINTMENT WHERE REPORTFILE!=' ' AND  COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE  GROUPCOMPANYID=" . @$companyId . ")";
        } else {
            $query = "SELECT count(*) FROM CRMCOMPANYPOSTAPPOINTMENT WHERE  REPORTFILE!=' ' AND COMPANYID = " . @$companyId;
        }
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $row = oci_fetch_row($sql);
    }
    return @$row[0];
}

function getNewAppointmentVisit($companyId = '', $type) {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        if ($type == "GroupUser") {
            $query = "SELECT count(*) FROM CRMCOMPANYPREAPPOINTMENT WHERE STATUS='DONE' AND  COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE  GROUPCOMPANYID=" . @$companyId . ")";
        } else {
            $query = "SELECT count(*) FROM CRMCOMPANYPREAPPOINTMENT WHERE  STATUS='DONE' AND COMPANYID = " . @$companyId;
        }
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $row = oci_fetch_row($sql);
    }
    return @$row[0];
}

function getNewAppointmentVisitPost($companyId = '', $type) {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        if ($type == "GroupUser") {
            $query = "SELECT count(*) FROM CRMCOMPANYPOSTAPPOINTMENT WHERE STATUS='DONE' AND  COMPANYID IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE  GROUPCOMPANYID=" . @$companyId . ")";
        } else {
            $query = "SELECT count(*) FROM CRMCOMPANYPOSTAPPOINTMENT WHERE  STATUS='DONE' AND COMPANYID = " . @$companyId;
        }
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $row = oci_fetch_row($sql);
    }
    return @$row[0];
}

function fetchPolicyList($companyId = '') {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        $query = 'SELECT * FROM "CRMPOLICY" WHERE COMPANYID=' . $companyId;
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function fetchCompanyDetails($id) {
    global $conn;
    $dataArray = array();
    $query = 'SELECT * FROM "CRMCOMPANY" WHERE COMPANYID=' . $id;
    $sql = oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function fetchGroupCompanyDetails($id) {
    global $conn;
    $dataArray = array();
    $query = 'SELECT * FROM "CRMGROUPCOMPANY" WHERE GROUPCOMPANYID=' . $id;
    $sql = oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function fetchCompanyData($groupCompanyId) {
    global $conn;
    $dataArray = array();
    $sql = oci_parse($conn, 'SELECT * FROM "CRMCOMPANY" WHERE GROUPCOMPANYID=' . $groupCompanyId . ' order by COMPANYNAME');
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function fetchCompanyDataAdmin($groupCompanyId) {
    global $conn;
    $dataArray = array();
    if ($groupCompanyId > 0) {
        $sql = oci_parse($conn, 'SELECT * FROM "CRMCOMPANY" WHERE GROUPCOMPANYID=' . $groupCompanyId . ' order by COMPANYNAME');
    } else {
        $sql = oci_parse($conn, "SELECT * FROM CRMCOMPANY WHERE GROUPCOMPANYID=0  order by COMPANYNAME");
    }
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function fetchGroupCompanyData() {
    global $conn;
    $dataArray = array();
    $sql = oci_parse($conn, 'SELECT * FROM "CRMGROUPCOMPANY"  order by GROUPCOMPANYNAME');
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function totalGroupCompanyData() {
    global $conn;
    $dataArray = array();
    $sql = oci_parse($conn, 'SELECT count(*) FROM "CRMGROUPCOMPANY"');
    // Execute Query
    oci_execute($sql);
    $i = 0;
    $row = oci_fetch_row($sql);
    return $row[0];
}

function totalCompanyData() {
    global $conn;
    $dataArray = array();
    $sql = oci_parse($conn, 'SELECT count(*) FROM "CRMCOMPANY"');
    // Execute Query
    oci_execute($sql);
    $i = 0;
    $row = oci_fetch_row($sql);
    return $row[0];
}

function totalPolicies() {
    global $conn;
    $dataArray = array();
    $sql = oci_parse($conn, 'SELECT count(*) FROM "CRMPOLICY"');
    // Execute Query
    oci_execute($sql);
    $i = 0;
    $row = oci_fetch_row($sql);
    return $row[0];
}

function list_display_group_company_list($groupCompanyId) {
    global $conn;
    $sql = oci_parse($conn, "SELECT COMPANYID,COMPANYNAME FROM CRMCOMPANY WHERE GROUPCOMPANYID=" . $groupCompanyId . " order by COMPANYNAME");
    // Execute Query
    oci_execute($sql);
    echo "<select id=\"COMPANYID\" style=\"width:160px\" class=\"vAlign\" name=\"COMPANYID\" >";
    while ($row = oci_fetch_row($sql)) {
        if ($row[0] == $field_value) {
            echo "<option value=\"" . $row[0] . "\" SELECTED>" . stripslashes(ucwords(strtolower($row[1]))) . "</option>";
        } else {
            echo "<option value=\"" . $row[0] . "\">" . stripslashes(ucwords(strtolower($row[1]))) . "</option>";
        }
    }
    echo "</select>";
}

function list_display_company_list($companyId, $field_value = '') {
    global $conn;
    $sql = oci_parse($conn, "SELECT COMPANYID,COMPANYNAME FROM CRMCOMPANY WHERE COMPANYID=" . $companyId . " order by COMPANYNAME");
    // Execute Query
    oci_execute($sql);
    echo "<select id=\"COMPANYID\" style=\"width:160px\" class=\"vAlign\"   name=\"COMPANYID\" >";
    while ($row = oci_fetch_row($sql)) {
        if ($row[0] == $field_value) {
            echo "<option value=\"" . $row[0] . "\" SELECTED>" . stripslashes(ucwords(strtolower($row[1]))) . "</option>";
        } else {
            echo "<option value=\"" . $row[0] . "\">" . stripslashes(ucwords(strtolower($row[1]))) . "</option>";
        }
    }
    echo "</select>";
}

function touploadItem($filedName, $target_path, $filename) {
    $target_path = $target_path . $filename;
    if (move_uploaded_file($_FILES[$filedName]['tmp_name'], $target_path)) {
        chmod($target_path, 0777);
    }
    return $filename;
}

function toHAndleSpace($string) {
    $string = @ereg_replace("[ \t\n\r]+", " ", $string);
    $text = @str_replace(" ", "_", $string);
    return $text;
}

function fetchListCondDoc($companyId = '', $type) {
    global $conn;
    $dataArray = array();
    if ($companyId > 0) {
        if ($type == "GroupUser") {
            $query = "select CRMDOCUMENTMAPPING.DOCUMENTNAME,CRMDOCUMENTMAPPING.DOCUMENTFILE,CRMDOCUMENTMAPPING.EFFECTIVEFROM from CRMDOCUMENTMAPPING where CRMDOCUMENTMAPPING.EFFECTIVEFROM <= sysdate and CRMDOCUMENTMAPPING.ACCESSLAVELHR='YES' and ( CRMDOCUMENTMAPPING.COMPANYID=IN (SELECT distinct COMPANYID from CRMCOMPANY WHERE CRMCOMPANY.GROUPCOMPANYID=$companyId) OR CRMDOCUMENTMAPPING.COMPANYID=0  ) and STATUS='ACTIVE' ORDER BY CRMDOCUMENTMAPPING.EFFECTIVEFROM DESC";
        } else {
            $query = "select CRMDOCUMENTMAPPING.DOCUMENTNAME,CRMDOCUMENTMAPPING.DOCUMENTFILE,CRMDOCUMENTMAPPING.EFFECTIVEFROM from CRMDOCUMENTMAPPING where CRMDOCUMENTMAPPING.EFFECTIVEFROM <= sysdate and CRMDOCUMENTMAPPING.ACCESSLAVELHR='YES' and ( CRMDOCUMENTMAPPING.COMPANYID=$companyId OR CRMDOCUMENTMAPPING.COMPANYID=0  ) and STATUS='ACTIVE' ORDER BY CRMDOCUMENTMAPPING.EFFECTIVEFROM DESC";
        }
        $sql = @oci_parse($conn, $query);
        // Execute Query
        @oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function fetchListByCond($table, $cond) {
    global $conn;
    $dataArray = array();
    /* $query="select * from CRMDOCUMENTMASTER where DOCUMENTID in (select DOCUMENTID from CRMDOCUMENTMAPPING where ACCESSLAVELHR='YES') and EFFECTIVEDATE < sysdate"; */
    $query = "select * from $table $cond";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function masteragegroup($AGEGROUPID) {
    if ($AGEGROUPID != '') {
        $sqlstr = ' AND AGEGROUPID = $AGEGROUPID';
    }
    global $conn;
    $dataArray = array();
    $query = 'SELECT * FROM "CRMAGEGROUP" WHERE STATUS=ACTIVE "' . $sqlstr . '" ';
    $sql = oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function masterlocation($LOCATIONID) {
    if ($LOCATIONID != '') {
        $sqlstr = ' AND LOCATIONID = $LOCATIONID';
    }
    global $conn;
    $dataArray = array();
    $query = 'SELECT * FROM "CRMLOCATIONMASTER" WHERE STATUS=ACTIVE "' . $sqlstr . '" ';
    $sql = oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function masterfamilystructure($id) {
    if ($id != '') {
        $sqlstr = ' AND ID = $id';
    }
    global $conn;
    $dataArray = array();
    $query = 'SELECT * FROM "CRMOAQFAMILYSTRUCTURE" WHERE STATUS=ACTIVE "' . $sqlstr . '" ';
    $sql = oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function masterpolicytype($id) {
    if ($id != '') {
        $sqlstr = ' AND ID = $id';
    }
    global $conn;
    $dataArray = array();
    $query = 'SELECT * FROM "CRMOAQPOLICYTYPE" WHERE STATUS=ACTIVE "' . $sqlstr . '" ';
    $sql = oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function mastergroupsize($id) {
    if ($id != '') {
        $sqlstr = ' AND ID = $id';
    }
    global $conn;
    $dataArray = array();
    $query = 'SELECT * FROM "CRMOAQGROUPSIZE" WHERE STATUS=ACTIVE "' . $sqlstr . '" ';
    $sql = oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function masterroomtype($id) {
    if ($id != '') {
        $sqlstr = ' AND ID = $id';
    }
    global $conn;
    $dataArray = array();
    $query = 'SELECT * FROM "CRMOAQROOMTYPE" WHERE STATUS=ACTIVE "' . $sqlstr . '" ';
    $sql = oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function mastersource($id) {
    if ($id != '') {
        $sqlstr = ' AND ID = $id';
    }
    global $conn;
    $dataArray = array();
    $query = 'SELECT * FROM "CRMOAQSOURCE" WHERE STATUS=ACTIVE "' . $sqlstr . '" ';
    $sql = oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function masterdiesease($id) {
    if ($id != '') {
        $sqlstr = ' AND ID = $id';
    }
    global $conn;
    $dataArray = array();
    $query = 'SELECT * FROM "CRMOAQDISEASE" WHERE STATUS=ACTIVE "' . $sqlstr . '" ';
    $sql = oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function masterindustry($id) {
    if ($id != '') {
        $sqlstr = ' AND ID = $id';
    }
    global $conn;
    $dataArray = array();
    $query = 'SELECT * FROM "CRMOAQINDUSTRY" WHERE STATUS=ACTIVE "' . $sqlstr . '" ';
    $sql = oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function fetchListCond($table, $cond) {
    global $conn;
    $dataArray = array();
    $query = "SELECT * FROM $table $cond";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function fetchListCondPagination($query) {
    global $conn;
    $dataArray = array();
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function getMappingValues($tableName, $con1, $con1val, $con2, $con2val, $con3, $con3val) {
    global $conn;
    $dataArray = array();
    $sqlstr = '';
    if ($con1 != '') {
        $sqlstr.=" AND " . $con1 . " ='" . $con1val . "' ";
    }
    if ($con2 != '') {
        $sqlstr.=" AND " . $con2 . " ='" . $con2val . "' ";
    }
    if ($con3 != '') {
        $sqlstr.=" AND " . $con3 . " ='" . $con3val . "' ";
    }

    $query = "SELECT * FROM " . $tableName . " where ID > 0 " . $sqlstr . " ";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function fetchListCondsWithColumn($column, $table, $cond) {
    global $conn;
    $dataArray = array();
    $query = "SELECT $column FROM $table $cond";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc(@$sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = @$value;
        }
        $i++;
    }
    return $dataArray;
}

function fetchListCondsWithColumn2($column, $table, $cond) {
    global $conn;
    $dataArray = array();
    $query = "SELECT $column FROM $table $cond";
//              echo $query; exit;
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc(@$sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = @$value;
        }
        $i++;
    }
    return $dataArray;
}

function getGroupSizeId($number) {
    global $conn;
    $dataArray = array();
    $query = "SELECT * FROM CRMOAQGROUPSIZE WHERE MINIMUMNUMBER<= " . $number . " AND MAXIMUMNUMBER>= " . $number . " ";
    $sql = oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function quoteDetails($id) {
    if ($id != '') {
        $sqlstr = " ID = " . $id . " ";
    }
    global $conn;
    $dataArray = array();
    $query = "SELECT * FROM CRMOAQQUOTE WHERE " . $sqlstr . " ";
    $sql = oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function gpaQuoteDetails($id) {
    if ($id != '') {
        $sqlstr = " ID = " . $id . " ";
    }
    global $conn;
    $dataArray = array();
    $query = "SELECT * FROM GPAQUOTE WHERE " . $sqlstr . " ";
    $sql = oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function fetchListById($tblname, $column1, $value1) {
    global $conn;
    $dataArray = array();
    $query = "SELECT * FROM $tblname where $column1='$value1'";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = @oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function getServiceTax() {
    global $conn;
    $dataArray = array();
    $query = "SELECT * FROM CRMOAQSERVICETAX ORDER BY ID DESC";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function getDistinctValue($column, $table, $cond) {
    global $conn;
    $dataArray = array();
    $query = "SELECT SIID,SI FROM CRMSI WHERE SIID IN (SELECT DISTINCT $column FROM $table $cond) ORDER BY SI ASC";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function getRoomSIMapVal($siId) {
    global $conn;
    $dataArray = array();
    $query = "SELECT * FROM CRMOAQROOMTYPE WHERE ID IN (SELECT DISTINCT ROOMTYPEID FROM CRMOAQROOMSIMAPPING WHERE SIID = '$siId') ";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function getRoomTypeDetails($roomTypeId) {
    global $conn;
    $dataArray = array();
    $query = "SELECT * FROM CRMOAQROOMTYPE WHERE ID = '$roomTypeId' ";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function getICULimitDetails($roomTypeId) {
    global $conn;
    $dataArray = array();
    $query = "SELECT * FROM CRMOAQICULIMIT WHERE ID = '$roomTypeId' ";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function getDistinctSiRoomType($quoteId) {
    global $conn;
    $dataArray = array();
    $query = "SELECT DISTINCT CRMOAQQUOTESUMINSURED.SIID,CRMOAQQUOTESUMINSURED.ROOMTYPEID,CRMOAQQUOTESUMINSURED.ICULIMITID,CRMSI.SI  FROM CRMOAQQUOTESUMINSURED LEFT JOIN CRMSI ON CRMSI.SIID = CRMOAQQUOTESUMINSURED.SIID WHERE CRMOAQQUOTESUMINSURED.QUOTEID =" . $quoteId . " ORDER BY CRMSI.SI ASC";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function maximumAgeBand($quoteId) {
    global $conn;
    $dataArray = array();
    $query = "SELECT MAXVALUE FROM CRMOAQAGEGROUP WHERE AGEGROUPID IN (SELECT DISTINCT AGEGROUPID  FROM CRMOAQQUOTESUMINSURED WHERE QUOTEID = '" . $quoteId . "') ORDER BY AGEGROUPID  DESC";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = @oci_fetch_assoc(@$sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function distinctPrePost($prepostId) {
    global $conn;
    $dataArray = array();
    $query = "SELECT PREPOST FROM CRMOAQPREPOSTHOSPITALIZATION WHERE ID ='" . $prepostId . "' ORDER BY ID DESC";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function distinctMaternity($maternityId) {
    global $conn;
    $dataArray = array();
    $query = "SELECT MAX(CAESERIAN) as CSC FROM CRMOAQMATERNITY WHERE ID = '" . @$maternityId . "' ORDER BY ID DESC";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function distincNormaltMaternity($maternityId) {
    global $conn;
    $dataArray = array();
    $query = "SELECT MAX(NORMAL) as NORMAL FROM CRMOAQNORMALMATERNITY WHERE ID = '" . @$maternityId . "' ORDER BY ID DESC";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function SendMail($email, $from, $subject, $message) {
    $from = 'info@religarehealthinsurance.com';
    $to = $email;
    $subject = $subject;
    $headers = 'From: ' . $from . "\r\n" . 'MIME-Version: 1.0' . "\r\n" . 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    if (mail($to, $subject, $message, $headers)) {
        $to = addslashes($to);
        $subject = addslashes($subject);
        $message = addslashes($message);
        $sql = "INSERT INTO CRMOAQSENTMAIL (MAILID,MAILTO,SUBJECT,MESSAGE,MAILDATE,TYPE) values(CRMOAQSENTMAIL_SEQ.nextval,'" . @$to . "','" . @$subject . "','" . @$message . "','" . @$entryTime . "','" . @$type . "') ";
        $stdid = @oci_parse($conn, $sql);
        $r = @oci_execute($stdid);
        return "Sent SuccessFully";
    }
}

function getMaxSize($tableName, $column, $primaryId, $condcol1, $conval1, $condcol2, $conval2) {
    global $conn;
    $dataArray = array();
    $sqlstr = '';
    if ($condcol1 != '') {
        $sqlstr.=" AND $condcol1 = '" . $conval1 . "' ";
    }
    if ($condcol2 != '') {
        $sqlstr.=" AND $condcol2 = '" . $conval2 . "' ";
    }
    $query = "SELECT MAX($column) as MAXNUMBER FROM $tableName WHERE $primaryId > 0 " . $sqlstr;
    $sql = @oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $row = oci_fetch_assoc($sql);
    return $row['MAXNUMBER'];
}

function oaqSI($sIAccessCond = '') {
    global $conn;
    $dataArray = array();
    if (@$sIAccessCond) {
        $queryres = fetchListCondsWithColumn('DISTINCT SIID', 'CRMOAQSI', "WHERE 1=1 $sIAccessCond");
        $i = 0;
        foreach ($queryres as $key => $siidval) {
            $siidArr[] = $siidval['SIID'];
            $i++;
        }
        $SIIDs = implode(',', $siidArr);

        $condition = $SIIDs;
    } else {
        $condition = "SELECT DISTINCT SIID FROM CRMOAQSI WHERE STATUS = 'ACTIVE'";
    }
    $query = "SELECT SIID, SI, VISIBLEALL FROM CRMSI WHERE SIID IN ($condition) ORDER BY TO_NUMBER(SI) ASC";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function oaqAgeGroup() {
    global $conn;
    $dataArray = array();
    $query = "SELECT AGEGROUPID, AGEGROUP, VISIBLEALL FROM CRMAGEGROUP WHERE AGEGROUPID IN (SELECT DISTINCT AGEGROUPID FROM CRMOAQAGEGROUP WHERE STATUS = 'ACTIVE') ORDER BY AGEGROUPID ASC";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function timeAgo($timeStamp) {
    $second = 1;
    $minute = $second * 60;
    $hour = $minute * 60;
    $day = $hour * 24;
    $week = $day * 7;
    $month = $day * 30; // We dont do exact days per month of 30/31/28/29 etc. All months are 30 days.
    $year = $month * 12;
    $str = '';
    $vals = array(0 => $year, 1 => $month, 2 => $week, 3 => $day, 4 => $hour, 5 => $minute, 6 => $second);
    $labels = array(0 => 'year', 1 => 'month', 2 => 'week', 3 => 'day', 4 => 'hour', 5 => 'minute', 6 => 'second');

    $now = time(); //Store current timestamp of the server.
    $ago = $now - $timeStamp; // How much time has passed in seconds since $timeStamp.
    //echo "difference: $ago <br>";
    for ($i = 0; $i < count($vals); $i++) {
        if ($ago > $vals[$i]) {
            $remainder = round($ago / $vals[$i]); // Divide each type of label value from the difference to find out values.
        } else {
            $remainder = 0;
        }
        //echo "$i >>>>> ".$labels[$i]."  $remainder ::: ".$vals[$i]." :::: $ago <br>";
        if ($remainder > 1) {
            $str .= ' ' . $remainder . ' ' . $labels[$i] . 's'; // 3 years"S"
            $ago = $ago - (round($vals[$i] * $remainder) );
            $str .= ' ago';
            return $str;
        } else if ($remainder == 1) {
            $str .= ' ' . $remainder . ' ' . $labels[$i]; // 1 year (no 's' here
            $ago = $ago - (round($vals[$i] * $remainder) );
            $str .= ' ago';
            return $str;
        }
    }
    ;
}

function getServiceIndustry($industryId) {
    global $conn;
    $dataArray = array();
    $query = "SELECT COUNT(*) as TOTALNUMBER FROM CRMOAQINDUSTRY WHERE ID = " . $industryId . " AND INDUSTRYNAME LIKE '%Service%' ";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $row = oci_fetch_assoc($sql);
    return $row['TOTALNUMBER'];
}

function getDistinctDisease($diseaseId) {
    global $conn;
    $dataArray = array();
    $query = "SELECT * FROM CRMOAQDISEASE WHERE ID = '" . @$diseaseId . "' ";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function countQuoteCopy($copyId) {
    global $conn;
    $dataArray = array();
    $query = "SELECT COUNT(*) as TOTALNUMBER FROM CRMOAQQUOTE WHERE COPYID = " . @$copyId . " ";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $row = oci_fetch_assoc($sql);
    return $row['TOTALNUMBER'];
}

function countQuoteCopyNew($copyId) {
    global $conn;
    $dataArray = array();
    $query = "SELECT COUNT(*) as TOTALNUMBER FROM CRMOAQQUOTE WHERE REFRENCENUMBER LIKE '" . @$copyId . "%'";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $row = oci_fetch_assoc($sql);
    return $row['TOTALNUMBER'];
}

function countGpaQuoteCopy($copyId) {
    global $conn;
    $dataArray = array();
    $query = "SELECT COUNT(*) as TOTALNUMBER FROM GPAQUOTE WHERE REFRENCENUMBER LIKE '" . @$copyId . "%'";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    oci_execute($sql);
    $row = oci_fetch_assoc($sql);
    return $row['TOTALNUMBER'];
}

function fetchcolumnListCond($col, $table, $cond) {
    global $conn;
    $dataArray = array();
    $query = "SELECT $col FROM $table $cond";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function countMoreCoverage($quoteId) {
    global $conn;
    $dataArray = array();
    $query = "SELECT GPACOVERAGE.COVERAGEID FROM GPAQUOTECOVERAGE, GPACOVERAGE WHERE GPACOVERAGE.STATUS = 'ACTIVE' AND GPACOVERAGE.PRIORITY != 'yes' AND GPAQUOTECOVERAGE.QUOTEID = '" . @$quoteId . "' AND GPACOVERAGE.COVERAGEID = GPAQUOTECOVERAGE.COVERAGEID ";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function fetchListCondUnion() {
    global $conn;
    $dataArray = array();

    $query = "SELECT ID,QUOTEREQUESTNO,CORPORATENAME,POLICYTYPE,STATUS FROM underwriterquote UNION 
                 SELECT ID,QUOTEREQUESTNO,CORPORATENAME,POLICYTYPE,STATUS FROM GPAQUOTE UNION 
                 SELECT ID,QUOTEREQUESTNO,CORPORATENAME,POLICYTYPE,STATUS FROM CRMOAQQUOTE";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}

//function to sanitize input password
function sanitize_password($input_data) {
    $searchArr = array("document", "write", "alert", "%", "$", ";", "+", "|", "#", "<", ">", ")", "(", "'", "\'", ",");
    $input_data = str_replace("script", "", $input_data);
    $input_data = str_replace("iframe", "", $input_data);
    $input_data = str_replace($searchArr, "", $input_data);
    $input_data = trim($input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}

function corpDetails($tTable, $id) {
    global $conn;
    $dataArray = array();
    $query = "SELECT * from $tTable WHERE ID = '" . $id . "' ";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function pr($var) {
    echo '<pre>';
    print_r($var);
    die;
}

function find_foll($id = null, $arr1, $col = "ID") {
    $my_followers = fetchListCondsWithColumn($col, "CRMOAQUSER", " where MANAGERID=" . $id . " AND ID != " . $_SESSION['userId']);
    $aray = create_arr($my_followers, $col);
    $arr1 = array_merge($arr1, $aray);
    $is_depth = chk_depth($aray, $col);
    if ($is_depth) {
        $arr2 = array();
        foreach ($aray as $val) {
            $arr2 = find_foll($val, $arr1, $col);
            if (!empty($arr2)) {
                $arr1 = array_merge($arr1, $arr2);
                $arr1 = array_unique($arr1);
            }
        }
        return $arr1;
    } else {
        return $arr1;
    }
}

function chk_depth($arr, $col = "ID") {
    if (!empty($arr)) {
        $arr_string = implode(",", array_filter($arr));
        $is_level_down = fetchListCondsWithColumn($col, "CRMOAQUSER", " where MANAGERID IN (" . $arr_string . ") AND ID != " . $_SESSION['userId']);
        if (!empty($is_level_down)) {
            return true;
        } else {
            return false;
        }
    }
    return false;
}

function create_arr($my_followers, $col = "ID") {
    $arr = array();
    if (!empty($my_followers)) {
        foreach ($my_followers as $follow_id) {
            $arr[] = $follow_id[$col];
        }
    }
    return $arr;
}

function scriptname() {
    $script_name = substr(strrchr($_SERVER['SCRIPT_NAME'], '/'), 1);
    return $script_name;
}

function getAssigneeByStatus($status, $quoteId, $createdBy) {
    if ($status == STATUS_PENDING_UWAPPROVAL || $status == STATUS_PENDING_RATEAPPROVAL || $status == STATUS_UW_APPROVAL || ($status == STATUS_PUTONHOLD && $_SESSION['type'] == SOURCE_UNDERWRITER)) {
        $sourceArr = fetchListCondsWithColumn("UNDERWRITERNAME", 'UNDERWRITERQUOTE', " where ID = '$quoteId'");
        return 'Underwriter (' . $sourceArr[0]['UNDERWRITERNAME'] . ')';
    } else if ($status == STATUS_PENDING_RIAPPROVAL) {
        return SOURCE_RI . ' Team';
    } else if ($status == STATUS_APPROVED || $status == STATUS_CONVERTED || $status == STATUS_POLICY_INFOREQUIRED) {
        return SOURCE_RM;
    } else if ($status == STATUS_PENDING_FORPOLICYISSUANCE || $status == STATUS_POLICY_PUTONHOLD) {
        return SOURCE_OPERATIONS . ' Team';
    } else {
        return ucfirst($createdBy);
    }
}

function GetTemplateContentByTemplateName($templateName) {
    global $conn;
    $dataArray = array();
    $query = "SELECT TEMPLATEID,SUBJECT,CONTENT,LARGECONTENT,FROMEMAIL,FROMNAME,TEMPLATENAME from QTPEMAILTEMPLATE where TEMPLATENAME='" . $templateName . "' ";
    $sql = @oci_parse($conn, $query);
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function SendMailToForgotPassword($email, $name, $updatePassword) {
    global $conn;
    $to = $email;
    $Mailtemplates = GetTemplateContentByTemplateName('QTP_USER_FORGOT');
    $subject = stripslashes($Mailtemplates[0]['SUBJECT']);
    $message = $Mailtemplates[0]['LARGECONTENT'] ? stripslashes($Mailtemplates[0]['LARGECONTENT']->load()) : stripslashes($Mailtemplates[0]['CONTENT']);
    $message = str_replace("##USER", @$name, $message);
    $message = str_replace("##updatePassword", @$updatePassword, $message);
    $headers = 'From:' . stripslashes($Mailtemplates[0]['FROMNAME']) . ' <' . stripslashes($Mailtemplates[0]['FROMEMAIL']) . "> \r\n" .
            'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    @mail($to, $subject, $message, $headers);
}

/*
 * Email Template for Quote Creation
 * Mail RM and RM email id inserted when quote created
 */

function SendMailToRMQuoteGenerated($rmemail, $selectedUW, $REFRENCENUMBER) {
    global $conn;
    $rmuserArr = fetchListCondsWithColumn("NAME", 'CRMOAQUSER', " where EMAILID = '" . $_SESSION["emailId"] . "' ");
    $toRM = $_SESSION["emailId"]; // Logged in RM
    $name = ucfirst($rmuserArr[0]['NAME']);
    $Mailtemplates = GetTemplateContentByTemplateName('NEW_QUOTE_GENERATED');
    $subject = stripslashes($Mailtemplates[0]['SUBJECT']);
    $message = $Mailtemplates[0]['LARGECONTENT'] ? stripslashes($Mailtemplates[0]['LARGECONTENT']->load()) : stripslashes($Mailtemplates[0]['CONTENT']);
    $message = str_replace("##USER", $name, $message);
    $message = str_replace("##QUOTENO", $REFRENCENUMBER, $message);
    $headers = 'From:' . stripslashes($Mailtemplates[0]['FROMNAME']) . ' <' . stripslashes($Mailtemplates[0]['FROMEMAIL']) . "> \r\n" .
            'Cc:' . stripslashes($rmemail) . "\r\n" .
            'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $mail = mail($toRM, $subject, $message, $headers);
}

function fetchListCondsWithUWJOIN($cond) {
    global $conn;
    $dataArray = array();
    $query = "SELECT UNDERWRITERQUOTE.ID,UNDERWRITERQUOTE.RMEMAIL,UNDERWRITERQUOTE.UNDERWRITERNAME,UNDERWRITERQUOTE.UNDERWRITERID,UNDERWRITERQUOTE.REFRENCENUMBER,UNDERWRITERQUOTE.CORPORATENAME,UNDERWRITERQUOTE.PRODUCT,UNDERWRITERQUOTE.NOOFLIVES,UNDERWRITERQUOTE.REQUIREDPREMIUM,CRMOAQUSER.ID,CRMOAQUSER.EMAILID,CRMOAQUSER.NAME FROM UNDERWRITERQUOTE LEFT JOIN CRMOAQUSER ON UNDERWRITERQUOTE.UNDERWRITERID=CRMOAQUSER.ID $cond";

    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

/*
 * Email Template for Client Id Creation by RM
 * Mail to UW
 * Mail cc RM and RM email id inserted when quote created
 */

function SendMailToRMAndUWForClientIdCreation($REFRENCENUMBER) {
    global $conn;
    $sourceArr = fetchListCondsWithUWJOIN("WHERE UNDERWRITERQUOTE.REFRENCENUMBER = '$REFRENCENUMBER'");
    $toRMEmail = $sourceArr[0]['RMEMAIL']; //RM Email id (Entered in quote creation step)
    $toUW = $sourceArr[0]['EMAILID']; // Under Writer Email
    $toRM = $_SESSION["emailId"]; // Logged in RM
    $cc = $toRM . ',' . $toRMEmail;
    $name = ucfirst($sourceArr[0]['NAME']); // uw name
    $corpName = ucfirst($sourceArr[0]['CORPORATENAME']);
    $panCard = $sourceArr[0]['PANCARD'];

    $Mailtemplates = GetTemplateContentByTemplateName('NEW_CLIENT_ID_CREATED');
    $subject = stripslashes($Mailtemplates[0]['SUBJECT']);
    $message = $Mailtemplates[0]['LARGECONTENT'] ? stripslashes($Mailtemplates[0]['LARGECONTENT']->load()) : stripslashes($Mailtemplates[0]['CONTENT']);
    $message = str_replace("##USER", $name, $message);
    $message = str_replace("##QUOTENO", $REFRENCENUMBER, $message);
    $message = str_replace("##CLIENTID", '', $message);
    $message = str_replace("##CORPORATENAME", $corpName, $message);
    $message = str_replace("##PANCARD", $panCard, $message);

    $headers = 'From:' . stripslashes($Mailtemplates[0]['FROMNAME']) . ' <' . stripslashes($Mailtemplates[0]['FROMEMAIL']) . "> \r\n" .
            'Cc:' . stripslashes($cc) . "\r\n" .
            'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $mail = @mail($toUW, $subject, $message, $headers);
}

/*
 * Email Template for Convert quote by RM
 * Mail to UW
 * Mail cc RM and RM email id inserted when quote created
 */

function SendMailForConvertedQuote($QUOTEID) {
    global $conn;
    $sourceArr = fetchListCondsWithUWJOIN("WHERE UNDERWRITERQUOTE.ID = '$QUOTEID'");
    $toRMEmail = $sourceArr[0]['RMEMAIL']; //RM Email id (Entered in quote creation step)
    $toUW = $sourceArr[0]['EMAILID']; // Under Writer Email
    $toRM = $_SESSION["emailId"]; // Logged in RM
    $cc = $toRM . ',' . $toRMEmail;

    $name = ucfirst($sourceArr[0]['NAME']); // uw name
    $refno = $sourceArr[0]['REFRENCENUMBER'];
    $corpName = ucfirst($sourceArr[0]['CORPORATENAME']);
    $product = $sourceArr[0]['PRODUCT'];
    $nooflives = $sourceArr[0]['NOOFLIVES'];
    $reqpremium = moneyFormat($sourceArr[0]['REQUIREDPREMIUM']);

    $Mailtemplates = GetTemplateContentByTemplateName('QUOTE_CONVERTED');
    $subject = stripslashes($Mailtemplates[0]['SUBJECT']);
    $message = $Mailtemplates[0]['LARGECONTENT'] ? stripslashes($Mailtemplates[0]['LARGECONTENT']->load()) : stripslashes($Mailtemplates[0]['CONTENT']);
    $message = str_replace("##USER", $name, $message);
    $message = str_replace("##QUOTENO", $refno, $message);
    $message = str_replace("##CORPORATENAME", $corpName, $message);
    $message = str_replace("##PRODUCT", $product, $message);
    $message = str_replace("##NOOFLIVES", $nooflives, $message);
    $message = str_replace("##PREMIUM", $reqpremium, $message);
    $headers = 'From:' . stripslashes($Mailtemplates[0]['FROMNAME']) . ' <' . stripslashes($Mailtemplates[0]['FROMEMAIL']) . "> \r\n" .
            'Cc:' . stripslashes($cc) . "\r\n" .
            'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $mail = @mail($toUW, $subject, $message, $headers);
}

/*
 * Email Template for reject quote by RM
 * Mail to UW
 * Mail cc RM and RM email id inserted when quote created
 */

function SendMailForRejectedQuote($QUOTEID, $remarks) {
    global $conn;
    $sourceArr = fetchListCondsWithUWJOIN("WHERE UNDERWRITERQUOTE.ID = '$QUOTEID'");
    $toRMEmail = $sourceArr[0]['RMEMAIL']; //RM Email id (Entered in quote creation step)
    $toUW = $sourceArr[0]['EMAILID']; // Under Writer Email
    $toRM = $_SESSION["emailId"]; // Logged in RM
    $name = ucfirst($sourceArr[0]['NAME']);
    $refno = $sourceArr[0]['REFRENCENUMBER'];

    $cc = $toRM . ',' . $toRMEmail;
    $Mailtemplates = GetTemplateContentByTemplateName('QUOTE_REJECTED');
    $subject = stripslashes($Mailtemplates[0]['SUBJECT']);
    $message = $Mailtemplates[0]['LARGECONTENT'] ? stripslashes($Mailtemplates[0]['LARGECONTENT']->load()) : stripslashes($Mailtemplates[0]['CONTENT']);
    $message = str_replace("##USER", $name, $message);
    $message = str_replace("##QUOTENO", $refno, $message);
    $message = str_replace("##REMARKS", $remarks, $message);
    $headers = 'From:' . stripslashes($Mailtemplates[0]['FROMNAME']) . ' <' . stripslashes($Mailtemplates[0]['FROMEMAIL']) . "> \r\n" .
            'Cc:' . stripslashes($cc) . "\r\n" .
            'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $mail = @mail($toUW, $subject, $message, $headers);
}

/*
 * Email Template for move quote to RI by UW
 * Mail to RM
 * Mail CC RM email id (Inserted at the time of quote creation)
 */

function SendMailForRM($quoteId, $remarks) {
    global $conn;
    $rmuserArr = fetchListCondsWithColumn("NAME", 'CRMOAQUSER', " where EMAILID = '" . $_SESSION["emailId"] . "' ");
    $sourceArr = fetchListCondsWithColumn("RMEMAIL,REFRENCENUMBER", 'UNDERWRITERQUOTE', " where ID = '$quoteId'");
    $toRMEmail = $sourceArr[0]['RMEMAIL']; //RM Email id (Entered in quote creation step)
    $toRM = $_SESSION["emailId"]; // Logged in RM
    $name = ucfirst($rmuserArr[0]['NAME']);
    $refno = $sourceArr[0]['REFRENCENUMBER'];

    $Mailtemplates = GetTemplateContentByTemplateName('QUOTE_PENDING_FOR_REVIEW_RM');
    $subject = stripslashes($Mailtemplates[0]['SUBJECT']);
    $message = $Mailtemplates[0]['LARGECONTENT'] ? stripslashes($Mailtemplates[0]['LARGECONTENT']->load()) : stripslashes($Mailtemplates[0]['CONTENT']);
    $message = str_replace("##USER", $name, $message);
    $message = str_replace("##QUOTENO", $refno, $message);
    $message = str_replace("##REMARKS", $remarks, $message);
    $headers = 'From:' . stripslashes($Mailtemplates[0]['FROMNAME']) . ' <' . stripslashes($Mailtemplates[0]['FROMEMAIL']) . "> \r\n" .
            'Cc:' . stripslashes($toRMEmail) . "\r\n" .
            'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $mail = @mail($toRM, $subject, $message, $headers);
}

/*
 * Email Template for move quote to RI by UW
 * Mail to All RI's in system
 */

function SendMailForMoveTORI($quoteId, $remarks) {
    global $conn;
    $sourceIDri = fetchListCondsWithColumn("ID", 'CRMOAQSOURCE', " where SOURCE = '" . SOURCE_RI . "'");
    $sourceArr = fetchListCondsWithColumn("EMAILID", 'CRMOAQUSER', " where TYPE = '" . $sourceIDri[0]['ID'] . "' ");
//    $to = $sourceArr[0]['RMEMAIL']; //RM Email id (Entered in quote creation step)
    $eMailArr = createEmailArr($sourceArr);
    $toRIEMail = implode(',', $eMailArr);
    $Mailtemplates = GetTemplateContentByTemplateName('QUOTE_PENDING_FOR_REVIEW_RI');
    $subject = stripslashes($Mailtemplates[0]['SUBJECT']);
    $message = $Mailtemplates[0]['LARGECONTENT'] ? stripslashes($Mailtemplates[0]['LARGECONTENT']->load()) : stripslashes($Mailtemplates[0]['CONTENT']);
//    $message = str_replace("##USER", @$name, $message);
    $headers = 'From:' . stripslashes($Mailtemplates[0]['FROMNAME']) . ' <' . stripslashes($Mailtemplates[0]['FROMEMAIL']) . "> \r\n" .
            'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $mail = @mail($toRIEMail, $subject, $message, $headers);
}

/*
 * Email Template for move quote to UW
 * Mail to UW
 * Mail cc RM inserted when quote created
 */

function SendMailForMoveTOUW($quoteId, $remarks) {
    global $conn;
    $sourceArr = fetchListCondsWithUWJOIN("WHERE UNDERWRITERQUOTE.ID = '$quoteId'");
    $toRM = $sourceArr[0]['RMEMAIL']; //RM Email id (Entered in quote creation step)
    $toUW = $sourceArr[0]['EMAILID']; // Under Writer Email
    $name = ucfirst($sourceArr[0]['NAME']);
    $refno = $sourceArr[0]['REFRENCENUMBER'];

    $Mailtemplates = GetTemplateContentByTemplateName('QUOTE_PENDING_FOR_REVIEW_UW');
    $subject = stripslashes($Mailtemplates[0]['SUBJECT']);
    $message = $Mailtemplates[0]['LARGECONTENT'] ? stripslashes($Mailtemplates[0]['LARGECONTENT']->load()) : stripslashes($Mailtemplates[0]['CONTENT']);
    $message = str_replace("##USER", $name, $message);
    $message = str_replace("##QUOTENO", $refno, $message);
    $message = str_replace("##REMARKS", $remarks, $message);
    $headers = 'From:' . stripslashes($Mailtemplates[0]['FROMNAME']) . ' <' . stripslashes($Mailtemplates[0]['FROMEMAIL']) . "> \r\n" .
            'Cc:' . stripslashes($toRM) . "\r\n" .
            'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $mail = @mail($toUW, $subject, $message, $headers);
}

function SendMailForConvertedRejectedApprovedQuote($QUOTEID, $STATUS, $REMARKS) {
    //for status converted and rejected only
    if ($STATUS == STATUS_CONVERTED) {
        $MailStatus = SendMailForConvertedQuote($QUOTEID);
    } else {
        $MailStatus = SendMailForRejectedQuote($QUOTEID, $REMARKS);
    }
    return $MailStatus;
}

function SendMailToRMOrMoveToRI($QUOTEID, $STATUS, $remarks) {
    if ($STATUS == STATUS_MOREINFO_REQUIRED) {
        $MailStatus = SendMailForRM($QUOTEID, $remarks);
    } else if ($STATUS == STATUS_PENDING_RIAPPROVAL) {
        $MailStatus = SendMailForMoveTORI($QUOTEID, $remarks);
    } else if ($STATUS == STATUS_PENDING_UWAPPROVAL) {
        $MailStatus = SendMailForMoveTOUW($QUOTEID, $remarks);
    }
    return $MailStatus;
}

/*
 * Email Template for Approved quote by UW
 * Mail to RM
 * Mail cc RM email id inserted when quote created
 */

function SendMailForApprovedQuote($quoteId, $remarks) {
    global $conn;
    $rmuserArr = fetchListCondsWithColumn("NAME", 'CRMOAQUSER', " where EMAILID = '" . $_SESSION["emailId"] . "' ");
    $sourceArr = fetchListCondsWithUWJOIN("WHERE UNDERWRITERQUOTE.ID = '$quoteId'");
    $toRMEmail = $sourceArr[0]['RMEMAIL']; //RM Email id (Entered in quote creation step)
    $toRM = $_SESSION["emailId"]; // Logged in RM
    $name = ucfirst($rmuserArr[0]['NAME']);
    $refno = $sourceArr[0]['REFRENCENUMBER'];
    $Mailtemplates = GetTemplateContentByTemplateName('QUOTE_UPDATE_UW');
    $subject = stripslashes($Mailtemplates[0]['SUBJECT']);
    $message = $Mailtemplates[0]['LARGECONTENT'] ? stripslashes($Mailtemplates[0]['LARGECONTENT']->load()) : stripslashes($Mailtemplates[0]['CONTENT']);
    $message = str_replace("##USER", $name, $message);
    $message = str_replace("##QUOTENO", $refno, $message);
    $message = str_replace("##REMARKS", $remarks, $message);
    $headers = 'From:' . stripslashes($Mailtemplates[0]['FROMNAME']) . ' <' . stripslashes($Mailtemplates[0]['FROMEMAIL']) . "> \r\n" .
            'Cc:' . stripslashes($toRM) . "\r\n" .
            'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $mail = @mail($toRMEmail, $subject, $message, $headers);
}

function createEmailArr($emailArr) {
    $arr = array();
    if (!empty($emailArr)) {
        foreach ($emailArr as $email) {
            $arr[] = $email['EMAILID'];
        }
    }
    return $arr;
}

//Added on 26/02/2016 to allow script keywords in sanitize
function sanitize_data2($input_data) {
    $searchArr = array("document", "write", "alert", "%", "$", ";", "+", "|", "#", "<", ">", ")", "(", "'", "\'", ",", "and", "USER_NAME", "AND");
    $input_data = str_replace("iframe", "", $input_data);
    $input_data = str_replace("javascript", "", $input_data);
    $input_data = str_replace($searchArr, "", $input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}

function moneyFormat($amount) {
    $amount = number_format($amount, 2);
    return $amount;
}

function sanitize_email($input_data) {
    $searchArr = array("document", "write", "alert", ";", "+", "|", "#", "<", ">", ")", "(", "'", "\'", ",", "<img", "src=", ".ini", "<iframe", "java:", "window.open", "http", "!", ":boot", ".exe", ".php", ".js", ".txt", ".css");
    $input_data = str_replace("script", "", $input_data);
    $input_data = str_replace("iframe", "", $input_data);
    $input_data = str_replace($searchArr, "", $input_data);
    $input_data = trim($input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}

/*
 * Function used to get client details
 * @params product type, quote Id
 * @author Ankur Kuriyal
 */

function clientDetails($source, $id) {
    global $conn;
    $dataArray = array();
    $query = "SELECT ID, STREET1, STREET2, LOCALITY, PINCODE, CITY, STATE, TELEPHONE, PANCARD, SERVICETAXNO, CONTACTPERSON, CONSENTEMAIL, TAXINFO, DATE_OF_INCORPORATION, FLAG, CLIENTID, SOURCE, USERID FROM POLICYDETAILS WHERE QUOTEID = '" . $id . "' AND SOURCE = '" . $source . "'";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function NoofPolicy() {

    global $conn;
    $dataArray = array();
    $query = "";
    $query .= "SELECT COUNT(CASE WHEN producttype='GHI' and flag BETWEEN 2 and 6 THEN 1 ELSE null END) as GHI,COUNT(CASE WHEN producttype='GHI' and flag=7 THEN 1 ELSE null END) as GHI_7,COUNT(CASE WHEN producttype='GHI' and flag=8 THEN 1 ELSE null END) as GHI_8,COUNT(CASE WHEN producttype='GPA' THEN 1 ELSE null END) as GPA,COUNT(CASE WHEN producttype='GPA' and flag BETWEEN 2 and 6 THEN 1 ELSE null END) as GPA,COUNT(CASE WHEN producttype='GPA' and flag=7 THEN 1 ELSE null END) as GPA_7,COUNT(CASE WHEN producttype='GPA' and flag=8 THEN 1 ELSE null END) as GPA_8,COUNT(CASE WHEN producttype='GTI' and flag BETWEEN 2 and 6 THEN 1 ELSE null END) as GTI,COUNT(CASE WHEN producttype='GTI' and flag=7 THEN 1 ELSE null END) as GTI_7,COUNT(CASE WHEN producttype='GTI' and flag=8 THEN 1 ELSE null END) as GTI_8 FROM POLICYDETAILS ";
    $USERID = $_SESSION['userId'];
    if ($_SESSION['type'] == SOURCE_ADMIN) {
        $query .= "";
    } elseif ($_SESSION['type'] == SOURCE_OPERATIONS) {
        $query .= "WHERE LOCAL_OPS=$USERID";
    } elseif ($_SESSION['type'] == SOURCE_CENTRALTEAM) {
        $query .= "WHERE CENTRAL_OPS=$USERID";
    } else {
        $query .= "WHERE UPDATEDBY=$USERID OR ASSIGNEDTO=$USERID OR USERID=$USERID";
    }
    $result = oci_parse($conn, $query);
    oci_execute($result);
    $i = 0;
    while (($row = oci_fetch_assoc($result))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

function MonthWisePolicy() {
    global $conn;
    $cond = "";
    $USERID = $_SESSION['userId'];
    if ($_SESSION['type'] == SOURCE_ADMIN) {
        $cond .= "";
    } elseif ($_SESSION['type'] == SOURCE_OPERATIONS) {
        $cond .= "AND LOCAL_OPS=$USERID";
    } elseif ($_SESSION['type'] == SOURCE_CENTRALTEAM) {
        $cond .= "CENTRAL_OPS=$USERID";
    } else {
        $cond .= "AND (UPDATEDBY=$USERID OR ASSIGNEDTO=$USERID OR USERID=$USERID)";
    }
    $i = 1;
    $curr_date = date("Y-m-d H:i");
    if (date("m") < 4)
        $i++;
    $lastFyear = date("Y-m-d H:i", strtotime("01-04-" . (date("Y") - $i) . " 00:00"));
    $query = "SELECT count(CASE WHEN producttype = 'GHI' THEN 1 ELSE null END) C_GHI,count(CASE WHEN producttype = 'GPA' THEN 1 ELSE null END) C_GPA,count(CASE WHEN producttype = 'GTI' THEN 1 ELSE null END) C_GTI,sum(CASE WHEN producttype = 'GHI' THEN premiumamount ELSE '0' END) S_GHI,sum(CASE WHEN producttype = 'GPA' THEN premiumamount ELSE '0' END) S_GPA,sum(CASE WHEN producttype = 'GTI' THEN premiumamount ELSE '0' END) S_GTI,to_char(to_date(UPDATEDON, 'YYYY-MM-DD HH24:MI'), 'yyyy-mm') year_month FROM POLICYDETAILS WHERE UPDATEDON BETWEEN '$lastFyear' AND '$curr_date' AND flag=9 $cond GROUP BY to_char(to_date(UPDATEDON, 'YYYY-MM-DD HH24:MI'), 'yyyy-mm') ORDER BY to_char(to_date(UPDATEDON, 'YYYY-MM-DD HH24:MI'), 'yyyy-mm')";

    $result = oci_parse($conn, $query);
    oci_execute($result);
    $i = 0;
    $dataArray = array();
    while (($row = oci_fetch_assoc($result))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    // pr($dataArray);
    return $dataArray;
}

function sendAttachment($mailto, $path, $filename) {
    $file_size = filesize($path);
    $handle = fopen($path, "r");
    $content = fread($handle, $file_size);
    fclose($handle);
    $content = chunk_split(base64_encode($content));
    $uid = md5(uniqid(time()));

    $Mailtemplates = GetTemplateContentByTemplateName('QUOTE_PDF_UPLOADED');
    $subject = stripslashes($Mailtemplates[0]['SUBJECT']);
    $message = $Mailtemplates[0]['LARGECONTENT'] != '' ? stripslashes($Mailtemplates[0]['LARGECONTENT']->load()) : stripslashes($Mailtemplates[0]['CONTENT']);
    // header
    $header = 'From:' . stripslashes($Mailtemplates[0]['FROMNAME']) . ' <' . stripslashes($Mailtemplates[0]['FROMEMAIL']) . "> \r\n";
    $header .= "MIME-Version: 1.0\r\n";
    $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";

    // message & attachment
    $nmessage = "--" . $uid . "\r\n";
    $nmessage .= "Content-type:text/html; charset=iso-8859-1\r\n";
    $nmessage .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
    $nmessage .= $message . "\r\n\r\n";
    $nmessage .= "--" . $uid . "\r\n";
    $nmessage .= "Content-Type: application/octet-stream; name=\"" . $filename . "\"\r\n";
    $nmessage .= "Content-Transfer-Encoding: base64\r\n";
    $nmessage .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
    $nmessage .= $content . "\r\n\r\n";
    $nmessage .= "--" . $uid . "--";
    if (mail($mailto, $subject, $nmessage, $header)) {
        return true;
    } else {
        return false;
    }
}

/*
 * Email Template for reject quote by underwriter
 * Mail to RM
 * Mail cc UW and RM email id inserted when quote created
 */

function SendMailForRejectedQuoteUWcase($QUOTEID, $remarks) {
    global $conn;
    $sourceArr = fetchListCondsWithUWJOIN("WHERE UNDERWRITERQUOTE.ID = '$QUOTEID'");
    $rmuserArr = fetchListCondsWithColumn("*", 'CRMOAQUSER', " where ID = '" . $sourceArr[0]['USERID'] . "' ");
    $toRMEmail = $sourceArr[0]['RMEMAIL']; //RM Email id (Entered in quote creation step)
    $toUW = $sourceArr[0]['EMAILID']; // Under Writer Email
    $toRM = $rmuserArr[0]["EMAILID"];
    $name = ucfirst($rmuserArr[0]['NAME']);
    $refno = $sourceArr[0]['REFRENCENUMBER'];
    $cc = $toUW . ',' . $toRMEmail;
    $Mailtemplates = GetTemplateContentByTemplateName('UW_REJECT');
    $subject = stripslashes($Mailtemplates[0]['SUBJECT']);
    $message = $Mailtemplates[0]['LARGECONTENT'] ? stripslashes($Mailtemplates[0]['LARGECONTENT']->load()) : stripslashes($Mailtemplates[0]['CONTENT']);
    $message = str_replace("##USER", $name, $message);
    $message = str_replace("##QUOTENO", $refno, $message);
    $message = str_replace("##REMARKS", $remarks, $message);
    $headers = 'From:' . stripslashes($Mailtemplates[0]['FROMNAME']) . ' <' . stripslashes($Mailtemplates[0]['FROMEMAIL']) . "> \r\n" .
            'Cc:' . stripslashes($cc) . "\r\n" .
            'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1' . "\r\n";
    $mail = @mail($toRM, $subject, $message, $headers);
}

function sanitize_quote_data($input_data) {
    $searchArr = array("document", "write", "alert", "%", "$", ";", "+", "|", "#", "<", ">", ")", "(", "\'", ",", "and", "USER_NAME", "AND");
    $input_data = str_replace("script", "", $input_data);
    $input_data = str_replace("iframe", "", $input_data);
    $input_data = str_replace("javascript", "", $input_data);
    $input_data = str_replace($searchArr, "", $input_data);
    return htmlentities(stripslashes($input_data), ENT_QUOTES);
}

function getNameFromNumber($num) {
    $numeric = $num % 26;
    $letter = chr(65 + $numeric);
    $num2 = intval($num / 26);
    if ($num2 > 0) {
        return getNameFromNumber($num2 - 1) . $letter;
    } else {
        return $letter;
    }
}

function fetchListByColumnName($table, $column, $condColumn, $condColumnVal) {
    global $conn;
    $dataArray = array();
    if (isset($table)) {
        $query = "SELECT $column FROM $table WHERE $condColumn='$condColumnVal'";
        $sql = oci_parse($conn, $query);
        // Execute Query
        oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

function SendMailToEmployee($email, $Password, $compTitle, $employeelink = '') {
    global $conn;
    if ($employeelink != '') {
        $Mailtemplates = GetCRMTemplateContentByTemplateName('COMPANY_EMPLOYEE_TEMP');
        $compTitle = "http://uat.religare.com/WebEnrol/login.php?u=" . base64_encode($employeelink);
        //$compTitle = "http://www.religarehealthinsurance.com/WebEnrol/login.php?u=" . base64_encode($employeelink);
    } else {
        $Mailtemplates = GetCRMTemplateContentByTemplateName('EMPLOYEE_TEMP');
    }
    $to = $email;
    $password = $Password;
    $subject = stripslashes($Mailtemplates[0]['SUBJECT']);
    $message = $Mailtemplates[0]['LARGECONTENT'] ? stripslashes($Mailtemplates[0]['LARGECONTENT']->load()) : stripslashes($Mailtemplates[0]['CONTENT']);
    $message = str_replace("##email", @$email, $message);
    $message = str_replace("##password", @$password, $message);
    $message = str_replace("##comptitle", @$compTitle, $message);
    $headers = 'From:' . stripslashes($Mailtemplates[0]['FROMNAME']) . ' <' . stripslashes($Mailtemplates[0]['FROMEMAIL']) . "> \r\n" .
            'MIME-Version: 1.0' . "\r\n" .
            'Content-type: text/html; charset=iso-8859-1';
    // Mail it
    if (mail($to, $subject, $message, $headers)) {
        $to = addslashes($to);
        $subject = addslashes($subject);
        $message = addslashes($message);
        $reasontomail = 'EMPLOYEE_TEMP';
        $sendby = '';
        $entryTime = date('d-M-Y');
        $status = 'ACTIVE';
        $sendfrom = 'info@religarehealthinsurance.com';
        $ipAddress = get_client_ip();
        $userType2 = '';
        //query to insert records

        $sqlLog = "INSERT INTO PPHCMAILLOG (MAILID,MAILTO,SUBJECT,MESSAGE,USERTYPE,REASONTOMAIL,SENDBY,SENDDATE,STATUS,SENDFROM,IPADDRESS) values(PPHCMAILLOG_SEQ.nextval,q'[" . $to . "]','" . @$subject . "',:largecontent,'" . @$userType2 . "','" . @$reasontomail . "','" . @$sendby . "','" . @$entryTime . "','" . @$status . "','" . @$sendfrom . "','" . @$ipAddress . "') ";
        $sqlLogExe = @oci_parse($conn, $sqlLog);
        oci_bind_by_name($sqlLogExe, ':largecontent', $message);
        $r = @oci_execute($sqlLogExe);
        //end of email log
    }
}

function get_client_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP'))
        $ipaddress = getenv('HTTP_CLIENT_IP');
    else if (getenv('HTTP_X_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    else if (getenv('HTTP_X_FORWARDED'))
        $ipaddress = getenv('HTTP_X_FORWARDED');
    else if (getenv('HTTP_FORWARDED_FOR'))
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    else if (getenv('HTTP_FORWARDED'))
        $ipaddress = getenv('HTTP_FORWARDED');
    else if (getenv('REMOTE_ADDR'))
        $ipaddress = getenv('REMOTE_ADDR');
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}

function GetCRMTemplateContentByTemplateName($templateName) {
    global $conn;
    $dataArray = array();
    $query = "SELECT TEMPLATEID,SUBJECT,CONTENT,LARGECONTENT,FROMEMAIL,FROMNAME,TEMPLATENAME from CRMEMAILTEMPLTE where TEMPLATENAME='" . $templateName . "' ";
    $sql = @oci_parse($conn, $query);
    @oci_execute($sql);
    $i = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $dataArray[$i][$key] = $value;
        }
        $i++;
    }
    return $dataArray;
}

?>