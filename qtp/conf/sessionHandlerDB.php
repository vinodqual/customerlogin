<?php 
$conn=null;

class SessionHandlerDb {

    function __construct() { //object constructor
        global $conn;
        if ($_SERVER['HTTP_HOST'] == 'localhost') {
            $db_hostName = "10.1.1.76"; //defined host name
            $db_hostPort = "1521"; //defined host name
            $db_serviceName = "QTP"; //defined db name
            $db_username = "RHICL";  // define db username
            $db_password = "RHICL"; // define db password
        } else {
            $db_hostName="10.216.9.197"; //defined host name
            $db_hostPort="2529"; //defined host name
            $db_serviceName="orcl"; //defined db name
            $db_username="rhicl";  // define db username
            $db_password="rhicl"; // define db password
        }
        
        // Connection  string with Oracle */
        $conn = oci_connect("$db_username", "$db_password", "(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)
            (HOST=" . $db_hostName . ")(PORT=" . $db_hostPort . "))
            (CONNECT_DATA=(SERVICE_NAME=" . $db_serviceName . ")))");
        if (!$conn) {
			$arr = array('msg' => 'Site Down', 'ref_no' => '');
			echo json_encode($arr);
			exit;
        }
        session_set_save_handler(
			array($this, 'open'), array($this, 'close'), array($this, 'read'), array($this, 'write'), array($this, 'destroy'), array($this, 'gc'));
    }

    public function start($session_name = null) {
        session_start($session_name);  //Start it here
    }

    public static function open() {
        global $conn;
        return true;
    }

    public static function read($id) {
        global $conn;
        
        $query = "SELECT DATA FROM CRMEMPLOPYEESESSION WHERE id = '$id'";
        $sql = @oci_parse($conn, $query);
        // Execute Query
        $result = oci_execute($sql) or die(header("Location: sitedown.php"));
        if (isset($result) && !empty($result)) {
            $row = oci_fetch_row(@$sql);
            return $row[0] ? $row[0]->load() : '';
        } else {
            return '';
        }
    }

    public static function write($id, $data) {
        global $conn;
        
        $CurrentTime = time();
        $query = "SELECT * FROM CRMEMPLOPYEESESSION WHERE ID='" . $id . "'";
        $sql = @oci_parse($conn, $query);
        $result = @oci_execute($sql);
        $row = oci_fetch_assoc(@$sql);
        $access = time();
        if (count($row) > 0) {

            $delquery = "DELETE FROM CRMEMPLOPYEESESSION WHERE ID='" . $id . "'";
            $stdid = @oci_parse($conn, $delquery);     //query to update records
            $r = @oci_execute($stdid);

            $insquery = "INSERT INTO CRMEMPLOPYEESESSION (ID,ACCESSTIME,DATA) values ('" . $id . "','" . $access . "',:datas)";
            $stdid2 = @oci_parse($conn, $insquery);
            oci_bind_by_name($stdid2, ':datas', $data);
            //query to update records

            $r2 = @oci_execute($stdid2) or die(print_r(oci_error($stdid2)));
        } else {
            $insquery = "INSERT INTO CRMEMPLOPYEESESSION (ID,ACCESSTIME,DATA) values ('" . $id . "','" . $access . "',:datas)";
            $stdid2 = @oci_parse($conn, $insquery);
            oci_bind_by_name($stdid2, ':datas', $data);
            //query to update records

            $r2 = @oci_execute($stdid2) or die(print_r(oci_error($stdid2)));
        }
        return 1;
    }

    public static function destroy($id) {
        global $conn;
        
        $q = "delete from CRMEMPLOPYEESESSION where id='" . $id . "'";
        $sql = @oci_parse($conn, $q);
        // Execute Query
        $result = @oci_execute($sql);
    }

    public static function gc() {
        return true;
    }

    public static function close() {
        global $conn;

        return true;
    }

    public function __destruct() {
        session_write_close();
    }

}
