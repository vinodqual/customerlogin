<?php
/**
 * Copyright 2013 Catabatic Automation Technology Pvt Ltd.
 * All rights reserved
 *
 * @description: qtpConstants.php, Define Contanst Variables of QTP Application (dependent file @see ..js/qtpconstants.js). 2016/02/29 11:30. 
 * @author: Shakti Rana <shakti@catpl.co.in>
 */

//Constants for CRMOAQSOURCE.
CONST SOURCE_RMRETAIL       = 'RMRetail';
CONST SOURCE_PARTNER        = 'Partner';
CONST SOURCE_RM             = 'RM';
CONST SOURCE_PARTNERAGENT   = 'Partner-Agent';
CONST SOURCE_PARTNERBROKER  = 'Partner-Broker';
CONST SOURCE_UNDERWRITER    = 'Under-Writer';
CONST SOURCE_RI             = 'RI';
CONST SOURCE_OPERATIONS     = 'Operations';
CONST SOURCE_CENTRALTEAM    = 'Central Team';
CONST SOURCE_ADMIN          = 'Admin';
CONST SOURCE_AGENT          = 'agent';
CONST SOURCE_OTCUW          = 'OTC UW';

//Constant for Common heading used in *quoteApproval.php
CONST QUOTEAPPROVALSUBHEADER= '<div class="title-bg"><h1>Quote Updates</h1></div>';

//Constants for QUOTE STATUS.
CONST STATUS_APPROVED                   = 'Approved';
CONST STATUS_REJECTED                   = 'Rejected';
CONST STATUS_PUTONHOLD                  = 'Put On Hold';

CONST STATUS_PENDING_RIAPPROVAL         = 'Pending RI Approval';
CONST STATUS_MOREINFO_REQUIRED          = 'More Info Required';
CONST STATUS_PENDING_UWAPPROVAL         = 'Pending UW Approval';
CONST STATUS_PENDING_RATEAPPROVAL       = 'Pending Rate Approval';

CONST STATUS_CONVERTED                  = 'Converted';
CONST STATUS_POLICY_INFOREQUIRED        = 'Policy Info Required';
CONST STATUS_PENDING_FORPOLICYISSUANCE  = 'Pending for Policy Issuance';
CONST STATUS_PENDING_FORDOCUMENT        = 'Pending for Document Upload';
CONST STATUS_POLICYISSUED               = 'Policy Issued';
CONST STATUS_POLICY_PUTONHOLD           = 'Policy Put On Hold';
CONST STATUS_UW_APPROVAL                = 'UW Approval';
CONST STATUS_PENDINGINFOREQUIRED        = 'Pending for Information';
CONST STATUS_PENDINGINFOREQUIRED_RM     = 'Pending information from RM';

CONST STATUS_PENDINGINFORRECEIPTANDHEADER  = 'Pending for Receipting/Header Info';
CONST STATUS_PENDINGINFORHEADER            = 'Pending for Header Info';
CONST STATUS_PENDINGINFORRECEIPTANDDOC     = 'Pending for Receipting/Document Upload';
CONST STATUS_PENDINGINFORRECEIPT           = 'Pending for Receipting';

class POLICY_FLAG{
    const CLIENT_INFORMATION_ADDED=1;
    const PAYMENT_DETAILS_ADDED=2;  // by RM
    const LOCAL_OPS_ASSIGNED=3;     // mailed to local ops
    
    const POLICY_HEADER_CREATED=4;  // by RM
    const DOCS_ADDED_NO_RECEIPTING=5;   // by RM (if no action taken by local ops now)
    const RECEIPTING_NO_DOCS=6;     // by local ops(if no action taken by RM till now)
    const CENTRAL_OPS_ASSIGNED=7;   // if both of 5&6 done in any sequence
    
    const BACK_TO_RM_FOR_DOCS=8;    // by central, now RM can add docs (only)
    const POLICY_ISSUED=9;          // by central, mil to all 3
}
if ($_SERVER['HTTP_HOST'] == 'localhost') {
    $ftpServer = "ftp.religarehealthinsurance.com";
    $ftpUser = "rhiclportal@religare.com";
    $ftpPass = "rh!cl@123";
} else {
    $ftpServer = "ftp.religarehealthinsurance.com";
    $ftpUser = "rhiclportal@religare.com";
    $ftpPass = "rh!cl@123";
}
$policyHeaderUrl = 'http://10.216.9.176/cordys/WSDLGateway.wcp?service=http://schemas.cordys.com/default/PolicyHeaderCreationBPMWS&organization=o=ReligareHealth,cn=cordys,cn=OTInst,o=religare.in&version=isv';
$policyHeaderEndPoint = 'http://10.216.9.176/cordys/com.eibus.web.soap.Gateway.wcp?organization=o=ReligareHealth,cn=cordys,cn=OTInst,o=religare.in';
$receiptUrl = "http://10.216.9.177:8090/relinterface/services/RelSymbiosysServices?wsdl"; //UAT
//$receiptUrl = "http://10.216.30.82:8090/relinterface/services/RelSymbiosysServices?wsdl"; //QC
$receiptEndPoint='http://10.216.9.177:8090/relinterface/services/RelSymbiosysServices.RelSymbiosysServicesHttpSoap12Endpoint/'; //UAT
//$receiptEndPoint='http://10.216.30.82:8090/relinterface/services/RelSymbiosysServices.RelSymbiosysServicesHttpSoap12Endpoint/'; //QC
?>