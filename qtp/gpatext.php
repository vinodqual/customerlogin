<?php 
//$pdf->content1($firststr);
if($quoteArr[0]['ADMINREMARKS']!='') {
$firststr.= 'Amended Information:
'.stripslashes(@$quoteArr[0]['ADMINREMARKS']).'
';
}
$pdf->content1($firststr);
$firststr ='';
/*******************************************/
$pdf->heading('Details of Benefits:');
$firststr ='';
$pdf->content2($firststr);
/*******************************************/
for($m=0;$m<count(@$covarr);$m++) {
	$pdf->coverageheading(@$covarr[$m]['COVERAGE']);
	//$firststr.= @$covarr[$m]['COVERAGE'];
	/*$firststr.='	
	';*/
	$firststr.=	@$covarr[$m]['DESCRIPTION'];
	/*$firststr.='
		
	';*/
	$pdf->content1($firststr);
	$firststr ='';
	} 
$firststr ='';
for($m=0;$m<count(@$othercovarr);$m++) {
	$pdf->coverageheading(@$othercovarr[$m]['COVERAGE']);
	//$firststr.= @$othercovarr[$m]['COVERAGE'];
	/*$firststr.='	
	';*/
	$firststr.=	@$othercovarr[$m]['DESCRIPTION'];
	$pdf->content1($firststr);
	$firststr ='';
	}
$firststr ='';
$pdf->heading('Notes:**');
$firststr.='
1.	Terrorism is covered in the policy except for that arising out of Nuclear; Biological and/or chemical means which is outside the scope of the policy.
2.	Premium for Addition & deletion to be charged on pro-rata.
3.	Premium shall not be refunded for deletion if any claim is paid during the policy.
4.	Any endorsements will be from the date of addition and not from the inception of the policy.
5.	Quote is valid for 90 days. Quote is liable to change with change in information.
6.	Quote is valid for the given set of data, any change in data will require revised premium.
7.	All terms and conditions as per existing policy except addition option provide base of existing policy calculation.';
$pdf->content1($firststr);
$firststr ='';
$pdf->heading('Major Exclusions:**');
$firststr.='
1.	Pre-Existing Diseases.
2.	Suicide, attempt to Suicide or intentionally self- inflicted injury, sexually transmitted conditions, mental disorder, anxiety, stress or depression. 
3.	Being under influence of drugs, alcohol, or other intoxication or hallucinogens.
4.	Participation in actual or attempted felony, riot, civil commotion, crime misdemeanor.
5.	Committing any breach of law of land with criminal intent. 
6.	Death or disablement resulting from Pregnancy or childbirth.
7.	Professional sports team in respect of specific benefit for inability to perform.
8.	Participation in any kind of motor speed contest.
9.	While engaged in aviation, or whilst mounting or dismounting from or traveling in any aircraft, other than as a fare paying passenger in a Scheduled Airline.
10.	Underground mining & contractor specializing in tunnelling.
11.	Naval, military or air force personnel.
12.	Radioactivity, Nuclear risks, ionizing radiation.
13.	Detailed Exclusion as per the Standard Policy Wordings of the Group Secure Policy.';
$pdf->content1($firststr);
$firststr ='';
$pdf->heading('Major Documentation Required to file a claim:- Immediate Written Intimation to the Insurer:**');
$firststr.='
1.	Claim Form Duly Signed.
2.	Identity Proof.
3.	Accident Proof - FIR, Panchnama, Final Police Report, State Electricity Board Report, Factory Inspection Report, Forensic Report etc.
4.	Cause of Loss - Viscera Report, Post Mortem Report (if conducted), MLC report, Medical Report or Certificate.
5.	Disability - Disability Certificate from Government Medical Board, Fitness Certificate, Medical Prescription.
6.	Accidental Death � Death Certificate.
7.	Medical Expenses - Hospital Discharge Summary, Bills, Receipts as original, Medical Practitioner Certificate, Medical or Clinical or Pathological or Diagnostics Records.

**These are just indicative documents; additional documents may be required as per the claim.
';
?>