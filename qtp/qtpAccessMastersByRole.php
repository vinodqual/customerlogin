<?php

/**
 * Copyright 2013 Catabatic Automation Technology Pvt Ltd.
 * All rights reserved
 *
 * @description: qtpAccessMastersByRole.php, 2016/04/05 15:37
 * @author: Pooja Choudhary <pooja@catpl.co.in>
 */
include_once("conf/company_session.php");
include_once("conf/conf.php");
include_once("conf/fucts.php");

class AccessMaster {
    
    /**
     * getDropdownAccessData. To get Dynamic dropdowns access by source Type.
     * 
     * @global resource $conn
     * @param  string $product
     * @param  string $uniqueKey
     * @return query String.
     */
    public static function getDropdownAccessData($product, $uniqueKey) {// $uniqueKey  unique parameter to get relevant data
        $sourceId = $_SESSION["sourceId"]; // to detect source id

        $data = fetchListCond('INPUTVALUECONTROL', " WHERE NAME='" . $uniqueKey . "' AND SOURCEID='" . $sourceId . "' AND PRODUCT='" . $product . "'");

        if (count($data) > 0) {
            $queryString = 'AND ' . $data[0]['COLUMNNAME'] . ' IN (' . $data[0]['INPUTVALUE'] . ')';
        } else {
            $queryString = '';
        }
        return $queryString;
    }
    
    public static function getDropdownAccessData_08April2016($product, $uniqueKey, $condition='') {// $uniqueKey  unique parameter to get relevant data
        $queryString = $condition;
        $sourceId = $_SESSION["sourceId"]; // to detect source id
        $data = fetchListCond('INPUTVALUECONTROL', " WHERE NAME='" . $uniqueKey . "' AND SOURCEID='" . $sourceId . "' AND PRODUCT='" . $product . "'");        
        
        if (count($data) > 0) 
        {
            if(@$queryString)
            {
                if(strstr($condition, ' order ', true))
                {
                    $orderBy = (strstr($condition, 'order'));
                }
                else if(strstr($condition, ' ORDER ', true))
                {
                    $orderBy = (strstr($condition, 'ORDER'));
                }
            }            
            $queryString = 'WHERE '. $data[0]['COLUMNNAME'] . ' IN (' . $data[0]['INPUTVALUE'] . ') '. @$orderBy;
        }        
        return $queryString;
    }

    /**
     * getStaticDropdownAccessData. To get Static dropdowns access by source Type.
     * 
     * @param  string $product
     * @param  string $uniqueKey
     * @param  string $name
     * @param  string $id
     * @param  string $dbValue
     * @return string.
     */
    public static function getStaticDropdownAccessData($product, $uniqueKey, $name, $id = '', $dbValue = '') {// $uniqueKey  unique parameter to get relevant data
        $sourceId = $_SESSION["sourceId"]; // to detect source id
        $DBdata = fetchListCond('INPUTVALUECONTROL', " WHERE NAME='" . $uniqueKey . "' AND SOURCEID='" . $sourceId . "' AND PRODUCT='" . $product . "'");
//        pr($DBdata[0]['INPUTVALUE']);
       $DBdataArr = explode(',',$DBdata[0]['INPUTVALUE']);
        
        $dropDownArr = self::getDropDownArrayByName($name);

        $selectVal = '<select id="' . $id . '" name="' . $name . '">
                      <option value="">Select ' . $uniqueKey . '</option>';
//        if (sizeof($dropDownArr['DropdownArray']) > 1) {
            foreach ($dropDownArr['DropdownArray'] as $key => $dropdownVal) {
                $selected = ($dropdownVal == $dbValue) ? ' selected="selected"' : '';
                if($selected==''){
                    $selected = (strtolower($dropdownVal) == 'single') ? ' selected="selected"' : '';
                }
                if(in_array($dropdownVal,$DBdataArr) && sizeof($DBdata)>0){
                $selectVal .= '<option value="' . $dropdownVal . '" ' . $selected . '>' . ucfirst($key) . '</option>';
                } 
                
                if(sizeof($DBdata)==0){
                $selectVal .= '<option value="' . $dropdownVal . '" ' . $selected . '>' . ucfirst($key) . '</option>';
                  
                }
                }
//        } else {
//            $keyVal = key($dropDownArr['DropdownArray']);
//            $selectVal .= '<option value="' . $dropDownArr['DropdownArray'][$keyVal] . '" selected>' . ucfirst($keyVal) . '</option>';
//        }
                
        return $selectVal.'</select>';
    }

    /**
     * getRadioAccessData. To get Radio buttons access by source Type.
     * 
     * @param  string $product
     * @param  string $uniqueKey
     * @param  string $name
     * @param  string $id
     * @param  string $dbValue
     * @param  string $disabled
     * @param  string $class
     * @return string.
     */
    public static function getRadioAccessData($product, $uniqueKey, $name, $disabled = '', $dbValue = '', $id = '', $class = '') {
        $sourceId = $_SESSION["sourceId"]; // to detect source id
        $DBdata = fetchListCond('INPUTVALUECONTROL', " WHERE NAME='" . $uniqueKey . "' AND SOURCEID='" . $sourceId . "' AND PRODUCT='" . $product . "'");
        $DBdataArr = explode(',',$DBdata[0]['INPUTVALUE']);

        if (!empty($disabled)) {
            $disabled = ' disabled="disabled"';
        }
        if (!empty($class)) {
            $class = ' class="' . $class . '"';
        }

        $radioArr = self::getRadioArrayByName($name);
//        if($name=='POLICYTYPE'){
//        print_r($DBdata);
////        pr($radioArr);
//        }
        if (sizeof($radioArr['RadioArray']) > 1) {
            foreach ($radioArr['RadioArray'] as $key => $radioVal) {

                if (!is_int($key)) {
                    $radioDisplayVal = $key;
                } else {
                    $radioDisplayVal = $radioVal;
                }
                //for onclick function
                if (sizeof($radioArr['action']) > 1) {
                    foreach ($radioArr['action'] as $action) {
                        $Action[] = $action . "('" . $radioVal . "')";
                    }
                    $actionImplode = implode(';', $Action);
                    $ActionVal = ' onclick="' . $actionImplode . '"';
                } else if (sizeof($radioArr['action']) == 1) {
                    $fFunction = $radioArr['action'][0] . "('" . $radioVal . "')";
                    $ActionVal = ' onclick="' . $fFunction . '"';
                }
                //for onclick function

                if ($name == 'POLICYTYPE') {
                    $checked = ($key == $dbValue) ? ' checked="checked"' : '';
                } else {
                    $checked = ($radioVal == $dbValue) ? ' checked="checked"' : '';
                }
                if(in_array($radioVal,$DBdataArr) && sizeof($DBdata)>0){
                $radioInput .= '<input type="radio" name="' . $name . '" value="' . $radioVal . '" ' . $ActionVal . $class . $checked . $disabled . '/>' . $radioDisplayVal . '&nbsp;&nbsp;&nbsp;';
                }
                if(sizeof($DBdata)==0){
                $radioInput .= '<input type="radio" name="' . $name . '" value="' . $radioVal . '" ' . $ActionVal . $class . $checked . $disabled . '/>' . $radioDisplayVal . '&nbsp;&nbsp;&nbsp;';
                }
        } } else {
            $radioInput = '<input type="radio" name="' . $name . '" value="' . $radioArr[0] . '" checked="checked" ' . $ActionVal . $class . $disabled . '/>' . $radioArr[0] . '&nbsp;&nbsp;&nbsp;';
        }
        return $radioInput;
    }

    /**
     * getRadioArrayByName. To get Radio array by name.
     * 
     * @param  string $name
     * @return Array.
     */
    private static function getRadioArrayByName($name) {
        $FuncNameArr = array();

        if ($name == 'POLICYTYPE') {
            $sourceList = fetchListCond('CRMOAQPOLICYTYPE', " WHERE STATUS = 'ACTIVE' order by ID ASC");
            $RadioArray = array();
            $i = 0;
            while ($i < count($sourceList)) {
                $RadioArray[$sourceList[$i]['TYPE']] = $sourceList[$i]['TYPE'] . '_' . $sourceList[$i]['LOADINGPERCENT'];
                $i++;
            }
        }

        if ($name == 'CONTRIBUTORYTYPE') { //Is  Every  Employee / Member in Company Covered
            $RadioArray = array('Yes' => 'Non-Contributory', 'No' => 'Contributory');
        }

        if ($name == 'CARDTYPE') { //Type of Health Card
            $RadioArray = array('E-Card' => 'ecard', 'Physical' => 'physical');
        }

        if ($name == 'PROPOSEDTYPE') {
            $RadioArray = array('Cashless', 'Reimbursement');
        }

        if ($name == 'WAITINGPERIOD') { //30 days / 1 yr / 2 yr Waiting period
            $RadioArray = array('Waived-Off' => 'waveoff', 'Applicable' => 'applicable');
        }

        if ($name == 'PEDEXCLUSION') {
            $RadioArray = array('Waived-Off' => 'waveoff', 'Applicable' => 'applicable', 'PED wait period 1 year' => 'pedWait1Year');
            $FuncNameArr = array('pedexclusion');
        }

        if ($name == 'proposalform') {
            $RadioArray = array('yes', 'no');
        }

        return array('RadioArray' => $RadioArray, 'action' => $FuncNameArr);
    }

    /**
     * getDropDownArrayByName. To get static Dropdown array by name.
     * 
     * @param  string $name
     * @return Array.
     */
    private static function getDropDownArrayByName($name) {
        $FuncNameArr = array();
        if ($name == 'MANDATETYPE') {
            $dropdownArray = array('Single' => 'SINGLE', 'Multiple' => 'MULTIPLE');
        }
        return array('DropdownArray' => $dropdownArray, 'action' => $FuncNameArr);
    }

    /**
     * getRadioButtonAccessData. To get Dynamic dropdowns access by source Type.
     * 
     * @param string $product
     * @param string $uniqueKey. unique parameter to get relevant data.
     * @param string $name
     * @param string $condition
     * @return array
     */
    public static function getRadioButtonAccessData($product, $uniqueKey, $condition='') {
 
        $data           = array();
        $queryString    = $condition;        
        $sourceId       = $_SESSION["sourceId"];
        $result         = fetchcolumnListCond('INPUTVALUE', 'INPUTVALUECONTROL', " WHERE NAME='" . $uniqueKey . "' AND SOURCEID='" . $sourceId . "' AND PRODUCT='" . $product . "'"); 
        $values         = (@$result[0]['INPUTVALUE']) ? explode(',', $result[0]['INPUTVALUE']) : '';
        foreach($values as $value)
        {
            $data[]= $value;
        }
        return $data;
        
//        if (count($data) > 0) 
//        {
//            if(@$queryString)
//            {
//                if(strstr($condition, ' order ', true))
//                {
//                    $orderBy = (strstr($condition, 'order'));
//                }
//                else if(strstr($condition, ' ORDER ', true))
//                {
//                    $orderBy = (strstr($condition, 'ORDER'));
//                }
//            }            
//            $queryString = 'WHERE '. $data[0]['COLUMNNAME'] . ' IN (' . $data[0]['INPUTVALUE'] . ') '. @$orderBy;
//        }        
//        return $queryString;
    }
}
