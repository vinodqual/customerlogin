<?php

include_once("conf/company_session.php");
include_once("conf/conf.php");
include_once("conf/fucts.php");
require('pdf-Religare/fpdf.php');

class PDF extends FPDF {

    // Colored table
    function FancyTable($columnWidth, $columnData) {
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(34);
        $this->SetFont('Helvetica', '', 10);
        // Data
        $fill = false;
        for ($k = 0; $k < count($columnData); $k++) {
            for ($i = 0; $i < count($columnWidth); $i++) {
                $this->Cell($columnWidth[$i], 7, $columnData[$k][$i], 1, 0, 'L', $fill);
            }
            $this->Ln();
        }
        $this->Ln(6);
    }

    function column($columnName, $columnWidth) {
        $this->SetFillColor(0, 95, 43);
        $this->SetTextColor(255);
        $this->SetDrawColor(218);
        $this->SetLineWidth(.2);
        $this->SetFont('Helvetica', 'b', 9);
        for ($i = 0; $i < count($columnWidth); $i++)
            $this->Cell($columnWidth[$i], 9, $columnName[$i], 1, 0, 'L', true);
        $this->Ln();
    }

    function heading($heading) {
        $this->SetDrawColor(218);
        $this->SetFillColor(247);
        $this->SetTextColor(7, 101, 51);
        $this->SetFont('Helvetica', '', 12);
        $this->Cell('', 9, $heading, 1, 0, 'L', true);
        $this->Ln();
    }

    function coverageheading($heading) {
        //$this->SetDrawColor(218);
        //$this->SetFillColor(247);
        $this->SetTextColor(7, 101, 51);
        $this->SetFont('Helvetica', '', 10);
        $this->Cell('', 7, $heading, 0, 0, 'L', true);
        $this->Ln();
    }

    function longrow($heading) {
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(34);
        $this->SetFont('Helvetica', '', 10);
        $fill = false;
        $this->Cell('', 9, $heading, 1, 0, 'L', $fill);
        $this->Ln();
    }

    function headingc($heading) {
        $this->SetDrawColor(218);
        $this->SetFillColor(247);
        $this->SetTextColor(7, 101, 51);
        $this->SetFont('Helvetica', '', 12);
        $this->Cell('', 9, $heading, 1, 0, 'C', true);
        $this->Ln();
    }

    function logo() {
        $this->Image('religare_logo.jpg', 10, 6, 70);
        $this->Ln(12);
    }

    function content($file) {
        $this->SetTextColor(108);
        // Read text file
        $txt = file_get_contents($file);
        // Times 12
        $this->SetFont('Helvetica', '', 9);
        // Output justified text
        $this->MultiCell(0, 5, $txt);
        // Line break
        $this->Ln(6);
    }

    function content1($txt) {
        $this->SetTextColor(108);
        // Read text file
        //$txt = file_get_contents($file);
        // Times 12
        $this->SetFont('Helvetica', '', 9);
        // Output justified text
        $this->MultiCell(0, 5, $txt);
        // Line break
        $this->Ln(6);
    }

    function content2($txt) {
        $this->SetTextColor(108);
        // Read text file
        //$txt = file_get_contents($file);
        // Times 12
        $this->SetFont('Helvetica', '', 9);
        // Output justified text
        $this->MultiCell(0, 5, $txt);
        // Line break
        $this->Ln(1);
    }

    function Footer() {
        //$this->SetTextColor(108);		
        $address = OFFICEADDRESS;
        // Go to 1.5 cm from bottom
        $this->SetY(-15);
        $this->SetFillColor(0, 95, 43);
        $this->SetTextColor(255);
        $this->SetDrawColor(218);
        $this->SetLineWidth(.2);
        $this->SetFont('Helvetica', 'b', 6.8);
        $this->MultiCell(0, 3, $address, 0, 'L', true);
        $this->Ln(0);
        //$this->MultiCell(0,0,'__________________________________________________________________________________________________________________________________________________________________________');
        //$this->Cell(0,10,@$address,0,0,'C');
    }

}

$pdf = new PDF();

$pdf->AddPage();
$pdf->logo();

foreach ($_REQUEST as $key => $value) {
    $$key = $value;
}
if ((trim(@$_REQUEST["quoteId"]) == "") || (trim(@$_SESSION["userId"]) == "")) {
    session_unset();
    session_destroy();
    $url = "Location:  index.php";
    header($url);
    exit;
}
$array_tblsite = fetchListById('CRMOAQSETTINGS', 'ID', '3');

$quoteId = sanitize_data(@$_REQUEST['quoteId']);
$quoteArr = gpaQuoteDetails($quoteId);
/* echo '<pre>';
  print_r($quoteArr);
  die(); */
$refnoarr = explode("_", @$quoteArr[0]['REFRENCENUMBER']);
$refno = $refnoarr[0];
$refinc = $refnoarr[1];
if ($refinc > 9) {
    $concatrefno = $refinc;
} elseif ($refinc > 0) {
    $concatrefno = '0' . $refinc;
} else {
    $concatrefno = '00';
}
$finalRefNo = $refno . $concatrefno;

$quoteCoverage = fetchListCond("GPAQUOTECOVERAGE", " WHERE QUOTEID = '" . @$quoteId . "' ORDER BY ID DESC");

if ($quoteArr[0]['CLOSESTATUS'] == 'CONVERTED') {
    $pextax = $quoteArr[0]['CONVERTEDPREMIUM'];
    $pextaxamt = $quoteArr[0]['CONVERTEDPREMIUM'];
} elseif ($quoteArr[0]['REQUIREDPREMIUM'] != '') {
    $pextax = $quoteArr[0]['REQUIREDPREMIUM'];
    $pextaxamt = $quoteArr[0]['REQUIREDPREMIUM'];
} elseif ($quoteArr[0]['ADMINUPDATEDPREMIUM'] != '') {
    $pextax = $quoteArr[0]['ADMINUPDATEDPREMIUM'];
    $pextaxamt = $quoteArr[0]['ADMINUPDATEDPREMIUM'];
} else {
    $pextax = $quoteArr[0]['BROKERAGEPREMIUM'];
    $pextaxamt = $quoteArr[0]['BROKERAGEPREMIUM'];
}
$quoteprm = $quoteArr[0]['BROKERAGEPREMIUM'];

$totalMembers = $quoteArr[0]['MEMBERNO'];

if ($quoteArr[0]['CLOSESTATUS'] == 'CONVERTED') {
    $totpremium = $quoteArr[0]['CONVERTEDTOTALPREMIUM'];
} elseif ($quoteArr[0]['REQUIREDPREMIUM'] != '') {
    $taxAmt = ($quoteArr[0]['REQUIREDPREMIUM'] * $quoteArr[0]['SERVICETAX']) / 100;
    $totpremium = $quoteArr[0]['REQUIREDPREMIUM'] + $taxAmt;
} elseif ($quoteArr[0]['ADMINUPDATEDPREMIUM'] != '') {
    $taxAmt = ($quoteArr[0]['ADMINUPDATEDPREMIUM'] * $quoteArr[0]['SERVICETAX']) / 100;
    $totpremium = $quoteArr[0]['ADMINUPDATEDPREMIUM'] + $taxAmt;
} else {
    $totpremium = $quoteArr[0]['BROKERAGEPREMIUM'];
}
/* $checkLeapYear = date('L',time());
  if($checkLeapYear==1){	$addDays = 365*24*3600;	} else {	$addDays = 364*24*3600;	} */

if ($quoteArr[0]['SITYPE'] == 'Flat') {
    $indTopFiftySI = '';
    $indTotalSI = '';
    $sival = @$quoteArr[0]['FLATSI'];
    $memberNo = @$quoteArr[0]['MEMBERNO'];
    if ($memberNo > 50) {
        $memmultiply = '50';
    } else {
        $memmultiply = $memberNo;
    }
    $indTopFiftySI = @$sival * $memmultiply;
    $indTotalSI = @$sival * $memberNo;
    $maximumsi = @$quoteArr[0]['FLATSI'];
    $totalsi = @$indTotalSI;
    $topfiftysi = @$indTopFiftySI;
} else {
    $maximumsi = @$quoteArr[0]['HIGHESTSI'];
    $totalsi = @$quoteArr[0]['TOTALSI'];
    $topfiftysi = @$quoteArr[0]['TOPFIFTYSI'];
}
$ratepermile = (($pextax / @$totalsi) * 1000);
/* echo 'pextax----'.$pextax;
  echo 'totalsi----'.$totalsi;
  echo 'ratepermile----'.$ratepermile; */
$taxAmt = ($pextax * $quoteArr[0]['SERVICETAX']) / 100;
$totpremium = $pextax + $taxAmt;

$covarr = array();
$query = "SELECT GPACOVERAGE.VALUETYPE,GPACOVERAGE.COVERAGE,GPACOVERAGE.DESCRIPTION,GPAQUOTECOVERAGE.COVERAGEID FROM GPAQUOTECOVERAGE, GPACOVERAGE WHERE GPAQUOTECOVERAGE.QUOTEID =" . $quoteId . " AND GPAQUOTECOVERAGE.COVERAGEID =  GPACOVERAGE.COVERAGEID AND GPACOVERAGE.PRIORITY = 'yes' ";
$sql = @oci_parse($conn, $query);
// Execute Query
@oci_execute($sql);
$w = 0;
while (($row = oci_fetch_assoc($sql))) {
    foreach ($row as $key => $value) {
        $covarr[$w][$key] = $value;
    }
    $w++;
}

$othercovarr = array();
$query = "SELECT GPACOVERAGE.VALUETYPE,GPACOVERAGE.COVERAGE,GPACOVERAGE.DESCRIPTION,GPAQUOTECOVERAGE.COVERAGEID FROM GPAQUOTECOVERAGE, GPACOVERAGE WHERE GPAQUOTECOVERAGE.QUOTEID =" . $quoteId . " AND GPAQUOTECOVERAGE.COVERAGEID =  GPACOVERAGE.COVERAGEID AND GPACOVERAGE.PRIORITY = 'no' ";
$sql = @oci_parse($conn, $query);
// Execute Query
@oci_execute($sql);
$t = 0;
while (($row = oci_fetch_assoc($sql))) {
    foreach ($row as $key => $value) {
        $othercovarr[$t][$key] = $value;
    }
    $t++;
}

$arrVal = '';
$s = 0;
$srno = '1';
for ($m = 0; $m < count(@$covarr); $m++) {
    $covid = $covarr[$m]['COVERAGEID'];
    $covvaltype = $covarr[$m]['VALUETYPE'];
    $covfeaturearray = array();
    $query = "SELECT GPACOVERAGEFEATURE.* FROM GPAQUOTECOVERAGE, GPACOVERAGEFEATURE WHERE GPAQUOTECOVERAGE.QUOTEID =" . @$quoteId . " AND GPAQUOTECOVERAGE.COVERAGEID =  " . $covid . " AND GPAQUOTECOVERAGE.FEATUREID =  GPACOVERAGEFEATURE.FEATUREID ";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $t = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $covfeaturearray[$t][$key] = $value;
        }
        $t++;
    }
    /* echo '<pre>';
      print_r(@$covfeaturearray);
      echo '<br>'; */
    if ($covfeaturearray[0]['TYPE'] == 'fixedamount') {
        $featureval = $covfeaturearray[0]['VALUE'];
    } else {
        if ($covvaltype == 'text') {
            $featureval = $covfeaturearray[0]['VALUE'];
        } else {
            $featureval = $covfeaturearray[0]['PERCENTSI'] . ' % of SI';
        }
    }

    $arrVal[$s][] = $srno;
    $arrVal[$s][] = $covarr[$m]['COVERAGE'];
    $arrVal[$s][] = $featureval;
    $s++;
    $srno++;
}

for ($m = 0; $m < count(@$othercovarr); $m++) {
    $othercovid = $othercovarr[$m]['COVERAGEID'];
    $othercovvaltype = $othercovarr[$m]['VALUETYPE'];
    $othercovfeaturearray = array();
    $query = "SELECT GPACOVERAGEFEATURE.* FROM GPAQUOTECOVERAGE, GPACOVERAGEFEATURE WHERE GPAQUOTECOVERAGE.QUOTEID =" . @$quoteId . " AND GPAQUOTECOVERAGE.COVERAGEID =  " . $othercovid . " AND GPAQUOTECOVERAGE.FEATUREID =  GPACOVERAGEFEATURE.FEATUREID ";
    $sql = @oci_parse($conn, $query);
    // Execute Query
    @oci_execute($sql);
    $t = 0;
    while (($row = oci_fetch_assoc($sql))) {
        foreach ($row as $key => $value) {
            $othercovfeaturearray[$t][$key] = $value;
        }
        $t++;
    }
    /* echo '<pre>';
      print_r(@$covfeaturearray);
      echo '<br>'; */
    if ($othercovfeaturearray[0]['TYPE'] == 'fixedamount') {
        $otherfeatureval = $othercovfeaturearray[0]['VALUE'];
    } else {
        if ($othercovvaltype == 'text') {
            $otherfeatureval = $othercovfeaturearray[0]['VALUE'];
        } else {
            $otherfeatureval = $othercovfeaturearray[0]['PERCENTSI'] . ' % of SI';
        }
    }
    $arrVal[$s][] = $srno;
    $arrVal[$s][] = $othercovarr[$m]['COVERAGE'];
    $arrVal[$s][] = $otherfeatureval;
    $s++;
    $srno++;
}

$gradebasis = '';
if ($quoteArr[0]['SITYPE'] == 'Graded') {
    $gradebasis = ' (' . $quoteArr[0]['GRADEBASIS'] . ')';
}
$pdf->heading('Group Secure');
/* $pdf->longrow('Name of Policyholder : '.@$quoteArr[0]['CORPORATENAME'].' ');
  $columnWidth = array(95,95); */
if ($quoteArr[0]['POLICYTYPE'] != 'Renewal') {
    $policyStartDate = ' -- ';
    $policyEndDate = ' -- ';
} else {
    $policyStartDate = '00:00hrs ' . date('d M Y', $quoteArr[0]['POLICYSTARTDATE']);
    $policyEndDate = 'Midnight ' . date('d M Y', (strtotime('+1 year', $quoteArr[0]['POLICYSTARTDATE']) - 86400));
    //$policyEndDate = 'Midnight '.date('d M Y',$quoteArr[0]['POLICYSTARTDATE']+@$addDays);
}
if ($quoteArr[0]['POLICYTYPE'] == 'Renewal') {
    $claimoutstanding = 'Rs. ' . @$quoteArr[0]['CLAIMAMOUNT'];
    $claimoutstandingcolname = 'Claims (Paid + O/S) :';
    $insurerArr = explode("_", $quoteArr[0]['EXPIRINGINSURER']);
    $expiringInsurer = $insurerArr[1];
    $expiringInsurercolname = 'Expiring Insurer';
    $columnData = array(array('Quote No', '' . @$finalRefNo . ''), array('Name of Policy holder', '' . @$quoteArr[0]['CORPORATENAME'] . ''), array('Cover type', 'Individual'), array('Policy Period - Start Date', '' . @$policyStartDate . ''), array('Policy Period - End Date', '' . @$policyEndDate . ''), array('Quote Type', '' . $quoteArr[0]['POLICYTYPE'] . ''), array($expiringInsurercolname, '' . $expiringInsurer . ''), array('Date of Quote Issue', '' . date('d M Y', $quoteArr[0]['CREATEDON']) . ''));
} else {
    $claimoutstandingcolname = '';
    $claimoutstanding = '';
    $columnData = array(array('Quote No', '' . @$finalRefNo . ''), array('Name of Policy holder', '' . @$quoteArr[0]['CORPORATENAME'] . ''), array('Cover type', 'Individual'), array('Policy Period - Start Date', '' . @$policyStartDate . ''), array('Policy Period - End Date', '' . @$policyEndDate . ''), array('Quote Type', '' . $quoteArr[0]['POLICYTYPE'] . ''), array('Date of Quote Issue', '' . date('d M Y', $quoteArr[0]['CREATEDON']) . ''));
}
$columnWidth = array(45, 145);
$pdf->FancyTable($columnWidth, $columnData);
/* $columnData = array(array('Policy Basis : '.$quoteArr[0]['POLICYBASIS'].'','Policy Period-Start Date : '.$policyStartDate.' '),array('Quote Type : '.$quoteArr[0]['POLICYTYPE'].' ','Policy Period-End Date 	: '.@$policyEndDate.''),array('Date of Quote Issue : '.date('d M Y',$quoteArr[0]['CREATEDON']).'',''),array(@$claimoutstandingcolname.@$claimoutstanding,''));  
  $pdf->FancyTable($columnWidth,$columnData); */

/* * **************************************** */
$pdf->heading('Premium Details');
$columnName = array('Premium excluding Tax', 'SERVICE TAX', 'TOTAL PREMIUM');
$columnWidth = array(63, 63, 64);
$columnData = array(array('INR ' . number_format($pextax, 2) . '', '' . number_format($taxAmt, 2) . '', 'INR ' . number_format($totpremium, 2) . ''));
$pdf->column($columnName, $columnWidth);
$pdf->FancyTable($columnWidth, $columnData);
/* * **************************************** */
/* $pdf->heading('Details of Members');
  $columnName = array('Sr No.', 'Basic Details', 'Particulars');
  $columnWidth = array(23,83,84);
  $columnData = array(array('1','Industry Type',''.$quoteArr[0]['INDUSTRY'].''),array('2','Total No.of Insured Members',''.$quoteArr[0]['MEMBERNO'].''),array('3','Rate Per Mille (Excluding Tax)',''.number_format(@$ratepermile,4).''),array('4','Risk Category',''.@$quoteArr[0]['RISKCATEGORY'].''),array('5','Maximum Sum insured',''.number_format(@$maximumsi).''),array('6','Top 50 lives  Sum insured',''.number_format(@$topfiftysi).''),array('7','Total Sum insured',''.number_format(@$totalsi).''),array('8','Sum Insured Base',''.@$quoteArr[0]['SITYPE'].$gradebasis.''));
  $pdf->column($columnName,$columnWidth);
  $pdf->FancyTable($columnWidth,$columnData); */
$pdf->heading('Details of Members');
$columnName = array('Sr No.', 'Basic Details', 'Particulars');
$columnWidth = array(15, 73, 102);
$columnData = array(array('1', 'Industry Type', '' . $quoteArr[0]['INDUSTRY'] . ''), array('2', 'Location', '' . $quoteArr[0]['LOCATION'] . ''), array('3', 'Policy Basis', 'Named'), array('4', 'Relationship with Insured', 'Employer-Employee'), array('5', 'Policy Type', 'Non- selective'), array('6', 'Funding Type', 'Non-Contributory'), array('7', 'Total No.of Insured Members', '' . $quoteArr[0]['MEMBERNO'] . ''), array('8', 'Rate Per Mille (Excluding Tax)', '' . number_format(@$ratepermile, 4) . ''), array('9', 'Risk Category', '' . @$quoteArr[0]['RISKCATEGORY'] . ''), array('10', 'Maximum Sum insured', '' . number_format(@$maximumsi) . ''), array('11', 'Top 50 lives  Sum insured', '' . number_format(@$topfiftysi) . ''), array('12', 'Total Sum insured', '' . number_format(@$totalsi) . ''), array('13', 'Sum Insured Base', '' . @$quoteArr[0]['SITYPE'] . $gradebasis . ''));
/* $columnData = array(array('1','Family Structure',''.htmlspecialchars_decode($locId[0]['FAMILYSTRUCTURE']).''),array('2','Total No.of Insured Members',''.$quoteArr[0]['TOTALMEMBERS'].''),array('3','Funding Type',''.htmlspecialchars_decode($quoteArr[0]['CONTRIBUTORYTYPE']).''),array('4','Age Band',''.$newborbabyval.''),array('5','Health Card',''.ucfirst(htmlspecialchars_decode($quoteArr[0]['CARDTYPE'])).''),array('6','Claim Servicing','In-House'),array('7','SI Type',''.@$oaqsiarr[0]['SITYPE'].'')); */
$pdf->column($columnName, $columnWidth);
$pdf->FancyTable($columnWidth, $columnData);
/* * **************************************** */

$pdf->heading('Table of Benefits');
$columnName = array('Sr No.', 'Coverage', 'Particulars');
$columnWidth = array(23, 83, 84);
$columnData = $arrVal;
$pdf->column($columnName, $columnWidth);
$pdf->FancyTable($columnWidth, $columnData);
/* * **************************************** */
//$pdf->AddPage();

include_once("gpatext.php");

$pdf->content1($firststr);
$gparemarks = strip_tags(@$array_tblsite[0]['GPAREMARKS']);
$pdf->content1($gparemarks);
if (@$quoteArr[0]['PROPOSALFORM'] == 'yes') {
    $pdf->AddPage();
    /*     * **************************************** */
    $pdf->headingc('Group Secure - Proposal Form');
    $pdf->content('');
    $pdf->content('gpafirst.txt');
    $pdf->heading('PROPOSER DETAILS - as per above Quote');
    $pdf->content('gpacare.txt');
    /*     * **************************************** */
    $pdf->content1('# Details of the persons to be insured and optional extensions opted are already given in the quote.');
    $pdf->heading('Past Policy and Claims Details:');
    $pdf->content1('1. Please provide the particulars for at least past 3 policy periods. If the past policy period is less than 3 years then for the complete period for which policy is availed.');
    $columnName = array('Policy period', 'Name & address', 'Policy No.', 'Total premium', 'Claim No.', 'Claim Amount', 'Lives No.', 'No. of Death Claim');
    $columnWidth = array(22, 27, 18, 25, 23, 25, 18, 32);
    $columnData = array(array('', '', '', '', '', '', '', ''), array('', '', '', '', '', '', '', ''), array('', '', '', '', '', '', '', ''), array('', '', '', '', '', '', '', ''), array('', '', '', '', '', '', '', ''));
    $pdf->column($columnName, $columnWidth);
    $pdf->FancyTable($columnWidth, $columnData);
    /*     * **************************************** */
    $pdf->content1('2. Is any of the following condition valid for your entity? If yes, provide details.');
    $columnName = array('Condition', 'Yes/ No', 'Name of Insurance Company', 'Address');
    $columnWidth = array(90, 20, 50, 30);
    $columnData = array(array('1. Declined to continue your insurance', '', '', ''), array('2. Not invited renewal of your policy', '', '', ''), array('3. Imposed any restrictions or special conditions', '', '', ''));
    $pdf->column($columnName, $columnWidth);
    $pdf->FancyTable($columnWidth, $columnData);
    /*     * **************************************** */
    $pdf->heading('MATERIAL DISCLOSURES');
    $pdf->content1('1.Any additional information relevant to the policy applied for');
    $pdf->content1('__________________________________________________________________________________________________________');
    $pdf->content1('__________________________________________________________________________________________________________');
    /*     * **************************************** */
    $pdf->content1('Signature of AuthorizedSignatory__________________________________________________________________________');
    $pdf->content1('Name & Designation________________________________________________________________________________________');
    /*     * **************************************** */
    $pdf->AddPage();
    $pdf->heading('DECLARATION');
    $pdf->content('gpadeclaration.txt');
    /*     * **************************************** */
    $pdf->AddPage();
    $pdf->headingc('STATUTORY WARNING');
    $pdf->content('gpastatutorywarning.txt');
    /*     * **************************************** */
    /*     * **************************************** */
    $pdf->headingc('Proposed Coverage and Payment Details:');
    $pdf->content('gpacoveragepayment.txt');
    /*     * **************************************** */
}
if ($quoteArr[0]['CORPORATENAME'] != '') {
    $outputFile = str_replace(" ", "-", trim(@$quoteArr[0]['CORPORATENAME']));
} else {
    $outputFile = 'quote_' . strtotime(date('d-m-y'));
}

if ($_REQUEST['email'] == 1) {
    $pdf->Output('quote/' . @$outputFile . '.pdf', 'F');
} else {
    $pdf->Output(@$outputFile . '.pdf', 'D');
}
?>