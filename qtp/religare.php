<?php

include_once("conf/company_session.php");
include_once("conf/conf.php");
include_once("conf/fucts.php");
require('pdf-Religare/fpdf.php');

class PDF extends FPDF {

    // Colored table
    function FancyTable($columnWidth, $columnData) {
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(34);
        $this->SetFont('Helvetica', '', 10);
        // Data
        $fill = false;
        for ($k = 0; $k < count($columnData); $k++) {
            for ($i = 0; $i < count($columnWidth); $i++) {
                $this->Cell($columnWidth[$i], 7, $columnData[$k][$i], 1, 0, 'L', $fill);
            }
            $this->Ln();
        }
        $this->Ln(6);
    }

    function column($columnName, $columnWidth) {
        $this->SetFillColor(0, 95, 43);
        $this->SetTextColor(255);
        $this->SetDrawColor(218);
        $this->SetLineWidth(.2);
        $this->SetFont('Helvetica', 'b', 9);
        for ($i = 0; $i < count($columnWidth); $i++)
            $this->Cell($columnWidth[$i], 9, $columnName[$i], 1, 0, 'L', true);
        $this->Ln();
    }

    function heading($heading) {
        $this->SetDrawColor(218);
        $this->SetFillColor(247);
        $this->SetTextColor(7, 101, 51);
        $this->SetFont('Helvetica', '', 12);
        $this->Cell('', 9, $heading, 1, 0, 'L', true);
        $this->Ln();
    }

    function longrow($heading) {
        $this->SetFillColor(224, 235, 255);
        $this->SetTextColor(34);
        $this->SetFont('Helvetica', '', 10);
        $fill = false;
        $this->Cell('', 9, $heading, 1, 0, 'L', $fill);
        $this->Ln();
    }

    function headingc($heading) {
        $this->SetDrawColor(218);
        $this->SetFillColor(247);
        $this->SetTextColor(7, 101, 51);
        $this->SetFont('Helvetica', '', 12);
        $this->Cell('', 9, $heading, 1, 0, 'C', true);
        $this->Ln();
    }

    function logo() {
        $this->Image('religare_logo.jpg', 10, 6, 70);
        $this->Ln(12);
    }

    function content($file) {
        $this->SetTextColor(108);
        // Read text file
        $txt = file_get_contents($file);
        // Times 12
        $this->SetFont('Helvetica', '', 9);
        // Output justified text
        $this->MultiCell(0, 5, $txt);
        // Line break
        $this->Ln(6);
    }

    function content1($txt) {
        $this->SetTextColor(108);
        // Read text file
        //$txt = file_get_contents($file);
        // Times 12
        $this->SetFont('Helvetica', '', 9);
        // Output justified text
        $this->MultiCell(0, 5, $txt);
        // Line break
        $this->Ln(6);
    }

    function spacecontent($txt) {
        $this->SetTextColor(108);
        // Read text file
        //$txt = file_get_contents($file);
        // Times 12
        $this->SetFont('Helvetica', '', 9);
        // Output justified text
        $this->MultiCell(0, 5, $txt);
        // Line break
        $this->Ln(2);
    }

    function Footer() {
        //$this->SetTextColor(108);		
        $address = OFFICEADDRESS;
        // Go to 1.5 cm from bottom
        $this->SetY(-15);
        $this->SetFillColor(0, 95, 43);
        $this->SetTextColor(255);
        $this->SetDrawColor(218);
        $this->SetLineWidth(.2);
        $this->SetFont('Helvetica', 'b', 6.8);
        $this->MultiCell(0, 3, $address, 0, 'L', true);
        $this->Ln(0);
        //$this->MultiCell(0,0,'__________________________________________________________________________________________________________________________________________________________________________');
        //$this->Cell(0,10,@$address,0,0,'C');
    }

}

$pdf = new PDF();

$pdf->AddPage();
$pdf->logo();

foreach ($_REQUEST as $key => $value) {
    $$key = $value;
}
if ((trim(@$_REQUEST["quoteId"]) == "") || (trim(@$_SESSION["userId"]) == "")) {
    session_unset();
    session_destroy();
    $url = "Location:  index.php";
    header($url);
    exit;
}
$array_tblsite = fetchListById('CRMOAQSETTINGS', 'ID', '3');
$quoteId = sanitize_data(@$_REQUEST['quoteId']);
$quoteArr = quoteDetails($quoteId);
/* echo '<pre>';
  print_r($quoteArr);
  die(); */
$distinctSiArr = getDistinctValue("SIID", "CRMOAQQUOTESUMINSURED", " WHERE QUOTEID = '$quoteId' ");
$distinctSiRoomTypeArr = getDistinctSiRoomType($quoteId);
$distinctDiseaseArr = getDistinctDisease(@$quoteArr[0]['DISEASEID']);
//echo '<pre>';
//print_r($distinctSiRoomTypeArr);//getRoomSIMapVal($siId)
$locId = fetchListCond("CRMOAQFAMILYSTRUCTURE", " WHERE ID = '" . @$quoteArr[0]['FAMILYDEFINITION'] . "' ");
if ($quoteArr[0]['CLOSESTATUS'] == 'CONVERTED') {
    $pextax = number_format($quoteArr[0]['CONVERTEDPREMIUM'], 2);
    $pextaxamt = $quoteArr[0]['CONVERTEDPREMIUM'];
} elseif ($quoteArr[0]['REQUIREDPREMIUM'] != '') {
    $pextax = number_format($quoteArr[0]['REQUIREDPREMIUM'], 2);
    $pextaxamt = $quoteArr[0]['REQUIREDPREMIUM'];
} elseif ($quoteArr[0]['ADMINUPDATEDPREMIUM'] != '') {
    $pextax = number_format($quoteArr[0]['ADMINUPDATEDPREMIUM'], 2);
    $pextaxamt = $quoteArr[0]['ADMINUPDATEDPREMIUM'];
} else {
    $pextax = number_format($quoteArr[0]['PREMIUMEXCLUDINGTAX'], 2);
    $pextaxamt = $quoteArr[0]['PREMIUMEXCLUDINGTAX'];
}
$quoteprm = $quoteArr[0]['PREMIUMEXCLUDINGTAX'];
if ($quoteprm != $pextaxamt) {
    $incper = @$pextaxamt / $quoteprm;
} else {
    $incper = '1';
}
$taxAmt = ($pextaxamt * $quoteArr[0]['SERVICETAX']) / 100;
$familyMinMembers = $locId[0]['MINIMUMMEMBERS'];
$familyMinPremium = $locId[0]['MINMEMPREMIUM'];
$totalMembers = $quoteArr[0]['TOTALMEMBERS'];

if ($quoteArr[0]['CLOSESTATUS'] == 'CONVERTED') {
    $totpremium = number_format($quoteArr[0]['CONVERTEDTOTALPREMIUM'], 2);
} elseif ($quoteArr[0]['REQUIREDPREMIUM'] != '') {
    //$taxAmt = ($quoteArr[0]['REQUIREDPREMIUM'] * $quoteArr[0]['SERVICETAX']) / 100;
    $totpremium = number_format($quoteArr[0]['REQUIREDPREMIUM'] + $taxAmt, 2);
} elseif ($quoteArr[0]['ADMINUPDATEDPREMIUM'] != '') {
    //$taxAmt = ($quoteArr[0]['ADMINUPDATEDPREMIUM'] * $quoteArr[0]['SERVICETAX']) / 100;
    $totpremium = number_format($quoteArr[0]['ADMINUPDATEDPREMIUM'] + $taxAmt, 2);
} else {
    $totpremium = number_format($quoteArr[0]['TOTALPREMIUM'], 2);
}
$ageBand = maximumAgeBand($quoteId);
$arrVal = '';
$increasePremium = round(@$quoteArr[0]['FAMILYMINMEMBERADD'] / @$quoteArr[0]['TOTALMEMBERS']);
$s = 0;
while ($s < count(@$distinctSiRoomTypeArr)) {
    $siId = $distinctSiRoomTypeArr[$s]['SIID'];
    $siArr = fetchListById('CRMSI', 'SIID', @$siId);
    $roomTypeId = $distinctSiRoomTypeArr[$s]['ROOMTYPEID'];
    $roomTypeArr = getRoomTypeDetails($roomTypeId);
    $icuLimitArr = getICULimitDetails(@$distinctSiRoomTypeArr[$s]['ICULIMITID']);
    $copayArray = fetchListCond("CRMOAQQUOTESUMINSURED", " WHERE QUOTEID = '" . @$quoteId . "' AND SIID = '" . @$siId . "' ORDER BY ID DESC");
    $copayId = @$copayArray[0]['COPAYID'];
    $copayarr = fetchListCond("CRMOAQCOPAY", " WHERE ID = '" . @$copayId . "' ");

    $roomrntlim = str_replace("percent", "%", @$roomTypeArr[0]['ROOMRENTLIMIT']);
    $roomrntlim = str_replace("per cent", "%", @$roomrntlim);
    $iculim = str_replace("percent", "%", @$icuLimitArr[0]['ICULIMIT']);
    $iculim = str_replace("per cent", "%", @$iculim);

    $arrVal[$s][] = $siArr[0]['SI'];
    $arrVal[$s][] = $roomrntlim;
    $arrVal[$s][] = $iculim ? $iculim : 'NA';
    $arrVal[$s][] = $copayarr[0]['COPAY'] ? $copayarr[0]['COPAY'] : 'NA';
    $s++;
}
/* echo '<pre>';
  print_r($arrVal);
  die(); */
$oaqsiarr = fetchListCond("CRMOAQQUOTESUMINSURED", " WHERE QUOTEID = '" . @$quoteId . "' ORDER BY ID DESC");
$siarrayval = array();
for ($siaarr = 0; $siaarr < count($oaqsiarr); $siaarr++) {
    $siarrayval[$oaqsiarr[$siaarr]['SIID']] = $oaqsiarr[$siaarr]['SITYPEREASON'];
}
/* * *********************************************************************************** */
$n = 0;
$distsiarrpre = '';
$siarrwidthpre = '';
$distsiarrpre[] = "Age Band/Sum Insured";
$siarrwidthpre[] = "40";
$siarrcolwidthpre = (150 / count($distinctSiArr));
while ($n < count(@$distinctSiArr)) {
    $resonres = '';
    if ($siarrayval[$distinctSiArr[$n]['SIID']] != '') {
        $resonres = '(' . substr($siarrayval[$distinctSiArr[$n]['SIID']], 0, 15) . ')';
    }
    $distsiarrpre[] = $distinctSiArr[$n]['SI'] . @$resonres;
//$distsiarr[] = $distinctSiArr[$n]['SI'];
    $siarrwidthpre[] = $siarrcolwidthpre;
    $n++;
}
/* * *********************************************************************************** */
$n = 0;
$distsiarr = '';
$siarrwidth = '';
$distsiarr[] = "Age Band/Sum Insured";
$siarrwidth[] = "37";
$siarrcolwidth = (120 / count($distinctSiArr));
while ($n < count(@$distinctSiArr)) {
    $resonres = '';
    if ($siarrayval[$distinctSiArr[$n]['SIID']] != '') {
        $resonres = '(' . substr($siarrayval[$distinctSiArr[$n]['SIID']], 0, 15) . ')';
    }
//$distsiarr[] = $distinctSiArr[$n]['SI'].@$resonres;
    $distsiarr[] = $distinctSiArr[$n]['SI'];
    $siarrwidth[] = $siarrcolwidth;
    $n++;
}
$distsiarr[] = "Total";
$siarrwidth[] = "33";
$agegroup_list = oaqAgeGroup();  //fetch crmage group
$z = 0;
$agegrouparr = '';
$rowarr = array();
$totalmemborres = '';
while ($z < count($agegroup_list)) { //loop to fetch CRMAGEGROUP
    $agid = $agegroup_list[$z]['AGEGROUPID'];
    //$agegrouparr[] = $agegroup_list[$z]['AGEGROUP'];
    $n = 0;
    $totalarr = array();
    while ($n < count(@$distinctSiArr)) {
        $nsiid = '';
        $nsiid = $distinctSiArr[$n]['SIID'];
        $agegrouparr[$z][0] = $agegroup_list[$z]['AGEGROUP'];
        $mapval = getMappingValues('CRMOAQQUOTESUMINSURED', 'SIID', @$nsiid, 'AGEGROUPID', @$agid, 'QUOTEID', @$quoteId);
        $agegrouparr[$z][] = @$mapval[0]['MEMBERNO'] ? @$mapval[0]['MEMBERNO'] : '0';
        $totalarr[] = @$mapval[0]['MEMBERNO'] ? @$mapval[0]['MEMBERNO'] : '0';
        $rowarr[$nsiid][] = @$mapval[0]['MEMBERNO'] ? @$mapval[0]['MEMBERNO'] : '0';
        $totalmemborres = $totalmemborres + @$mapval[0]['MEMBERNO'] ? @$mapval[0]['MEMBERNO'] : '0';
        $n++;
    }
    $agegrouparr[$z][] = array_sum($totalarr);
    $z++;
}
$n = 0;
$agegrouparr[$z][0] = "Total";
while ($n < count(@$distinctSiArr)) {
    $nsiid = '';
    $nsiid = $distinctSiArr[$n]['SIID'];
    $agegrouparr[$z][] = array_sum($rowarr[$nsiid]);
    $n++;
}
$agegrouparr[$z][] = $quoteArr[0]['TOTALMEMBERS'];
/* echo '<pre>';
  print_r($rowarr);
  die(); */
$z = 0;
$agegrouppremiumarr = '';
$totalarr = array();
while ($z < count($agegroup_list)) { //loop to fetch CRMAGEGROUP
    $agid = $agegroup_list[$z]['AGEGROUPID'];
    //$agegrouparr[] = $agegroup_list[$z]['AGEGROUP'];
    $n = 0;
    while ($n < count(@$distinctSiArr)) {
        $requiredamt = '';
        $premiumval = '';
        $REQUIREDPERCENT = '';
        $nsiid = '';
        $nsiid = $distinctSiArr[$n]['SIID'];
        $agegrouppremiumarr[$z][0] = $agegroup_list[$z]['AGEGROUP'];
        /* $mapval 			= getMappingValues('CRMOAQQUOTESUMINSURED','SIID',@$nsiid,'AGEGROUPID',@$agid,'QUOTEID',@$quoteId);		
          $PREMIUM		 	= @$mapval[0]['PREMIUM'];
          $PREMIUM		 	= round(@$PREMIUM);
          $increasePremiumVal = @$PREMIUM+@$increasePremium; */
        $mapval = getMappingValues('CRMOAQQUOTESUMINSURED', 'SIID', @$nsiid, 'AGEGROUPID', @$agid, 'QUOTEID', @$quoteId);
        $PREMIUM = @$mapval[0]['PREMIUM'] ? @$mapval[0]['PREMIUM'] : '0';

        if ($quoteArr[0]['CALCULATIONTYPE'] == 'prorata') {
            if ($totalMembers < $familyMinMembers) {
                $PREMIUM = round((@$familyMinMembers / @$totalMembers) * $PREMIUM);
            } else {
                $PREMIUM = round(@$PREMIUM);
            }
        } else {
            /* if($totalMembers<$familyMinMembers){
              $PREMIUM 	= round(@$PREMIUM+@$familyMinPremium);
              } else {
              $PREMIUM 	= round(@$PREMIUM);
              } */
            $PREMIUM = round(@$PREMIUM);
        }
        $premiumval = @$PREMIUM;
        /*
          if($quoteArr[0]['CLOSESTATUS']=='CONVERTED'){
          $premiumval = @$PREMIUM;
          }
          elseif($quoteArr[0]['REQUIREDPREMIUM']!=''){
          $REQUIREDPERCENT = @$quoteArr[0]['REQUIREDPERCENT']?@$quoteArr[0]['REQUIREDPERCENT']:'0';
          $PREMIUM		 = @$mapval[0]['PREMIUM']?@$mapval[0]['PREMIUM']:'0';
          $requiredamt = @$REQUIREDPERCENT*@$PREMIUM;
          $requiredamt = @$requiredamt/100;
          if($quoteArr[0]['REQUIREDTYPE']=='LOADING') {
          $premiumval = @$PREMIUM+@$requiredamt;
          } else {  $premiumval = @$PREMIUM-@$requiredamt; }
          } else {	$premiumval = @$PREMIUM; }
         */
        $increasePremiumVal = @$premiumval + @$increasePremium;
        $increasePremiumVal = round(@$increasePremiumVal * @$incper, 2);
        $agegrouppremiumarr[$z][] = @$increasePremiumVal ? @$increasePremiumVal : '0';
        $totalarr[] = @$increasePremiumVal ? @$increasePremiumVal : '0';
        $n++;
    }
    //$agegrouppremiumarr[$z][] 	= 	array_sum($totalarr);
    $z++;
}
$distPrePost = distinctPrePost(@$quoteArr[0]['PREPOSTID']);
$s = 0;
$prepoststr = '';
while ($s < count(@$distPrePost)) {
    $prepoststr.= $distPrePost[$s]['PREPOST'] . ' ';
    if ($s > 0) {
        $prepoststr.=', ';
    }
    $s++;
}
//$distinctDiseaseArr = getDistinctDisease(@$quoteId);

if (@$quoteArr[0]['BABYID'] > 0) {
    $newborbabyval = 'Upto 80 years';
} else {
    $newborbabyval = '3 months to 80 years';
}
$maternityArr = distinctMaternity(@$quoteArr[0]['MATERNITYID']);
$normalmaternityArr = distincNormaltMaternity(@$quoteArr[0]['NORMALMATERNITYID']);
/* echo '<pre>';
  print_r($corporatefloaterarr);
  print_r($quoteArr);
  print_r($agegrouppremiumarr);
  print_r($distsiarr);
  print_r($siarrwidth);
  die();
  $refno = $quoteArr[0]['QUOTEPARENT'];
  $copyrefno = $quoteArr[0]['QUOTEPARENTNO'];
  $reflen = strlen(@$refno);
  $copyreflen = strlen(@$copyrefno);
  $refconcat = '';
  for($ref=$reflen;$ref<8;$ref++) {
  $refconcat.='0';
  }
  $copyrefconcat = '';
  for($copyref=$copyreflen;$copyref<2;$copyref++) {
  $copyrefconcat.='0';
  }
  $finalRefNo = 'OAQ'.@$refconcat.''.@$refno.''.@$copyrefconcat.''.@$copyrefno;
 */
$finalRefNo = '';
$refnoarr = explode("_", @$quoteArr[0]['REFRENCENUMBER']);
$refno = $refnoarr[0];
$refinc = $refnoarr[1];
if ($refinc > 9) {
    $concatrefno = $refinc;
} elseif ($refinc > 0) {
    $concatrefno = '0' . $refinc;
} else {
    $concatrefno = '00';
}
$finalRefNo = $refno . $concatrefno;

/* $checkLeapYear = date('L',time());
  if($checkLeapYear==1){	$addDays = 365*24*3600;	} else {	$addDays = 364*24*3600;	} */
$pdf->heading('Group Care');
/* $pdf->heading('Group Care - Quote No: '.@$finalRefNo.' ');
  $pdf->longrow('Name of Policyholder : '.htmlspecialchars_decode(@$quoteArr[0]['CORPORATENAME']).' ');
  $pdf->longrow('Covertype : '.htmlspecialchars_decode($locId[0]['FAMILYSTRUCTURE']).' ');
  $columnWidth = array(95,95); */
if ($quoteArr[0]['POLICYTYPE'] != 'Renewal') {
    $policyStartDate = ' -- ';
    $policyEndDate = ' -- ';
} else {
    //$futureDate			=	date('d M Y', (strtotime('+1 year', $quoteArr[0]['POLICYSTARTDATE'])-86400));
    $policyStartDate = '00:00hrs ' . date('d M Y', $quoteArr[0]['POLICYSTARTDATE']);
    $policyEndDate = 'Midnight ' . date('d M Y', (strtotime('+1 year', $quoteArr[0]['POLICYSTARTDATE']) - 86400));
}
if ($quoteArr[0]['POLICYTYPE'] == 'Renewal') {
    $claimoutstanding = 'Rs. ' . @$quoteArr[0]['CLAIMAMOUNT'] . ' as on ' . date('d M Y', $quoteArr[0]['CLAIMDATE']);
    $claimoutstandingcolname = 'Claims (Paid + O/S) :';
    $expiringInsurer = $quoteArr[0]['EXPIRINGINSURERNAME'];
    $expiringInsurercolname = 'Expiring Insurer';
    $columnData = array(array('Quote No', '' . @$finalRefNo . ''), array('Name of Policy holder', '' . htmlspecialchars_decode(@$quoteArr[0]['CORPORATENAME']) . ''), array('Cover type', '' . htmlspecialchars_decode(@$quoteArr[0]['COVERTYPEVAL']) . ''), array('Policy Period -Start Date', '' . @$policyStartDate . ''), array('Policy Period - End Date', '' . @$policyEndDate . ''), array('Quote Type', '' . htmlspecialchars_decode($quoteArr[0]['POLICYTYPE']) . ''), array('Expiring Insurer', '' . $expiringInsurer . ''), array('Date of Quote Issue', '' . date('d M Y', $quoteArr[0]['CREATEDON']) . ''));
} else {
    $claimoutstandingcolname = '';
    $claimoutstanding = '';
    $columnData = array(array('Quote No', '' . @$finalRefNo . ''), array('Name of Policy holder', '' . htmlspecialchars_decode(@$quoteArr[0]['CORPORATENAME']) . ''), array('Cover type', '' . htmlspecialchars_decode(@$quoteArr[0]['COVERTYPEVAL']) . ''), array('Policy Period -Start Date', '' . @$policyStartDate . ''), array('Policy Period - End Date', '' . @$policyEndDate . ''), array('Quote Type', '' . htmlspecialchars_decode($quoteArr[0]['POLICYTYPE']) . ''), array('Date of Quote Issue', '' . date('d M Y', $quoteArr[0]['CREATEDON']) . ''));
}
$columnWidth = array(45, 145);
$pdf->FancyTable($columnWidth, $columnData);

/* $columnData = array(array('Nature of work : '.htmlspecialchars_decode($quoteArr[0]['INDUSTRYNAME']).'','Date of Quote Issue : '.date('d M Y',$quoteArr[0]['CREATEDON']).' '),array('Policy Period-Start Date : '.$policyStartDate.' ','Policy Period-End Date 	: '.@$policyEndDate.''),array('Quote Type : '.htmlspecialchars_decode($quoteArr[0]['POLICYTYPE']).'',''),array(@$claimoutstandingcolname.@$claimoutstanding,''));  
  $pdf->FancyTable($columnWidth,$columnData); */

/* * **************************************** */
$pdf->heading('Premium Details');
$columnName = array('Premium excluding Tax', 'SERVICE TAX', 'TOTAL PREMIUM');
$columnWidth = array(63, 63, 64);
$columnData = array(array('INR ' . $pextax . '', '' . $taxAmt . '', 'INR ' . $totpremium . ''));
$pdf->column($columnName, $columnWidth);
$pdf->FancyTable($columnWidth, $columnData);
/* * **************************************** */
if ($quoteArr[0]['POLICYTYPE'] == 'Renewal') {
    /*     * **************************************** */
    $pdf->heading('Expiring Claim Snap Shot');
    $columnName = array('Claim Report', 'Total Claim (Paid + O/s)', 'Premium Amount');
    $columnWidth = array(63, 63, 64);
    $columnData = array(array('' . date('d M Y', $quoteArr[0]['CLAIMDATE']) . '', 'INR ' . @$quoteArr[0]['CLAIMAMOUNT'] . '', 'INR ' . @$quoteArr[0]['PREMIUMAMOUNT'] . ''));
    $pdf->column($columnName, $columnWidth);
    $pdf->FancyTable($columnWidth, $columnData);
    $renewalstr = 'The quote is based on the claims details as mentioned above and the demography details as pasted below, if there is any change in the claims or the demography details, the quote is subject to change.


';
    $pdf->content1($renewalstr);
    /*     * **************************************** */
}
$pdf->heading('Details of Members');
$columnName = array('Sr No.', 'Basic Details', 'Particulars');
$columnWidth = array(15, 73, 102);
$columnData = array(array('1', 'Family Structure', '' . htmlspecialchars_decode($locId[0]['FAMILYSTRUCTURE']) . ''), array('2', 'Total No.of Insured Members', '' . $quoteArr[0]['TOTALMEMBERS'] . ''), array('3', 'Policy Type', 'Non Selective Policy'), array('4', 'Funding Type', '' . htmlspecialchars_decode($quoteArr[0]['CONTRIBUTORYTYPE']) . ''), array('5', 'Type of Industry', '' . htmlspecialchars_decode($quoteArr[0]['INDUSTRYNAME']) . ''), array('6', 'Relationship with Insured', 'Employer- Employees'), array('7', 'AgeBand', '' . $newborbabyval . ''), array('8', 'Health Card', '' . ucfirst(htmlspecialchars_decode($quoteArr[0]['CARDTYPE'])) . ''), array('9', 'ClaimServicing', 'In-House'));
/* $columnData = array(array('1','Family Structure',''.htmlspecialchars_decode($locId[0]['FAMILYSTRUCTURE']).''),array('2','Total No.of Insured Members',''.$quoteArr[0]['TOTALMEMBERS'].''),array('3','Funding Type',''.htmlspecialchars_decode($quoteArr[0]['CONTRIBUTORYTYPE']).''),array('4','Age Band',''.$newborbabyval.''),array('5','Health Card',''.ucfirst(htmlspecialchars_decode($quoteArr[0]['CARDTYPE'])).''),array('6','Claim Servicing','In-House'),array('7','SI Type',''.@$oaqsiarr[0]['SITYPE'].'')); */
$pdf->column($columnName, $columnWidth);
$pdf->FancyTable($columnWidth, $columnData);
/* * **************************************** */
$pdf->heading('Details of Benefit');
$columnName = array('Sr No.', 'Benefit', 'Details');
$columnWidth = array(23, 83, 84);
$columnData = array(array('1', 'In-patient Care', 'Sum Insured as per ' . @$oaqsiarr[0]['SITYPE'] . ' basis'));
$pdf->column($columnName, $columnWidth);
$pdf->FancyTable($columnWidth, $columnData);
/* * **************************************** */
$pdf->heading('Room Rent');
$columnName = array('SumInsured', 'Maximum eligibility for normal room', 'Maximum eligibility for ICU', 'CO-PAY(%)');
$columnWidth = array(20, 57, 43, 70);
$columnData = $arrVal;
$pdf->column($columnName, $columnWidth);
$pdf->FancyTable($columnWidth, $columnData);

$roomrentstr = '';
$roomrentstr.= "If the Insured Member is admitted in a room where the room rent incurred is higher than the room rent limit specified above, then the Insured Member shall bear the ratable proportion of the total Medical Expenses in the proportion of 'the room rent actually incurred-room rent limit'/'room rent actually incurred'.This shall be applicable to the total Medical Expenses (including surcharge or taxes there on)incurred during the stay in the Hospital Room.";
$pdf->content1($roomrentstr);
/* * **************************************** */
if (($quoteArr[0]['OPD'] == 'yes') || ($quoteArr[0]['HEALTHCHECKUPID'] != '')) {
    $pdf->heading('Add on Products');
    $pdf->spacecontent('');
}
if ($quoteArr[0]['OPD'] == 'yes') {
    $pdf->heading('OPD : Coverage - ' . htmlspecialchars_decode(@$quoteArr[0]['OPDCOVERAGEVAL']));
    $columnName = array('SumInsured', 'Location', 'Employee', 'Members', 'Type', 'Family', 'Co-pay');
    $columnWidth = array(30, 30, 25, 25, 25, 25, 30);
    $columnData = array(array(htmlspecialchars_decode(@$quoteArr[0]['OPDSIVAL']), htmlspecialchars_decode(@$quoteArr[0]['OPDLOCATIONVAL']), htmlspecialchars_decode(@$quoteArr[0]['OPDEMPLOYEE']), htmlspecialchars_decode(@$quoteArr[0]['OPDMEMBERS']), htmlspecialchars_decode(@$quoteArr[0]['PROPOSEDTYPE']), htmlspecialchars_decode(@$quoteArr[0]['OPDFAMILYVAL']), htmlspecialchars_decode(@$quoteArr[0]['OPDCOPAYVAL'])));
    $pdf->column($columnName, $columnWidth);
    $pdf->FancyTable($columnWidth, $columnData);
}
/* * **************************************** */
//$pdf->AddPage();
/* * **************************************** */
if ($quoteArr[0]['HEALTHCHECKUPID'] != '') {
    $columnName = array('Health Checkup', 'Members');
    $columnWidth = array(95, 95);
    $columnData = array(array(htmlspecialchars_decode(@$quoteArr[0]['HEALTHCHECKUP']), htmlspecialchars_decode(@$quoteArr[0]['HEALTHCHECKUPMEMBER'])));
    $pdf->column($columnName, $columnWidth);
    $pdf->FancyTable($columnWidth, $columnData);
}
/* * **************************************** */
include_once("first.php");
//$txt = file_get_contents('first.php');
$pdf->content1($firststr);
/* * **************************************** */
$pdf->heading('Demography');
$columnName = $distsiarr;
//print_r($columnName1);
//$columnName = array('Age Band/Sum Insured', '100000', '200000');
//print_r($columnName); 
$columnWidth = $siarrwidth;
//echo '<pre>';
//print_r($columnWidth); 
//$columnData1 = array(array('0-35','1'),array('0-35','1'),array('0-35','1'));
//print_r($columnData1);
$columnData = @$agegrouparr;
$pdf->column($columnName, $columnWidth);
$pdf->FancyTable($columnWidth, $columnData);
/* * **************************************** */
$pdf->heading('Premium per life excluding tax');
$columnName = $distsiarrpre;
$columnWidth = $siarrwidthpre;
$columnData = @$agegrouppremiumarr;
//print_r($agegrouppremiumarr);
$pdf->column($columnName, $columnWidth);
$pdf->FancyTable($columnWidth, $columnData);
/* * **************************************** */
include_once("second.php");
$pdf->content1($secondstr);
if (@$quoteArr[0]['PROPOSALFORM'] == 'yes') {
    $pdf->AddPage();
    /*     * **************************************** */
    $pdf->headingc('Group Care - Proposal Form');
    $pdf->content('care1.txt');
    /*     * **************************************** */
    $pdf->content1('1. Kindly provide the particulars for the past 3 (three) policy periods for which policy availed.');
    $columnName = array('Policy period', 'Name & address', 'Policy no.', 'Total premium', 'Claim Number', 'Claim Amount', 'Lives No', 'TPA');
    $columnWidth = array(30, 30, 20, 25, 25, 25, 20, 15);
    $columnData = array(array('', '', '', '', '', '', '', ''), array('', '', '', '', '', '', '', ''), array('', '', '', '', '', '', '', ''), array('', '', '', '', '', '', '', ''), array('', '', '', '', '', '', '', ''));
    $pdf->column($columnName, $columnWidth);
    $pdf->FancyTable($columnWidth, $columnData);
    /*     * **************************************** */
    $pdf->content1('2. Please provide detailson the following condition(s) ?');
    $columnName = array('Condition(s)applicable to your health insurance policy', 'Yes/ No', 'Name of Insurance Company', 'Address');
    $columnWidth = array(90, 20, 50, 30);
    $columnData = array(array('', '', '', ''), array('', '', '', ''), array('', '', '', ''), array('', '', '', ''), array('', '', '', ''), array('', '', '', ''));
    $pdf->column($columnName, $columnWidth);
    $pdf->FancyTable($columnWidth, $columnData);
    /*     * **************************************** */
    $pdf->heading('DECLARATION');
    $pdf->content('declaration.txt');
    /*     * **************************************** */
    $pdf->headingc('STATUTORY WARNING');
    $pdf->content('statutorywarning.txt');
    /*     * **************************************** */
}
if ($quoteArr[0]['CORPORATENAME'] != '') {
    $outputFile = str_replace(" ", "-", trim(htmlspecialchars_decode(@$quoteArr[0]['CORPORATENAME'])));
} else {
    $outputFile = 'quote_' . strtotime(date('d-m-y'));
}
if ($_REQUEST['email'] == 1) {
    $pdf->Output('quote/' . @$outputFile . '.pdf', 'F');
} else {
    $pdf->Output(@$outputFile . '.pdf', 'D');
}
?>