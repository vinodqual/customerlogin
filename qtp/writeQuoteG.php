<?php
include_once("conf/company_session.php");
include_once("conf/conf.php");
include_once("conf/fucts.php");
include_once("conf/qtpConstants.php");
include_once("allocationProcess.php");
include_once("qtpAccessMastersByRole.php");

$settingsArr = fetchListById('GPASETTINGS', 'ID', '1');
$SILIMIT = $settingsArr[0]['SILIMIT'];
$TOPFIFTYSILIMIT = $settingsArr[0]['TOPFIFTYSILIMIT'];
$TOTALSILIMIT = $settingsArr[0]['TOTALSILIMIT'];
$HIGHESTSILIMIT = $settingsArr[0]['HIGHESTSILIMIT'];

$_SESSION['quoteform'] = $_POST;
//DNS MERGING
if (@$_FILES && !empty($_FILES)) {
    $uploadDocs = "1";
}

$pProduct = sanitize_data(@$_POST['PRODUCT']);
$CORPORATENAME = sanitize_quote_data(@$_POST['CORPORATENAME']);
$locationId = sanitize_data(@$_POST['LOCATIONID']);
$locationArr = explode("_", @$locationId);
$locationId = @$locationArr[0];
$LOCATIONNAME = @$locationArr[1];
$locationTypeId = @$locationArr[2];

$industryId = sanitize_data(@$_POST['INDUSTRYID']);
$industryArr = explode("_", @$industryId);
$INDUSTRYID = $industryArr[0];
$INDUSTRYNAME = $industryArr[1];
$CAUTIONLIST = $industryArr[2];

$policyTypeId = sanitize_data(@$_POST['POLICYTYPE']);
$policyTypeArr = explode("_", @$policyTypeId);
$POLICYTYPE = @$policyTypeArr[0];
$POLICYTYPELOADDISC = @$policyTypeArr[1];

$POLICYSTRUCTURE = sanitize_data(@$_POST['POLICYSTRUCTURE']);
$POLICYSTRUCTUREArr = explode("_", $POLICYSTRUCTURE);
$POLICYSTRUCTUREID = $POLICYSTRUCTUREArr[0];
$POLICYSTRUCTURE = $POLICYSTRUCTUREArr[1];

$RMNAME = sanitize_data(@$_POST['RMNAME']);
$RMEMAIL = sanitize_data(strtolower($_POST['RMEMAIL']));
$MANDATETYPE = sanitize_data(@$_POST['MANDATETYPE']);


$SELECTINTERMEDIARY = sanitize_data(@$_POST['SELECTINTERMEDIARY']);
if ($SELECTINTERMEDIARY == 'other') {
    $SOURCENAME = sanitize_data(@$_POST['OTINTERMEDIARY']);
} else {
    $SOURCENAME = @$SELECTINTERMEDIARY;
}

$EXPIRINGINSURER = sanitize_data(@$_POST['EXPIRINGINSURER']);
$insurerArr = explode("_", @$EXPIRINGINSURER);
$EXPIRINGINSURER = @$insurerArr[0];
$EXPIRINGINSURERNAME = @$insurerArr[1];
$POLICYSTARTDATE = strtotime(@$_POST['POLICYSTARTDATE']);

$EMPLOYEENO = sanitize_data(@$_POST['EMPLOYEENO']); // no of employee
$NOOFEMPLOYEE = sanitize_data(@$_POST['NOOFEMPLOYEE']); // no of lives
$PARENTSNO = sanitize_data(@$_POST['PARENTSNO']);
$POLICYBASIS = sanitize_data(@$_POST['POLICYBASIS']);


$PREMIUMAMOUNT = sanitize_data(round(str_replace(",", "", @$_POST['PREMIUMAMOUNT'])));
$CONTRIBUTORYTYPE = sanitize_data(@$_POST['CONTRIBUTORYTYPE']);


//Only for GHI product
$CARDTYPE = sanitize_data(@$_POST['CARDTYPE']);
$FAMILYDEFINITION = sanitize_data(@$_POST['FAMILYDEFINITION']);
$familyArr = explode("_", @$FAMILYDEFINITION);
$FAMILYDEFINITION = @$familyArr[0];
$familyChkRatio = @$familyArr[1];
$FAMILYTYPE = @$familyArr[2];
$FAMILYQuote = @$familyArr[3];
$IS_WEB_REQUEST = @$_POST['IS_WEB_REQUEST']; // Field to detect whether it is a web service request or not
if (trim($IS_WEB_REQUEST) == "")
    $IS_WEB_REQUEST = false;
// server side validations
if (empty($_POST['CORPORATENAME']) || empty($_POST['NOOFEMPLOYEE']) || empty($_POST['INDUSTRYID']) || empty($_POST['LOCATIONID']) ||
        empty($_POST['FAMILYDEFINITION']) || empty($_POST['SELECTINTERMEDIARY']) || empty($_POST['RMEMAIL']) || empty($_POST['POLICYSTRUCTURE'])) {
    $location = 'quotenew.php?error=Required fields are blank for renewal case.';
} else if ($POLICYTYPE == 'Renewal' && (empty($_POST['POLICYSTARTDATE']) || empty($_POST['EXPIRINGINSURER']) || empty($_POST['PREMIUMAMOUNT']) ||
        $_POST['CLAIMAMOUNT'] == '' || empty($_POST['CLAIMDATE']))) {
    $location = 'quotenew.php?error=Fields are blank for renewal case.';
} else if ($pProduct == 'GPA' && $_POST['SITYPE'] == 'Flat' && empty($_POST['FLATSI'])) {
    $location = 'quotenew.php?error=Flat SI value is empty.';
} else if ($pProduct == 'GPA' && $_POST['SITYPE'] == 'Graded' && (empty($_POST['TOTALSI']) || empty($_POST['TOPFIFTYSI']) || empty($_POST['HIGHESTSI']) || empty($_POST['GRADEBASISID']))) {
    $location = 'quotenew.php?error=Graded SI values are empty.';
} else if ($familyChkRatio == 'YES' && empty($_POST['PARENTSNO'])) {
    $location = 'quotenew.php?error=No of parents is empty.';
} else if ($_POST['SELECTINTERMEDIARY'] != 'Direct' && empty($_POST['MANDATETYPE'])) {
    $location = 'quotenew.php?error=Mandate type is empty.';
} else if ($pProduct == 'GHI' && $_POST['EMPLOYEENO'] > $_POST['NOOFEMPLOYEE']) {
    $location = 'quotenew.php?error=No of employees must less then or equal to no of lives.';
} else {

    /*     * **************Check Min no. of lives from backend****************** */
    if ($pProduct == 'GHI') {
        $maxLivesVal = $_SESSION['maxLives'];
    } else if ($pProduct == 'GPA') {
        $maxLivesVal = $_SESSION['gpaMaxLives'];
    }

    /* _______________________________________________________________________ */

    if ($policyTypeId == 'Fresh') {
        $strlength = strlen(@$quoteId);
        $concat = '';
        for ($len = @$strlength; $len < 6; $len++) {
            $concat.= '0';
        }
        $REFRENCENUMBER = 'P' . date('y', time()) . date('m', time()) . @$concat . @$quoteId;
    } else {
        $COPYID = @$copyQuoteId;
        $copyrefarr = explode("_", @$COPYREFNO);
        $COPYREFNO1 = @$copyrefarr[0];
        $copyQuoteCount = countGpaQuoteCopy(@$COPYREFNO1);
        $incrementCount = @$copyQuoteCount;
        if ($COPYREFNO1 != '') {
            $COPYREFNOVAL = @$COPYREFNO1;
        } else {
            $COPYREFNOVAL = @$COPYREFNO;
        }
        $REFRENCENUMBER = @$COPYREFNOVAL . '_' . @$incrementCount;
    }

    /* -------------------------Claim Ratio------------------------------- */
    $CLAIMDATE = strtotime(@$_POST['CLAIMDATE']);
    $CLAIMAMOUNT = sanitize_data(round(str_replace(",", "", @$_POST['CLAIMAMOUNT'])));
    $PREMIUMAMOUNT = sanitize_data(round(str_replace(",", "", @$_POST['PREMIUMAMOUNT'])));
    $startDate = $POLICYSTARTDATE - 31536000;
    $diffDays = ($CLAIMDATE - $startDate) / 86400;
    if ($POLICYTYPE == 'Renewal') {
        $claimAmt = $CLAIMAMOUNT == 0 ? '1' : $CLAIMAMOUNT;
        $annualClaimAmt = round($claimAmt * (365 / $diffDays), 2);
        $claimRatioPercent = round((@$annualClaimAmt / @$PREMIUMAMOUNT) * 100);
        if ($pProduct == 'GPA') {
            $claimRatioIds = AccessMaster::getDropdownAccessData(PRODUCT_GPA, 'Claim Ratio');
            $crId = fetchListCondsWithColumn('QUOTETYPE', 'GPACLAIMRATIO', " WHERE $clainRationQuery MINIMUM<=" . @$claimRatioPercent . " AND MAXIMUM>=" . @$claimRatioPercent . " AND STATUS = 'ACTIVE' $claimRatioIds");
            $QUOTETYPE = @$crId[0]['QUOTETYPE'];
        }
        if ($pProduct == 'GHI') {
            $claimRatioIds = AccessMaster::getDropdownAccessData(PRODUCT_GHI, 'Claim Ratio');
            $crId = fetchListCondsWithColumn('QUOTETYPE', 'CRMOAQCLAIMRATIO', " WHERE MINIMUM<=" . @$claimRatioPercent . " AND MAXIMUM>=" . @$claimRatioPercent . " AND STATUS = 'ACTIVE' $claimRatioIds");
            $QUOTETYPE = @$crId[0]['QUOTETYPE'];
        }

        if ($pProduct == 'GTI') {
            $QUOTETYPE = 'manual'; // until GTI functionality not added
        }
    }
    /* -------------------------End Claim Ratio------------------------------- */

    /* -------------------------Parents Employee Ratio------------------------------- */
    if ($familyChkRatio == 'YES') {
        if (@$EMPLOYEENO > 0) {
            $peratio = round(@$PARENTSNO / @$EMPLOYEENO, 2);
        }
        $ratioArr = fetchListCond('CRMOAQPARENTEMPRATIO', " WHERE STARTRATIO <=  " . @$peratio . " AND ENDRATIO >=  " . @$peratio . " AND STATUS = 'ACTIVE' ");
        $peRatioId = @$ratioArr[0]['ID'];
        if (@$peRatioId < 1) {
            //echo "Parents employee ratio is not present. Please route the case to U/W team.";
            $parentEmpRatio = 'YES';
        } else {
            $parentEmpRatio = 'NO';
        }
    }
    /* -------------------------End Parents Employee Ratio------------------------------- */
    /* ---------------------------Sum Insured Type for GPA (FAC)------------------------- */
    if ($pProduct == 'GPA') {
        $SITYPE = sanitize_data(@$_POST['SITYPE']);
        $gradeBasisId = sanitize_data(@$_POST['GRADEBASISID']);
        $gradeBasisarr = explode("_", @$gradeBasisId);
        $gradeBasisId = @$gradeBasisarr[0];
        $gradeBasisVal = @$gradeBasisarr[1];

        if ($SITYPE == 'Flat') {
            $FLATSI = sanitize_data(trim(@$_POST['FLATSI']));
            $sival = $FLATSI;
        } else {
            $TOTALSI = sanitize_data(trim(@$_POST['TOTALSI']));
            $HIGHESTSI = sanitize_data(trim(@$_POST['HIGHESTSI']));
            $TOPFIFTYSI = sanitize_data(trim(@$_POST['TOPFIFTYSI']));
        }

        if ($SITYPE == 'Flat') {
            $indTopFiftySI = '';
            if ($NOOFEMPLOYEE > 50) {
                $memmultiply = '50';
            } else {
                $memmultiply = $NOOFEMPLOYEE;
            }
            $indTopFiftySI = @$sival * $memmultiply;
            $indTotalSI = @$sival * $NOOFEMPLOYEE;
            if ((@$sival > @$SILIMIT) || (@$indTopFiftySI > @$TOPFIFTYSILIMIT) || (@$indTotalSI > @$TOTALSILIMIT)) {
                $SumInsuredType = 'manual'; // "This quote requires FAC support, Please route the case to U/W team.";
            }
        } else {
            if ((@$HIGHESTSI > @$HIGHESTSILIMIT) || (@$TOTALSI > @$TOTALSILIMIT) || (@$TOPFIFTYSI > @$TOPFIFTYSILIMIT)) {
                $SumInsuredType = 'manual'; //  "This quote requires FAC support, Please route the case to U/W team.";
            }
        }
    }

    if (@$QUOTETYPE == '' && $POLICYTYPE == 'Renewal') {
        $_SESSION['claimratio'] = 'Quote cannot be created for the Claim Ratio of ' . $claimRatioPercent . '.';
        $location = 'quotenew.php?error=Quote cannot be created for the Claim Ratio of ' . $claimRatioPercent . '.';
    } else {
        if ($CONTRIBUTORYTYPE == 'Contributory' || $FAMILYQuote == 'manual' || $QUOTETYPE == 'manual' || $CAUTIONLIST == 'yes' || $NOOFEMPLOYEE > $maxLivesVal || $parentEmpRatio == 'YES' || @$SumInsuredType == 'manual') {
            $underwriterId = NULL;
            $underwriterName = NULL;
            $status = 'Pending UW Approval';
            $entryTime = time();
            if ($IS_WEB_REQUEST) {
                $arr = array('msg' => 'UW Created', 'ref_no' => '');
                echo json_encode($arr);
                exit;
            } else {
                if ($uploadDocs == '') {
                    $location = 'quotenew.php?upload=required';
                    $_SESSION['fileupload'] = "Since this case is for Under Writer, Please upload necessary documents first.";
                } else {
                    if (@$CAUTIONLIST == 'yes') {
                        $userTypeId = fetchListCondsWithColumn('ID', 'CRMOAQSOURCE', "WHERE SOURCE = 'Under-Writer'"); //Reason for this condition: multiple caution quote approvers are created.            

                        $approver = fetchListCondsWithColumn('ID, NAME', 'CRMOAQUSER', "WHERE IS_CAUTION='YES' AND TYPE=" . $userTypeId[0]['ID'] . " AND STATUS='ACTIVE' ORDER BY ID DESC");
                        $selectedUW = array('UWID' => $approver[0]['ID'], 'UWNAME' => $approver[0]['NAME']);
                    } else {
                        $selectedUW = AllocationProcess::getAllocatedUW(@$CORPORATENAME, @$NOOFEMPLOYEE);
                        if ($selectedUW == array()) {
                            $location = 'quotenew.php?error=uwerror';
                            $_SESSION['uwerror'] = 'No suitable Under-writer is available.';
                        }
                    }
                    $underwriterName = $selectedUW['UWNAME'];
                    $underwriterId = $selectedUW['UWID'];

                    /* Retrive Quoteid CODE-1988 */
                    $query = "SELECT UNDERWRITERQUOTE_SEQ.NEXTVAL FROM DUAL";
                    $sql2 = @oci_parse($conn, $query);
                    @oci_execute($sql2);
                    $row = oci_fetch_assoc($sql2);
                    $quoteId = $row["NEXTVAL"];
                    /* Retrive Quoteid CODE-1988 */

                    $modQuoteID = '000000' . $quoteId;
                    $REFRENCENUMBER = 'U' . date('y', time()) . date('m', time()) . substr($modQuoteID, -6, 6);

					try {
						if (!include('file_upload_form_submit.php')) {
                            $_SESSION['fileuploaderror'] = "Error in calling DMS API.";
							$location = 'quotenew.php';
						}
					} catch (Exception $e) {
						$eMsg = $e->getMessage();
						$eCode = $e->getCode();
						if ($eCode == 0 && trim($eMsg) == '') {
							$insertquote = "INSERT INTO UNDERWRITERQUOTE (ID,QUOTEREQUESTNO,CORPORATENAME,LOCATIONID,LOCATIONNAME,EMPLOYEENO,EXPIRINGINSURER,PREMIUMAMOUNT,INDUSTRYID,INDUSTRYNAME,POLICYTYPE,POLICYSTARTDATE,MANDATETYPE,SOURCENAME, RMNAME,RMEMAIL,POLICYSTRUCTUREID,POLICYSTRUCTURE,CARDTYPE,STATUS,CREATEDBY,CREATEDON,UPDATEDBY,UPDATEDON,PRODUCT,UNDERWRITERNAME,UNDERWRITERID,USERID,USERTYPE,USERNAME,CAUTIONLIST,REFRENCENUMBER,FAMILYDEFINITION,CLAIMAMOUNT,CLAIMDATE,CONTRIBUTORYTYPE,NOOFLIVES,PARENTSNO,SITYPE,FLATSI,TOTALSI,TOPFIFTYSI,HIGHESTSI,GRADEBASISID,GRADEBASIS,POLICYSELECTION)
					VALUES ($quoteId,$quoteId,'" . @$CORPORATENAME . "','" . @$locationId . "','" . @$LOCATIONNAME . "','" . @$EMPLOYEENO . "','" . @$EXPIRINGINSURER . "','" . @$PREMIUMAMOUNT . "','" . @$INDUSTRYID . "','" . @$INDUSTRYNAME . "','" . @$POLICYTYPE . "','" . @$POLICYSTARTDATE . "','" . @$MANDATETYPE . "','" . @$SOURCENAME . "','" . @$RMNAME . "','" . @$RMEMAIL . "','" . @$POLICYSTRUCTUREID . "','" . @$POLICYSTRUCTURE . "','" . @$CARDTYPE . "','" . @$status . "','" . @$_SESSION['userName'] . "','" . @$entryTime . "','" . @$_SESSION['userName'] . "','" . @$entryTime . "','" . $pProduct . "','" . $underwriterName . "'," . $underwriterId . ",'" . @$_SESSION['userId'] . "','" . @$_SESSION['type'] . "','" . @$USERNAME . "','$CAUTIONLIST','$REFRENCENUMBER','$FAMILYDEFINITION','" . @$CLAIMAMOUNT . "','" . @$CLAIMDATE . "','" . $CONTRIBUTORYTYPE . "','" . $NOOFEMPLOYEE . "','" . @$PARENTSNO . "','" . @$SITYPE . "','" . @$FLATSI . "','" . @$TOTALSI . "','" . @$TOPFIFTYSI . "','" . @$HIGHESTSI . "','" . @$gradeBasisId . "','" . @$gradeBasisVal . "','" . @$POLICYBASIS . "')";
							if (@oci_execute(@oci_parse($conn, $insertquote))) {
								$insertquery = "INSERT INTO QTPQUOTEAPPROVALREMARK(ID,QUOTEID,REMARKS,STATUS,USERID,USERNAME,CREATEDON) VALUES(QTPQUOTEAPPROVALREMARK_SEQ.nextval,'$quoteId','N/A','Quote Create','" . $_SESSION['userId'] . "','" . $_SESSION['userName'] . "',$entryTime)";
								$insertquery = oci_parse($conn, $insertquery);
								@oci_execute($insertquery);

								$mStatus = SendMailToRMQuoteGenerated(@$RMEMAIL, $underwriterId, $REFRENCENUMBER);

								$location = "quote_list.php?QUOTETYPE=UNDERWRITERQUOTE&POLICYTYPE=$POLICYTYPE";
							} else {die('==');
								$location = 'quotenew.php?error=uwerror';
								$_SESSION['uwerror'] = 'Unable to create quote. please try again.';
							}
						} else {
							if($eMsg == 'WEBSERVICE_TIMEOUT_ERROR')
								$_SESSION['fileuploaderror'] = 'DMS web service is down at this moment.';
							else
								$_SESSION['fileuploaderror'] = $eMsg;
							$location = 'quotenew.php?e=12';
						}
					}                    
                }
            }
        } else {
            if ($IS_WEB_REQUEST) {
                $arr = array('msg' => 'Create system', 'ref_no' => '');
                echo json_encode($arr);
                exit;
            } else {
                if ($pProduct == 'GHI') {
                    $location = 'createQuoteGHI.php';
                }
                if ($pProduct == 'GPA') {
                    $location = "createQuoteGPA.php";
                }
            }
        }
    }
}
?>

<script type="text/javascript">
    document.location = '<?php echo $location; ?>';
</script>


