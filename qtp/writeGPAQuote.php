<?php
include_once("conf/company_session.php");
include_once("conf/conf.php");
include_once("conf/fucts.php");
foreach ($_REQUEST as $key => $value) {
    $$key = $value;
}
$settingsArr = fetchListById('GPASETTINGS', 'ID', '1');
$SILIMIT = $settingsArr[0]['SILIMIT'];
$TOPFIFTYSILIMIT = $settingsArr[0]['TOPFIFTYSILIMIT'];
$TOTALSILIMIT = $settingsArr[0]['TOTALSILIMIT'];
$HIGHESTSILIMIT = $settingsArr[0]['HIGHESTSILIMIT'];
$serviceTaxArr = getServiceTax();
//pr($_POST);
$serviceTax = $serviceTaxArr[0]['TAX'];
$totalGroupMembers = '0';
$CORPORATENAME = sanitize_data($_POST['CORPORATENAME']);
$memberNo = sanitize_data(@$_POST['memberNo']);
$SITYPE = sanitize_data(@$_POST['SITYPE']);
$riskCatId = sanitize_data(@$_POST['RISKCATID']);
$riskarr = explode("_", @$riskCatId);
$riskCatId = @$riskarr[0];
$riskval = @$riskarr[1];
$activityId = sanitize_data(@$_POST['ACTIVITYID']);
$activityarr = explode("_", @$activityId);
$activityId = @$activityarr[0];
$activityval = @$activityarr[1];
$locationId = sanitize_data(@$_POST['LOCATIONID']);
$locationarr = explode("_", @$locationId);
$locationId = @$locationarr[0];
$locationval = @$locationarr[1];
$industryId = sanitize_data(@$_POST['INDUSTRYID']);
$industryarr = explode("_", @$industryId);
$industryId = @$industryarr[0];
$industryval = @$industryarr[1];
$policyBasisId = sanitize_data(@$_POST['POLICYBASISID']);
$poilicyarr = explode("_", @$policyBasisId);
$policyBasisId = @$poilicyarr[0];
$policyval = @$poilicyarr[1];
$policyBasisId = sanitize_data(@$_POST['POLICYBASISID']);
$poilicyarr = explode("_", @$policyBasisId);
$policyBasisId = @$poilicyarr[0];
$policyval = @$poilicyarr[1];
$gradeBasisId = sanitize_data(@$_POST['GRADEBASISID']);
$gradeBasisarr = explode("_", @$gradeBasisId);
$gradeBasisId = @$gradeBasisarr[0];
$gradeBasisVal = @$gradeBasisarr[1];
$policyTypeId = sanitize_data(@$_POST['POLICYTYPE']);
$poilicytypearr = explode("_", @$policyTypeId);
$POLICYTYPE = @$poilicytypearr[0];
$POLICYTYPEID = @$poilicytypearr[1];

$copyQuoteId = sanitize_data(@$_POST['copyQuoteId']);
$POLICYBASISDESC = sanitize_data(@$_POST['POLICYBASISDESC']);
$CREATETYPE = sanitize_data(@$_POST['CREATETYPE']);
$COPYREFNO = sanitize_data(@$_POST['COPYREFNO']);
$CAUTIONLIST = sanitize_data(@$_POST['CAUTIONLIST']);
$GHICOVERED = sanitize_data(@$_POST['GHICOVERED']);
$EMPCOVERED = sanitize_data(@$_POST['EMPCOVERED']);
$proposalform = sanitize_data(@$_POST['proposalform']);
$SELECTINTERMEDIARY = sanitize_data(@$_POST['SELECTINTERMEDIARY']);

$FAMILYDEFINITION = sanitize_data(@$_POST['FAMILYDEFINITION']);
$familyArr = explode("_", @$FAMILYDEFINITION);
$FAMILYDEFINITION = @$familyArr[0];
$familyChkRatio = @$familyArr[1];
$FAMILYTYPE = @$familyArr[2];
$FAMILYQuote = @$familyArr[3];

$POLICYSTRUCTURE = sanitize_data(@$_POST['POLICYSTRUCTURE']);
$POLICYSTRUCTUREArr = explode("_", $POLICYSTRUCTURE);
$POLICYSTRUCTUREID = $POLICYSTRUCTUREArr[0];
$POLICYSTRUCTURE = $POLICYSTRUCTUREArr[1];

$RMNAME = sanitize_data(@$_POST['RMNAME']);
$RMEMAIL = sanitize_data(strtolower($_POST['RMEMAIL']));
$MANDATETYPE = sanitize_data(@$_POST['MANDATETYPE']);
$pProduct = sanitize_data(@$_POST['PRODUCT']);
$USERNAME = @$_SESSION['name'];
$LEGALNAME = sanitize_data(@$_POST['LEGALNAME']);
$POLICYBASIS = sanitize_data($POLICYBASIS);
if ($SELECTINTERMEDIARY == 'other') {
    $SOURCENAME = sanitize_data(@$_POST['OTINTERMEDIARY']);
} else {
    $SOURCENAME = @$SELECTINTERMEDIARY;
}
if ($SITYPE == 'Flat') {
    $FLATSI = sanitize_data(trim(@$_POST['FLATSI']));
    $sival = $FLATSI;
} else {
    $TOTALSI = sanitize_data(trim(@$_POST['TOTALSI']));
    $HIGHESTSI = sanitize_data(trim(@$_POST['HIGHESTSI']));
    $TOPFIFTYSI = sanitize_data(trim(@$_POST['TOPFIFTYSI']));
}
$POLICYSTARTDATE = strtotime(@$_POST['POLICYSTARTDATE']);
$CLAIMDATE = strtotime(@$_POST['CLAIMDATE']);
$CLAIMAMOUNT = sanitize_data(round(str_replace(",", "", @$_POST['CLAIMAMOUNT'])));
$PREMIUMAMOUNT = sanitize_data(round(str_replace(",", "", @$_POST['PREMIUMAMOUNT'])));
$startDate = $POLICYSTARTDATE - 31536000;
$diffDays = ($CLAIMDATE - $startDate) / 86400;
if ($POLICYTYPE == 'Renewal') {
    $claimAmt = $CLAIMAMOUNT == 0 ? '1' : $CLAIMAMOUNT;
    $annualClaimAmt = round($claimAmt * (365 / $diffDays), 2);
    $claimRatioPercent = round((@$annualClaimAmt / @$PREMIUMAMOUNT) * 100);
    $crId = fetchListCondsWithColumn('LOADINGPERCENT', 'GPACLAIMRATIO', " WHERE MINIMUM<=" . @$claimRatioPercent . " AND MAXIMUM>=" . @$claimRatioPercent . " AND STATUS = 'ACTIVE' ");
    $claimRatioPerCent = @$crId[0]['LOADINGPERCENT'];
    //echo 'Claim ratio----'.$claimRatioId;
}
if ($memberNo > 0) {
    $groupMem = fetchListCondsWithColumn('LOADINGPERCENT', 'GPAGROUPSIZE', " WHERE MINIMUM<=" . @$memberNo . " AND MAXIMUM>=" . @$memberNo . " AND STATUS = 'ACTIVE' ");
    $groupMemPerCent = @$groupMem[0]['LOADINGPERCENT'];
}


if ($SITYPE == 'Flat') {
    $indTopFiftySI = '';
    if ($memberNo > 50) {
        $memmultiply = '50';
    } else {
        $memmultiply = $memberNo;
    }
    $indTopFiftySI = @$sival * $memmultiply;
    $indTotalSI = @$sival * $memberNo;
}
if ($REMARKS != '') {
    $adminStatus = 'PENDING';
} else {
    $adminStatus = 'OPEN';
}
$entryTime = time();
$adminStatus = 'OPEN'; // line inserted by wasim
$quotesiquery = "INSERT INTO GPAQUOTE(ID,CORPORATENAME,ACTIVITYID,ACTIVITY,LOCATIONID,LOCATION,SUMINSUREDID,SUMINSURED,INDUSTRYID,INDUSTRY,RISKCATID,RISKCATEGORY,POLICYBASISID,POLICYBASISDESC,POLICYBASIS,GRADEBASISID,GRADEBASIS,
BROKERAGE,MEMBERNO,USERID,SOURCEID,SOURCENAME,SITYPE,FLATSI,TOTALSI,TOPFIFTYSI,HIGHESTSI,POLICYTYPEID,POLICYTYPE,EXPIRINGINSURER,PREMIUMAMOUNT,LASTYEARLIVES,EXPIRINGTPA,CLAIMAMOUNT,CLAIMDATE,CAUTIONLIST,GHICOVERED,EMPCOVERED,REMARKS,
STATUS,ADMINSTATUS,CLOSESTATUS,COPYQUOTE,POLICYSTARTDATE,SHOWINRMPANEL,PROPOSALFORM,CREATEDBY,CREATEDON,UPDATEDBY,UPDATEDON,
QUOTEREQUESTNO, MANDATETYPE, RMNAME, RMEMAIL,POLICYSTRUCTUREID,POLICYSTRUCTURE,PRODUCT,USERTYPE,USERNAME,FAMILYDEFINITION,LEGALNAME,POLICYSELECTION) VALUES (GPAQUOTE_SEQ.nextval,'" . @$CORPORATENAME . "','" . @$activityId . "','" . @$activityval . "','" . @$locationId . "','" . @$locationval . "','" . @$siId . "','" . @$sival . "','" . @$industryId . "','" . @$industryval . "','" . @$riskCatId . "','" . @$riskval . "','" . @$policyBasisId . "','" . @$POLICYBASISDESC . "','" . @$policyval . "','" . @$gradeBasisId . "','" . @$gradeBasisVal . "','" . @$BROKERAGE . "','" . @$memberNo . "','" . @$_SESSION['userId'] . "','" . @$SOURCENAME . "','" . @$SOURCENAME . "','" . @$SITYPE . "','" . @$FLATSI . "','" . @$TOTALSI . "','" . @$TOPFIFTYSI . "','" . @$HIGHESTSI . "','" . @$POLICYTYPEID . "','" . @$POLICYTYPE . "','" . @$EXPIRINGINSURER . "','" . @$PREMIUMAMOUNT . "','" . @$LASTYEARLIVES . "','" . @$EXPIRINGTPA . "','" . @$CLAIMAMOUNT . "','" . @$CLAIMDATE . "','" . @$CAUTIONLIST . "','" . @$GHICOVERED . "','" . @$EMPCOVERED . "','" . @$REMARKS . "','" . @$adminStatus . "','" . @$adminStatus . "','" . @$adminStatus . "','" . @$copyQuoteId . "','" . @$POLICYSTARTDATE . "','yes','" . @$proposalform . "','" . @$_SESSION['userName'] . "','" . @$entryTime . "','" . @$_SESSION['userName'] . "','" . @$entryTime . "',GPAQUOTE_SEQ.nextval,'" . @$MANDATETYPE . "','" . @$RMNAME . "','" . @$RMEMAIL . "','" . @$POLICYSTRUCTUREID . "','" . @$POLICYSTRUCTURE . "','" . $pProduct . "','" . @$_SESSION['type'] . "','" . @$USERNAME . "','" . @$FAMILYDEFINITION . "','" . @$LEGALNAME . "','" . @$POLICYBASIS . "')";
//pr($_POST);
//writePremiumData($quotesiquery);//please remove. @SHAKTIRANA

$quotesiquery = @oci_parse($conn, $quotesiquery);
if (oci_execute($quotesiquery) === false) {
    if ($IS_WEB_REQUEST) {
        $arr = array('msg' => 'Something went wrong', 'ref_no' => '');
        echo json_encode($arr);
        exit;
    }
}

$check1 = @oci_parse($conn, 'SELECT GPAQUOTE_SEQ.currval FROM DUAL');
@oci_execute($check1);
$res = @oci_fetch_assoc($check1);
$quoteId = @$res['CURRVAL'];

for ($i = 0; $i < count($featurearr); $i++) {
    $featureId = '';
    $featurevar = 'OPTION' . $featurearr[$i];
    //echo '<br>';
    $featureId = $_POST[$featurevar];
//	echo 'featureId-----'.$featureId;
//	echo '<br>';
    if ($featureId > 0) {
        $coveragefeatureList = fetchListCond('GPACOVERAGEFEATURE', " WHERE STATUS = 'ACTIVE' AND FEATUREID  = '" . @$featureId . "' order by FEATUREID ASC");
//	print_r(@$coveragefeatureList);	
        if ($SITYPE == 'Flat') {
            if ($coveragefeatureList[0]['TYPE'] == 'percentsi') {
                $covpercentsi = $coveragefeatureList[0]['PERCENTSI'];
                if ($covpercentsi > 0) {
                    $applicableSI = (((@$covpercentsi / 100) * @$sival) * @$_REQUEST['memberNo']);
                }
            } else {
                $covfixedamt = $coveragefeatureList[0]['FIXEDAMOUNT'];
                if ($covfixedamt > 0) {
                    $applicableSI = @$covfixedamt * @$_REQUEST['memberNo'];
                }
            }
        } else {
            //$applicableSI	=	@$TOTALSI;
            if ($coveragefeatureList[0]['TYPE'] == 'percentsi') {
                $covpercentsi = $coveragefeatureList[0]['PERCENTSI'];
                if ($covpercentsi > 0) {
                    $applicableSI = ((@$covpercentsi / 100) * @$TOTALSI);
                }
            } else {
                $covfixedamt = $coveragefeatureList[0]['FIXEDAMOUNT'];
                if ($covfixedamt > 0) {
                    $applicableSI = @$covfixedamt * @$_REQUEST['memberNo'];
                }
            }
        }
        //echo $applicableSI.'-'.@$featurearr[$i].'-'.$riskCatId;
        //echo '<br>';
        $applicableSI = round((@$applicableSI / 100000), 2);
        //echo 'applicableSI---'.$applicableSI;
        //echo '<br>';
        $crm = fetchListCondsWithColumn('MULTIPLIER', 'GPACOVERAGEMULTIPLIER', " WHERE COVERAGEID  = '" . @$featurearr[$i] . "' AND RISKID  = '" . @$riskCatId . "' order by ID ASC ");
        $cm = @$crm[0]['MULTIPLIER'];
        //echo '<br>cm--'.$cm;
        $arm = fetchListCondsWithColumn('MULTIPLIER', 'GPAACTIVITYMULTIPLIER', " WHERE COVERAGEID  = '" . @$featurearr[$i] . "' AND RISKID  = '" . @$riskCatId . "' AND ACTIVITYID  = '" . @$activityId . "' order by ID ASC ");
        $am = @$arm[0]['MULTIPLIER'];
        //echo '<br>am--'.$am;
        $lrm = fetchListCondsWithColumn('MULTIPLIER', 'GPALOCATIONMULTIPLIER', " WHERE COVERAGEID  = '" . @$featurearr[$i] . "' AND RISKID  = '" . @$riskCatId . "' AND LOCATIONID  = '" . @$locationId . "' order by ID ASC ");
        $lm = @$lrm[0]['MULTIPLIER'];
        //echo '<br>lm--'.$lm;
        $irm = fetchListCondsWithColumn('MULTIPLIER', 'GPAINDUSTRYMULTIPLIER', " WHERE COVERAGEID  = '" . @$featurearr[$i] . "' AND RISKID  = '" . @$riskCatId . "' AND INDUSTRYID  = '" . @$industryId . "' order by ID ASC ");
        $im = @$irm[0]['MULTIPLIER'];
        //echo '<br>im--'.$im;
        $prm = fetchListCondsWithColumn('MULTIPLIER', 'GPAPOLICYMULTIPLIER', " WHERE COVERAGEID  = '" . @$featurearr[$i] . "' AND RISKID  = '" . @$riskCatId . "' AND POLICYID  = '" . @$policyBasisId . "' order by ID ASC ");
        $pm = @$prm[0]['MULTIPLIER'];
        //echo '<br>pm--'.$pm;
        $finalMultiplier = @$am * @$lm * @$im * @$pm;
        //echo '<br>finalMultiplier----'.$finalMultiplier;
        $finalRator = @$cm * @$finalMultiplier;
        //echo '<br>finalRator----'.$finalRator;
        //echo '<br>';
        $premium = @$applicableSI * @$finalRator;
        //echo '<br>premium----'.$premium;
        //echo '<br>';
        $totalPremium = @$totalPremium + @$premium;
        //echo '<br>totalPremium----'.$totalPremium;
        //echo '<br>';
        $quotesiquery = "INSERT INTO GPAQUOTECOVERAGE(ID,QUOTEID,COVERAGEID,FEATUREID,MULTIPLIER,PREMIUM,CREATEDBY,CREATEDON,UPDATEDBY,UPDATEDON) 
	VALUES (GPAQUOTECOVERAGE_SEQ.nextval,'" . @$quoteId . "','" . @$featurearr[$i] . "','" . @$featureId . "','" . @$finalMultiplier . "','" . @$premium . "','" . @$_SESSION['userName'] . "','" . @$entryTime . "','" . @$_SESSION['userName'] . "','" . @$entryTime . "')";
        writePremiumData($quotesiquery); //please remove. @SHAKTIRANA
        $quotesiquery = @oci_parse($conn, $quotesiquery);
        if (oci_execute($quotesiquery) === false) {
            if ($IS_WEB_REQUEST) {
                $arr = array('msg' => 'Something went wrong', 'ref_no' => '');
                echo json_encode($arr);
                exit;
            }
        }
    }
}
if ($claimRatioPerCent > 0) {
    $totalPremium = round(@$totalPremium * @$claimRatioPerCent);
}
if ($groupMemPerCent > 0) {
    $totalPremium = round(@$totalPremium * @$groupMemPerCent);
}
//	echo 'totalPremium---'.@$totalPremium;
//$totalPremium	=	round(@$totalPremium*@$claimRatioPerCent*@$groupMemPerCent);	
$brokeragePremium = round((@$totalPremium / (100 - @$_REQUEST['BROKERAGE'])) * 100);
// echo $brokeragePremium;die;
//	echo '<br>brokeragePremium----'.$brokeragePremium;
//	echo '<br>';
$taxAmount = (@$brokeragePremium * @$serviceTax) / 100;
//	echo 'taxAmount---'.$taxAmount 		= 	round($taxAmount);
$premiumWithTax = round(@$brokeragePremium + @$taxAmount);
$year = date('y', time());
$month = date('m', time());
if ($CREATETYPE == 'FRESH') {
    $strlength = strlen(@$quoteId);
    $concat = '';
    for ($len = @$strlength; $len < 6; $len++) {
        $concat.= '0';
    }
    $refNo = 'P' . @$year . @$month . @$concat . @$quoteId;
    $REFRENCENUMBER = @$refNo;
    $COPYID = '0';
} else {
    $COPYID = @$copyQuoteId;
    $copyrefarr = explode("_", @$COPYREFNO);
    $COPYREFNO1 = @$copyrefarr[0];
    $copyQuoteCount = countGpaQuoteCopy(@$COPYREFNO1);
    $incrementCount = @$copyQuoteCount;
    if ($COPYREFNO1 != '') {
        $COPYREFNOVAL = @$COPYREFNO1;
    } else {
        $COPYREFNOVAL = @$COPYREFNO;
    }
    $REFRENCENUMBER = @$COPYREFNOVAL . '_' . @$incrementCount;
}
$updatesql = "UPDATE GPAQUOTE SET TOTALPREMIUM = '" . @$totalPremium . "',BROKERAGEPREMIUM = '" . @$brokeragePremium . "',SERVICETAX = '" . @$serviceTax . "',TAXAMOUNT = '" . @$taxAmount . "',PREMIUMWITHTAX = '" . @$premiumWithTax . "',UPDATEDBY='" . $_SESSION['userName'] . "',UPDATEDON= '" . @$entryTime . "',COPYID= '" . @$COPYID . "',REFRENCENUMBER= '" . @$REFRENCENUMBER . "' WHERE ID = '" . $quoteId . "' ";

writePremiumData($updatesql); //please remove. @SHAKTIRANA

$updatesql = @oci_parse($conn, $updatesql);
if (oci_execute($updatesql)) {
    if ($IS_WEB_REQUEST) {
        $arr = array('msg' => 'Quote created', 'ref_no' => $REFRENCENUMBER, 'premiumamount' => $totalPremium);
        echo json_encode($arr);
        exit;
    }
} else {
    if ($IS_WEB_REQUEST) {
        $arr = array('msg' => 'Something went wrong', 'ref_no' => '');
        echo json_encode($arr);
        exit;
    }
}
unset($_SESSION['quoteform']);
?>
<script type="text/javascript">
//var r = confirm('Are you sure you want to generate quotes now.');
//if (r==true){
    document.location = "gpaQuoteDetails.php?quoteId=<?php echo @$quoteId; ?>"
//} else {
//document.location="quote_list.php?PRODUCTNAME=GPA"
//} 
</script>
