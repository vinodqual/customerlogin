<?php

/**
 * Copyright 2016 Qualtech-Consultants Pvt Ltd.
 * All rights reserved
 * @description: newquotetest.php, 2016/05/24 16:43
 * @author: Sumit Kumar <sumit.kumar@qualtech-consultants.com>
 * @updated by: Sumit Kumar 
 */
include_once('../conf/company_session_index.php');
if (!$conn) {
    $err = "QC801";
    goto error_exit;
}
if (!isset($_POST["data"])) {
    $err = "QC802";
    goto error_exit;
}
$post_string = $_POST["data"];
$enKey = "z5yK1lw7XYt6PKdP7Pne2Jw3zRkMAziH";
$enIV = "i0kbCAlFTlDXshGVCT2IxLOdJ0iWEwqK";
$partnerName = "";
$partnerId = "";
$partnerManager = "";

$baseURL="https://uat-my.religarehealthinsurance.com/qtp/";
$oaqError = "";

function encrypt($str) {
    global $enKey, $enIV;
    return base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $enKey, $str, MCRYPT_MODE_CBC, $enIV));
}

function decrypt($str) {
    global $enKey, $enIV;
    return mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $enKey, base64_decode($str), MCRYPT_MODE_CBC, $enIV);
}

//$post_string = 'WTnNnivmI/X0AivErCMWodSF35lRuUqfm6ol1FrGuyCFClpMcMZ5zK8OwAlMxr8xqxaX5UPIkCLRQIE1sx49nmjo5fs70liGLZrAJfgrEegKiFNWC8pUdGAGtebcpeyxZmm334hh/eyvYMJ0mY98djbzz9lk2risCR4ltgWBd0o=';//to get pdf 
$decrypt_data = decrypt($post_string);
$request = json_decode(trim($decrypt_data), true);
if (!$request) {
    $err = "QC803";
    goto error_exit;
}
if (!isset($request['action'])) {
    $err = "QM001";
    goto error_exit;
}
if (!in_array($request['action'], array('GETQUOTE', 'GETRATE'))) {
    $err = "QV001";
    goto error_exit;
}
$api_key = trim($request['api_key']);
$query = "SELECT u.ID, u.USERNAME,u.MANAGERID, u.NAME, u.EMAILID, u.TYPE, s.GHI, s.GPA, s.SOURCE, s.MINPERCENT, s.MAXPERCENT, s.GPAMINPERCENT, s.GPAMAXPERCENT, s.GPACASEALLOWED, s.GPAMAXDISCOUNT, s.GHIGPADISCOUNT, s.HIGHSILIMIT, s.TOPFIFTSILIMIT, u.BRANCH, u.VERTICAL, u.LEVELID, s.MINLIVES, s.MAXLIVES, s.GPAMINLIVES, s.GPAMAXLIVES, s.LOADING FROM OAQ_PARTNER_KEY k LEFT JOIN CRMOAQUSER u on u.ID=k.user_id LEFT JOIN CRMOAQSOURCE s on u.TYPE=s.ID WHERE k.API_KEY = :key";
if ($result = oci_parse($conn, $query)) {
    oci_bind_by_name($result, ":key", $api_key);
    if (oci_execute($result)) {
        if ($row = oci_fetch_assoc($result)) {
            $partnerManager = $row["MANAGERID"];
            $_SESSION["userId"] = $partnerId = $row['ID'];
            $_SESSION["userName"] = $row['USERNAME'];
            $_SESSION["name"] = $partnerName = $row['NAME'];
            $_SESSION["emailId"] = $row['EMAILID'];
            $_SESSION["typeId"] = $row['TYPE'];
            $_SESSION["GHI"] = $row['GHI'];
            $_SESSION["GPA"] = $row['GPA'];
            $_SESSION["type"] = $row['SOURCE'];
            $_SESSION["minPercent"] = $row['MINPERCENT'];
            $_SESSION["maxPercent"] = $row['MAXPERCENT'];
            $_SESSION["GPAMINPERCENT"] = $row['GPAMINPERCENT'];
            $_SESSION["GPAMAXPERCENT"] = $row['GPAMAXPERCENT'];
            $_SESSION["GPACASEALLOWED"] = $row['GPACASEALLOWED'];
            $_SESSION["GPAMAXDISCOUNT"] = $row['GPAMAXDISCOUNT'];
            $_SESSION["GHIGPADISCOUNT"] = $row['GHIGPADISCOUNT'];
            $_SESSION["HIGHSILIMIT"] = $row['HIGHSILIMIT'];
            $_SESSION["TOPFIFTSILIMIT"] = $row['TOPFIFTSILIMIT'];
            $_SESSION["branch"] = $row['BRANCH'];
            $_SESSION["vertical"] = $row['VERTICAL'];
            $_SESSION["level"] = $row['LEVELID'];
            $_SESSION["sourceminlives"] = $row['MINLIVES'];
            $_SESSION["minLives"] = $row['MINLIVES'] ? $row['MINLIVES'] : 0;
            $_SESSION["maxLives"] = $row['MAXLIVES'] ? $row['MAXLIVES'] : 500;
            $_SESSION["gpaMinLives"] = $row['GPAMINLIVES'] ? $row['GPAMINLIVES'] : 0;
            $_SESSION["gpaMaxLives"] = $row['GPAMAXLIVES'] ? $row['GPAMAXLIVES'] : 500;
            $_SESSION["sourceloading"] = $row['LOADING'];
        } else {
            $err = "QD001";
            goto error_exit;
        }
    } else {
        $err = "QC001";
        goto error_exit;
    }
} else {
    $err = "QC002";
    goto error_exit;
}
$strCookie = 'PHPSESSID=' . session_id() . '; path=/';
session_write_close();

$rmName = "";
$data_to_post = array();
$query = "SELECT EMAILID,NAME FROM CRMOAQUSER WHERE id = :param";
if ($result = oci_parse($conn, $query)) {
    oci_bind_by_name($result, ":param", $partnerManager);
    if (oci_execute($result)) {
        if ($row = oci_fetch_assoc($result)) {
            $rmName = $row['NAME'];
            $data_to_post['RMEMAIL'] = $row['EMAILID'];
        } else {
            $err = "QD002";
            goto error_exit;
        }
    } else {
        $err = "QC003";
        goto error_exit;
    }
} else {
    $err = "QC004";
    goto error_exit;
}
if ($request["action"] == "GETRATE") {
    if (!isset($request['quote_data'][0])) {
        $err = "QM002";
        goto error_exit;
    }
    if (!isset($request['api_key'])) {
        $err = "QM003";
        goto error_exit;
    }
    if (!isset($request['quote_data'][0]['product']) || empty($request['quote_data'][0]['product'])) {
        $err = "QM004";
        goto error_exit;
    }
    if (!isset($request['quote_data'][0]['corporate_name']) || empty($request['quote_data'][0]['corporate_name'])) {
        $err = "QM005";
        goto error_exit;
    }
    if (!isset($request['quote_data'][0]['no_of_lives']) || empty($request['quote_data'][0]['no_of_lives'])) {
        $err = "QM006";
        goto error_exit;
    }
    if ($request['quote_data'][0]['no_of_lives'] < 7 || $request['quote_data'][0]['no_of_lives'] > 5000) {
        $err = "QM043";
        goto error_exit;
    }
    if (!isset($request['quote_data'][0]['industry_type']) || empty($request['quote_data'][0]['industry_type'])) {
        $err = "QM007";
        goto error_exit;
    }
    if (!isset($request['quote_data'][0]['location_name']) || empty($request['quote_data'][0]['location_name'])) {
        $err = "QM008";
        goto error_exit;
    }
    if (!isset($request['quote_data'][0]['family_strucure']) || empty($request['quote_data'][0]['family_strucure'])) {
        $err = "QM009";
        goto error_exit;
    }
    if ($request['quote_data'][0]['product'] == 'GHI') {
        if (!isset($request['quote_data'][0]['no_of_employees']) || empty($request['quote_data'][0]['no_of_employees'])) {
            $err = "QM010";
            goto error_exit;
        }
        if ($request['quote_data'][0]['no_of_employees'] > $request['quote_data'][0]['no_of_lives']) {
            $err = "QM044";
            goto error_exit;
        }
    }
    if (!isset($request['quote_data'][0]['contribution_type']) || empty($request['quote_data'][0]['contribution_type'])) {
        $err = "QM011";
        goto error_exit;
    }
    if ($request['quote_data'][0]['product'] == 'GHI' || $request['quote_data'][0]['product'] == 'GPA') {
        if (!isset($request['quote_data'][0]['card_type']) || empty($request['quote_data'][0]['card_type'])) {
            $err = "QM012";
            goto error_exit;
        }
    }
    if (!isset($request['quote_data'][0]['mandate_type']) || empty($request['quote_data'][0]['mandate_type'])) {
        $err = "QM013";
        goto error_exit;
    }
    if (!isset($request['quote_data'][0]['policy_structure']) || empty($request['quote_data'][0]['policy_structure'])) {
        $err = "QM014";
        goto error_exit;
    }
    if (!isset($request['quote_data'][0]['policy_type']) || empty($request['quote_data'][0]['policy_type'])) {
        $err = "QM015";
        goto error_exit;
    }
    if ($request['quote_data'][0]['product'] == 'GHI') {
        if (!isset($request['quote_data'][0]['slabs'][0]['sum_insured']) || empty($request['quote_data'][0]['slabs'][0]['sum_insured'])) {
            $err = "QM016";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['slabs'][0]['co_pay']) || empty($request['quote_data'][0]['slabs'][0]['co_pay'])) {
            $err = "QM017";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['slabs'][0]['sum_insured_type']) || empty($request['quote_data'][0]['slabs'][0]['sum_insured_type'])) {
            $err = "QM018";
            goto error_exit;
        }
        if ($request['quote_data'][0]['slabs'][0]['sum_insured_type'] == 'Graded') {
            if (!isset($request['quote_data'][0]['slabs'][0]['grade_basis']) || empty($request['quote_data'][0]['slabs'][0]['grade_basis'])) {
                $err = "QM019";
                goto error_exit;
            }
        }
        if (!isset($request['quote_data'][0]['slabs'][0]['room_rent']) || empty($request['quote_data'][0]['slabs'][0]['room_rent'])) {
            $err = "QM020";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['slabs'][0]['icu_rent']) || empty($request['quote_data'][0]['slabs'][0]['icu_rent'])) {
            $err = "QM021";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['slabs'][0]['reason']) || empty($request['quote_data'][0]['slabs'][0]['reason'])) {
            $err = "QM022";
            goto error_exit;
        }
        if (!array_filter($request['quote_data'][0]['slabs'][0]['member_age_group'])) {
            $err = "QC111";
            goto error_exit;
        }
        $slabTotal = $request['quote_data'][0]['slabs'][0]['member_age_group']['1-35'] + $request['quote_data'][0]['slabs'][0]['member_age_group']['36-45'] + $request['quote_data'][0]['slabs'][0]['member_age_group']['46-55'] + $request['quote_data'][0]['slabs'][0]['member_age_group']['56-65'] + $request['quote_data'][0]['slabs'][0]['member_age_group']['66-70'] + $request['quote_data'][0]['slabs'][0]['member_age_group']['71-75'] + $request['quote_data'][0]['slabs'][0]['member_age_group']['76-80'] + $request['quote_data'][0]['slabs'][0]['member_age_group']['81-90'];
        if ($slabTotal != $request['quote_data'][0]['no_of_lives']) {
            $err = "QV003";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['disease_limit']) || empty($request['quote_data'][0]['disease_limit'])) {
            $err = "QM023";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['pre_post_hospitalization']) || empty($request['quote_data'][0]['pre_post_hospitalization'])) {
            $err = "QM024";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['ambulence']) || empty($request['quote_data'][0]['ambulence'])) {
            $err = "QM025";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['cover_type']) || empty($request['quote_data'][0]['cover_type'])) {
            $err = "QM026";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['brokerage']) || empty($request['quote_data'][0]['brokerage'])) {
            $err = "QM039";
            goto error_exit;
        }
        if ($request['quote_data'][0]['brokerage'] < 0 || $request['quote_data'][0]['brokerage'] > 17.5) {
            $err = "QM040";
            goto error_exit;
        }
    }
    if ($request['quote_data'][0]['policy_type'] == 'Renewal') {
        if (!isset($request['quote_data'][0]['policy_start_date']) || empty($request['quote_data'][0]['policy_start_date'])) {
            $err = "QM027";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['expiring_insurer']) || empty($request['quote_data'][0]['expiring_insurer'])) {
            $err = "QM028";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['expiring_premium']) || empty($request['quote_data'][0]['expiring_premium'])) {
            $err = "QM029";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['claim_amount']) || $request['quote_data'][0]['claim_amount'] < 0) {
            $err = "QM030";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['claim_date']) || empty($request['quote_data'][0]['claim_date'])) {
            $err = "QM031";
            goto error_exit;
        }
    }
    if ($request['quote_data'][0]['product'] == 'GPA' && $request['quote_data'][0]['sum_insured_type'] == 'Graded') {
        if (!isset($request['quote_data'][0]['highest_si']) || empty($request['quote_data'][0]['highest_si'])) {
            $err = "QM032";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['total_si']) || empty($request['quote_data'][0]['total_si'])) {
            $err = "QM033";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['top_50_live_si']) || empty($request['quote_data'][0]['top_50_live_si'])) {
            $err = "QM034";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['gpa_grade_basis']) || empty($request['quote_data'][0]['gpa_grade_basis'])) {
            $err = "QM035";
            goto error_exit;
        }
    }
    if ($request['quote_data'][0]['product'] == 'GPA') {
        if (!isset($request['quote_data'][0]['risk_class']) || empty($request['quote_data'][0]['risk_class'])) {
            $err = "QM036";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['activity']) || empty($request['quote_data'][0]['activity'])) {
            $err = "QM037";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['policy_basis']) || empty($request['quote_data'][0]['policy_basis'])) {
            $err = "QM038";
            goto error_exit;
        }
        if (!isset($request['quote_data'][0]['brokerage']) || empty($request['quote_data'][0]['brokerage'])) {
            $err = "QM041";
            goto error_exit;
        }
        if ($request['quote_data'][0]['brokerage'] < 0 || $request['quote_data'][0]['brokerage'] > 17.5) {
            $err = "QM042";
            goto error_exit;
        }
    }

    $qData = $request['quote_data'][0];
    $data_to_post['PRODUCT'] = strtoupper($qData['product']);
    if (!in_array($data_to_post['PRODUCT'], array('GHI', 'GPA', 'GTI'))) {
        $err = "QV004";
        goto error_exit;
    }
    $data_to_post['IS_WEB_REQUEST'] = 1;
    if (!calParamsforPrimary($qData)) {
        goto error_exit;
    }
    $data_to_post['CORPORATENAME'] = $qData['corporate_name'];
    $INDUSTRYNAME = $qData['industry_type'];
    if ($data_to_post['PRODUCT'] == 'GPA') {
        $query = "SELECT INDUSTRYID,IS_CAUTIONLIST FROM GPAINDUSTRY WHERE lower(INDUSTRYNAME)=:param AND STATUS = 'ACTIVE'";
    } elseif ($data_to_post['PRODUCT'] == 'GHI') {
        $query = "SELECT ID,IS_CAUTIONLIST FROM CRMOAQINDUSTRY WHERE lower(INDUSTRYNAME)=:param AND STATUS = 'ACTIVE'";
    } else {
        $query = "SELECT INDUSTRYID,IS_CAUTIONLIST FROM GTIINDUSTRY WHERE lower(INDUSTRYNAME)=:param AND STATUS = 'ACTIVE'";
    }
    if ($result = oci_parse($conn, $query)) {
        oci_bind_by_name($result, ':param', strtolower($INDUSTRYNAME));
        if (oci_execute($result)) {
            if ($row = oci_fetch_row($result)) {
                $INDUSTRYID = $row[0];
                $CAUTIONLIST = $row[1];
            } else {
                $err = "QD003";
                goto error_exit;
            }
        } else {
            $err = "QC105";
            goto error_exit;
        }
    } else {
        $err = "QC005";
        goto error_exit;
    }
    $data_to_post['INDUSTRYID'] = $INDUSTRYID . '_' . $INDUSTRYNAME . '_' . $CAUTIONLIST;
    $data_to_post['EMPLOYEENO'] = $qData['no_of_employees'];
    $data_to_post['PARENTSNO'] = $qData['no_of_parents'];

    $data_to_post['CONTRIBUTORYTYPE'] = $qData['contribution_type'];
    if (!in_array($data_to_post['CONTRIBUTORYTYPE'], array('Non-Contributory', 'Contributory'))) {
        $err = "QV006";
        goto error_exit;
    }
    $data_to_post['CARDTYPE'] = $qData['card_type'];
    if (!in_array($data_to_post['CARDTYPE'], array('ecard', 'physical'))) {
        $err = "QV007";
        goto error_exit;
    }
    $data_to_post['SELECTINTERMEDIARY'] = $partnerName;
    $data_to_post['OTINTERMEDIARY'] = "";

    $PolicyType = $qData['policy_type'];
    $query = "SELECT TYPE,LOADINGPERCENT FROM CRMOAQPOLICYTYPE WHERE lower(TYPE)=:param AND STATUS = 'ACTIVE'";
    if ($result = oci_parse($conn, $query)) {
        oci_bind_by_name($result, ":param", strtolower($PolicyType));
        if (oci_execute($result)) {
            if ($row = oci_fetch_assoc($result)) {
                $PTYPE = $row["TYPE"];
                $PLOAD = $row["LOADINGPERCENT"];
            } else {
                $err = "QD004";
                goto error_exit;
            }
        } else {
            $err = "QC006";
            goto error_exit;
        }
    } else {
        $err = "QC007";
        goto error_exit;
    }

    $data_to_post['POLICYTYPE'] = $PolicyType = $PTYPE . '_' . $PLOAD;
    $data_to_post['SITYPE'] = $qData['sum_insured_type'];
    $data_to_post['FLATSI'] = $qData['individual_si'];
    $data_to_post['HIGHESTSI'] = $qData['highest_si'];
    $data_to_post['TOTALSI'] = $qData['total_si'];
    $data_to_post['TOPFIFTYSI'] = $qData['top_50_live_si'];
    if (!empty($qData['gpa_grade_basis'])) {
        $query = "SELECT GRADEBASISID FROM GPAGRADEBASIS WHERE lower(GRADEBASIS)=:param AND STATUS = 'ACTIVE'";
        if ($result = oci_parse($conn, $query)) {
            oci_bind_by_name($result, ":param", strtolower($qData['gpa_grade_basis']));
            if (oci_execute($result)) {
                if ($row = oci_fetch_assoc($result)) {
                    $GRADEID = $row["GRADEBASISID"];
                } else {
                    $err = "QD005";
                    goto error_exit;
                }
            } else {
                $err = "QC104";
                goto error_exit;
            }
        } else {
            $err = "QC008";
            goto error_exit;
        }
        $data_to_post['GRADEBASISID'] = $GRADEID . '_' . $qData['gpa_grade_basis'];
    } else {
        $data_to_post['GRADEBASISID'] = "";
    }
    if ($PolicyType == "Renewal_1.05") {
        $data_to_post['POLICYSTARTDATE'] = $qData['policy_start_date'];
        $ExpiringInsurername = $qData['expiring_insurer'];
        $query = "SELECT ID,INSURER FROM CRMOAQINSURER WHERE lower(INSURER)=:param AND STATUS = 'ACTIVE'";
        if ($result = oci_parse($conn, $query)) {
            oci_bind_by_name($result, ":param", strtolower($ExpiringInsurername));
            if (oci_execute($result)) {
                if ($row = oci_fetch_assoc($result)) {
                    $INID = $row["ID"];
                    $INNAME = $row["INSURER"];
                } else {
                    $err = "QD006";
                    goto error_exit;
                }
            } else {
                $err = "QC009";
                goto error_exit;
            }
        } else {
            $err = "QC010";
            goto error_exit;
        }
        $data_to_post['EXPIRINGINSURER'] = $INID . '_' . $INNAME;
        $data_to_post['PREMIUMAMOUNT'] = $qData['expiring_premium'];
        $data_to_post['CLAIMAMOUNT'] = $qData['claim_amount'];
        $data_to_post['CLAIMDATE'] = $qData['claim_date'];
    }
    $data_to_post['LEGALNAME'] = "";
    $data_to_post['CREATETYPE'] = "FRESH";
} elseif ($request["action"] == "GETQUOTE") {
    $quote_ref_no = $request['quote_data']['reference_no'];
    switch ($quote_ref_no[0]) {
        case "H":
            $table_name = "CRMOAQQUOTE";
            $url = $baseURL . "religare.php?quoteId=";
            break;
        case "P":
            $table_name = "GPAQUOTE";
            $url = $baseURL . "gpareligare.php?quoteId=";
            break;
        default :
            $err = "QV008";
            goto error_exit;
    }
    $sql = "SELECT ID FROM $table_name WHERE REFRENCENUMBER = :param";
    if ($stmt = oci_parse($conn, $sql)) {
        oci_bind_by_name($stmt, ":param", strtoupper($quote_ref_no));
        if (oci_execute($stmt)) {
            if ($row = oci_fetch_row($stmt)) {
                $url = $url . $row[0];
            } else {
                $err = "QD007";
                goto error_exit;
            }
        } else {
            $err = "QC010";
            goto error_exit;
        }
    } else {
        $err = "QC011";
        goto error_exit;
    }
    $response = false;
    if ($ch = curl_init()) {
        if (curl_setopt_array($ch, array(CURLOPT_URL => $url, CURLOPT_COOKIE => $strCookie,
                    CURLOPT_RETURNTRANSFER => true, CURLOPT_CONNECTTIMEOUT => 30, CURLOPT_SSL_VERIFYPEER => false))) {
            $response = curl_exec($ch);
        }
        curl_close($ch);
    } else {
        $err = "QC012";
        goto error_exit;
    }
    if ($response !== false) {
        curl_close($ch);
        header('Content-Type: application/x-download');
        header('Content-Disposition: attachment; filename="' . $quote_ref_no . '.pdf"');
        header('Cache-Control: private, max-age=0, must-revalidate');
        die($response);
    } else {
        $err = "QC013";
        goto error_exit;
    }
} else {
    $err = "QV009";
    goto error_exit;
}
$url = $baseURL . "writeQuoteG.php";
$field_string = http_build_query($data_to_post);
// Initialize cURL
$response = false;
if ($ch = curl_init()) {
    if (curl_setopt_array($ch, array(CURLOPT_URL => $url, CURLOPT_COOKIE => $strCookie,
                CURLOPT_RETURNTRANSFER => true, CURLOPT_CONNECTTIMEOUT => 30, CURLOPT_POST => sizeof($field_string),
                CURLOPT_POSTFIELDS => $field_string, CURLOPT_SSL_VERIFYPEER => false))) {
        $response = curl_exec($ch);
    };
} else {
    $err = "QC014";
    goto error_exit;
}
if ($response !== false) {
    $rsp = json_decode($response, true);
    if (isset($rsp['msg'])) {
        if ($rsp['msg'] == "UW Created") {
            $err = "QC103";
            goto error_exit;
        } elseif ($rsp['msg'] == "Create system") {
            if (!calParamsforSecondry($qData)) {
                goto error_exit;
            }
            //$data_to_post['test']=1;
            if ($qData['product'] == 'GHI') {
                $field_string = http_build_query($data_to_post);
                $url = $baseURL . "writeQuote.php";
            } else {
                $data_to_post['BROKERAGE'] = $qData['brokerage']; // Fetch from post value

                $field_string = http_build_query($data_to_post);
                $url = $baseURL . "writeGPAQuote.php";
            }
            $response = false;
            if (curl_setopt_array($ch, array(CURLOPT_URL => $url,
                        CURLOPT_POST => sizeof($field_string), CURLOPT_POSTFIELDS => $field_string))) {
                $response = curl_exec($ch);
            }
            if ($response !== false) {
                if ($rsp = json_decode($response, true)) {
                    if ($rsp["msg"] !== "Quote created") {
                        $oaqError = $rsp["msg"];
                        $err = "QC106";
                        goto error_exit;
                    } else {
                        $pre_amount = $rsp["premiumamount"] > 3000 ? $rsp["premiumamount"] : 3000;
                        $success = '{"status": "SUCCESS",
                            "quote_data": [{
                                "Reference No.": "' . $rsp["ref_no"] . '",
                                "Premium": "' . $pre_amount . '"
                                }';
                        if ($qData['product'] == "GHI") {
                            $data_to_post['QUOTEPARENT'] = substr($rsp["ref_no"], -6);
                            $data_to_post['QUOTEPARENTNO'] = "0";
                            $data_to_post['COPYREFNO'] = $rsp["ref_no"];
                            $data_to_post['CREATETYPE'] = "COPY";
                            $data_to_post['COPYREFNUMBER'] = "";
                            $data_to_post['REFRENCENUMBER'] = $rsp["ref_no"];
                            $data_to_post['EXPIRYNUMBER'] = "1";
                            $data_to_post['clratioper'] = "";
                            $query = "SELECT * FROM CRMOAQQUOTE WHERE REFRENCENUMBER= '" . $rsp["ref_no"] . "'";
                            if ($result = oci_parse($conn, $query)) {
                                if (oci_execute($result)) {
                                    if ($row = oci_fetch_assoc($result)) {
                                        $data_to_post['copyQuoteId'] = $data_to_post['COPYID'] = $row['ID'];
                                        $data_to_post['firstQuoteCreateDate'] = $row['FIRSTQUOTECREATION'];
                                        $data_to_post['NEWPOLICYTYPE'] = $row['POLICYTYPE'];
                                        $data_to_post['NEWPOLICYSTARTDATE'] = date('Y-m-d', $row['POLICYSTARTDATE']);
                                        $data_to_post['NEWCLAIMDATE'] = date('Y-m-d', $row['CLAIMDATE']);
                                    } else {
                                        $err = "QD008";
                                        goto error_exit;
                                    }
                                } else {
                                    $err = "QC016";
                                    goto error_exit;
                                }
                            } else {
                                $err = "QC017";
                                goto error_exit;
                            }
                            $url = $baseURL . "writeQuote.php";
                        } else {
                            $data_to_post['CREATETYPE'] = "COPY";
                            $data_to_post['COPYREFNO'] = $rsp["ref_no"];
                            $query = "SELECT * FROM GPAQUOTE WHERE REFRENCENUMBER= '" . $rsp["ref_no"] . "'";
                            if ($result = oci_parse($conn, $query)) {
                                if (oci_execute($result)) {
                                    if ($row = oci_fetch_assoc($result)) {
                                        $data_to_post['copyQuoteId'] = $data_to_post['COPYID'] = $row['ID'];
                                    } else {
                                        $err = "QD009";
                                        goto error_exit;
                                    }
                                } else {
                                    $err = "QC018";
                                    goto error_exit;
                                }
                            } else {
                                $err = "QC019";
                                goto error_exit;
                            }
                            $url = $baseURL . "writeGPAQuote.php";
                        }
                        for ($i = 1; $i < count($request['quote_data']); $i++) {
                            if (!calParamsforPrimary($request['quote_data'][$i])) {
                                goto error_exit;
                            }
                            if (!calParamsforSecondry($request['quote_data'][$i])) {
                                goto error_exit;
                            }
                            $field_string = http_build_query($data_to_post);
                            $response = false;
                            if (curl_setopt_array($ch, array(CURLOPT_URL => $url,
                                        CURLOPT_POST => sizeof($field_string), CURLOPT_POSTFIELDS => $field_string))) {
                                $response = curl_exec($ch);
                            }
                            if ($response !== false) {
                                if ($rsp = json_decode($response, true)) {
                                    if ($rsp["msg"] !== "Quote created") {
                                        $oaqError = $rsp["msg"];
                                        $err = "QC107";
                                        goto error_exit;
                                    } else {
                                        $pre_amount = $rsp["premiumamount"] > 3000 ? $rsp["premiumamount"] : 3000;
                                        $success .= ',{"Reference No.": "' . $rsp["ref_no"] . '","Premium": "' . $pre_amount . '"}';
                                    }
                                } else {
                                    $err = "QC020";
                                    goto error_exit;
                                }
                            } else {
                                $err = "QC021";
                                goto error_exit;
                            }
                        }
                        if ($ch) {
                            curl_close($ch);
                        }
                        $success.=']}';
                        die(encrypt($success));
                    }
                } else {
                    $err = "QC022";
                    goto error_exit;
                }
            } else {
                $err = "QC023";
                goto error_exit;
            }
        } else {
            $err = "QC108";
            $oaqError = $rsp["msg"];
            goto error_exit;
        }
    } else {
        $err = "QC109";
        goto error_exit;
    }
} else {
    $err = "QC110";
    goto error_exit;
}

error_exit:
//log request

if (empty($conn)) {
    goto logInFile;
}
$CREATEDON = date('Y-m-d H:i');
if ($partnerId == '') {
    goto logInFile;
}
$query = "INSERT INTO QTP_PARTNER_QUOTE (ID,CORPORATENAME,PARTNERID,PARTNERNAME,REQUESTDATA,PRODUCTTYPE,RMID,RMNAME,REMARKS,STATUS,UPDATEDBY,CREATEDON,UPDATEDON) VALUES (QTP_PARTNER_QUOTE_SEQ.nextval,'" . $qData['corporate_name'] . "','$partnerId','$partnerName','$decrypt_data','" . $qData['product'] . "','$partnerManager','$rmName','','OPEN','$partnerId','$CREATEDON','$CREATEDON')";

if (!$query = oci_parse($conn, $query)) {
    goto logInFile;
}
if (!oci_execute($query)) {
    goto logInFile;
}
goto return_error;

logInFile:
if ($fObj = fopen("untrackedLog.txt", "a")) {
    fwrite($fObj, $request_de . PHP_EOL);
    fclose($fObj);
}

return_error:
if (!empty($ch)) {
    curl_close($ch);
}
if (!empty($conn)) {
    oci_close($conn);
}

$errors = array();
$errorMsg = array(101 => "Unknown Request, Record not found.", 102 => "Unable to connect with QTP system.", 103 => "Unable to connect with database.", 104 => "Request not decoded.", 105 => "Post data missing.", 106 => "Since This quote is fall in Underwriter Category, So you can not create quote right now. Our Business team will get back to you.", 107 => "There is some error, So you can not create quote right now. Our Business team will get back to you.", 108 => "Incorrect Request.",
    401 => "Action missing.", 402 => "Quote Data missing.", 403 => "Api Key missing.", 404 => "Product missing.", 405 => "Corporate Name missing.", 406 => "Number of Lives missing.", 407 => "Industry Type missing", 408 => "Location Name missing.", 409 => "Family Structure missing.", 410 => "Number of Employees missing.", 411 => "Contribution Type missing.", 412 => "Card Type missing.", 413 => "Mandate Type missing. ", 414 => "Policy Structure missing.", 415 => "Policy Type missing.", 416 => "Slab SI missing.", 417 => "Slab copay missing.", 418 => "Slab SI Type missing.", 419 => "Slab Grade basis missing.", 420 => "Slab Room Rent missing.", 421 => "Slab ICU Rent missing.", 422 => "Slab Reason missing.", 423 => "Disease Limit missing.", 424 => "Pre Post Hospitalization missing", 425 => "Ambulance missing.", 426 => "Cover Type missing.", 427 => "Policy Start Date missing.", 428 => "Expiring Insurer missing", 429 => "Expiring Premium missing.", 430 => "Claim Amount missing.", 431 => "Claim Date missing.", 432 => "Highest SI missing.", 433 => "Total SI missing.", 434 => "Top 50 Live SI missing.", 435 => "GPA Grade Basis missing.", 436 => "Risk Class missing.", 437 => "Activity missing.", 438 => "Policy Basis missing.", 439 => "Brokerage or Intermediary Commision is missing.", 440 => "Brokerage or Intermediary Commision must between 0 to 17.5.", 441 => "Number of Lives must between 7 to 5,000.", 442 => "No of employees must less then or equal to no of lives.",
    201 => "Getquote and Getrate action are allowed only.", 202 => "Total members including each age group must be equal to Number of Lives.", 203 => "Unknown Product.", 204 => "Unknown Contributory Type.", 205 => "Unknown Card Type.", 206 => "Request Id is not related to GPA/GHI product.", 207 => "Unknown Action.", 208 => "", 209 => "No rocord found for secondary.", 210 => "No rocord found for primary.",
    701 => "Api Key is not found in database.", 702 => "No RM assigned to this partner.", 703 => "Industry Name is not found in database.", 704 => "Policy Type is not found in database.", 705 => "Grade Basis Id is not found in database.", 706 => "Expiring Insurer Name is not found in database.", 707 => "Quote Id is not found in database.", 708 => "Reference Id is not found in database.", 709 => "Reference Number is not found in database.", 710 => "Location not found in database.", 711 => "Family Structure is not found in database.", 712 => "Policy Structure Name is not found in database.", 713 => "Health Checkup is not found in database.", 714 => "Baby Details is not found in database.", 715 => "Normal Maternity Amount is not found in database.", 716 => "Copay is not found in database.", 717 => "Maternity amount is not found in database.", 718 => "Room Rent Type is not found in database.", 719 => "Corporate Floater is not found in database.", 720 => "ICU Limit is not found in database.", 721 => "Disease Name is not found in database. ", 722 => "Pre post name is not found in database.", 723 => "Ambulance is not found in database.", 724 => "Cover Type is not found in database.", 725 => "sum insured is not found in database.", 726 => "Coverage is not found in database.", 727 => "OPD SI is not found in database.", 728 => "OPD Family Structure is not found in database.", 729 => "OPD copay is not found in database.", 730 => "Activity Name is not found in database.", 731 => "Risk category is not found in database.", 732 => "Policy basis is not found in database.",
    733 => "Feature value of ptd improvement is not found in database.", 734 => "Feature value of mortal remains is not found in database.", 735 => "Feature value of reconstructive surgery is not found in database.", 736 => "Feature value of marriage allowance is not found in database.", 737 => "Feature value of mobility expenses is not found in database.", 738 => "Feature value of hospital cash is not found in database.", 739 => "Feature value of funeral expenses is not found in database.", 740 => "Feature value of home modification is not found in database.", 741 => "Feature value of fracture is not found in database.", 742 => "Feature value of compensate visit is not found in database.", 743 => "Feature value of children education is not found in database.", 744 => "Feature value of burn is not found in database.", 745 => "Feature value of Ambulance Service is not found in database.", 746 => "Feature value of TTD is not found in database.", 747 => "Feature value of disappearance is not found in database.", 748 => "Required Id is not found in database.",);

$error["QC001"] = $error["QC002"] = $error["QC003"] = $error["QC004"] = $error["QC005"] = $error["QC105"] = $error["QC006"] = $error["QC007"] = $error["QC104"] = $error["QC008"] = $error["QC009"] = $error["QC010"] = $error["QC010"] = $error["QC011"] = $error["QC016"] = $error["QC017"] = $error["QC018"] = $error["QC019"] = $error["QC025"] = $error["QC026"] = $error["QC026"] = $error["QC027"] = $error["QC028"] = $error["QC029"] = $error["QC029"] = $error["QC030"] = $error["QC031"] = $error["QC032"] = $error["QC033"] = $error["QC034"] = $error["QC035"] = $error["QC036"] = $error["QC037"] = $error["QC038"] = $error["QC039"] = $error["QC040"] = $error["QC041"] = $error["QC042"] = $error["QC043"] = $error["QC044"] = $error["QC045"] = $error["QC046"] = $error["QC047"] = $error["QC048"] = $error["QC049"] = $error["QC050"] = $error["QC051"] = $error["QC052"] = $error["QC053"] = $error["QC054"] = $error["QC055"] = $error["QC056"] = $error["QC057"] = $error["QC058"] = $error["QC059"] = $error["QC060"] = $error["QC061"] = $error["QC062"] = $error["QC063"] = $error["QC064"] = $error["QC065"] = $error["QC066"] = $error["QC067"] = $error["QC068"] = $error["QC069"] = $error["QC070"] = $error["QC071"] = $error["QC072"] = $error["QC073"] = $error["QC074"] = $error["QC075"] = $error["QC076"] = $error["QC077"] = $error["QC078"] = $error["QC079"] = $error["QC080"] = $error["QC081"] = $error["QC082"] = $error["QC083"] = $error["QC084"] = $error["QC085"] = $error["QC085"] = $error["QC087"] = $error["QC088"] = $error["QC089"] = $error["QC090"] = $error["QC091"] = $error["QC092"] = $error["QC093"] = $error["QC094"] = $error["QC095"] = $error["QC096"] = $error["QC097"] = $error["QC098"] = $error["QC099"] = $error["QC100"] = $error["QC101"] = $error["QC102"] = 101;

$error["QC012"] = $error["QC013"] = $error["QC014"] = $error["QC015"] = $error["QC021"] = $error["QC110"] = $error["QC023"] = 102;

$error["QC106"] = $error["QC107"] = $error["QC020"] = $error["QC022"] = $error["QC108"] = $error["QC109"] = 107;

$error["QC801"] = 103;
$error["QC803"] = 104;
$error["QC802"] = 105;
$error["QC103"] = 106;
$error["QM001"] = 401;
$error["QV001"] = 201;
$error["QD001"] = 701;
$error["QD002"] = 702;
$error["QM002"] = 402;
$error["QM003"] = 403;
$error["QM004"] = 404;
$error["QM005"] = 405;
$error["QM006"] = 406;
$error["QM007"] = 407;
$error["QM008"] = 408;
$error["QM009"] = 409;
$error["QM010"] = 410;
$error["QM011"] = 411;
$error["QM012"] = 412;
$error["QM013"] = 413;
$error["QM014"] = 414;
$error["QM015"] = 415;
$error["QM016"] = 416;
$error["QM017"] = 417;
$error["QM018"] = 418;
$error["QM019"] = 419;
$error["QM020"] = 420;
$error["QM021"] = 421;
$error["QM022"] = 422;
$error["QC111"] = 108;
$error["QV003"] = 202;
$error["QM023"] = 423;
$error["QM024"] = 424;
$error["QM025"] = 425;
$error["QM026"] = 426;
$error["QM027"] = 427;
$error["QM028"] = 428;
$error["QM029"] = 429;
$error["QM030"] = 430;
$error["QM031"] = 431;
$error["QM032"] = 432;
$error["QM033"] = 433;
$error["QM034"] = 434;
$error["QM035"] = 435;
$error["QM036"] = 436;
$error["QM037"] = 437;
$error["QM038"] = 438;
$error["QM039"] = 439;
$error["QM040"] = 440;
$error["QM041"] = 439;
$error["QM042"] = 440;
$error["QM043"] = 441;
$error["QM044"] = 442;
$error["QV004"] = 203;
$error["QD003"] = 703;
$error["QV006"] = 204;
$error["QV007"] = 205;
$error["QD004"] = 704;
$error["QD005"] = 705;
$error["QD006"] = 706;
$error["QV008"] = 206;
$error["QD007"] = 707;
$error["QV009"] = 207;
$error["QD008"] = 708;
$error["QD009"] = 709;
$error["QD010"] = 710;
$error["QD011"] = 711;
$error["QD012"] = 712;
$error["QD013"] = 713;
$error["QD014"] = 714;
$error["QD015"] = 715;
$error["QD016"] = 716;
$error["QD017"] = 717;
$error["QD018"] = 718;
$error["QD019"] = 719;
$error["QD020"] = 720;
$error["QD021"] = 721;
$error["QD022"] = 722;
$error["QD023"] = 723;
$error["QD024"] = 724;
$error["QD025"] = 725;
$error["QD026"] = 726;
$error["QD027"] = 727;
$error["QD028"] = 728;
$error["QD029"] = 729;
$error["QD030"] = 730;
$error["QD031"] = 731;
$error["QD032"] = 732;
$error["QD033"] = 733;
$error["QD034"] = 754;
$error["QD035"] = 735;
$error["QD036"] = 736;
$error["QD037"] = 737;
$error["QD038"] = 738;
$error["QD039"] = 739;
$error["QD040"] = 740;
$error["QD041"] = 741;
$error["QD042"] = 742;
$error["QD043"] = 743;
$error["QD044"] = 744;
$error["QD045"] = 745;
$error["QD046"] = 746;
$error["QD047"] = 747;
$error["QD048"] = 748;

$res = '{"status": "ERROR",
        "error_data": {
            "error_code": "' . $err . '",
            "error_message": "' . $errorMsg[$error[$err]] . '",
            "oaq_error":"' . $oaqError . '"
        }
    }';
header("HTTP/1.1", true, 200);
//die($res);
die(encrypt($res));

function calParamsforPrimary($qData) {
    global $data_to_post, $conn, $err;
    $Product = strtoupper($qData['product']);
    $LOCATIONNAME = $qData['location_name'];
    if ($Product == 'GPA') {
        $query = "SELECT LOCATIONID, '' as LOCATIONTYPEID FROM GPALOCATION WHERE lower(LOCATION)=:param AND STATUS = 'ACTIVE'";
    } elseif ($Product == 'GHI') {
        $query = "SELECT ID, LOCATIONTYPEID FROM CRMOAQLOCATION WHERE lower(LOCATIONNAME)=:param AND STATUS = 'ACTIVE'";
    } else {
        $query = "SELECT LOCATIONID, '' as LOCATIONTYPEID FROM GTILOCATION WHERE lower(LOCATION)=:param AND STATUS = 'ACTIVE'";
    }
    if ($result = oci_parse($conn, $query)) {
        oci_bind_by_name($result, ':param', strtolower($LOCATIONNAME));
        if (oci_execute($result)) {
            if ($row = oci_fetch_row($result)) {
                $locationId = $row[0];
                $locationTypeId = $row[1];
            } else {
                $err = "QD010";
                return false;
            }
        } else {
            $err = "QC025";
            return false;
        }
    } else {
        $err = "QC026";
        return false;
    }
    $data_to_post['LOCATIONID'] = $locationId . '_' . $LOCATIONNAME . '_' . $locationTypeId;

    $data_to_post['NOOFEMPLOYEE'] = $qData['no_of_lives'];
    $FAMILYDEFINITION = $qData['family_strucure'];
    if ($Product == 'GPA') {
        $query = "SELECT ID,CHECKRATIO,QUOTETYPE,FAMILYSTRUCTURE FROM CRMGPAFAMILYSTRUCTURE WHERE lower(FAMILYSTRUCTURE)=:param AND STATUS = 'ACTIVE'";
    } elseif ($Product == 'GHI') {
        $query = "SELECT ID,CHECKRATIO,QUOTETYPE,FAMILYSTRUCTURE FROM CRMOAQFAMILYSTRUCTURE WHERE lower(FAMILYSTRUCTURE)=:param AND STATUS = 'ACTIVE'";
    } else {
        $query = "SELECT ID,CHECKRATIO,QUOTETYPE,FAMILYSTRUCTURE FROM CRMQTPGTIFAMILYSTRUCTURE WHERE lower(FAMILYSTRUCTURE)=:param AND STATUS = 'ACTIVE'";
    }
    if ($result = oci_parse($conn, $query)) {
        oci_bind_by_name($result, ":param", strtolower($FAMILYDEFINITION));
        if (oci_execute($result)) {
            if ($row = oci_fetch_assoc($result)) {
                $ID = $row["ID"];
                $CHECKRATIO = $row["CHECKRATIO"];
                $FAMILYSTRUCTURE = $row["FAMILYSTRUCTURE"];
                $QUOTETYPE = $row["QUOTETYPE"];
            } else {
                $err = "QD011";
                return false;
            }
        } else {
            $err = "QC026";
            return false;
        }
    } else {
        $err = "QC027";
        return false;
    }
    $data_to_post['FAMILYDEFINITION'] = $ID . '_' . $CHECKRATIO . '_' . ucfirst($FAMILYSTRUCTURE) . '_' . $QUOTETYPE;
    $data_to_post['MANDATETYPE'] = $qData['mandate_type'];
    $POLICYSTRUCTURENAME = $qData['policy_structure'];
    $query = "SELECT ID FROM CRMQTPPOLICYSTRUCTURE WHERE lower(POLICYSTR)=:param AND STATUS = 'ACTIVE'";
    if ($result = oci_parse($conn, $query)) {
        oci_bind_by_name($result, ":param", strtolower($POLICYSTRUCTURENAME));
        if (oci_execute($result)) {
            if ($row = oci_fetch_assoc($result)) {
                $PSTRID = $row["ID"];
            } else {
                $err = "QD012";
                return false;
            }
        } else {
            $err = "QC028";
            return false;
        }
    } else {
        $err = "QC029";
        return false;
    }
    $data_to_post['POLICYSTRUCTURE'] = $PSTRID . '_' . $POLICYSTRUCTURENAME;

    $data_to_post['INTERMEDIARYCOMMISSION'] = $qData['brokerage']; // Fetch from post value
    return true;
}

function calParamsforSecondry($qData) {
    global $data_to_post, $conn, $err;
    $Product = strtoupper($qData['product']);
    $data_to_post['proposalform'] = $qData['proposal_form'];
    $data_to_post['CARDAMOUNT'] = '10';

    if ($Product == 'GHI') {
        $data_to_post['WAITINGPERIOD'] = $qData['waiting_period'];
        $data_to_post['PEDEXCLUSION'] = $qData['ped_exclusion'];

        $HEALTHCHECKUP = trim($qData['health_checkup']);
        if ($HEALTHCHECKUP != '') {
            $query = "SELECT ID,MULTIPLIER,HEALTHCHECKUP,HEALTHCHECKUPFILE FROM OPDHEALTHCHECKUP WHERE lower(HEALTHCHECKUP) = :param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($HEALTHCHECKUP));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $HEALTHID = $row["ID"];
                        $MULTIPLIER = $row["MULTIPLIER"];
                        $HEALTHCHECKUP = $row["HEALTHCHECKUP"];
                        $HEALTHCHECKUPFILE = $row["HEALTHCHECKUPFILE"];
                    } else {
                        $err = "QD013";
                        return false;
                    }
                } else {
                    $err = "QC029";
                    return false;
                }
            } else {
                $err = "QC030";
                return false;
            }
            $data_to_post['HEALTHCHECKUPID'] = $HEALTHID . '|' . $MULTIPLIER . '|' . $HEALTHCHECKUP . '|' . $HEALTHCHECKUPFILE;
        }
        $data_to_post['HEALTHCHECKUPNUMBER'] = $qData['health_checkup_member'];

        $NEWBORNBABY = $qData['new_born_baby_coverage'];
        $data_to_post['NEWBABY1'] = 'yes';
        if (empty($NEWBORNBABY)) {
            $data_to_post['NEWBABY1'] = 'no';
        }
        if ($data_to_post['NEWBABY1'] == 'yes') {
            $query = "SELECT ID,BABY FROM CRMOAQNEWBORNBABY WHERE lower(BABY) = :param AND VISIBLEALL = 'yes' AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($NEWBORNBABY));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $BABYID = $row["ID"];
                        $BABYAMOUNT = $row["BABY"];
                    } else {
                        $err = "QD014";
                        return false;
                    }
                } else {
                    $err = "QC031";
                    return false;
                }
            } else {
                $err = "QC032";
                return false;
            }
            $data_to_post['BABYAMOUNT1'] = $BABYID . '_' . $BABYAMOUNT;
        }

        $NORMALMATERNITYAMOUNT = $qData['normal_maternity_amount'];
        $data_to_post['NORMALMATERNITY1'] = 'yes';
        if (empty($NORMALMATERNITYAMOUNT)) {
            $data_to_post['NORMALMATERNITY1'] = 'no';
        }
        if ($data_to_post['NORMALMATERNITY1'] == 'yes') {
            $query = "SELECT ID,NORMAL FROM CRMOAQNORMALMATERNITY WHERE lower(NORMAL)=:param AND STATUS = 'ACTIVE' AND VISIBLEALL= 'yes'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($NORMALMATERNITYAMOUNT));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $NORMALID = $row["ID"];
                        $AMOUNT = $row["NORMAL"];
                    } else {
                        $err = "QD015";
                        return false;
                    }
                } else {
                    $err = "QC033";
                    return false;
                }
            } else {
                $err = "QC034";
                return false;
            }
            $data_to_post['NORMALMATERNITYTYPE1'] = $NORMALID . '_' . $AMOUNT;
        }

        $copay1 = $qData['slabs'][0]['co_pay'];
        $query = "SELECT ID,COPAY FROM CRMOAQCOPAY WHERE lower(COPAY)=:param AND STATUS = 'ACTIVE' AND VISIBLEALL= 'yes'";
        if ($result = oci_parse($conn, $query)) {
            oci_bind_by_name($result, ":param", strtolower($copay1));
            if (oci_execute($result)) {
                if ($row = oci_fetch_assoc($result)) {
                    $COPAY = $row["ID"];
                } else {
                    $err = "QD016";
                    return false;
                }
            } else {
                $err = "QC035";
                return false;
            }
        } else {
            $err = "QC036";
            return false;
        }
        $data_to_post['copay1'] = $COPAY;
        $Maternityamount = $qData['c_section_maternity_amount'];
        $data_to_post['MATERNITY1'] = 'yes';
        if (empty($Maternityamount)) {
            $data_to_post['MATERNITY1'] = 'no';
        }
        if ($qData['MATERNITY1'] == 'yes') {
            $query = "SELECT ID,CAESERIAN FROM CRMOAQMATERNITY WHERE lower(CAESERIAN)=:param AND STATUS = 'ACTIVE' AND VISIBLEALL= 'yes'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($Maternityamount));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $MATID = $row["ID"];
                        $MATAMOUNT = $row["CAESERIAN"];
                    } else {
                        $err = "QD017";
                        return false;
                    }
                } else {
                    $err = "QC037";
                    return false;
                }
            } else {
                $err = "QC038";
                return false;
            }
            $data_to_post['MATERNITYTYPE1'] = $MATID . '_' . $MATAMOUNT;
        }
        $ROOMRENTTYPE = $qData['slabs'][0]['room_rent'];

        $query = "SELECT ID FROM CRMOAQROOMTYPE WHERE lower(TYPE)=:param AND STATUS = 'ACTIVE' AND VISIBLEALL= 'yes'";
        if ($result = oci_parse($conn, $query)) {
            oci_bind_by_name($result, ":param", strtolower($ROOMRENTTYPE));
            if (oci_execute($result)) {
                if ($row = oci_fetch_assoc($result)) {
                    $ROOMTYPEID = $row["ID"];
                } else {
                    $err = "QD018";
                    return false;
                }
            } else {
                $err = "QC039";
                return false;
            }
        } else {
            $err = "QC040";
            return false;
        }
        $data_to_post['ROOMTYPE1'] = $ROOMTYPEID;
        $CFAMOUNT = $qData['cf_amount'];
        $data_to_post['CORPORATEFLOATER1'] = 'yes';
        if (empty($CFAMOUNT)) {
            $data_to_post['CORPORATEFLOATER1'] = 'no';
        }
        if ($data_to_post['CORPORATEFLOATER1'] == 'yes') {
            $query = "SELECT ID,CORPORATEFLOATER FROM CRMOAQCORPORATEFLOATER WHERE CORPORATEFLOATER=:param AND STATUS = 'ACTIVE' AND VISIBLEALL= 'yes'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", $CFAMOUNT);
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $CFID = $row["ID"];
                        $CFAMOUNT = $row["CORPORATEFLOATER"];
                    } else {
                        $err = "QD019";
                        return false;
                    }
                } else {
                    $err = "QC041";
                    return false;
                }
            } else {
                $err = "QC042";
                return false;
            }
            $data_to_post['CFMOUNT1'] = $CFID . '_' . $CFAMOUNT;
        }
        $ICULIMIT = $qData['slabs'][0]['icu_rent'];
        $query = "SELECT ID FROM CRMOAQICULIMIT WHERE lower(ICULIMIT)=:param AND STATUS = 'ACTIVE' AND VISIBLEALL= 'yes'";
        if ($result = oci_parse($conn, $query)) {
            oci_bind_by_name($result, ":param", strtolower($ICULIMIT));
            if (oci_execute($result)) {
                if ($row = oci_fetch_assoc($result)) {
                    $CFID = $row["ID"];
                } else {
                    $err = "QD020";
                    return false;
                }
            } else {
                $err = "QC043";
                return false;
            }
        } else {
            $err = "QC044";
            return false;
        }
        $data_to_post['ICULIMIT1'] = $CFID;
        $diseaseIdlimit = $qData['disease_limit'];
        $query = "SELECT ID,DISEASENAME FROM CRMOAQDISEASE WHERE lower(DISEASENAME)=:param AND STATUS = 'ACTIVE'";
        if ($result = oci_parse($conn, $query)) {
            oci_bind_by_name($result, ":param", strtolower($diseaseIdlimit));
            if (oci_execute($result)) {
                if ($row = oci_fetch_assoc($result)) {
                    $DISEASEID = $row["ID"];
                    $DISEASENAME = $row["DISEASENAME"];
                } else {
                    $err = "QD021";
                    return false;
                }
            } else {
                $err = "QC045";
                return false;
            }
        } else {
            $err = "QC046";
            return false;
        }
        $data_to_post['diseaseId1'] = $DISEASEID . '_' . $DISEASENAME;
        $PrePost = $qData['pre_post_hospitalization'];
        $query = "SELECT ID,PREPOST FROM CRMOAQPREPOSTHOSPITALIZATION WHERE lower(PREPOST)=:param AND STATUS = 'ACTIVE' AND VISIBLEALL= 'yes'";
        if ($result = oci_parse($conn, $query)) {
            oci_bind_by_name($result, ":param", strtolower($PrePost));
            if (oci_execute($result)) {
                if ($row = oci_fetch_assoc($result)) {
                    $PREPOSTID = $row["ID"];
                    $PREPOSTNAME = $row["PREPOST"];
                } else {
                    $err = "QD022";
                    return false;
                }
            } else {
                $err = "QC047";
                return false;
            }
        } else {
            $err = "QC048";
            return false;
        }
        $data_to_post['prePostId1'] = $PREPOSTID . '_' . $PREPOSTNAME;
        //$slabmembers = $qData['slabs'][0]['member_age_group'];
        //$array = json_decode(json_encode($slabmembers), True);
        $data_to_post['slab1_101'] = $qData['slabs'][0]['member_age_group']['1-35'];
        $data_to_post['slab1_102'] = $qData['slabs'][0]['member_age_group']['36-45'];
        $data_to_post['slab1_103'] = $qData['slabs'][0]['member_age_group']['46-55'];
        $data_to_post['slab1_104'] = $qData['slabs'][0]['member_age_group']['56-65'];
        $data_to_post['slab1_105'] = $qData['slabs'][0]['member_age_group']['66-70'];
        $data_to_post['slab1_106'] = $qData['slabs'][0]['member_age_group']['71-75'];
        $data_to_post['slab1_107'] = $qData['slabs'][0]['member_age_group']['76-80'];
        $data_to_post['slab1_108'] = $qData['slabs'][0]['member_age_group']['81-90'];
        $data_to_post['slab1[0]'] = '101';
        $data_to_post['slab1[1]'] = '102';
        $data_to_post['slab1[2]'] = '103';
        $data_to_post['slab1[3]'] = '104';
        $data_to_post['slab1[4]'] = '105';
        $data_to_post['slab1[5]'] = '106';
        $data_to_post['slab1[6]'] = '107';
        $data_to_post['slab1[7]'] = '108';
        $data_to_post['slabNo'] = '1';

        $query = "SELECT ID FROM CRMOAQAMBULANCE WHERE AMBULANCE=:param AND STATUS = 'ACTIVE' AND VISIBLEALL= 'yes'";
        if ($result = oci_parse($conn, $query)) {
            oci_bind_by_name($result, ":param", $qData['ambulence']);
            if (oci_execute($result)) {
                if ($row = oci_fetch_assoc($result)) {
                    $AMBULANCEID = $row["ID"];
                } else {
                    $err = "QD023";
                    return false;
                }
            } else {
                $err = "QC049";
                return false;
            }
        } else {
            $err = "QC050";
            return false;
        }
        $data_to_post['ambulanceId1'] = $AMBULANCEID . '_' . $qData['ambulence'];

        $GHICOVERTYPE = $qData['cover_type'];
        $query = "SELECT ID,COVERTYPE FROM CRMOAQCOVERTYPE WHERE lower(COVERTYPE)=:param AND STATUS = 'ACTIVE'";
        if ($result = oci_parse($conn, $query)) {
            oci_bind_by_name($result, ":param", strtolower($GHICOVERTYPE));
            if (oci_execute($result)) {
                if ($row = oci_fetch_assoc($result)) {
                    $COVERTYPEID = $row["ID"];
                    $COVERTYPENAME = $row["COVERTYPE"];
                } else {
                    $err = "QD024";
                    return false;
                }
            } else {
                $err = "QC051";
                return false;
            }
        } else {
            $err = "QC052";
            return false;
        }
        $data_to_post['GHICOVERTYPE1'] = $COVERTYPEID . '_' . $COVERTYPENAME;

        $data_to_post['GHIPREPOSTNATAL1'] = $qData['PrePost_Natal'];
        $data_to_post['PREPOSTNATAL1'] = 'yes';
        if (empty($data_to_post['GHIPREPOSTNATAL1'])) {
            $data_to_post['PREPOSTNATAL1'] = 'no';
        }
        $data_to_post['SITYPE1'] = $qData['slabs'][0]['sum_insured_type'];
        if ($data_to_post['SITYPE1'] == 'Graded') {
            $data_to_post['gradeBaisisId1'] = $qData['slabs'][0]['grade_basis'];
            $data_to_post['SITYPEREMARKS1'] = $qData['slabs'][0]['reason'];
        }
        $query = "SELECT SIID FROM CRMSI WHERE SI=:param AND STATUS = 'ACTIVE' AND VISIBLEALL='yes'";
        if ($result = oci_parse($conn, $query)) {
            oci_bind_by_name($result, ":param", $qData['slabs'][0]['sum_insured']);
            if (oci_execute($result)) {
                if ($row = oci_fetch_assoc($result)) {
                    $data_to_post['SUMINSURED1'] = $row["SIID"];
                } else {
                    $err = "QD025";
                    return false;
                }
            } else {
                $err = "QC053";
                return false;
            }
        } else {
            $err = "QC054";
            return false;
        }
        $data_to_post['PROPOSEDTYPE'] = $qData['opd_proposed_type'];

        if (!empty($qData['opd_coverage'])) {
            $CoverageId = $qData['opd_coverage'];
            $query = "SELECT ID,COVERAGE,SHOWSUMINSURED FROM OPDCOVERAGE WHERE lower(COVERAGE)=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($CoverageId));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $COVERAGEID = $row["ID"];
                        $COVERAGE = $row["COVERAGE"];
                        $SHOWSUMINSURED = $row["SHOWSUMINSURED"];
                    } else {
                        $err = "QD026";
                        return false;
                    }
                } else {
                    $err = "QC055";
                    return false;
                }
            } else {
                $err = "QC056";
                return false;
            }
            $data_to_post['OPDCOVERAGEID'] = $COVERAGEID . '_' . ucfirst($COVERAGE) . '_' . $SHOWSUMINSURED;
        } else {
            $data_to_post['OPDCOVERAGEID'] = "";
        }

        if (!empty($qData['opd_sum_insured'])) {
            $OpdSi = $qData['opd_sum_insured'];
            $query = "SELECT ID,SI FROM OPDSI WHERE SI=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", $OpdSi);
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $OPDSIID = $row["ID"];
                        $OPDSI = $row["SI"];
                    } else {
                        $err = "QD027";
                        return false;
                    }
                } else {
                    $err = "QC057";
                    return false;
                }
            } else {
                $err = "QC058";
                return false;
            }
            $data_to_post['OPDSIID'] = $OPDSIID . '_' . $OPDSI;
        } else {
            $data_to_post['OPDSIID'] = "";
        }

        $data_to_post['OPDEMPLOYEE'] = isset($qData['opd_no_of_employee']) ? $qData['opd_no_of_employee'] : "";
        $data_to_post['OPDMEMBERS'] = isset($qData['opd_no_of_member']) ? $qData['opd_no_of_member'] : "";

        if (!empty($qData['opd_family_strucure'])) {
            $OpdFamily = $qData['opd_family_strucure'];
            $query = "SELECT ID,MULTIPLIER,FAMILYSTRUCTURE FROM OPDFAMILYSTRUCTURE WHERE lower(FAMILYSTRUCTURE)=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($OpdFamily));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $OPDFAMILYID = $row["ID"];
                        $OPDFAMILYSTRUCTURE = $row["FAMILYSTRUCTURE"];
                        $OPDFAMILYMULTIPLIER = $row["MULTIPLIER"];
                    } else {
                        $err = "QD028";
                        return false;
                    }
                } else {
                    $err = "QC059";
                    return false;
                }
            } else {
                $err = "QC060";
                return false;
            }
            $data_to_post['OPDFAMILYID'] = $OPDFAMILYID . '_' . ucfirst($OPDFAMILYSTRUCTURE) . '_' . $OPDFAMILYMULTIPLIER;
        } else {
            $data_to_post['OPDFAMILYID'] = "";
        }

        if (!empty($qData['opd_co_pay'])) {
            $OpdCopay = $qData['opd_co_pay'];
            $query = "SELECT ID,COPAY,MULTIPLIER FROM OPDCOPAY WHERE COPAY=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", $OpdCopay);
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $OPDCOPAYID = $row["ID"];
                        $OPDCOPAY = $row["FAMILYSTRUCTURE"];
                        $OPDCOPAYMULTIPLIER = $row["MULTIPLIER"];
                    } else {
                        $err = "QD029";
                        return false;
                    }
                } else {
                    $err = "QC061";
                    return false;
                }
            } else {
                $err = "QC062";
                return false;
            }
            $data_to_post['OPDCOPAYID'] = $OPDCOPAYID . '_' . ucfirst($OPDCOPAY) . '_' . $OPDCOPAYMULTIPLIER;
        } else {
            $data_to_post['OPDCOPAYID'] = "";
        }
    }
    if ($Product == 'GPA') {
        $data_to_post['GHICOVERED'] = $qData['ghi_covered'];
        $data_to_post['memberNo'] = $qData['no_of_employees'];
        $Activity = $qData['activity'];
        $query = "SELECT ACTIVITYID,ACTIVITYNAME FROM GPAACTIVITY WHERE lower(ACTIVITYNAME)=:param AND STATUS = 'ACTIVE'";
        if ($result = oci_parse($conn, $query)) {
            oci_bind_by_name($result, ":param", strtolower($Activity));
            if (oci_execute($result)) {
                if ($row = oci_fetch_assoc($result)) {
                    $ACTIVITYID = $row["ACTIVITYID"];
                    $ACTIVITYNAME = $row["ACTIVITYNAME"];
                } else {
                    $err = "QD030";
                    return false;
                }
            } else {
                $err = "QC063";
                return false;
            }
        } else {
            $err = "QC064";
            return false;
        }
        $data_to_post['ACTIVITYID'] = $ACTIVITYID . '_' . $ACTIVITYNAME;
        $RiskClass = $qData['risk_class'];
        $query = "SELECT RISKCATEGORYID,RISKCATEGORY FROM GPARISKCATEGORY WHERE lower(RISKCATEGORY)=:param AND STATUS = 'ACTIVE'";

        if ($result = oci_parse($conn, $query)) {
            oci_bind_by_name($result, ":param", strtolower($RiskClass));
            if (oci_execute($result)) {
                if ($row = oci_fetch_assoc($result)) {
                    $RISKCATEGORYID = $row["RISKCATEGORYID"];
                    $RISKCATEGORY = $row["RISKCATEGORY"];
                } else {
                    $err = "QD031";
                    return false;
                }
            } else {
                $err = "QC065";
                return false;
            }
        } else {
            $err = "QC066";
            return false;
        }
        $data_to_post['RISKCATID'] = $RISKCATEGORYID . '_' . ucfirst($RISKCATEGORY);
        $PolicyBasis = $qData['policy_basis'];
        if ($PolicyBasis != "") {
            $query = "SELECT POLICYBASISID,POLICYBASIS FROM GPAPOLICYBASIS WHERE lower(POLICYBASIS)=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($PolicyBasis));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $POLICYBASISID = $row["POLICYBASISID"];
                        $POLICYBASIS = $row["POLICYBASIS"];
                    } else {
                        $err = "QD032";
                        return false;
                    }
                } else {
                    $err = "QC067";
                    return false;
                }
            } else {
                $err = "QC068";
                return false;
            }
            $data_to_post['POLICYBASISID'] = $POLICYBASISID . '_' . ucfirst($POLICYBASIS);
			$data_to_post['POLICYBASISDESC'] = $qData['policy_basis_desc'];
        } else {
            $data_to_post['POLICYBASISDESC'] = $qData['policy_basis_desc'];
        }
        $PPD = $qData['coverage']['ppd'];
        $PTD = $qData['coverage']['ptd'];
        $DEATH = $qData['coverage']['death'];
        if ($PPD == true) {
            $data_to_post['OPTION41'] = '82';
        } else {
            $data_to_post['OPTION41'] = '0';
        }
        if ($PTD == true) {
            $data_to_post['OPTION2'] = '83';
        } else {
            $data_to_post['OPTION2'] = '0';
        }
        if ($DEATH == true) {
            $data_to_post['OPTION1'] = '6';
        } else {
            $data_to_post['OPTION1'] = '0';
        }
        if (!empty($qData['coverage']['medex'])) {
            $data_to_post['OPTION108'] = $qData['coverage']['medex'];
        }
        if (!empty($qData['coverage']['acc_hosp'])) {
            $data_to_post['OPTION102'] = $qData['coverage']['acc_hosp'];
        }

        $PTDIMPROVEMENT = $qData['coverage']['ptd_improvement'];
        if (!empty($PTDIMPROVEMENT)) {
            $query = "SELECT FEATUREID,VALUE FROM GPACOVERAGEFEATURE WHERE lower(VALUE)=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($PTDIMPROVEMENT));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $FIDPTDIMP = $row["FEATUREID"];
                        $VALUE = $row["VALUE"];
                    } else {
                        $err = "QD033";
                        return false;
                    }
                } else {
                    $err = "QC069";
                    return false;
                }
            } else {
                $err = "QC070";
                return false;
            }
            $data_to_post['OPTION91'] = $FIDPTDIMP;
        } else {
            $data_to_post['OPTION91'] = "";
        }
        if ($qData['coverage']['ppd_improvement'] == true) {
            $data_to_post['OPTION90'] = '6';
        } else {
            $data_to_post['OPTION90'] = '0';
        }
        $MortalRemains = $qData['coverage']['mortal_remains'];
        if (!empty($MortalRemains)) {
            $query = "SELECT FEATUREID,VALUE FROM GPACOVERAGEFEATURE WHERE lower(VALUE)=:param AND STATUS = 'ACTIVE'";

            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($MortalRemains));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $FIDMORTAL = $row["FEATUREID"];
                    } else {
                        $err = "QD034";
                        return false;
                    }
                } else {
                    $err = "QC071";
                    return false;
                }
            } else {
                $err = "QC072";
                return false;
            }
            $data_to_post['OPTION93'] = $FIDMORTAL;
        } else {
            $data_to_post['OPTION93'] = "";
        }
        $ReconstructiveSurgary = $qData['coverage']['reconstructive_surgery'];
        if (!empty($ReconstructiveSurgary)) {
            $query = "SELECT FEATUREID,VALUE FROM GPACOVERAGEFEATURE WHERE lower(VALUE)=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($ReconstructiveSurgary));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $FIDSURGARY = $row["FEATUREID"];
                    } else {
                        $err = "QD035";
                        return false;
                    }
                } else {
                    $err = "QC073";
                    return false;
                }
            } else {
                $err = "QC074";
                return false;
            }
            $data_to_post['OPTION92'] = $FIDSURGARY;
        } else {
            $data_to_post['OPTION92'] = "";
        }
        $MARIEGEALLOWANCE = $qData['coverage']['marriage_allowance'];
        if (!empty($MARIEGEALLOWANCE)) {
            $query = "SELECT FEATUREID,VALUE FROM GPACOVERAGEFEATURE WHERE lower(VALUE)=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($MARIEGEALLOWANCE));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $FIDMARIEGE = $row["FEATUREID"];
                    } else {
                        $err = "QD036";
                        return false;
                    }
                } else {
                    $err = "QC075";
                    return false;
                }
            } else {
                $err = "QC076";
                return false;
            }
            $data_to_post['OPTION88'] = $FIDMARIEGE;
        } else {
            $data_to_post['OPTION88'] = "";
        }
        $MobilityExt = $qData['coverage']['mobility_extension'];
        if (!empty($MobilityExt)) {
            $query = "SELECT FEATUREID,VALUE FROM GPACOVERAGEFEATURE WHERE lower(VALUE)=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($MobilityExt));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $FIDMOBILITY = $row["FEATUREID"];
                    } else {
                        $err = "QD037";
                        return false;
                    }
                } else {
                    $err = "QC077";
                    return false;
                }
            } else {
                $err = "QC078";
                return false;
            }
            $data_to_post['OPTION89'] = $FIDMOBILITY;
        } else {
            $data_to_post['OPTION89'] = "";
        }
        $HospitalCash = $qData['coverage']['hospital_cash_allowance'];
        if (!empty($HospitalCash)) {
            $query = "SELECT FEATUREID,VALUE FROM GPACOVERAGEFEATURE WHERE lower(VALUE)=:param AND STATUS = 'ACTIVE'";

            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($HospitalCash));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $FIDHOSPITAL = $row["FEATUREID"];
                    } else {
                        $err = "QD038";
                        return false;
                    }
                } else {
                    $err = "QC079";
                    return false;
                }
            } else {
                $err = "QC080";
                return false;
            }
            $data_to_post['OPTION87'] = $FIDHOSPITAL;
        } else {
            $data_to_post['OPTION87'] = "";
        }
        $FuneralExp = $qData['coverage']['funeral_expenses'];
        if (!empty($FuneralExp)) {
            $query = "SELECT FEATUREID,VALUE FROM GPACOVERAGEFEATURE WHERE lower(VALUE)=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($FuneralExp));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $FIDFUNERAL = $row["FEATUREID"];
                    } else {
                        $err = "QD039";
                        return false;
                    }
                } else {
                    $err = "QC081";
                    return false;
                }
            } else {
                $err = "QC082";
                return false;
            }
            $data_to_post['OPTION85'] = $FIDFUNERAL;
        } else {
            $data_to_post['OPTION85'] = "";
        }
        $HomeModification = $qData['coverage']['home_modification'];
        if (!empty($HomeModification)) {
            $query = "SELECT FEATUREID,VALUE FROM GPACOVERAGEFEATURE WHERE lower(VALUE)=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($HomeModification));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $FIDHOME = $row["FEATUREID"];
                    } else {
                        $err = "QD040";
                        return false;
                    }
                } else {
                    $err = "QC083";
                    return false;
                }
            } else {
                $err = "QC084";
                return false;
            }
            $data_to_post['OPTION86'] = $FIDHOME;
        } else {
            $data_to_post['OPTION86'] = "";
        }
        $Fracture = $qData['coverage']['fracture'];
        if (!empty($Fracture)) {
            $query = "SELECT FEATUREID,VALUE FROM GPACOVERAGEFEATURE WHERE lower(VALUE)=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($Fracture));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $FIDFRACTURE = $row['FEATUREID'];
                    } else {
                        $err = "QD041";
                        return false;
                    }
                } else {
                    $err = "QC085";
                    return false;
                }
            } else {
                $err = "QC085";
                return false;
            }
            $data_to_post['OPTION84'] = $FIDFRACTURE;
        } else {
            $data_to_post['OPTION84'] = "";
        }
        $CompansateVisit = $qData['coverage']['compassionate_visit'];
        if (!empty($CompansateVisit)) {
            $query = "SELECT FEATUREID,VALUE FROM GPACOVERAGEFEATURE WHERE lower(VALUE)=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($CompansateVisit));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $FIDVISIT = $row['FEATUREID'];
                    } else {
                        $err = "QD042";
                        return false;
                    }
                } else {
                    $err = "QC087";
                    return false;
                }
            } else {
                $err = "QC088";
                return false;
            }
            $data_to_post['OPTION82'] = $FIDVISIT;
        } else {
            $data_to_post['OPTION82'] = "";
        }
        $ChildrenEducation = $qData['coverage']['children_education'];
        if (!empty($ChildrenEducation)) {
            $query = "SELECT FEATUREID,VALUE FROM GPACOVERAGEFEATURE WHERE lower(VALUE)=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($ChildrenEducation));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $FIDEDUCATION = $row['FEATUREID'];
                    } else {
                        $err = "QD043";
                        return false;
                    }
                } else {
                    $err = "QC089";
                    return false;
                }
            } else {
                $err = "QC090";
                return false;
            }
            $data_to_post['OPTION81'] = $FIDEDUCATION;
        } else {
            $data_to_post['OPTION81'] = "";
        }
        $Burn = $qData['coverage']['burns'];
        if (!empty($Burn)) {
            $query = "SELECT FEATUREID,VALUE FROM GPACOVERAGEFEATURE WHERE lower(VALUE)=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($Burn));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $FIDBURN = $row['FEATUREID'];
                    } else {
                        $err = "QD044";
                        return false;
                    }
                } else {
                    $err = "QC091";
                    return false;
                }
            } else {
                $err = "QC092";
                return false;
            }
            $data_to_post['OPTION63'] = $FIDBURN;
        } else {
            $data_to_post['OPTION63'] = "";
        }
        $AmbulanceService = $qData['coverage']['ambulance_service'];
        if (!empty($AmbulanceService)) {
            $query = "SELECT FEATUREID,VALUE FROM GPACOVERAGEFEATURE WHERE lower(VALUE)=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($AmbulanceService));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $FIDAMBULANCE = $row['FEATUREID'];
                    } else {
                        $err = "QD045";
                        return false;
                    }
                } else {
                    $err = "QC093";
                    return false;
                }
            } else {
                $err = "QC094";
                return false;
            }
            $data_to_post['OPTION62'] = $FIDAMBULANCE;
        } else {
            $data_to_post['OPTION62'] = "";
        }
        $TTD = $qData['coverage']['ttd'];
        if ($TTD == true) {
            $data_to_post['OPTION3'] = '22';
        } else {
            $data_to_post['OPTION3'] = '0';
        }
        /*if (!empty($TTD)) {
            $query = "SELECT FEATUREID,VALUE FROM GPACOVERAGEFEATURE WHERE lower(VALUE)=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($TTD));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $FIDTTD = $row['FEATUREID'];
                    } else {
                        $err = "QD046";
                        return false;
                    }
                } else {
                    $err = "QC095";
                    return false;
                }
            } else {
                $err = "QC096";
                return false;
            }
            $data_to_post['OPTION3'] = $FIDTTD;
        } else {
            $data_to_post['OPTION3'] = "";
        }*/
        $Disaparense = $qData['coverage']['disappearance'];
        if (!empty($Disaparense)) {
            $query = "SELECT FEATUREID,VALUE FROM GPACOVERAGEFEATURE WHERE lower(VALUE)=:param AND STATUS = 'ACTIVE'";
            if ($result = oci_parse($conn, $query)) {
                oci_bind_by_name($result, ":param", strtolower($Disaparense));
                if (oci_execute($result)) {
                    if ($row = oci_fetch_assoc($result)) {
                        $FIDDISAPARENSE = $row['FEATUREID'];
                    } else {
                        $err = "QD047";
                        return false;
                    }
                } else {
                    $err = "QC097";
                    return false;
                }
            } else {
                $err = "QC098";
                return false;
            }
            $data_to_post['OPTION83'] = $FIDDISAPARENSE;
        } else {
            $data_to_post['OPTION83'] = "";
        }
        $featurearray = array();
        // $CovId = array();
        // $morecoverageList = fetchListCond('GPACOVERAGE', " WHERE STATUS = 'ACTIVE' AND PRIORITY != 'yes' order by UPDATEDON DESC");
        $query = "SELECT * FROM GPACOVERAGE WHERE STATUS = 'ACTIVE' AND PRIORITY != 'yes' order by UPDATEDON DESC";
        if ($result = oci_parse($conn, $query)) {
            if (oci_execute($result)) {
                $i = 0;
                while ($row = oci_fetch_assoc($result)) {
                    foreach ($row as $key => $value) {
                        $CovrageId[$i][$key] = $value;
                    }
                    $i++;
                }
            } else {
                $err = "QC099";
                return false;
            }
        } else {
            $err = "QC100";
            return false;
        }
        $yh = 0;
        while (count($CovrageId) > $yh) {
            $covId = $CovrageId[$yh]['COVERAGEID'];

            $featurearray[] = $covId;
            $yh++;
        }
        $data_to_post['featurearr'] = $featurearray;


        $query = "SELECT SILIMIT,TOPFIFTYSILIMIT,TOTALSILIMIT,HIGHESTSILIMIT FROM GPASETTINGS WHERE ID='1'";
        if ($result = oci_parse($conn, $query)) {
            if (oci_execute($result)) {
                if ($row = oci_fetch_assoc($result)) {
                    $data_to_post['SILIMIT'] = $row['SILIMIT'];
                    $data_to_post['TOPFIFTYSILIMIT'] = $row['TOPFIFTYSILIMIT'];
                    $data_to_post['TOTALSILIMIT'] = $row['TOTALSILIMIT'];
                    $data_to_post['HIGHESTSILIMIT'] = $row['HIGHESTSILIMIT'];
                } else {
                    $err = "QD048";
                    return false;
                }
            } else {
                $err = "QC101";
                return false;
            }
        } else {
            $err = "QC102";
            return false;
        }
    }
    return true;
}
