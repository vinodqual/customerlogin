<?php if(!isset($_REQUEST["get"])) $_REQUEST["get"]="req";
if($_REQUEST["get"]=="req"){
    $req=array(
        "action"=>"GETRATE",
        "quote_data"=>array(array(
            "corporate_name"=>"Test Shingal Industries",
            "product"=>"GHI",       // GHI, GTI, GPA
            "industry_type"=>"Services",     //Master A1
            "location_name"=>"Delhi",     //Master A2
            "no_of_employees"=>400, // {GHI}
            "no_of_parent"=>100, // {GHI}
            "no_of_lives"=>500,
            "family_strucure"=>"NO_Self Only",   //Master A3
            "contribution_type"=>"Non-Contributory",    //Non-Contributory, Contributory
            "card_type"=>"ecard",       // ecard, physical  {GHI}
            "mandate_type"=>"SINGLE",   // SINGLE, MULTIPLE
            "policy_structure"=>"Employee-Employer",      // Master A4
            "policy_type"=>"Fresh",     // Fresh, Renewal
            "sum_insured_type"=>"Flat", // Flat, Graded {GPA}
            "individual_si"=>10000,       // {GPA, Flat}
            "total_si"=>10000,       // {GPA, Graded}
            "top_50_live_si"=>10000,    // {GPA, Graded}
            "highest_si"=>10000,   // {GPA, Graded}
            "gpa_grade_basis"=>10000,   // {GPA, Graded}
            "policy_start_date"=>"",
            "expiring_insurer"=>"",
            "expiring_premium"=>1000,
            "claim_amount"=>10000,
            "claim_date"=>"",
//=================================================================
            "health_checkup"=>"Set 1",              //Master A17
            "health_checkup_members"=>200,
            "proposal_form"=>true,      //true, false
            
            "risk-class"=>"RISK",           // RISK, RIS1, RIS2, R1&2, RIS3 {GPA}
            "activity"=>"Hazardous",        // Hazardous, Non-Hazardous {GPA}
            "policy_basis"=>"named",        // Master A17 {GPA}
            "policy_basis_desc"=>"larum ipsum text",    // if policy_basic=blank {GPA}
            "caution_firm"=>"No",          // Yes, No {GPA}
            "ghi_covered"=>"Yes",          // Yes, No {GPA}
            "coverage"=>array(              // GPA
                "ppd"=>true,"ptd"=>true,"death"=>true,
                "acc_hosp"=>"Upto INR 25000",
                "medex"=>"Upto INR 10,000",
                "ptd_improvement"=>"Double Dismemberment",
                "ppd_improvement"=>true,
                "mortal_remains"=>"Upto INR 5,000 per incidence",   // dinemic 86
                "reconstructive_surgery"=>"Upto INR 50,000 per incidence", // dinemic 85
                "marriage_allowance"=>"upto INR 10,000",  // dinemic 69
                "mobility_extension"=>"Upto INR 5,000 per incidence",   // dinemic 81
                "hospital_cash_allowance"=>"upto INR 5,000 per incidence",   // dinemic 68
                "funeral_expenses"=>"INR 5,000 per incidence",   // dinemic 66
                "home_modification"=>"INR 50,000 per incidence",   // dinemic 67
                "fracture"=>"INR 10,000 per incidence",   // dinemic 65
                "compassionate_visit"=>"INR 5,000 per incidence",   // dinemic 63
                "children_education"=>"INR 5,000 per child for max 2 children",   // dinemic 48 (49,61)
                "burns"=>"INR 10,000 per incidence",   // dinemic 62
                "ambulance_service"=>"Fixed INR 1,000 per incidence",   // dinemic 52 (53,54,55,56)
                "ttd"=>"5000/ 1% of SI whichever lower for 104 weeks",   // dinemic 22 (42,43)
                "disappearance"=>"upto SI",    // dinemic 64
            ),
            
            "normal_maternity_amount"=>25000,       // Master A6, 0 for no cover on normal maternity
            "c_section_maternity_amount"=>35000,    //Master A7, 0 for no cover
            "cf_amount"=>200000,         // Master A8, 0 for no CF allowed
            "disease_limit"=>"9 Disease",     // Master A9
            "pre_post_hospitalization"=>"30 - 60 days",      // Master A10
            "ambulence"=>1500,         // Master A11
            "cover_type"=>"Individual", //Individual, Floater
            "PrePost Natal"=>false,     //true, false
            "waiting_period"=>"waveoff",    // waveoff, applicable
            "ped_exclusion"=>"applicable",    // waveoff, applicable, pedWait1Year
            "new_born_covered"=>"UP to Family SI",  // Master 18
            "slabs"=>array(
                array(
                    "sum_insured"=>1000,                    // Master A13 {GHI}
                    "co_pay"=>"25 Percent Parental Copay",  // Master A5
                    "sum_insured_type"=>"Flat", // Flat, Graded {GHI}
                    "grade_basis"=>"Salary",    // Master A12
                    "room_rent"=>"2%",          // Master A14
                    "icu_rent"=>"4%",          // Master A15
                    "reason"=>"Test",
                    "member_age_group"=>array(
                        "1-35"=>45,
                        "36-45"=>0,
                        "46-55"=>5,
                        "56-65"=>0,
                        "66-70"=>0,
                        "71-75"=>0,
                        "76-80"=>0,
                        "81-90"=>0,
                    )
                )
            ),
            "opd_coverage"=>"Consultation_yes",     // Master A16 (blank for no coverage, further parameter not required)
            "opd_sum_insured"=>1000,
            "opd_family_strucure"=>"NO_Self Only",
            "opd_no_of_employee"=>100,
            "opd_no_of_member"=>112,
            "opd_co_pay"=>"25 Percent Parental Copay",
            "opd_proposed_type"=>"Cashless",        // Cashless, Reimburshment
        ),
        array(
            "location_name"=>"Delhi",     //Master A2
            "no_of_lives"=>400,
            "family_strucure"=>"NO_Self Only",   //Master A3
            "mandate_type"=>"SINGLE",   // SINGLE, MULTIPLE
            "policy_structure"=>"Employee-Employer",      // Master A4
            "proposal_form"=>true,      //true, false
            "additional_requirement"=>"",
            "risk-class"=>"RISK",           // RISK, RIS1, RIS2, R1&2, RIS3 {GPA}
            "activity"=>"Hazardous",        // Hazardous, Non-Hazardous {GPA}
            "policy_basis"=>"named",        // Master A17 {GPA}
            "policy_basis_desc"=>"larum ipsum text",    // if policy_basic=blank {GPA}
            "caution_firm"=>false,          // true, false {GPA}
            "coverage"=>array(              // GPA
                "ppd"=>true,"ptd"=>true,"death"=>true,
                "acc_hosp"=>array(
                    "25,000"=>"",
                    "50,000"=>"",
                    "100,000"=>"",
                ),
                "medex"=>array(
                    "5,000"=>"",
                    "10,000"=>"",
                    "25,000"=>"",
                    "50,000"=>"",
                    "75,000"=>"",
                    "100,000"=>"",
                ),
                "medex_actual_wl"=>"",
                "ptd_improvement"=>true,
                "ppd_improvement"=>true,
                "mortal_remains"=>"Upto INR 5,000 per incidence",   // dinemic 86
                "reconstructive_surgery"=>"Upto INR 50,000 per incidence", // dinemic 85
                "marriage_allowance"=>"upto INR 10,000",  // dinemic 69
                "mobility_extension"=>"Upto INR 5,000 per incidence",   // dinemic 81
                "hospital_cash_allowance"=>"upto INR 5,000 per incidence",   // dinemic 68
                "funeral_expenses"=>"INR 5,000 per incidence",   // dinemic 66
                "home_modification"=>"INR 50,000 per incidence",   // dinemic 67
                "fracture"=>"INR 10,000 per incidence",   // dinemic 65
                "compassionate_visit"=>"INR 5,000 per incidence",   // dinemic 63
                "children_education"=>"INR 5,000 per child for max 2 children",   // dinemic 48 (49,61)
                "burns"=>"INR 10,000 per incidence",   // dinemic 62
                "ambulance_service"=>"Fixed INR 1,000 per incidence",   // dinemic 52 (53,54,55,56)
                "ttd"=>"5000/ 1% of SI whichever lower for 104 weeks",   // dinemic 22 (42,43)
                "disappearance"=>"upto SI",    // dinemic 64
            ),
            
            "normal_maternity_amount"=>25000,       // Master A6, 0 for no cover on normal maternity
            "c_section_maternity_amount"=>35000,    //Master A7, 0 for no cover
            "cf_amount"=>200000,         // Master A8, 0 for no CF allowed
            "disease_limit"=>"9 Disease",     // Master A9
            "pre_post_hospitalization"=>"30 - 60 days",      // Master A10
            "ambulence"=>1500,         // Master A11
            "cover_type"=>"Individual", //Individual, Floater
            "PrePost Natal"=>false,     //true, false
            "waiting_period"=>"waveoff",    // waveoff, applicable
            "ped_exclusion"=>"applicable",    // waveoff, applicable, pedWait1Year
            "slabs"=>array(
                array(
                    "sum_insured"=>1000,                    // Master A13 {GHI}
                    "co_pay"=>"25 Percent Parental Copay",  // Master A5
                    "sum_insured_type"=>"Flat", // Flat, Graded {GHI}
                    "grade_basis"=>"Salary",    // Master A12
                    "room_rent"=>"2%",          // Master A14
                    "icu_rent"=>"4%",          // Master A15
                    "reason"=>"Test",
                    "member_age_group"=>array(
                        "1-35"=>45,
                        "36-45"=>0,
                        "46-55"=>5,
                        "56-65"=>0,
                        "66-70"=>0,
                        "71-75"=>0,
                        "76-80"=>0,
                        "81-90"=>0,
                    )
                )
            ),
            "opd_coverage"=>"Consultation_yes",     // Master A16 (blank for no coverage, further parameter not required)
            "opd_sum_insured"=>1000,
            "opd_family_strucure"=>"NO_Self Only",
            "opd_no_of_employee"=>100,
            "opd_no_of_member"=>112,
            "opd_co_pay"=>"25 Percent Parental Copay",
            "opd_proposed_type"=>"Cashless",        // Cashless, Reimburshment
        )),
        "api_key"=>"NGDKH456AB45HLVNG8NB",
    );
} 
if($_REQUEST["get"]=="res"){
    $res=array(
        "status"=>"SUCCESS",        //SUCCESS, ERROR
        "quote_data"=>array(array(        //in case of SUCCESS
            "refrence_no"=>"H160500034500",
            "premium"=>10000,     // Assigned U/W, otherwise blank
        ),array(        //in case of SUCCESS
            "refrence_no"=>"H160500034501",
            "premium"=>10500,     // Assigned U/W, otherwise blank
        )),
        "error_data"=>array(        // in case of error
            "error_code"=>"A3012",
            "error_message"=>"Agent email id is not valid.",
        ),
    );
}
if($_REQUEST["get"]=="req1"){
    $req1=array(
        "action"=>"GETQUOTE", 
        "quote_data"=>array(
            "refrence_no"=>"H160500034500",
        ),
        "api_key"=>"NGDKH456AB45HLVNG8NB",
    );
}
if($_REQUEST["get"]=="res1"){
    $res1=array( // on success 200 http code & pdf file as download
        // on error 404 http code & json as response string containing error detail
        "status"=>"ERROR",
        "error_data"=>array(        // in case of error
            "error_code"=>"U231",
            "error_message"=>"PDF not generated. Unknown partner.",
        ),
    );
}
?>
<!DOCTYPE html>
<html><head>
    <meta charset="utf-8">
    <title>OAQ web service information viewer</title>
    <link rel="stylesheet" href="../../style/jquery.jsonview.css" />
    <script type="text/javascript" src="../../script/jquery.min.js"></script>
    <script type="text/javascript" src="../../script/jquery.jsonview.js"></script>
    <script type="text/javascript">
        var json = <?php echo json_encode(${$_REQUEST["get"]}); ?>;

        $(function() {
            $('#json').JSONView(json);
        });
        
        function toggleLevel(lvl){
            if(!isNaN(lvl) && parseInt(Number(lvl)) == lvl && !isNaN(parseInt(lvl, 10))){
                $('#json').JSONView('toggle', lvl);
            }
        }
    </script>
</head><body>
  <button id="toggle-btn" onclick="$('#json').JSONView('toggle');">Collapse/Expand All</button>
  Toggle to level: <input type="text" value="" placeholder="1" onblur="toggleLevel(this.value)" />
  <div id="json"></div>
</body>
</html>