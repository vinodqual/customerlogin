<?php
$mater=array(
    "A1"=>array(1=>"Services",2=>"Manufacturing Construction Logistics",21=>"IT BPO",
        41=>"Staff Solution",42=>"Trading",81=>"Caution yes"),
    "A2"=>array(1=>"Delhi",2=>"Mumbai",3=>"Bangalore",4=>"Kolkata",5=>"Hyderabad",6=>"Ahmedabad",
        7=>"Pune",8=>"Nashik",9=>"Mangalore",10=>"other",11=>"Amritsar",12=>"Nagpur",13=>"Chandigarh",
        14=>"Ludhiana",15=>"Indore",16=>"Mysore",17=>"Jaipur",18=>"Chennai"), 
    "A3"=>array(1=>"NO_Self Only",3=>"NO_Self Spouse and 2 Children upto 25 years",
        6=>"NO_Quote type manual",17=>"YES_Self Spouse Children upto 25 years and Parents upto 80 years",
        18=>"NO_Self and spouse only",19=>"NO_E and P for unmarried and ESC for married"),
    "A4"=>array(2=>"Employee-Employer",3=>"Non Employee-Employer(w/o selection)",
        4=>"Non Employee-Employer(with selection)"),
    "A5"=>array(81=>"25 Percent Parental Copay",63=>"20 percent parental copay",
        62=>"15 percent parental copay",61=>"10 percent Parental copay",42=>"NA",
        41=>"5",25=>"10",24=>"15",23=>"20",22=>"25",1=>"30"),
    "A6"=>array(8=>"50K",7=>"40K",6=>"35K",5=>"30K",4=>"25K",3=>"20K",2=>"15K",1=>"10K"),
    "A7"=>array(41=>"50K",22=>"40K",21=>"35K",2=>"30K",1=>"25K"),
    "A8"=>array(6=>"200000",5=>"1500000",4=>"0",3=>"2000000",2=>"1000000",1=>"500000"),
    "A9"=>array(41=>"NA",21=>"Cataract only",2=>"4 Disease",1=>"9 Disease"),
    "A10"=>array(22=>"60-90 days",21=>"30 - 60 days",1=>"NA"),
    "A11"=>array(4=>"2500",3=>"2000",2=>"1500",1=>"1000"),  //Ambulence
    "A12"=>array(61=>"Salary"), //Grade Basis
    "A13"=>array(1=>100000,2=>200000,3=>300000,21=>400000,22=>500000,81=>150000,
        82=>250000,83=>350000,101=>600000,244=>1000000,381=>750000,382=>50000),  //SI for GHI slabs
    "A14"=>array(10=>"Single AC Room",5=>"1 % of SI per day",4=>"1.5 % SI per day",
        3=>"2 % SI per day",1=>"0.5 % SI per day"), // Room Rent
    "A15"=>array(8=>"No limit",7=>"Single A/C",6=>"4 % SI per day",5=>"3 % SI per day",
        4=>"2.5 % SI per day",3=>"2 % of SI per day",2=>"1.5 % SI per day",1=>"1 % of SI per day"), // ICU
    "A16"=>array(382=>"Consultation_yes",381=>"Dental_yes",383=>"Dental Consultation &amp; Diagnostics_yes"), // OPD Coverage
    "A17"=>array(2=>"named"),
    "A18"=>array(86=>"Upto INR 5,000 per incidence"),
    "A19"=>array(85=>"Upto INR 50,000 per incidence"),
);
if(array_key_exists($_REQUEST["of"], $mater)){
    echo '<pre>';
    print_r($mater[$_REQUEST["of"]]);
}
else{
    echo "No master found";
}