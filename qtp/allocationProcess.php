<?php
/**
 * Copyright 2013 Catabatic Automation Technology Pvt Ltd.
 * All rights reserved
 *
 * @description: allocationProcess.php, 2016/01/08 15:52
 * @see: writeQuoteG.php
 * @updation: 2016/02/16 11:42
 * @author: Shakti Rana <shakti@catpl.co.in>
 */

include_once("conf/company_session.php");
include_once("conf/conf.php");
include_once("conf/fucts.php");

class AllocationProcess
{       
    /**
     * getAllocatedUW. Allocates under-writer based on the criteria CORPORATENAME, LIVEBAND, CURRCASES, NOOFCASES, ABSENTTO (in case of absent), PREMIUMAMOUNT (optional).
     * 
     * @global resource $conn
     * @param string $compNameInitial
     * @param interger $nooflives
     * @param integer $premiumAmount
     * @return array
     */

    
    static function getAllocatedUW($compNameInitial, $nooflives, $premiumAmount=0)
    {
        global $conn;
        $dataArray=array();
        $currentTime = time();
        
        $compNameInitial = substr(ucfirst($compNameInitial), 0, 1);
        $premiumAmount = $premiumAmount ? " AND PREMIUMAMOUNT >= $premiumAmount" : "";
        
        $cond           = " WHERE ($currentTime <= ABSENTTO OR ABSENTTO = 0)"
                        . " AND (STATUS='ACTIVE' OR STATUS IS NULL)"        
                        . " AND '$compNameInitial' BETWEEN SUBSTR(CORPORATENAME,1,1) AND SUBSTR(CORPORATENAME,-1,1)"
                        . " AND $nooflives BETWEEN SUBSTR(LIVEBAND, 1, INSTR(LIVEBAND, '-')-1) AND SUBSTR(LIVEBAND, INSTR(LIVEBAND, '-')+1)"
                        . $premiumAmount
                        . " AND CURRCASES+1 <= NOOFCASES";        
        
        $condOverload   = " WHERE ($currentTime <= ABSENTTO OR ABSENTTO = 0) "
                        . " AND (STATUS='ACTIVE' OR STATUS IS NULL)"        
                        . " AND '$compNameInitial' BETWEEN SUBSTR(CORPORATENAME,1,1) AND SUBSTR(CORPORATENAME,-1,1)"
                        . " AND $nooflives BETWEEN SUBSTR(LIVEBAND, 1, INSTR(LIVEBAND, '-')-1) AND SUBSTR(LIVEBAND, INSTR(LIVEBAND, '-')+1)"
                        . $premiumAmount;   
        
        $query          = "SELECT ID,UWNAME,CURRCASES,NOOFCASES,LIVEBAND,PREMIUMAMOUNT FROM CRMQTPALLOCATION $cond";             
        $dataArrayWithLoad = self::getDataArray($query,$nooflives);

	if(@$dataArrayWithLoad['dataArray']=='' || @$dataArrayWithLoad['dataArray'][0]=='')
        {   
             $queryOverload  = "SELECT ID,UWNAME,CURRCASES,NOOFCASES,LIVEBAND,PREMIUMAMOUNT FROM CRMQTPALLOCATION $condOverload";                   
            $dataArrayWithLoad   = self::getDataArray($queryOverload,$nooflives);
        }

        if(@$dataArrayWithLoad['dataArray']!='' || @$dataArrayWithLoad[0]['dataArray']!='')
        {
            $dataArray      = $dataArrayWithLoad['dataArray']; 
            $uwLoad         = $dataArrayWithLoad['uwLoad'];  
            $allocatedUW    = self::updateAllocationTable($dataArray, $uwLoad);
            return $allocatedUW;
        }
        return array(); 
    }
    
    /**
     * getDataArray.
     * 
     * @global resource $conn
     * @param type $sqlQuery
     * @param type $nooflives
     * @return array
     */
    private static function getDataArray($sqlQuery, $nooflives)
    {   
        global $conn;
        $i=0;
        $sqlResource = @oci_parse($conn, $sqlQuery);
        @oci_execute($sqlResource) or die(oci_error());
        while (($row = oci_fetch_assoc($sqlResource)))
        {
            foreach($row as $key=>$value)
            {
                $dataArray[$i][$key] = @$value;				
            }                    
            if(@$dataArray[$i]['ID'])
            {
                $liveLimit = explode('-',$dataArray[$i]['LIVEBAND']);
                $numerator   = ($dataArray[$i]['NOOFCASES'] - @$dataArray[$i]['CURRCASES'] - 1) * $nooflives;
                $denominator = $dataArray[$i]['NOOFCASES'] * ($liveLimit[1]-$liveLimit[0]);
                $uwLoad[$dataArray[$i]['ID']] = $dataArray[$i]['load'] = $numerator/$denominator;
            }
            $i++;
        }
        return array('dataArray' => @$dataArray, 'uwLoad' => @$uwLoad);
    }
    
    
    /**
     * UpdateAllocationTable. 
     * 
     * @param array $dataArray
     * @param array $uwLoad
     * @return boolean
     */
    private static function updateAllocationTable($dataArray, $uwLoad)
    {
        global $conn;
        if(count($dataArray)>0)
        {
            asort($uwLoad);            
            $rId        = array_search(end($uwLoad), $uwLoad);
            $sql        = "UPDATE CRMQTPALLOCATION SET CURRCASES=CURRCASES+1 WHERE ID='$rId'";                                                
            $stdid      = @oci_parse($conn, $sql);
            if(@oci_execute($stdid))
            {
                $selectedUW = fetchListCondsWithColumn('UWID, UWNAME', 'CRMQTPALLOCATION', "WHERE ID='$rId'");
                return $selectedUW[0];
            } 
        }
        return array();
    }
}
?>