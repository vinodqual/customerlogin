<?php 
/*****************************************************************************
* COPYRIGHT
* Copyright 2015 Catabatic Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: view_reimbursement.php,v 1.0 2015/10/07 05:30:15 amit kumar dubey Exp $
* $Author: amit kumar dubey $
*
****************************************************************************/
include_once("conf/conf.php");           //include configuration file 
include_once("conf/common_functions.php");          // include function file
include_once("conf/session.php");		//including session file 
$appointmentid=sanitize_data(base64_decode(@$_REQUEST['id']));
if($_SESSION['LOGINTYPE']=='CORPORATE'){ 
$table='CRMCOMPANYPOSTAPPOINTMENT';
} else {
$table='CRMRETAILPOSTAPPOINTMENT';

}
//$list=fetchListByColumnName($table,'URINEREPORTFILE,BLOODREPORTFILE,REPORTFILE','APPOINTMENTID',@$appointmentid);
$list=fetchListByColumnName($table,'CREATEDON,URINEREPORTFILE,BLOODREPORTFILE,REPORTFILE','APPOINTMENTID',@$appointmentid);

?>
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="model-header" id="exampleModalLabel">Health Report Document Details</div>
      </div>
      <div class="modal-middle" id="reimbursmentdoc">
			<table class="responsive" width="100%">
			  <tr>
				<th>#</th>
				<th>File  Name</th>
				<th class="text-right">Download</th>
			  </tr>
				<?php  
				if(count($list) > 0){
					$k=1;
                                        /*-------------------------- Code added for download file using SFTP--------*/
					$bloodreport=!empty($list[0]['BLOODREPORTFILE'])? $list[0]['BLOODREPORTFILE']:'';
					$urinereport=!empty($list[0]['URINEREPORTFILE'])?$list[0]['URINEREPORTFILE']:'';
					$otherreport=!empty($list[0]['REPORTFILE'])?$list[0]['REPORTFILE']:'';
                                        $created_on=!empty($list[0]['CREATEDON'])?$list[0]['CREATEDON']:'';
                                        $folder_name='';
                                        if(!empty($created_on)){
                                            $created_on=strtotime($created_on);
                                            $month_year=date('M',$created_on).'_'.date('Y',$created_on);
                                            $folder_name=REPORT_DOC.$month_year;        // REPORT_DOC folder name define in sftp_connect.php for upload report file.
                                        }
                                        /*-------------------- End of Code ------------------------------*/
					if(isset($bloodreport) && !empty($bloodreport)){
				?>
			  <tr>
				<td><?php echo $k++;  ?></td>
				<td><?php echo stripslashes($bloodreport); ?></td>
				<td class="text-right">
                                <!--------------------- Code added for file download using SFTP ----------------->
                                    <?php if(!empty($folder_name)) { 
                                        $blood_report_url=create_url($folder_name,$bloodreport,CUSTOMERLOGIN);        // Create File Download Link
                                            if($blood_report_url!==FALSE){
                                            ?>
                                        <a href="<?php echo $blood_report_url ?>"><span class="downloadIcon"></span></a>
                                    <?php   }else{
                                                echo SFTP0026;
                                            } 
                                        
                                        }else{ echo 'NA';} ?>
                                </td>
                                                <!----------- End of Code --------------->
			  </tr> 
              <?php } if(isset($urinereport) && !empty($urinereport)){?>
			  <tr>
				<td><?php echo $k++;  ?></td>
				<td><?php echo stripslashes(@$urinereport); ?></td>
                                
                                <!--------------------- Code added for file download using SFTP ----------------->
				<td class="text-right">
                                    <?php if(!empty($folder_name)) { 
                                        $urine_report_url=create_url($folder_name,$urinereport,CUSTOMERLOGIN);        // Create File Download Link
                                            if($urine_report_url!==FALSE){
                                        ?>
                                    <a href="<?php echo $urine_report_url; ?>"><span class="downloadIcon"></span></a>
                                    <?php 
                                            }else{
                                                echo SFTP0026;
                                            } 
                                        }else{ echo 'NA';} ?>
                                </td>
                                                <!--------- End of Code ------------------>
                                
<!--                                <td class="text-right"><a href="download_file_sftp.php?fileName=<?php echo @$urinereport; ?>"><span class="downloadIcon"></span></a></td>-->
			  </tr> 
              <?php } if(isset($otherreport) && !empty($otherreport)){?> 
			  <tr>
				<td><?php echo $k++;  ?></td>
				<td><?php echo stripslashes(@$otherreport); ?></td>
				<td class="text-right">
                                    <!--------------------- Code added for file download using SFTP ----------------->
                                    <?php if(!empty($folder_name)) { 
                                        $other_report_url=create_url($folder_name,$otherreport,CUSTOMERLOGIN);        // Create File Download Link
                                            if($other_report_url!==FALSE){
                                        ?>
                                    <a href="<?php echo $other_report_url; ?>"><span class="downloadIcon"></span></a>
                                    <?php 
                                            }else{
                                                echo SFTP0026;
                                            }
                                        }else{ echo 'NA';} ?>
                                </td>
                                                         <!--------- End of Code --------------->
<!--                                <td class="text-right"><a href="download_file_sftp.php?fileName=<?php echo @$otherreport; ?>"><span class="downloadIcon"></span></a></td>-->
			  </tr> 
			  <?php }
				} else {
			  ?>
			  <tr>
				<td colspan="4"><?php echo "No Health Report Uploaded";?></td>
			  </tr>  
			<?php
			  }
			 ?>  
			</table>      
     </div>
      
    </div>
                     