<?php ini_set('session.cookie_httponly',1);
ini_set('session.use_only_cookies',1);
include_once("conf/conf.php");    //include configuration file to access database connectivity
include_once("conf/common_functions.php"); //include common_functions file to access defined function
//print_r($_SESSION); die;
if(isset($_SESSION['LOGINTYPE']) && isset($_SESSION['USERID']) && isset($_SESSION['userName']) && isset($_SESSION['policyNumber']) && isset($_SESSION['ISPASSWORDCHANGED']) && !empty($_SESSION['LOGINTYPE']) && !empty($_SESSION['USERID']) && !empty($_SESSION['userName']) && !empty($_SESSION['policyNumber']) && !empty($_SESSION['ISPASSWORDCHANGED']) && count($_SESSION) > 0){
header('location:dashboard.php');	
exit;
}
/************************* Google login - Shakti. Dec 02, 2015 15:45 ********************/

require_once 'googleplus/src/Google_Client.php';
require_once 'googleplus/src/contrib/Google_Oauth2Service.php';

$client = new Google_Client();
$client->setApplicationName("Google UserInfo PHP Starter Application");

$oauth2 = new Google_Oauth2Service($client);
$authUrl = $client->createAuthUrl();

/*************************** Google login -EOC- shakti*********************/
$successmsg =sanitize_data(@$_GET['message']);
$errormsg = sanitize_data(@$_GET['errormsg']);
$attemp =sanitize_data(@$_GET['attemp']);
$newpolicynewac =sanitize_data(@$_GET['newpolicynewac']);
//$forgotpass = @$_GET['type'];
$type = sanitize_data(@$_GET['type']);
$acactivated =sanitize_data(@$_GET['acactivated']);

$_SESSION['dataArray']="";
$_SESSION["Flag"]=0;
@$_SESSION['policyNumber']="";
@$_SESSION['userName']="";
$_SESSION["firstName"]="";
$_SESSION['validateLogin']=0;
@$_SESSION['cId']="";
@$_SESSION['sId']="";
$accountCreated =sanitize_data(@$_GET['accountcreated']) == 'yes';
$policyMapped = sanitize_data(@$_GET['policymapped']) == 'yes';
$newPolicyNewAccount =sanitize_data(@$_GET['newpolicynewac']) == 'yes';

if(isset($_POST['emailid']) && $_POST['emailid']!=''){
require_once 'passwordverify/PkcsKeyGenerator.php';
require_once 'passwordverify/DesEncryptor.php';
require_once 'passwordverify/PbeWithMd5AndDes.php';		
$userType = sanitize_data(@$_POST['userType']);
$userType2 = sanitize_data(@$_POST['userType2']);
 if($userType2=='RETAIL'){
		 //Create Login Check Query			
		   $query=fetchcolumnListCond("USERSTATUS,USERNAME,USERID,PASSWORD","RETUSERAUTH","WHERE USERNAME='".sanitize_email(@$_POST['emailid'])."' ");
		$status=@$query[0]['USERSTATUS'];
		//print_r(sanitize_email(@$_POST['emailid'])); die;
		if($status=='DEACTIVE') {
			echo "<script>window.location.href='index.php?message=Account has been deactivated'</script>";
		}else{
			 $emailmessage="";
			if(count($query)>0)	{
					$dbpassword=@$query[0]['PASSWORD'];
					$decryptPass = PbeWithMd5AndDes::decrypt($dbpassword, $keystring);
					
					$status=$query[0]['USERSTATUS'];
					$stdid = @oci_parse($conn, $sql);
					$r = @oci_execute($stdid);				
					$emailmessage="";						
					$to = sanitize_email($_POST['emailid']);
					$text ='email='.$to.'&date='.time();
					$resetpwdlink=_SITEURL_CUSTOMER."resetpwd.php?data=".rawurlencode(simple_encrypt($text,$pwdsalt));
					$userDetails=fetchcolumnListCond("FIRSTNAME,LASTNAME","RETUSERMASTER"," WHERE EMAILID='".@$to."' ");
					$Mailtemplates=GetTemplateContentByTemplateName('RET_USER_FORGOT');
					$lastname=@$userDetails[0]['LASTNAME']?@$userDetails[0]['LASTNAME']:'';
				
					$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
					$message  = $Mailtemplates[0]['LARGECONTENT']?stripslashes($Mailtemplates[0]['LARGECONTENT']->load()):'';
					$message  = str_replace("##USER",@$userDetails[0]['FIRSTNAME']." ".@$lastname,$message);			
					$message  = str_replace("##PASSWORDLINK",@$resetpwdlink,$message);			
					
					$headers  = 'From:'.stripslashes($Mailtemplates[0]['FROMNAME']).' <'.stripslashes($Mailtemplates[0]['FROMEMAIL']). "> \r\n" .
						'MIME-Version: 1.0' . "\r\n" .
						'Content-type: text/html; charset=iso-8859-1';
								// Mail it
					//echo $message; die;
					if(mail($to, $subject, $message, $headers)){
					$reasontomail = 'FORGOTPASSWORD';
					$sendby = '';
					$entryTime=date('d-M-Y');
					$status = 'ACTIVE';
					$sendfrom = 'wellness@religare.com';
					$ipAddress = get_client_ip();
					//query to insert records
					$sqlLog="INSERT INTO PPHCMAILLOG (MAILID,MAILTO,SUBJECT,MESSAGE,USERTYPE,REASONTOMAIL,SENDBY,SENDDATE,STATUS,SENDFROM,IPADDRESS) values(PPHCMAILLOG_SEQ.nextval,q'[".$to."]','".@$subject."',:largecontent,'".@$userType2."','".@$reasontomail."','".@$sendby."','".@$entryTime."','".@$status."','".@$sendfrom."','".@$ipAddress."') "; 	
					$sqlLogExe = @oci_parse($conn, $sqlLog);
					oci_bind_by_name($sqlLogExe, ':largecontent', $message);
					$r = @oci_execute($sqlLogExe);
					
					echo "<script>window.location.href='index.php?message=Please check your registered email id to reset your password!'</script>";
				}		
			}
			else
			{
				echo "<script>window.location.href='index.php?errormsg=Email ID does not exists&type=forgotpassword'</script>";
				exit;
				//header('Location: index.php?errormsg=Email ID does not exists&type=forgotpassword');	
			}	
	}
			
 }
// corporate forgot password code starts
if($userType2=='CORPORATE'){
	
		 //Create Login Check Query			
		   $query=fetchcolumnListCond("STATUS,EMAIL,EMPLOYEENAME,ENCRYPTPASSWORD","CRMEMPLOYEELOGIN","WHERE EMAIL='".sanitize_email(@$_POST['emailid'])."'");
		 $status=@$query[0]['STATUS'];
		if($status=='DEACTIVE') {
		$url="Location: index.php?message=Account has been deactivated.";
		
		} else {
			 $emailmessage="";
			
			if(count($query)>0)	{
					// send forgot link to emailid created by amit kumar dubey on 2 february 2016 at 12:32 PM
					$dbpassword=@$query[0]['ENCRYPTPASSWORD'];
					$decryptPass = PbeWithMd5AndDes::decrypt($dbpassword, $keystring);
					
					$status=$query[0]['USERSTATUS'];
					$emailmessage="";						
					$to = sanitize_email($_POST['emailid']);
					$text ='email='.$to.'&date='.time();
					$resetpwdlink=_SITEURL_CUSTOMER."resetpwdcorp.php?data=".rawurlencode(simple_encrypt($text,$pwdsalt));
					$Mailtemplates=GetTemplateContentByTemplateName('CORP_USER_FORGOT');
					$name=@$query[0]['EMPLOYEENAME']?@$query[0]['EMPLOYEENAME']:'User';
				
					$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
					$message  = $Mailtemplates[0]['LARGECONTENT']?stripslashes($Mailtemplates[0]['LARGECONTENT']->load()):'';
					$message  = str_replace("##USER",@$name,$message);			
					$message  = str_replace("##PASSWORDLINK",@$resetpwdlink,$message);			
					
					$headers  = 'From:'.stripslashes($Mailtemplates[0]['FROMNAME']).' <'.stripslashes($Mailtemplates[0]['FROMEMAIL']). "> \r\n" .
						'MIME-Version: 1.0' . "\r\n" .
						'Content-type: text/html; charset=iso-8859-1';

					//end of send link for forgot password


					if(mail($to, $subject, $message, $headers)){
					$reasontomail = 'FORGOTPASSWORD';
					$sendby = '';
					$entryTime=date('d-M-Y');
					$status = 'ACTIVE';
					$sendfrom = 'wellness@religare.com';
					$ipAddress = get_client_ip();
					//query to insert records
					$sqlLog="INSERT INTO PPHCMAILLOG (MAILID,MAILTO,SUBJECT,MESSAGE,USERTYPE,REASONTOMAIL,SENDBY,SENDDATE,STATUS,SENDFROM,IPADDRESS) values(PPHCMAILLOG_SEQ.nextval,q'[".$to."]','".@$subject."',:largecontent,'".@$userType2."','".@$reasontomail."','".@$sendby."','".@$entryTime."','".@$status."','".@$sendfrom."','".@$ipAddress."') "; 	
					$sqlLogExe = @oci_parse($conn, $sqlLog);
					oci_bind_by_name($sqlLogExe, ':largecontent', $message);
					$r = @oci_execute($sqlLogExe);
                    echo "<script>window.location.href='index.php?message=Please check your registered email id to reset your password!'</script>";
					
				}	
			}
			else
			{
				echo "<script>window.location.href='index.php?errormsg=Email ID does not exists&type=forgotpassword'</script>";
				exit;
				//header('Location: index.php?errormsg=Email ID does not exists&type=forgotpassword');
				
			}
	}
			
 }
 
}
// corporate forgot password code ends


// Login code starts
if (isset($_POST["username"]) && $_POST['username']!='') {
	$userType = sanitize_data($_POST['userType']);
  if($userType=='RETAIL'){
    require_once 'passwordverify/PkcsKeyGenerator.php';
    require_once 'passwordverify/DesEncryptor.php';
    require_once 'passwordverify/PbeWithMd5AndDes.php';
    $password = ($_POST['password']);
    $crypt = PbeWithMd5AndDes::encrypt($password, $keystring);
    //Create Login Check Query
   // $query1 = sprintf("SELECT * FROM RETUSERAUTH  WHERE USERNAME like '%s' AND USERSTATUS='ACTIVE'", sanitize_username(trim(@$_POST['username'])), @$password);   //query to select records
    $query1 = sprintf("SELECT * FROM RETUSERAUTH  WHERE USERNAME like '%s'", sanitize_username(trim(@$_POST['username'])), @$password);   //query to select records
    $login = @oci_parse($conn, $query1);
	
    @oci_execute($login);
    $nrows1 = @oci_fetch_all($login, $res1);
    $dbpassword = @$res1['PASSWORD'][0];
	//$dbpassword="U2FsdGVkX19YsDiBCls+r1NLrU9EiVa9vF3coDn5K1U=";
     $decrypt = PbeWithMd5AndDes::decrypt($dbpassword, $keystring);
    if (($nrows1 > 0) && ($decrypt == $password) && $res1['SOURCE'][0] != "GPLUS") {

       // $decrypt = PbeWithMd5AndDes::decrypt($res1['PASSWORD'][0], $keystring);

        if ($res1['USERSTATUS'][0] == "DEACTIVE") {
				$url = "index.php?attemp=attemp&s=r"; //url locate to index page with account deactivated msg
			}
			if ($res1['USERSTATUS'][0] == "ACTIVE") {
            session_regenerate_id();
            $_SESSION["userName"] = trim($res1['USERNAME'][0]);
            $_SESSION["USERID"] = trim($res1['USERID'][0]);
			$_SESSION["LOGINTYPE"] = 'RETAIL';
            $_SESSION['validateLogin'] = 1;
			$_SESSION['ISPASSWORDCHANGED']=trim($res1['ISPASSWORDCHANGED'][0]);
            $loginTime = time();
            $_SESSION["lastLoginTime"] = time();
            $sql = "INSERT INTO RETUSERLOGIN (USERLOGINSEQ,USERID,LASTLOGINDT,FIRSTLOGINDATE,LOGINSTATUS,IP) VALUES (RETUSERLOGIN_SEQ.nextval,'" . trim(@$_SESSION['USERID']) . "','" . $loginTime . "','" . $loginTime . "','" . $res1['USERSTATUS'][0] . "','" . @$_SERVER['REMOTE_ADDR'] . "')"; //insert query to insert records 
            $stdid = @oci_parse($conn, $sql);
            $r = @oci_execute($stdid);

            $stid = @oci_parse($conn, "SELECT LASTLOGINDT FROM RETUSERLOGIN WHERE USERID='" . $_SESSION["USERID"] . "' order by LASTLOGINDT desc");
            // Execute Query
            @oci_execute($stid);
            $logindetails = @oci_fetch_all($stid, $res);
            if ($logindetails == 1 || $logindetails == 0) {
                $_SESSION["lastLogin"] = @$loginTime;
            } else {
                $_SESSION["lastLogin"] = $res['LASTLOGINDT'][1];
            }
			// // query to check wrong attempts
			$queryCheckWrongAttempts=fetchcolumnListCond("UNSUCCESSFULATTEMPTS","RETUSERAUTH","WHERE USERNAME='".sanitize_username(@$_POST['username'])."' AND USERSTATUS='ACTIVE' ");
			$totalWrongAttempts = @$queryCheckWrongAttempts[0]['UNSUCCESSFULATTEMPTS'];
				
			if(@$totalWrongAttempts>0 && @$totalWrongAttempts !== 5){
				$res_attempts1=0;
				 $sqlRetail = "UPDATE RETUSERAUTH SET UNSUCCESSFULATTEMPTS=".@$res_attempts1 . " where USERNAME='".@$res1['USERNAME'][0]."'";
				$stdids2 = @oci_parse($conn, $sqlRetail);
				$rsd2 = @oci_execute($stdids2);
			} 
			///added condition for mapping policy with proposals from proposal database table RETUSERPOLICYMAP cretaed by amit kumar dubey on 18 january 2016 at 11:56 AM
			$proposalPolicyList=fetchListCondsWithColumnProposal("POLICY_NUMBER,DOB","reference_data_final"," WHERE POLICY_NUMBER IS NOT NULL AND EMAIL_ID ='".$res1['USERNAME'][0]."'");
			
			if(count($proposalPolicyList) > 0){
				$k=0;
				while($k<count($proposalPolicyList)){
					$queryCheckWrongAttempts=fetchcolumnListCond("POLICYNUM","RETUSERPOLICYMAP","WHERE POLICYNUM='".@$proposalPolicyList[$k]['POLICY_NUMBER']."' ");
					
					 if(count($queryCheckWrongAttempts)==0){
					  $sqlInsertMapping = "INSERT INTO RETUSERPOLICYMAP (USERPOLICYMAPSEQ,USERID,POLICYNUM,CUSTOMERID,EMAILID,DOB,MEMBERTYPE) VALUES (RETUSERPOLICYMAP_SEQ.nextval,'" . trim(@$_SESSION['USERID']) . "','".$proposalPolicyList[$k]['POLICY_NUMBER']."','','" .@$res1['USERNAME'][0]. "','".$proposalPolicyList[$k]['DOB']."','')"; //insert query to insert records 
					
						$stdidsMap = @oci_parse($conn, $sqlInsertMapping);
						$rsdMap = @oci_execute($stdidsMap);
					 }
					
				$k++;
				}
			}
			///end added condition for mapping policy with proposals from proposal database table RETUSERPOLICYMAP
            $url = "dashboard.php";
        } else {
            $url = "index.php?attemp=attemp&s=r"; //url locate to index page with account deactivated msg
        }
    } else {
		
		if ($nrows1 > 0) {
        session_regenerate_id();
        if (@$res1 > 0) {
            $res_attempts = @$res1['UNSUCCESSFULATTEMPTS'][0] ? @$res1['UNSUCCESSFULATTEMPTS'][0] : 0;
            if ($res_attempts != 5) {
                $attempts = $res_attempts + 1;
                $sqlg = "UPDATE RETUSERAUTH SET UNSUCCESSFULATTEMPTS=" . @$attempts . " where USERNAME='" . @$res1['USERNAME'][0] . "' ";
                $stdids = @oci_parse($conn, $sqlg);
                $rsd = @oci_execute($stdids);
            }
			// query to check wrong attempts
			$queryCheckWrongAttempts=fetchcolumnListCond("UNSUCCESSFULATTEMPTS","RETUSERAUTH","WHERE USERNAME='".sanitize_username(@$_POST['username'])."' AND USERSTATUS='ACTIVE' ");
			$totalWrongAttempts1 = @$queryCheckWrongAttempts[0]['UNSUCCESSFULATTEMPTS'];
            if ($totalWrongAttempts1 == 5) {
                $sqlg = "UPDATE RETUSERAUTH SET USERSTATUS='DEACTIVE' where USERNAME='" . @$res1['USERNAME'][0] . "' ";
                $stdids = @oci_parse($conn, $sqlg);
                $rsd = @oci_execute($stdids);
                header('location: index.php?attemp=attemp&s=r');
                exit;
            }
        }
    }
	else{
		
		 $url = "index.php?message=Incorrect Username or Password&s=r"; //url locate to index page with incorrect Username/Password msg
		}
        $url = "index.php?message=Incorrect Username or Password&s=r"; //url locate to index page with incorrect Username/Password msg
    }
  }

if($userType=='CORPORATE'){
	include_once($apiurl);	//include api file to access API

	$password=$_POST['password'];
 	//Create Login Check Query
    $query1 = sprintf("SELECT * FROM CRMEMPLOYEELOGIN  WHERE EMAIL like '%s' AND ENCRYPTPASSWORD like '%s'",
		sanitize_username(@$_POST['username']),
		md5(@$password));			//query to select records
	// Parse Query 
	$login = @oci_parse($conn, $query1);
	// Execute Query
	@oci_execute($login);
	// Fetch Data IN Associative Array
 	 $nrows1 = @oci_fetch_all($login, $res1);
	 
	 if (@$res1['STATUS'][0] == "DEACTIVE") {
				$url = "index.php?attemp=attemp&s=r"; //url locate to index page with account deactivated msg
			}
	 
	 if($nrows1==0){ 
			session_regenerate_id();
			 $attemptsquery = sprintf("SELECT * FROM CRMEMPLOYEELOGIN WHERE EMAIL like '%s' ",
			sanitize_username(@$_POST['username'])); 			//query to select records
			// Parse Query 
			$attresult = @oci_parse($conn, $attemptsquery);
			// Execute Query
			@oci_execute($attresult);
			// Fetch Data IN Associative Array
			$nrows_res_result = @oci_fetch_all($attresult, $res_result);
			if($nrows_res_result > 0) {
			 $res_attempts=@$res_result['BADATTEMPTS'][0]?@$res_result['BADATTEMPTS'][0]:0;
			 if($res_attempts!=5) {
					$attempts=$res_attempts+1;
			 $sqlg="UPDATE CRMEMPLOYEELOGIN SET BADATTEMPTS=".@$attempts." where EMAIL='".@$res_result['EMAIL'][0]."' "; 
					$stdids = @oci_parse($conn, $sqlg);
					$rsd = @oci_execute($stdids);
					}
					// query to check wrong attempts
			$queryCheckWrongAttempts=fetchcolumnListCond("BADATTEMPTS","CRMEMPLOYEELOGIN","WHERE EMAIL='".sanitize_username(@$_POST['username'])."' AND STATUS='ACTIVE' ");
			$totalWrongAttempts1 = @$queryCheckWrongAttempts[0]['BADATTEMPTS'];			
				if($totalWrongAttempts1==5) {
			 $sqlg="UPDATE CRMEMPLOYEELOGIN SET STATUS='DEACTIVE' where EMAIL='".@$res_result['EMAIL'][0]."' ";
					$stdids = @oci_parse($conn, $sqlg);
					$rsd = @oci_execute($stdids);
					header('location: index.php?attemp=attemp&s=c');
					exit;
				} 
				
			}
		}
		else{
			// query to check wrong attempts
			$queryCheckWrongAttempts=fetchcolumnListCond("BADATTEMPTS","CRMEMPLOYEELOGIN","WHERE EMAIL='".sanitize_username(@$_POST['username'])."' AND STATUS='ACTIVE' ");
			$totalWrongAttempts = @$queryCheckWrongAttempts[0]['BADATTEMPTS'];
				
			if(@$totalWrongAttempts>0 && @$totalWrongAttempts !== 5){
				$res_attempts1=0;
				 $sqlCorporate = "UPDATE CRMEMPLOYEELOGIN SET BADATTEMPTS=".@$res_attempts1 . " where EMAIL='".@$res_result['EMAIL'][0]."'";
				$stdids2 = @oci_parse($conn, $sqlCorporate);
				$rsd2 = @oci_execute($stdids2);
			}
		}
	 if($nrows1>0){ 
	
			if(trim(@$res1['POLICYID'][0])!='')	{	
				
					$checkArray=checkEmployeePolicyORCompanyActivated(@$res1['POLICYID'][0]);
					
					if(@$checkArray[0]['POLICYSTATUS']=="ACTIVE" && @$checkArray[0]['COMPANYSTATUS']=="ACTIVE" && $res1['STATUS'][0]=="ACTIVE"){			
						session_regenerate_id();
							$_SESSION["userName"]=trim($res1['EMAIL'][0]);
							$_SESSION["employeeName"]=trim($res1['EMPLOYEENAME'][0]);
							$_SESSION["COMPANYID"]=trim($res1['COMPANYID'][0]);
							$_SESSION["policyNumberCheck"]=trim($res1['POLICYNUMBER'][0]);
							$_SESSION['policyNumber']=trim($res1['POLICYNUMBER'][0]);
							$_SESSION['policyNumNew']=trim($res1['POLICYNUMBER'][0]);
							$_SESSION["customerIdCheck"]=trim($res1['CUSTOMERID'][0]);
							$_SESSION["employeeIdCheck"]=trim($res1['EMPLOYEEID'][0]);
							$_SESSION["emp_mobile"]=trim($res1['MOBILENUMBER'][0]);
							$_SESSION["USERID"] = trim($res1['LOGINID'][0]);
							$_SESSION["type"]="Employee";
							$_SESSION['validateLogin']=1;
							$_SESSION['dobCheck']=@$dob;
							$_SESSION['companySeoName']=@$companySeoName;
							$_SESSION["LOGINTYPE"] = 'CORPORATE';
							$_SESSION['ISPASSWORDCHANGED']=trim($res1['ISPASSWORDCHANGED'][0]);
							//Insert Login Log
							//$loginTime=date('d-M-y H:i A');
							$loginTime=time();
							$_SESSION["lastLoginTime"] = time();
							 $sql = "INSERT INTO CRMEMPLOYEELOGINLOG (POLICYNUMBER, EMPLOYEEID,CUSTOMERID,IPADDRESS,LOGINTIME) VALUES ('".trim(@$_SESSION['policyNumber'])."','".@$_SESSION["employeeIdCheck"]."','".@$_SESSION["customerIdCheck"]."','".@$_SERVER['REMOTE_ADDR']."','".@$loginTime."')"; //insert query to insert records 
							$stdid = @oci_parse($conn, $sql);
							$r = @oci_execute($stdid);
							



					$stid = @oci_parse($conn, "SELECT LOGINTIME FROM CRMEMPLOYEELOGINLOG WHERE POLICYNUMBER='".$_SESSION["policyNumNew"]."' AND ( EMPLOYEEID=".$_SESSION["employeeIdCheck"]." OR CUSTOMERID=".$_SESSION["customerIdCheck"]." ) order by LOGINTIME desc");
					// Execute Query
					@oci_execute($stid);
					$logindetails = @oci_fetch_all($stid, $res);
					
					if($logindetails==1 || $logindetails==0){
						$_SESSION["lastLogin"]=@$loginTime;
					}else{
						$_SESSION["lastLogin"]=$res['LOGINTIME'][1];
					}	
								
							// Check details in Webservice					
							if(isset($_SESSION["employeeIdCheck"])){
								$query['EMPNO'] = @$_SESSION["employeeIdCheck"];
							}
							if(isset($_SESSION["customerIdCheck"])){
								$query['CLNTNUM'] = @$_SESSION["customerIdCheck"];
							}
							if(isset($dob)){
								$query['CLTDOBX'] = @$dob;
							}
								$query['CHDRNUM'] = @$_SESSION["policyNumberCheck"];
								$dataArray=getPolicyEnquiry($query);
								if(isset($_SESSION['response_error'])){
									$responseError  = $_SESSION['response_error'];
								}else{
									$responseArray = $_SESSION['dataArray'];
								}

								if(@$responseArray[0]['BGEN-EMPNO']['#text']!=''){
								$_SESSION['empId']=@$responseArray[0]['BGEN-EMPNO']['#text'];
								$_SESSION['empNo']=@$responseArray[0]['BGEN-EMPNO']['#text'];
								$_SESSION['clientNumber']=@$responseArray[0]['BGEN-CLNTNM']['#text'];
								}else{
								$_SESSION['empNo']=@$responseArray[0]['BGEN-EMPNO']['#text'];
								$_SESSION['empId']=@$responseArray[0]['BGEN-CLNTNM']['#text'];
								$_SESSION['clientNumber']=@$responseArray[0]['BGEN-CLNTNM']['#text'];
								}

								if(@$res1['NOOFLOGIN'][0]==1 || @$res1['NOOFLOGIN'][0]==''){
									$url="dashboard.php";
								}else{
									@$_SESSION['validateLogin']=2;
									$url="dashboard.php";
								}
							// query to check wrong attempts
							$queryCheckWrongAttempts=fetchcolumnListCond("BADATTEMPTS","CRMEMPLOYEELOGIN","WHERE EMAIL='".sanitize_username(@$_POST['username'])."' AND STATUS='ACTIVE' ");
							$totalWrongAttempts = @$queryCheckWrongAttempts[0]['BADATTEMPTS'];
								
							if(@$totalWrongAttempts>0 && @$totalWrongAttempts !== 5){
								$res_attempts1=0;
								 $sqlCorporate = "UPDATE CRMEMPLOYEELOGIN SET BADATTEMPTS=".@$res_attempts1 . " where EMAIL='".@$res1['EMAIL'][0]."'";
								$stdids2 = @oci_parse($conn, $sqlCorporate);
								$rsd2 = @oci_execute($stdids2);
							} 
							
					}else{
							$url="index.php?message=Your account has been de-activated, please contact administrator!&s=c";	//url locate to index page with account deactivated msg
					}
			}else{
							$url="index.php?message=Incorrect Username/Password&s=c";	//url locate to index page with incorrect Username/Password msg
			}
		} else{
						$url="index.php?message=Incorrect Username or Password&s=c";	//url locate to index page with incorrect Username/Password msg
		}
}
echo "<script>window.location.href='$url'</script>";
    exit;
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Religare Login</title>
        <meta name="description" content="{$METADESC}">
        <meta name="keywords" content="{$METAKEYWORDS}"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="author" content="">
        <link rel="shortcut icon" href="favicon.ico">
        <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" >
        <link rel="stylesheet" type="text/css" href="css/bootstrap-theme.min.css" >		
        <link rel="stylesheet" type="text/css" href="font-awesome/css/font-awesome.min.css">
		<link rel="stylesheet" type="text/css" href="css/stylesheet.css" >
        <link rel="stylesheet" type="text/css" href="css/styles.css" >
		<link rel="stylesheet" type="text/css" href="css/style.css" >
		<link rel="stylesheet" type="text/css" href="css/datepiker.css" >		
		<link rel="stylesheet" type="text/css" href="css/index-custom.css" >
		<script src="js/alice.js" type="text/javascript"></script>  	  
		<link href="css/owl.carousel.css" rel="stylesheet">
		<link href="css/owl.theme.css" rel="stylesheet">	
		<script language="javascript" src="js/encrypt.js"></script>
		<script src="js/common.js"></script>
		<script src="js/facebook.js"></script>
<style>
#chkboxrole-error{
	float: left;
    margin-left: 0;
    margin-top: 16%;
}

</style>		
<script type="text/javascript"> 
var aliceImageHost = '';
var globalConfig = {};     
</script> 
<script>
	$(document).ready(function() {
	$(".rightnavBox ul li a.claimcl").click(function(){
	$('#clickarrowDivServices, .clickarrow').hide();
	$(this).parent().children("div").show();	
	$('#clickarrowDivClaims').show();
});

$(".rightnavBox ul li a.servicecl").click(function(){
	$('#clickarrowDivClaims, .clickarrow').hide();
	$('.service_tabs').hide();
	$(this).parent().children("div").show();	
	$('#clickarrowDivServices, #tax_receiptstab').show();
        
    $(".claimdisplaynav1 ul li a").removeClass("activetab");
    $("#tax_receipts").addClass("activetab");
});

$(".grCross").click(function(event){
    $('.clickarrow, .clickarrowDiv').hide();
//    event.stopPropagation();
});

$("#loginNav").click(function(){
	$('#clickarrowDivServices,#clickarrowDivClaims, .clickarrow, .service_tabs').hide();
});
$('.navigation, .nav1').click(function(event) {
  $('html').one('click',function() {
	$('.nav1').hide();
  });
  event.stopPropagation();
});

$(".rightnavBox ul li #loginNav").click(function(){
	$('.loginDiv').toggle();
});

$('#loginNav, .loginDiv').click(function(event) {
  $('html').one('click',function() {
	$('.loginDiv').hide();
  });

  event.stopPropagation();
});

	$('#password').hide();
	// Show the fake pass (because JS is enabled)
	$('#fake_Password').show();

	// On focus of the fake password field
	$('#fake_Password').focus(function() {
		$(this).hide(); //  hide the fake password input text
		$('#password').show().focus(); // and show the real password input password
	});
	// On blur of the real pass
	$('#password').blur(function() {
		if ($(this).val() == "") { // if the value is empty, 
			$(this).hide(); // hide the real password field
			$('#fake_Password').show(); // show the fake password
		}
		// otherwise, a password has been entered,
		// so do nothing (leave the real password showing)
	});

	});
</script>
</head>
<body>

<!-- header -->
<div class="headerMain">
<?php include('pphcheader.php'); ?>
</div>
        <!-- body container -->
        <section id="body_container"> 
            <div class="right-Cont" id="login_section">
                <h1>Login to your Account</h1>
                <!-- Heding top  closed-->
                <div class="clearfix"></div>
                <div class="spacer4"></div>
                <!-- Step start -->
                <div class="right-Cont-step1">
				 <form action="" name="loginForm" id="loginForm" method="post">
                    <div class="middleContainerBox">
						<h2>Please select your user type and enter details</h2>
						<?php 
							if($attemp == 'attemp'){
						?>	
						</a> <div style="margin-bottom:3%;"><a href="javascript:void(0);" class="activate_account" onClick="return removeErrorAccountActivate();">Click here to activate your account <span>?</span></a></div>
						<?php	}
						 ?>
						<?php if(isset($accountCreated) && $accountCreated !='') {?>
						<div style="height:20px; margin-bottom:10px; color: red;">Your account created successfully, Please Login!</div>
						<?php } 
								
						?>
						<?php if(isset($policyMapped) && $policyMapped !='') {?>
						<div style="height:20px; margin-bottom:10px; color: red;">Your policy mapped with your existing account!</div>
						<?php } 
								
						?>
						<div class="genderBox" style="padding:0;">
						<div class="genderBox">
						<a href="javascript:void(0);" onClick="getCenterValue('RETAIL');"><span class="maleIcon <?php if(@$_REQUEST['s']=='r'){ echo 'maleIconActive'; } ?>"  id="tpaicon_login">RETAIL</span></a>
						<a href="javascript:void(0);" onClick="getCenterValue('CORPORATE');"><span class="femaleIcon <?php if(@$_REQUEST['s']=='c'){ echo 'femaleIconActive'; } ?>"  id="chainicon_login">CORPORATE</span></a>
						<input type="hidden" value="<?php if(@$_REQUEST['s']=='r'){ echo 'RETAIL'; } else if(@$_REQUEST['s']=='c'){ echo 'CORPORATE'; } else { } ?>" id="userType" name="userType" />
						
						 <div id="userType-error1" class="error"><?php if(@$successmsg !=''){echo '<font color="red">'.@$successmsg.'</font>'; @$successmsg=''; }?>
						  <?php if(@$attemp !=''){
							  
							  echo '<font color="red">Your Account has been deactivated due to more than 5 bad attempts contact admnistrator</font>';
						  }
						   else if(@$acactivated =='yes'){		  
								  echo '<font color="red">Your Account has been Activated. Pls check your email for login details</font>';
							  }
							  else if(@$newpolicynewac =='yes'){		  
								  echo '<font color="red">Your Account has been Created. Pls check your email for login details</font>';
							  }
						  ?>
							</div>
						</div>

				
					</div>

					
					 <div class="middleContainerBox">
                     <div id="errorMsg1" class="customerrormsg"></div>
                            <div class="graytxtBox3 space-marging marginspl"><span class="txtIcon1"></span>
                                <div class="txtMrgn ">
								<input type="text" value="" name="username" class="txtfieldFull" id="username"  AUTOCOMPLETE="OFF" placeholder="Username*" onFocus="if (this.value == 'Username*') { this.value = '';}" onBlur="if (this.value == '') { this.value = 'Username*';  document.loginForm.username.value = ''; }">
								
								
								
								</div>
                            </div>
                            <div class="graytxtBox3"><span class="txtIconnew"></span>
                                <div class="txtMrgn">
									<input type="text" value="Password*" placeholder="Password*" class="txtfieldFull" id="fake_Password" style="display:none"  name="fake_Password" onFocus="if (this.value == 'Password*') { this.value = '';}" onBlur="if (this.value == '') { this.value = 'Password*';  document.loginForm.password.value = ''; }">
									
                                    <input type="password" value="" class="txtfieldFull" id="password" name="password" AUTOCOMPLETE="OFF" >
                                    <input type="Submit" style="display:none"  />
                                </div>
                            </div>
                       
						</div>
                    </div>
					</form>
                    <div class="right-Cont-step1-button">

					<a href="javascript:void(0);" onClick="$('#loginForm').submit();$('#password').val(base64_encode($('#password').val()));
					">Sign in <img src="images/arrow.png"  border="0" alt="" title=""></a>  
					
                    </div>
					 <div class="or">
                        <div class="or-text">OR</div>
                    </div>
					<div class="signup-text">sign-up with</div>
					<div class="loginimages">
					<a href="register.php" ><img src="images/icon-attherate.jpg"></a>
					<a href="javascript:void(0);" id="fb-login"><img src="images/icon-fb.jpg"></a>
					<a href="<?php echo $authUrl;?>" ><img src="images/icon-googleplus.jpg"></a>
					</div>
                    <div class="spacer2"></div>
					<a href="javascript:void(0);" class="right-Cont-step1-button1 forgot_pass" onClick="return removeErrorForgot();">Forgot Password <span>?</span></a>
                   
                    <!--<div class="spacer3"></div>
                   
                    <div class="spacer4"></div> -->
                </div>
            </div>
	<!-- forgot password section starts-->
	<div class="right-Cont" id="forgot_pass_section">
      <h1>Account Help</h1>
  
    <!-- Heding top  closed-->
    <div class="clearfix"></div>
    <div class="spacer4"></div>
    <!-- Step start -->
			  <div class="right-Cont-step1">
				 <form action="" name="forgotForm" id="forgotForm" method="post">
                    <div class="middleContainerBox">
						<h2>Please select your user type and enter details</h2>
						<div class="genderBox" style="padding:0;">
						<div class="genderBox">
						<a href="javascript:void(0);" onClick="getCenterValue2('RETAIL');"><span class="maleIcon <?php if(@$_REQUEST['s']=='r'){ echo 'maleIconActive'; } ?>"  id="tpaicon_forgot">RETAIL</span></a>
						<a href="javascript:void(0);" onClick="getCenterValue2('CORPORATE');"><span class="femaleIcon <?php if(@$_REQUEST['s']=='c'){ echo 'femaleIconActive'; } ?>"  id="chainicon_forgot">CORPORATE</span></a>
						<input type="hidden" value="<?php if(@$_REQUEST['s']=='r'){ echo 'RETAIL'; } else if(@$_REQUEST['s']=='c'){ echo 'CORPORATE'; } else { } ?>" id="userType2" name="userType2" />
						
						 <div id="userType-error2" class="error2"><?php if(@$errormsg !=''){echo '<font color="red">'.@$errormsg.'</font>'; @$errormsg=''; }?></div>
						</div>
					</div>
					 <div class="middleContainerBox">
                     <div id="errorMsg1" class="customerrormsg"></div>
                            <div class="graytxtBox3 space-marging "><span class="txtIcon1"></span>
                            <div class="txtMrgn ">
			  <input type="text" value="" class="txtfieldFull" id="emailid" name="emailid" placeholder="Enter Email *" ></div>
                            </div>
                       
						</div>
                    </div>
					</form>
                   

                         <div class="right-Cont-step1-button">
        
        <a id="loginBtn" style="margin-right:10px;" onClick="$('#login_section').show();
    $('#forgot_pass_section').hide(); return removeErrorLogin();"><img src="images/arrowleft.png"  border="0" alt="" title=""> Back </a>
		  <a href="javascript:void(0);" onClick="$('#forgotForm').submit();">Generate Password <img src="images/arrow.png"  border="0" alt="" title=""></a> 
         
      </div>       
    </div>
    </div>
   <!-- Forgot password section ends -->
<!-- Activate Account section starts-->
	<div class="right-Cont" id="activate_account_section">
      <h1>Activate Account Help</h1> 
    <!-- Heding top  closed-->
    <div class="clearfix"></div>
    <div class="spacer4"></div>    
    <!-- Step start -->   
	<div class="right-Cont-step1">
  <form name="activateAccountForm" id="activateAccountForm" method="POST" action="">
    
	<div class="middleContainerBox">
	  <h2>Please select your user type and enter details</h2>
	<div class="genderBox" style="padding:0;">
	<div class="genderBox">
	<a href="javascript:void(0);" onClick="getCenterValue3('RETAIL');"><span class="maleIcon <?php if(@$_REQUEST['s']=='r'){ echo 'maleIconActive'; } ?>"  id="tpaicon_activate_ac">RETAIL</span></a>
			<a href="javascript:void(0);" onClick="getCenterValue3('CORPORATE');"><span class="femaleIcon <?php if(@$_REQUEST['s']=='c'){ echo 'femaleIconActive'; } ?>"  id="chainicon_activate_ac">CORPORATE</span></a>
			<input type="hidden" value="<?php if(@$_REQUEST['s']=='r'){ echo 'RETAIL'; } else if(@$_REQUEST['s']=='c'){ echo 'CORPORATE'; } else { } ?>" id="userType3" name="userType3" />
			<div id="userType-error3" class="error3"></div>
	</div>
	</div>
      <div class="middleContainerBox">
	   <div id="errorMsg1" class="customerrormsg"></div>
	  
        	<div class="graytxtBox3 space-marging-custom "><span class="txtIcon5"></span>
              <div class="txtMrgn ">
			  <input type="text" value="" class="txtfieldFull" id="emailidActivate" name="emailidActivate" placeholder="Enter Email *"></div>
            </div>
			<div class="graytxtBox3 space-marging-custom "><span class="txtIcon7"></span>
              <div class="txtMrgn ">
			  <input type="text" value="" class="txtfieldFull" id="mobile" name="mobile" placeholder="Mobile *" ></div>
            </div>
            
			<div class="right-Cont-step1-button-2 space-marging1">
        <span class="left space-marging4">Genrate OTP</span> 
        
        <div class="insuredQuestioncheckBox">
			<div class="insuredQuestioncheckRight">
			
			<div class="insuredQuestioncheck">
					<input type="checkbox" id="test1" name="chkemail" class="chkbox">
					<label for="test1"></label>
			</div>
			
			<div class="insuredQuestionc-left">
				   Email
			</div>
			
			<div class="insuredQuestioncheck">
					<input type="checkbox" id="test2" name="chkmobile" class="chkbox">
					
					<label for="test2"></label>
			</div>
			<div class="insuredQuestionc-left">
				   SMS
			</div>
		
			</div>
		   
			</div>
        <div class="errorTxt" id="customError"></div>
      </div>
		
        </div>
		
        <div class="right-Cont-step1-button-custom">
		<a href="javascript:void(0);" onClick="$('#login_section').show(); $('#forgot_pass_section').hide(); $('#activate_account_section').hide(); return removeErrorLogin();" style="margin-right:5px;"><img src="images/arrowleft.png"  border="0" alt="" title="" > Back </a>
		 <a id="otpBtnClick" href="javascript:void(0);">Send OTP <img src="images/arrow.png"  border="0" alt="" title=""></a>  
         <div id="ajaxLoading" style="display:none; float:right; position:absolute; margin-left:10px;">
		Please wait...<img class="doc_load_img" alt="Loading.." src="images/ajax-loader_12.gif">
		</div>
		</div>
	 
      <div class="spacer2"></div>
	<div class="spacer3"></div>
         <div class="spacer4"></div>
    </div>
	</form>
    </div>  
	</div>
   <!-- Activate Account section ends-->

            <!-- Left container -->
            <div class="Left-Cont">
                <div class="Left-Cont-box"> 
                    <!-- quote -->
                    <div class="Left-Cont-quot"><img src="images/quote.png" border="0" alt=""></div>
                    <!-- quote end-->

                    <h1>WHY RELIGARE?</h1>
                    <p> Religare Health Insurance (RHI), is a specialized Health Insurer offering health insurance services to employees of corporates, individual customers and for financial inclusion as well. Launched in July’12, Religare Health Insurance has made significant progress within a short span of time, and is already operating out of 54 offices with an employee strength of 2150, servicing more than 2.7 million lives (FY’15) across 400+ locations, including over 2500 corporates. </p>
                    <div class="clearfix"></div>
                    <!-- quote slider -->
                    <div class="Left-Cont-test">
                        <div id="owl-demo" class="owl-carousel">
          <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>BEST MEDICLAIM INSURANCE PRODUCT</h2>
            <p>FICCI HEALTHCARE EXCELLENCE AWARDS 2015</p>
          </div>
          <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>BEST HEALTH INSURANCE</h2>
            <p>MINT MEDICLAIM RATINGS 2015</p>
          </div>
          <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>BEST HEALTH INSURANCE COMPANY</h2>
            <p>ABP NEWS BFSI AWARDS 2015</p>
          </div>
		   <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>RISING STAR INSURER</h2>
            <p>INDIA INSURANCE AWARDS 2014</p>
          </div>
		   <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>BEST HEALTH INSURANCE FOR SENIOR CITIZENS</h2>
            <p>VOICE 2014</p>
          </div>
		   <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>"A" RATED HEALTH INSURANCE</h2>
            <p>MINT MEDICLAIM RATINGS 2014</p>
          </div>
		   <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>MOST AFFORDABLE COMPREHENSIVE HEALTH INSURANCE</h2>
           <p>MINT MEDICLAIM RATINGS 2014</p>
          </div>
		   <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>BEST PRODUCT INNOVATION AWARD</h2>
            <p>EDITOR'S CHOICE AWARD FINNOVITI 2013</p>
          </div>
		   <div class="item">
            <div class="top-testimonial"></div>
            <div class="item-img"><img src="images/aw3.jpg" border="0" alt="" title=""> </div>
            <h2>BEST TECHNOLOGY INNOVATION AWARD</h2>
            <p>INDIA INSURANCE AWARDS 2013</p>
          </div>
        </div>
</div>
			<!-- quote slider closed-->
			<div class="spacer2"></div>
		</div>
	</div>

        </div>
    </section>
    <div class="clearfix"></div>
    <!-- body container --> 
    <!-- footer -->
    <footer class="footer">
    
     <?php include("inc/inc.ft-home.php"); ?> 	
    </footer>
    <!-- footer closed -->
	<script src="js/jquery.validate.min.js" type="text/javascript"></script>
	<script src="js/additional-methods.min.js" type="text/javascript"></script>
	<script src="js/owl.carousel.js"></script> 
<!-- Demo --> 

<script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
      navigation : true,
      slideSpeed : 300,
      paginationSpeed : 400,
      singleItem : true
      });
    });
    </script>	
   <script type="text/javascript">
	// fade out error messages after 3 sec
	//$('#userType-error1').delay(3000).fadeOut();
	$('#userType-error2').delay(3000).fadeOut();
	
		$("#loginForm").validate({
			ignore: [], // this will allow to validate any hidden field inside a form
		rules :{
			"password" : {
				required : true,
				
				//noStartEndWhiteSpaces: true
			},
			"username" : {
			   required : true,
			},
			"userType" : {
			   required : true,
			}
				
		},
		messages :{
			"password" : {
				required : 'Please enter password'
			},
			"username" : {
				required : 'Please enter username'
			},
			"userType" :{
				required:'Please select your user type' 
			}
		},
		
		errorElement: "div"		
});
$("#forgotForm").validate({
	ignore: [], // this will allow to validate any hidden field inside a form
		rules :{
			"emailid" : {
				required : true,
				email:true
			},
			"userType2" : {
			   required : true,
			}
				
		},
		messages :{
			"emailid" : {
				required : 'Please enter email id'
			},
			"userType2" :{
				required:'Please select your user type' 
			}
		},
		
		errorElement: "div"		
});
// activate account form validation code 
$("#activateAccountForm").validate({
	ignore: [], // this will allow to validate any hidden field inside a form
			groups: { // added to display single custom error message for OTP Method
            chkboxrole: "chkemail chkmobile"
			},
			// this condition will work to display error message for check box at custom place
			errorPlacement: function(error, element) {
				if (element.attr("name") == "chkemail" )
					error.insertAfter(".errorTxt");
				else if  (element.attr("name") == "chkmobile" )
					error.insertAfter(".errorTxt");
				else
					error.insertAfter(element);
			},
		rules :{
			"emailidActivate" : {
				required : true,
				email:true
			},
			"userType3" : {
			   required : true,
			},
			"chkemail": {
            require_from_group: [1, '.chkbox']
			},
			"chkmobile": {
            require_from_group: [1, '.chkbox']
			},
			"mobile" : {
			   number: true,
			   minlength: 10,
			   maxlength: 10,
			required: {
				depends: function() {
					return $('#test2').is(":checked");			
				}
			}
			},
		},
		messages :{
			"emailidActivate" : {
				required : 'Please enter email id'
			},
			"userType3" :{
				required:'Please select your user type' 
			},
			"mobile" :{
				required:'Please enter Mobile No' 
			},
			"chkemail" :{
				require_from_group:'Please select generate OTP method.' 
			},
			"chkmobile" :{
				require_from_group:'Please select generate OTP method.' 
			},
		},
		
		errorElement: "div"		
});
// custom rule for checking . and character after . for email validation
jQuery.validator.addMethod("email", function (value, element) {
	return this.optional(element) || (/^[a-zA-Z0-9]+([-._][a-zA-Z0-9]+)*@([a-zA-Z0-9]+(-[a-zA-Z0-9]+)*\.)+[a-zA-Z]{2,4}$/.test(value) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(value));
}, 'Please enter valid email address.');
    </script>
<script>
$("#otp").keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
        //$("registrationForm").submit();
		$('#confirmOTP').trigger('click');   
    }
});
$("form input").keypress(function (e) { // code to make working enter key to submit
   if(e.which === 13) {
	   $(this).submit();
      //$("form").submit();
   }
});

$('#activate_account_section').hide();
</script>
<script type="text/javascript">
$(function() {
 $('#otpBtnClick').click(function(){
	$('#activateAccountForm').validate();
	if ($('#activateAccountForm').valid()) // check if form is valid
	{
	   $.ajax({
		type : 'POST',
		data : $('#activateAccountForm').serialize(), // serialize() function will post all input data to page
		url  : 'checkUserDetails.php',
		beforeSend: function () {
		$("#ajaxLoading").css("display", "inline");
		$("#ajaxLoading").show();
	   },
		success : function(data)
			{
			if(data == 'success')
			{
				window.location.href="addOTP.php";
				return true;
			}
			else if(data == 'emailnotexists'){
				
				$("#userType-error3").html("Email ID does not exists.");
				return false;
			}
			else if(data == 'failure'){
				$("#userType-error3").html("Please try after some time.");
				return false;
			}
			
			},
			complete: function(){
			$('#ajaxLoading').hide();
			}
	}); 
	}
	else 
	{
		return false;
	}
});
});
</script>
 
 <?php 
if($type == 'deactivated'){ // condition check to show hide login and forgot password and Activate account section
?>	
<script>
$('#login_section').hide();
$('#forgot_pass_section').hide();
$('#activate_account_section').show();
</script>	
<?php }
else
{ ?>
<script>
$('#activate_account_section').hide();	
</script>
<?php }
?>
<?php 
if($type == 'forgotpassword'){
?>	
<script>
$('#login_section').hide();
$('#forgot_pass_section').show();
$('#activate_account_section').hide();
</script>	
	
<?php }
else
{ ?>
<script>
$('#forgot_pass_section').hide();
$('#activate_account_section').hide();	
</script>
<?php }
?> 
<script>
$(".chkbox").each(function()
{
$(this).change(function()
{
	$(".chkbox").prop('checked',false);
	$(this).prop('checked',true);
});
});
	
$(".editQuote, .editNav").click(function(evt) {
	$(".selectParent select").removeAttr('disabled');
	$(".selectParent").addClass('selectParentEnable');
	$("#mobid").show();
	$(".tooltipgr").hide();
	$(".editQuote").hide();
	$("#submitid").show();
	$(".yourQuoteSummary").show();
});

$(".maleIcon").click(function() {
	$(".maleIcon").toggleClass('maleIconActive');
	$(".femaleIcon").removeClass('femaleIconActive');
});
$('.forgot_pass').click(function(){
	$('#login_section').hide();
	$('#activate_account_section').hide();
    $('#forgot_pass_section').slideToggle('slow');
});
$('.activate_account').click(function(){
	$('#login_section').hide();
	$('#forgot_pass_section').hide();
    $('#activate_account_section').slideToggle('slow');
});
$(".femaleIcon").click(function() {
	$(".maleIcon").removeClass('maleIconActive');
	$(".femaleIcon").toggleClass('femaleIconActive');
});
    </script>
<?php  
if($browser['browser'] == 'IE' && $browser['version'] == '8.0'){ ?>
<script type="text/javascript" src="js/jquery.js"></script>   
<?php } else {  ?>
<script  src="js/jquery.min.js"></script> 
<?php } ?>

<?php include_once('feedback_mail.php'); ?> 
<script src="js/bootstrap.min.js"></script>
<script src="js/datepiker.js"></script> 
<script>
 var DT = jQuery.noConflict();
DT(function() {
DT('#datepicker').datepicker( {
        changeMonth: true,
        changeYear: true,
	    yearRange: "-99:-0",
	    dateFormat: "dd/mm/yy"
 
    });
});
</script>
</div>

</body>
</html>