 <?php include_once("conf/conf.php");
include_once("conf/common_functions.php");
include_once("SMTPRel/class.phpmailer.php");
$emailAddress = sanitize_data_email(@$_POST['emailaddress']);
$firstname = sanitize_data(@$_POST['firstname']);
$downloademailid = sanitize_data(@$_POST['downloademailid']);
$pagename = sanitize_data(@$_POST['pagename']);
if (trim(@$emailAddress) != '') {
    switch ($pagename) {
        case 'CENTER': {
				$pageList =  xml2array(@file_get_contents(ADMINURL.'resource/download_centre.xml'),'1','');
                $pageListArray = $pageList['DownloadCentre'];
                $fileList = download_page_array($pageListArray);
                if (!empty($fileList)) {
                    for ($k = 0; $k < count($fileList); $k++) {
                        if ($fileList[$k]["downloadId"] == $downloademailid) {
                            $downloadTitle = $fileList[$k]["downloadTitle"];
                            $fileName = $fileList[$k]["fileName"];
                        }
                    }
                }
                $subject = 'Download Center';
                break;
            }
        case 'T': {
                $pageList = xml2array(@file_get_contents(ADMINURL . 'resource/tools.xml'), '1', '');
                $pageListArray = $pageList['Tools'];
                $fileList = tools_page_array($pageListArray);
                if (!empty($fileList)) {
                    for ($k = 0; $k < count($fileList); $k++) {
                        if ($fileList[$k]["toolsId"] == $downloademailid) {
                            $downloadTitle = $fileList[$k]["downloadTitle"];
                            $fileName = $fileList[$k]["fileName"];
                        }
                    }
                }
                $subject = 'Others';
                break;
            }
        case 'CLAIMFORMS': {
                $pageList = xml2array(@file_get_contents(ADMINURL.'resource/claimforms.xml'), '1', '');
                $pageListArray = $pageList['ClaimForms'];
                $fileList = claim_forms_page_array($pageListArray);
                if (!empty($fileList)) {
                    for ($k = 0; $k < count($fileList); $k++) {
                        if ($fileList[$k]["claimformsId"] == $downloademailid) {
                            $downloadTitle = $fileList[$k]["downloadTitle"];
                            $fileName = $fileList[$k]["fileName"];
                        }
                    }
                }
                $subject = 'Claim Forms';
                break;
            }
        case 'GOODNESSWALLPAPERS': {
                $subject = 'Policy Terms and Conditions';
                $pageList = xml2array(@file_get_contents(ADMINURL.'resource/goodnesswallpapers.xml'), '1', '');
                $pageListArray = $pageList['GoodnessWallpapers'];
                $fileList = goodnesswallpapers_page_array($pageListArray);
                if (!empty($fileList)) {
                    for ($k = 0; $k < count($fileList); $k++) {
                        if ($fileList[$k]["goodnesswallpapersId"] == $downloademailid) {
                            $downloadTitle = $fileList[$k]["downloadTitle"];
                            $fileName = $fileList[$k]["fileName"];
                        }
                    }
                }
                break;
            }
    }
    $bodytext = '<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
<table width="600" border="0" align="center" cellpadding="0" cellspacing="0">
  <tr>
    <td height="70" align="left" valign="middle" style="border-bottom:1px solid #0f6639;"><img src="img/religare_logo.jpg" alt="Religare Health Insurance" style="padding-left:6px;" /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td style="font:normal 11px Tahoma, Arial, Helvetica, sans-serif; color:#424242;">Dear ' . ucwords(@$firstname) . ',<br /><br />
Greetings !!!<br /><br />
</td>
  </tr>
  <tr><td style="font:normal 12px Tahoma, Arial, Helvetica, sans-serif; color:#424242;">Please see the attached file as requested by you.</td></tr>
   <tr><td style="font:normal 12px Tahoma, Arial, Helvetica, sans-serif; color:#424242;">If you cannot find the attached in this mail. Please refer the following link: &nbsp;&nbsp;&nbsp;  <a target="_blank" href="https://www.religarehealthinsurance.com/download_file.php?fileName=' . $fileName . '">' . stripslashes(base64_decode($downloadTitle)) . '</a></td></tr>

  <tr>
    <td style="font:normal 11px Tahoma, Arial, Helvetica, sans-serif; color:#424242;"><br />We hope that the information provided by us will be useful to you.<br /><br />
Regards,<br />
Religare Health Insurance Co. Ltd<br /><br /></td>
  </tr>

</table>
';
$email = new PHPMailer();
$email->From      = 'rhiclportaladmin@religarehealthinsurance.com';
$email->FromName  = 'Religare Health Insurance';
$email->Subject   = $subject;
$email->Body      = $bodytext;
$email->AddAddress($emailAddress);
$file_to_attach = '../siteupload/doc/'.$fileName;
//$file_to_attach = '23803Care_-_Health_Insurance_plan_Brochure.pdf';
$email->IsHTML(true);
$email->AddAttachment($file_to_attach , $fileName);
///print $fileName;exit;
if(!$email->Send()) {
echo 'Message was not sent.';
echo 'Mailer error: ' . $email->ErrorInfo;
} else {
echo 'Message has been sent.';
}
}
?>