﻿<?php 
/*****************************************************************************
* COPYRIGHT
* Copyright 2016 Qualtech Consultants Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
* $Author: Chandan Singh $
*
****************************************************************************/
include("conf/conf.php");
include("inc/inc-header-registration.php");
include_once("conf/common_functions.php");
$browser = get_browser(null, true);
?>
<style>
div.error {
	position:absolute;
	margin-top:3px;
	color:red;
	padding:3px;
	text-align:left;
	z-index:1;
	margin-left: -47px;
}
.owl-theme .owl-controls {
 margin-top: -146px;
	}
#chkboxrole-error{
	
}	
</style>
</head>
<body>
<!-- header -->
  <div class="headerMain">
<?php include('pphcheader.php'); ?>
</div>
<link rel="stylesheet" type="text/css" href="css/datepiker.css" >

<!-- body container -->
<section id="body_container"> 
  
  <div class="right-Cont">
      <h1>Individual Policy Registration</h1>
  
    <!-- Heding top  closed-->
    <div class="clearfix"></div>
    <div class="spacer4"></div>
    
    <!-- Step start -->
   
  <?php
  $fullname     = @$_SESSION['fullname'];
  $dob          = @$_SESSION['dob'];
  $email        = @$_SESSION['email'];
  $policyNo     = @$_SESSION['policyNumber'];
  $customerId   = @$_SESSION['customerId'];
  $gender       = @$_SESSION['gender'];
  $mobile       = @$_SESSION['mobile'];

  $fname = @$fullname[2] ? $fullname[0] . ' ' . $fullname[1] : $fullname[0];
  $lname = @$fullname[2] ? $fullname[2] : $fullname[1];

////$policyRegWithEmailid  ='';

if(isset($_SESSION['policyRegWithEmailid']) && $_SESSION['policyRegWithEmailid'] !='')
{
	$policyRegWithEmailid = @$_SESSION['policyRegWithEmailid'];
}
else{
	$policyRegWithEmailid ='';
}
 ?>
<div class="right-Cont-step1 ">
<!-- <h2>Welcome Kawaljet -</h2>-->
<div style="padding-top:4%; height:20px; margin-bottom:10px; color: red; position:absolute;" id="emailAlreadyReg"></div>
<div style="padding-top:4%; height:20px; margin-bottom:10px; color: red; position:absolute; display:none;" id="policyRegWithEmailid">Your Policy is registered with <?php echo $policyRegWithEmailid; ?> Email id. If you wish to create new account, We might send you an otp on your registered email. </div>
<h3 style="padding-top:10%;">You are our valued customer !</h3>
<p>Please verify the details and register your account -</p>
<div class="spacer3"></div>
<form action="individual-policy-registration-step3.php" name="registrationForm1" id="registrationForm1" method="post">
      <div class="middleContainerBox space-marging-1">
        	<div class="graytxtBox3 space-marging "><span class="txtIcon1"></span>
				<div class="txtMrgn "><div class="txtfieldFull" ><?php echo ($fname)?$fname:"Not Available";?></div>
				<input type="hidden" value="<?php echo @$fname;?>" name="firstName" class="txtfieldFull"  id="firstName"  placeholder="First Name *" >
				</div> 
            </div>
            
            <div class="graytxtBox3"><span class="txtIcon1"></span>
              <div class="txtMrgn"><div class="txtfieldFull" ><?php echo $lname?$lname:"Not Available";?></div>
			  <input type="hidden" value="<?php echo @$lname;?>" name="lastName" class="txtfieldFull"  id="lastName" placeholder="Last Name *">
                 </div>
            </div>
            
            <div class="graytxtBox3"><span class="txtIcon4"></span>
              <div class="txtMrgn"><div class="txtfieldFull" ><?php echo $dob?$dob:"Not Available";?></div>
			  <input type="hidden" value="<?php echo @$dob;?>" name="dob" class="txtfieldFull"  id="dob" placeholder="DOB *">
                  </div>
            </div>
            
            <div class="graytxtBox3"><span class="txtIcon5"></span>
<!--              <div class="txtMrgn"><input type="email" value="Email *" name="email" class="txtfieldFull" required id="email"  onFocus="if (this.value == 'Email *') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Email *';}">
                  </div>-->
               <!-- <div class="txtMrgn"><div class="txtfieldFull" ><?php // echo emailMasking(@$email)?emailMasking($email):"Not Available";?></div> -->
				<input type="text" value="<?php echo @$email?$email:"Not Available";?>" name="email" class="txtfieldFull" id="email"  onkeyup="updateEmailorMobile(this.id);" placeholder="Email *">
                <input type="hidden" value="<?php echo @$email;?>" name="emailhide" class="txtfieldFull" id="emailhide"  >
                  </div>
            </div>
            <div class="graytxtBox3"><span class="txtIcon1"></span>
              <div class="txtMrgn"><div class="txtfieldFull" ><?php echo @$customerId?$customerId:"Not Available"; ?></div>
			  <input type="hidden" value="<?php echo @$customerId;?>" name="customerId" class="txtfieldFull" readonly id="customerId"  placeholder="Customer ID *">
                  </div>
            </div>
            <div class="graytxtBox3"><span class="txtIcon7"></span>
<!--            <div class="txtMrgn"><input type="text" value="Mobile *" name="mobile" class="txtfieldFull" readonly id="mobile" maxlength="10"  onFocus="if (this.value == 'Mobile *') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Mobile *';}">
                </div>-->
                <div class="txtMrgn"><div class="txtfieldFull" ><?php echo mobileMasking(@$mobile)?mobileMasking($mobile):"Not Available"; ?></div>
				<input type="hidden" value="<?php echo mobileMasking(@$mobile);?>" name="mobilehide" class="txtfieldFull" id="mobile"   onkeyup="updateEmailorMobile(this.id);" placeholder="Mobile *" readonly>
                <input type="hidden" value="<?php echo @$mobile;?>" name="mobile" class="txtfieldFull" id="mobilehide">
                 </div>
            </div>
<!--            <div class="graytxtBox3"><span class="txtIcon2"></span>
                <div class="txtMrgn">
                <input type="text" value="Password *" class="txtfieldFull" id="fake_Password" style="display:none"  name="fake_Password" onFocus="if (this.value == 'Password *') {this.value = '';}" onBlur="if (this.value == '') {this.value = 'Password *';document.registrationForm1.password.value = '';}">
              <input type="password" value="" class="txtfieldFull" id="password" required name="password"  maxlength="25">  
                </div>
            </div>-->
            <div class="clearfix"></div>
		 <?php if(empty($email) || empty($mobile)){ ?>
          <!-- span class="left space-marging4">Please contact <a href="mailto:customerfirst@religarehealthinsurance.com" target="_top">Customer Care</a> to get your policy registered!</span removed by cks --> 
		  <span class="left space-marging4">Email or Mobile number is not available. Please Contact <a href="mailto:customerfirst@religarehealthinsurance.com" target="_top">Customer Care</a> </span>
         <?php } else {?>
            <div class="right-Cont-step1-button-2 space-marging1">
        
        <span class="left space-marging4">Generate OTP</span> 
        
        <div class="insuredQuestioncheckBox">
			<div class="insuredQuestioncheckRight">
			
			<div class="insuredQuestioncheck">
					<input type="checkbox" id="test1" name="chkemail" class="chkbox">
					<label for="test1"></label>
			</div>
			<div class="errorTxt" id="customError"></div>
			<div class="insuredQuestionc-left">
				   Email
			</div>
			
			<div class="insuredQuestioncheck">
					<input type="checkbox" id="test2" name="chkmobile" class="chkbox">
					
					<label for="test2"></label>
			</div>
			<div class="insuredQuestionc-left">
				   SMS
			</div>
		
			</div>
		   
			</div>
       
      </div>
            <div class="spacer3"></div>
            <div class="graytxtBox3">
              <div class="txtMrgn-1"><input type="text" value="" class="txtfieldFull" id="emailormobile" name="textfield2" readonly>
              <input type="hidden" value="" class="txtfieldFull" id="emailormobilehide" name="textfield2hide">
                  </div>
            </div>
            <div class="right-Cont-step1-button space-marging1" style="padding-top:2%;">
        <a id="btnClick" href="javascript:void(0);">Send OTP <img src="images/arrow.png"  border="0" alt="" title=""></a> 
		<a id="proceedToMapBtn" href="javascript:void(0);" style="display:none;">PROCEED TO MAP POLICY<img src="images/arrow.png"  border="0" alt="" title=""></a> 
		
		<a id="btnClickNewAccount" href="javascript:void(0);" style="display:none;">Send OTP on Reg Email<img src="images/arrow.png"  border="0" alt="" title=""></a>
		
        <div id="ajaxLoading" style="display:none; float:right; position:absolute; margin-left:10px;">
		Please wait...<img class="doc_load_img" alt="Loading.." src="images/ajax-loader_12.gif">
		</div>
      </div>
         <?php } ?>
        </div>
		<?php 
		 if(isset($_SESSION['successSameAccount']) && $_SESSION['successSameAccount'] =='successSameAccount'){
			?>
			<input type="hidden" name="successSameAccount" value="<?php echo $_SESSION['successSameAccount']; ?>" />
		 <?php 
		 }
		?>
		<?php 
		 if(isset($_SESSION['newpolicyNewAccount']) && $_SESSION['newpolicyNewAccount'] =='newpolicyNewAccount'){
			?>
			<input type="hidden" name="newpolicyNewAccount" value="<?php echo $_SESSION['newpolicyNewAccount']; ?>" />
		 <?php 
		 }
		?>
		
</form>
         <div class="spacer4"></div>
    </div>
    </div>
  
  <!-- Left container -->
     <div class="Left-Cont">
       <?php include("inc/inc.left-registration.php");?>
    </div>
  
  </div>
</section>
<div class="clearfix"></div>
<!-- body container --> 

<script type="text/javascript">
$("input").keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
        //$("registrationForm").submit();
		$('#btnClick').trigger('click');   
    }
});
</script>	
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/additional-methods.min.js" type="text/javascript"></script>	
   <script type="text/javascript">
		$("#registrationForm1").validate({
			ignore: [], // this will allow to validate any hidden field inside a form
			groups: { // added to display single custom error message for OTP Method
            chkboxrole: "chkemail chkmobile"
			},
			// this condition will work to display error message for check box at custom place
			errorPlacement: function(error, element) {
				if (element.attr("name") == "chkemail" )
					error.insertAfter(".errorTxt");
				else if  (element.attr("name") == "chkmobile" )
					error.insertAfter(".errorTxt");
				else
					error.insertAfter(element);
			},
			
		rules :{
			"firstName" : {
				required : true,
			},
			"lastName" : {
			   required : true,
			},
			"dob" : {
			   required : true,
			},
			"email" : {
			   required : true,
			   email: true,
			},
			"mobile" : {
			  // number: true,
			  required : true,
			   minlength: 10,
			   maxlength: 10,
			},
			"chkemail": {
            require_from_group: [1, '.chkbox']
			},
			"chkmobile": {
            require_from_group: [1, '.chkbox']
			},
			
		},
		messages :{
			"firstName" : {
				required : 'Please enter first name',
			},
			"lastName" : {
				required : 'Please enter last name'
			},
			"dob" :{
				required:'Please enter DOB' 
			},
			"email" :{
				required:'Please enter Email ID' 
			},
			"mobile" :{
				required:'Please enter Mobile No' 
			},
			"chkemail" :{
				require_from_group:'Please select generate OTP method.' 
			},
			"chkmobile" :{
				require_from_group:'Please select generate OTP method.' 
			},
		},
		
		
		errorElement: "div",
		//errorLabelContainer: '.errorTxt'		
});

// custom rule for checking . and character after . for email validation
jQuery.validator.addMethod("email", function (value, element) {
	return this.optional(element) || (/^[a-zA-Z0-9]+([-._][a-zA-Z0-9]+)*@([a-zA-Z0-9]+(-[a-zA-Z0-9]+)*\.)+[a-zA-Z]{2,4}$/.test(value) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(value));
}, 'Please enter valid email address.');

</script>

<script type="text/javascript">
$(function() {
 $('#btnClick').click(function(){
	$('#registrationForm1').validate();
	if ($('#registrationForm1').valid()) // check if form is valid
	{
	   $.ajax({
		type : 'POST',
		//data : {'email':email,'mobile':mobile,'customerId':customerId,'policyNo':policyNo,'OTPType':OTPType, 'msgTo':msgTo},
		data : $('#registrationForm1').serialize() + '&policyNo=<?php echo $policyNo;?>' + '&gender=<?php echo $gender;?>',
		url  : 'registration.php',
		beforeSend: function () {
		$("#ajaxLoading").css("display", "inline");
		$("#ajaxLoading").show();
	   },
		success : function(data)
			{
			if(data == 'success' )
			{
				$('#registrationForm1').submit();
				//top.location='individual-policy-registration-step3.php';
				return true;
			}
			else if(data == 'emailareadyregistered'){
				
				$("#emailAlreadyReg").html("Account already exist with same email id, if you wish to map this policy with your existing account, Please click on PROCEED TO MAP POLICY button.");
				$("#proceedToMapBtn").show();
				$("#btnClick").hide();
			}
			else if(data == 'policyRegWithEmailid'){
				
				//$("#emailAlreadyReg").html("Your Policy is registered with <?php echo $policyRegWithEmailid; ?> Email id. If you wish to create new account, We might send you an otp on your registered email.");
				$("#policyRegWithEmailid").show();
				$("#proceedToMapBtn").hide();
				$("#btnClickNewAccount").show();
				$("#btnClick").hide();
			}
			
			else{
				return false;
			}
			
			},
			complete: function(){
			$('#ajaxLoading').hide();
			}
	}); 
	}
	else 
	{
		return false;
	}
});
});
</script>
<script type="text/javascript">
$(function() {
 $('#proceedToMapBtn').click(function(){
	$('#registrationForm1').validate();
	if ($('#registrationForm1').valid()) // check if form is valid
	{
	   $.ajax({
		type : 'POST',
		//data : {'email':email,'mobile':mobile,'customerId':customerId,'policyNo':policyNo,'OTPType':OTPType, 'msgTo':msgTo},
		data : $('#registrationForm1').serialize() + '&policyNo=<?php echo $policyNo;?>' + '&gender=<?php echo $gender;?>' + '&formtype=newpolicySameAccount',
		url  : 'registration.php',
		beforeSend: function () {
		$("#ajaxLoading").css("display", "inline");
		$("#ajaxLoading").show();
	   },
		success : function(data)
			{
			if(data == 'success')
			{
				$('#registrationForm1').submit();
				//top.location='individual-policy-registration-step3.php';
				return true;
			}
			else if(data == 'successSameAccount')
			{
				//top.location='individual-policy-registration-step3.php';
				$('#registrationForm1').submit();
				return true;
			}
			else if(data == 'emailareadyregistered'){
				
				$("#emailAlreadyReg").html("Account already exist with same email id, if you wish to map this policy with your existing account, Please click on PROCEED TO MAP POLICY button.");
				$("#proceedToMapBtn").show();
				$("#btnClick").hide();
				$("#btnClickNewAccount").hide();
				
			}
			else{
				return false;
			}
			
			},
			complete: function(){
			$('#ajaxLoading').hide();
			}
	}); 
	}
	else 
	{
		return false;
	}
});
});
</script>
<script type="text/javascript">
$(function() {
 $('#btnClickNewAccount').click(function(){
	$('#registrationForm1').validate();
	if ($('#registrationForm1').valid()) // check if form is valid
	{
	   $.ajax({
		type : 'POST',
		//data : {'email':email,'mobile':mobile,'customerId':customerId,'policyNo':policyNo,'OTPType':OTPType, 'msgTo':msgTo},
		data : $('#registrationForm1').serialize() + '&policyNo=<?php echo $policyNo;?>' + '&gender=<?php echo $gender;?>' + '&formtype=newpolicyNewAccount',
		url  : 'registration.php',
		beforeSend: function () {
		$("#ajaxLoading").css("display", "inline");
		$("#ajaxLoading").show();
	   },
		success : function(data)
			{
			if(data == 'policyRegWithEmailidSuccess')
			{
				$('#registrationForm1').submit();
				
				return true;
			}
			
			else if(data == 'failurepolicyRegWithEmailid'){
				
				$("#emailAlreadyReg").html("Fail");
			}
			else{
				return false;
			}
			
			},
			complete: function(){
			$('#ajaxLoading').hide();
			}
	}); 
	}
	else 
	{
		return false;
	}
});
});
</script>

<script>
$(document).ready(function(){
$('#password').hide(); 
// Show the fake pass (because JS is enabled)
$('#fake_Password').show(); 

// On focus of the fake password field
$('#fake_Password').focus(function(){
    $(this).hide(); //  hide the fake password input text
    $('#password').show().focus(); // and show the real password input password
}); 
// On blur of the real pass
$('#password').blur(function(){  
    if($(this).val() == ""){ // if the value is empty, 
        $(this).hide(); // hide the real password field
        $('#fake_Password').show(); // show the fake password
    }
    // otherwise, a password has been entered,
    // so do nothing (leave the real password showing)
}); 

});

$(document).ready(function(){
    $('#emailormobile').val('');
    $('#emailormobilehide').val('');
    $(".chkbox").each(function()
    {
        $(".chkbox").prop('checked',false);
    });
});
    
$(".chkbox").each(function()
{
    $(this).change(function()
    {
        $(".chkbox").prop('checked',false);
        $(this).prop('checked',true);
        var boxName = $(this).attr("name").substring(3);
		$('#emailormobile').val($('#'+boxName).val());
        $('#emailormobilehide').val($('#'+boxName+'hide').val());
    });
});

function updateEmailorMobile(id)
{
    var chkd='';
    if(id == 'mobile')
    {
        chkd = $('#test2').is(':checked');
    }
    else
    {
        chkd = $('#test1').is(':checked');
    }
    if(chkd)
    {
        $('#emailormobile').val($('#'+id).val());
        $('#emailormobilehide').val($('#'+id).val());
    }
}

function formSubmit(registrationForm1)
{    
    var error = validatePolicyReg1(registrationForm1);    
    
    if(error != false)
    {
//       
        $.ajax({
			type : 'POST',
			
			data : $('#registrationForm1').serialize() + '&policyNo=<?php echo $policyNo;?>' + '&gender=<?php echo $gender;?>',
			url  : 'registration.php',
			success : function(data)
				{
					if(data == 'success')
					{
						$('#registrationForm1').submit();
						return true;
					}
					return false;
				}
        });        
    }
}
</script>
<script src="js/owl.carousel.js" type="text/javascript"></script>	
<script>
    $(document).ready(function() {
      $("#owl-demo").owlCarousel({
		  navigation : true,
		  slideSpeed : 300,
		  paginationSpeed : 400,
		  singleItem : true
      });
    });
</script>
<?php 
if($browser['browser'] == 'IE' && $browser['version'] == '8.0'){ ?>
<script type="text/javascript" src="js/jquery.js"></script>   
<?php } else {  ?>
<script type="text/javascript" src="js/jquery-1.4.4.min.js"></script>
<?php } ?>
<script src="js/datepiker.js"></script> 
<script>
 var DT = jQuery.noConflict();
DT(function() {
DT('#datepicker').datepicker( {
        changeMonth: true,
        changeYear: true,
	    yearRange: "-99:-0",
	    dateFormat: "dd/mm/yy"
 
    });
	DT('#dob').datepicker( {
        changeMonth: true,
        changeYear: true,
	    yearRange: "-99:-0",
	    dateFormat: "yy-mm-dd"
 
    });
	
});
</script>
<!-- body container --> 
<!-- footer -->
<?php include("inc/inc.ft-registration.php"); ?>
<!-- footer closed -->
</div>
</body>
</html>