<?php 
include("conf/conf.php");
include("conf/common_functions.php");
include("inc/inc-header-registration.php"); 
?>
<style>
.owl-theme .owl-controls {
margin-top: -146px;
}
</style>
<!--<script src="js/alice.js" type="text/javascript"></script>-->
</head>
<body>
<!-- header -->
<div class="headerMain">
<?php include('pphcheader.php'); ?>
<link rel="stylesheet" type="text/css" href="css/datepiker.css" >
</div>
<!-- body container -->
<section id="body_container"> 
  <div class="right-Cont">
      <h1>User Registration</h1>
  
    <!-- Heding top  closed-->
    <div class="clearfix"></div>
    <div class="spacer4"></div>
    
    <!-- Step start -->
    
    <div class="right-Cont-step1">
<form action="#" name="registrationForm" id="registrationForm" method="post">
      <div class="middleContainerBox">
        	 <h2>Please select your policy type</h2>
                
<div class="genderBox" style="padding:0;">
    <div class="genderBox">
        <a href="individual-policy-registration.php" ><span class="maleIcon<?php if(@$_REQUEST['s']=='r'){ echo 'maleIconActive'; } ?>"  id="tpaicon">Retail</span></a>
        <a href="corporate_registration.php"> <span class="femaleIcon <?php if(@$_REQUEST['s']=='c'){ echo 'femaleIconActive'; } ?>" id="chainicon">CORPORATE</span></a> 

	</div>
							   
	</div>

	</div>

	</form>
 
	<div class="spacer4"></div>

	<div class="right-Cont-step1-button space-marging1">

	<a style="margin-right:10px;" href="index.php" onClick="$('#login_section').show();
	$('#forgot_pass_section').hide();"><img src="images/arrowleft.png"  border="0" alt="" title=""> Back </a> 
	</div>
    </div>
    </div>
  <!-- Left container -->
    <div class="Left-Cont">
      <?php include("inc/inc.left-registration.php");?>
    </div>

  </div>
</section>
<div class="clearfix"></div>
<!-- body container --> 
<!-- footer -->
<?php include("inc/inc.ft-registration.php"); ?>
<!-- footer closed --> 	
<script>
$(".editQuote, .editNav").click(function (evt) {
    $(".selectParent select").removeAttr('disabled');
	$(".selectParent").addClass('selectParentEnable');
	$("#mobid").show();
	$(".tooltipgr").hide();
	$(".editQuote").hide();
	$("#submitid").show();
	$(".yourQuoteSummary").show();
});

$(".maleIcon").click(function () {
	$(".maleIcon").toggleClass('maleIconActive');
	$(".femaleIcon").removeClass('femaleIconActive');
});
$(".femaleIcon").click(function () {
	$(".maleIcon").removeClass('maleIconActive');
	$(".femaleIcon").toggleClass('femaleIconActive');
});
</script> 
</div>
</body>
</html>