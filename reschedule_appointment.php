<?php /*****************************************************************************
* COPYRIGHT
* Copyright 2013 Catabatic Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: appointment_request_scheduler.php,v 1.0 2015/10/05 05:16:0 PM Amit Kumar Dubey Exp $
* $Author: Amit Kumar Dubey $
*
****************************************************************************/

include("inc/inc.hd.php");  			//include header file to show header on page
if(isset($_SESSION['response_error'])){
	$responseError  = $_SESSION['response_error'];
}else{
	$responseArray = $_SESSION['dataArray'];
}
$flag=0; 


 $planId=sanitize_title(base64_decode(@$_REQUEST['pId']));
//$planId=sanitize_title(base64_decode('1001'));
 $appointmentId=sanitize_title(base64_decode(@$_REQUEST['appId']));
$purchaseId=sanitize_title(base64_decode(@$_REQUEST['purId']));
if($_SESSION['LOGINTYPE']=='CORPORATE'){
	$_SESSION['POLICYID']=$wellnessList[0]['POLICYID'];
	$planCount=checkPlanForEmployee(@$wellnessList[0]['POLICYID'],@$_SESSION['empNo'],@$_SESSION['clientNumber']);//function to fetch records
	
	$planDetails=get_particular_plan_details_for_policy($planId,@$wellnessList[0]['POLICYID']);// fetch plan details for plan id
		$memberList=array();
		if(@$planDetails[0]['PAYMENTOPTIONS']=="BUY"){
			$memberList=get_balance_member_list_for_appointment($purchaseId,$planId,@$_SESSION['COMPANYID']);//function to fetch records
		}else{
			$responseArray['purchaseId']=$purchaseId;
			$responseArray['planId']=$planId;
			$responseArray['companyId']=@$_SESSION['COMPANYID'];
			$responseArray['policyId']=@$wellnessList[0]['POLICYID'];
        $memberList = get_member_list_for_free_appointment(@$responseArray, $relationArray);
    }
	$appDetail=fetchListCondsWithColumn("EMAILID,MOBILENO,ADDRESS,CITYID,STATEID,CENTERNAME,DATE1,DATE2,SLOTID,SELECTEDSLOT,CENTERID,SLOTMAPPINGID","CRMCOMPANYPOSTAPPOINTMENT"," where APPOINTMENTID=$appointmentId");
	$slotDetail=fetchListCondsWithColumn("STARTTIMENAME","CRMSLOTMASTER"," where SLOTID=".$appDetail[0]['SLOTID']."");
	$cityName=fetchListCondsWithColumn("CITYNAME","LWCITYMASTER"," where CITYID=".@$appDetail[0]['CITYID']." and STATEID=".@$appDetail[0]['STATEID']."");
	
}else {
	$appDetail=fetchListCondsWithColumn("*","CRMRETAILPOSTAPPOINTMENT"," where APPOINTMENTID=$appointmentId");
	$slotDetail=fetchListCondsWithColumn("STARTTIMENAME","CRMSLOTMASTER"," where SLOTID=".$appDetail[0]['SLOTID']."");
	//$slotMappingDetail=fetchListCondsWithColumn("STARTTIMENAME","CRMSLOTMASTER"," where SLOTID=".$appDetail[0]['SLOTID']."");
	//$stateName=fetchListCondsWithColumn("STATENAME","LWSTATEMASTER"," where STATEID=".@$appDetail[0]['STATEID']."");
	$cityName=fetchListCondsWithColumn("CITYNAME","LWCITYMASTER"," where CITYID=".@$appDetail[0]['CITYID']." and STATEID=".@$appDetail[0]['STATEID']."");
	
	$planCount=checkPlanForRetEmployee($_SESSION['productId'],@$_SESSION['empNo'],@$_SESSION['customerId']);//function to fetch records
	$planDetails=get_particular_retplan_details_for_policy($planId,@$_SESSION['productId']);// fetch plan details for plan id

}
	
	if(count(@$planDetails)==0){
	echo "<script>window.location.href='health_checkup.php'</script>";
	exit;
	}
	$memberList=array();

$AddAppointment=sanitize_title(@$_POST['AddAppointment']);
if($_SESSION['LOGINTYPE']=='CORPORATE'){
	if($AddAppointment!='' && isset($AddAppointment)){
			$timeslot='slot1';
			if($timeslot=='slot1'){
				$slotmappingid=sanitize_title(@$_POST['timeslot1']);
				$slotmappingid=addslashes(@$slotmappingid);
			}
			if($timeslot=='slot2'){
				$slotmappingid=sanitize_title(@$_POST['timeslot2']);
				$slotmappingid=addslashes(@$slotmappingid);
			}
			$date1=sanitize_data(@$_POST['date1']);
			$date1=date('d-M-Y',strtotime(@$date1));
			$date2=sanitize_data(@$_POST['date2']);
			$date2=date('d-M-Y',strtotime(@$date2));
			$timeslot=sanitize_data(@$_POST['timeslot_update']);
			$timeslot=addslashes(@$timeslot);
			$timeslot='slot1';
			if($timeslot=='slot1'){
				$slotmappingid=sanitize_title(@$_POST['timeslot1']);
				$slotmappingid=addslashes(@$slotmappingid);
			}
			$slotid=sanitize_data(@$_POST['slotid']);
			$slotid=addslashes(@$slotid); 
			$centerName=@$_POST['centerName'];
			$centerName=addslashes(@$centerName);
			$selectedcenterid=@$_POST['selectedcenterid'];
			$selectedcenterid=addslashes(@$selectedcenterid);

			$noofappointment=sanitize_title(@$_POST['noofappointment']); 
			$noofappointment=addslashes(@$noofappointment);
			$maxappointment=sanitize_title(@$_POST['maxappointment']);
			$maxappointment=addslashes(@$maxappointment);
			$entryTime=date('d-m-Y H:i');
			$status='PENDING';
		

			//check condition to insert records
				if($timeslot=='slot1'){
			   $sql="UPDATE  CRMCOMPANYPOSTAPPOINTMENT SET CENTERID='".@$selectedcenterid."',CENTERNAME='".@$centerName."',DATE1='".@$date1."',SELECTEDSLOT='".@$timeslot."',STATUS='PENDING',UPDATEDON='".@$entryTime."',UPDATEDBY='".@$_SESSION['userName']."',SLOTID='".@$slotid."',SLOTMAPPINGID='".@$slotmappingid."' WHERE APPOINTMENTID='".@$appointmentId."'";		//query to update records
				} 				
				if($timeslot=='slot2'){
			   $sql="UPDATE  CRMCOMPANYPOSTAPPOINTMENT SET CENTERID='".@$selectedcenterid."',CENTERNAME='".@$centerName."',DATE2='".@$date2."',SELECTEDSLOT='".@$timeslot."',STATUS='PENDING',UPDATEDON='".@$entryTime."',UPDATEDBY='".@$_SESSION['userName']."',SLOTID='".@$slotid."',SLOTMAPPINGID='".@$slotmappingid."' WHERE APPOINTMENTID='".@$appointmentId."'";		//query to update records
				} 				
			$stdid = @oci_parse($conn, $sql);
			$r = @oci_execute($stdid);
	//update calender data according to appointment
			$appfull='';
			if($noofappointment==$maxappointment){
				$appfull="ISAVAIL='NO' ,";
			}
			if($slotmappingid != $appDetail[0]['SLOTMAPPINGID']){
				$sql3="UPDATE  CRMCALENDERSLOTMAPPING SET NOOFAPPOINTMENT='".@$noofappointment."',$appfull UPDATEDON='".@$entryTime."',UPDATEDBY='".@$_SESSION['userName']."' WHERE ID='".@$slotmappingid."'";	
				$stdid3 = @oci_parse($conn, $sql3);
				$r3 = @oci_execute($stdid3);
			} 
			
	//end of update calender data according to appointment		

		$sql2="INSERT INTO CRMCOMPANYAPPHISTORY(ID,APPOINTMENTID,STATUS,CREATEDON,CREATEDBY) VALUES(CRMCOMPANYAPPHISTORY_SEQ.nextval,".$noofappointment.",'PENDING','".@$entryTime."','".@$_SESSION['userName']."') ";  //query to update status based on ID
		$stdid2 = @oci_parse($conn, $sql2);
		$r2 = @oci_execute($stdid2);

		$from='';
		$subject='';
		$message='';
		$tmslot1=@$timeslot[0]." ".@$timeslot[1];
		$tmslot2=@$timeslot2[0]." ".@$timeslot2[1];
		
		 //SendMailToEmp($EMAILID,$centerName,$EMPLOYEELOCATION,$appointmentDate,$tmslot1,$appointmentDate2,$tmslot2);	//function to send email        
		 //SendMail($EMAILID,$from,$subject,$message,'HR');		//function to send email
		 
			echo '<script type="text/javascript">';
			echo 'window.location.href="existing_appointments.php?message=Inserted Successfull";';
			echo '</script>';
			echo '<noscript>';
			echo '<meta http-equiv="refresh" content="0;url=existing_appointments.php?message=Inserted Successfull" />';
			echo '</noscript>';
			//	echo '<meta http-equiv="refresh" content="0;URL="existing_appointments.php?message=Inserted Successfull" />';
			//header("Location: existing_appointments.php?message=Inserted Successfully");//locate to existing_appointments.php
			exit;	
	}
		
} else {
if($AddAppointment!='' && isset($AddAppointment)){

			$date1=sanitize_data(@$_POST['date1']);
			$date1=date('d-M-Y',strtotime(@$date1));
			$slotmappingid=sanitize_title(@$_POST['timeslot1']);
			$slotmappingid=addslashes(@$slotmappingid);
			$slotid=sanitize_data(@$_POST['slotid']);
			$slotid=addslashes(@$slotid); 
			$centerName=@$_POST['centerName'];
			$centerName=addslashes(@$centerName);
			$selectedcenterid=@$_POST['selectedcenterid'];
			$selectedcenterid=addslashes(@$selectedcenterid);
			$noofappointment=sanitize_title(@$_POST['noofappointment']); 
			$noofappointment=addslashes(@$noofappointment);
			$maxappointment=sanitize_title(@$_POST['maxappointment']);
			$maxappointment=addslashes(@$maxappointment);

			$entryTime=date('d-M-Y');
			$updatedTime=date('d-M-Y h:i:s');
			$productid=$appDetail[0]['PRODUCTID'];
			$productname=$appDetail[0]['PRODUCTNAME'];
			$CUSTOMERID=$appDetail[0]['CUSTOMERID'];
			$NAME=$appDetail[0]['FIRSTNAME'];
			$AGE=$appDetail[0]['DOB'];
			$email=$appDetail[0]['EMAILID'];
			$Numbers=$appDetail[0]['MOBILENO'];
			$location=$appDetail[0]['EMPLOYEELOCATION'];
			$centerName=addslashes($centerName);
			$timeslot1='';
			$timeslot1am='';
			$employeeId=$appDetail[0]['EMPLOYEEID'];
			$PolicyNumber=$appDetail[0]['POLICYNUMBER'];
			$healthcheckupPlans=$appDetail[0]['HEALTHCHECKUPID'];
			$purchaseId=$appDetail[0]['PURCHASEID'];
			$MEMBERID=$appDetail[0]['MEMBERID'];
			$refrencenum='';
			$selectedcenterid=$appDetail[0]['CENTERID'];
			$cityId=$appDetail[0]['CITYID'];
			$stateId=$appDetail[0]['STATEID'];
			$pincode=$appDetail[0]['PINCODE'];
			$caseid='';
			$MEMBERRELATION=$appDetail[0]['MEMBERRELATION'];
			$memberGender=$appDetail[0]['GENDER'];
			$memberSi=$appDetail[0]['SI'];
			$alldata=$appDetail[0]['MMBRWEBSERVICEDATA'];
			$WsPolicyStartDate=$appDetail[0]['WSPOLICYSTARTDATE'];
			$WsPolicyEndDate=$appDetail[0]['WSPOLICYENDDATE'];
			$prevAppId=$appDetail[0]['APPOINTMENTID'];
			$RESCHEDULED='YES';
			$status='PENDING';
			$chkapp=$noofappointment;
			if($chkapp <= $maxappointment){

			//check condition to insert records
		 	 $sql="INSERT INTO CRMRETAILPOSTAPPOINTMENT (APPOINTMENTID,PRODUCTID,PRODUCTNAME,CUSTOMERID,FIRSTNAME,DOB,EMAILID,MOBILENO,EMPLOYEELOCATION,CENTERNAME,DATE1,TIMESLOT1,TIMESLOT1AM,DATE2,TIMESLOT2,TIMESLOT2AM,SELECTEDSLOT,EMPLOYEEID,POLICYNUMBER,STATUS,REPORTSTATUS,REPORTFILE,REPORTUPLOADEDBY,CREATEDON,CREATEDBY,UPDATEDON,HEALTHCHECKUPID,PURCHASEID,MEMBERID,PLANTYPE,HEALTHCHECKUPTYPE,HEALTHCHECKUPPLAN,REFERENCENUMBER,SLOTID,SLOTMAPPINGID,CENTERID,CITYID,STATEID,PINCODE,CASEID,IPADDRESS,MEMBERRELATION,GENDER,SI,MMBRWEBSERVICEDATA,WSPOLICYSTARTDATE,WSPOLICYENDDATE,PREVAPPOINTMENTID) values(CRMRETAILPOSTAPPOINTMENT_SEQ.nextval,'".@$productid."','".@$productname."','".@$CUSTOMERID."','".@$NAME."','".@$AGE."','".@$email."','".@$Numbers."','".@$location."','".@$centerName."','".@$date1."','".@$timeslot1."','".@$timeslot1am."','".@$date2."','".@$timeslot2."','".@$timeslot2am."','".@$timeslot."','".@$employeeId."','".@$PolicyNumber."','".@$status."','NOTUPLOADED','".@$reportfile."','".@$_SESSION['userName']."','".@$entryTime."','".@$_SESSION['userName']."','".@$entryTime."','".@$healthcheckupPlans."','".@$purchaseId."','".@$MEMBERID."','GENERAL','GENERAL','".@$planDetails[0]['PLANNAME']."','".@$refrencenum."','".@$slotid."','".@$slotmappingid."','".@$selectedcenterid."','".@$cityId."','".@$stateId."','".@$pincode."','".@$caseid."','".@$_SERVER['REMOTE_ADDR']."','".@$MEMBERRELATION."','".@$memberGender."','".@$memberSi."','".$alldata."','".$WsPolicyStartDate."','".$WsPolicyEndDate."',".$prevAppId.") "; 
			
			$stdid = @oci_parse($conn, $sql);
			$r = @oci_execute($stdid);
	
		//set rescheduled flag in appointment table
			 $sql_resch="UPDATE CRMRETAILPOSTAPPOINTMENT SET RESCHEDULED='".$RESCHEDULED."',UPDATEDON='".$updatedTime."',UPDATEDBY='".$_SESSION['userName']."' WHERE APPOINTMENTID=".$prevAppId.""; 
			$stdid_resch = @oci_parse($conn, $sql_resch);
			@oci_execute($stdid_resch);
		
		//end of set reschedule in appointment table

			$check1 = @oci_parse($conn, 'SELECT CRMRETAILPOSTAPPOINTMENT_SEQ.currval FROM DUAL');
			@oci_execute($check1);
			$res=@oci_fetch_assoc($check1);
			$rowid=@$res['CURRVAL'];

			 $sql2="INSERT INTO CRMRETAILAPPHISTORY(ID,APPOINTMENTID,STATUS,CREATEDON,CREATEDBY) VALUES(CRMRETAILAPPHISTORY_SEQ.nextval,".$rowid.",'".$status."','".@$entryTime."','".@$_SESSION['userName']."') ";  //query to update status based on ID
			
			$stdid2 = @oci_parse($conn, $sql2);
			$r2 = @oci_execute($stdid2);
	//update calender data according to appointment
			$appfull='';
			if($noofappointment==$maxappointment){
				$appfull="ISAVAIL='NO' ,";
			}
			//if($slotmappingid != $appDetail[0]['SLOTMAPPINGID']){
				$sql3="UPDATE  CRMCALENDERSLOTMAPPING SET NOOFAPPOINTMENT='".@$noofappointment."',$appfull UPDATEDON='".@$entryTime."',UPDATEDBY='".@$_SESSION['userName']."' WHERE ID='".@$slotmappingid."'";	
				$stdid3 = @oci_parse($conn, $sql3);
				$r3 = @oci_execute($stdid3);
			//} 
			//send email after rescheduling appointmentment for retail
					$slotDetails=fetchSlotListByCond(" WHERE CRMCALENDERSLOTMAPPING.ID=".$slotmappingid."");
					$time=$slotDetails[0]['STARTTIMENAME'];
					$AmPm=$slotDetails[0]['STARTTIMENAME'];
					
					 SendSMSforFixedAppointment(@$time,@$AmPm,@$date1,@$centerName,'APP_CRM_RESCHEDULE',$NAME,$centerPhone,$Numbers,$caseid);
					//FunSendMail($email,$status,'POSTAPPOINTMENTHR',@$username,$caseid);
					if($memberGender=='F'){
						$solutation="Ms.";
					} else {
						$solutation="Mr.";
					}
					$namewithgender=$solutation." ".$NAME;
					FunSendChangeAppMailRetailResch(@$email,@$namewithgender,@$centerName,@$location,@$location,@$date1,$date2,$time,@$AmPm,@$timeslot2am,$timeslot2,'POSTAPPOINTMENT_RESCHEDULE_RETAIL',@$caseid);
			
			//end of send email

			
	//end of update calender data according to appointment		
				echo '<script type="text/javascript">';
				echo 'window.location.href="existing_appointments.php?message=Inserted Successfull&tab=ZXhpc3RpbmdhcHA=";';
				echo '</script>';
				echo '<noscript>';
				echo '<meta http-equiv="refresh" content="0;url=existing_appointments.php?message=Inserted Successfull&tab=ZXhpc3RpbmdhcHA=" />';
				echo '</noscript>';
				exit;
			} else{
				echo '<script type="text/javascript">';
				echo 'window.location.href="existing_appointments.php?message=No slots available&tab=ZXhpc3RpbmdhcHA=";';
				echo '</script>';
				echo '<noscript>';
				echo '<meta http-equiv="refresh" content="0;url=existing_appointments.php?message=No slots available&tab=ZXhpc3RpbmdhcHA=" />';
				echo '</noscript>';
				exit;
			}
		
	}

}
	//printr($_SESSION);
?>
<link rel="stylesheet" href="css/jquery-ui.css" />
<script>
function getCenterVal(currentval){
var splitValues=currentval.split('@@');
	$('#center_autocomp').val(splitValues[0]);
	$('#centerName2').val(1);
	$('#selectedcenterid').val(splitValues[1]);
}
function tests(){
$("#selectedcenterid").val('');
$("#centerName2").val(0);
var stateId=$("#state").val();
var cityId=$("#cityId").val();
if(cityId=='' || stateId==''){
	alert('state or city cannot be empty');
	$("#state").focus();
	return false;
}
  var planId=$.trim($("#planId").val());
	$("#center_autocomp").autocomplete({
		source: function(request, response) {
			$.ajax({
			   url: 'ajax/getAutocompleteResRet.php',
				dataType: "json",
				data: {
					q : request.term,
					policyId : $("#policyId").val(),
					compId : $("#company").val(),
					planId : planId,
					state : stateId,
					city : cityId,
				},
				success: function(data) {
			 
				response( $.map( data, function( val,key ) {
			var val2=val.split("@@");	
				  return {
					label: val2[0],
					value: val2[0],
					values: val2[1]
				  }
				}));
			  }
			});
		},
		select:function(event,ui){
		//alert(console.log(ui));
		$("#centerName2").val(1);
		$("#selectedcenterid").val(ui.item.values);
		$("#centerId").val(ui.item.label+'@@'+ui.item.values);
		$("#center_autocomp").val(ui.item.label);
		},
		change: function( event, ui ) {
				//$( "#centerName2" ).val(1);
		  },
		minLength : 3,
	   
	});

}
	$(document).ready(function(){
			$( "#datepicker1" ).datepicker({
					dateFormat: 'dd-M-yy',
					minDate:3,
                                        maxDate:'<?php echo $policyExpiredplus10Days?$policyExpiredplus10Days:''; ?>',
					onSelect:function(selected){
						$.ajax({
							type:'post',
							url:'ajax/get_data.php',
							data:{'type':'GETSLOTBYDATE','datevalue':selected,'centerId':$("#selectedcenterid").val()},
							beforeSend:function() {
							//$(".content").html('<div id="SuccessMsg" >Please Wait...</div>');
							},
							success:function(resp){
								//if($('.radio_1').is(':checked')) {
									$("#timeslotdata1").html(resp);
								//}
							}
						});

					},
				});
			$( "#datepicker2" ).datepicker({
					dateFormat: 'dd-M-yy',
					minDate:3,
					onSelect:function(selected){
						$.ajax({
							type:'post',
							url:'ajax/get_data.php',
							data:{'type':'GETSLOTBYDATE2','datevalue':selected},
							beforeSend:function() {
							//$(".content").html('<div id="SuccessMsg" >Please Wait...</div>');
							},
							success:function(resp){
								$("#timeslotdata2").html(resp);
								/*if($('.radio_2').is(':checked')) {
									$("#timeslotdata2").html(resp);
								} else {
								$("#datepicker2").val('');
								alert('First You have to select Time Slot 2');
								
								}*/
							}
						});

					},
				});
	});			

</script>
<section id="middleContainer">
  <div class="container-fluid">
    <div class="middlebox">
      <div class="col-md-9">
        <div class="middlebox-left">
           <?php include('inc/inc.healthplan_tab.php'); ?>
          <div class="tab-content ">
            <div class="tab-pane" id="buyPlan">
            
            <div class="tab-paneIn">
              <div class="col-md-6">
                
              </div>
              <div class="col-md-12 topMrgn">
              	res1
              </div>
              <div class="clearfix"></div>
              </div>
            </div>
            <div class="tab-pane" id="existingAppointments"><div class="tab-paneIn">2</div></div>
            <div class="tab-pane active" id="myPlans">
            	<div class="tab-paneIn" id="myplanForm">
               	 <div class="title-bg textleft">
                <h1>Appointment Request Scheduler (<?php echo @$planDetails[0]['PLANNAME']?$planDetails[0]['PLANNAME']:$planDetails[0]['PLANNAME2']; ?>)</h1>
                </div>
                <form name="appointmentForm" id="appointmentForm" method="post" >
                 <div class="grayBorder">
                	<h2>User Details</h2>
                    <div class="myPlanForm">
                    
                    	<div class="myPlanformBox myPlanLeft">
                        	<label>Emp Id / Client Id</label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                            	  <input type="text" disabled="disabled" name="EMPLOYEEID" class="email_f txtfield_app txtField" id="EMPLOYEEID" readonly="yes" value="<?php echo ($_SESSION['LOGINTYPE']=='CORPORATE')?@$_SESSION['empId']:@$_SESSION['customerId'];?>">
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        <div class="myPlanformBox myPlanRight">
                        	<label>Email Id <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
					              <input type="text" disabled="disabled" name="EMAILID" id="EMAILID" value="<?php echo @$appDetail[0]['EMAILID']?@$appDetail[0]['EMAILID']:trim(@$_SESSION['employeeEmail']); ?>" class="email_f txtfield_app txtField" maxlength="255" AUTOCOMPLETE="OFF" />
                                	
                                </div>
                            </div>
                        <div class="clearfix"></div></div>
                        <div class="myPlanformBox myPlanLeft">
                        	<label>Mobile <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
	                               <input type="text" disabled="disabled" name="MOBILENO" id="MOBILENO" value="<?php echo @$appDetail[0]['MOBILENO']?@$appDetail[0]['MOBILENO']:trim(@$_SESSION['employeePhone']); ?>" class="email_f txtfield_app txtField"  maxlength="10" AUTOCOMPLETE="OFF" onKeyPress="return kp_numeric(event)" />
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        <div class="myPlanformBox myPlanRight">
                        	<label>Address <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                                	<input type="text" disabled="disabled" name="address" id="address" class="txtField" value="<?php echo @$appDetail[0]['ADDRESS']?stripslashes(@$appDetail[0]['ADDRESS']):''; ?>">
                                </div>
                            </div>
                        <div class="clearfix"></div></div>
                    <div class="clearfix"></div></div>
                    <h2>Location</h2>
                    <div class="myPlanForm">
                    	<div class="myPlanformBox myPlanLeft">
                        	<label>State <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn dropBg">
                            	  <select  id="state" name="state" onChange="getCity(this.value);" disabled="disabled">
                                    <option value="">Select state</option>
                                    <?php
									if($_SESSION['LOGINTYPE']=='CORPORATE'){
									 $state_list=GetStateWhereCenterExist(@$_SESSION['COMPANYID'],@$wellnessList[0]['POLICYID'],$planId);
									} else {
									 $state_list=GetRetStateWhereCenterExist(@$_SESSION['productId'],@$_SESSION['policyNumber'],$planId);
									}
									//echo @$appDetail[0]['STATEID']."<pre>"; print_r($state_list);
										$i=0; 
										while($i < count($state_list)){ ?>
                                    <option value="<?php echo @$state_list[$i]['STATEID'];?>" <?php if(@$appDetail[0]['STATEID']==@$state_list[$i]['STATEID']){ echo "selected='true'"; } ?>><?php echo @$state_list[$i]['STATENAME'];?></option>
                                    <?php  $i++;  }?>
                          	    </select>
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        <div class="myPlanformBox myPlanRight">
                        	<label>City <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn dropBg" id="city">
                            	  <select id="cityId" name="city"  onChange="getCityName(this.value);" disabled="disabled">
                                  	<option value="<?php echo @$appDetail[0]['CITYID'];?>"><?php echo @$cityName[0]['CITYNAME']?stripslashes(@$cityName[0]['CITYNAME']):'Select City';?></option>
                          	    </select>
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        <div class="myPlanformBox myPlanLeft">
                        	<label>Locality/Center</label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                            	  <input type="text" id="center_autocomp" maxlength="255" name="centerName"   class="txtField" value="<?php echo htmlentities(stripslashes(@$appDetail[0]['CENTERNAME']),ENT_QUOTES, "UTF-8"); ?>" onkeyup="return tests();">
                                  <input type="hidden"  name="centerName2" id="centerName2" value="<?php echo @$appDetail[0]['CENTERID']?1:0; ?>" />
                                  <input type="hidden" name="planCenter" id="planId" value="<?php echo @$planId; ?>" >
                                  <input type="hidden" name="selectedcenterid" id="selectedcenterid" class="selectedcenterid" value="<?php echo @$appDetail[0]['CENTERID']; ?>" /> 
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        
                    <div class="clearfix"></div></div>
                    <h2>Appointment Date / Time</h2>
                    <div class="myPlanForm">
                    	<div class="myPlanformBox myPlanLeft">
                        	<label>Date<span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn dateBg">
                            	  <input readonly id="datepicker1" name="date1" type="text" class="txtField" value="<?php $da= @$appDetail[0]['DATE1']?strtotime(@$appDetail[0]['DATE1']):''; if(!empty($da)) { echo date('d-m-Y',$da); } ?>">
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        <div class="myPlanformBox myPlanRight">
                        	<label>Time Slot</label>
                            <div class="inputBox">
                            	<div class="inputBoxIn dropBg" id="timeslotdata1">
                                    <select id="timeslot1" class="txtfield_new update_timeslot1" name="timeslot1">
                                        <option value="<?php echo (@$appDetail[0]['SLOTID']!='')?@$appDetail[0]['SLOTMAPPINGID']:''; ?>"><?php echo (@$appDetail[0]['SELECTEDSLOT']=='slot1')?@$slotDetail[0]['STARTTIMENAME']:'Select Slot'; ?></option>
                                    </select> 
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        
                        
                        
                        
                    <div class="clearfix"></div></div>
                    <div class="myPlanBtn">
					<?php $slotmappingid=sanitize_data(@$appDetail[0]['SLOTMAPPINGID']);
							$applistList=fetchAppListBySlot($slotmappingid); 
							if(count($applistList) > 0){
 	
								if($applistList[0]['MAXAPPOINTMENT'] > $applistList[0]['NOOFAPPOINTMENT']){
									$appexist=1;
								} else {
									$appexist=0;
								}
									$maxapp=$applistList[0]['MAXAPPOINTMENT'];
									$noofapp=$applistList[0]['NOOFAPPOINTMENT'];
							} else {
									$applistList=0;
									$maxapp='';
									$noofapp='';
							}
							?>
                         <input type="hidden" name="pendingapp" id="pendingapp" value="<?php echo $appexist; ?>" />
                         <input type="hidden" name="slotid" id="slotid" value="<?php echo @$appDetail[0]['SLOTID']; ?>" />
                         <input type="hidden" name="maxappointment" id="maxappointment" value="<?php echo $maxapp; ?>" />
                         <input type="hidden" name="noofappointment" id="noofappointment" value="<?php echo $noofapp; ?>" />
                        <input type="hidden" name="policyId" value="<?php if($_SESSION['LOGINTYPE']=='CORPORATE'){ echo $wellnessList[0]['POLICYID'];} else { } ?>" class="policyId" />
                        <input type="hidden" name="AddAppointment" id="button" value="Submit" class="gobtn fr submitbutton" />
                    	<a href="javascript:void(0);" onClick="$('#appointmentForm').submit();" class="submit-btn">Submit ></a><a href="existing_appointments.php?tab=<?php echo base64_encode('existingapp'); ?>" class="cancel-btn">Cancel ></a>
                    </div>
                </div>
                </form>
                <div class="clearfix"></div></div>
            </div>
          </div>
        </div>
        
      </div>
    <div class="col-md-3">
        <?php include("inc/inc.right.php");
        
        if($_SESSION['LOGINTYPE']=='RETAIL'){  
                     $policyExpirydate=strtotime(trim(@$_SESSION['POLICYENDDATE']));
                    if($policyExpirydate < strtotime(date("Y-m-d"))) {
                      $policyExpiredplus10Days = date('d-M-Y', strtotime("-1 day", $policyExpirydate));
                  } else {
                      $policyExpiredplus10Days = date('d-M-Y', strtotime("+10 day", $policyExpirydate));
                  }
         }
        ?>
    </div> 
             
    </div>
  </div>
</section>
<?php include("inc/inc.ft.php"); ?>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/additional-methods.min.js" type="text/javascript"></script>
<script>
 $("#appointmentForm").validate({
    rules :{
		"EMPLOYEEID" : {
		   required : true,
        },
        "EMAILID" : {
            required : true,
			email: true
			//noStartEndWhiteSpaces: true
        },
		"MOBILENO" : {
		   required : true,
		   number: true,
		   maxlength: 10,
		   minlength: 10
        },
		"state" :{
			required : true,
			//specialChars: true
			//noStartEndWhiteSpacesAlpha:true
		},
		"city" :{
			required : true,
			//specialChars: true
			//noStartEndWhiteSpacesAlpha:true
		},
		"centerName" :{
			required : true,
			//specialChars: true
			//noStartEndWhiteSpacesAlpha:true
		},
		"centerId" :{
			required : true,
			//specialChars: true
			//noStartEndWhiteSpacesAlpha:true
		},
		"date1" :{
			required : true,
			//specialChars: true
			//noStartEndWhiteSpacesAlpha:true
		},
		"timeslot1" :{
			required : true,
			//specialChars: true
			//noStartEndWhiteSpacesAlpha:true
		},
		"member[]" :{
			required : true,
			//specialChars: true
			//noStartEndWhiteSpacesAlpha:true
		}
    },
    messages :{
        "EMPLOYEEID" : {
            required : 'Please enter Employee Id'
        },
        "EMAILID" : {
            required : 'Please Enter the Email Id'
        },
		"MOBILENO" : {
            required : 'Please Enter the Mobile No.'
        },
		"state":{
			required: 'Please select State'
		},
		"city":{
			required: 'Please select City'
		},
		"centerName":{
			required: 'Please enter the Center Name'
		},
		"centerId":{
			required: 'Please enter the Center Name'
		},
		"date1":{
			required: 'Please select the Appointment Date'
		},
		"timeslot1":{
			required: 'Please select the Time Slot'
		},
		"member[]":{
			required: 'Please select the member'
		}
    },
	errorElement: "div",
	errorPlacement: function(error, element) {
     if (element.attr("name") == "member[]") {

       // do whatever you need to place label where you want

         // an example
         error.insertBefore( $("#membererror") );

         // just another example
         //$("#yetAnotherPlace").html( error );  

     } else {

         // the default error placement for the rest
         error.insertAfter(element);

     }
   },submitHandler:function(){
	   	var expireDateStr =$.trim($("#datepicker1").val());
		//alert(expireDateStr);
		var expireDateArr = expireDateStr.split("-");
		var expireDate = new Date(+expireDateArr[2]+"-"+expireDateArr[1]+"-"+expireDateArr[0]);
		var todayDate = new Date();
		
		var todayDate = addDays(new Date(), 3);
		if (expireDate < todayDate) {
		   alert("Appointment date cannot be less than current date.Please select another.");
		   $("#datepicker1").focus();
			return false;
		}else{
		return true;	
		}

	   }		
  }); 


function addDays(theDate, days) {
    return new Date(theDate.getTime() + days*24*60*60*1000);
}


</script>
<style>
div.error {
	position:absolute;
	margin-top:14px;
	color:red;
	background-color:#fff;
	padding:3px;
	text-align:left;
	z-index:1;
	margin-left: -17px;
}
div.membertypeerror .error{
	margin-top:-9px !important;
	margin-left:0px !important;
}
</style>
