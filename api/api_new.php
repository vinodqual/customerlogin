<?php	
//$policyURL="http://10.216.6.50:80/cordys/com.eibus.web.soap.Gateway.wcp?organization=o=ReligareHealth,cn=cordys,cn=devinst,o=religare.in";
$policyURL="http://cordysstg.religare.in/cordys/com.eibus.web.soap.Gateway.wcp?organization=o=ReligareHealth,cn=cordys,cn=defaultInst,o=religare.in";
include('apiObject2Array_new.php');
	function getPolicyEnquiry($query){
		foreach($query as $key=>$value)	{
			$$key=$value;
		}
		$xml_data = 
			"<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">	
	  <SOAP:Body>
		<getPolicyEnquiry xmlns=\"http://composite.groupinsurance.religare.in\">
		   	<RMBLPHONE xmlns=\"http://schemas.cordys.com/default\"/>
		  	<EMPNO xmlns=\"http://schemas.cordys.com/default\">".@$EMPNO."</EMPNO>
		  	<RINTERNET xmlns=\"http://schemas.cordys.com/default\"/>
		 	<CHDRNUM xmlns=\"http://schemas.cordys.com/default\">".@$CHDRNUM."</CHDRNUM>
			<CLTDOBX xmlns=\"http://schemas.cordys.com/default\">".@$CLTDOBX."</CLTDOBX>
			<CLNTNUM xmlns=\"http://schemas.cordys.com/default\">".@$CLNTNUM."</CLNTNUM>
			<PASPRTNO xmlns=\"http://schemas.cordys.com/default\"/>    
		</getPolicyEnquiry>
	  </SOAP:Body>
</SOAP:Envelope>";	
				$fileName='Policy_Enquiry_'.time();
			$dataArr = getXMLResponse($xml_data,'PolicyEnquiry',$fileName);
		return $dataArr;
	}
function getALClaimEnquiryWithPeriod($query){
		foreach($query as $key=>$value)	{
			$$key=$value;
		}
		$xml_data = 
			"<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">
  <SOAP:Body>
    <GetZclhpfObjectbyParameters xmlns=\"http://esbinternal.cordys.com\" preserveSpace=\"no\" qAccess=\"0\" qValues=\"\">
      <GCSTS>".@$GCSTS."</GCSTS>
      <strtdate>".@$strtdate."</strtdate>
      <endate>".@$endate."</endate>
      <CHDRNUM>".@$CHDRNUM."</CHDRNUM>
    </GetZclhpfObjectbyParameters>
  </SOAP:Body>
</SOAP:Envelope>";	
				$fileName='AL_Policy_Claim_Period_'.time();
			$dataArr = getXMLClaimResponse($xml_data,'ALPeriodEnquiry',$fileName);
		return $dataArr;
	}
function getClaimEnquiryWithPeriod($query){
		foreach($query as $key=>$value)	{
			$$key=$value;
		}
		$xml_data = 
			"<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">
  <SOAP:Body>
    <GetGclhpfObjectsByParameters xmlns=\"http://esbinternal.cordys.com\" preserveSpace=\"no\" qAccess=\"0\" qValues=\"\">
      <GCSTS>".@$GCSTS."</GCSTS>
      <strtdate>".@$strtdate."</strtdate>
      <endate>".@$endate."</endate>
      <policynum>".@$policynum."</policynum>
    </GetGclhpfObjectsByParameters>
  </SOAP:Body>
</SOAP:Envelope>";	
				$fileName='Policy_Claim_Period_'.time();
			$dataArr = getXMLClaimResponse($xml_data,'ClaimPeriodEnquiry',$fileName);
		return $dataArr;
	}
function getPDFURL($query){
		foreach($query as $key=>$value)	{
			$$key=$value;
		}
		$xml_data = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:web="http://web.com/">
   <soapenv:Header/>
   <soapenv:Body>
      <web:GET_PDF>
         <!--Optional:-->
         <policyNo>'.@$policyNo.'</policyNo>
         <!--Optional:-->
         <ltype>HLTCRD</ltype>
      </web:GET_PDF>
   </soapenv:Body>
</soapenv:Envelope>';
			
		$fileName='GET_PDF_'.@$policyNo.'_'.time();
		$dataArr = getXMLResponse($xml_data,'GETPDF',$fileName);
		return $dataArr;
	}
	function getClaimEnquiry($query){
		foreach($query as $key=>$value)	{
			$$key=$value;
		}
		$xml_data = 
			"<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">
  <SOAP:Header xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">
    <header xmlns=\"http://schemas.cordys.com/General/1.0/\">
      <Logger xmlns=\"http://schemas.cordys.com/General/1.0/\" />
    </header>
  </SOAP:Header>
  <SOAP:Body>
    <GetClaimEnquiry xmlns=\"http://schemas.cordys.com/default\">
      <BGEN-GCOCCNO>".@$GCOCCNO."</BGEN-GCOCCNO>
      <BGEN-CHDRNUM>".@$CHDRNUM."</BGEN-CHDRNUM>
      <BGEN-CLAMNUM>".@$CLAMNUM."</BGEN-CLAMNUM>
      <BGEN-PREAUTNO>".@$PREAUTNO."</BGEN-PREAUTNO>
      <BGEN-CLNTNUM>".@$CLNTNUM."</BGEN-CLNTNUM>
    </GetClaimEnquiry>
  </SOAP:Body>
</SOAP:Envelope>";	
				$fileName='Policy_Claim_'.time();
			$dataArr = getXMLClaimResponse($xml_data,'ClaimEnquiry',$fileName);
		return $dataArr;
	}
function getClaimDetailsNew($query){
		foreach($query as $key=>$value)	{
			$$key=$value;
		}
		$xml_data ="<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:bas=\"http://basic.groupinsurance.religare.in\">
   <soapenv:Header/>
   <soapenv:Body>
  <bas:CLMALDETLIImpl>
    <REQMSG1 xmlns=\"http://basic.groupinsurance.religare.in\">
      <copybook filename=\"LDRHDR.txt\">
        <LEADER-HEADER>
          <MSGREFNO />
          <USRPRF>CSCTAB</USRPRF>
          <WKSID />
          <OBJID>CLMALDTL</OBJID>
          <VRBID>CLMALDTL</VRBID>
          <TOTMSGLNG>300</TOTMSGLNG>
          <OPMODE>1</OPMODE>
          <CMTCONTROL>N</CMTCONTROL>
          <RSPMODE>I</RSPMODE>
          <MSGINTENT>R</MSGINTENT>
          <MORE-IND>N</MORE-IND>
          <ERRLVL>0</ERRLVL>
          <IGNORE-DRIVER-HELD>N</IGNORE-DRIVER-HELD>
          <SUPPRESS-RCLRSC>N</SUPPRESS-RCLRSC>
          <FILLER />
        </LEADER-HEADER>
      </copybook>
    </REQMSG1>
    <REQMSG2 xmlns=\"http://basic.groupinsurance.religare.in\">
      <copybook filename=\"CLMALDETLI.txt\">
        <CLMALDETLI-REC>
          <MESSAGE-HEADER>
            <MSGID>CLMALDETLI</MSGID>
            <MSGLNG>142</MSGLNG>
            <MSGCNT>1</MSGCNT>
            <FILLER />
          </MESSAGE-HEADER>
          <MESSAGE-DATA>
            <ADDITIONAL-FIELDS>
              <BGEN-CLAMNUM>".@$CLAMNUM."</BGEN-CLAMNUM>
              <BGEN-PREAUTNO>".@$PREAUTNO."</BGEN-PREAUTNO>
              <BGEN-GCOCCNO>".@$GCOCCNO."</BGEN-GCOCCNO>
            </ADDITIONAL-FIELDS>
          </MESSAGE-DATA>
        </CLMALDETLI-REC>
      </copybook>
    </REQMSG2>
    <REQMSG3 xmlns=\"http://basic.groupinsurance.religare.in\">
      <copybook filename=\"SESSIONI.txt\">
        <SESSIONI-REC>
          <MSGID>SESSIONI</MSGID>
          <MSGLNG>40</MSGLNG>
          <MSGCNT>1</MSGCNT>
          <FILLER />
          <SESSIONI-COMPANY>3</SESSIONI-COMPANY>
          <SESSIONI-BRANCH>10</SESSIONI-BRANCH>
          <SESSIONI-LANGUAGE>E</SESSIONI-LANGUAGE>
          <SESSIONI-ACCTYR />
          <SESSIONI-ACCTMN />
        </SESSIONI-REC>
      </copybook>
    </REQMSG3>
  </bas:CLMALDETLIImpl>   </soapenv:Body>
</soapenv:Envelope>";	
				$fileName='Policy_Claim_'.time();
			$dataArr = getXMLClaimResponse($xml_data,'ClaimDetailsNew',$fileName);
		return $dataArr;
	}
	function getClaimDetails($query){
		foreach($query as $key=>$value)	{
			$$key=$value;
		}
		$xml_data = 
			"<SOAP:Envelope xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">
  <SOAP:Header xmlns:SOAP=\"http://schemas.xmlsoap.org/soap/envelope/\">
    <header xmlns=\"http://schemas.cordys.com/General/1.0/\">
      <Logger xmlns=\"http://schemas.cordys.com/General/1.0/\" />
    </header>
  </SOAP:Header>
  <SOAP:Body>
    <GetClaimEnquiry xmlns=\"http://schemas.cordys.com/default\">
      <BGEN-GCOCCNO>".@$GCOCCNO."</BGEN-GCOCCNO>
      <BGEN-CHDRNUM>".@$CHDRNUM."</BGEN-CHDRNUM>
      <BGEN-CLAMNUM>".@$CLAMNUM."</BGEN-CLAMNUM>
      <BGEN-PREAUTNO>".@$PREAUTNO."</BGEN-PREAUTNO>
      <BGEN-CLNTNUM>".@$CLNTNUM."</BGEN-CLNTNUM>
    </GetClaimEnquiry>
  </SOAP:Body>
</SOAP:Envelope>";	
				$fileName='Policy_Claim_Details'.time();
			$dataArr = getXMLClaimResponse($xml_data,'ClaimDetails',$fileName);
		return $dataArr;
	}
	function getXMLClaimResponse($data,$tagTitle,$fileName) 
	{		
		global $policyURL;
		$soapaction="";
		$headers = array(
				"Content-Type: text/xml;charset=\"utf-8\"",
				"Content-length: ".strlen($data),
				"Authorization: Basic Q2F0YWJhdGljdXNlcjpjb3J1YXRAMTIz"
			);

		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, @$policyURL );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data );
		curl_setopt ( $ch, CURLOPT_VERBOSE, true );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 60);
		$xmlResponse1 = curl_exec ( $ch );
		$ch_info=curl_getinfo($ch);
		curl_close($ch);
		file_put_contents('data/'.time()."_".$fileName."_request.xml",$data);
		file_put_contents('data/'.time()."_".$fileName."_response.xml",$xmlResponse1);

		$dataArr = object2array($xmlResponse1,$tagTitle);
	return $dataArr ;
}
	function getXMLResponse($data,$tagTitle,$fileName) 
	{		
		//$policyURL="http://203.160.138.164/cordys/com.eibus.web.soap.Gateway.wcp?organization=o=ReligareHealth,cn=cordys,cn=devinst,o=religare.in";
		global $policyURL;
		$soapaction="";
		$headers = array(
				"Content-Type: text/xml;charset=\"utf-8\"",
				"Content-length: ".strlen($data),
				"Authorization: Basic Q2F0YWJhdGljdXNlcjpjb3J1YXRAMTIz"
			);
		$ch = curl_init ();
		curl_setopt ( $ch, CURLOPT_URL, @$policyURL );
		curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
		curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers);
		curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data );
		curl_setopt ( $ch, CURLOPT_VERBOSE, true );
		curl_setopt( $ch, CURLOPT_TIMEOUT, 60);
		$xmlResponse1 = curl_exec ( $ch );
		$ch_info=curl_getinfo($ch);
		curl_close($ch);
		file_put_contents('data/'.time()."_".$fileName."_request.xml",$data);
		file_put_contents('data/'.time()."_".$fileName."_response.xml",$xmlResponse1);
		$dataArr = object2array($xmlResponse1,$tagTitle);
		return $dataArr ;
}
?>