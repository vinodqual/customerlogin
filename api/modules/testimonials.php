<?php

// This module requires user login in $_SESSION
// Login data exists; continuing
// echo "<pre>";print_r($_REQUEST);exit;
/*

  [img_path] => https://scontent.xx.fbcdn.net/v/t1.0-1/p50x50/12717282_1243174905698195_5509915745405304656_n.jpg?oh=085a9a70b98ce30a8feab6328b819756&oe=58367A13
  [name_txt] => Rijumone Choudhuri
  [email_txt] => mailmeonriju@gmail.com
  [status_txt] => 0
  [product_name] => care
  [rating] => 2
  [feedback] => cdascsa
  [module] => testimonials

 */
// Build payload

$payload = array();
/*
  foreach ($_REQUEST as $key => $value) {
  $payload[$key] = $value;
  } */

$name = ($_SESSION['renewalArray'][0]['firstName1']['#text']) ? $_SESSION['renewalArray'][0]['titleCd']['#text'] . " " . $_SESSION['renewalArray'][0]['firstName1']['#text'] . " " . $_SESSION['renewalArray'][0]['lastName1']['#text'] : "";

if (!$name) {
    $fetchPolListByUserId = fetchListCond("RETUSERMASTER", " WHERE USERID=" . @$_SESSION['USERID'] . "");
    $name = ucfirst(@$fetchPolListByUserId[0]['TITLE']) . ' ' . ucfirst(@$fetchPolListByUserId[0]['FIRSTNAME']) . ' ' . ucfirst(@$fetchPolListByUserId[0]['LASTNAME']);
}

$payload['name'] = ($name) ? $name : $_REQUEST['name_txt'];
$payload['feedback'] = $_REQUEST['feedback'];
$payload['rating'] = $_REQUEST['rating'];
$payload['img_path'] = $_REQUEST['img_path'];
$payload['email'] = ($_SESSION['userName']) ? $_SESSION['userName'] : $_REQUEST['email_txt'];
$payload['product'] = $_REQUEST['product_name'];
$payload['created_by'] = $_SESSION['USERID'];
$payload['user_type'] = $_SESSION['LOGINTYPE'];

// send cURL hit
// echo "<pre>";print_r($payload);exit;
$return = curlHit(API_ENDPOINT . $_REQUEST['module'] . "/" . $_REQUEST['action'], $payload);
$return = json_decode($return, true);
// echo "<pre>";print_r($return);exit;
$returnData['alert_type'] = ($return['status']) ? $return['status'] : "danger";
$returnData['alert_msg'] = ($return['status'] != "error") ? "Thank you for your feedback!" : "An error has occurred. Please try again.";
redirectURL($_REQUEST['redirect_url'], $returnData);
outputAPI($return['status'], $return['message']);
