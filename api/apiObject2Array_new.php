<?php
	include_once('xml2array.php');
	include_once("domxml-php4-to-php5.php");
	//echo "<pre>";
	function xml_to_array($root) {
    $result = array();

    if ($root->hasAttributes()) {
        $attrs = $root->attributes;
        foreach ($attrs as $attr) {
            $result['@attributes'][$attr->name] = $attr->value;
        }
    }

    if ($root->hasChildNodes()) {
        $children = $root->childNodes;
        if ($children->length == 1) {
            $child = $children->item(0);
            if ($child->nodeType == XML_TEXT_NODE) {
                $result['_value'] = $child->nodeValue;
                return count($result) == 1
                    ? $result['_value']
                    : $result;
            }
        }
        $groups = array();
        foreach ($children as $child) {
            if (!isset($result[$child->nodeName])) {
			$test = xml_to_array($child);
				if(is_array($test)){
                $result[$child->nodeName] = $test;
				} else{
                $result[$child->nodeName]['#text'] = $test;
				}
            } else {
                if (!isset($groups[$child->nodeName])) {
                    $result[$child->nodeName] = array($result[$child->nodeName]);
                    $groups[$child->nodeName] = 1;
                }
                $result[$child->nodeName][] = xml_to_array($child);
            }
        }
    }
	
	
    return $result;
}
	function object2array($data,$objectType)
	{
		$outputArray = array();
		switch ($objectType){
				case 'ClaimDetails':
				$xml = new DOMDocument();
				$xml->loadXML($data);
				$dataArr = xml_to_array($xml);
				/*echo "<pre>";
				print_r($dataArr);
				die;*/
				$dataOut=array();
					
				$dataArray  = @$dataArr['SOAP:Envelope']['SOAP:Body']['GetClaimEnquiryResponse']['CLMALDETLIImplResponse'];	
				$dataArray['RESMSG1']=@$dataArr['SOAP:Envelope']['SOAP:Body']['GetClaimEnquiryResponse']['CLMALDETLIImplResponse']['RESMSG2']['copybook']['LEADER-HEADER'];
				$dataArray['RESMSG2']=@$dataArr['SOAP:Envelope']['SOAP:Body']['GetClaimEnquiryResponse']['CLMALDETLIImplResponse']['RESMSG3']['copybook']['CLMALDETLO-REC'];
				
				if(@$dataArray['RESMSG1']['MSGREFNO']['#text']!=''){
				$dataOut['MSGREFNO']	=@$dataArray['RESMSG1']['MSGREFNO']['#text'];
				$dataOut['USRPRF']		=@$dataArray['RESMSG1']['USRPRF']['#text'];
				$dataOut['WKSID']		=@$dataArray['RESMSG1']['WKSID']['#text'];
				$dataOut['OBJID']		=@$dataArray['RESMSG1']['OBJID']['#text'];
				$dataOut['VRBID']		=@$dataArray['RESMSG1']['VRBID']['#text'];
				$dataOut['TOTMSGLNG']	=@$dataArray['RESMSG1']['TOTMSGLNG']['#text'];
				$dataOut['OPMODE']		=@$dataArray['RESMSG1']['OPMODE']['#text'];
				$dataOut['CMTCONTROL']	=@$dataArray['RESMSG1']['CMTCONTROL']['#text'];
				$dataOut['RSPMODE']		=@$dataArray['RESMSG1']['RSPMODE']['#text'];
				$dataOut['MSGINTENT']	=@$dataArray['RESMSG1']['MSGINTENT']['#text'];
				$dataOut['MORE-IND']	=@$dataArray['RESMSG1']['MORE-IND']['#text'];
				$dataOut['ERRLVL']		=@$dataArray['RESMSG1']['ERRLVL']['#text'];				
				$dataOut['MSGID']		=@$dataArray['RESMSG2']['MESSAGE-HEADER']['MSGID']['#text'];
				$dataOut['MSGLNG']		=@$dataArray['RESMSG2']['MESSAGE-HEADER']['MSGLNG']['#text'];
				$dataOut['MSGCNT']		=@$dataArray['RESMSG2']['MESSAGE-HEADER']['MSGCNT']['#text'];
				$dataOut['MSGLNG']		=@$dataArray['RESMSG2']['MESSAGE-HEADER']['MSGLNG']['#text'];
				
				$dataOut['MSGLNG']		=@$dataArray['RESMSG2']['MESSAGE-DATA']['MSGLNG']['#text'];
				$outputArray['dataArr'] = @$dataArray['RESMSG2']['MESSAGE-DATA']['ADDITIONAL-FIELDS'];
				$c=0;
				if(trim(@$outputArray['dataArr']['BGEN-MEMNAME']['#text'])!=''){
					$dataOut['claimList'][$c]['BGEN-MEMNAME']['#text']		= @$outputArray['dataArr']['BGEN-MEMNAME']['#text'];
					$dataOut['claimList'][$c]['BGEN-CLAMNUM']['#text']		= @$outputArray['dataArr']['BGEN-CLAMNUM']['#text'];
					$dataOut['claimList'][$c]['BGEN-CLNTNUM']['#text']		= @$outputArray['dataArr']['BGEN-CLNTNUM']['#text'];
					$dataOut['claimList'][$c]['BGEN-CNAME']['#text']		= @$outputArray['dataArr']['BGEN-CNAME'][0]['#text'];
					$dataOut['claimList'][$c]['BGEN-DATIME']['#text']		= @$outputArray['dataArr']['BGEN-DATIME']['#text'];
					$dataOut['claimList'][$c]['BGEN-AMNT']['#text']			= @$outputArray['dataArr']['BGEN-AMNT'][0]['#text'];
					$dataOut['claimList'][$c]['BGEN-BNFTGRP']['#text']		= @$outputArray['dataArr']['BGEN-BNFTGRP']['#text'];
					$dataOut['claimList'][$c]['BGEN-GCSTS']['#text']		= @$outputArray['dataArr']['BGEN-GCSTS']['#text'];
					$dataOut['claimList'][$c]['BGEN-CLMSRVTP']['#text']		= @$outputArray['dataArr']['BGEN-CLMSRVTP']['#text'];
					$dataOut['claimList'][$c]['BGEN-DTEVISIT']['#text']		= @$outputArray['dataArr']['BGEN-DTEVISIT']['#text'];
				//	$dataOut['claimList'][$c]['BGEN-DOCRCVDT']['#text']		= $outputArray['dataArr']['BGEN-DOCRCVDT']['#text'];
					$dataOut['claimList'][$c]['BGEN-INCURRED']['#text']		= @$outputArray['dataArr']['BGEN-INCURRED']['#text'];
					$dataOut['claimList'][$c]['BGEN-TLHMOSHR']['#text']		= @$outputArray['dataArr']['BGEN-TLHMOSHR']['#text'];	
					$dataOut['claimList'][$c]['BGEN-MBRNO']['#text']		= $outputArray['dataArr']['BGEN-MBRNO']['#text'];
					$dataOut['claimList'][$c]['BGEN-DPNTNO']['#text']		= $outputArray['dataArr']['BGEN-DPNTNO']['#text'];	
					$dataOut['claimList'][$c]['BGEN-CHDRNUM']['#text']		= $outputArray['dataArr']['BGEN-CHDRNUM']['#text'];
					$dataOut['claimList'][$c]['BGEN-PRODTYP']['#text']		= $outputArray['dataArr']['BGEN-PRODTYP']['#text'];	
					$dataOut['claimList'][$c]['BGEN-PLANNO']['#text']		= $outputArray['dataArr']['BGEN-PLANNO']['#text'];	
					$dataOut['claimList'][$c]['BGEN-DIAGCDE']['#text']		= $outputArray['dataArr']['BGEN-DIAGCDE']['#text'];	
					$dataOut['claimList'][$c]['BGEN-PROVORG']['#text']		= $outputArray['dataArr']['BGEN-PROVORG']['#text'];	
					$dataOut['claimList'][$c]['BGEN-GCFRPDTE']['#text']		= $outputArray['dataArr']['BGEN-GCFRPDTE']['#text'];	
					$dataOut['claimList'][$c]['BGEN-DTEVISIT']['#text']		= $outputArray['dataArr']['BGEN-DTEVISIT']['#text'];	
					$dataOut['claimList'][$c]['BGEN-DTEDCHRG']['#text']		= $outputArray['dataArr']['BGEN-DTEDCHRG']['#text'];	
					$dataOut['claimList'][$c]['BGEN-DOCRCVDT']['#text']		= $outputArray['dataArr']['BGEN-DOCRCVDT']['#text'];
					$dataOut['claimList'][$c]['BGEN-REJDATE']['#text']		= @$outputArray['dataArr']['BGEN-REJDATE']['#text'];
					$dataOut['claimList'][$c]['BGEN-REASONDESC']['#text']		= @$outputArray['dataArr']['BGEN-REASONDESC']['#text'];	
					$dataOut['claimList'][$c]['BGEN-TLMBRSHR']['#text']		= $outputArray['dataArr']['BGEN-TLMBRSHR']['#text'];	
					$dataOut['claimList'][$c]['BGEN-DDEDUCT']['#text']		= $outputArray['dataArr']['BGEN-DDEDUCT']['#text'];	
					$dataOut['claimList'][$c]['BGEN-COPAYAMT']['#text']		= $outputArray['dataArr']['BGEN-COPAYAMT']['#text'];
					$dataOut['claimList'][$c]['BGEN-GCOCCNO']['#text']		= $outputArray['dataArr']['BGEN-GCOCCNO']['#text'];
					$dataOut['claimList'][$c]['BGEN-SURNAME']['#text']		= $outputArray['dataArr']['BGEN-SURNAME']['#text'];
					$dataOut['claimList'][$c]['BGEN-PATNTYN']['#text']		= $outputArray['dataArr']['BGEN-PATNTYN']['#text'];
					$dataOut['claimList'][$c]['BGEN-REMDTE']['#text']		="";
					$dataOut['claimList'][$c]['BGEN-REMDTETWO']['#text']	="";
					$dataOut['claimList'][$c]['BGEN-REMDTETHE']['#text']	="";
					$dataOut['claimList'][$c]['BGEN-CHEQNO']['#text']		= @$outputArray['dataArr']['BGEN-CHEQNO'][0]['#text'];
					$dataOut['claimList'][$c]['BGEN-ZNEFTREF']['#text']		= @$outputArray['dataArr']['BGEN-ZNEFTREF'][0]['#text'];
					for($f=0;$f<count(@$outputArray['dataArr']['BGEN-FOLLOWUPS']['FILLER']);$f++){
						if($outputArray['dataArr']['BGEN-FOLLOWUPS']['FILLER'][$f]['BGEN-REMDTE']['#text']!=''){
						$dataOut['claimList'][$c]['BGEN-REMDTE']['#text']		= $outputArray['dataArr']['BGEN-FOLLOWUPS']['FILLER'][$f]['BGEN-REMDTE']['#text'];
						}
						if($outputArray['dataArr']['BGEN-FOLLOWUPS']['FILLER'][$f]['BGEN-REMDTETWO']['#text']!=''){
						$dataOut['claimList'][$c]['BGEN-REMDTETWO']['#text']		= $outputArray['dataArr']['BGEN-FOLLOWUPS']['FILLER'][$f]['BGEN-REMDTETWO']['#text'];
						}
						if($outputArray['dataArr']['BGEN-FOLLOWUPS']['FILLER'][$f]['BGEN-REMDTETHE']['#text']!=''){
						$dataOut['claimList'][$c]['BGEN-REMDTETHE']['#text']		= $outputArray['dataArr']['BGEN-FOLLOWUPS']['FILLER'][$f]['BGEN-REMDTETHE']['#text'];
						}
					}
					if(@$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-ZRCODE']['#text']!=''){
					$dataOut['claimList']['DISSALWNCE'][$c]['BGEN-MEMNAME']['#text']		= @$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-ZRCODE']['#text'];
					$dataOut['claimList']['DISSALWNCE'][$c]['BGEN-CLAMNUM']['#text']		= @$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-DISALOWAMT']['#text'];
					$c++;
					}
					for($k=0;$k<count(@$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-ZRCODE']);$k++){
					$dataOut['claimList']['DISSALWNCE'][$c]['BGEN-MEMNAME']['#text']		= @$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-ZRCODE'][$k]['#text']?@$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-ZRCODE'][$k]['#text']:@$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-ZRCODE'][$k];
					
					
					@$amount =number_format(@$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-DISALOWAMT'][$k]['#text']);
					if($amount>0){
					$amount =@$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-DISALOWAMT'][$k]['#text'];
					} else{
					$amount =@$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-DISALOWAMT'][$k];
					}
					$dataOut['claimList']['DISSALWNCE'][$c]['BGEN-CLAMNUM']['#text']		= @$amount;
						$c++;
					}
					//Deficiency Type	
					$f=0;
					if(@$outputArray['dataArr']['BGEN-FOLLOWUPS']['BGEN-GFUPCDE']['#text']!=''){
						$dataOut['claimList']['Deficiency'][$f]['BGEN-GFUPCDE']['#text']		= @$outputArray['dataArr']['BGEN-FOLLOWUPS']['BGEN-GFUPCDE']['#text'];
						$f++;
					}
					for($k=0;$k<count(@$outputArray['dataArr']['BGEN-FOLLOWUPS']['BGEN-GFUPCDE']);$k++){
						$dataOut['claimList']['Deficiency'][$f]['BGEN-GFUPCDE']['#text']		= @$outputArray['dataArr']['BGEN-FOLLOWUPS']['BGEN-GFUPCDE'][$k]['#text'];
						$f++;
					}		
					//Deficiency Status
					$f=0;
					if(@$outputArray['dataArr']['BGEN-FOLLOWUPS']['BGEN-GFUPSTS']['#text']!=''){
						$dataOut['claimList']['Deficiency'][$f]['BGEN-GFUPSTS']['#text']		= @$outputArray['dataArr']['BGEN-FOLLOWUPS']['BGEN-GFUPSTS']['#text'];
						$f++;
					}
					for($k=0;$k<count(@$outputArray['dataArr']['BGEN-FOLLOWUPS']['BGEN-GFUPSTS']);$k++){
						$dataOut['claimList']['Deficiency'][$f]['BGEN-GFUPSTS']['#text']		= @$outputArray['dataArr']['BGEN-FOLLOWUPS']['BGEN-GFUPSTS'][$k]['#text'];
						$f++;
					}		
					//start case note
					$f=0;
					if(trim(@$outputArray['dataArr']['BGEN-CASENOTE']['#text'])!=''){
					$dataOut['claimList']['CASENOTE'][$f]['BGEN-CASENOTE']['#text']		= @$outputArray['dataArr']['BGEN-CASENOTE']['#text'];
					$f++;
					}
					for($k=0;$k<count(@$outputArray['dataArr']['BGEN-CASENOTE']);$k++){
						if(trim(@$outputArray['dataArr']['BGEN-CASENOTE'][$k]['#text'])!=''){
							$dataOut['claimList']['CASENOTE'][$f]['BGEN-CASENOTE']['#text']		= @$outputArray['dataArr']['BGEN-CASENOTE'][$k]['#text'];
							$f++;
						}
					}		
					//end case note
					//start SYMPTOM
					$f=0;
					if(trim(@$outputArray['dataArr']['BGEN-SYMPTOM']['#text'])!=''){
					$dataOut['claimList']['SYMPTOM'][$f]['BGEN-SYMPTOM']['#text']		= @$outputArray['dataArr']['BGEN-SYMPTOM']['#text'];
					$f++;
					}
					for($k=0;$k<count(@$outputArray['dataArr']['BGEN-CASENOTE']);$k++){
					@$claimDatas = trim(@$outputArray['dataArr']['BGEN-SYMPTOM'][$k]['#text']);
					@$claimDatas1 = trim(@$outputArray['dataArr']['BGEN-SYMPTOM'][$k]);
					if($claimDatas!=''){
					$claimDatasAll = trim(@$outputArray['dataArr']['BGEN-SYMPTOM'][$k]['#text']);
					}  if($claimDatas1!=''){
					@$claimDatasAll = trim(@$outputArray['dataArr']['BGEN-SYMPTOM'][$k]);
					} 
						if(trim($claimDatas)!='' || trim($claimDatas1)!=''){
							$dataOut['claimList']['SYMPTOM'][$f]['BGEN-SYMPTOM']['#text']		= @$claimDatasAll;
							$f++;
						}
					}		
					//end SYMPTOM
				}
				
				}				
				return $dataOut;		
				break;
				// for new added disallowance,rejected reason,payee name,from recieved on 28july2015 Er amit kumar dubey
				case 'ClaimDetailsNew':
				$xml = new DOMDocument();
				$xml->loadXML($data);
				$dataArr = xml_to_array($xml);
				/*echo "<pre>";
				print_r($dataArr);
				die;*/
				$dataOut=array();
					
				$dataArray  = @$dataArr['soapenv:Envelope']['soapenv:Body']['CLMALDETLIImplResponse'];	
				$dataArray['RESMSG1']=@$dataArr['soapenv:Envelope']['soapenv:Body']['CLMALDETLIImplResponse']['RESMSG1']['copybook']['LEADER-HEADER'];
				$dataArray['RESMSG2']=@$dataArr['soapenv:Envelope']['soapenv:Body']['CLMALDETLIImplResponse']['RESMSG2']['copybook']['CLMALDETLO-REC'];
				if(@$dataArray['RESMSG1']['MSGREFNO']['#text']!=''){
				$dataOut['MSGREFNO']	=@$dataArray['RESMSG1']['MSGREFNO']['#text'];
				$dataOut['USRPRF']		=@$dataArray['RESMSG1']['USRPRF']['#text'];
				$dataOut['WKSID']		=@$dataArray['RESMSG1']['WKSID']['#text'];
				$dataOut['OBJID']		=@$dataArray['RESMSG1']['OBJID']['#text'];
				$dataOut['VRBID']		=@$dataArray['RESMSG1']['VRBID']['#text'];
				$dataOut['TOTMSGLNG']	=@$dataArray['RESMSG1']['TOTMSGLNG']['#text'];
				$dataOut['OPMODE']		=@$dataArray['RESMSG1']['OPMODE']['#text'];
				$dataOut['CMTCONTROL']	=@$dataArray['RESMSG1']['CMTCONTROL']['#text'];
				$dataOut['RSPMODE']		=@$dataArray['RESMSG1']['RSPMODE']['#text'];
				$dataOut['MSGINTENT']	=@$dataArray['RESMSG1']['MSGINTENT']['#text'];
				$dataOut['MORE-IND']	=@$dataArray['RESMSG1']['MORE-IND']['#text'];
				$dataOut['ERRLVL']		=@$dataArray['RESMSG1']['ERRLVL']['#text'];				
				$dataOut['MSGID']		=@$dataArray['RESMSG2']['MESSAGE-HEADER']['MSGID']['#text'];
				$dataOut['MSGLNG']		=@$dataArray['RESMSG2']['MESSAGE-HEADER']['MSGLNG']['#text'];
				$dataOut['MSGCNT']		=@$dataArray['RESMSG2']['MESSAGE-HEADER']['MSGCNT']['#text'];
				$dataOut['MSGLNG']		=@$dataArray['RESMSG2']['MESSAGE-HEADER']['MSGLNG']['#text'];
				
				$dataOut['MSGLNG']		=@$dataArray['RESMSG2']['MESSAGE-DATA']['MSGLNG']['#text'];
				$outputArray['dataArr'] = @$dataArray['RESMSG2']['MESSAGE-DATA']['ADDITIONAL-FIELDS'];
				$c=0;
				if(trim(@$outputArray['dataArr']['BGEN-MEMNAME']['#text'])!=''){
					$dataOut['claimList'][$c]['BGEN-MEMNAME']['#text']		= $outputArray['dataArr']['BGEN-MEMNAME']['#text'];
					$dataOut['claimList'][$c]['BGEN-CLAMNUM']['#text']		= $outputArray['dataArr']['BGEN-CLAMNUM']['#text'];
					$dataOut['claimList'][$c]['BGEN-CLNTNUM']['#text']		= $outputArray['dataArr']['BGEN-CLNTNUM']['#text'];
					$dataOut['claimList'][$c]['BGEN-CNAME']['#text']		= @$outputArray['dataArr']['BGEN-CNAME'][0]['#text'];
					$dataOut['claimList'][$c]['BGEN-DATIME']['#text']		= @$outputArray['dataArr']['BGEN-DATIME']['#text'];
					$dataOut['claimList'][$c]['BGEN-AMNT']['#text']			= @$outputArray['dataArr']['BGEN-AMNT'][0]['#text'];
					$dataOut['claimList'][$c]['BGEN-BNFTGRP']['#text']		= $outputArray['dataArr']['BGEN-BNFTGRP']['#text'];
					$dataOut['claimList'][$c]['BGEN-GCSTS']['#text']		= $outputArray['dataArr']['BGEN-GCSTS']['#text'];
					$dataOut['claimList'][$c]['BGEN-CLMSRVTP']['#text']		= $outputArray['dataArr']['BGEN-CLMSRVTP']['#text'];
					$dataOut['claimList'][$c]['BGEN-DTEVISIT']['#text']		= $outputArray['dataArr']['BGEN-DTEVISIT']['#text'];
				//	$dataOut['claimList'][$c]['BGEN-DOCRCVDT']['#text']		= $outputArray['dataArr']['BGEN-DOCRCVDT']['#text'];
					$dataOut['claimList'][$c]['BGEN-INCURRED']['#text']		= $outputArray['dataArr']['BGEN-INCURRED']['#text'];
					$dataOut['claimList'][$c]['BGEN-TLHMOSHR']['#text']		= $outputArray['dataArr']['BGEN-TLHMOSHR']['#text'];	
					$dataOut['claimList'][$c]['BGEN-MBRNO']['#text']		= $outputArray['dataArr']['BGEN-MBRNO']['#text'];
					$dataOut['claimList'][$c]['BGEN-DPNTNO']['#text']		= $outputArray['dataArr']['BGEN-DPNTNO']['#text'];	
					$dataOut['claimList'][$c]['BGEN-CHDRNUM']['#text']		= $outputArray['dataArr']['BGEN-CHDRNUM']['#text'];
					$dataOut['claimList'][$c]['BGEN-ZCLMRECD']['#text']		= $outputArray['dataArr']['BGEN-ZCLMRECD']['#text'];
					$dataOut['claimList'][$c]['BGEN-PRODTYP']['#text']		= $outputArray['dataArr']['BGEN-PRODTYP']['#text'];	
					$dataOut['claimList'][$c]['BGEN-PLANNO']['#text']		= $outputArray['dataArr']['BGEN-PLANNO']['#text'];	
					$dataOut['claimList'][$c]['BGEN-DIAGCDE']['#text']		= $outputArray['dataArr']['BGEN-DIAGCDE']['#text'];	
					$dataOut['claimList'][$c]['BGEN-PROVORG']['#text']		= $outputArray['dataArr']['BGEN-PROVORG']['#text'];	
					$dataOut['claimList'][$c]['BGEN-GCFRPDTE']['#text']		= $outputArray['dataArr']['BGEN-GCFRPDTE']['#text'];	
					$dataOut['claimList'][$c]['BGEN-DTEVISIT']['#text']		= $outputArray['dataArr']['BGEN-DTEVISIT']['#text'];	
					$dataOut['claimList'][$c]['BGEN-DTEDCHRG']['#text']		= $outputArray['dataArr']['BGEN-DTEDCHRG']['#text'];	
					$dataOut['claimList'][$c]['BGEN-DOCRCVDT']['#text']		= $outputArray['dataArr']['BGEN-DOCRCVDT']['#text'];
					$dataOut['claimList'][$c]['BGEN-REJDATE']['#text']		= @$outputArray['dataArr']['BGEN-REJDATE']['#text'];
					$dataOut['claimList'][$c]['BGEN-REASONDESC']['#text']		= @$outputArray['dataArr']['BGEN-REASONDESC']['#text'];	
					$dataOut['claimList'][$c]['BGEN-TLMBRSHR']['#text']		= $outputArray['dataArr']['BGEN-TLMBRSHR']['#text'];	
					$dataOut['claimList'][$c]['BGEN-DDEDUCT']['#text']		= $outputArray['dataArr']['BGEN-DDEDUCT']['#text'];	
					$dataOut['claimList'][$c]['BGEN-COPAYAMT']['#text']		= $outputArray['dataArr']['BGEN-COPAYAMT']['#text'];
					$dataOut['claimList'][$c]['BGEN-GCOCCNO']['#text']		= $outputArray['dataArr']['BGEN-GCOCCNO']['#text'];
					$dataOut['claimList'][$c]['BGEN-SURNAME']['#text']		= $outputArray['dataArr']['BGEN-SURNAME']['#text'];
					$dataOut['claimList'][$c]['BGEN-PATNTYN']['#text']		= $outputArray['dataArr']['BGEN-PATNTYN']['#text'];
					$dataOut['claimList'][$c]['BGEN-REMDTE']['#text']		="";
					$dataOut['claimList'][$c]['BGEN-REMDTETWO']['#text']	="";
					$dataOut['claimList'][$c]['BGEN-REMDTETHE']['#text']	="";
					$dataOut['claimList'][$c]['BGEN-CHEQNO']['#text']		= @$outputArray['dataArr']['BGEN-CHEQNO'][0]['#text'];
					$dataOut['claimList'][$c]['BGEN-ZNEFTREF']['#text']		= @$outputArray['dataArr']['BGEN-ZNEFTREF'][0]['#text'];
					for($f=0;$f<count(@$outputArray['dataArr']['BGEN-FOLLOWUPS']['FILLER']);$f++){
						if($outputArray['dataArr']['BGEN-FOLLOWUPS']['FILLER'][$f]['BGEN-REMDTE']['#text']!=''){
						$dataOut['claimList'][$c]['BGEN-REMDTE']['#text']		= $outputArray['dataArr']['BGEN-FOLLOWUPS']['FILLER'][$f]['BGEN-REMDTE']['#text'];
						}
						if($outputArray['dataArr']['BGEN-FOLLOWUPS']['FILLER'][$f]['BGEN-REMDTETWO']['#text']!=''){
						$dataOut['claimList'][$c]['BGEN-REMDTETWO']['#text']		= $outputArray['dataArr']['BGEN-FOLLOWUPS']['FILLER'][$f]['BGEN-REMDTETWO']['#text'];
						}
						if($outputArray['dataArr']['BGEN-FOLLOWUPS']['FILLER'][$f]['BGEN-REMDTETHE']['#text']!=''){
						$dataOut['claimList'][$c]['BGEN-REMDTETHE']['#text']		= $outputArray['dataArr']['BGEN-FOLLOWUPS']['FILLER'][$f]['BGEN-REMDTETHE']['#text'];
						}
					}
					if(@$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-ZRCODE']['#text']!=''){
					$dataOut['claimList']['DISSALWNCE'][$c]['BGEN-MEMNAME']['#text']		= @$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-ZRCODE']['#text'];
					$dataOut['claimList']['DISSALWNCE'][$c]['BGEN-CLAMNUM']['#text']		= @$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-DISALOWAMT']['#text'];
					$c++;
					}
					//start used for new added claim reasons in claim details page promary and secondry
					if(@$outputArray['dataArr']['BGEN-DISALLOW-DETAILS']['BGEN-ZRCODE-NEW']['BGEN-ZRCODE-NEW-CDE']['#text']!=''){
					$dataOut['claimList']['DISSALWNCENEWP'][$c]['BGEN-MEMNAME']['#text']		= @$outputArray['dataArr']['BGEN-DISALLOW-DETAILS']['BGEN-ZRCODE-NEW']['BGEN-ZRCODE-NEW-CDE']['#text'];
					$c++;
					}
					
					if(@$outputArray['dataArr']['BGEN-DISALLOW-DETAILS']['BGEN-TPRCODEA-NEW']['BGEN-TPRCODEA-NEW-CDE']['#text']!=''){
					$dataOut['claimList']['DISSALWNCENEWS'][$c]['BGEN-MEMNAME']['#text']		= @$outputArray['dataArr']['BGEN-DISALLOW-DETAILS']['BGEN-TPRCODEA-NEW']['BGEN-TPRCODEA-NEW-CDE']['#text'];
					$c++;
					}
					//end of used for new added claim reasons in claim details page promary and secondry






					for($k=0;$k<count(@$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-ZRCODE']);$k++){
					$dataOut['claimList']['DISSALWNCE'][$c]['BGEN-MEMNAME']['#text']		= @$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-ZRCODE'][$k]['#text']?@$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-ZRCODE'][$k]['#text']:@$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-ZRCODE'][$k];
					$amount =number_format(@$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-DISALOWAMT'][$k]['#text']);
						if($amount>0){
						$amount =@$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-DISALOWAMT'][$k]['#text'];
						} else{
						$amount =@$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-DISALOWAMT'][$k];
						}
					$dataOut['claimList']['DISSALWNCE'][$c]['BGEN-CLAMNUM']['#text']		= @$amount;
						$c++;
					}

					//start used for new added claim reasons in claim details page promary and secondry
					for($k1=0;$k1<count(@$outputArray['dataArr']['BGEN-DISALLOW-DETAILS']['BGEN-ZRCODE-NEW']['BGEN-ZRCODE-NEW-CDE']);$k1++){
					$dataOut['claimList']['DISSALWNCENEWP'][$c]['BGEN-MEMNAME']['#text']		= @$outputArray['dataArr']['BGEN-DISALLOW-DETAILS']['BGEN-ZRCODE-NEW']['BGEN-ZRCODE-NEW-CDE'][$k1];
						$c++;
					}
					
					$rd=0;
					for($k2=0;$k2<count(@$outputArray['dataArr']['BGEN-DISALLOW-DETAILS']);$k2++){
						for($k3=0;$k3<count(@$outputArray['dataArr']['BGEN-DISALLOW-DETAILS'][$k2]);$k3++){
						//if(@$outputArray['dataArr']['BGEN-DISALLOW-DETAILS'][$k2]['BGEN-TPRCODEA-NEW'][$k3]['BGEN-TPRCODEA-NEW-CDE']['#text']!=''){
							$dataOut['claimList']['DISSALWNCENEWS'][$rd]['BGEN-MEMNAME']['#text']		= @$outputArray['dataArr']['BGEN-DISALLOW-DETAILS'][$k2]['BGEN-TPRCODEA-NEW'][$k3]['BGEN-TPRCODEA-NEW-CDE']['#text'];
							$rd++;
							//}
						}
						
					}
					
					// start of rejection new reasons
					//echo "<pre>";		
					$is=0;
					if(@$outputArray['dataArr']['BGEN-GCOPRSCD']['#text']!=''){
						$dataOut['claimList']['REJECTIONREASONNEW'][$is]['BGEN-GFUPCDE']['#text']		= @$outputArray['dataArr']['BGEN-GFUPCDE']['#text'];
						$is++;
					}
					for($r2=0;$r2<count(@$outputArray['dataArr']['BGEN-GCOPRSCD']);$r2++){
						if(is_array($outputArray['dataArr']['BGEN-GCOPRSCD'][$r2]) && count($outputArray['dataArr']['BGEN-GCOPRSCD'][$r2]) > 0){
						 if(isset($outputArray['dataArr']['BGEN-GCOPRSCD'][$r2]['#text']) && !empty($outputArray['dataArr']['BGEN-GCOPRSCD'][$r2]['#text'])){
						 	$dataOut['claimList']['REJECTIONREASONNEW'][$is]['BGEN-GCOPRSCD']['#text']		= @$outputArray['dataArr']['BGEN-GCOPRSCD'][$r2]['#text'];
						 }
						} else {
							if(isset($outputArray['dataArr']['BGEN-GCOPRSCD'][$r2]) && !empty($outputArray['dataArr']['BGEN-GCOPRSCD'][$r2])){
							 $dataOut['claimList']['REJECTIONREASONNEW'][$is]['BGEN-GCOPRSCD']['#text']		= @$outputArray['dataArr']['BGEN-GCOPRSCD'][$r2];
							 }
						}
						$is++;
					}
				
					
					//echo $dataOut['claimList'][$is]['BGEN-GCOPRSCD']['#text'];
				
					// end of new rejection reasons
					
					
					//@$claimArray['claimList']['DISSALWNCENEWS']=array_unique(@$claimArray['claimList']['DISSALWNCENEWS']);
					
					//end of used for new added claim reasons in claim details page promary and secondry
					
					//Deficiency Type	
					$f=0;
					if(@$outputArray['dataArr']['BGEN-FOLLOWUPS']['BGEN-GFUPCDE']['#text']!=''){
						$dataOut['claimList']['Deficiency'][$f]['BGEN-GFUPCDE']['#text']		= @$outputArray['dataArr']['BGEN-FOLLOWUPS']['BGEN-GFUPCDE']['#text'];
						$f++;
					}
					for($k=0;$k<count(@$outputArray['dataArr']['BGEN-FOLLOWUPS']['BGEN-GFUPCDE']);$k++){
						$dataOut['claimList']['Deficiency'][$f]['BGEN-GFUPCDE']['#text']		= @$outputArray['dataArr']['BGEN-FOLLOWUPS']['BGEN-GFUPCDE'][$k]['#text'];
						$f++;
					}		
					//Deficiency Status
					$f=0;
					if(@$outputArray['dataArr']['BGEN-FOLLOWUPS']['BGEN-GFUPSTS']['#text']!=''){
						$dataOut['claimList']['Deficiency'][$f]['BGEN-GFUPSTS']['#text']		= @$outputArray['dataArr']['BGEN-FOLLOWUPS']['BGEN-GFUPSTS']['#text'];
						$f++;
					}
					for($k=0;$k<count(@$outputArray['dataArr']['BGEN-FOLLOWUPS']['BGEN-GFUPSTS']);$k++){
						$dataOut['claimList']['Deficiency'][$f]['BGEN-GFUPSTS']['#text']		= @$outputArray['dataArr']['BGEN-FOLLOWUPS']['BGEN-GFUPSTS'][$k]['#text'];
						$f++;
					}		
					//start case note
					$f=0;
					if(trim(@$outputArray['dataArr']['BGEN-CASENOTE']['#text'])!=''){
					$dataOut['claimList']['CASENOTE'][$f]['BGEN-CASENOTE']['#text']		= @$outputArray['dataArr']['BGEN-CASENOTE']['#text'];
					$f++;
					}
					for($k=0;$k<count(@$outputArray['dataArr']['BGEN-CASENOTE']);$k++){
						
						
						
						
						
					$casenoteDatas = trim(@$outputArray['dataArr']

['BGEN-CASENOTE'][$k]['#text']);
					$casenoteDatas1 = trim(@$outputArray

['dataArr']['BGEN-CASENOTE'][$k]);
					if($casenoteDatas!=''){
					$casenoteDatasAll = trim(@$outputArray

['dataArr']['BGEN-CASENOTE'][$k]['#text']);
					}  if($casenoteDatas1!=''){
					$casenoteDatasAll = trim(@$outputArray

['dataArr']['BGEN-CASENOTE'][$k]);
					} 	
						if(trim($casenoteDatas)!='' || trim

($casenoteDatas1)!=''){
							$dataOut['claimList']

['CASENOTE'][$f]['BGEN-CASENOTE']['#text']		= @$casenoteDatasAll;;
							$f++;
						}
					}		
					//end case note
					//start SYMPTOM
					$f=0;
					if(trim(@$outputArray['dataArr']['BGEN-SYMPTOM']['#text'])!=''){
					$dataOut['claimList']['SYMPTOM'][$f]['BGEN-SYMPTOM']['#text']		= @$outputArray['dataArr']['BGEN-SYMPTOM']['#text'];
					$f++;
					}
					for($k=0;$k<count(@$outputArray['dataArr']['BGEN-CASENOTE']);$k++){
					$claimDatas = trim(@$outputArray['dataArr']['BGEN-SYMPTOM'][$k]['#text']);
					$claimDatas1 = trim(@$outputArray['dataArr']['BGEN-SYMPTOM'][$k]);
					if($claimDatas!=''){
					$claimDatasAll = trim(@$outputArray['dataArr']['BGEN-SYMPTOM'][$k]['#text']);
					}  if($claimDatas1!=''){
					$claimDatasAll = trim(@$outputArray['dataArr']['BGEN-SYMPTOM'][$k]);
					} 
						if(trim($claimDatas)!='' || trim($claimDatas1)!=''){
							$dataOut['claimList']['SYMPTOM'][$f]['BGEN-SYMPTOM']['#text']		= @$claimDatasAll;
							$f++;
						}
					}		
					//end SYMPTOM
				}
				
				}				
				return $dataOut;		
				break; 
				
		}		
		
	}
?>