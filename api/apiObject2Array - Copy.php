<?php
	include_once('xml2array.php');
	include_once("domxml-php4-to-php5.php");
function xml_to_array($root) {
$result = array();

if ($root->hasAttributes()) {
	$attrs = $root->attributes;
	foreach ($attrs as $attr) {
		$result['@attributes'][$attr->name] = $attr->value;
	}
}

if ($root->hasChildNodes()) {
	$children = $root->childNodes;
	if ($children->length == 1) {
		$child = $children->item(0);
		if ($child->nodeType == XML_TEXT_NODE) {
			$result['_value'] = $child->nodeValue;
			return count($result) == 1
				? $result['_value']
				: $result;
		}
	}
	$groups = array();
	foreach ($children as $child) {
		if (!isset($result[$child->nodeName])) {
		$test = xml_to_array($child);
			if(is_array($test)){
			$result[$child->nodeName] = $test;
			} else{
			$result[$child->nodeName]['#text'] = $test;
			}
		} else {
			if (!isset($groups[$child->nodeName])) {
				$result[$child->nodeName] = array($result[$child->nodeName]);
				$groups[$child->nodeName] = 1;
			}
			$result[$child->nodeName][] = xml_to_array($child);
		}
	}
}


return $result;
}
	function object2array($data,$objectType)
	{
		
		$outputArray = array();
		switch ($objectType){
		
		 case 'PolicyEnquiry':	
				$converter= new xml2array($data); 
				$dataArr=$converter->getResult();
				//echo "<pre>";print_r($dataArr);			
				$error  = @$dataArr['SOAP:Envelope']['SOAP:Body']['SOAP:Fault']['detail']['bpm:FaultDetails']['cordys:FaultDetailString']['#text'];				
				if(isset($error)){	
					$error = "Please try after some time.";
					$_SESSION['response_error'] = @$error;					
				}else{
					$outputArray['owner_number'] 	= @$dataArr['SOAP:Envelope']['SOAP:Body']['getPolicyEnquiryResponse']['RESMSG2']['copybook']['POLENQO-REC']['MESSAGE-DATA']['ADDITIONAL-FIELDS']['BGEN-OWNERNUM']['#text'];
					$outputArray['owner_name']   	= @$dataArr['SOAP:Envelope']['SOAP:Body']['getPolicyEnquiryResponse']['RESMSG2']['copybook']['POLENQO-REC']['MESSAGE-DATA']['ADDITIONAL-FIELDS']['BGEN-OWNERNAME']['#text'];
					
					$outputArray['policy_number'] 	= @$dataArr['SOAP:Envelope']['SOAP:Body']['getPolicyEnquiryResponse']['RESMSG2']['copybook']['POLENQO-REC']['MESSAGE-DATA']['ADDITIONAL-FIELDS']['BGEN-CHDRNUM']['#text'];
					$outputArray['dataArr']       	= @$dataArr['SOAP:Envelope']['SOAP:Body']['getPolicyEnquiryResponse']['RESMSG2']['copybook']['POLENQO-REC']['MESSAGE-DATA']['ADDITIONAL-FIELDS']['BGEN-DPNTDTL'];
					$outputArray['POLICYSTARTDATE']       	= @$dataArr['SOAP:Envelope']['SOAP:Body']['getPolicyEnquiryResponse']['RESMSG2']['copybook']['POLENQO-REC']['MESSAGE-DATA']['ADDITIONAL-FIELDS']['BGEN-CCDATE'];
					$outputArray['POLICYENDDATE']       	= @$dataArr['SOAP:Envelope']['SOAP:Body']['getPolicyEnquiryResponse']['RESMSG2']['copybook']['POLENQO-REC']['MESSAGE-DATA']['ADDITIONAL-FIELDS']['BGEN-CRDATE'];
					
					$counter = 0;
					$responseArray = array();
					
					if(is_array(@$outputArray['dataArr'])){		
						foreach(@$outputArray['dataArr'] as $key  => $value){ 	
								if(count($value) == 1){
									$counter = 0;
									$responseArray[$counter][$key] = $value;
								}else{
									if($value['BGEN-MEMNAME']['#text']){
										$responseArray[$counter] = $value;
									}
								}
							$counter++;			
						}
					}
					
					
					$_SESSION['dataArray'] 			= @$responseArray;
					$_SESSION['policyNumber'] 		= @$outputArray['policy_number'];
					$_SESSION['owner_name'] 		= @$outputArray['owner_name'];
					$_SESSION['owner_number'] 		= @$outputArray['owner_number'];
					$dateparse1=date_parse(@$outputArray['POLICYSTARTDATE']['#text']);					
					$dateparse2=date_parse(@$outputArray['POLICYENDDATE']['#text']);					
					$_SESSION['POLICYSTARTDATE'] 	= $dateparse1['year']."-".$dateparse1['month']."-".$dateparse1['day'];					
					$_SESSION['POLICYENDDATE'] 		= $dateparse2['year']."-".$dateparse2['month']."-".$dateparse2['day'];						
				}	
				
				return $_SESSION;			
				break;
				case 'ClaimEnquiry':
				$converter= new xml2array($data); 
				$dataArr=$converter->getResult();
				$dataOut=array();
				$dataArray  = @$dataArr['SOAP:Envelope']['SOAP:Body']['GetClaimEnquiryResponse']['CLMALDETLIImplResponse'];		
				$dataArray['RESMSG1']=@$dataArr['SOAP:Envelope']['SOAP:Body']['GetClaimEnquiryResponse']['CLMALLISTIImplResponse']['RESMSG2']['copybook']['LEADER-HEADER'];
				$dataArray['RESMSG2']=@$dataArr['SOAP:Envelope']['SOAP:Body']['GetClaimEnquiryResponse']['CLMALLISTIImplResponse']['RESMSG3']['copybook']['CLMALLISTO-REC'];
				if(@$dataArray['RESMSG1']['MSGREFNO']['#text']!=''){
				$dataOut['MSGREFNO']	=@$dataArray['RESMSG1']['MSGREFNO']['#text'];
				$dataOut['USRPRF']		=@$dataArray['RESMSG1']['USRPRF']['#text'];
				$dataOut['WKSID']		=@$dataArray['RESMSG1']['WKSID']['#text'];
				$dataOut['OBJID']		=@$dataArray['RESMSG1']['OBJID']['#text'];
				$dataOut['VRBID']		=@$dataArray['RESMSG1']['VRBID']['#text'];
				$dataOut['TOTMSGLNG']	=@$dataArray['RESMSG1']['TOTMSGLNG']['#text'];
				$dataOut['OPMODE']		=@$dataArray['RESMSG1']['OPMODE']['#text'];
				$dataOut['CMTCONTROL']	=@$dataArray['RESMSG1']['CMTCONTROL']['#text'];
				$dataOut['RSPMODE']		=@$dataArray['RESMSG1']['RSPMODE']['#text'];
				$dataOut['MSGINTENT']	=@$dataArray['RESMSG1']['MSGINTENT']['#text'];
				$dataOut['MORE-IND']	=@$dataArray['RESMSG1']['MORE-IND']['#text'];
				$dataOut['ERRLVL']		=@$dataArray['RESMSG1']['ERRLVL']['#text'];
				
				$dataOut['MSGID']		=@$dataArray['RESMSG2']['MESSAGE-HEADER']['MSGID']['#text'];
				$dataOut['MSGLNG']		=@$dataArray['RESMSG2']['MESSAGE-HEADER']['MSGLNG']['#text'];
				$dataOut['MSGCNT']		=@$dataArray['RESMSG2']['MESSAGE-HEADER']['MSGCNT']['#text'];
				$dataOut['MSGLNG']		=@$dataArray['RESMSG2']['MESSAGE-HEADER']['MSGLNG']['#text'];
				
				$dataOut['MSGLNG']		=@$dataArray['RESMSG2']['MESSAGE-DATA']['MSGLNG']['#text'];
				$outputArray['dataArr'] = @$dataArray['RESMSG2']['MESSAGE-DATA']['ADDITIONAL-FIELDS'];
				$c=0;
				if(trim(@$outputArray['dataArr']['BGEN-CLAIMALDTL']['BGEN-MEMNAME']['#text'])!=''){
					$dataOut['claimList'][$c]['BGEN-MEMNAME']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL']['BGEN-MEMNAME']['#text'];
					$dataOut['claimList'][$c]['BGEN-CLAMNUM']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL']['BGEN-CLAMNUM']['#text'];
					$dataOut['claimList'][$c]['BGEN-CLNTNUM']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL']['BGEN-CLNTNUM']['#text'];
					$dataOut['claimList'][$c]['BGEN-BNFTGRP']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL']['BGEN-BNFTGRP']['#text'];
					$dataOut['claimList'][$c]['BGEN-GCSTS']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL']['BGEN-GCSTS']['#text'];
					$dataOut['claimList'][$c]['BGEN-GCSECSTS']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL']['BGEN-GCSECSTS']['#text'];
					$dataOut['claimList'][$c]['BGEN-CLMSRVTP']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL']['BGEN-CLMSRVTP']['#text'];
					$dataOut['claimList'][$c]['BGEN-DTEVISIT']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL']['BGEN-DTEVISIT']['#text'];
					$dataOut['claimList'][$c]['BGEN-INCURRED']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL']['BGEN-INCURRED']['#text'];
					$dataOut['claimList'][$c]['BGEN-TLHMOSHR']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL']['BGEN-TLHMOSHR']['#text'];
					$dataOut['claimList'][$c]['BGEN-GCOCCNO']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL']['BGEN-GCOCCNO']['#text'];
					$c++;
					for($k=0;$k<count(@$outputArray['dataArr']['BGEN-CLAIMALDTL']);$k++){
						if(trim(@$outputArray['dataArr']['BGEN-CLAIMALDTL'][$k]['BGEN-MEMNAME']['#text'])!=''){
						$dataOut['claimList'][$c]['BGEN-MEMNAME']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL'][$k]['BGEN-MEMNAME']['#text'];
					$dataOut['claimList'][$c]['BGEN-CLAMNUM']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL'][$k]['BGEN-CLAMNUM']['#text'];
					$dataOut['claimList'][$c]['BGEN-CLNTNUM']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL']['BGEN-CLNTNUM']['#text'];
					$dataOut['claimList'][$c]['BGEN-BNFTGRP']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL'][$k]['BGEN-BNFTGRP']['#text'];
					$dataOut['claimList'][$c]['BGEN-GCSTS']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL'][$k]['BGEN-GCSTS']['#text'];
					$dataOut['claimList'][$c]['BGEN-GCSECSTS']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL'][$k]['BGEN-GCSECSTS']['#text'];
					$dataOut['claimList'][$c]['BGEN-CLMSRVTP']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL'][$k]['BGEN-CLMSRVTP']['#text'];
					$dataOut['claimList'][$c]['BGEN-DTEVISIT']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL'][$k]['BGEN-DTEVISIT']['#text'];
					$dataOut['claimList'][$c]['BGEN-INCURRED']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL'][$k]['BGEN-INCURRED']['#text'];
					$dataOut['claimList'][$c]['BGEN-TLHMOSHR']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL'][$k]['BGEN-TLHMOSHR']['#text'];
					$dataOut['claimList'][$c]['BGEN-GCOCCNO']['#text']		= $outputArray['dataArr']['BGEN-CLAIMALDTL'][$k]['BGEN-GCOCCNO']['#text'];
					$c++;
						}
					}		
			}
				
				}				
				return $dataOut;		
				break;
				case 'GETPDF':
				$converter= new xml2array($data); 
				$dataArr=$converter->getResult();
				$dataOut=array();
				$searchArray=array("<Status>","<FilePath>","<StreamData>");
				$searchArray1=array("</Status>","</FilePath>","</StreamData>");
				$dataArray1=str_replace($searchArray,"",@$dataArr['soapenv:Envelope']['soapenv:Body']['ns2:GET_PDFResponse']['return']['#text']);
				$textReturn=str_replace($searchArray1,"&",@$dataArray1);	
				$dataParse=explode("&",@$textReturn);
				$dataOut['Status']=@$dataParse[0];
				$dataOut['FilePath']=@$dataParse[1];
				$dataOut['StreamData']=@$dataParse[2];
				return $dataOut;		
				break;
                            	case 'GETBULKHEALTHCARDURL':
				$converter= new xml2array($data); 
				$dataArr=$converter->getResult();
				$dataOut=array();
				$searchArray=array("<Status>","<FilePath>","<StreamData>");
				$searchArray1=array("</Status>","</FilePath>","</StreamData>");
				$dataArray1=str_replace($searchArray,"",@$dataArr['soapenv:Envelope']['soapenv:Body']['ns2:GET_PDFResponse']['return']['#text']);
				$textReturn=str_replace($searchArray1,"&",@$dataArray1);	
				$dataParse=explode("&",@$textReturn);
				$dataOut['Status']=@$dataParse[0]; 
				$dataOut['FilePath']=@$dataParse[1];
				$dataOut['StreamData']=@$dataParse[2];
				return $dataOut;		
				break;

                            
                            
				case 'ClaimDetails':
				$converter= new xml2array($data); 
				$dataArr=$converter->getResult();
				$dataOut=array();
				$dataArray  = @$dataArr['SOAP:Envelope']['SOAP:Body']['GetClaimEnquiryResponse']['CLMALDETLIImplResponse'];		
				$dataArray['RESMSG1']=@$dataArr['SOAP:Envelope']['SOAP:Body']['GetClaimEnquiryResponse']['CLMALDETLIImplResponse']['RESMSG1']['copybook']['LEADER-HEADER'];
				$dataArray['RESMSG2']=@$dataArr['SOAP:Envelope']['SOAP:Body']['GetClaimEnquiryResponse']['CLMALDETLIImplResponse']['RESMSG2']['copybook']['CLMALDETLO-REC'];
			
				if(@$dataArray['RESMSG1']['MSGREFNO']['#text']!=''){
				$dataOut['MSGREFNO']	=@$dataArray['RESMSG1']['MSGREFNO']['#text'];
				$dataOut['USRPRF']		=@$dataArray['RESMSG1']['USRPRF']['#text'];
				$dataOut['WKSID']		=@$dataArray['RESMSG1']['WKSID']['#text'];
				$dataOut['OBJID']		=@$dataArray['RESMSG1']['OBJID']['#text'];
				$dataOut['VRBID']		=@$dataArray['RESMSG1']['VRBID']['#text'];
				$dataOut['TOTMSGLNG']	=@$dataArray['RESMSG1']['TOTMSGLNG']['#text'];
				$dataOut['OPMODE']		=@$dataArray['RESMSG1']['OPMODE']['#text'];
				$dataOut['CMTCONTROL']	=@$dataArray['RESMSG1']['CMTCONTROL']['#text'];
				$dataOut['RSPMODE']		=@$dataArray['RESMSG1']['RSPMODE']['#text'];
				$dataOut['MSGINTENT']	=@$dataArray['RESMSG1']['MSGINTENT']['#text'];
				$dataOut['MORE-IND']	=@$dataArray['RESMSG1']['MORE-IND']['#text'];
				$dataOut['ERRLVL']		=@$dataArray['RESMSG1']['ERRLVL']['#text'];
				
				$dataOut['MSGID']		=@$dataArray['RESMSG2']['MESSAGE-HEADER']['MSGID']['#text'];
				$dataOut['MSGLNG']		=@$dataArray['RESMSG2']['MESSAGE-HEADER']['MSGLNG']['#text'];
				$dataOut['MSGCNT']		=@$dataArray['RESMSG2']['MESSAGE-HEADER']['MSGCNT']['#text'];
				$dataOut['MSGLNG']		=@$dataArray['RESMSG2']['MESSAGE-HEADER']['MSGLNG']['#text'];
				
				$dataOut['MSGLNG']		=@$dataArray['RESMSG2']['MESSAGE-DATA']['MSGLNG']['#text'];
				$outputArray['dataArr'] = @$dataArray['RESMSG2']['MESSAGE-DATA']['ADDITIONAL-FIELDS'];
				$c=0;
				if(trim(@$outputArray['dataArr']['BGEN-MEMNAME']['#text'])!=''){
					$dataOut['claimList'][$c]['BGEN-MEMNAME']['#text']		= $outputArray['dataArr']['BGEN-MEMNAME']['#text'];
					$dataOut['claimList'][$c]['BGEN-CLAMNUM']['#text']		= $outputArray['dataArr']['BGEN-CLAMNUM']['#text'];
					$dataOut['claimList'][$c]['BGEN-CLNTNUM']['#text']		= $outputArray['dataArr']['BGEN-CLNTNUM']['#text'];
					$dataOut['claimList'][$c]['BGEN-BNFTGRP']['#text']		= $outputArray['dataArr']['BGEN-BNFTGRP']['#text'];
					$dataOut['claimList'][$c]['BGEN-GCSTS']['#text']		= $outputArray['dataArr']['BGEN-GCSTS']['#text'];
					$dataOut['claimList'][$c]['BGEN-CLMSRVTP']['#text']		= $outputArray['dataArr']['BGEN-CLMSRVTP']['#text'];
					$dataOut['claimList'][$c]['BGEN-DTEVISIT']['#text']		= $outputArray['dataArr']['BGEN-DTEVISIT']['#text'];
					$dataOut['claimList'][$c]['BGEN-INCURRED']['#text']		= $outputArray['dataArr']['BGEN-INCURRED']['#text'];
					$dataOut['claimList'][$c]['BGEN-TLHMOSHR']['#text']		= $outputArray['dataArr']['BGEN-TLHMOSHR']['#text'];	
					$dataOut['claimList'][$c]['BGEN-MBRNO']['#text']		= $outputArray['dataArr']['BGEN-MBRNO']['#text'];
					$dataOut['claimList'][$c]['BGEN-DPNTNO']['#text']		= $outputArray['dataArr']['BGEN-DPNTNO']['#text'];	
					$dataOut['claimList'][$c]['BGEN-CHDRNUM']['#text']		= $outputArray['dataArr']['BGEN-CHDRNUM']['#text'];
					$dataOut['claimList'][$c]['BGEN-PRODTYP']['#text']		= $outputArray['dataArr']['BGEN-PRODTYP']['#text'];	
					$dataOut['claimList'][$c]['BGEN-PLANNO']['#text']		= $outputArray['dataArr']['BGEN-PLANNO']['#text'];	
					$dataOut['claimList'][$c]['BGEN-DIAGCDE']['#text']		= $outputArray['dataArr']['BGEN-DIAGCDE']['#text'];	
					$dataOut['claimList'][$c]['BGEN-PROVORG']['#text']		= $outputArray['dataArr']['BGEN-PROVORG']['#text'];	
					$dataOut['claimList'][$c]['BGEN-GCFRPDTE']['#text']		= $outputArray['dataArr']['BGEN-GCFRPDTE']['#text'];	
					$dataOut['claimList'][$c]['BGEN-DTEVISIT']['#text']		= $outputArray['dataArr']['BGEN-DTEVISIT']['#text'];	
					$dataOut['claimList'][$c]['BGEN-DTEDCHRG']['#text']		= $outputArray['dataArr']['BGEN-DTEDCHRG']['#text'];	
					$dataOut['claimList'][$c]['BGEN-DOCRCVDT']['#text']		= $outputArray['dataArr']['BGEN-DOCRCVDT']['#text'];	
					$dataOut['claimList'][$c]['BGEN-TLMBRSHR']['#text']		= $outputArray['dataArr']['BGEN-TLMBRSHR']['#text'];	
					$dataOut['claimList'][$c]['BGEN-DDEDUCT']['#text']		= $outputArray['dataArr']['BGEN-DDEDUCT']['#text'];	
					$dataOut['claimList'][$c]['BGEN-COPAYAMT']['#text']		= $outputArray['dataArr']['BGEN-COPAYAMT']['#text'];
					$dataOut['claimList'][$c]['BGEN-GCOCCNO']['#text']		= $outputArray['dataArr']['BGEN-GCOCCNO']['#text'];
					$dataOut['claimList'][$c]['BGEN-SURNAME']['#text']		= $outputArray['dataArr']['BGEN-SURNAME']['#text'];
					$dataOut['claimList'][$c]['BGEN-PATNTYN']['#text']		= $outputArray['dataArr']['BGEN-PATNTYN']['#text'];
					if(@$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-ZRCODE']['#text']!=''){
					$dataOut['claimList']['DISSALWNCE'][$c]['BGEN-MEMNAME']['#text']		= @$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-ZRCODE']['#text'];
					$dataOut['claimList']['DISSALWNCE'][$c]['BGEN-CLAMNUM']['#text']		= @$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-DISALOWAMT']['#text'];
					$c++;
					}
					for($k=0;$k<count(@$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-ZRCODE']);$k++){
					$dataOut['claimList']['DISSALWNCE'][$c]['BGEN-MEMNAME']['#text']		= @$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-ZRCODE'][$k]['#text'];
					$dataOut['claimList']['DISSALWNCE'][$c]['BGEN-CLAMNUM']['#text']		= @$outputArray['dataArr']['BGEN-DISSALWNCE']['BGEN-DISALOWAMT'][$k]['#text'];
						$c++;
					}		
				}
				
				}				
				return $dataOut;		
				break;
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                
                                case 'PolicyCdStatement':
                                 $converter= new xml2array($data); 
				$dataArr=$converter->getResult();
				$dataOut=array();
				$dataArray  = @$dataArr['soapenv:Envelope']['soapenv:Body']['CDStatementWSResponse']['GetRtrnarvdataResponse']['tuple'];		
				 if(@$dataArray!=''){                 
                                    $c=0;
                                    $k=0;
                                    if (@$dataArray[$k]['old']['RTRNARV']['TRANDATE']) {
                                                        $dataOut[$c]['BGEN-TRANDATE'] = $dataArray['old']['RTRNARV'] ['TRANDATE'];
                                                        $dataOut[$c]['BGEN-TRANDESC'] = $dataArray['old']['RTRNARV'] ['TRANDESC'];                                                        
                                                        $dataOut[$c]['BGEN-RDOCNUM'] = $dataArray['old']['RTRNARV'] ['RDOCNUM'];
                                                        $dataOut[$c]['BGEN-TRANNO'] = $dataArray['old']['RTRNARV'] ['TRANNO'];
                                                        $dataOut[$c]['BGEN-ORIGAMT'] = $dataArray['old']['RTRNARV'] ['ORIGAMT'];
                                                        $dataOut[$c]['BGEN-GLSIGN'] = $dataArray['old']['RTRNARV'] ['GLSIGN'];
                                                        $c++;
                                                    }
					for($k=0;$k<count(@$dataArray);$k++){
                                                       if (@$dataArray[$k]['old']['RTRNARV']['TRANDATE']) {
                                                        $dataOut[$c]['BGEN-TRANDATE'] = $dataArray[$k]['old']['RTRNARV'] ['TRANDATE'];
                                                        $dataOut[$c]['BGEN-TRANDESC'] = $dataArray[$k]['old']['RTRNARV'] ['TRANDESC'];                                                        
                                                        $dataOut[$c]['BGEN-RDOCNUM'] = $dataArray[$k]['old']['RTRNARV'] ['RDOCNUM'];
                                                        $dataOut[$c]['BGEN-TRANNO'] = $dataArray[$k]['old']['RTRNARV'] ['TRANNO'];
                                                        $dataOut[$c]['BGEN-ORIGAMT'] = $dataArray[$k]['old']['RTRNARV'] ['ORIGAMT'];
                                                        $dataOut[$c]['BGEN-GLSIGN'] = $dataArray[$k]['old']['RTRNARV'] ['GLSIGN'];
                                                        $c++;
                                                    }
					} 
				}				
				return $dataOut;	
				break;
                                
		 case 'GETRENEWALPOLICYDETAILS':
	
				$xml = new DOMDocument();

				$xml->loadXML($data);

				$dataArr = xml_to_array($xml);

				$error  = @$dataArr['soapenv:Envelope']['soapenv:Body']['soapenv:Fault']['detail']['bpm:FaultDetails']['cordys:FaultDetailString']['#text'];				
				if(isset($error)){	
					$error = "Please try after some time.";
					$_SESSION['response_error'] = @$error;					
				}else{
					$dataArray  = @$dataArr['soapenv:Envelope']['soapenv:Body']['GetRenewalPolicyDetails_WSResponse']['policyIO']['policy'];	
					if($dataArray!=''){
						$c=0;
						$dataOut[$c]['policyNum']['#text'] 			 = $dataArray['policyNum']['#text'];
						$dataOut[$c]['baseProductId']['#text'] 		 = $dataArray['baseProductId']['#text'];
						$dataOut[$c]['baseProductFamilyCd']['#text'] = $dataArray['baseProductFamilyCd']['#text'];
						$dataOut[$c]['policyMaturityDt']['#text'] = $dataArray['policyMaturityDt']['#text'];
						$dataOut[$c]['policyCommencementDt']['#text'] = $dataArray['policyCommencementDt']['#text'];
						if(count($dataArray['partyDO'])>0){
						$dataOut[$c]['customerId']['#text']			 = $dataArray['partyDO'][$c]['customerId']['#text']?$dataArray['partyDO'][$c]['customerId']['#text']:$dataArray['partyDO']['customerId']['#text'];
						$dataOut[$c]['firstName1']['#text']			 = $dataArray['partyDO'][$c]['firstName1']['#text']?$dataArray['partyDO'][$c]['firstName1']['#text']:$dataArray['partyDO']['firstName1']['#text'];
						$dataOut[$c]['lastName1']['#text']			 = $dataArray['partyDO'][$c]['lastName1']['#text']?$dataArray['partyDO'][$c]['lastName1']['#text']:$dataArray['partyDO']['lastName1']['#text'];
						$dataOut[$c]['birthDt']['#text']			 = $dataArray['partyDO'][$c]['birthDt']['#text']?$dataArray['partyDO'][$c]['birthDt']['#text']:$dataArray['partyDO']['birthDt']['#text'];
						$dataOut[$c]['genderCd']['#text']			 = $dataArray['partyDO'][$c]['genderCd']['#text']?$dataArray['partyDO'][$c]['genderCd']['#text']:$dataArray['partyDO']['genderCd']['#text'];
						$dataOut[$c]['titleCd']['#text']			 = $dataArray['partyDO'][$c]['titleCd']['#text']?$dataArray['partyDO'][$c]['titleCd']['#text']:$dataArray['partyDO']['titleCd']['#text'];
						} else {
						$dataOut[$c]['customerId']['#text']			 = $dataArray['partyDO']['customerId']['#text'];
						$dataOut[$c]['firstName1']['#text']			 = $dataArray['partyDO']['firstName1']['#text'];
						$dataOut[$c]['lastName1']['#text']			 = $dataArray['partyDO']['lastName1']['#text'];
						$dataOut[$c]['birthDt']['#text']			 = $dataArray['partyDO']['birthDt']['#text'];
						$dataOut[$c]['genderCd']['#text']			 = $dataArray['partyDO']['genderCd']['#text'];
						$dataOut[$c]['titleCd']['#text']			 = $dataArray['partyDO']['titleCd']['#text'];
						}
						
						if(count($dataArray['partyDO'][$c]['partyAddressDO'])>0){
							for($k=0;$k<count(@$dataArray['partyDO'][$c]['partyAddressDO']);$k++){
								$dataOut[$k]['addressTypeCd']['#text']		= $dataArray['partyDO'][$c]['partyAddressDO'][$k]['addressTypeCd']['#text'];
								$dataOut[$k]['addressLine1Lang1']['#text']	= $dataArray['partyDO'][$c]['partyAddressDO'][$k]['addressLine1Lang1']['#text'];
								$dataOut[$k]['addressLine2Lang1']['#text']	= $dataArray['partyDO'][$c]['partyAddressDO'][$k]['addressLine2Lang1']['#text'];
								$dataOut[$k]['countryCd']['#text']			= $dataArray['partyDO'][$c]['partyAddressDO'][$k]['countryCd']['#text'];
								$dataOut[$k]['stateCd']['#text']			= $dataArray['partyDO'][$c]['partyAddressDO'][$k]['stateCd']['#text'];
								$dataOut[$k]['cityCd']['#text']				= $dataArray['partyDO'][$c]['partyAddressDO'][$k]['cityCd']['#text'];
								$dataOut[$k]['pinCode']['#text']			= $dataArray['partyDO'][$c]['partyAddressDO'][$k]['pinCode']['#text'];
								$dataOut[$k]['areaCd']['#text']				= $dataArray['partyDO'][$c]['partyAddressDO'][$k]['areaCd']['#text'];
								
							}
						}
						
						if(count($dataArray['partyDO'][$c]['partyContactDO'])>0){
							$dataOut[$c]['contactTypeCd']['#text']	= $dataArray['partyDO'][$c]['partyContactDO']['contactTypeCd']['#text'];
							$dataOut[$c]['contactNum']['#text']		= $dataArray['partyDO'][$c]['partyContactDO']['contactNum']['#text'];
							$dataOut[$c]['stdCode']['#text']		= $dataArray['partyDO'][$c]['partyContactDO']['stdCode']['#text'];
						}
						if(count($dataArray['partyDO'][$c]['partyEmailDO'])>0){
							$dataOut[$c]['emailTypeCd']['#text']	= $dataArray['partyDO'][$c]['partyEmailDO']['emailTypeCd']['#text'];
							$dataOut[$c]['emailAddress']['#text']	= $dataArray['partyDO'][$c]['partyEmailDO']['emailAddress']['#text'];
						}
						
						
						for($k=0;$k<count(@$dataArray['partyDO']['partyAddressDO']);$k++){
							$dataOut[$k]['addressTypeCd']['#text']		= $dataArray['partyDO']['partyAddressDO'][$k]['addressTypeCd']['#text'];
							$dataOut[$k]['addressLine1Lang1']['#text']	= $dataArray['partyDO']['partyAddressDO'][$k]['addressLine1Lang1']['#text'];
							$dataOut[$k]['addressLine2Lang1']['#text']	= $dataArray['partyDO']['partyAddressDO'][$k]['addressLine2Lang1']['#text'];
							$dataOut[$k]['countryCd']['#text']			= $dataArray['partyDO']['partyAddressDO'][$k]['countryCd']['#text'];
							$dataOut[$k]['stateCd']['#text']			= $dataArray['partyDO']['partyAddressDO'][$k]['stateCd']['#text'];
							$dataOut[$k]['cityCd']['#text']				= $dataArray['partyDO']['partyAddressDO'][$k]['cityCd']['#text'];
							$dataOut[$k]['pinCode']['#text']			= $dataArray['partyDO']['partyAddressDO'][$k]['pinCode']['#text'];
							$dataOut[$k]['areaCd']['#text']				= $dataArray['partyDO']['partyAddressDO'][$k]['areaCd']['#text'];
							
						}/*
						if(count($dataArray['partyDO']['partyAddressDO'])>1){
							$c2=1;
							$dataOut[$c2]['addressTypeCd']['#text']		= $dataArray['partyDO'][$c2]['partyAddressDO'][$c2]['addressTypeCd']['#text'];
							$dataOut[$c2]['addressLine1Lang1']['#text']	= $dataArray['partyDO'][$c2]['partyAddressDO'][$c2]['addressLine1Lang1']['#text'];
							$dataOut[$c2]['countryCd']['#text']			= $dataArray['partyDO'][$c2]['partyAddressDO'][$c2]['countryCd']['#text'];
							$dataOut[$c2]['stateCd']['#text']			= $dataArray['partyDO'][$c2]['partyAddressDO'][$c2]['stateCd']['#text'];
							$dataOut[$c2]['cityCd']['#text']				= $dataArray['partyDO'][$c2]['partyAddressDO'][$c2]['cityCd']['#text'];
							$dataOut[$c2]['pinCode']['#text']			= $dataArray['partyDO'][$c2]['partyAddressDO'][$c2]['pinCode']['#text'];
							$dataOut[$c2]['areaCd']['#text']				= $dataArray['partyDO'][$c2]['partyAddressDO'][$c2]['areaCd']['#text'];
						}*/
						
						if(count($dataArray['partyDO']['partyContactDO'])>0){
							$dataOut[$c]['contactTypeCd']['#text']	= $dataArray['partyDO']['partyContactDO']['contactTypeCd']['#text'];
							$dataOut[$c]['contactNum']['#text']		= $dataArray['partyDO']['partyContactDO']['contactNum']['#text'];
							$dataOut[$c]['stdCode']['#text']		= $dataArray['partyDO']['partyContactDO']['stdCode']['#text'];
						}
						if(count($dataArray['partyDO']['partyEmailDO'])>0){
							$dataOut[$c]['emailTypeCd']['#text']	= $dataArray['partyDO']['partyEmailDO']['emailTypeCd']['#text'];
							$dataOut[$c]['emailAddress']['#text']	= $dataArray['partyDO']['partyEmailDO']['emailAddress']['#text'];
						}
						


						
					/*for($k=0;$k<count(@$dataArray['partyDO']);$k++){
							$dataOut['MEMBERLIST'][$k]['firstName1']['#text']	= @$dataArray['partyDO'][$k]['firstName1']['#text'];
							$dataOut['MEMBERLIST'][$k]['lastName1']['#text']	= @$dataArray['partyDO'][$k]['lastName1']['#text'];
							$dataOut['MEMBERLIST'][$k]['birthDt']['#text']		= @$dataArray['partyDO'][$k]['birthDt']['#text'];
							if(isset($dataArray['partyDO'][$k]['birthDt']['#text']) && !empty($dataArray['partyDO'][$k]['birthDt']['#text'])){
								$explodedRes=explode("-",@$dataArray['partyDO'][$k]['birthDt']['#text']);
								$age=(date('Y',time())-$explodedRes[0]);
							}
							$dataOut['MEMBERLIST'][$k]['age']['#text']		= @$age;
							$dataOut['MEMBERLIST'][$k]['genderCd']['#text']		= @$dataArray['partyDO'][$k]['genderCd']['#text'];
							$dataOut['MEMBERLIST'][$k]['titleCd']['#text']		= @$dataArray['partyDO'][$k]['titleCd']['#text'];
					}*/
					}
				}	
				return $dataOut;			
				break;

                                
		 case 'CorporatePolicyEnquiry':	
				$converter= new xml2array($data); 
				$dataArr=$converter->getResult();
				$responseArray=array();
				$error  = @$dataArr['SOAP:Envelope']['SOAP:Body']['SOAP:Fault']['detail']['bpm:FaultDetails']['cordys:FaultDetailString']['#text'];				
				if(isset($error)){	
					$error = "Please try after some time.";
					$_SESSION['response_error'] = @$error;					
				}else{
					$outputArray['dataArr']       	= @$dataArr['SOAP:Envelope']['SOAP:Body']['getPolicyEnquiryResponse']['RESMSG2']['copybook']['POLENQO-REC']['MESSAGE-DATA']['ADDITIONAL-FIELDS']['BGEN-DPNTDTL'];
					
					$counter = 0;
					$responseArray = array();
					
					if(is_array(@$outputArray['dataArr'])){		
						foreach(@$outputArray['dataArr'] as $key  => $value){ 	
								if(count($value) == 1){
									$counter = 0;
									$responseArray[$counter][$key] = $value;
								}else{
									if($value['BGEN-MEMNAME']['#text']){
										$responseArray[$counter] = $value;
									}
								}
							$counter++;			
						}
					}
					
					@$responseArray;
				}	
				
				return $responseArray;			
				break;
		}		
		
	}
?>