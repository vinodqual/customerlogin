<?php 
/*****************************************************************************
* COPYRIGHT
* Copyright 2015 Catabatic Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: view_reimbursement.php,v 1.0 2015/10/07 05:30:15 amit kumar dubey Exp $
* $Author: amit kumar dubey $
*
****************************************************************************/
include_once("conf/conf.php");           //include configuration file 
include_once("conf/common_functions.php");          // include function file
include_once("conf/session.php");		//including session file 
$requestId=sanitize_data(base64_decode(@$_REQUEST['id']));		//decode requested id

if($_SESSION['LOGINTYPE']=='CORPORATE'){
	$policy=sanitize_data(@$_SESSION['policyNumber']);
	$wellnessList=fetchPolicyDetails($policy);// check wellness option 
	$planId=sanitize_data(base64_decode(@$_REQUEST['id']));
	$purchaseId=sanitize_data(base64_decode(@$_REQUEST['pId']));
	$planDetails=get_particular_plan_detailsall($planId,@$wellnessList[0]['POLICYID']);
 } else {
	$planId=sanitize_data(base64_decode(@$_REQUEST['id']));
	$planDetails=get_particular_retplan_details_for_policy($planId,@$_SESSION["productId"]);
 }
?>
<script>
     $(function(){
         $('.closeBox').click(function(){
		//location.reload();
		  });
      });
 </script>


    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close closeBox" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <div class="model-header" id="exampleModalLabel">VIEW HEALTH CHECK-UP PLAN</div>
      </div>
      <div class="modal-middle" id="reimbursmentdoc">
                    <table class="responsive" width="100%">
						  <tr>
							<td height="30" >Name</td>
							<td height="30">:</td>
							<td height="30"><?php echo $planDetails[0]['PLANNAME']?stripslashes($planDetails[0]['PLANNAME']):stripslashes($planDetails[0]['PLANNAME2']);?></td>
						  </tr>
                          <?php if($planDetails[0]['PLANTYPE']!='LIMITED'){ ?>
						  <tr>
							<td class="whitebg" height="30">Age</td>
							<td class="whitebg" height="30">:</td>
							<td class="whitebg" height="30">
							<?php 
							 $pieces = explode(',' , @$planDetails[0]['AGEGROUPID']);
							 echo @$pieces?implode(getRanges(@$pieces),','):'NA';
							?>
                             </td>
						  </tr>
						  <tr>
							<td height="30">Grade/SI</td>
							<td height="30">:</td>
							<td height="30"><?php	
					$siName1="";
					$si_list = explode(',' , $planDetails[0]['SI']);
					for($j=0;$j<count($si_list);$j++) {
							$siName=fetchListById('CRMSI','SIID',$si_list[$j]); 
							$siName1.=@$siName[0]['SI'].", ";
					}
					echo $siName1?substr($siName1,0,-2):'NA';
					 ?></td>
						  </tr>
						  <tr>
							<td  class="whitebg" height="30">Gender</td>
							<td  class="whitebg" height="30">:</td>
							<td  class="whitebg" height="30"><?php echo $planDetails[0]['GENDER']?stripslashes(@$planDetails[0]['GENDER']):'NA';?></td>
						  </tr>
                          
						  <tr>
							<td height="30">Price</td>
							<td height="30">:</td>
							<td height="30">
							<?php  if($planDetails[0]['PAYMENTOPTIONS']=='BUY') {
							
							if($_SESSION['LOGINTYPE']=='CORPORATE'){
								
								$purchaseDetail = fetchListCondsWithColumn('TOTALAMOUNT,COST',"CRMEMPLOYEEPURCHASEPLAN","where CRMEMPLOYEEPURCHASEPLAN.PURCHASEPLANID=".@$purchaseId."");
							} else {
								$purchaseDetail = fetchListCondsWithColumn('TOTALAMOUNT,COST',"CRMRETEMPLOYEEPURCHASEPLAN","where CRMRETEMPLOYEEPURCHASEPLAN.PURCHASEPLANID=".@$purchaseId."");
							}
							if(isset($planDetails[0]['COST']) && !empty($planDetails[0]['COST'])){
							echo @$planDetails[0]['COST'];
							} else {
								echo stripslashes(@$purchaseDetail[0]['TOTALAMOUNT'])?stripslashes(@$purchaseDetail[0]['TOTALAMOUNT']):'NA';
							}
						   } else { echo "Free"; };?>
						  </td>
						  </tr>
                          <?php } ?>
						  <tr>
							<td class="whitebg" height="30">Description</td>
							<td class="whitebg" height="30">:</td>
							<td class="whitebg" height="30"><?php echo stripslashes(@$planDetails[0]['DESCRIPTION']);?></td>
						  </tr>
						  <?php if(stripslashes(@$planDetails[0]['UPLOADPDF'])!=''){?>
						  <tr>
							<td height="30">PDF</td>
							<td height="30">:</td>
							<td height="30">
                                            <!------------ Code added for download plan document -------------------> 
                                                    <?php   $folder_name=PLAN_DOC;
                                                            $url=create_url($folder_name,$planDetails[0]['UPLOADPDF'],CUSTOMERLOGIN);        // Create File Download Link
                                                            if($url!==FALSE){
                                                                echo '<a href="'.$url.'"><img src="images/pdf_icon.png" border="0" /></a>';
                                                            }else{
                                                                echo SFTP0026;
                                                            }
                                                             ?>
<!--                                                            <a style="color:#226A35;" href="download.php?fileName=<?php echo @$planDetails[0]['UPLOADPDF'];?>" class="view_nav"><img src="img/pdficon.png" border="0" alt="" class="fl"></a>-->
                                                        </td>
						  </tr>
						  <?php } else  if(stripslashes(@$planDetails[0]['UPLOADPDF2'])!=''){?>
						  <tr>
							<td height="30">PDF</td>
							<td height="30">:</td>
							<td height="30">
                                                            <?php   $folder_name=PLAN_DOC;
                                                            $url=create_url($folder_name,$planDetails[0]['UPLOADPDF2'],CUSTOMERLOGIN);        // Create File Download Link
                                                            if($url!==FALSE){
                                                                echo '<a href="'.$url.'"><img src="images/pdf_icon.png" border="0" /></a>';
                                                            }else{
                                                                echo SFTP0026;
                                                            }
                                                             ?>
<!--                                                            <a style="color:#226A35;" href="download.php?fileName=<?php echo @$planDetails[0]['UPLOADPDF2'];?>" class="view_nav"><img src="img/pdficon.png" border="0" alt="" class="fl"></a>-->
                                                        </td>
                                                        
                                                        <!-------------- End of Code ------------------------>
						  </tr>
						  <?php } else { }  ?>
					</table>      
      </div>
      
    </div>
                     