<?php
/*****************************************************************************
* COPYRIGHT
* Copyright 2013 Catabatic Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: getAutocompleteResRet.php,v 1.0 2015/10/06 11:54:29 amit dubey Exp $
* $Author: amit dubey $
*
****************************************************************************/
include_once("../conf/conf.php");           //include configuration file 
include_once("../conf/common_functions.php");          // include function file
$q=sanitize_data(@$_REQUEST['q']);
ini_set('display_errors', 1);
$table=sanitize_data(@$_REQUEST['type']);
$name=sanitize_data(@$_REQUEST['name']);
$compId=sanitize_data(@$_REQUEST['compId']);
$stateid=sanitize_data(@$_REQUEST['state']);
$cityid=sanitize_data(@$_REQUEST['city']);
$planId=sanitize_data(@$_REQUEST['planId']);
$policyId=sanitize_data(@$_REQUEST['policyId']);
$policyNumber=sanitize_data(@$_REQUEST['policyNumber']);// requested type,name,company id,policy id,policy number

if($_SESSION['LOGINTYPE']=='CORPORATE'){
	if(isset($policyNumber) && !empty($policyNumber)) {
	$items=SearchAutoCenter($q,$table,$name,$compId,$policyNumber,"AND CRMCOMPANYCENTERMAPPING.POLICYNUMBER='".@$policyNumber."' ",$stateid,$cityid,$planId);//fuction to fetch records
	} else {
	$items=SearchAutoCenter($q,$table,$name,$compId,$policyId,"AND CRMCOMPANYCENTERMAPPING.POLICYID='".@$policyId."' ",$stateid,$cityid,$planId);//fuction to fetch records
	}
} else {
if(isset($policyNumber) && !empty($policyNumber)) {
	$items=SearchRetAutoCenter($q,$table,$name,$compId,$policyNumber,"AND CRMRETAILCENTERMAPPING.POLICYNUMBER='".@$policyNumber."' ",$stateid,$cityid,$planId);//fuction to fetch records
	} else {
	$items=SearchRetAutoCenter($q,$table,$name,$compId,$policyId,"AND CRMRETAILCENTERMAPPING.POLICYID='".@$policyId."' ",$stateid,$cityid,$planId);//fuction to fetch records
	}

}

if(count($items)>0) {
	if(is_array($items))
	{ 
	
		echo json_encode($items);
	}
	else
	{
		echo json_encode(array(0=>"No match"));		//error massege when records does not match
	}
} else {
		echo json_encode(array(0=>"No match"));		//error massege when records does not match
} 
	
?>


