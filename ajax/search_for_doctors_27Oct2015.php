<?php 
include_once("../conf/conf.php");           //include configuration file  
include_once("../conf/common_functions.php");          // include function file
 
$search_id= $_REQUEST['search_id'];
$city_id= $_REQUEST['city_id']; 
$location_info=  explode("|", $city_id);
$location_info_id=$location_info[0];
$location_info_type=@$location_info[1]; 
     
$city_condition="";
if ($location_info_id != "") {
    $city_condition = " WHERE ";
    if ($location_info_type == "CITY")
        $hospital_per_city = fetchListCondsWithColumn("CENTERID", "LWCENTERMASTER", " where CITYID =" . $location_info_id);
    else
        $hospital_per_city = fetchListCondsWithColumn("CENTERID", "LWCENTERMASTER", " where STATEID =" . $location_info_id);
    $hospital_array = array();
    foreach ($hospital_per_city as $hospital_info) {
        $hospital_array[] = $hospital_info['CENTERID'];
    } 
    $hospital_str = implode(",", $hospital_array); 
    $city_condition = " AND LWDOCTORNEWMASTER.CENTERID IN(" . $hospital_str . ")"; 
}
 
$search_info=  explode("|",$search_id);
$search_info_id=$search_info[0];
$search_info_type=$search_info[1];
$doctor_search=array();


/* Find services  Session policy*/
include("fetch_retail_or_corp_service.php");
//if($order=="")
    
$order=$_REQUEST['sorting_id'];
if ($search_info_type == "DOCTOR") {
    $doctor_search = fetchListCondsWithColumn("DOCTORID,DOCTORNAME,CENTERID,SPECIALITYID,QUALIFICATION,CERTIFICATIONS,DESCRIPTION,CONTACTNUMBER,OPDTIMING,APPOINTMENTNUMBER", "LWDOCTORNEWMASTER", " where DOCTORID='" . strtolower($search_info_id) . "' " . $city_condition); //function to fetch doctor records
}
if ($search_info_type == "HOSPITAL") {
    if($order=="DOCTORNAME ASC" || $order=="DOCTORNAME DESC")
    {
         $order_str=" ORDER BY ".$order;
         $doctor_search = fetchListCondsWithColumn("DOCTORID,DOCTORNAME,CENTERID,SPECIALITYID,QUALIFICATION,CERTIFICATIONS,DESCRIPTION,CONTACTNUMBER,OPDTIMING,APPOINTMENTNUMBER", "LWDOCTORNEWMASTER", " where SERVICEID IN(" . $service_of_policy . ") AND CENTERID='" . strtolower($search_info_id) . "'" . $city_condition.$order_str); //function to fetch doctor records
    }
    else if($order=="RATING ASC" || $order=="RATING DESC")
    {
        $doctor_info= fetchListCondsWithColumn("DOCTORID,DOCTORNAME,CENTERID,SPECIALITYID,QUALIFICATION,CERTIFICATIONS,DESCRIPTION,CONTACTNUMBER,OPDTIMING,APPOINTMENTNUMBER", "LWDOCTORNEWMASTER", " where SERVICEID IN(" . $service_of_policy . ") AND CENTERID='" . strtolower($search_info_id) . "'" . $city_condition); //function to fetch doctor records
        include("feedback_sort.php");  
    }
    else
    {
       $doctor_search = fetchListCondsWithColumn("DOCTORID,DOCTORNAME,CENTERID,SPECIALITYID,QUALIFICATION,CERTIFICATIONS,DESCRIPTION,CONTACTNUMBER,OPDTIMING,APPOINTMENTNUMBER", "LWDOCTORNEWMASTER", " where SERVICEID IN(" . $service_of_policy . ") AND CENTERID='" . strtolower($search_info_id) . "'" . $city_condition); //function to fetch doctor records 
    }
   
} 
if ($search_info_type == "SERVICE") { 
    $service_info = fetchListCondsWithColumn("SHOWCENTERORDOCTOR", "LWSERVICEMASTER", " where SERVICEID=" . $search_info_id);
    if ($service_info[0]["SHOWCENTERORDOCTOR"] == "SHOWDOCTOR") { 
                    if($order=="DOCTORNAME ASC" || $order=="DOCTORNAME DESC")
                    {
                         $order_str=" ORDER BY LWDOCTORNEWMASTER.".$order;
                         $doctor_search = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.*", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where   LWDOCTORNEWMASTER.SERVICEID='" . $search_info_id . "' " . $city_condition.$order_str);
                    }
                   else if($order=="RATING ASC" || $order=="RATING DESC")
                    { 
                       $doctor_info = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.*", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where   LWDOCTORNEWMASTER.SERVICEID='" . $search_info_id . "' " . $city_condition);
                       include("feedback_sort.php");

                    }
                    else if($order=="DISCOUNTOFFER ASC" || $order=="DISCOUNTOFFER DESC")
                    {
                            $order_str=" ORDER BY LWCENTERMASTER.".$order;
                            $doctor_search = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.*", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where   LWDOCTORNEWMASTER.SERVICEID='" . $search_info_id . "' " . $city_condition.$order_str);

                    } 
    } 
    
    
    
    else {
            $cond = " LWDOCTORNEWMASTER.SERVICEID='" . $search_info_id . "'";
            $city_condition = "";
            if ($location_info_id != "") {
                $city_condition = " AND LWCENTERMASTER.CENTERID IN(" . $hospital_str . ")";
            }
            if($order=="DISCOUNTOFFER ASC" || $order=="DISCOUNTOFFER DESC")
            {
                $order_str=" ORDER BY LWCENTERMASTER.".$order;
                $center_list = FetchJoinResultINNER("DISTINCT LWCENTERMASTER.CENTERID  ", "LWDOCTORNEWMASTER.CENTERID,LWCENTERMASTER.CENTERNAME,LWCENTERMASTER.ADDRESS1,LWCENTERMASTER.ADDRESS2,LWCENTERMASTER.CITYID,LWCENTERMASTER.STATEID,LWCENTERMASTER.PINCODE,LWCENTERMASTER.PHONENUMBER,LWCENTERMASTER.DISCOUNTOFFER", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where   " . $cond . " " . $city_condition.$order_str);       
            }
            else{
                $center_list = FetchJoinResultINNER("DISTINCT LWCENTERMASTER.CENTERID  ", "LWDOCTORNEWMASTER.CENTERID,LWCENTERMASTER.CENTERNAME,LWCENTERMASTER.ADDRESS1,LWCENTERMASTER.ADDRESS2,LWCENTERMASTER.CITYID,LWCENTERMASTER.STATEID,LWCENTERMASTER.PINCODE,LWCENTERMASTER.PHONENUMBER,LWCENTERMASTER.DISCOUNTOFFER", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where   " . $cond . " " . $city_condition);
            }
        
        }
}
if ($search_info_type == "SPECIALTY") {
    
      if ($order == "DOCTORNAME ASC" || $order == "DOCTORNAME DESC") {
        $order_str = " ORDER BY LWDOCTORNEWMASTER." . $order;
        $doctor_search = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.*", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where   INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $search_info_id . "',1,1)>0 " . $city_condition . $order_str);
    } else if ($order == "RATING ASC" || $order == "RATING DESC") {
        $doctor_info = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.*", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $search_info_id . "',1,1)>0  " . $city_condition);
        include("feedback_sort.php");
    } else if ($order == "DISCOUNTOFFER ASC" || $order == "DISCOUNTOFFER DESC") {
        $order_str = " ORDER BY LWCENTERMASTER." . $order;
        $doctor_search = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.*", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where  INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $search_info_id . "',1,1)>0  " . $city_condition . $order_str);
    }


    //$doctor_search = fetchDoctorFromSecialty($search_info_id, $city_condition);
}
if ($search_info_type == "AILMENT") {
   
    
    
     $specialty_ids = fetchListCondsWithColumn("SPECAILTYID", "LWSPECIALTY_AILMENTMASTER", " where AILMENTID=" . $search_info_id); 
   $dataArray=array();
     foreach ($specialty_ids as $spec_id) {
       
    if ($order == "DOCTORNAME ASC" || $order == "DOCTORNAME DESC") {
        $order_str = " ORDER BY LWDOCTORNEWMASTER." . $order;
        $doctor_search = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.*", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where   INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec_id['SPECAILTYID'] . "',1,1)>0 " . $city_condition . $order_str);
    } else if ($order == "RATING ASC" || $order == "RATING DESC") {
        $doctor_info = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.*", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec_id['SPECAILTYID'] . "',1,1)>0  " . $city_condition);
        include("feedback_sort.php");
    } else if ($order == "DISCOUNTOFFER ASC" || $order == "DISCOUNTOFFER DESC") {
        $order_str = " ORDER BY LWCENTERMASTER." . $order;
        $doctor_search = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.*", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where  INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec_id['SPECAILTYID'] . "',1,1)>0  " . $city_condition . $order_str);
    }
         $dataArray = array_merge($dataArray, $doctor_search);
    }
    
    $doctor_search= $dataArray;
    
    
    
    //$doctor_search = fetchDoctorFromAilment($search_info_id, $city_condition);
}

 
                    if(count(@$doctor_search) > 0) {
    $i=0;$k=1; 
    while(count($doctor_search)>$i){ 
              $doctors = fetchListCondsWithColumn("DOCTORID,DOCTORNAME,CENTERID,SPECIALITYID,QUALIFICATION,CERTIFICATIONS,DESCRIPTION,CONTACTNUMBER,OPDTIMING,APPOINTMENTNUMBER", "LWDOCTORNEWMASTER", " where DOCTORID='" . $doctor_search[$i]["DOCTORID"] . "' "); //function to fetch doctor records
              $speciality_of_doc= $doctors[0]["SPECIALITYID"];
              $speciality_of_doc_arr=  explode(",",$speciality_of_doc);
              $spec_name_arr=array();
              $hospital_info=fetchHospitalDetailOfDoctor($doctors[0]["CENTERID"]);   
              
              foreach ($speciality_of_doc_arr as $list) {
                            $spec_names = fetchListCond("LWSPECIALTYMASTER", " where STATUS='ACTIVE' AND SPECIALTYID =" . $list);
                            if (isset($spec_names[0]["SPECIALTYNAME"]) && !empty($spec_names[0]["SPECIALTYNAME"])) {
                                $spec_name = @$spec_names[0]["SPECIALTYNAME"];
                                $spec_name_arr[] = @$spec_name;
                            }
              }
              $specialty_names=  implode(",",$spec_name_arr); 
                    ?>
                      <div class="wecare-leftTop">
      	<div class="wecare-leftTopIn">
        	<div class="topBar"> <?php echo $doctors[0]["DOCTORNAME"];?> <span><?php //echo $specialty_names;?> </span></div>
            
            <div class="doclorAddressLeft">
            	<p><?php echo @$doctors[0]["QUALIFICATION"] ;?>( <?php echo @$hospital_info[0]["CENTERNAME"];  ?>)<br />
                 <?php echo $doctors[0]["CONTACTNUMBER"];  ?><br />
                Appointment No. <span><?php echo $doctors[0]["APPOINTMENTNUMBER"];  ?></span><br />
                OPD Timing <span><?php echo $doctors[0]["OPDTIMING"];  ?></span></p>
                <?php $doctor_id= $doctors[0]['DOCTORID']; ?>
                <a   class="smsIcon" style="cursor:pointer;" onclick="doctor_sms('<?php echo $doctor_id;  ?>');"> </a>
                <a   class="mailIcon" style="cursor:pointer;" onclick="network_doctor_email('<?php echo $doctor_id;  ?>');" ></a>
            </div>
            
            <div class="doclorAddressRight">
            	<div class="topStar"> 
                 <?php
                 
                    $feedbacks=fetchListCond("LWDOCTORFEEDBACK"," where DOCTORID=". $doctors[0]["DOCTORID"]." order by CREATEDON DESC"); 
                    $total_feedback=count($feedbacks);
                    $avg_rating=0;
                    if($total_feedback>0){
                      $tot_rate=0;
                      foreach($feedbacks as $feedback){
                      $tot_rate=$feedback["RATE"]+$tot_rate; 
                      }  
                     $avg_rating= number_format(abs($tot_rate/$total_feedback),1);
                    }

                 
            if($total_feedback>1){ $user_text="Users";}else{$user_text="User";}
            $star_text="";
            if($avg_rating>1){ $star_text="Stars";}
            else if($avg_rating==1){$star_text="Star";} 
            else {$star_text=="";}
            if($total_feedback>0){ echo $avg_rating." ".$star_text." from ".$total_feedback. " ".$user_text;} else{ ?>
                    <a onclick="feedback(<?php echo $doctors[0]["DOCTORID"];?>);"  style="cursor: pointer; " class="topStar"  >Be First to Rate <?php echo @$doctors[0]["DOCTORNAME"];?> </a>
            <?php } ?>
                   </div><br />
                <p><a  style="text-decoration:none;" class="feedbacknav"><?php if($total_feedback>0){ echo $total_feedback;} else { echo "No"; } ?> Feedback</a><br />
                <span class="officon"><?php echo @$hospital_info[0]["DISCOUNTOFFER"];  ?></span><br />
                <span class="locationicon"><?php echo @$hospital_info[0]["CENTERNAME"];  ?> </span><br/>
                <span  style="margin-left:32px;"> <?php echo @$hospital_info[0]["CITYNAME"].",".@$hospital_info[0]["STATENAME"];  ?></span>
                </p><br />
                <div class="bottomNav">
                 <a class="generate-btn left" onclick="generate_coupon(<?php echo $doctors[0]["DOCTORID"];?>)" style="cursor: pointer;" >Generate Coupon</a>
                <a class="fedback-btn left" onclick="feedback(<?php echo $doctors[0]["DOCTORID"];?>)"  style="cursor: pointer;"  >Feedback</a>
                <div class="clearfix"></div></div>
            </div>
        <div class="clearfix"></div>
     
        
        </div>
        <div align="center" ><img id="generate_coupon_load_image<?php echo $doctors[0]["DOCTORID"];?>" src="images/loading.gif" style="display:none;"></div>
        
        <div class="wecare-generateCoupon"   id="coupon_form<?php echo $doctors[0]["DOCTORID"];?>" style="display:none;">
        	 
        </div>
        
      </div>
                    <?php $i++;
                    
                            } } 
           elseif(count(@$center_list) > 0) 
           {
             include("center_result.php");
           }
            else
           {
              // echo "No Record Found";
           }
	?>

 
<script type="text/javascript">
$(".custom-checkbox").click(function () {
	$(this).toggleClass('custom-checkboxActive');
});

$("h3").click(function(){ 
	$(".selectColorMain").removeClass("selectColorMain");
	$(this).parent().parent().children().addClass("selectColorMain");
	
	$(".addressDetail").hide();
	$(this).parent().parent().children().children().show();	
	
	$(".srcharrowup1").removeClass("srcharrowup1");
	$(this).parent().parent().children().children().children("span").addClass("srcharrowup1");
});
 function generate_coupon(doc_id)
    { 
        $("#generate_coupon_load_image"+doc_id).show();
         $(".wecare-generateCoupon").hide();
        $(".wecare-generateCoupon").html("");
        $.ajax({
                type:"post",
                url:'ajax/generate_coupon.php',
                 data:{'id':doc_id,'policyNumber':<?php echo $_SESSION['policyNumber']; ?>},
                success:function(resp){         
                        $("#coupon_form"+doc_id).show();
                        $("#coupon_form"+doc_id).html(resp);
                         $("#generate_coupon_load_image"+doc_id).hide();
                        } 
                });
    }  
   function feedback(doc_id)
    { 
         $(".wecare-generateCoupon").hide();
         $("#generate_coupon_load_image"+doc_id).show();
         
        $(".wecare-generateCoupon").html("");
         $.ajax({
                type:"post",
                url:'ajax/feedback.php',
                data:{'id':doc_id},
                success:function(resp){     
                     $("#coupon_form"+doc_id).show();
                     $("#coupon_form"+doc_id).html(resp);
                      $("#generate_coupon_load_image"+doc_id).hide();
                      //  $("#inline_example1").html(resp);
                        } 
                });
    }

 
function doctor_sms(doc_id)
{       $(".wecare-generateCoupon").hide();
      $("#generate_coupon_load_image"+doc_id).show();
        $(".wecare-generateCoupon").html("");
         $.ajax({
                type:"post",
                url:'ajax/send_sms_doc.php',
                data:{'id':doc_id},
                success:function(resp){     
                     $("#coupon_form"+doc_id).show();
                     $("#coupon_form"+doc_id).html(resp);
                      $("#generate_coupon_load_image"+doc_id).hide();
                     
                        } 
                });
}

function sendDoctorListSMS1(){  
		var t=trim($("#firstName").val());
		var n=trim($("#mobileNo").val());
		var doctorId=trim($("#doctorId").val());
		if(t==""||t=="Your Name *"){
			alert("Please enter your name");
			return false;
		}
		if(n==""||n=="Phone No. *"){
			alert("Please enter your Phone");
			return false;
		}else if(!isCharsInBag1(n, "0123456789")) {
				alert( "Mobile must only contain Numbers");					
				return false;
			}
                         if(n.length<10)
                    {
                        alert( "Mobile Number Not valid.");
                        return false;
                    }
		$("#hospitallistsms1").html('<img src="images/loading.gif" class="fl">');
		$.ajax({ 
				type: "POST", 
				url:"ajax/doctor_sms.php",
				data:"firstname="+t+"&mobilephone="+n+"&doctorId="+doctorId,
				success: function(msg){
				 
				alert("Doctor details has been sent to your Mobile.");
                                  $("#coupon_form"+doctorId).html(""); 
				 
				}
				});
}
function isCharsInBag1(s, bag)
{
    var i;
    for (i = 0; i < s.length; i++)
    {
        var c = s.charAt(i);
        if (bag.indexOf(c) == -1)
            return false;
    }
    return true;
}
 
function network_doctor_email(docid) {
    //var hospitalList = getInboxSelectedValue('hospital');
     $(".wecare-generateCoupon").hide();
     $("#generate_coupon_load_image"+docid).show();
      $(".wecare-generateCoupon").html("");
        $.ajax({
                type:"post",
                url:'ajax/send_email_doc.php', 
                data:"doctorid="+docid,
                success:function(resp){     
                     $("#coupon_form"+docid).show();
                      $("#coupon_form"+docid).html(resp);
                       $("#generate_coupon_load_image"+docid).hide();
                        } 
                });
      
        
        
    

}
 
 
 
 
 function sendDoctorListEmail(doctorid){
		var e=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var t=trim($("#yourname").val());
		var n=trim($("#emailId").val());
		if(t==""||t=="Your Name *"){
			alert("Please enter your name");
			return false;
		}
		if(n==""||n=="Email ID *"){
			alert("Please enter your email address");
			return false;
			}else{
				if(e.test(n)==false){alert("Please enter your email address in correct format");return false}
			}
	 
	$("#hospitallistemail").html('<img src="images/loading.gif" class="fl">');
	$.ajax({ 
				type: "POST", 
				url:"ajax/doctor_email.php",
				data:"firstname="+t+"&emailaddress="+n+"&doctorid="+doctorid,
				success: function(msg){ 
                                        alert("Doctor details have been sent to your email ID.");
                                        $("#coupon_form"+doctorid).html(""); 
				}
				});
}
 
</script>