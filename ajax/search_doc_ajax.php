<?php

include_once("../conf/conf.php");           //include configuration file  
include_once("../conf/common_functions.php");          // include function file        // include function file


$search_keyword = $_REQUEST['term'];
$location_id = $_REQUEST['city_id'];
$location_info = explode("|", $location_id);
$location_info_id = $location_info[0];
$location_info_type = @$location_info[1];
$search_array = array();
$city_cond = "";

/* Find services  Session policy */
include("fetch_retail_or_corp_service.php");

$ret_corp_cond = "";
if ($_SESSION["LOGINTYPE"] == "RETAIL") {
    $ret_corp_cond = " AND LWDOCTORNEWMASTER.STATUS='ACTIVE' AND LWDOCTORNEWMASTER.AF_RETAIL='YES' ";
    $ret_corp_cond_d = " AND STATUS='ACTIVE' AND AF_RETAIL='YES' ";
} else {
    $ret_corp_cond = " AND LWDOCTORNEWMASTER.STATUS='ACTIVE' AND LWDOCTORNEWMASTER.AF_CORPORATEPORTAL='YES' ";
    $ret_corp_cond_d = " AND STATUS='ACTIVE' AND AF_CORPORATEPORTAL='YES' ";
}
//echo "SE".$service_of_policy;exit;
//$service_of_policy="8";
//echo $_SESSION["policyNumber"]."/";echo "<pre>";
//print_r($_SESSION);
//echo $service_of_policy;exit;
/* ---------------------------------------------------START: Fetch HOSPITAL  ------------------------------------------------------------------------------------------------ */
if ($location_info_type == "CITY") {
    $hospital_search = FetchJoinResultINNER("DISTINCT LWCENTERMASTER.CENTERID,LWCENTERMASTER.CENTERNAME,LWCENTERMASTER.CITYID", "LWDOCTORNEWMASTER.CENTERID", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where LWCENTERMASTER.STATUS='ACTIVE' AND SERVICEID IN(" . $service_of_policy . ") AND LWCENTERMASTER.CITYID=" . $location_info_id . "   AND Lower(LWCENTERMASTER.CENTERNAME) LIKE '%" . strtolower($search_keyword) . "%'" . $ret_corp_cond);
} else if ($location_info_type == "STATE") {
    $hospital_search = FetchJoinResultINNER("DISTINCT LWCENTERMASTER.CENTERID,LWCENTERMASTER.CENTERNAME,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.CENTERID", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where LWCENTERMASTER.STATUS='ACTIVE' AND SERVICEID IN(" . $service_of_policy . ") AND  LWCENTERMASTER.STATEID=" . $location_info_id . "   AND Lower(LWCENTERMASTER.CENTERNAME) LIKE '%" . strtolower($search_keyword) . "%'" . $ret_corp_cond);
} else {
    $hospital_search = FetchJoinResultINNER("DISTINCT LWCENTERMASTER.CENTERID,LWCENTERMASTER.CENTERNAME,LWCENTERMASTER.CITYID", "LWDOCTORNEWMASTER.CENTERID", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where LWCENTERMASTER.STATUS='ACTIVE' AND SERVICEID IN(" . $service_of_policy . ") AND  Lower(LWCENTERMASTER.CENTERNAME) LIKE '%" . strtolower($search_keyword) . "%'" . $ret_corp_cond);
}

if (!empty($hospital_search)) {
    /**
     * @author Sanjeev Kumar
     * Code added for fetch submodule mapping with center.
     * 
     */
    $query_submodule="select * from CRMSUBMODULEMASTER where upper(SUBMODULENAME) LIKE '%DISCOUNT CONNECT%' and STATUS='ACTIVE'";                                       //Fetch Submodule id based on submodule name
    $sql_submodule=oci_parse($conn,$query_submodule);
    oci_execute($sql_submodule);
    $submodule=oci_fetch_assoc($sql_submodule);
    $submodule_id='';
    if(!empty($submodule))
    {
        $submodule_id=$submodule['ID'];
    }
    foreach ($hospital_search as $info) {
        if(!empty($submodule_id)){
            $query="select * from LWCENTERSUBMODULEMAPPING where centerid='".$info["CENTERID"]."' and submoduleid='".$submodule_id."' AND STATUS='ACTIVE'";
            $sql=oci_parse($conn,$query);
            oci_execute($sql);
            $submodule_mapping_data='';
            $submodule_mapping_data=oci_fetch_assoc($sql);
            if(!empty($submodule_mapping_data)){
                $search_array[] = array("id" => $info['CENTERID'] . "|HOSPITAL", "value" => html_entity_decode($info['CENTERNAME']) . " [CENTER]");
            }
        }
    }
}

/* ---------------------------------------------------END: Fetch HOSPITAL  ------------------------------------------------------------------------------------------------ */


/* ---------------------------------------------------START: Fetch DOCTOR  ------------------------------------------------------------------------------------------------ */


if ($location_info_type == "CITY") {
    $doctor_search = FetchJoinResultINNER("LWDOCTORNEWMASTER.DOCTORID,LWDOCTORNEWMASTER.DOCTORNAME,LWDOCTORNEWMASTER.CENTERID", "LWCENTERMASTER.CENTERID,LWCENTERMASTER.CITYID", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where LWCENTERMASTER.STATUS='ACTIVE' AND  SERVICEID IN(" . $service_of_policy . ") AND  LWCENTERMASTER.CITYID=" . $location_info_id . "   AND Lower(LWDOCTORNEWMASTER.DOCTORNAME) LIKE '%" . strtolower($search_keyword) . "%'" . $ret_corp_cond);
} else if ($location_info_type == "STATE") {
    $doctor_search = FetchJoinResultINNER("LWDOCTORNEWMASTER.DOCTORID,LWDOCTORNEWMASTER.DOCTORNAME,LWDOCTORNEWMASTER.CENTERID", "LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where LWCENTERMASTER.STATUS='ACTIVE' AND SERVICEID IN(" . $service_of_policy . ") AND LWCENTERMASTER.STATEID=" . $location_info_id . "   AND Lower(LWDOCTORNEWMASTER.DOCTORNAME) LIKE '%" . strtolower($search_keyword) . "%'" . $ret_corp_cond);
} else {
    $doctor_search = fetchListCondsWithColumn("DOCTORID,DOCTORNAME", "LWDOCTORNEWMASTER", " where SERVICEID IN(" . $service_of_policy . ") AND Lower(DOCTORNAME) LIKE '%" . strtolower($search_keyword) . "%'" . $ret_corp_cond_d); //function to fetch doctor records
}

if (!empty($doctor_search)) {
    foreach ($doctor_search as $info) {
        $search_array[] = array("id" => $info['DOCTORID'] . "|DOCTOR", "value" => $info['DOCTORNAME'] . " [DOCTOR]");
    }
}
/* ---------------------------------------------------END: Fetch DOCTOR  ------------------------------------------------------------------------------------------------ */



/* ---------------------------------------------------START: Fetch SERVICE  ------------------------------------------------------------------------------------------------ */

//$service_search = fetchListCondsWithColumn("SERVICEID,NAME,SHOWCENTERORDOCTOR", "LWSERVICEMASTER", " where SERVICEID IN(" . $service_of_policy . ") AND  Lower(NAME) LIKE '%" . strtolower($search_keyword) . "%'"); //function to fetch service records
////  echo "<pre>"; print_r($service_of_policy);exit;
$query = "SELECT srvc_name NAME,max(SERVICEID) SERVICEID,cntrdoc_name SHOWCENTERORDOCTOR FROM (
SELECT (lower(NAME)) srvc_name, SERVICEID,upper((SHOWCENTERORDOCTOR)) cntrdoc_name FROM LWSERVICEMASTER where STATUS='ACTIVE' AND Lower(NAME) LIKE '%" . strtolower($search_keyword) . "%') group by srvc_name,cntrdoc_name";
$service_search = array();
if ($sql = oci_parse($conn, $query)) {
    if (oci_execute($sql)) {
        $j = 0;
        while ((@$row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $service_search[$j][$key] = $value;
            }
            $j++;
        }
    } else {
        exit;
    }
} else {
    exit;
}

$service_searchs = array();
if ($location_info_type == "CITY") {
    foreach ($service_search as $service) {
        if ($service["SHOWCENTERORDOCTOR"] == "SHOWDOCTOR") {
            $hospital_per_services = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.CITYID", "LWDOCTORNEWMASTER.CENTERID", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where LWCENTERMASTER.STATUS='ACTIVE' AND  LWCENTERMASTER.CITYID=" . $location_info_id . "   AND LWDOCTORNEWMASTER.SERVICEID='" . $service['SERVICEID'] . "' " . $ret_corp_cond);
            if (!empty($hospital_per_services)) {
                $service_searchs[] = $service;
            }
        } else {

            $hospital_per_services = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.CITYID", "LWSUBMODSERVICEMAPPING.CENTERID", "LWSUBMODSERVICEMAPPING", "LWCENTERMASTER", "LWSUBMODSERVICEMAPPING.CENTERID=LWCENTERMASTER.CENTERID", " where LWCENTERMASTER.STATUS='ACTIVE' AND LWSUBMODSERVICEMAPPING.STATUS='ACTIVE' AND  LWCENTERMASTER.CITYID=" . $location_info_id . "   AND LWSUBMODSERVICEMAPPING.SERVICEID='" . $service['SERVICEID'] . "' ");
            if (!empty($hospital_per_services)) {
                $service_searchs[] = $service;
            }
        }
    }
} else if ($location_info_type == "STATE") {
    foreach ($service_search as $service) {
        if ($service["SHOWCENTERORDOCTOR"] == "SHOWDOCTOR") {
            $hospital_per_services = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.CENTERID", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where LWCENTERMASTER.STATUS='ACTIVE' AND  LWCENTERMASTER.STATEID=" . $location_info_id . "   AND LWDOCTORNEWMASTER.SERVICEID='" . $service['SERVICEID'] . "' " . $ret_corp_cond);
            if (!empty($hospital_per_services)) {
                $service_searchs[] = $service;
            }
        } else {
            $hospital_per_services = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.CITYID", "LWSUBMODSERVICEMAPPING.CENTERID", "LWSUBMODSERVICEMAPPING", "LWCENTERMASTER", "LWSUBMODSERVICEMAPPING.CENTERID=LWCENTERMASTER.CENTERID", " where LWCENTERMASTER.STATUS='ACTIVE' AND LWSUBMODSERVICEMAPPING.STATUS='ACTIVE' AND LWCENTERMASTER.STATEID=" . $location_info_id . "   AND LWSUBMODSERVICEMAPPING.SERVICEID='" . $service['SERVICEID'] . "' ");
            if (!empty($hospital_per_services)) {
                $service_searchs[] = $service;
            }
        }
    }
} else {
    foreach ($service_search as $service) {
        if ($service["SHOWCENTERORDOCTOR"] == "SHOWDOCTOR") {
            $hospital_per_services = fetchListCondsWithColumn("CENTERID", "LWDOCTORNEWMASTER", " where SERVICEID='" . $service['SERVICEID'] . "'" . $ret_corp_cond_d);
            if (!empty($hospital_per_services)) {
                $service_searchs[] = $service;
            }
        } else {
            $hospital_per_services = fetchListCondsWithColumn("CENTERID", "LWSUBMODSERVICEMAPPING ", " where LWSUBMODSERVICEMAPPING.STATUS='ACTIVE' AND SERVICEID='" . $service['SERVICEID'] . "'");
            if (!empty($hospital_per_services)) {
                $service_searchs[] = $service;
            }
        }
    }
}


if (!empty($service_searchs)) {
    foreach ($service_searchs as $info) {
        $search_array[] = array("id" => $info['SERVICEID'] . "|SERVICE", "value" => $info['NAME'] . " [SERVICE]");
    }
}
/* ---------------------------------------------------END: Fetch SERVICE  ------------------------------------------------------------------------------------------------ */




/* ---------------------------------------------------START: Fetch SPECIALTY  ------------------------------------------------------------------------------------------------ */

//                                    $specialty_search = fetchListCondsWithColumn("DISTINCT SPECIALTYID,SPECIALTYNAME", "LWSPECIALTYMASTER", " where Lower(SPECIALTYNAME) LIKE '%" . strtolower(trim($search_keyword)) . "%'"); //function to fetch ailment records
$query = "SELECT spl_name SPECIALTYNAME,max(SPECIALTYID) SPECIALTYID FROM (
SELECT (lower(SPECIALTYNAME)) spl_name, SPECIALTYID FROM LWSPECIALTYMASTER where STATUS='ACTIVE' AND Lower(SPECIALTYNAME) LIKE '%" . strtolower($search_keyword) . "%') group by spl_name";
$specialty_search = array();
if ($sql = oci_parse($conn, $query)) {
    if (oci_execute($sql)) {
        $j = 0;
        while ((@$row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $specialty_search[$j][$key] = $value;
            }
            $j++;
        }
    } else {
        exit;
    }
} else {
    exit;
}

$specialty_searchs = array();
if ($location_info_type == "CITY") {
    foreach ($specialty_search as $spec) {
        $doc_per_services = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.CITYID", "LWDOCTORNEWMASTER.CENTERID,LWDOCTORNEWMASTER.DOCTORID,INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec['SPECIALTYID'] . "',1,1) AS ISSPECIALTY", "LWCENTERMASTER", "LWDOCTORNEWMASTER", " LWCENTERMASTER.CENTERID =LWDOCTORNEWMASTER.CENTERID", " Where LWCENTERMASTER.STATUS='ACTIVE' AND INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec['SPECIALTYID'] . "',1,1)>0 AND  LWCENTERMASTER.CITYID=" . $location_info_id . $ret_corp_cond);
        if (!empty($doc_per_services)) {
            $specialty_searchs[] = $spec;
        }
    }
} else if ($location_info_type == "STATE") {
    foreach ($specialty_search as $spec) {
        $doc_per_services = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.CENTERID,LWDOCTORNEWMASTER.DOCTORID,INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec['SPECIALTYID'] . "',1,1) AS ISSPECIALTY", "LWCENTERMASTER", "LWDOCTORNEWMASTER", " LWCENTERMASTER.CENTERID =LWDOCTORNEWMASTER.CENTERID", " Where LWCENTERMASTER.STATUS='ACTIVE' AND INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec['SPECIALTYID'] . "',1,1)>0 AND  LWCENTERMASTER.STATEID=" . $location_info_id . $ret_corp_cond);
        if (!empty($doc_per_services)) {
            $specialty_searchs[] = $spec;
        }
    }
} else {
    foreach ($specialty_search as $spec) {
        $doc_per_services = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.CENTERID,LWDOCTORNEWMASTER.DOCTORID,INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec['SPECIALTYID'] . "',1,1) AS ISSPECIALTY", "LWCENTERMASTER", "LWDOCTORNEWMASTER", " LWCENTERMASTER.CENTERID =LWDOCTORNEWMASTER.CENTERID", " Where LWCENTERMASTER.STATUS='ACTIVE' AND INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec['SPECIALTYID'] . "',1,1)>0 " . $ret_corp_cond);
        if (!empty($doc_per_services)) {
            $specialty_searchs[] = $spec;
        }
    }
}

if (!empty($specialty_searchs)) {
    foreach ($specialty_searchs as $info) {
        $search_array[] = array("id" => $info['SPECIALTYID'] . "|SPECIALTY", "value" => $info['SPECIALTYNAME'] . " [SPECIALTY]");
    }
}
/* ---------------------------------------------------END: Fetch SPECIALTY  ------------------------------------------------------------------------------------------------ */



/* ---------------------------------------------------START: Fetch AILMENT  ------------------------------------------------------------------------------------------------ */


$spec_in_ailment_search = FetchJoinResultINNER("LWAILMENTMASTER.AILMENTID,LWAILMENTMASTER.AILMENTNAME", " LWSPECIALTY_AILMENTMASTER.AILMENTID, LWSPECIALTY_AILMENTMASTER.SPECAILTYID  ", "LWAILMENTMASTER", "LWSPECIALTY_AILMENTMASTER", "LWAILMENTMASTER.AILMENTID=LWSPECIALTY_AILMENTMASTER.AILMENTID", " where LWAILMENTMASTER.STATUS='ACTIVE' AND Lower(LWAILMENTMASTER.AILMENTNAME) LIKE '%" . strtolower($search_keyword) . "%' "); //function to fetch ailment records
$specialty_search = array();
$duplicate = array();
foreach ($spec_in_ailment_search as $spec_in_ailment) {
    if (in_array($spec_in_ailment['SPECAILTYID'], $duplicate)) {
        continue;
    }
    $specialty_search[] = $spec_in_ailment;
    $duplicate[] = $spec_in_ailment['SPECAILTYID'];
}
$ailment_search = array();
if ($location_info_type == "CITY") {
    foreach ($specialty_search as $spec) {
        $doc_per_services = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.CITYID", "LWDOCTORNEWMASTER.CENTERID,LWDOCTORNEWMASTER.DOCTORID,INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec['SPECAILTYID'] . "',1,1) AS ISSPECIALTY", "LWCENTERMASTER", "LWDOCTORNEWMASTER", " LWCENTERMASTER.CENTERID =LWDOCTORNEWMASTER.CENTERID", " Where LWCENTERMASTER.STATUS='ACTIVE' AND INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec['SPECAILTYID'] . "',1,1)>0 AND  LWCENTERMASTER.CITYID=" . $location_info_id . $ret_corp_cond);
        if (!empty($doc_per_services)) {
            $ailment_search[] = $spec;
        }
    }
} else if ($location_info_type == "STATE") {
    foreach ($specialty_search as $spec) {
        $doc_per_services = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.CENTERID,LWDOCTORNEWMASTER.DOCTORID,INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec['SPECAILTYID'] . "',1,1) AS ISSPECIALTY", "LWCENTERMASTER", "LWDOCTORNEWMASTER", " LWCENTERMASTER.CENTERID =LWDOCTORNEWMASTER.CENTERID", " Where LWCENTERMASTER.STATUS='ACTIVE' AND INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec['SPECAILTYID'] . "',1,1)>0 AND  LWCENTERMASTER.STATEID=" . $location_info_id . $ret_corp_cond);
        if (!empty($doc_per_services)) {
            $ailment_search[] = $spec;
        }
    }
} else {
    foreach ($specialty_search as $spec) {
        $doc_per_services = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.CENTERID,LWDOCTORNEWMASTER.DOCTORID,INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec['SPECAILTYID'] . "',1,1) AS ISSPECIALTY", "LWCENTERMASTER", "LWDOCTORNEWMASTER", " LWCENTERMASTER.CENTERID =LWDOCTORNEWMASTER.CENTERID", " Where LWCENTERMASTER.STATUS='ACTIVE' AND INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec['SPECAILTYID'] . "',1,1)>0 " . $ret_corp_cond);
        if (!empty($doc_per_services)) {
            $ailment_search[] = $spec;
        }
    }
}
$ailment_searchs = array();
$duplicate_ailment = array();
foreach ($ailment_search as $ailment_info) {
    if (in_array($ailment_info['AILMENTID'], $duplicate_ailment)) {
        continue;
    }
    $ailment_searchs[] = $ailment_info;
    $duplicate_ailment[] = $ailment_info['AILMENTID'];
}

if (!empty($ailment_searchs)) {
    foreach ($ailment_searchs as $info) {
        $search_array[] = array("id" => $info['AILMENTID'] . "|AILMENT", "value" => $info['AILMENTNAME'] . " [AILMENT]");
    }
}

/* ---------------------------------------------------END: Fetch AILMENT  ------------------------------------------------------------------------------------------------ */
//$search_array = htmlspecialchars_decode($search_array);
$k = 0;
while (count($search_array) > $k) {
    $search_array[$k]['value'] = htmlspecialchars_decode($search_array[$k]['value']);
    $k++;
}
echo json_encode($search_array);
exit;

function FetchJoinResultINNER3($firstcolumn, $secondcolumn, $table1, $table2, $cond1, $where) {
    global $conn;
    $dataArray = array();
    if ($table1 != '') {

        $query = "select $firstcolumn,$secondcolumn  from $table1 INNER JOIN $table2 ON  $cond1  $where";
        $sql = @oci_parse($conn, $query);
        // Execute Query
        @oci_execute($sql);
        $i = 0;
        while (($row = oci_fetch_assoc($sql))) {
            foreach ($row as $key => $value) {
                $dataArray[$i][$key] = $value;
            }
            $i++;
        }
    }
    return $dataArray;
}

?>
