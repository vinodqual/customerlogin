<?php
include_once("../conf/conf.php");
include("../conf/session.php");  
if (isset($_SESSION['response_error'])) {
	$responseError  = $_SESSION['response_error'];
} else {
	$responseArray = $_SESSION['dataArray'];
}
$j=0;
foreach ($responseArray as $key => $response) {
	if ($key == 0) {
		if ($response['BGEN-MEMNAME']['#text']!=" ") {
			$livewell_users_arr[$j]["policyHolderName"] = $response['BGEN-MEMNAME']['#text'];
			$livewell_users_arr[$j]["policyHolderAge"] = $response['BGEN-AGE']['#text'];
			$livewell_users_arr[$j]["policyHolderMember"] = $relationArray[trim($response['BGEN-DPNTTYP']['#text'])];
			$livewell_users_arr[$j]["GENDER"] = $response['BGEN-GENDER']['#text'];
			$j++;
		}
	} else {
      if (trim($response['BGEN-MEMNAME']['#text'])!="") {
		   $livewell_users_arr[$j]["policyHolderName"] = $response['BGEN-MEMNAME']['#text'];
		   $livewell_users_arr[$j]["policyHolderAge"] = $response['BGEN-AGE']['#text'];
		   $livewell_users_arr[$j]["policyHolderMember"] = $relationArray[trim($response['BGEN-DPNTTYP']['#text'])];
		   $livewell_users_arr[$j]["GENDER"] = $response['BGEN-GENDER']['#text'];
		   $j++;
       }
	}   
}
//echo "<pre>";
//print_r($livewell_users_arr);

?>
<div class="topBarCode">Generate Coupon <span>(Select Member to Generate Coupon)</span></div>
 
<form action="ajax/mail_coupon.php" id="mail_coupon" method="POST">
<div class="generatecodePlan" style="overflow:auto;" >  
     <div class="error_message" > <div id="error_message"  ><?php echo "&nbsp;&nbsp;&nbsp;"; ?></div><div id="success_message"  ><?php echo "&nbsp;&nbsp;&nbsp;"; ?></div></div>
                <table   width="100%">
                            <tr>
                                <th>Select</th>
                                <th>Gender</th>
                                <th>Relation</th>
                                <th>Name</th>
                                <th>Age</th>
                            </tr>
                             <?php $livewell_counter=0; foreach($livewell_users_arr as $live_well_info) { ?>
                            <tr>
                                
                                <td> <input type="radio" name="livewell_user" value="<?php echo urlencode(serialize($live_well_info)); ?>" <?php if($livewell_counter==0){ echo "checked"; }?> /></td>
                                <td> <?php echo $live_well_info["GENDER"];?></td>
                                <td> <?php echo $live_well_info["policyHolderMember"];?></td>
                                <td><strong class="greenGeneralTxt"><?php echo $live_well_info["policyHolderName"];?></strong></td>
                                <td><?php echo $live_well_info["policyHolderAge"];?></td>
                            </tr>
                             <?php $livewell_counter++; } ?> 
            </table> 
  </div> 
	<div class="couponDetailtxt">Coupon / Details will be sent on- </div>
	<div class="generatecodePlan">
	<input name="center_id" type="hidden" class="feedbackratinginput2" value="<?php echo $_REQUEST['id']; ?>"  /> 
	<input name="service_id" type="hidden"   value="<?php echo $_REQUEST['service_id']; ?>"  /> 
	<div style="float:left;padding-top:9px">Email :<br/></div>
	<input type="text" value="<?php echo (isset($_SESSION["employeeEmail"]) && $_SESSION["employeeEmail"]!='')?$_SESSION["employeeEmail"]:$_SESSION["userName"]; ?>" id="coupon_email_id"      class="coupontextfield"   name="email_id" placeholder="Email">
	<div style="float:left;padding-top:9px">Mobile No :<br/></div>
	<input type="text" value="<?php echo @$_SESSION["emp_mobile"]; ?>" id="coupon_mobile_number" onkeypress='return kp_numeric(event)' class="coupontextfield"   name="mobile_number" placeholder="Mobile Num">
	<div class="right"><a    onclick="submit_mail_coupon(<?php echo $_REQUEST['id']; ?>)" class="generate-btn left" style="cursor: pointer;"  >Generate Coupon</a></div>
	</div>
	<div align="center" ><img id="mail_coupon_load_image<?php echo $_REQUEST['id'];?>" src="images/loading.gif" style="display:none;"></div>
	<div class="clearfix"></div>
        
 </form>



<script> 
    
    
function submit_mail_coupon(id)
    {
            $("#mail_coupon_load_image"+id).show(); 
            coupon_mobile_number=$("#coupon_mobile_number").val();
            coupon_email_id=$("#coupon_email_id").val();
            if(coupon_email_id=="")
              {
                          $("#error_message").show();
                          $("#error_message").html("<span style='color:red;' >Please enter your  Email Id.</span>");
                          $("#mail_coupon_load_image"+id).hide();
                          return false;
              } 
             if(coupon_email_id !='')
                {
                    if(!validateEmail(coupon_email_id))
                    {
                          $("#error_message").show();
                          $("#error_message").html("<span style='color:red;' >Please enter a valid  Email Id.</span>");
                          $("#mail_coupon_load_image"+id).hide();
                         return false;	
                    }
                }
           if(coupon_mobile_number=="")
              {
                 
                          $("#error_message").show();
                          $("#error_message").html("<span style='color:red;' >Please Enter your Mobile Number.</span>");
                          $("#mail_coupon_load_image"+id).hide();
                         return false;
              }
        var r = confirm("Are you sure you want to generate coupon?");
        if (r == true) 
        {    
          $("#mail_coupon_load_image"+id).show(); 
          $.ajax({
          type:"post",
          url:'ajax/mail_doc_coupon.php',   
          data:$("#mail_coupon").serialize(),
          
            success:function(resp){ 
                          $("#mail_coupon_load_image"+id).hide();
                          $("#error_message").show();
                          document.getElementById("error_message").style.textAlign = "center";
                          $("#error_message").html("<span style='color:green;font-size:20px;'><b>Your Coupon has been sent.</b></span>");
                       //   $("#error_message").hide(5000);
        
 
                  }  
            }); 
        }
    }
    
    
    function validateEmail($email) {
			emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			  if( !emailReg.test( $email ) ) {
				return false;
			  } else {
				return true;
			  }
	}
        function kp_numeric(e) 
{
	if(window.event) {
		// for IE, e.keyCode or window.event.keyCode can be used
		keynum = e.keyCode; 
	}
	else if(e.which) {
		// netscape
		keynum = e.which; 
	}
	else {
		// no event, so pass through
		return true;
	}
     if ((keynum != 46) && (keynum != 8) && (keynum < 48 || keynum > 57))
         return false;
 
 }
</script>