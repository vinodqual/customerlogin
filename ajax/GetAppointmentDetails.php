<?php
/***
* COPYRIGHT
* Copyright 2016 Qualtech Consultants.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR Amit Choyal
*
* $Id: getCorporateDetail.php,v 1.0 2016/12/26 11:33:29 amit choyal Exp $
* $Author: amit choyal $
*
****************************************************************************/
include_once("../conf/conf.php");           //include configuration file 
include_once("../conf/common_functions.php");          // include function file
include_once __DIR__.'/../../religare1/projectconfig/function/function_general.php';
include_once __DIR__.'/../../religare1/projectconfig/appointment/classes/listMemberPlan.php';
include_once(__DIR__.'/../../religare1/projectconfig/appointment/classes/appointmentMails.php');
include_once __DIR__.'/../../religare1/projectconfig/appointment/function/function.php';
$type 	= req_param('type');
if ($type == "CHECKPOLICYEXIST") {	
	$subtype 	= req_param('subtype');
	$dataArray 	= array();
	$PlanList	= array();
	$success 	= 0;
	$message 	= '';
	$policyNumber 	= req_param('policyNumber');
	$customerId 	= req_param('customerId');
	$productId		= '';
	$productcode    = '';
	$productname 	= '';
    $policyDetails 	= GetPolicyDetailsByPolicyNumber($policyNumber,$subtype);
	if ($policyDetails['error']=='') {
		if ( $policyDetails['customerId'] !='' || $customerId!='') {
			$customerId = ($policyDetails['customerId']!='')?$policyDetails['customerId']:$customerId;
			if ($policyDetails['productId']!=''){
				$productcode = $policyDetails['productId'];
				dbSelect("select PRODUCTID,PRODUCTCODE,TITLE from CRMRETAILPRODUCT where PRODUCTCODE='".$policyDetails['productId']."'",$bproductid, $aproductid);
				if ($bproductid){
					$productId = $aproductid[0]['PRODUCTID'];
					$productname = $aproductid[0]['TITLE'];
				}
			}				
			$pmlist = new listMemberPlan();
			$planandmembersarray = $pmlist->getMemberDetail($policyNumber, $customerId, array(), $productcode);
			if (isset($planandmembersarray['ERROR']) || $planandmembersarray['ERROR']!='') {
				$policyDetails['error'] = '<li>'.$planandmembersarray['ERROR'].'<li>';
			} else {
				$PlanList = $planandmembersarray;
				$success = 1;
			}			
		} else {
			$message = 'Please Enter customerid.';
		}
	}
	
	///Need to Fixed it latter///
	$policyliststr = '';
	if ($_SESSION['portalaccess'] == 'FULLACCESS' && $success == 1) {
		$policylist = getBookedAppointmentdetailbypolicynumber($policyNumber);
		if (count($policylist)>0){
			$policyliststr = CreateHtmltableforBookedAppointmentList($policylist);
		}
	}
	////Copy and past///
	$response = array(
        'errors' 		=> (isset($policyDetails['error'])?$policyDetails['error']:''),
		'message' 		=> $message,
		'success' 		=> $success,
		'planlist' 		=> $PlanList,
		'policyid' 		=> $policyDetails['policyId'],
        'companyid' 	=> $policyDetails['companyId'], 
		'policyExpirydate' 	=> date('d-M-y', $policyDetails['policyExpirydate']),
		'policyStartdate' 	=> date('d-M-y', $policyDetails['policyStartdate']),
		'productId' 	=>  $productId,
		'productname' 	=>	$productname,
		'customerId' 	=>  $customerId,
		'productcode' 	=>	$productcode,
		'companyname' 	=>	$policyDetails['companyname'],
		'policylist' 	=>	$policyliststr,
    );
    echo json_encode($response); //return response 
}
if ($type == "GETPLANDETAILS") { 
    $error 		= '';
	$centerlist = '';
    $statelist  = '';
    $citylist   = '';
    $plan       = req_param('planid'); 
    $policyid   = req_param('policyid');
    $companyid  = req_param('companyid');
    $productid  = req_param('productid');
    if ($plan != '') {
        $plandetail = explode('||', $plan);
		$planid = $plandetail[0];
        $centerlist = getCenterstateCitydetails($planid, $policyid, $companyid, $productid);
		if ( count($centerlist)>0 ) {
		    $unique_state   = array_unique(array_column($centerlist, 'STATEID'));
			foreach($unique_state as $key=> $stateid){
			   $statelist[] = $centerlist[$key];
		    }
            $unique_city    = array_unique(array_column($centerlist, 'CITYID'));
            foreach($unique_city as $key=> $cityid){
               $citylist[] = $centerlist[$key];
            }
        } else {
			$error = 'No center mapped with selected plan.';
		}
    }
    $response = array(
		'error' => $error,
        'centerlist' => $centerlist,
		'statelist' => $statelist,
		'citylist' => $citylist,
    );
    echo json_encode($response); //return response 
}
if ($type == "GETAVAILABLEDATES") {
	$centerid = req_param('centerid');
	$response = GetAvailableSlotsForCenterOnSelectedDate($centerid);
    echo json_encode($response); //return response 
}
if ($type == 'SAVEAPPOINTMENTDATA'){
	
	$response = SaveAppointmentData($_POST);
	echo json_encode($response); //return response 
}

if ($type == 'RESCHEDULEAPPOINTMENTDATA'){
	$response = SaveAppointmentData($_POST);
	echo json_encode($response); //return response 
}
?>