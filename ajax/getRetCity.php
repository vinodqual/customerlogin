<?php include_once("../conf/conf.php");           //include configuration file 
include_once("../conf/common_functions.php");          // include function file

/*****************************************************************************
* COPYRIGHT
* Copyright 2013 Catabatic Automation Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: getCity.php,v 1.0 2013/04/03 12:30:17 ashish gupta Exp $
* $Author: ashish gupta $
*
****************************************************************************/
		//include header to show header on page 
@$_SESSION['sId']="";
$id=sanitize_data($_POST['id']);
$planId=sanitize_data($_POST['planId']);
@$_SESSION['sId']=$id;
$s='';
?>
 
      <select  id="cityId" name="city" onchange="getCityName(this.value);get_value(this.value);">
	   <option value="">Select City</option>
	  <?php 
	  if($_SESSION['LOGINTYPE']=='CORPORATE'){
		  $City_list=GetCityWhereCenterExist($planId,$id); 
	  } else {
		  $City_list=GetRetCityWhereCenterExist($planId,$id); 
	  }
	  if(count($City_list) > 0){
	  
	  $i=0;
	  while($i<count($City_list)) //loop to listing company orderby companyID
	{
			if(@$_REQUEST['id']==$City_list[$i]["CITYID"]){
			$s=stripslashes($City_list[$i]["CITYNAME"]);
			}
	?>
	<option value="<?php echo @$City_list[$i]['CITYID']; ?>" ><?php echo @$City_list[$i]['CITYNAME']; ?></option>
    <?php $i++; } 
	} else {
	?>
	<option value="">Other</option>
	<?php
	}
	?>      
	  </select>
	<span id="changeText"  class="customStyleSelectBox changed" style="display: block;"><span class="customStyleSelectBoxInner" style="width: 180px; display: block;"><?php echo $s?$s:'Select City';?></span></span>

	<script>
	function changeText() {
		
	var selected = $("#city option:selected").text();
	$("#changeText").html(selected);
	$("#changeText").attr('style','width:180px;');
	}	
</script>