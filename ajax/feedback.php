<?php 

include_once("../conf/conf.php");
include_once("../conf/common_functions.php");
include("../conf/session.php");
$id=$_POST['id']; 
$doctors  =fetchListCond("LWDOCTORNEWMASTER"," where DOCTORID=".$_POST['id']); 
 
$feedbacks=fetchListCond("LWDOCTORFEEDBACK"," where DOCTORID=".$_POST['id']." order by CREATEDON DESC");

$total_feedback=count($feedbacks);
$avg_rating=0;
if($total_feedback>0){
$tot_rate=0;
  foreach($feedbacks as $feedback){
  $tot_rate=$feedback["RATE"]+$tot_rate; 
  } 
 
 $avg_rating= number_format(abs($tot_rate/$total_feedback),1);
}

?>   


<script type="text/javascript">
    
        $(function () {  
            $('#feedback_rate_info<?php echo $_POST['id'];?>').barrating({ showSelectedRating:false }); 
        });
</script>
    
<style>
.rating-f .br-widget {  height: 24px;}

.rating-f .br-widget a {
    background: url('images/star.jpg');
    width: 24px;
    height: 24px;
    display: block;
    float: left;
} 
.rating-f .br-widget a:hover,
.rating-f .br-widget a.br-active,
.rating-f .br-widget a.br-selected {
    background-position: 0 24px;
}

.br-widget
{
    overflow: auto;width:120px;
}
    </style>
 
 <div class="topBarCode">Feedback </div>
 
 <form   method="POST" name="feedback_form" id="feedback_id" class="feedback_class" >
<div class="generatecodePlan"  style="overflow:auto;" >   
                <div class="error_message" > <div id="error_message"  ><?php echo "&nbsp;&nbsp;&nbsp;"; ?></div><div id="success_message"  ><?php echo "&nbsp;&nbsp;&nbsp;"; ?></div></div>
                <table   width="100%">
                          <tr>
            <td width="91" height="40">Select Ratings </td>
            <td width="368" height="40">
                <div class="input select rating-f">
                <select id="feedback_rate_info<?php echo $_POST['id'];?>" name="rate" >                   
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                </select>
                </div>
                <span style="font-size:16px;color:#5c9a1b;" >
                    <?php
            if($total_feedback>1){ $user_text="Users";}else{$user_text="User";}
            $star_text="";
            if($avg_rating>1){ $star_text="Stars";}
            else if($avg_rating==1){$star_text="Star";} 
            else {$star_text=="";}
            if($total_feedback>0){ echo $avg_rating." ".$star_text." from ".$total_feedback. " ".$user_text;} ?>
                </span>
            </td>
             
          </tr>
          <tr>
            <td height="40">Your Name </td>
            <td height="40">
                <input name="feddbacker_name" id="feddbacker_name" type="text" class="coupontextfield" value="<?php echo @$_SESSION["employeeName"]; ?>" placeholder="Name" />
            <input name="doctor_id"   type="hidden" value="<?php echo $doctors[0]["DOCTORID"];  ?>" class="feedbackratinginput" />
            <input name="hospital_id" type="hidden" value="<?php echo $doctors[0]["CENTERID"];  ?>" class="feedbackratinginput" />
            
             
            </td>
            
          </tr>
          <tr>
            <td height="40">Email Id </td>
            <td height="40"><input name="feddbacker_email" id="feddbacker_email" type="text" class="coupontextfield" value="<?php echo @$_SESSION["employeeEmail"]; ?>" placeholder="Email" /></td>
             
          </tr>
          <tr>
            <td height="90" valign="top" class="forpatdpadd">Feedback </td>
            <td height="100"><textarea name="feddbacker_text" id="feddbacker_text"  cols="" rows="" class="coupontextfield"></textarea></td>
             
          </tr>
          <tr><td></td>
              <td>  <div class="bottomNav" >
                <a    onclick="submit_feedback()" style="cursor: pointer;margin-bottom: 10px;" class="generate-btn left"   >Submit Feedback</a></div>
              </td>
              
          </tr>
            </table> 
  </div> 
 
         <div class="clearfix"></div>
        
 </form>
 
 <?php if(!empty($feedbacks)){?>
   <div class="couponDetailtxt">View Feedback/Rating<span>(<?php echo $doctors[0]["DOCTORNAME"];  ?> )</span></div>
                    
                    <div class="generatecodePlan" style="overflow:auto;">
<!--                        <table class="responsive" width="100%">-->
                             <table   width="100%">
                              <?php foreach($feedbacks as $feedback){ ?>
                            <tr>
                                <td>
                                    <div class="input select rating-f" >
                                     <select id="feedback_rate<?php echo $feedback["FEEDBACKID"]; ?>" name="rate"  >                   
                <option value="1" <?php if($feedback["RATE"]=="1"){ echo "selected";   } ?>>1</option>
                <option value="2" <?php if($feedback["RATE"]=="2"){ echo "selected"; } ?>>2</option>
                <option value="3" <?php if($feedback["RATE"]=="3"){  echo "selected";  } ?>>3</option>
                <option value="4" <?php if($feedback["RATE"]=="4"){  echo "selected"; } ?>>4</option>
                <option value="5" <?php if($feedback["RATE"]=="5"){  echo "selected";  } ?>>5</option>
                </select>
                                    </div>
                                </td>
                                <td>  <?php echo $feedback["FEEDBACK"];  ?> 
                                <script type="text/javascript">
                                    $(function () {
                                        $('#feedback_rate<?php echo $feedback["FEEDBACKID"]; ?>').barrating({ showSelectedRating:false,readonly:true }); 
                                    });
                                </script>
                                </td> 
                            </tr>
                             <?php } ?> 
                        </table>
                       
                    
                    </div>
     
 <?php } ?>
      <div class="clearfix"></div> 
                  
     
     <script>
function submit_feedback()
{
    
    
    var e=/^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		var t=trim($("#feddbacker_name").val());
		var n=trim($("#feddbacker_email").val());
                var feddbacker_text=trim($("#feddbacker_text").val());
                
		if(t==""){
			 $("#error_message").show();
                          $("#error_message").html("Please enter your name.");
                          $("#error_message").hide(5000);
                        $("#feddbacker_name").focus();
			return false;
		}
		if(n==""){
			 $("#error_message").show();
                          $("#error_message").html("Please enter your Email Address.");
                          $("#error_message").hide(5000);
                         $("#feddbacker_email").focus();
			return false;
			}else{
				if(e.test(n)==false){ 
                                    $("#error_message").show();
                                    $("#error_message").html("Please enter your email address in correct format.");
                                    $("#error_message").hide(5000);
                                 $("#feddbacker_email").focus();return false}
			}
    
    if(feddbacker_text=="")
        { 
            $("#error_message").show();
            $("#error_message").html("Please enter the feedback.");
            $("#error_message").hide(5000);
            $("#feddbacker_text").focus();
            return false;
        }
    
 var feedback_info=   $("#feedback_id").serialize();
 
      $.ajax({
                type:"post",
                url:'ajax/submit_feedback.php',
                data:feedback_info,
                success:function(resp){    
                    $("#error_message").show();
                    $("#error_message").html("<span style='color:green;' >You have successfully rated For <?php echo $doctors[0]["DOCTORNAME"];  ?>.</span>.");
                    $("#error_message").hide(5000);
                    
                   fetch_doctor(<?php echo $doctors[0]["DOCTORID"];  ?>);
                       
                        } 
                });
}
function fetch_doctor(docid)
{
                $.ajax({
                type:"post",
                url:'ajax/fetch_doctor.php',
                data:"doctorid="+docid,
                success:function(resp){  
                    $("#doctor_unique_id"+docid).html(resp);
                     } 
                });
}
</script>