<?php include_once("../../conf/session.php");	//including session file
include_once("../../conf/conf.php");		//including configuration file
include_once("../../conf/fucts.php");		//including function file
$Id=sanitize_data(@$_REQUEST['status_id']);	
$type=sanitize_data(@$_REQUEST['type']);	
$status=sanitize_data(@$_REQUEST['status']);	//requested type and status
$sql = "select CRMTOPUPPROPOSERDETAILS.*,CRMCORPORATE.CORPORATENAME,CRMCORPORATE.XOLPREVENTIVESTARTDATE,CRMCORPORATE.XOLPREVENTIVEENDDATE,CRMCORPORATE.XOLSUPERTOPUPSTARTDATE,CRMCORPORATE.XOLSUPERTOPUPENDDATE,CRMCORPORATE.XOLPARENTPOLICYSTARTDATE,CRMCORPORATE.XOLPARENTPOLICYENDDATE from CRMTOPUPPROPOSERDETAILS left join CRMCORPORATE on CRMCORPORATE.CORPORATEID=CRMTOPUPPROPOSERDETAILS.CORPORATEID WHERE CRMTOPUPPROPOSERDETAILS.PROPOSERID=".@$Id."";//query to count records
$result = @oci_parse($conn, $sql);
@oci_execute($result);
$row = oci_fetch_assoc($result);
$sql_servicetax = "select TAX from CRMCORPORATESERVICETAX where rownum=1";//query to count records
$result_servicetax = @oci_parse($conn, $sql_servicetax);
@oci_execute($result_servicetax);
$row_servicetax= oci_fetch_assoc($result_servicetax);
$corporateName=@$row['CORPORATENAME']?@$row['CORPORATENAME']:'NA';
$employeeName=@$row['EMPLOYEENAME']?@$row['EMPLOYEENAME']:'NA';
$address1=@$row['ADDRESSLINE1']?@$row['ADDRESSLINE1']:'NA';
$address2=@$row['ADDRESSLINE2']?@$row['ADDRESSLINE2']:'';
$buyingdate=@$row['BUYINGDATE']?@$row['BUYINGDATE']:'';
if($row['PRODUCTTYPE']=='PREVENTIVE'){
$policyperiod=@$row['XOLPREVENTIVESTARTDATE']."&nbsp;To&nbsp;".@$row['XOLPREVENTIVEENDDATE'];
}
if($row['PRODUCTTYPE']=='SUPERTOPUP'){
$policyperiod=@$row['XOLSUPERTOPUPSTARTDATE']."&nbsp;To&nbsp;".@$row['XOLSUPERTOPUPENDDATE'];
}
if($row['PRODUCTTYPE']=='PARENTPOLICY'){
$policyperiod=@$row['XOLPARENTPOLICYSTARTDATE']."&nbsp;To&nbsp;".@$row['XOLPARENTPOLICYENDDATE'];
}
$policyperiod1=@$row['EMPLOYEENAME'];
$policyperiod2=@$row['EMPLOYEENAME'];
$premimum=@$row['TOTALPREMIUM'];
$servicetaxes=number_format($row_servicetax['TAX'],2);
 $totalservicetax=($premimum*$servicetaxes)/100;
 $totalservicetax=number_format($totalservicetax,2);
 $totalpremimum=number_format($premimum+$totalservicetax,2);
$total=@$row['EMPLOYEENAME'];
$issuedate=@$row['EMPLOYEENAME'];

require_once('tcpdf_include.php');
// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
	$pdf->SetTitle('TCPDF Example 006');
$pdf->SetSubject('TCPDF Tutorial');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 006', PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

//set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

//set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

//set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

//set some language-dependent strings
$pdf->setLanguageArray($l);

$pdf->AddPage();

$html = <<<EOF
<style>
.acknowldge{
font-size:12px;
font-family:"Times New Roman", Times, serif;
}
</style>

<div>
<strong>Premium Acknowledgement</strong><br>
<div style="background-color:#D0E290;"><img src="images/tcpdf_logo.jpg" height="55" width="225"></div><div style="background-color:#D0E290;"></div>
</div>
<div>
<table width="627" border="1" cellpadding="0" cellspacing="0">
  <tr>
    <td width="184" bgcolor="#D0E290">&nbsp;Policy holder</td>
    <td width="437">&nbsp;$corporateName</td>
  </tr>
 
  <tr>
    <td bgcolor="#D0E290">&nbsp;Employee name</td>
    <td>&nbsp;$employeeName</td>
  </tr>
  
  <tr>
    <td bgcolor="#D0E290">&nbsp;Address</td>
    <td>&nbsp;$address1<br>
      &nbsp;$address2<br>
    </td>
  </tr>
 
  <tr>
    <td bgcolor="#D0E290">&nbsp;Policy Period </td>
    <td>&nbsp;$policyperiod</td>
  </tr>
  
</table>
</div>
<div style="background-color:#D0E290;color:black;"><strong>&nbsp;Premium Details</strong></div>
<div>
<table width="627" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td width="170"><strong>Particulars</strong></td>
	<td width="21">&nbsp;</td>
    <td width="430"><strong>Amount (in Rs.)</strong></td>
  </tr>
   <tr>
    <td width="170">&nbsp;</td>
	<td width="21">&nbsp;</td>
    <td width="430">&nbsp;</td>
  </tr>
  <tr>
    <td width="170">Premium</td>
	<td width="21">&nbsp;</td>
    <td width="430">$premimum</td>
  </tr>
   <tr>
    <td width="170">&nbsp;</td>
	<td width="21">&nbsp;</td>
    <td width="430">&nbsp;</td>
  </tr>
  <tr>
    <td width="170">Service Tax & Levies</td>
	<td width="21">&nbsp;</td>
    <td width="430">$totalservicetax</td>
  </tr>
   <tr>
    <td width="170">&nbsp;</td>
	<td width="21">&nbsp;</td>
    <td width="430">&nbsp;</td>
  </tr>
  <tr>
    <td width="170"><strong>Total</strong></td>
	<td width="21">&nbsp;</td>
    <td width="430"><strong>$totalpremimum</strong></td>
  </tr>
   <tr>
    <td width="170">&nbsp;</td>
	<td width="21">&nbsp;</td>
    <td width="430">&nbsp;</td>
  </tr>
    <tr>
    <td width="610" colspan="3">The Premium is rounded off to the nearest rupee</td>
  </tr>
   <tr>
    <td width="170">&nbsp;</td>
	<td width="21">&nbsp;</td>
    <td width="430">&nbsp;</td>
  </tr>

</table>

</div>
<div style="background-color:#D0E290;color:black;"><strong>&nbsp;Eligibility of Premium for Deduction u/s 80D of the Income Tax Act, 1961</strong></div>
<div style="color:black;">This is to certify that Religare Health Insurance Co.Ltd. has received an amount of Rs. $totalpremimum/- from $employeeName towards Payment of Health insurance premium as per the details mentioned above. The premium 
paid for this policy is eligible for applicable tax benefits u/s 80D of the Income Tax Act, 1961 and amendments 
thereof</div>
<div style="color:black;">For <strong>Religare Health Insurance Company Limited</strong></div>
<div class="acknowldge">
<table width="700" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td colspan="3"><img src="images/tcpdf_logo.jpg" width="50" height="50" /></td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td width="160">Authorized Signatory</td>
    <td width="234">Date of Issue: $buyingdate</td>
    <td width="234">Place of Issue: <strong>Saket, New Delhi</strong></td>
  </tr>
</table>
</div>
<div style="font-size:10px;font-weight:bold;"><strong>Note</strong>
<div style="font-weight:100;font-family:Arial, Helvetica, sans-serif;font-size:9px;">1) In case of any discrepancy, the Policyholder is requested to contact the Company immediately.<br/>
2) Any amount paid in cash towards the premium would not qualify for tax benefits as mentioned above</div>
</div>

EOF;

// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
//Close and output PDF document
$pdf->Output('../../template/corp_acknowledgement.pdf', 'F');
echo "success";
//============================================================+
// END OF FILE                                                
//============================================================+
