<?php
include_once("../../conf/session.php");	//including session file
include_once("../../conf/conf.php");		//including configuration file
include_once("../../conf/fucts.php");		//including function file
$type=sanitize_data(@$_REQUEST['type']);	
$Rec=sanitize_data(@$_REQUEST['Rec']);	
	$table = "CRMEMPLOYEEPURCHASEPLAN";
	$table1 = "CRMCOMPANY";
$compId=sanitize_data(@$_REQUEST['compId']);
	if($_SESSION["type"] == 'superadmin') {
		 $query_user="select CRMCOMPANY.COMPANYID from CRMCOMPANY where STATUS = 'ACTIVE'";
	} else if($_SESSION["type"] == 'relationshipmanager') { 
		 $adminids=getUserListUnderRM();
		 $query_user="select CRMCOMPANY.COMPANYID from CRMCOMPANY LEFT JOIN CRMUSERCMPALLOCATION  ON  CRMCOMPANY.COMPANYID=CRMUSERCMPALLOCATION.COMPANYID  where CRMUSERCMPALLOCATION.USERID IN ($adminids) and CRMCOMPANY.STATUS='ACTIVE'";
	} else {
		  $query_user="select CRMCOMPANY.COMPANYID from CRMCOMPANY LEFT JOIN CRMUSERCMPALLOCATION  ON  CRMCOMPANY.COMPANYID=CRMUSERCMPALLOCATION.COMPANYID  where CRMUSERCMPALLOCATION.USERID=(select ADMINID from CRMADMINUSER where USERNAME = '".@$_SESSION["userName"]."' and STATUS = 'ACTIVE') and CRMCOMPANY.STATUS='ACTIVE'";
	}
	 $query="SELECT * FROM CRMEMPLOYEEPURCHASEPLAN where COMPANYID IN ($query_user) and PAYMENTSTATUS='CONFIRMED' $cond  order by PURCHASEPLANID desc ";
if(isset($compId) && !empty($compId)){

	@$cond='';
	if(isset($Rec)&& !empty($Rec)) {
	@$cond .= " and PAYMENTRECEIVED = '$Rec' ";
	}

                   $sql = "select REFNO as REFERENCENO,EMPLOYEEID,EMPNAME as EMPLOYEENAME,EMPEMAILID as EMAILID,EMPMOBILE as MOBILE,PLANNAME,POLICYNUMBER,PAYMENTTYPE,PAYMENTSTATUS,PAYMENTRECEIVED,TOTALAMOUNT,(SELECT CRMCOMPANY.COMPANYNAME FROM CRMCOMPANY WHERE  CRMCOMPANY.COMPANYID=$table.COMPANYID) as COMPANYNAME  from $table WHERE $table.COMPANYID = $compId and $table.PAYMENTSTATUS='CONFIRMED' $cond order by $table.PURCHASEPLANID desc";		//query to count records
} else {
                   $sql = "select REFNO as REFERENCENO,EMPLOYEEID,EMPNAME as EMPLOYEENAME,EMPEMAILID as EMAILID,EMPMOBILE as MOBILE,PLANNAME,POLICYNUMBER,PAYMENTTYPE,PAYMENTSTATUS,PAYMENTRECEIVED,TOTALAMOUNT,(SELECT CRMCOMPANY.COMPANYNAME FROM CRMCOMPANY WHERE  CRMCOMPANY.COMPANYID=$table.COMPANYID) as COMPANYNAME  from $table WHERE $table.COMPANYID IN ($query_user) and $table.PAYMENTSTATUS='CONFIRMED' $cond order by $table.PURCHASEPLANID desc";//query to count records
}
                  // $fp = fopen($filename, "wb");
				   $result = @oci_parse($conn, $sql);
				   @oci_execute($result);

//============================================================+
// File name   : example_048.php
// Begin       : 2009-03-20
// Last Update : 2013-05-14
//
// Description : Example 048 for TCPDF class
//               HTML tables and table headers
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: HTML tables and table headers
 * @author Nicola Asuni
 * @since 2009-03-20
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('Individual Collection Report');
$pdf->SetSubject('Collection Report');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 10);

// add a page
$pdf->AddPage();

$pdf->Write(0, 'Total Collection report', '', 0, 'L', true, 1, false, false, 0);

$pdf->SetFont('helvetica', '', 10);

	for ($k =1; $k <= oci_num_fields($result); $k++){  
	 @$insert_rows = oci_field_name($result,$k);
	 @$heading_rows .= "<th align=\"center\">"."<b>".@$insert_rows."</b>"."</th>";
	}
	while($row = oci_fetch_assoc($result))
	   {
	   @$heading_columns .= "<tr><td>".@$row['REFERENCENO']."</td><td>".@$row['EMPLOYEEID']."</td>"."<td>".@$row['EMPLOYEENAME']."</td>"."<td>".@$row['EMAILID']."</td>"."<td>".@$row['MOBILE']."</td>"."<td>".@$row['PLANNAME']."</td>"."<td>".@$row['POLICYNUMBER']."</td>"."<td>".@$row['PAYMENTTYPE']."</td>"."<td>".@$row['PAYMENTSTATUS']."</td>"."<td>".@$row['PAYMENTRECEIVED']."</td><td>".@$row['TOTALAMOUNT']."</td>"."<td>".@$row['COMPANYNAME']."</td></tr>";
	   }
$tbl = <<<EOD
<table border="1">
					

<tr>
	
$heading_rows
</tr>
$heading_columns
</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

$pdf->Output('collectonReport.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
