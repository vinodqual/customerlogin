<?php
include_once("../../conf/session.php");	//including session file
include_once("../../conf/conf.php");		//including configuration file
include_once("../../conf/fucts.php");		//including function file
$type=sanitize_data(@$_REQUEST['type']);	
$RefNumber1=sanitize_data(@$_REQUEST['RefNumber1']);	
$EmployeeId1=sanitize_data(@$_REQUEST['EmployeeId1']);	
$ModeofPayment1=sanitize_data(@$_REQUEST['ModeofPayment1']);	
$Numbers1=sanitize_data(@$_REQUEST['Numbers1']);	
$EmailId1=sanitize_data(@$_REQUEST['EmailId1']);	
$planId=sanitize_data(@$_REQUEST['planId']);
if(isset($planId) && !empty($planId)) {
     $conds .="  and PLANID='".@$planId."' "; 
}

	$table = "CRMEMPLOYEEPURCHASEPLAN";
	$table1 = "CRMCOMPANY";
$compId=sanitize_data(@$_REQUEST['compId1']);
	if($_SESSION["type"] == 'superadmin') {
		 $query_user="select CRMCOMPANY.COMPANYID from CRMCOMPANY where STATUS = 'ACTIVE'";
	} else if($_SESSION["type"] == 'relationshipmanager') { 
		 $adminids=getUserListUnderRM();
		 $query_user="select CRMCOMPANY.COMPANYID from CRMCOMPANY LEFT JOIN CRMUSERCMPALLOCATION  ON  CRMCOMPANY.COMPANYID=CRMUSERCMPALLOCATION.COMPANYID  where CRMUSERCMPALLOCATION.USERID IN ($adminids) and CRMCOMPANY.STATUS='ACTIVE'";
	} else {
		  $query_user="select CRMCOMPANY.COMPANYID from CRMCOMPANY LEFT JOIN CRMUSERCMPALLOCATION  ON  CRMCOMPANY.COMPANYID=CRMUSERCMPALLOCATION.COMPANYID  where CRMUSERCMPALLOCATION.USERID=(select ADMINID from CRMADMINUSER where USERNAME = '".@$_SESSION["userName"]."' and STATUS = 'ACTIVE') and CRMCOMPANY.STATUS='ACTIVE'";
	}
	 $query="SELECT * FROM CRMEMPLOYEEPURCHASEPLAN where COMPANYID IN ($query_user) and PAYMENTSTATUS='CONFIRMED' $cond  order by PURCHASEPLANID desc ";
if(isset($compId) && !empty($compId)){

	@$cond='';
	if(isset($RefNumber1)&& !empty($RefNumber1)) {
	@$cond .= " and REFNO = '$RefNumber1' ";
	}
	if(isset($EmployeeId1)&& !empty($EmployeeId1)) {
	@$cond .= " and EMPLOYEEID = '$EmployeeId1'  ";
	}
	if(isset($ModeofPayment1)&& !empty($ModeofPayment1)) {
	@$cond .= "  and PAYMENTTYPE = '$ModeofPayment1' ";
	}
	if(isset($Numbers1)&& !empty($Numbers1)) {
	@$cond .= "  and EMPMOBILE = '$Numbers1' ";
	}
	if(isset($EmailId1)&& !empty($EmailId1)) {
	@$cond .= " and EMPEMAILID = '$EmailId1' ";
	}

                   $sql = "select REFNO as REFERENCENO,EMPLOYEEID,EMPNAME as EMPLOYEENAME,EMPEMAILID as EMAILID,EMPMOBILE as MOBILE,PLANNAME,POLICYNUMBER,PAYMENTTYPE,PAYMENTSTATUS,PAYMENTRECEIVED,TOTALAMOUNT,(SELECT CRMCOMPANY.COMPANYNAME FROM CRMCOMPANY WHERE  CRMCOMPANY.COMPANYID=$table.COMPANYID) as COMPANYNAME  from $table WHERE $table.COMPANYID IN ($query_user) and $table.PAYMENTSTATUS='CONFIRMED' $cond $conds order by $table.PURCHASEPLANID desc";
} else {
                   $sql = "select REFNO as REFERENCENO,EMPLOYEEID,EMPNAME as EMPLOYEENAME,EMPEMAILID as EMAILID,EMPMOBILE as MOBILE,PLANNAME,POLICYNUMBER,PAYMENTTYPE,PAYMENTSTATUS,PAYMENTRECEIVED,TOTALAMOUNT,(SELECT CRMCOMPANY.COMPANYNAME FROM CRMCOMPANY WHERE  CRMCOMPANY.COMPANYID=$table.COMPANYID) as COMPANYNAME  from $table WHERE $table.COMPANYID IN ($query_user) and $table.PAYMENTSTATUS='CONFIRMED' $cond $conds order by $table.PURCHASEPLANID desc";//query to count records
}
                  // $fp = fopen($filename, "wb");
				   $result = @oci_parse($conn, $sql);
				   @oci_execute($result);

require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);

$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('Collection Report');
$pdf->SetSubject('Collection Report');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}
// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica','B',13);

// add a page
$pdf->AddPage();

$pdf->Write(0, 'Total Collection report', '', 0, 'L', true, 1, false, false, 0);

$pdf->SetFont('helvetica', '', 10);

	for ($k =1; $k <= oci_num_fields($result); $k++){  
	 @$insert_rows = oci_field_name($result,$k);
	 @$heading_rows .= "<th align=\"center\">"."<b>".@$insert_rows."</b>"."</th>";
	}
	while($row = oci_fetch_assoc($result))
	   {
	   @$heading_columns .= "<tr><td>".@$row['REFERENCENO']."</td><td>".@$row['EMPLOYEEID']."</td>"."<td>".@$row['EMPLOYEENAME']."</td>"."<td>".@$row['EMAILID']."</td>"."<td>".@$row['MOBILE']."</td>"."<td>".@$row['PLANNAME']."</td>"."<td>".@$row['POLICYNUMBER']."</td>"."<td>".@$row['PAYMENTTYPE']."</td>"."<td>".@$row['PAYMENTSTATUS']."</td>"."<td>".@$row['PAYMENTRECEIVED']."</td>"."<td>".@$row['TOTALAMOUNT']."</td>"."<td>".@$row['COMPANYNAME']."</td></tr>";
	   }
	   $pdf->Ln(2);
$tbl = <<<EOD
<table border="1">
<tr>
	
$heading_rows
</tr>
$heading_columns
</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');
//Close and output PDF document
$pdf->Output('collectonReport.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
