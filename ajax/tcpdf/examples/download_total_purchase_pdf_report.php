<?php
include_once("../../conf/session.php");	//including session file
include_once("../../conf/conf.php");		//including configuration file
include_once("../../conf/fucts.php");		//including function file
$type=sanitize_data(@$_REQUEST['type']);	
$status=sanitize_data(@$_REQUEST['status']);	//requested type and status
	$table = "CRMEMPLOYEEPURCHASEPLAN";
	$table1 = "CRMCOMPANY";
$compId=sanitize_data(@$_REQUEST['compId']);

	if($_SESSION["type"] == 'superadmin') {
		 $query_user="select CRMCOMPANY.COMPANYID from CRMCOMPANY where STATUS = 'ACTIVE'";
	} else {
		  $query_user="select CRMCOMPANY.COMPANYID from CRMCOMPANY LEFT JOIN CRMUSERCMPALLOCATION  ON  CRMCOMPANY.COMPANYID=CRMUSERCMPALLOCATION.COMPANYID  where CRMUSERCMPALLOCATION.USERID=(select ADMINID from CRMADMINUSER where USERNAME = '".@$_SESSION["userName"]."' and STATUS = 'ACTIVE') and CRMCOMPANY.STATUS='ACTIVE'";
	}

if(isset($compId) && !empty($compId)){

		$payment=sanitize_data($_REQUEST['payment']);
				if(isset($payment) && !empty($payment)) {
		$cond1 =" and CRMEMPLOYEEPURCHASEPLAN.PAYMENTRECEIVED='$payment'";
		}
		 $cond =" and CRMEMPLOYEEPURCHASEPLAN.COMPANYID='$compId' $cond1";

		
		
                   $sql = "select (SELECT CRMCOMPANY.COMPANYNAME FROM CRMCOMPANY WHERE  CRMCOMPANY.COMPANYID=$table.COMPANYID) as COMPANYNAME,PLANNAME,count(*) as QUANTITY,sum(TOTALAMOUNT) as TOTALAMOUNT,PLANID,COMPANYID from $table WHERE $table.COMPANYID IN ($query_user) and $table.PAYMENTSTATUS='CONFIRMED' $cond GROUP BY $table.PLANID,$table.PLANNAME,$table.COMPANYID ";		//query to count records
} else {
                   $sql = "select (SELECT CRMCOMPANY.COMPANYNAME FROM CRMCOMPANY WHERE  CRMCOMPANY.COMPANYID=$table.COMPANYID) as COMPANYNAME,PLANNAME,count(*) as QUANTITY,sum(TOTALAMOUNT) as TOTALAMOUNT,PLANID,COMPANYID  from $table WHERE $table.COMPANYID IN ($query_user) and $table.PAYMENTSTATUS='CONFIRMED' GROUP BY $table.PLANID,$table.PLANNAME,$table.COMPANYID ";//query to count records
}
                  // $fp = fopen($filename, "wb");
				   $result = @oci_parse($conn, $sql);
				   @oci_execute($result);
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information 
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('Purchase Report');
$pdf->SetSubject('Purchase Report');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica','B',13);

// add a page
$pdf->AddPage();

$pdf->Write(0, 'Total Purchase Report','', 0, 'L', true, 1, false, false, 0);

$pdf->SetFont('helvetica', '', 10);
$heading_columns='';
	for ($k =1; $k <= 4; $k++){  
	 @$insert_rows = oci_field_name($result,$k);
	 @$heading_rows .= "<th align=\"center\">"."<b>".@$insert_rows."</b>"."</th>";
	}
	
	$i=0;
	while($row = oci_fetch_assoc($result))
	   {
	  $insert_columns_company = $row['COMPANYNAME'];
	  $insert_columns_planname = $row['PLANNAME'];
	  $insert_columns_quantity = $row['QUANTITY'];
	  $insert_columns_amount = $row['TOTALAMOUNT'];
	  @$heading_columns .= "<tr><td>".@$insert_columns_company."</td><td>".@$insert_columns_planname."</td><td align=\"center\">".@$insert_columns_quantity."</td><td align=\"right\"  >".@$insert_columns_amount."</td></tr>";
	   $i++; 
	    }
$pdf->Ln(2);	    
$tbl = <<<EOD

<table border="1" width="100%" >
<tr>
$heading_rows
</tr>
$heading_columns
</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

$pdf->Output('purchaseReport.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
