<?php
include_once("../../conf/session.php");	//including session file
include_once("../../conf/conf.php");		//including configuration file
include_once("../../conf/fucts.php");		//including function file
$type=sanitize_data(@$_REQUEST['type']);	
$status=sanitize_data(@$_REQUEST['status']);	//requested type and status
	$table = "CRMEMPLOYEEPURCHASEPLAN";
	$table1 = "CRMCOMPANY";
$compId=sanitize_data(@$_REQUEST['compId']);

	if($_SESSION["type"] == 'superadmin') {
		 $query_user="select CRMCOMPANY.COMPANYID from CRMCOMPANY where STATUS = 'ACTIVE'";
	} else {
		  $query_user="select CRMCOMPANY.COMPANYID from CRMCOMPANY LEFT JOIN CRMUSERCMPALLOCATION  ON  CRMCOMPANY.COMPANYID=CRMUSERCMPALLOCATION.COMPANYID  where CRMUSERCMPALLOCATION.USERID=(select ADMINID from CRMADMINUSER where USERNAME = '".@$_SESSION["userName"]."' and STATUS = 'ACTIVE') and CRMCOMPANY.STATUS='ACTIVE'";
	}

if(isset($compId) && !empty($compId)){

		$payment=sanitize_data($_REQUEST['payment']);
				if(isset($payment) && !empty($payment)) {
		$cond1 =" and CRMEMPLOYEEPURCHASEPLAN.PAYMENTRECEIVED='$payment'";
		}
		 $cond =" and CRMEMPLOYEEPURCHASEPLAN.COMPANYID='$compId' $cond1";

		
		
                   $sql = "select (SELECT CRMCOMPANY.COMPANYNAME FROM CRMCOMPANY WHERE  CRMCOMPANY.COMPANYID=$table.COMPANYID) as COMPANYNAME,sum(TOTALAMOUNT) as TOTALAMOUNT  from $table WHERE $table.COMPANYID IN ($query_user) and $table.PAYMENTSTATUS='CONFIRMED' $cond GROUP BY COMPANYID ";		//query to count records
} else {
                   $sql = "select (SELECT CRMCOMPANY.COMPANYNAME FROM CRMCOMPANY WHERE  CRMCOMPANY.COMPANYID=$table.COMPANYID) as COMPANYNAME,sum(TOTALAMOUNT) as TOTALAMOUNT  from $table WHERE $table.COMPANYID IN ($query_user) and $table.PAYMENTSTATUS='CONFIRMED' GROUP BY COMPANYID ";//query to count records
}
             
                  // $fp = fopen($filename, "wb");
				   $result = @oci_parse($conn, $sql);
				   @oci_execute($result);

//============================================================+
// File name   : example_048.php
// Begin       : 2009-03-20
// Last Update : 2013-05-14
//
// Description : Example 048 for TCPDF class
//               HTML tables and table headers
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: HTML tables and table headers
 * @author Nicola Asuni
 * @since 2009-03-20
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('Purchase Report');
$pdf->SetSubject('Purchase Report');
$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('helvetica', 'B', 10);

// add a page
$pdf->AddPage();

$pdf->Write(0, 'Total Purchase report', '', 0, 'L', true, 1, false, false, 0);

$pdf->SetFont('helvetica', '', 10);

	for ($k =1; $k <= oci_num_fields($result); $k++){  
	 @$insert_rows = oci_field_name($result,$k);
	 @$heading_rows .= "<th align=\"center\">"."<b>".@$insert_rows."</b>"."</th>";
	}
	while($row = oci_fetch_assoc($result))
	   {
	   $insert_columns_company = $row['COMPANYNAME'];
	   $insert_columns_amount = $row['TOTALAMOUNT'];
	   @$heading_columns .= "<tr><td>".@$insert_columns_company."</td><td>".@$insert_columns_amount."</td></tr>";
	   }
$tbl = <<<EOD
<table border="1">
					

<tr>
	
$heading_rows
</tr>
$heading_columns
</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

// -----------------------------------------------------------------------------

// Table with rowspans and THEAD
/*$tbl = <<<EOD
<table border="1" cellpadding="2" cellspacing="2">
<thead>
 <tr style="background-color:#FFFF00;color:#0000FF;">
  <td width="30" align="center"><b>A</b></td>
  <td width="140" align="center"><b>XXXX</b></td>
  <td width="140" align="center"><b>XXXX</b></td>
  <td width="80" align="center"> <b>XXXX</b></td>
  <td width="80" align="center"><b>XXXX</b></td>
  <td width="45" align="center"><b>XXXX</b></td>
 </tr>
 <tr style="background-color:#FF0000;color:#FFFF00;">
  <td width="30" align="center"><b>B</b></td>
  <td width="140" align="center"><b>XXXX</b></td>
  <td width="140" align="center"><b>XXXX</b></td>
  <td width="80" align="center"> <b>XXXX</b></td>
  <td width="80" align="center"><b>XXXX</b></td>
  <td width="45" align="center"><b>XXXX</b></td>
 </tr>
</thead>
 <tr>
  <td width="30" align="center">1.</td>
  <td width="140" rowspan="6">XXXX<br />XXXX<br />XXXX<br />XXXX<br />XXXX<br />XXXX<br />XXXX<br />XXXX</td>
  <td width="140">XXXX<br />XXXX</td>
  <td width="80">XXXX<br />XXXX</td>
  <td width="80">XXXX</td>
  <td align="center" width="45">XXXX<br />XXXX</td>
 </tr>
 <tr>
  <td width="30" align="center" rowspan="3">2.</td>
  <td width="140" rowspan="3">XXXX<br />XXXX</td>
  <td width="80">XXXX<br />XXXX</td>
  <td width="80">XXXX<br />XXXX</td>
  <td align="center" width="45">XXXX<br />XXXX</td>
 </tr>
 <tr>
  <td width="80">XXXX<br />XXXX<br />XXXX<br />XXXX</td>
  <td width="80">XXXX<br />XXXX</td>
  <td align="center" width="45">XXXX<br />XXXX</td>
 </tr>
 <tr>
  <td width="80" rowspan="2" >RRRRRR<br />XXXX<br />XXXX<br />XXXX<br />XXXX<br />XXXX<br />XXXX<br />XXXX</td>
  <td width="80">XXXX<br />XXXX</td>
  <td align="center" width="45">XXXX<br />XXXX</td>
 </tr>
 <tr>
  <td width="30" align="center">3.</td>
  <td width="140">XXXX1<br />XXXX</td>
  <td width="80">XXXX<br />XXXX</td>
  <td align="center" width="45">XXXX<br />XXXX</td>
 </tr>
 <tr>
  <td width="30" align="center">4.</td>
  <td width="140">XXXX<br />XXXX</td>
  <td width="80">XXXX<br />XXXX</td>
  <td width="80">XXXX<br />XXXX</td>
  <td align="center" width="45">XXXX<br />XXXX</td>
 </tr>
</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

$pdf->writeHTML($tbl, true, false, false, false, '');

// -----------------------------------------------------------------------------

// NON-BREAKING TABLE (nobr="true")

$tbl = <<<EOD
<table border="1" cellpadding="2" cellspacing="2" nobr="true">
 <tr>
  <th colspan="3" align="center">NON-BREAKING TABLE</th>
 </tr>
 <tr>
  <td>1-1</td>
  <td>1-2</td>
  <td>1-3</td>
 </tr>
 <tr>
  <td>2-1</td>
  <td>3-2</td>
  <td>3-3</td>
 </tr>
 <tr>
  <td>3-1</td>
  <td>3-2</td>
  <td>3-3</td>
 </tr>
</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

// -----------------------------------------------------------------------------

// NON-BREAKING ROWS (nobr="true")

$tbl = <<<EOD
<table border="1" cellpadding="2" cellspacing="2" align="center">
 <tr nobr="true">
  <th colspan="3">NON-BREAKING ROWS</th>
 </tr>
 <tr nobr="true">
  <td>ROW 1<br />COLUMN 1</td>
  <td>ROW 1<br />COLUMN 2</td>
  <td>ROW 1<br />COLUMN 3</td>
 </tr>
 <tr nobr="true">
  <td>ROW 2<br />COLUMN 1</td>
  <td>ROW 2<br />COLUMN 2</td>
  <td>ROW 2<br />COLUMN 3</td>
 </tr>
 <tr nobr="true">
  <td>ROW 3<br />COLUMN 1</td>
  <td>ROW 3<br />COLUMN 2</td>
  <td>ROW 3<br />COLUMN 3</td>
 </tr>
</table>
EOD;

$pdf->writeHTML($tbl, true, false, false, false, '');

*/// -----------------------------------------------------------------------------

//Close and output PDF document
$pdf->Output('purchaseReport.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
