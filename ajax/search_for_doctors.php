<?php
include_once("../conf/conf.php");           //include configuration file  
include_once("../conf/common_functions.php");          // include function file

$search_id = $_REQUEST['search_id'];
$city_id = $_REQUEST['city_id'];
$location_info = explode("|", $city_id);
$location_info_id = $location_info[0];
$location_info_type = @$location_info[1];

$status_cond=" STATUS='ACTIVE' AND ";
$city_condition = "";
if ($location_info_id != "") {
    $city_condition = " WHERE ";
    if ($location_info_type == "CITY")
        $hospital_per_city = fetchListCondsWithColumn("CENTERID", "LWCENTERMASTER", " where".$status_cond." CITYID =" . $location_info_id);
    else
        $hospital_per_city = fetchListCondsWithColumn("CENTERID", "LWCENTERMASTER", " where".$status_cond." STATEID =" . $location_info_id);
    
    $hospital_array = array();
    foreach ($hospital_per_city as $hospital_info) {
        $hospital_array[] = $hospital_info['CENTERID'];
    }
    $hospital_str = implode(",", $hospital_array);
    $city_condition = " AND LWDOCTORNEWMASTER.CENTERID IN(" . $hospital_str . ")";
}
if ($city_condition != "") {
    if ($_SESSION["LOGINTYPE"] == "RETAIL") {
        $city_condition.=" AND LWDOCTORNEWMASTER.STATUS='ACTIVE' AND LWDOCTORNEWMASTER.AF_RETAIL='YES' ";
    } else {
        $city_condition.=" AND LWDOCTORNEWMASTER.STATUS='ACTIVE' AND LWDOCTORNEWMASTER.AF_CORPORATEPORTAL='YES' ";
    }
} else {
    if ($_SESSION["LOGINTYPE"] == "RETAIL") {
        $city_condition = " AND LWDOCTORNEWMASTER.STATUS='ACTIVE' AND LWDOCTORNEWMASTER.AF_RETAIL='YES' ";
    } else {
        $city_condition = " AND LWDOCTORNEWMASTER.STATUS='ACTIVE' AND LWDOCTORNEWMASTER.AF_CORPORATEPORTAL='YES' ";
    }
}
$search_info = explode("|", $search_id);
$search_info_id = $search_info[0];
$search_info_type = $search_info[1];
$doctor_search = array();


/* Find services  Session policy */
include("fetch_retail_or_corp_service.php");
$order = $_REQUEST['sorting_id'];


/* Fetch Doctors Detail if search keyword Type is Doctor */
if ($search_info_type == "DOCTOR") {
    $doctor_search = fetchListCondsWithColumn("DOCTORID", "LWDOCTORNEWMASTER", " where DOCTORID='" . strtolower($search_info_id) . "' " . $city_condition); //function to fetch doctor records
}

/* Fetch Doctors Detail if search keyword Type is Hospitals */
if ($search_info_type == "HOSPITAL") {
    $center_status='';
    $center_status=fetchListCondsWithColumn("CENTERID,STATUS", "LWCENTERMASTER", " where STATUS='ACTIVE' AND  CENTERID='" . strtolower($search_info_id) . "'");
    if(!empty($center_status) && $center_status[0]['STATUS']=='ACTIVE'){
        if ($order == "DOCTORNAME ASC" || $order == "DOCTORNAME DESC") {
            $order_str = " ORDER BY " . $order;
            $doctor_search = fetchListCondsWithColumn("DOCTORID", "LWDOCTORNEWMASTER", " where SERVICEID IN(" . $service_of_policy . ") AND CENTERID='" . strtolower($search_info_id) . "'" . $city_condition . $order_str); //function to fetch doctor records
        } else if ($order == "RATING ASC" || $order == "RATING DESC") {
            $doctor_info = fetchListCondsWithColumn("DOCTORID", "LWDOCTORNEWMASTER", " where SERVICEID IN(" . $service_of_policy . ") AND CENTERID='" . strtolower($search_info_id) . "'" . $city_condition); //function to fetch doctor records
            include("feedback_sort.php");
        } else {
            $doctor_search = fetchListCondsWithColumn("DOCTORID", "LWDOCTORNEWMASTER", " where SERVICEID IN(" . $service_of_policy . ") AND CENTERID='" . strtolower($search_info_id) . "'" . $city_condition); //function to fetch doctor records 
        }
    } 
}

/* Fetch Doctors Detail if search keyword Type is SERVICE */
if ($search_info_type == "SERVICE") {
    $service_info = fetchListCondsWithColumn("SHOWCENTERORDOCTOR", "LWSERVICEMASTER", " where".$status_cond."  SERVICEID=" . $search_info_id);
    if ($service_info[0]["SHOWCENTERORDOCTOR"] == "SHOWDOCTOR") {
        if ($order == "DOCTORNAME ASC" || $order == "DOCTORNAME DESC") {
            $order_str = " ORDER BY LWDOCTORNEWMASTER." . $order;
            $doctor_search = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.DOCTORID", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where LWCENTERMASTER.STATUS='ACTIVE' AND  LWDOCTORNEWMASTER.SERVICEID='" . $search_info_id . "' " . $city_condition . $order_str);
            
            
        } else if ($order == "RATING ASC" || $order == "RATING DESC") {
            $doctor_info = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.DOCTORID", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where LWCENTERMASTER.STATUS='ACTIVE' AND  LWDOCTORNEWMASTER.SERVICEID='" . $search_info_id . "' " . $city_condition);
            include("feedback_sort.php");
        } else if ($order == "DISCOUNTOFFER ASC" || $order == "DISCOUNTOFFER DESC") {
            $order_str = " ORDER BY LWSUBMODSERVICEMAPPING." . $order;
            $doctor_search = FetchJoinResultINNER("LWSUBMODSERVICEMAPPING.DISCOUNTOFFER", "LWDOCTORNEWMASTER.DOCTORID", "LWDOCTORNEWMASTER", "LWSUBMODSERVICEMAPPING", " LWSUBMODSERVICEMAPPING.CENTERID=LWDOCTORNEWMASTER.CENTERID AND LWSUBMODSERVICEMAPPING.SUBMODULEID=LWDOCTORNEWMASTER.SUBMODULEID AND LWSUBMODSERVICEMAPPING.SERVICEID=LWDOCTORNEWMASTER.SERVICEID ", " where LWSUBMODSERVICEMAPPING.STATUS='ACTIVE' AND   LWDOCTORNEWMASTER.SERVICEID='" . $search_info_id . "' " . $city_condition . $order_str);
        }
    } else {
        $city_condition = "";
        if ($location_info_id != "") {
            $city_condition = " AND LWCENTERMASTER.CENTERID IN(" . $hospital_str . ")";
        }
        else{
            $city_condition=" AND LWCENTERMASTER.STATUS='ACTIVE'";
        }
        
        if ($order == "DISCOUNTOFFER ASC" || $order == "DISCOUNTOFFER DESC") {
            $order_str = " ORDER BY LWSUBMODSERVICEMAPPING." . $order;
            $center_list = FetchJoinResultINNER("DISTINCT LWCENTERMASTER.CENTERID  ", "LWSUBMODSERVICEMAPPING.CENTERID,LWCENTERMASTER.CENTERNAME,LWCENTERMASTER.ADDRESS1,LWCENTERMASTER.ADDRESS2,LWCENTERMASTER.CITYID,LWCENTERMASTER.STATEID,LWCENTERMASTER.PINCODE,LWCENTERMASTER.PHONENUMBER,LWSUBMODSERVICEMAPPING.DISCOUNTOFFER", "LWSUBMODSERVICEMAPPING", "LWCENTERMASTER", "LWSUBMODSERVICEMAPPING.CENTERID=LWCENTERMASTER.CENTERID", " where LWSUBMODSERVICEMAPPING.STATUS='ACTIVE' AND LWSUBMODSERVICEMAPPING.SERVICEID='" . $search_info_id . "'   " . $city_condition . $order_str);
        } else if ($order == "DOCTORNAME ASC" || $order == "DOCTORNAME DESC") {
            if ($order == "DOCTORNAME ASC") {
                $order_str = " ORDER BY Lower(LWCENTERMASTER.CENTERNAME) ASC";
            } else {
                $order_str = " ORDER BY Lower(LWCENTERMASTER.CENTERNAME) DESC";
            }
            $center_list = FetchJoinResultINNER("DISTINCT LWCENTERMASTER.CENTERID  ", "LWSUBMODSERVICEMAPPING.CENTERID,LWCENTERMASTER.CENTERNAME,LWCENTERMASTER.ADDRESS1,LWCENTERMASTER.ADDRESS2,LWCENTERMASTER.CITYID,LWCENTERMASTER.STATEID,LWCENTERMASTER.PINCODE,LWCENTERMASTER.PHONENUMBER,LWSUBMODSERVICEMAPPING.DISCOUNTOFFER", "LWSUBMODSERVICEMAPPING", "LWCENTERMASTER", "LWSUBMODSERVICEMAPPING.CENTERID=LWCENTERMASTER.CENTERID", " where LWSUBMODSERVICEMAPPING.STATUS='ACTIVE' AND  LWSUBMODSERVICEMAPPING.SERVICEID='" . $search_info_id . "'   " . $city_condition . $order_str);
        } else {
            $center_list = FetchJoinResultINNER("DISTINCT LWCENTERMASTER.CENTERID  ", "LWSUBMODSERVICEMAPPING.CENTERID,LWCENTERMASTER.CENTERNAME,LWCENTERMASTER.ADDRESS1,LWCENTERMASTER.ADDRESS2,LWCENTERMASTER.CITYID,LWCENTERMASTER.STATEID,LWCENTERMASTER.PINCODE,LWCENTERMASTER.PHONENUMBER,LWSUBMODSERVICEMAPPING.DISCOUNTOFFER", "LWSUBMODSERVICEMAPPING", "LWCENTERMASTER", "LWSUBMODSERVICEMAPPING.CENTERID=LWCENTERMASTER.CENTERID", " where LWSUBMODSERVICEMAPPING.STATUS='ACTIVE' AND  LWSUBMODSERVICEMAPPING.SERVICEID='" . $search_info_id . "'   " . $city_condition);
        }
    }
}
/* Fetch Doctors Detail if search keyword Type is SPECIALTY */
if ($search_info_type == "SPECIALTY") {
    if ($order == "DOCTORNAME ASC" || $order == "DOCTORNAME DESC") {
        if ($order == "DOCTORNAME ASC") {
            $order_str = " ORDER BY Lower(LWDOCTORNEWMASTER.DOCTORNAME) ASC";
        } else {
            $order_str = " ORDER BY Lower(LWDOCTORNEWMASTER.DOCTORNAME) DESC";
        }
        $doctor_search = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.DOCTORID", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where LWCENTERMASTER.STATUS='ACTIVE' AND  INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $search_info_id . "',1,1)>0 " . $city_condition . $order_str);
    } else if ($order == "RATING ASC" || $order == "RATING DESC") {
        $doctor_info = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.DOCTORID", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where LWCENTERMASTER.STATUS='ACTIVE' AND INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $search_info_id . "',1,1)>0  " . $city_condition);
        include("feedback_sort.php");
    } else if ($order == "DISCOUNTOFFER ASC" || $order == "DISCOUNTOFFER DESC") {
        $order_str = " ORDER BY LWSUBMODSERVICEMAPPING." . $order;
        $doctor_search = FetchJoinResultINNER("LWSUBMODSERVICEMAPPING.DISCOUNTOFFER", "LWDOCTORNEWMASTER.DOCTORID", "LWDOCTORNEWMASTER", "LWSUBMODSERVICEMAPPING", " LWSUBMODSERVICEMAPPING.CENTERID=LWDOCTORNEWMASTER.CENTERID AND LWSUBMODSERVICEMAPPING.SUBMODULEID=LWDOCTORNEWMASTER.SUBMODULEID AND LWSUBMODSERVICEMAPPING.SERVICEID=LWDOCTORNEWMASTER.SERVICEID ", " where LWSUBMODSERVICEMAPPING.STATUS='ACTIVE' AND  INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $search_info_id . "',1,1)>0  " . $city_condition . $order_str);
    }
}

/* Fetch Doctors Detail if search keyword Type is AILMENT */
if ($search_info_type == "AILMENT") {
    $specialty_ids = fetchListCondsWithColumn("SPECAILTYID", "LWSPECIALTY_AILMENTMASTER", " where STATUS='ACTIVE' AND AILMENTID=" . $search_info_id);
    $dataArray = array();
    foreach ($specialty_ids as $spec_id) {
        if ($order == "DOCTORNAME ASC" || $order == "DOCTORNAME DESC") {
            $order_str = " ORDER BY LWDOCTORNEWMASTER." . $order;
            $doctor_search = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.DOCTORID", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where LWCENTERMASTER.STATUS='ACTIVE' AND  INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec_id['SPECAILTYID'] . "',1,1)>0 " . $city_condition . $order_str);
        } else if ($order == "RATING ASC" || $order == "RATING DESC") {
            $doctor_info = FetchJoinResultINNER("LWCENTERMASTER.CENTERID,LWCENTERMASTER.STATEID", "LWDOCTORNEWMASTER.DOCTORID", "LWDOCTORNEWMASTER", "LWCENTERMASTER", "LWDOCTORNEWMASTER.CENTERID=LWCENTERMASTER.CENTERID", " where LWCENTERMASTER.STATUS='ACTIVE' AND INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $spec_id['SPECAILTYID'] . "',1,1)>0  " . $city_condition);
            include("feedback_sort.php");
        } else if ($order == "DISCOUNTOFFER ASC" || $order == "DISCOUNTOFFER DESC") {
            $order_str = " ORDER BY LWSUBMODSERVICEMAPPING." . $order;
            $doctor_search = FetchJoinResultINNER("LWSUBMODSERVICEMAPPING.DISCOUNTOFFER", "LWDOCTORNEWMASTER.DOCTORID", "LWDOCTORNEWMASTER", "LWSUBMODSERVICEMAPPING", " LWSUBMODSERVICEMAPPING.CENTERID=LWDOCTORNEWMASTER.CENTERID AND LWSUBMODSERVICEMAPPING.SUBMODULEID=LWDOCTORNEWMASTER.SUBMODULEID AND LWSUBMODSERVICEMAPPING.SERVICEID=LWDOCTORNEWMASTER.SERVICEID ", " where LWCENTERMASTER.STATUS='ACTIVE' AND  INSTR (LWDOCTORNEWMASTER.SPECIALITYID, '" . $search_info_id . "',1,1)>0  " . $city_condition . $order_str);
        }
        $dataArray = array_merge($dataArray, $doctor_search);
    }
    $doctor_search = $dataArray;
}
if (count(@$doctor_search) > 0) {
    $i = 0;
    $k = 1;
    $error_discount='';
    $error_service='';
    /**
    * @author Sanjeev Kumar
    * Code modified for check center mapped with discount connect.
    * 
    */
    $query_submodule="select * from CRMSUBMODULEMASTER where upper(SUBMODULENAME) LIKE '%DISCOUNT CONNECT%' and STATUS='ACTIVE'";                                       //Fetch Submodule id based on submodule name
    $sql_submodule=oci_parse($conn,$query_submodule);
    oci_execute($sql_submodule);
    $submodule=oci_fetch_assoc($sql_submodule);
    $submodule_id='';
    if(!empty($submodule))
    {
        $submodule_id=$submodule['ID'];
    }
    while (count($doctor_search) > $i) {
        
        if(!empty($submodule_id)){
            if(isset($doctor_search[$i]["CENTERID"]) && !empty($doctor_search[$i]["CENTERID"])){
                $query="select * from LWCENTERSUBMODULEMAPPING where centerid='".$doctor_search[$i]["CENTERID"]."' and submoduleid='".$submodule_id."' AND STATUS='ACTIVE'";
            } else{
                $query_doc="select CENTERID from LWDOCTORNEWMASTER where DOCTORID='".$doctor_search[$i]["DOCTORID"]."'";
                $sql_doc=oci_parse($conn,$query_doc);
                oci_execute($sql_doc);
                $center_id='';
                $doctor=oci_fetch_assoc($sql_doc);
                $center_id=$doctor['CENTERID'];
                $query="select * from LWCENTERSUBMODULEMAPPING where centerid='".$center_id."' and submoduleid='".$submodule_id."' AND STATUS='ACTIVE'";
            }
            $sql=oci_parse($conn,$query);
            oci_execute($sql);
            $submodule_mapping_data='';
            $submodule_mapping_data=oci_fetch_assoc($sql);
            if(!empty($submodule_mapping_data)){
                $error_discount=1;                      //End of Code
                
           $doctors = fetchListCondsWithColumn("DOCTORID,DOCTORNAME,CENTERID,SUBMODULEID ,SERVICEID,SPECIALITYID,QUALIFICATION,CERTIFICATIONS,DESCRIPTION,CONTACTNUMBER,OPDTIMING,APPOINTMENTNUMBER", "LWDOCTORNEWMASTER", " where DOCTORID='" . $doctor_search[$i]["DOCTORID"] . "' "); //function to fetch doctor records
        $speciality_of_doc = $doctors[0]["SPECIALITYID"];
        $speciality_of_doc_arr = explode(",", $speciality_of_doc);
        $spec_name_arr = array();
        $hospital_info = fetchHospitalDetailOfDoctor($doctors[0]["CENTERID"]);
        $discount_info = fetchListCondsWithColumn("DISCOUNTOFFER", "LWSUBMODSERVICEMAPPING", " where CENTERID='" . @$doctors[0]['CENTERID'] . "' AND SUBMODULEID='" . @$doctors[0]['SUBMODULEID'] . "' AND SERVICEID='" . @$doctors[0]['SERVICEID'] . "'");
        $discount = $discount_info[0]['DISCOUNTOFFER'];
       // Code added for check mapping of service with center.
        $query_service="select * from LWSUBMODSERVICEMAPPING where CENTERID='".$doctors[0]['CENTERID']."' AND SUBMODULEID='".$doctors[0]['SUBMODULEID']."' AND SERVICEID='".$doctors[0]['SERVICEID']."' AND STATUS='ACTIVE'";
        $sql_service=oci_parse($conn,$query_service);
        oci_execute($sql_service);
        $service=oci_fetch_assoc($sql_service);
        if(isset($service) && !empty($service)){                    //End of Code
            $error_service=1;
        foreach ($speciality_of_doc_arr as $list) {
            $spec_names = fetchListCond("LWSPECIALTYMASTER", " where STATUS='ACTIVE' AND SPECIALTYID =" . $list);
            if (isset($spec_names[0]["SPECIALTYNAME"]) && !empty($spec_names[0]["SPECIALTYNAME"])) {
                $spec_name = @$spec_names[0]["SPECIALTYNAME"];
                $spec_name_arr[] = @$spec_name;
            }
        }
        $specialty_names = implode(",", $spec_name_arr);
        ?> <?php $doctor_id = $doctors[0]['DOCTORID']; ?>
        <div class="wecare-leftTop" id="doctor_unique_id<?php echo $doctor_id; ?>" >
            <div class="wecare-leftTopIn">
                <div class="topBar"> <?php echo $doctors[0]["DOCTORNAME"]; ?> <span><?php //echo $specialty_names;                                ?> </span></div>

                <div class="doclorAddressLeft">
                    <p><?php echo @$doctors[0]["QUALIFICATION"]; ?>( <?php echo @$hospital_info[0]["CENTERNAME"]; ?>)<br />
                        <?php echo $doctors[0]["CONTACTNUMBER"]; ?><br />
                        Appointment No. <span><?php echo $doctors[0]["APPOINTMENTNUMBER"]; ?></span><br />
                        OPD Timing <span><?php echo $doctors[0]["OPDTIMING"]; ?></span></p>


                    <a   class="smsIcon" style="cursor:pointer;" onclick="doctor_sms('<?php echo $doctor_id; ?>');"> </a>
                    <a   class="mailIcon" style="cursor:pointer;" onclick="network_doctor_email('<?php echo $doctor_id; ?>');" ></a>
                </div>

                <div class="doclorAddressRight">
                    <div class="topStar"> 
                        <?php
                        $feedbacks = fetchListCond("LWDOCTORFEEDBACK", " where DOCTORID=" . $doctors[0]["DOCTORID"] . " order by CREATEDON DESC");
                        $total_feedback = count($feedbacks);
                        $avg_rating = 0;
                        if ($total_feedback > 0) {
                            $tot_rate = 0;
                            foreach ($feedbacks as $feedback) {
                                $tot_rate = $feedback["RATE"] + $tot_rate;
                            }
                            $avg_rating = number_format(abs($tot_rate / $total_feedback), 1);
                        }


                        if ($total_feedback > 1) {
                            $user_text = "Users";
                        } else {
                            $user_text = "User";
                        }
                        $star_text = "";
                        if ($avg_rating > 1) {
                            $star_text = "Stars";
                        } else if ($avg_rating == 1) {
                            $star_text = "Star";
                        } else {
                            $star_text == "";
                        }
                        if ($total_feedback > 0) {
                            echo $avg_rating . " " . $star_text . " from " . $total_feedback . " " . $user_text;
                        } else {
                            ?>
                            <a onclick="feedback(<?php echo $doctors[0]["DOCTORID"]; ?>);"  style="cursor: pointer; " class="topStar"  >Be First to Rate <?php echo @$doctors[0]["DOCTORNAME"]; ?> </a>
                        <?php } ?>
                    </div><br />
                    <p><a  style="text-decoration:none;" class="feedbacknav"><?php
                            if ($total_feedback > 0) {
                                echo $total_feedback;
                            } else {
                                echo "No";
                            }
                            ?> Feedback</a><br />
                        <?php if ($_SESSION['policyexpiredisables']['vas.discount_connect.download_in_search'] != 'NO') { ?>
                            <span   class="officon"><?php echo @$discount . "% "; ?></span><br />
                        <?php } ?>
                        <span class="locationicon"><?php echo @$hospital_info[0]["CENTERNAME"]; ?> </span><br/>
                        <span  style="margin-left:32px;"> <?php echo @$hospital_info[0]["CITYNAME"] . "," . @$hospital_info[0]["STATENAME"]; ?></span>
                    </p><br />
                    <div class="bottomNav">
                        <?php if ($_SESSION['policyexpiredisables']['vas.discount_connect.download_coupon'] != 'NO') { ?>
                            <a class="generate-btn left" onclick="generate_coupon(<?php echo $doctors[0]["DOCTORID"]; ?>, '<?php echo @$_SESSION['policyNumber'] ? $_SESSION['policyNumber'] : 'NA'; ?>')" style="cursor: pointer;" >Generate Coupon</a>
                        <?php } else { ?>
                            <a class="generate-btn left" onclick="alert('You can\'t download discount copupn as your policy is expired ! ')" style="cursor: pointer;" >Generate Coupon</a>
                        <?php } ?>
                        <a class="fedback-btn left" onclick="feedback(<?php echo $doctors[0]["DOCTORID"]; ?>)"  style="cursor: pointer;"  >Feedback</a>
                        <div class="clearfix"></div></div>
                </div>
                <div class="clearfix"></div>


            </div>
            <div align="center" ><img id="generate_coupon_load_image<?php echo $doctors[0]["DOCTORID"]; ?>" src="images/loading.gif" style="display:none;"></div>

            <div class="wecare-generateCoupon"   id="coupon_form<?php echo $doctors[0]["DOCTORID"]; ?>" style="display:none;">

            </div>

        </div>
<?php
                }
            } 
        } 
        
        ?>
        <?php
        $i++;
    }
    if(isset($error_discount) && empty($error_discount)){
        echo "<div class='dashboard-leftTop' style='text-align:center' >Discount connect does not mapped to the center. Please try again later.</div>";
    }
    if(isset($error_service) && empty($error_service)){
        echo "<div class='dashboard-leftTop' style='text-align:center' >This Service is not available. Please check any other service</div>";
    }
    if ($search_info_type == "HOSPITAL") {
        $query = "select LWSUBMODSERVICEMAPPING.SERVICEID, LWSUBMODSERVICEMAPPING.DISCOUNTOFFER, LWSERVICEMASTER.NAME from LWSUBMODSERVICEMAPPING inner join LWSERVICEMASTER on LWSUBMODSERVICEMAPPING.SERVICEID=LWSERVICEMASTER.SERVICEID where LWSUBMODSERVICEMAPPING.STATUS='ACTIVE' AND LWSERVICEMASTER.STATUS='ACTIVE' AND  LWSUBMODSERVICEMAPPING.CENTERID=" . $doctors[0]['CENTERID'] . " AND LWSUBMODSERVICEMAPPING.DISCOUNTOFFER IS NOT NULL";
        if ($sql = oci_parse($conn, $query)) {
            if (oci_execute($sql)) {
                $q = 0;
                while (($row = oci_fetch_assoc($sql))) {
                    foreach ($row as $key => $value) {
                        $service_discount[$q][$key] = $value;
                    }
                    $q++;
                }
            } else {
                echo "<div class='dashboard-leftTop' style='text-align:center' >There is some error. Please try again later.</div>";
            }
        } else {
            echo "<div class='dashboard-leftTop' style='text-align:center' >There is some error. Please try again later.</div>";
        }
        if (count($service_discount) > 0) {
            $query = "select CENTERID,CENTERNAME,ADDRESS1,ADDRESS2,CITYID,STATEID,PINCODE,PHONENUMBER,DISCOUNTOFFER from LWCENTERMASTER where STATUS='ACTIVE' AND CENTERID=" . $doctors[0]['CENTERID'];
            if ($sql = oci_parse($conn, $query)) {
                if (oci_execute($sql)) {
                    if ($center_list = oci_fetch_assoc($sql)) {
                        
                    } else {
                        
                    }
                } else {
                    echo "<div class='dashboard-leftTop' style='text-align:center' >There is some error. Please try again later.</div>";
                }
            } else {
                echo "<div class='dashboard-leftTop' style='text-align:center' >There is some error. Please try again later.</div>";
            }
            include("service_detail.php");
        }
    }
} elseif (count(@$center_list) > 0) {

    include("center_result.php");
} else {
    echo "<div class='dashboard-leftTop' style='text-align:center' >No records founds as per your search criteria, Try choosing other combinations !</div>";
}
?>


<script type="text/javascript">

</script>