<?php
include_once("../conf/conf.php");
include_once("../conf/common_functions.php");
?> 

<?php  
$l = 0;
$k = 1;
$error_discount_service='';
$query_submodule="select * from CRMSUBMODULEMASTER where upper(SUBMODULENAME) LIKE '%DISCOUNT CONNECT%' and STATUS='ACTIVE'";                                       //Fetch Submodule id based on submodule name
$sql_submodule=oci_parse($conn,$query_submodule);
oci_execute($sql_submodule);
$submodule=oci_fetch_assoc($sql_submodule);
$submodule_id='';
if(!empty($submodule))
{
    $submodule_id=$submodule['ID'];
}
while (count($service_discount) > $l) {
        
        if(!empty($submodule_id)){
            $query="select * from LWCENTERSUBMODULEMAPPING where centerid='".$center_list["CENTERID"]."' and submoduleid='".$submodule_id."' AND STATUS='ACTIVE'";
            $sql=oci_parse($conn,$query);
            oci_execute($sql);
            $submodule_mapping_data='';
            $submodule_mapping_data=oci_fetch_assoc($sql);
            if(!empty($submodule_mapping_data)){
                $error_discount_service=1; 
    
    ?>   
    <div class="wecare-leftTop">
        <div class="wecare-leftTopIn">
            <div class="topBar">
    <?php echo $service_discount[$l]['NAME']; ?></div>
            <div class="doclorAddressLeft">
                <p><?php echo $center_list["CENTERNAME"]; ?><br />
                    <?php echo $center_list["ADDRESS1"]; ?>,<?php echo @$center_list["ADDRESS2"]; ?><br />
    <?php echo $center_list["PHONENUMBER"]; ?><br />
                    Pincode. <span><?php echo @$center_list["PINCODE"]; ?></span><br />
                    Phone.   <span><?php echo @$center_list["PHONENUMBER"]; ?></span>
                </p>
                <a   class="smsIcon"  style="cursor:pointer;"   onclick="network_hospital_sms(<?php echo $center_list["CENTERID"]; ?>,<?php echo $l; ?>);" id="h_<?php echo $center_list["CENTERID"]; ?>" ></a>
                <a   class="mailIcon"  style="cursor:pointer;"  onclick="network_hospital_email(<?php echo $center_list["CENTERID"]; ?>,<?php echo $l; ?>);" id="email_<?php echo $center_list["CENTERID"]; ?>" ></a>
            </div>

            <div class="doclorAddressRight" style="width:300px;"> <p>
                    <?php if ($_SESSION['policyexpiredisables']['vas.discount_connect.download_in_search'] != 'NO') { ?>
                        <span class="officon"><?php echo $service_discount[$l]['DISCOUNTOFFER']?$service_discount[$l]['DISCOUNTOFFER']."%": "NA"; ?></span><br />
                    <?php } else { ?>
                        <span class="officon" style="display:none"><?php echo 'NA'; ?></span><br />
    <?php } ?>
                    <span class="locationicon"><?php echo @$center_list["CENTERNAME"]; ?>. (<?php echo $hospital_info[0]["CITYNAME"] . "," . $hospital_info[0]["STATENAME"]; ?>)</span>
                </p><br />
                <div class="bottomNav"> 
                    <?php if ($_SESSION['policyexpiredisables']['vas.discount_connect.download_coupon'] != 'NO') { ?>
                        <a class="generate-btn left" onclick="generate_center_coupon(<?php echo $center_list["CENTERID"]; ?>,<?php echo @$search_info_id; ?>,<?php echo $l; ?>)" style="cursor: pointer;" >Generate Coupon</a>
                    <?php } else { ?>
                        <a class="generate-btn left" style="cursor: pointer;" onclick="alert('You can\'t download discount copupn as your policy is expired ! ')" >Generate Coupon</a>
    <?php } ?>
                    <div class="clearfix"></div></div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div align="center" ><img id="generate_coupon_load_image<?php echo $center_list["CENTERID"] . '_' . $l; ?>" src="images/loading.gif" style="display:none;"></div>

        <div class="wecare-generateCoupon"  id="sms_email_form<?php echo $center_list["CENTERID"] . '_' . $l; ?>" style="display:none;" >
        </div>

    </div>
        <?php }}
    $l++;
    $k++;
}
if(isset($error_discount_service) && empty($error_discount_service)){
        echo "<div class='dashboard-leftTop' style='text-align:center' >Discount connect does not mapped to the center. Please try again later.</div>";
    }
?>

<script>
    function network_hospital_sms(hosp_id, num)
    {
        $("#generate_coupon_load_image" + hosp_id + "_" + num).show();
        $(".wecare-generateCoupon").html("");
        $.ajax({
            type: "post",
            url: 'ajax/send_sms.php',
            data: {'id': hosp_id, 'num': num},
            success: function (resp) {
                $("#sms_email_form" + hosp_id + "_" + num).show();
                $("#sms_email_form" + hosp_id + "_" + num).html(resp);
                $("#generate_coupon_load_image" + hosp_id + "_" + num).hide();

            }
        });
    }

    function network_hospital_email(centerid, num) {
        $("#generate_coupon_load_image" + centerid + "_" + num).show();
        $.ajax({
            type: "post",
            url: 'ajax/send_email.php',
            data: {"centerid": centerid, "num": num},
            success: function (resp) {
                $("#sms_email_form" + centerid + "_" + num).show();
                $("#sms_email_form" + centerid + "_" + num).html(resp);
                $("#generate_coupon_load_image" + centerid + "_" + num).hide();
            }
        });
    }

    function generate_center_coupon(center_id, service_id, num)
    {
        $("#generate_coupon_load_image" + center_id + "_" + num).show();
        $(".wecare-generateCoupon").html("");
        $.ajax({
            type: "post",
            url: 'ajax/generate_center_coupon.php',
            data: {'id': center_id, 'service_id': service_id},
            success: function (resp) {
                $("#sms_email_form" + center_id + "_" + num).show();
                $("#sms_email_form" + center_id + "_" + num).html(resp);
                $("#generate_coupon_load_image" + center_id + "_" + num).hide();
                $('html,body').animate({
                scrollTop: $("#sms_email_form" + center_id + "_" + num).offset().top},
                500);
            }
        });
    }

    function sendHospitalListSMS1() {
        var t = trim($("#firstName").val());
        var n = trim($("#mobileNo").val());
        var num = trim($("#num").val());
        var hospitalId = trim($("#hospitalId").val());
        if (t == "" || t == "Your Name *") {
            $("#error_message").show();
            $("#error_message").html("Please enter your name.");
            $("#error_message").hide(5000);
            return false;
        }
        if (n == "" || n == "Phone No. *") {

            $("#error_message").show();
            $("#error_message").html("Please enter your Mobile Number.");
            $("#error_message").hide(5000);
            return false;
        } else if (!isCharsInBag1(n, "0123456789")) {
            $("#error_message").show();
            $("#error_message").html("Mobile must only contain Numbers");
            $("#error_message").hide(5000);

            return false;
        }
        if (n.length < 10)
        {
            $("#error_message").show();
            $("#error_message").html("Mobile Number Not valid.");
            $("#error_message").hide(5000);
            return false;
        }
        $("#hospitallistsms1").html('<img src="images/loading.gif" class="fl">');
        $.ajax({
            type: "POST",
            url: "ajax/hospital_sms.php",
            data: "firstname=" + t + "&mobilephone=" + n + "&hospitalId=" + hospitalId,
            success: function (msg) {

                alert("Network Hospital detail has been sent to your Mobile.");
                $("#sms_email_form" + hospitalId + "_" + num).html("");

            }
        });
    }

    function isCharsInBag1(s, bag)
    {
        var i;
        for (i = 0; i < s.length; i++)
        {
            var c = s.charAt(i);
            if (bag.indexOf(c) == -1)
                return false;
        }
        return true;
    }

    function CheckAll()
    {
        var className = 'checkbox_chk';
        if ($("#checkbox_").prop('checked') == true)
        {
            $(".checkbox_chk").prop('checked', true);
        } else
        {
            $(".checkbox_chk").prop('checked', false);
        }
    }

    function getInboxSelectedValue(className) {
        var strID = "";

        $("." + className).each(function ()
        {
            if (this.checked == true)
            {
                if (strID == "")
                {
                    strID = this.value
                } else
                {
                    strID = strID + ',' + this.value;
                }
            }
        });
        return strID;
    }




    function sendHospitalListEmail(centerid) {
        var e = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
        var t = trim($("#yourname").val());
        var n = trim($("#emailId").val());
        var num = trim($("#num").val());
        if (t == "" || t == "Your Name *") {
            $("#error_message").show();
            $("#error_message").html("Please enter your name.");
            $("#error_message").hide(5000);
            return false;
        }
        if (n == "" || n == "Email ID *") {
            $("#error_message").show();
            $("#error_message").html("Please enter your email address.");
            $("#error_message").hide(5000);
            return false;
        } else {
            if (e.test(n) == false) {
                $("#error_message").show();
                $("#error_message").html("Please enter your email address in correct format.");
                $("#error_message").hide(5000);
                return false;
            }
        }

        $("#hospitallistemail").html('<img src="images/loading.gif" class="fl">');
        $.ajax({
            type: "POST",
            url: "ajax/hospital_email.php",
            data: "firstname=" + t + "&emailaddress=" + n + "&centerid=" + centerid,
            success: function (msg) {
                $("#error_message").show();
                $("#error_message").html("<span style='color:green;' >Network Hospital details have been sent to your email ID.</span>");
                $("#error_message").hide(5000);
                alert("Network Hospital detail has been sent on your Email Id.");
                $("#sms_email_form" + centerid + "_" + num).html("");
            }
        });
    }
</script>