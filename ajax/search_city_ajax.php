<?php

//include_once("../conf/session.php");              //include session file 
include_once("../conf/conf.php");                   //include configuration file 
include_once("../conf/common_functions.php");       // include function file
if (!isset($_SESSION["productId"])) {
    $_SESSION["productId"] = 2;
}

$search_keyword = $_REQUEST['term'];
$search_array = array();
$centers = "";
/* Find services  Session policy */
include("fetch_retail_or_corp_service.php");
/* Find centers of the services match with Session policy */

$center_mapping_info = fetchListCondsWithColumn("DISTINCT(CENTERID)", "LWSUBMODSERVICEMAPPING", " Where SERVICEID IN($service_of_policy)");
$center_array = array();
foreach ($center_mapping_info as $center_mapping) {
    $center_array[] = $center_mapping["CENTERID"];
}
if (!empty($center_array)) {
    $centers = implode(",", $center_array);
}

/* Find City and State if find some center match with Policy */

if ($centers != "") {
    /* Find City if it match with the keyword given by location textbox and  Serivies of Session Policy and Center Match (City of Session Policy Service) */
    $city_search = FetchJoinResultINNER("DISTINCT LWCITYMASTER.CITYID,LWCITYMASTER.CITYNAME,LWCITYMASTER.STATEID", "LWCENTERMASTER.CITYID", "LWCITYMASTER", "LWCENTERMASTER", "LWCITYMASTER.CITYID=LWCENTERMASTER.CITYID", " where Lower(LWCITYMASTER.CITYNAME) LIKE '" . strtolower($search_keyword) . "%'  AND LWCENTERMASTER.CENTERID IN($centers) "); //
    if (!empty($city_search)) {
        foreach ($city_search as $info) {
            $state_info = fetchListCondsWithColumn("STATENAME,STATEID", "LWSTATEMASTER", " where STATEID='".strtolower($info["STATEID"])."'");
            $search_array[] = array("id" => $info['CITYID'] . "|" . "CITY", "value" => '[CITY] '.$info['CITYNAME']." - ".$state_info[0]['STATENAME']);
        }
    }
    //echo "<pre>";print_r($city_search);exit;
    /* Find State if it match with the keyword given by location textbox and  Serivies of Session Policy and Center Match (State of Session Policy Service) */
    if (empty($search_array)) {
        $state_search = FetchJoinResultINNER("DISTINCT LWSTATEMASTER.STATEID,LWSTATEMASTER.STATENAME", "LWCENTERMASTER.STATEID", "LWSTATEMASTER", "LWCENTERMASTER", "LWSTATEMASTER.STATEID=LWCENTERMASTER.STATEID", " where Lower(LWSTATEMASTER.STATENAME) LIKE '" . strtolower($search_keyword) . "%' AND LWCENTERMASTER.CENTERID IN($centers)  "); // 
        if (!empty($state_search)) {
            foreach ($state_search as $info) {
                $state_info = fetchListCondsWithColumn("STATENAME,STATEID", "LWSTATEMASTER", " where STATEID='" . strtolower($info["STATEID"]) . "'");
                $search_array[] = array("id" => $info['STATEID'] . "|" . "STATE", "value" => '[STATE] '.$info['STATENAME']);
            }
        }
    }
}





//$address_search = fetchListCondsWithColumn("ADDRESS1,ADDRESS2,STATEID,CITYID,CENTERID", "LWCENTERMASTER", " where Lower(ADDRESS1)LIKE '%".strtolower($search_keyword)."%' OR Lower(ADDRESS2)LIKE '%".strtolower($search_keyword)."%'");
//if (!empty($address_search)) {
//    foreach ($address_search as $info) {  
//        $state_info = fetchListCondsWithColumn("STATENAME,STATEID", "LWSTATEMASTER", " where STATEID='".$info["STATEID"]."'");         
//        $city_info = fetchListCondsWithColumn("CITYNAME,CITYID", "LWCITYMASTER", " where CITYID='".$info["CITYID"]."'");
//        $search_array[] = array("id" => $info['CITYID']."|"."CITY", "value" => $info['ADDRESS1']." ".$info['ADDRESS2']."-".@$city_info[0]["CITYNAME"]."-".@$state_info[0]["STATENAME"]);
//    }
//}
$k = 0;
while (count($search_array) > $k) {
    $search_array[$k]['value'] = htmlspecialchars_decode($search_array[$k]['value']);
    $k++;
}
echo json_encode($search_array);
exit;
?>
