<?php
if(file_exists('.env')){
    $config = parse_ini_file(".env");
}

function env($key) {
	global $config;
	return isset($config[$key])?$config[$key]:'';
}
?>