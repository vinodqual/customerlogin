<?php
/* * ***************************************************************************
 * COPYRIGHT
 * Copyright 2013 Catabatic Technology Pvt Ltd.
 * All rights reserved
 *
 * DISCLAIMER
 *
 *
 * AUTHOR
 *
 * $Id: claims.php,v 1.0 2015/10/06 04:55:0 Amit Kumar Dubey Exp $
 * $Author: Amit Kumar Dubey $
 *
 * ************************************************************************** */
include_once("conf/constants.php");
include("inc/inc.hd.php");              //include header file to show header on page
if (env("DISABLE_CMS") == "true") {
    header("Location:" . env("DASHURL"));
}
include_once($apiurl);
if (isset($_SESSION['response_error'])) {
    $responseError = $_SESSION['response_error'];
} else {
    $responseArray = $_SESSION['dataArray'];
}
if (@$TPAManagedchkdis == 'Yes') {
    echo "<script>window.location.href='dashboard.php';</script>";
    exit;
}
$claimArray = array();
if (isset($_POST['formtype'])) {

    $name = sanitize_center(@$_SESSION['policyHolderName']);
    $phone = sanitize_center(@$_POST['txtPhone']);
    $email = sanitize_center(@$_POST['txtEmail']);
    $policyNumber = sanitize_data(@$_SESSION['policyNumber']);
    $enquiry = sanitize_center(@$_POST['txtEnquiry']);
    $formtype = sanitize_data(@$_POST['formtype']);
    if ($formtype == 'CORPORATE' && $_SESSION['LOGINTYPE'] == 'CORPORATE') {
        if ($policyNumber != '') {
            $entryTime = date('d-m-Y H:i');
            $sql = "INSERT INTO CRMCOMPANYREIMBURSEMENTREQUEST (REQUESTID,COMPANYID,EMPLOYEEID,POLICYNO,EMPLOYEENAME,EMAIL,REMARKS,STATUS,CREATEDON,CREATEDBY,UPDATEDON,UPDATEDBY,REQUESTDATE,IPADDRESS) values( CRMCOMREIMBREQU_SEQ.nextval,'" . @$_SESSION['COMPANYID'] . "','" . @$_SESSION['empId'] . "','" . @$policyNumber . "','" . @$name . "','" . @$email . "','" . addslashes(@$enquiry) . "','PENDING','" . @$entryTime . "','" . @$_SESSION['userName'] . "','" . @$entryTime . "','" . @$_SESSION['userName'] . "','" . date('d-M-Y') . "','" . @$_SERVER['REMOTE_ADDR'] . "') ";
            $stdid = @oci_parse($conn, $sql);
            $r = @oci_execute($stdid);
            if ($r == 1) {
                $msg = 'success';
            } else {
                $msg = 'failure';
            }
            $check1 = @oci_parse($conn, 'SELECT CRMCOMREIMBREQU_SEQ.currval FROM DUAL');
            @oci_execute($check1);
            $res = @oci_fetch_assoc($check1);
            $rowid = @$res['CURRVAL'];
            //print_r($_FILES); die;
            for ($i = 1; $i <= 5; $i++) {
                if ($_FILES['upload' . $i]['name'] != '') {
                    if (($_FILES["upload" . $i]["type"] == "application/msword") || ($_FILES["upload" . $i]["type"] == "application/kswps") || ($_FILES["upload" . $i]["type"] == "application/pdf") || ($_FILES["upload" . $i]["type"] == "application/binary") || ($_FILES["upload" . $i]["type"] == "application/octet-stream") || ($_FILES["upload" . $i]["type"] == "application/vnd.ms-excel") || ($_FILES["upload" . $i]["type"] == "image/jpg") || ($_FILES["upload" . $i]["type"] == "image/jpeg") || ($_FILES["upload" . $i]["type"] == "image/gif")) {
                        $prefix = substr(time(), 5, 5) . $i;
                        $filename = basename($prefix . toHAndleSpace($_FILES['upload' . $i]['name']));
                        $target_path = "../religarehrcrm/fileupload/";
                        $filename = touploadItem('upload', $target_path, $filename);
                        $docTitle = sanitize_center(@$_POST['docTitle' . $i]);
                        $sql1 = "INSERT INTO CRMCOMPANYREIMBURSEMENTDOCS (DOCID,REQUESTID,DOCTITLE,FILENAME,UPLOADDATE,CREATEDON,CREATEDBY,UPDATEDON,UPDATEDBY,IPADDRESS) values( SYSTEM.CRMCOMREIMBDOCS_SEQ.nextval,'" . @$rowid . "','" . @$docTitle . "','" . @$filename . "','" . date('d-M-Y') . "','" . @$entryTime . "','" . @$_SESSION['userName'] . "','" . @$entryTime . "','" . @$_SESSION['userName'] . "','" . @$_SERVER['REMOTE_ADDR'] . "') ";

                        $stdid = @oci_parse($conn, $sql1);
                        $r = @oci_execute($stdid);
                    }
                }
            }
        }
    } else {
        if (@$_SESSION['productId'] > 0 && $policyNumber != '') {
            $entryTime = date('d-m-Y H:i');
            $sql = "INSERT INTO CRMRETAILREIMBURSEMENTREQUEST (REQUESTID,PRODUCTID,EMPLOYEEID,POLICYNO,EMPLOYEENAME,EMAIL,REMARKS,STATUS,CREATEDON,CREATEDBY,UPDATEDON,UPDATEDBY,REQUESTDATE,IPADDRESS) values(CRMRETREIMBREQU_SEQ.nextval,'" . @$_SESSION['productId'] . "','" . @$_SESSION['customerId'] . "','" . @$policyNumber . "','" . @$name . "','" . @$email . "','" . addslashes(@$enquiry) . "','PENDING','" . @$entryTime . "','" . @$_SESSION['userName'] . "','" . @$entryTime . "','" . @$_SESSION['userName'] . "','" . date('d-M-Y') . "','" . @$_SERVER['REMOTE_ADDR'] . "') ";
            $stdid = @oci_parse($conn, $sql);
            $r = @oci_execute($stdid);
            if ($r == 1) {
                $msg = 'success';
            } else {
                $msg = 'failure';
            }
            $check1 = @oci_parse($conn, 'SELECT CRMRETREIMBREQU_SEQ.currval FROM DUAL');
            @oci_execute($check1);
            $res = @oci_fetch_assoc($check1);
            $rowid = @$res['CURRVAL'];
            //print_r($_FILES);
            for ($i = 1; $i <= 5; $i++) {
                if ($_FILES['upload' . $i]['name'] != '') {
                    if (($_FILES["upload" . $i]["type"] == "application/msword") || ($_FILES["upload" . $i]["type"] == "application/pdf") || ($_FILES["upload" . $i]["type"] == "application/binary") || ($_FILES["upload" . $i]["type"] == "application/octet-stream") || ($_FILES["upload" . $i]["type"] == "application/vnd.ms-excel") || ($_FILES["upload" . $i]["type"] == "image/jpg") || ($_FILES["upload" . $i]["type"] == "image/jpeg") || ($_FILES["upload" . $i]["type"] == "image/gif")) {
                        $prefix = substr(time(), 5, 5) . $i;
                        $filename = basename($prefix . toHAndleSpace($_FILES['upload' . $i]['name']));
                        $target_path = "../religarehrcrm/fileupload/";
                        $filename = touploadItem('upload', $target_path, $filename);
                        $docTitle = sanitize_center(@$_POST['docTitle' . $i]);
                        $sql1 = "INSERT INTO CRMRETAILREIMBURSEMENTDOCS (DOCID,REQUESTID,PRODUCTID,DOCTITLE,FILENAME,UPLOADDATE,CREATEDON,CREATEDBY,UPDATEDON,UPDATEDBY,IPADDRESS) values(CRMRETREIMBDOCS_SEQ.nextval,'" . @$rowid . "','" . @$_SESSION['productId'] . "','" . @$docTitle . "','" . @$filename . "','" . date('d-M-Y') . "','" . @$entryTime . "','" . @$_SESSION['userName'] . "','" . @$entryTime . "','" . @$_SESSION['userName'] . "','" . @$_SERVER['REMOTE_ADDR'] . "') ";
                        $stdid = @oci_parse($conn, $sql1);
                        $r = @oci_execute($stdid);
                    }
                }
            }
        }
        FunSendReimbursementMail($email, $policyNumber, $name, $phone, $enquiry);
    }
    echo "<script>window.location.href='claims.php?tab=cmVpbWJ1cg==&msg=$msg'</script>";
    //header('Location: claims.php?tab=cmVpbWJ1cg==&msg='.$msg);
    exit;
}
?>
<script type="text/javascript" src="js/iframeResizer.min.js"></script>  
<style>
    div.error {
        position:absolute;
        margin-top:14px;
        color:red;
        background-color:#fff;
        padding:3px;
        text-align:left;
        z-index:1;
        margin-left: -17px;
        font-size:14px;
    }
</style>
<style>
    div #upload1-error {
        margin-left: 231px;
        margin-top: 52px;
    }
    div #upload2-error {
        margin-left: 231px;
        margin-top: 52px;
    }
    div #upload3-error {
        margin-left: 231px;
        margin-top: 52px;
    }
    div #upload4-error {
        margin-left: 231px;
        margin-top: 52px;
    }
    div #upload5-error {
        margin-left: 231px;
        margin-top: 52px;
    }
    .tab-content>.active{
        overflow:hidden;
    }
    .colfullBot{
        margin-top: 0px !important;
    }
</style>
<script>
    $(document).ready(function () {
        $(".downloaddocument").attr('onclick', "window.location.href='downloadCenter.php'");
        $(".downloadhospitals").attr('onclick', "window.location.href='network-hospital-for-uat.php'");
        $(".nonprefferedhospitals").attr('onclick', "window.location.href='non-preferred-hospitals.php'");
    });
</script>

<section id="middleContainer">
    <div class="container-fluid">
        <div class="middlebox">
            <div class="col-md-9">
                <div class="middlebox-left">

                    <div class="tab-content responsive">
                        <div class="tab-pane <?php
                        if (empty($tab) || $tab == "claims") {
                            echo "active";
                        }
                        ?>" id="claims">


                            <div class="colfullBot">
                                <div class="tab-paneIn">
                                    <div class="col-md-12">


                                        <div class="myPlanForm">   
                                            <iframe id="testimonials_iframe"  onload="//iframeLoaded()" width="100%" scrolling="no" frameborder="0"  src="<?php echo env("CMS_URL"); ?>questionnaire"></iframe>

                                        </div>                     
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane <?php
                        if (!empty($tab) && $tab == "reimbur") {
                            echo "active";
                        }
                        ?>" id="reimbursment"><div class="tab-paneIn">
                                <div class="col-md-12">
                                    <div class="title-bg textleft">
                                        <h1>Reimbursements</h1>
                                    </div>
                                    <div class="myPlanForm">
                                        <?php
                                        if ($_SESSION['LOGINTYPE'] == 'CORPORATE') {
                                            $reimbursementList = getReimbursementList(@$_SESSION['empId'], @$_SESSION['policyNumber']);
                                        } else {
                                            $reimbursementList = getRetReimbursementList(@$_SESSION['customerId'], @$_SESSION['policyNumber']);
                                        }
                                        ?>
                                        <table class="responsive" width="100%">
                                            <tr>
                                                <th>S.No.</th>
                                                <th>Request No.</th>
                                                <th>Request Date</th>
                                                <th>Reason of Resubmission</th>
                                                <th>Status</th>
                                                <th class="brdrnone">Action</th>
                                            </tr>
                                            <tbody id="myTable" >
                                                <?php
                                                $a = 0;
                                                $k = 1;
                                                if (count(@$reimbursementList) > 0) {
                                                    while ($a < count(@$reimbursementList)) {
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $k; ?>.</td>
                                                            <td><?php echo stripslashes(@$reimbursementList[$a]['REQUESTID']); ?></td>
                                                            <td><?php echo stripslashes(@$reimbursementList[$a]['REQUESTDATE']); ?></td>
                                                            <td><?php echo stripslashes(@$reimbursementList[$a]['REASONFORRESUBMISSION']) ? stripslashes(@$reimbursementList[$a]['REASONFORRESUBMISSION']) : 'NA'; ?></td>
                                                            <td><?php echo stripslashes(@$reimbursementList[$a]['STATUS']); ?></td>
                                                            <td class="brdrnone"><?php if (@$reimbursementList[$a]['STATUS'] == 'OPEN' && @$reimbursementList[$a]['REASONFORRESUBMISSION'] != '') { ?>
                                                                    <a href="reimbursement_doc.php?requestId=<?php echo base64_encode(@$reimbursementList[$a]['REQUESTID']); ?>" class="view_nav">Re-Submit</a>
                                                        <?php
                                                        } else {
                                                            echo "NA";
                                                        }
                                                        ?>&nbsp; | <a data-toggle="modal" data-target="#exampleModal1"  href="view_reimbursement.php?id=<?php echo base64_encode(@$reimbursementList[$a]['REQUESTID']); ?>" class="view_nav example8">View</a></td>
                                                        </tr>
                                                        <?php
                                                        $a++;
                                                        $k++;
                                                    }
                                                } else {
                                                    ?>
                                                    <tr>
                                                        <td colspan="6" style="padding-left:312px !important;">No Reimbursement</td>
                                                    </tr>
<?php } ?>
                                            </tbody>
                                        </table>
<?php if (count(@$reimbursementList) > 0) { ?>
                                            <ul class="pagination pagination-lg pager" id="myPager"></ul>
<?php } ?>
                                    </div>                     
                                </div>
                                <div class="clearfix"></div></div>
                            <div class="colfullBot">
                                <div class="tab-paneIn">
                                    <div class="col-md-12">
                                        <div class="title-bg textleft">
                                            <h1>Claim Intimation</h1>
                                        </div>
                                        <form action="" method="post" name="DocRequestForm" id="DocRequestForm" enctype="multipart/form-data">
                                            <div class="grayBorder">
                                                <h2>User Details</h2>
                                                <div class="myPlanForm">
                                                    <div class="myPlanformBox30per myPlanLeft mrgnMyPlan">
                                                        <label>Client ID/Emp No. <span class="greenTxt">*</span></label>
                                                        <div class="inputBox inputBoxcolor">

                                                            <div class="inputBoxIn">
<?php
if ($_SESSION['LOGINTYPE'] == 'CORPORATE') {
    echo @$_SESSION['empId'] ? @$_SESSION['empId'] : 'NA';
} else {
    echo @$_SESSION['customerId'] ? @$_SESSION['customerId'] : 'NA';
}
?>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div></div>
                                                    <div class="myPlanformBox30per myPlanLeft">
                                                        <label>Name <span class="greenTxt">*</span></label>
                                                        <div class="inputBox inputBoxcolor">
                                                            <div class="inputBoxIn">
<?php echo @$_SESSION['policyHolderName'] ? @$_SESSION['policyHolderName'] : 'NA'; ?>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div></div>


                                                    <div class="myPlanformBox30per myPlanRight">
                                                        <label>Policy No <span class="greenTxt">*</span></label>
                                                        <div class="inputBox inputBoxcolor">
                                                            <div class="inputBoxIn">
<?php echo @$_SESSION['policyNumber'] ? @$_SESSION['policyNumber'] : 'NA'; ?>
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div></div>


                                                    <div class="myPlanformBox30per myPlanLeft mrgnMyPlan form-group">
                                                        <label>Email Id <span class="greenTxt">*</span></label>
                                                        <div class="inputBox">
                                                            <div class="inputBoxIn">
                                                                <input type="text" id="txtEmail" name="txtEmail" class="txtField"  value="" placeholder="Enter Email">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div></div>

                                                    <div class="myPlanformBox30per myPlanLeft">
                                                        <label>Contact No <span class="greenTxt">*</span></label>
                                                        <div class="inputBox">
                                                            <div class="inputBoxIn">
                                                                <input type="text" name="txtPhone" id="txtPhone" class="txtField" value="" placeholder="Enter Contact No">
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div></div>


                                                    <div class="myPlanformBox30per myPlanRight">
                                                        <label>Request <span class="greenTxt">*</span></label>
                                                        <div class="inputBox">
                                                            <div class="inputBoxIn">
                                                                <input type="text" name="txtEnquiry" id="txtEnquiry"  value="" class="txtField" placeholder="Enter Request" >
                                                            </div>
                                                        </div>
                                                        <div class="clearfix"></div></div>


                                                    <div class="clearfix"></div></div>

                                                <h2>Upload Docs</h2>
                                                <div class="myPlanForm">


                                                    <div class="uploadDocTitle">Doc Title <span class="greenTxt">*</span></div>
                                                    <div>
                                                        <div class="uploadDocFormBox">
                                                            <div class="leftCol45"><div class="inputBox forMobInput">
                                                                    <div class="inputBoxIn">
                                                                        <input type="text" name="docTitle1" id="docTitle1" AUTOCOMPLETE="OFF"  class="txtField" value="" placeholder="Enter Title" onFocus="this.placeholder = ''" onBlur="this.placeholder = 'Enter Title'">
                                                                    </div>
                                                                </div>
                                                                <input type="file" name="upload1" id="upload1" style="-moz-opacity:0 ; filter:alpha(opacity: 0); opacity: 0; position: absolute; margin-left:345px; width:2px;" onChange="$('#filename1').html(this.value)">

                                                              <!--<input type="file" name="upload1" id="upload1" style="display:none" onChange="$('#filename1').html(this.value)"> -->

                                                                <a class="cancel-btn right forMob" href="javascript:document.getElementById('upload1').click();">Browse &gt;</a>
                                                            </div>
                                                            <div class="rightCol55"><div class="inputBox">
                                                                    <div class="inputBoxIn" id="filename1">
                                                                        No file Selected
                                                                    </div>
                                                                </div>
                                                                <a class="plus-btn right" onclick="showClass('docdiv2');" href="javascript:void(0);">+</a></div>
                                                        </div>
                                                        <div class="uploadDocGrayTitle uploadDocGrayTitleMob">[*.jpg,*.xls,*.doc and *.pdf only]</div>
                                                    </div>

                                                    <div class="docdiv2" style="display:none;">
                                                        <div class="uploadDocFormBox">
                                                            <div class="leftCol45"><div class="inputBox forMobInput">
                                                                    <div class="inputBoxIn">
                                                                        <input type="text" name="docTitle2" id="docTitle2" maxlength="200" AUTOCOMPLETE="OFF"  class="txtField" placeholder="Enter Title" onFocus="this.placeholder = ''" onBlur="this.placeholder = 'Enter Title'" value="">
                                                                    </div>
                                                                </div>
                                                                <input type="file" name="upload2" id="upload2" style="-moz-opacity:0 ; filter:alpha(opacity: 0); opacity: 0; position: absolute; margin-left:345px; width:2px;" onChange="$('#filename2').html(this.value)">
                                                                <a class="cancel-btn right forMob" href="javascript:document.getElementById('upload2').click();">Browse &gt;</a>
                                                            </div>
                                                            <div class="rightCol55"><div class="inputBox">
                                                                    <div class="inputBoxIn" id="filename2">
                                                                        No file Selected
                                                                    </div>
                                                                </div>
                                                                <a class="plus-btn right"  onclick="showClass('docdiv3');" href="javascript:void(0);">+</a></div>
                                                        </div>
                                                        <div class="uploadDocGrayTitle uploadDocGrayTitleMob">[*.jpg,*.xls,*.doc and *.pdf only]</div>
                                                    </div>

                                                    <div class="docdiv3" style="display:none;">
                                                        <div class="uploadDocFormBox">
                                                            <div class="leftCol45"><div class="inputBox forMobInput">
                                                                    <div class="inputBoxIn">
                                                                        <input type="text" name="docTitle3" id="docTitle3" maxlength="200" AUTOCOMPLETE="OFF" class="txtField" placeholder="Enter Title" onFocus="this.placeholder = ''" onBlur="this.placeholder = 'Enter Title'" value="">

                                                                    </div>
                                                                </div>
                                                                <input type="file" name="upload3" id="upload3" style="-moz-opacity:0 ; filter:alpha(opacity: 0); opacity: 0; position: absolute; margin-left:345px; width:2px;" onChange="$('#filename3').html(this.value)">
                                                                <a class="cancel-btn right forMob" href="javascript:document.getElementById('upload3').click();">Browse &gt;</a>
                                                            </div>
                                                            <div class="rightCol55"><div class="inputBox">
                                                                    <div class="inputBoxIn" id="filename3" name="filename3">
                                                                        No file Selected
                                                                    </div>
                                                                </div>
                                                                <a class="plus-btn right"  onclick="showClass('docdiv4');" href="javascript:void(0);">+</a></div>
                                                        </div>
                                                        <div class="uploadDocGrayTitle uploadDocGrayTitleMob">[*.jpg,*.xls,*.doc and *.pdf only]</div>
                                                    </div>

                                                    <div class="docdiv4" style="display:none;">
                                                        <div class="uploadDocFormBox">
                                                            <div class="leftCol45"><div class="inputBox forMobInput">
                                                                    <div class="inputBoxIn">
                                                                        <input type="text" name="docTitle4" id="docTitle4" maxlength="200" AUTOCOMPLETE="OFF"  class="txtField" placeholder="Enter Title" onFocus="this.placeholder = ''" onBlur="this.placeholder = 'Enter Title'" value="">
                                                                    </div>
                                                                </div>
                                                                <input type="file" name="upload4" id="upload4" style="-moz-opacity:0 ; filter:alpha(opacity: 0); opacity: 0; position: absolute; margin-left:345px; width:2px;" onChange="$('#filename4').html(this.value)">
                                                                <a class="cancel-btn right forMob" href="javascript:document.getElementById('upload4').click();">Browse &gt;</a>
                                                            </div>
                                                            <div class="rightCol55"><div class="inputBox">
                                                                    <div class="inputBoxIn" id="filename4">
                                                                        No file Selected
                                                                    </div>
                                                                </div>
                                                                <a class="plus-btn right"  onclick="showClass('docdiv5');" href="javascript:void(0);">+</a></div>
                                                        </div>
                                                        <div class="uploadDocGrayTitle uploadDocGrayTitleMob">[*.jpg,*.xls,*.doc and *.pdf only]</div>
                                                    </div>

                                                    <div class="docdiv5" style="display:none;">
                                                        <div class="uploadDocFormBox">
                                                            <div class="leftCol45"><div class="inputBox forMobInput">
                                                                    <div class="inputBoxIn">
                                                                        <input type="text" name="docTitle5" id="docTitle5" maxlength="200" AUTOCOMPLETE="OFF"  class="txtField" placeholder="Enter Title" onFocus="this.placeholder = ''" onBlur="this.placeholder = 'Enter Title'" value="">
                                                                    </div>
                                                                </div>
                                                                <input type="file" name="upload5" id="upload5" style="-moz-opacity:0 ; filter:alpha(opacity: 0); opacity: 0; position: absolute; margin-left:345px; width:2px;" onChange="$('#filename5').html(this.value)">
                                                                <a class="cancel-btn right forMob" href="javascript:document.getElementById('upload5').click();">Browse &gt;</a>
                                                            </div>
                                                            <div class="rightCol55"><div class="inputBox">
                                                                    <div class="inputBoxIn" id="filename5">
                                                                        No file Selected
                                                                    </div>
                                                                </div>
                                                                <a class="plus-btn right" href="javascript:void(0);">+</a></div>
                                                        </div>
                                                        <div class="uploadDocGrayTitle uploadDocGrayTitleMob">[*.jpg,*.xls,*.doc and *.pdf only]</div>
                                                    </div>


                                                    <div class="clearfix"></div>

                                                </div>

                                                <div class="myPlanBtn">
                                                    <input type="hidden" value="<?php echo @$_SESSION['LOGINTYPE']; ?>" name="formtype" >
                                                    <a href="javascript:void(0);" onClick="$('#DocRequestForm').submit();" class="submit-btn">Submit ></a>
                                                </div> 
                                            </div>                     
                                        </form>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>            
                    </div>
                </div>
            </div>
            <div class="col-md-3">
<?php include("inc/inc.right.php"); ?>
            </div> 
        </div>
    </div>
</section>
<script>
    $(function () {
        $('.closeBox').click(function () {
            $('#exampleModal1').modal('hide');
        });
    });
</script>

<script type='text/javascript'>
    $(window).load(function () {
        $(function () {
            $(".dropdown-menu").on("click", "li", function (event) {
                console.log("You clicked the drop downs", event)
            })
        })
    });
</script>

<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <div class="model-header" id="exampleModalLabel">Reimbursement Doc List</div>
            </div>
            Please Wait....

        </div>
    </div>
</div>  
<?php include("inc/inc.ft.php"); ?>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/additional-methods.min.js" type="text/javascript"></script>
<script src="js/owl.carousel.js" type="text/javascript"></script>
<script type="text/javascript">
    function resizeIFrameToFitContent(iFrame) {

        iFrame.width = iFrame.contentWindow.document.body.scrollWidth;
        iFrame.height = iFrame.contentWindow.document.body.scrollHeight;
    }

    window.addEventListener('DOMContentReady', function (e) {

        var iFrame = document.getElementById('testimonials_iframe');
        resizeIFrameToFitContent(iFrame);

        // or, to resize all iframes:
        var iframes = document.querySelectorAll("iframe");
        for (var i = 0; i < iframes.length; i++) {
            resizeIFrameToFitContent(iframes[i]);
        }
    });

</script>
<script src="js/pagination.js"></script>






<script type="text/javascript">
    iFrameResize({
        minHeight: 600
    });
</script>