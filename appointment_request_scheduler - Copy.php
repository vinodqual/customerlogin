<?php /*****************************************************************************
* COPYRIGHT
* Copyright 2013 Catabatic Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: appointment_request_scheduler.php,v 1.0 2015/10/05 05:16:0 PM Amit Kumar Dubey Exp $
* $Author: Amit Kumar Dubey $
*
****************************************************************************/

include("inc/inc.hd.php");  			//include header file to show header on page
if(isset($_SESSION['response_error'])){
	$responseError  = $_SESSION['response_error'];
}else{
	$responseArray = $_SESSION['dataArray'];
}
$flag=0; 
$planId=sanitize_title(base64_decode(@$_REQUEST['id']));
$purchaseId=sanitize_title(base64_decode(@$_REQUEST['pId']));

if($_SESSION['LOGINTYPE']=='CORPORATE'){
	$employeeDetails=fetchcolumnListCond("EMAIL,MOBILENUMBER","CRMEMPLOYEELOGIN","WHERE LOGINID='".@$_SESSION['USERID']."'");
	$_SESSION['POLICYID']=$wellnessList[0]['POLICYID'];
	$planCount=checkPlanForEmployee(@$wellnessList[0]['POLICYID'],@$_SESSION['empNo'],@$_SESSION['clientNumber']);//function to fetch records
	$planDetails=get_particular_plan_details_for_policy($planId,@$wellnessList[0]['POLICYID']);// fetch plan details for plan id
		$memberList=array();

		if(@$planDetails[0]['PAYMENTOPTIONS']=="BUY"){
			$memberList=get_balance_member_list_for_appointment($purchaseId,$planId,@$_SESSION['COMPANYID']);//function to fetch records
		}else{
			$responseArray['purchaseId']=$purchaseId;
			$responseArray['planId']=$planId;
			$responseArray['companyId']=@$_SESSION['COMPANYID'];
			$responseArray['policyId']=@$wellnessList[0]['POLICYID'];
			$memberList=get_member_list_for_free_appointment(@$responseArray);
		}
		

} else {
	
	$planCount=checkPlanForRetEmployee($_SESSION['productId'],@$_SESSION['empNo'],@$_SESSION['customerId']);//function to fetch records
	$planDetails=get_particular_retplan_details_for_policy($planId,@$_SESSION['productId']);// fetch plan details for plan id
	
	$memberList=array();
	
	if(@$planDetails[0]['PAYMENTOPTIONS']=="BUY"){
		$memberList=get_balance_retmember_list_for_appointment($purchaseId,$planId,@$_SESSION['productId']);//function to fetch records
	}else{
		$responseArray['purchaseId']=$purchaseId;
		$responseArray['planId']=$planId;
		$responseArray['productId']=@$_SESSION['productId'];
		$responseArray['policyNumber']=@$_SESSION['policyNumber'];
		$memberList=get_retmember_list_for_free_appointment(@$responseArray);
		
	}
}
//echo count(@$planDetails); die;
if(count(@$planDetails)==0){
	echo "<script>window.location.href='health_checkup.php'</script>";
	exit;
}


$AddAppointment=sanitize_title(@$_POST['AddAppointment']);

if($_SESSION['LOGINTYPE']=='CORPORATE'){
	if($AddAppointment!='' && isset($AddAppointment)){
	$COMPANYID=$wellnessList[0]['COMPANYID'];
	$EMPLOYEEID=sanitize_title(@$_POST['EMPLOYEEID']);
	$MOBILENO=sanitize_title(@$_POST['MOBILENO']);
	$address=sanitize_data(@$_POST['address']);
	$state=sanitize_title(@$_POST['state']);
	$city=sanitize_title(@$_POST['city']);
	$stateName=fetchListCondsWithColumn("STATENAME","LWSTATEMASTER"," where STATEID=$state");
	$cityName=fetchListCondsWithColumn("CITYNAME","LWCITYMASTER"," where CITYID=$city and STATEID=$state");
	if(isset($stateName) && isset($cityName)) {
		$location=$stateName[0]['STATENAME'].",".$cityName[0]['CITYNAME'];
	}
	$EMAILID=sanitize_title(@$_POST['EMAILID']);
	$changeprofile=sanitize_data($_POST['changeprofile']);
	$slotid=sanitize_data(@$_POST['slotid']);
	$slotid=addslashes(@$slotid);
	$centerName=@$_POST['centerName'];
	$centerName=addslashes(@$centerName);
	$selectedcenterid=@$_POST['selectedcenterid'];
	$selectedcenterid=addslashes(@$selectedcenterid);

	$date1=sanitize_data(@$_POST['date1']);
	$date1=addslashes(@$date1);
	if(isset($date1) && !empty($date1)){
		$date1=date('d-M-Y',strtotime(@$date1));
	}
	//$date1=strtoupper(@$date1);
	$noofappointment=sanitize_title(@$_POST['noofappointment']);
	$noofappointment=addslashes(@$noofappointment);
	$noofappointment=$noofappointment+1;

	$maxappointment=sanitize_title(@$_POST['maxappointment']);
	$maxappointment=addslashes(@$maxappointment);

	$timeslot='slot1';
	if($timeslot=='slot1'){
		$slotmappingid=sanitize_title(@$_POST['timeslot1']);
		$slotmappingid=addslashes(@$slotmappingid);
	}
	if($timeslot=='slot2'){
		$slotmappingid=sanitize_title(@$_POST['timeslot2']);
		$slotmappingid=addslashes(@$slotmappingid);
	}
	$date2=sanitize_data(@$_POST['date2']);
	$date2=addslashes(@$date2);
	if(isset($date2) && !empty($date2)){
		$date2=date('d-M-Y',strtotime(@$date2));
	}
	$policyNumber=@$_SESSION['policyNumber'];
	$entryTime=date('d-m-Y H:i');
	$ipaddress=$_SERVER['REMOTE_ADDR'];
	$existPlan=fetchExistcanceledPlan('CRMCOMPANYPOSTAPPOINTMENT','EMPLOYEEID',@$EMPLOYEEID,'HEALTHCHECKUPID',@$planId);
	if($COMPANYID>0 && trim($EMPLOYEEID)!='' && trim($policyNumber)!='' && trim($centerName)!='' && trim($date1)!='' && trim($noofappointment)!='') {
	$planName=@$planDetails[0]['PLANNAME']?$planDetails[0]['PLANNAME']:$planDetails[0]['PLANNAME2'];
	if(count($memberList) > 0){		
		
			//check condition to insert records
	for($m1=0;$m1<count($_POST['member']);$m1++){
	
			$memberSel=sanitize_title(@$_POST['member'][$m1]);
			$firstName=sanitize_title(@$_POST['name'][$memberSel]);
			$NAME=sanitize_title(@$_POST['name'][$memberSel]);
			$AGE=sanitize_title(@$_POST['age'][$memberSel]);
			$MEMBERRELATION=sanitize_title(@$_POST['memberrelation'][$memberSel]);
			$CUSTOMERIDS=sanitize_title(@$_POST['retcustomerId'][$memberSel]);
			$MEMBERID=sanitize_title(@$_POST['memberId'][$memberSel]);

		 	 $sql="INSERT INTO CRMCOMPANYPOSTAPPOINTMENT (APPOINTMENTID,COMPANYID,CUSTOMERID,FIRSTNAME,DOB,EMAILID,MOBILENO,EMPLOYEELOCATION,CENTERNAME,DATE1,TIMESLOT1,TIMESLOT1AM,DATE2,TIMESLOT2,TIMESLOT2AM,SELECTEDSLOT,EMPLOYEEID,POLICYNUMBER,STATUS,REPORTSTATUS,REPORTFILE,REPORTUPLOADEDBY,CREATEDON,CREATEDBY,UPDATEDON,HEALTHCHECKUPID,PURCHASEID,MEMBERID,PLANTYPE,HEALTHCHECKUPTYPE,HEALTHCHECKUPPLAN,REFERENCENUMBER,SLOTID,SLOTMAPPINGID,CENTERID,CITYID,STATEID,ADDRESS,IPADDRESS) values(CRMCOMPANYPREAPPOINTMENT_SEQ.nextval,'".@$COMPANYID."','".@$CUSTOMERIDS."','".@$NAME."','".@$AGE."','".@$EMAILID."','".@$MOBILENO."','".@$location."','".@$centerName."','".@$date1."','".@$timeslot1."','".@$timeslot1am."','".@$date2."','".@$timeslot2."','".@$timeslot2am."','".@$timeslot."','".@$EMPLOYEEID."','".@$policyNumber."','PENDING','NOTUPLOADED','".@$reportfile."','".@$_SESSION['userName']."','".@$entryTime."','".@$_SESSION['userName']."','".@$entryTime."','".@$planId."','".@$purchaseId."','".@$MEMBERID."','GENERAL','GENERAL','".@$planName."','".@$refrencenum."','".@$slotid."','".@$slotmappingid."','".@$selectedcenterid."','".@$city."','".@$state."','".@$address."','".$ipaddress."') "; 

		
			$stdid = @oci_parse($conn, $sql);
			$r = @oci_execute($stdid);			
		$check1 = @oci_parse($conn, 'SELECT CRMCOMPANYPREAPPOINTMENT_SEQ.currval FROM DUAL');
		@oci_execute($check1);
		$res=@oci_fetch_assoc($check1);
		$rowid=@$res['CURRVAL'];

		$sql2="INSERT INTO CRMCOMPANYAPPHISTORY(ID,APPOINTMENTID,STATUS,CREATEDON,CREATEDBY) VALUES(CRMCOMPANYAPPHISTORY_SEQ.nextval,".$rowid.",'PENDING','".@$entryTime."','".@$_SESSION['userName']."') ";  //query to update status based on ID
		$stdid2 = @oci_parse($conn, $sql2);
		$r2 = @oci_execute($stdid2);
			$appfull='';
			if($noofappointment==$maxappointment){
				$appfull="ISAVAIL='NO' ,";
			}
	  		$sql3="UPDATE  CRMCALENDERSLOTMAPPING SET NOOFAPPOINTMENT='".@$noofappointment."',$appfull UPDATEDON='".@$entryTime."',UPDATEDBY='".@$_SESSION['userName']."' WHERE ID='".@$slotmappingid."'";	

				//query to update records
			$stdid3 = @oci_parse($conn, $sql3);
			$r3 = @oci_execute($stdid3);
			
		}
		
		if($changeprofile=='YES'){
			$queryCheckExist=fetchcolumnListCond("EMAIL","CRMEMPLOYEELOGIN","WHERE EMAIL='".@$EMAILID."' AND STATUS='ACTIVE' ");
			$emailUpdate='';
			
			if(count($queryCheckExist) == 0){
				 $emailUpdate=" EMAIL='".$EMAILID."' , ";	
			}
				 $sql4="UPDATE  CRMEMPLOYEELOGIN SET ".@$emailUpdate." MOBILENUMBER='".@$MOBILENO."',IPADDRESS='".@$_SERVER['REMOTE_ADDR']."', UPDATEDON='".@$entryTime."',UPDATEDBY='".@$_SESSION['userName']."' WHERE LOGINID='".@$_SESSION['USERID']."'";	
				$stdid4 = @oci_parse($conn, $sql4);
				$r4 = @oci_execute($stdid4);
			
		}
		
		$from='';
		$subject='';
		$message='';
		$tmslot1=$timeslot[0]." ".@$timeslot[1];
		$tmslot2=@$timeslot2[0]." ".@$timeslot2[1];
		$slot=fetchSlotListByCond(" WHERE CRMCALENDERSLOTMAPPING.SLOTID=".$slotid." AND CRMCALENDERSLOTMAPPING.ID=".$slotmappingid."");
		$time=@$slot[0]['STARTTIMENAME'];

		 SendMailToEmp($EMAILID,$centerName,$location,$date1,$time,@$appointmentDate2,@$tmslot2);	//function to send email     
		 SendMail($EMAILID,$from,$subject,$message,'HR');		//function to send email
		 
			echo '<script type="text/javascript">';
			echo 'window.location.href="existing_appointments.php?message=Inserted Successfull";';
			echo '</script>';
			echo '<noscript>';
			echo '<meta http-equiv="refresh" content="0;url=existing_appointments.php?message=Inserted Successfull" />';
			echo '</noscript>';
			//	echo '<meta http-equiv="refresh" content="0;URL="existing_appointments.php?message=Inserted Successfull" />';
			//header("Location: existing_appointments.php?message=Inserted Successfully");//locate to existing_appointments.php
			exit;		
		}else{
			echo '<script type="text/javascript">';
			echo 'window.location.href="existing_appointments.php";';
			echo '</script>';
			echo '<noscript>';
			echo '<meta http-equiv="refresh" content="0;url=existing_appointments.php" />';
			echo '</noscript>';
			//header("Location: existing_appointments.php");//locate to existing_appointments.php
			exit;}
		}else{
			echo '<script type="text/javascript">';
			echo 'window.location.href="existing_appointments.php?message=Please try again";';
			echo '</script>';
			echo '<noscript>';
			echo '<meta http-equiv="refresh" content="0;url=existing_appointments.php?message=Please try again" />';
			echo '</noscript>';
			//header("Location: existing_appointments.php?message=Please try again");//locate to existing_appointments.php
			exit;
		}
	}

} else {
	if($AddAppointment!='' && isset($AddAppointment)){

	$COMPANYID=@$wellnessList[0]['COMPANYID'];
	$EMPLOYEEID=sanitize_title(@$_POST['EMPLOYEEID']);
	$MOBILENO=sanitize_title(@$_POST['MOBILENO']);
	$changeprofile=sanitize_data($_POST['changeprofile']);
	$address=sanitize_data(@$_POST['address']);
	$state=sanitize_title(@$_POST['state']);
	$city=sanitize_title(@$_POST['city']);
	$stateName=fetchListCondsWithColumn("STATENAME","LWSTATEMASTER"," where STATEID=$state");
	$cityName=fetchListCondsWithColumn("CITYNAME","LWCITYMASTER"," where CITYID=$city and STATEID=$state");
	$productDetail=fetchListCondsWithColumn("TITLE","CRMRETAILPRODUCT"," WHERE PRODUCTID=".@$_SESSION['productId']."");
	//$healthcheckupPlans=sanitize_title(@$_POST['planCenter']);	
	if(isset($stateName) && isset($cityName)) {
		$location=$stateName[0]['STATENAME'].",".$cityName[0]['CITYNAME'];
	}
	$EMAILID=sanitize_title(@$_POST['EMAILID']);
	$slotid=sanitize_data(@$_POST['slotid']);
	$slotid=addslashes(@$slotid);
	$centerName=@$_POST['centerName'];
	$centerName=addslashes(@$centerName);
	$selectedcenterid=@$_POST['selectedcenterid'];
	$selectedcenterid=addslashes(@$selectedcenterid);

	$date1=sanitize_data(@$_POST['date1']);
	$date1=addslashes(@$date1);
	if(isset($date1) && !empty($date1)){
		$date1=date('d-M-Y',strtotime(@$date1));
	}
	//$date1=strtoupper(@$date1);
	$noofappointment=sanitize_title(@$_POST['noofappointment']);
	$noofappointment=addslashes(@$noofappointment);
	$noofappointment=$noofappointment+1;

	$maxappointment=sanitize_title(@$_POST['maxappointment']);
	$maxappointment=addslashes(@$maxappointment);

	$timeslot='slot1';
	if($timeslot=='slot1'){
		$slotmappingid=sanitize_title(@$_POST['timeslot1']);
		$slotmappingid=addslashes(@$slotmappingid);
	}
	if($timeslot=='slot2'){
		$slotmappingid=sanitize_title(@$_POST['timeslot2']);
		$slotmappingid=addslashes(@$slotmappingid);
	}
	$date2=sanitize_data(@$_POST['date2']);
	$date2=addslashes(@$date2);
	if(isset($date2) && !empty($date2)){
		$date2=date('d-M-Y',strtotime(@$date2));
	}


	$entryTime=date('d-m-Y H:i');
	$ipaddress=$_SERVER['REMOTE_ADDR'];
	$policyNumber=@$_SESSION['policyNumber'];
	$productId=@$_SESSION['productId'];
	$productname=$productDetail[0]['TITLE'];
	$status='PENDING';
   if(trim($EMPLOYEEID)!='' && trim($policyNumber)!='' && trim($centerName)!='' && trim($date1)!='' && trim($noofappointment)!='' && trim($productId)!='' ) { 
	$planName=@$planDetails[0]['PLANNAME']?$planDetails[0]['PLANNAME']:$planDetails[0]['PLANNAME2'];
	if(count($memberList) > 0){		

			//check condition to insert records
		for($m1=0;$m1<count($_POST['member']);$m1++){
			$memberSel=sanitize_title(@$_POST['member'][$m1]);
			$firstName=sanitize_title(@$_POST['name'][$memberSel]);
			$NAME=sanitize_title(@$_POST['name'][$memberSel]);
			$AGE=sanitize_title(@$_POST['age'][$memberSel]);
			$MEMBERRELATION=sanitize_title(@$_POST['memberrelation'][$memberSel]);
			$CUSTOMERIDS=sanitize_title(@$_POST['retcustomerId'][$memberSel]);
			$alldata=@$_POST['memberIds'][$memberSel];
			$memberData=explode(':',$alldata);
			$memberGender=$memberData[0];
			$memberSi=str_replace(",","",number_format(@$memberData[4]));

		 	 $sql="INSERT INTO CRMRETAILPOSTAPPOINTMENT (APPOINTMENTID,PRODUCTID,PRODUCTNAME,CUSTOMERID,FIRSTNAME,DOB,EMAILID,MOBILENO,EMPLOYEELOCATION,CENTERNAME,DATE1,TIMESLOT1,TIMESLOT1AM,DATE2,TIMESLOT2,TIMESLOT2AM,SELECTEDSLOT,EMPLOYEEID,POLICYNUMBER,STATUS,REPORTSTATUS,REPORTFILE,REPORTUPLOADEDBY,CREATEDON,CREATEDBY,UPDATEDON,HEALTHCHECKUPID,PURCHASEID,MEMBERID,PLANTYPE,HEALTHCHECKUPTYPE,HEALTHCHECKUPPLAN,REFERENCENUMBER,SLOTID,SLOTMAPPINGID,CENTERID,CITYID,STATEID,ADDRESS,IPADDRESS,MEMBERRELATION,GENDER,SI,MMBRWEBSERVICEDATA) values(CRMRETAILPOSTAPPOINTMENT_SEQ.nextval,'".@$productId."','".@$productname."','".@$CUSTOMERIDS."','".@$NAME."','".@$AGE."','".@$EMAILID."','".@$MOBILENO."','".@$location."','".@$centerName."','".@$date1."','".@$timeslot1."','".@$timeslot1am."','".@$date2."','".@$timeslot2."','".@$timeslot2am."','".@$timeslot."','".@$EMPLOYEEID."','".@$policyNumber."','".@$status."','NOTUPLOADED','".@$reportfile."','".@$_SESSION['userName']."','".@$entryTime."','".@$_SESSION['userName']."','".@$entryTime."','".@$planId."','".@$purchaseId."','".@$MEMBERID."','GENERAL','GENERAL','".@$planName."','".@$refrencenum."','".@$slotid."','".@$slotmappingid."','".@$selectedcenterid."','".@$city."','".@$state."','".@$address."','".@$ipaddress."','".@$MEMBERRELATION."','".@$memberGender."','".@$memberSi."','".$alldata."') "; 
			
			$stdid = @oci_parse($conn, $sql);
			$r = @oci_execute($stdid);


		$check1 = @oci_parse($conn, 'SELECT CRMRETAILPOSTAPPOINTMENT_SEQ.currval FROM DUAL');
		@oci_execute($check1);
		$res=@oci_fetch_assoc($check1);
		$rowid=@$res['CURRVAL'];

		$sql2="INSERT INTO CRMRETAILAPPHISTORY(ID,APPOINTMENTID,STATUS,CREATEDON,CREATEDBY) VALUES(CRMRETAILAPPHISTORY_SEQ.nextval,".$rowid.",'".$status."','".@$entryTime."','".@$_SESSION['userName']."') ";  //query to update status based on ID
		$stdid2 = @oci_parse($conn, $sql2);
		$r2 = @oci_execute($stdid2);
			$appfull='';
			if($noofappointment==$maxappointment){
				$appfull="ISAVAIL='NO' ,";
			}
	  		$sql3="UPDATE  CRMCALENDERSLOTMAPPING SET NOOFAPPOINTMENT='".@$noofappointment."',$appfull UPDATEDON='".@$entryTime."',UPDATEDBY='".@$_SESSION['userName']."' WHERE ID='".@$slotmappingid."'";	

			$stdid3 = @oci_parse($conn, $sql3);
			$r3 = @oci_execute($stdid3);
			
			//FunSendMail($email,$status,'POSTAPPOINTMENTHR');
			//FunSendChangeAppMail(@$email,@$centerName,@$location,@$location,@$date1,$date2,$timeslot1,@$timeslot1am,@$timeslot2am,$timeslot2,'POSTAPPOINTMENT_NEW');
		}
		if($changeprofile=='YES'){
			$queryCheckExist=fetchcolumnListCond("EMAILID","RETUSERMASTER","WHERE EMAILID='".@$EMAILID."' AND STATUS='ACTIVE' ");
			$emailUpdate='';
			if(count($queryCheckExist) == 0){
				 $emailUpdate=" EMAILID='".$EMAILID."' , ";	
			}
				 $sql4="UPDATE  RETUSERMASTER SET ".@$emailUpdate." CONTACTNUM='".@$MOBILENO."',IPADDRESS='".@$_SERVER['REMOTE_ADDR']."', UPDATEDON='".@$entryTime."',UPDATEDBY='".@$_SESSION['userName']."' WHERE USERID='".@$_SESSION['USERID']."'";	
				$stdid4 = @oci_parse($conn, $sql4);
				$r4 = @oci_execute($stdid4);
			
		}
		
		$from='';
		$subject='';
		$message='';
			$slot=fetchSlotListByCond(" WHERE CRMCALENDERSLOTMAPPING.SLOTID=".$slotid." AND CRMCALENDERSLOTMAPPING.ID=".$slotmappingid."");
			
			$time=@$slot[0]['STARTTIMENAME'];

		
		 SendMailToEmpRetail(@$EMAILID,@$centerName,@$location,@$date1,@$time,@$appointmentDate2,@$tmslot2);	//function to send email        
		 SendMailRetail($EMAILID,$from,$subject,$message,'HR');		//function to send email
		 
			echo '<script type="text/javascript">';
			echo 'window.location.href="existing_appointments.php?tab=ZXhpc3RpbmdhcHA=&message=Inserted Successfull";';
			echo '</script>';
			echo '<noscript>';
			echo '<meta http-equiv="refresh" content="0;url=existing_appointments.php?tab=ZXhpc3RpbmdhcHA=&message=Inserted Successfull" />';
			echo '</noscript>';
			//	echo '<meta http-equiv="refresh" content="0;URL="existing_appointments.php?message=Inserted Successfull" />';
			//header("Location: existing_appointments.php?message=Inserted Successfully");//locate to existing_appointments.php
			exit;		
		}else{
			echo '<script type="text/javascript">';
			echo 'window.location.href="existing_appointments.php?tab=ZXhpc3RpbmdhcHA=";';
			echo '</script>';
			echo '<noscript>';
			echo '<meta http-equiv="refresh" content="0;url=existing_appointments.php?tab=ZXhpc3RpbmdhcHA=" />';
			echo '</noscript>';
			//header("Location: existing_appointments.php");//locate to existing_appointments.php
			exit;}
		}else{ 
			echo '<script type="text/javascript">';
			echo 'window.location.href="existing_appointments.php?message=Please try again";';
			echo '</script>';
			echo '<noscript>';
			echo '<meta http-equiv="refresh" content="0;url=existing_appointments.php?message=Please try again" />';
			echo '</noscript>';
			//header("Location: existing_appointments.php?message=Please try again");//locate to existing_appointments.php
			exit;
		}
	}
}

	//printr($_SESSION);
if($_SESSION['LOGINTYPE']=='RETAIL'){ 
	$policyExpirydate=strtotime(trim(@$_SESSION['POLICYENDDATE']));
	$policyExpiredplus10Days=date('d-M-Y',strtotime("+10 day", $policyExpirydate));	
}
?>
<link rel="stylesheet" href="css/jquery-ui.css" />
<script>
function getCenterVal(currentval){
var splitValues=currentval.split(':');
	$('#center_autocomp').val(splitValues[0]);
	$('#centerName2').val(1);
	$('#selectedcenterid').val(splitValues[1]);
}
function tests(){
var stateId=$("#state").val();
var cityId=$("#cityId").val();
if(cityId=='' || stateId==''){
	alert('state or city cannot be empty');
	$("#state").focus();
	return false;
}
  var planId=$.trim($("#planId").val());
	$("#center_autocomp").autocomplete({
		source: function(request, response) {
			$.ajax({
			   url: 'ajax/getAutocompleteResRet.php',
				dataType: "json",
				data: {
					q : request.term,
					policyId : $("#policyId").val(),
					compId : $("#company").val(),
					planId : planId,
					state : stateId,
					city : cityId,
				},
				success: function(data) {
			 
				response( $.map( data, function( val,key ) {
			var val2=val.split(":");	
				  return {
					label: val2[0],
					value: val2[0],
					values: val2[1]
				  }
				}));
			  }
			});
		},
		select:function(event,ui){
		//alert(console.log(ui));
		$("#centerName2").val(1);
		$("#selectedcenterid").val(ui.item.values);
		$("#centerId").val(ui.item.label+':'+ui.item.values);
		$("#center_autocomp").val(ui.item.label);
		},
		change: function( event, ui ) {
				//$( "#centerName2" ).val(1);
		  },
		minLength : 3,
	   
	});

}
	$(document).ready(function(){
			$( "#datepicker1" ).datepicker({
					changeMonth: true,
					changeYear: true,
					dateFormat: 'dd-M-yy',
					minDate:3,
					maxDate:'<?php echo $policyExpiredplus10Days?$policyExpiredplus10Days:''; ?>',
					
					onSelect:function(selected){
						$.ajax({
							type:'post',
							url:'ajax/get_data.php',
							data:{'type':'GETSLOTBYDATE','datevalue':selected,'centerId':$("#selectedcenterid").val()},
							beforeSend:function() {
							//$(".content").html('<div id="SuccessMsg" >Please Wait...</div>');
							},
							success:function(resp){
								//if($('.radio_1').is(':checked')) {
									$("#timeslotdata1").html(resp);
								//}
							}
						});

					},
				});
			$( "#datepicker2" ).datepicker({
					changeMonth: true,
					changeYear: true,
					dateFormat: 'dd-M-yy',
					minDate:3,
					onSelect:function(selected){
						$.ajax({
							type:'post',
							url:'ajax/get_data.php',
							data:{'type':'GETSLOTBYDATE2','datevalue':selected},
							beforeSend:function() {
							//$(".content").html('<div id="SuccessMsg" >Please Wait...</div>');
							},
							success:function(resp){
								$("#timeslotdata2").html(resp);
								/*if($('.radio_2').is(':checked')) {
									$("#timeslotdata2").html(resp);
								} else {
								$("#datepicker2").val('');
								alert('First You have to select Time Slot 2');
								
								}*/
							}
						});

					},
				});
	});			

</script>

<section id="middleContainer">
  <div class="container-fluid">
    <div class="middlebox">
      <div class="col-md-9">
        <div class="middlebox-left">
           <?php include('inc/inc.healthplan_tab.php'); ?>
          <div class="tab-content">
            <div class="tab-pane" id="buyPlan">
            
            <div class="tab-paneIn">
              <div class="col-md-6">
                
              </div>
              <div class="col-md-12 topMrgn">
              	res1
              </div>
              <div class="clearfix"></div>
              </div>
            </div>
            <div class="tab-pane" id="existingAppointments"><div class="tab-paneIn">2</div></div>
            <div class="tab-pane active" id="myPlans">
            	<div class="tab-paneIn" id="myplanForm">
               	 <div class="title-bg textleft">
                <h1>Appointment Request Scheduler (<?php echo @$planDetails[0]['PLANNAME']?$planDetails[0]['PLANNAME']:$planDetails[0]['PLANNAME2']; ?>)</h1>
                </div>
                <form name="appointmentForm" id="appointmentForm" method="post" >
                 <div class="grayBorder">
                	<h2>User Details</h2>
                    <div class="myPlanForm">
                    
                    	<div class="myPlanformBox myPlanLeft">
                        	<label>Emp Id / Client Id</label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                            	  <input type="text" name="EMPLOYEEID" class="email_f txtfield_app txtField" id="EMPLOYEEID" readonly="yes" value="<?php echo ($_SESSION['LOGINTYPE']=='CORPORATE')?@$_SESSION['empId']:@$_SESSION['customerId'];?>">
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        <div class="myPlanformBox myPlanRight">
                        	<label>Email Id <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
					              <input type="text" name="EMAILID" id="EMAILID" value="<?php echo ($_SESSION['LOGINTYPE']=='CORPORATE')?@$employeeDetails[0]['EMAIL']?@$employeeDetails[0]['EMAIL']:@$_SESSION['userName']:trim(@$_SESSION['employeeEmail']); ?>" class="email_f txtfield_app txtField" maxlength="255" AUTOCOMPLETE="OFF" />
                                	
                                </div>
                            </div>
                        <div class="clearfix"></div></div>
                        <div class="myPlanformBox myPlanLeft">
                        	<label>Mobile <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
	                               <input type="text" name="MOBILENO" id="MOBILENO" value="<?php echo @$employeeDetails[0]['MOBILENUMBER']?@$employeeDetails[0]['MOBILENUMBER']:trim(@$_SESSION['employeePhone']); ?>" class="email_f txtfield_app txtField"  maxlength="10" AUTOCOMPLETE="OFF" onKeyPress="return kp_numeric(event)" />
                                   <input type="hidden" name="emailcheck" id="emailcheck" value="<?php echo ($_SESSION['LOGINTYPE']=='CORPORATE')?@$_SESSION['userName']:trim(@$_SESSION['employeeEmail']); ?>" />
                                   <input type="hidden" name="mobilecheck" id="mobilecheck" value="<?php echo @$employeeDetails[0]['MOBILENUMBER']?@$employeeDetails[0]['MOBILENUMBER']:trim(@$_SESSION['employeePhone']); ?>" />
                                   <input type="hidden" name="changeprofile" id="changeprofile" value="NO" />
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        <div class="myPlanformBox myPlanRight">
                        	<label>Address </label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                                	<input type="text" name="address" id="address" class="txtField" value="">
                                </div>
                            </div>
                        <div class="clearfix"></div></div>
                    <div class="clearfix"></div></div>
                    <h2>Location</h2>
                    <div class="myPlanForm">
                    	<div class="myPlanformBox myPlanLeft">
                        	<label>State <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn dropBg">
                            	  <select  id="state" name="state" onChange="getRetCity(this.value);">
                                    <option value="">Select state</option>
                                    <?php
									if($_SESSION['LOGINTYPE']=='CORPORATE'){
									 $state_list=GetStateWhereCenterExist(@$_SESSION['COMPANYID'],@$wellnessList[0]['POLICYID'],$planId);
									} else {
									 $state_list=GetRetStateWhereCenterExist(@$_SESSION['productId'],@$_SESSION['policyNumber'],$planId);
									}
										
										$i=0; 
										while($i < count($state_list)){ ?>
                                    <option value="<?php echo @$state_list[$i]['STATEID'];?>"><?php echo @$state_list[$i]['STATENAME'];?></option>
                                    <?php  $i++;  }?>
                          	    </select>
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        <div class="myPlanformBox myPlanRight">
                        	<label>City <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn dropBg" id="city">
                            	  <select id="cityId" name="city"  onChange="getCityName(this.value);">
                                  	<option value="">Select City</option>
                          	    </select>
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        <div class="myPlanformBox myPlanLeft">
                        	<label>Locality/Center</label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                            	  <input type="text" id="center_autocomp" maxlength="255" name="centerName"   class="txtField" value="" onkeyup="return tests();">
                                  <input type="hidden"  name="centerName2" id="centerName2" value="" />
                                  <input type="hidden" name="planCenter" id="planId" value="<?php echo @$planId; ?>" >
                                  <input type="hidden" name="selectedcenterid" id="selectedcenterid" class="selectedcenterid" value="" /> 
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        <div class="myPlanformBox myPlanRight">
                        	<label>Center <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn dropBg" id="center">
                            	  <select id="centerId" name="centerId">
                                  	<option value="">Select Center</option>
                          	    </select>
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                    <div class="clearfix"></div></div>
                    <h2>Appointment Date / Time</h2>
                    <div class="myPlanForm">
                    	<div class="myPlanformBox myPlanLeft">
                        	<label>Date <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn dateBg">
                            	  <input readonly id="datepicker1" name="date1" type="text" class="txtField" value="<?php $da= strtotime(@$array_tblsite[0]['EFFECTIVEDATE']); if(!empty($da)) { echo date('d-m-Y',$da); } ?>">
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        <div class="myPlanformBox myPlanRight">
                        	<label>Time Slot</label>
                            <div class="inputBox">
                            	<div class="inputBoxIn dropBg" id="timeslotdata1">
                                    <select id="timeslot1" class="txtfield_new update_timeslot1" name="timeslot1">
                                        <option value="">Select Slot</option>
                                    </select> 
                            	</div>
                                 <input type="hidden" name="pendingapp" id="pendingapp" value="0" />
                             <input type="hidden" name="slotid" id="slotid" value="" />
                             <input type="hidden" name="maxappointment" id="maxappointment" value="" />
                             <input type="hidden" name="noofappointment" id="noofappointment" value="" />
                            </div>
                        <div class="clearfix"></div></div>
                        
                        
                        <div class="myPlanformBox myPlanLeft" style="display:none;" >
                        	<label>Date 2 <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn dateBg">
                            	  <input type="text"  readonly id="datepicker2" name="date2"  class="txtField" value="<?php $da= strtotime(@$array_tblsite[0]['EFFECTIVEDATE']); if(!empty($da)) { echo date('d-m-Y',$da); } ?>">
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        <div class="myPlanformBox myPlanRight"  style="display:none;">
                        	<label>Time Slot 2 <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn dropBg" id="timeslotdata2">
                                    <select id="timeslot2" class="txtfield_new update_timeslot2" name="timeslot2">
                                        <option value="">Select Slot</option>
                                    </select> 
                            	</div>
                            
                            </div>
                        <div class="clearfix"></div></div>
                        
                        
                    <div class="clearfix"></div></div>
                    
		         <?php  if(count($memberList)>0){?>
                    
                    <h2>Appointment Date / Time</h2>
                    
                    <div class="myPlanForm membertypeerror">
                    
                       <table class="responsive" width="100%" id="membererror">
                          <tr>
                            <th>Add</th>
                            <th>Gender</th>
                            <th>Relation</th>
                            <th>Name</th>
                            <th>Age</th>
                          </tr>
            	 <?php $red=1; for($m=0;$m<count($memberList);$m++){ 
				 			$allvalues=trim($response['BGEN-GENDER']['#text']).":".$relationArray[trim($response['BGEN-DPNTTYP']['#text'])].":".trim($response['BGEN-MEMNAME']['#text']).":".trim($response['BGEN-AGE']['#text']).":".trim($response['BGEN-ZBALCURR']['#text']).":".trim($response['BGEN-CLNTNM']['#text']).":".trim($policyNumber);

				 ?>
                  <tr>
                    <td><div class="insuredQuestioncheckBox">
                        <div class="insuredQuestioncheckRight">
                          <div class="insuredQuestioncheck">
                          <input type="checkbox" name="member[]" value="<?php echo $m;?>" class="memberc"  id="test<?php echo @$red; ?>" />
                            <label for="test<?php echo @$red; ?>"></label> 
                              <input type="hidden" name="memberrelation[]" value="<?php echo trim(@$memberList[$m]['RELATION']); ?>" />
                             <input type="hidden" name="retcustomerId[]" value="<?php echo trim(@$memberList[$m]['CUSTOMERID'])?trim(@$memberList[$m]['CUSTOMERID']):trim(@$memberList[$m]['CLIENTID']); ?>" />
                           <input type="hidden" name="memberId[]" value="<?php echo @$memberList[$m]['MEMBERID'];?>" />
                           <input type="hidden" name="memberIds[]" value="<?php echo @$memberList[$m]['ALLDATA'];?>" />
                           </div>
                        </div>
                      </div></td>
                    <td><?php echo @$memberList[$m]['GENDER'];?><input name="gender[]" readonly="yes" type="hidden" maxlength="40" class="email_f txtfield_gender" id="textfield" value="<?php echo @$memberList[$m]['GENDER'];?>"></td>
                    <td><?php echo ucwords(@$relationArray[trim(@$memberList[$m]['RELATION'])]);?> <input name="relationshipType[]"  readonly="yes" type="hidden" maxlength="100" class="email_f txtfield_app" id="textfield" value="<?php echo ucwords(@$relationArray[trim(@$memberList[$m]['RELATION'])]);?>" ></td>
                    <td><strong class="greenGeneralTxt"><?php echo ucwords(strtolower(trim(@$memberList[$m]['NAME'])));?> <input name="name[]" readonly="yes" type="hidden" maxlength="3" class="email_f txtfield_app" id="textfield" value="<?php echo ucwords(strtolower(trim(@$memberList[$m]['NAME'])));?>" /></strong></td>
                    <td><?php echo ucwords(strtolower(trim(@$memberList[$m]['AGE'])));?><input name="age[]" readonly="yes" type="hidden" maxlength="3" class="email_f txtfield_gender" id="textfield"  value="<?php echo ucwords(strtolower(trim(@$memberList[$m]['AGE'])));?>" /></td>
                  </tr>
                <?php $red++; } ?>
                </table>
                    </div>
                    
                 		<?php }else{ 
					?>
					<table width="100%" border="0" cellspacing="0" cellpadding="0" class="fl">
					<?php echo "<tr><td colspan='5' align='center' height='40'><b style='padding-left:33px;'>You can't schedule appointment for this plan. You have been already requested appointment for this plan</b></td></tr>"; ?>
					</table>
					
					</form>
					<?php
					}  if(count($memberList)>0){?>
					
                    
                    <div class="myPlanBtn">
                        <input type="hidden" name="companyId" value="<?php echo @$compId; ?>" />
                        <input type="hidden" name="policyId" value="" class="policyId" />
                        <input type="hidden" name="AddAppointment" id="button" value="Submit" class="gobtn fr submitbutton" />
                    	<a href="javascript:void(0);" onClick="$('#appointmentForm').submit();" class="submit-btn">Submit ></a><a href="javascript:void(0);" class="cancel-btn">Cancel ></a>
                    </div>
                    <?php } ?>
                </div>
                </form>
                <div class="clearfix"></div></div>
            </div>
          </div>
        </div>
        
      </div>
    <div class="col-md-3">
        <?php include("inc/inc.right.php"); ?>
    </div> 
             
    </div>
  </div>
</section>
<?php include("inc/inc.ft.php"); ?>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/additional-methods.min.js" type="text/javascript"></script>
<script>
 $("#appointmentForm").validate({
    rules :{
		"EMPLOYEEID" : {
		   required : true,
        },
        "EMAILID" : {
            required : true,
			email: true
			//noStartEndWhiteSpaces: true
        },
		"MOBILENO" : {
		   required : true,
		   number: true,
		   maxlength: 10,
		   minlength: 10
        },
		"state" :{
			required : true,
			//specialChars: true
			//noStartEndWhiteSpacesAlpha:true
		},
		"city" :{
			required : true,
			//specialChars: true
			//noStartEndWhiteSpacesAlpha:true
		},
		"centerName" :{
			required : true,
			//specialChars: true
			//noStartEndWhiteSpacesAlpha:true
		},
		"centerId" :{
			required : true,
			//specialChars: true
			//noStartEndWhiteSpacesAlpha:true
		},
		"date1" :{
			required : true,
			//specialChars: true
			//noStartEndWhiteSpacesAlpha:true
		},
		"timeslot1" :{
			required : true,
			//specialChars: true
			//noStartEndWhiteSpacesAlpha:true
		},
		"member[]" :{
			required : true,
			//specialChars: true
			//noStartEndWhiteSpacesAlpha:true
		}
    },
    messages :{
        "EMPLOYEEID" : {
            required : 'Please enter Employee Id'
        },
        "EMAILID" : {
            required : 'Please Enter the Email Id'
        },
		"MOBILENO" : {
            required : 'Please Enter the Mobile No.'
        },
		"state":{
			required: 'Please select State'
		},
		"city":{
			required: 'Please select City'
		},
		"centerName":{
			required: 'Please enter the Center Name'
		},
		"centerId":{
			required: 'Please enter the Center Name'
		},
		"date1":{
			required: 'Please select the Appointment Date'
		},
		"timeslot1":{
			required: 'Please select the Time Slot'
		},
		"member[]":{
			required: 'Please select the member'
		}
    },
	errorElement: "div",
	errorPlacement: function(error, element) {
     if (element.attr("name") == "member[]") {

       // do whatever you need to place label where you want

         // an example
         error.insertBefore( $("#membererror") );

         // just another example
         //$("#yetAnotherPlace").html( error );  

     } else {

         // the default error placement for the rest
         error.insertAfter(element);

     }
   },submitHandler:function(){
	var preEmailvalue=$.trim($("#EMAILID").val());	
	var preMobilevalue=$.trim($("#MOBILENO").val());	
	var newEmailvalue=$.trim($("#emailcheck").val());	
	var newMobilevalue=$.trim($("#mobilecheck").val());
	if((preEmailvalue != newEmailvalue) || (preMobilevalue != newMobilevalue)){
		if(confirm('Do you wish to update this email id / mobile in your profile?')){
			$("#changeprofile").val('YES');	
			return true;
		} else {
			$("#changeprofile").val('NO');	
			return true;
		}
	} else {
			$("#changeprofile").val('NO');	
			return true;
		}
	  }	
  }); 
</script>
<style>
div.error {
	position:absolute;
	margin-top:14px;
	color:red;
	background-color:#fff;
	padding:3px;
	text-align:left;
	z-index:1;
	margin-left: -17px;
}
div.membertypeerror .error{
	margin-top:-9px !important;
	margin-left:0px !important;
}
</style>

