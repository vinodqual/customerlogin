<?php /*****************************************************************************
* COPYRIGHT
* Copyright 2013 Catabatic Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: claims.php,v 1.0 2015/10/06 04:55:0 Amit Kumar Dubey Exp $
* $Author: Amit Kumar Dubey $
*
****************************************************************************/

include("inc/inc.hd.php");  			//include header file to show header on page
include_once($apiurlNew);
ini_set('error_reporting',0);
include_once("conf/common_functions.php");	//include common_functions file to access defined function
if(@$TPAManagedchkdis=='Yes'){
	echo "<script>window.location.href='dashboard.php';</script>"; 
	exit;
}

$responseArray = @$_SESSION['dataArray'];
$query['GCOCCNO']=sanitize_data(base64_decode(@$_REQUEST['gcocNo']));
$query['CHDRNUM']="";
$query['CLAMNUM']=sanitize_data(base64_decode(@$_REQUEST['claimNumber']));
$query['PREAUTNO']="";
$query['CLNTNUM']="";

/*
    * Request param for getting claim status and remark from new API.
     *
     * @author Rahul Shrivastava
     * @date 05-12-2016
**/

$cStatusData = array(
                "ClaimNumber" => sanitize_data(base64_decode(@$_REQUEST['claimNumber'])),
    "OccurrenceNumber" => sanitize_data(base64_decode(@$_REQUEST['gcocNo'])),
               );

               $claimStatusNew = getClaimStatus($cStatusData);

$claimArray = getClaimDetailsNew($query);
$c=0;
switch(@$claimArray['claimList'][$c]['BGEN-CLMSRVTP']['#text']){
case 'R': $claimType="Reimbursement";
break;
case 'C': $claimType="Cashless";
break;
}

$benefitGroup=@$claimArray['claimList'][$c]['BGEN-BNFTGRP']['#text'];
$claimStatusGroup=@$claimArray['claimList'][$c]['BGEN-GCSTS']['#text'];

$benefitGroupName=@$benefitGroupArray[$benefitGroup];
$claimGroupName=@$claimStatusArray[$claimStatusGroup];
?>
<section id="middleContainer">
  <div class="container-fluid">
    <div class="middlebox">
      <div class="col-md-9">
        <div class="middlebox-network">
          <div class="col-md-12">
            <div class="title-bg textleft">
              <h1>Claim Summary</h1>
            </div>
            <table class="responsive" width="100%">
              <tr>
                <td>Claim/Al Number</td>
                <td>:</td>
                <td><?php echo @$claimArray['claimList'][$c]['BGEN-CLAMNUM']['#text'];?> - <?php echo @$claimArray['claimList'][$c]['BGEN-GCOCCNO']['#text'];?></td>
                <td>Benifit Group</td>
                <td>:</td>
                <td> <?php echo @$benefitGroupName;?></td>
              </tr>
              <tr>
                <td>Client Number</td>
                <td>:</td>
                <td><?php echo @$claimArray['claimList'][$c]['BGEN-MEMNAME']['#text'];?></td>
                <td>Claim/AL Type </td>
                <td>:</td>
                <td> <?php echo @$claimType;?></td>
              </tr>
              <tr>
                <td>Client Name </td>
                <td>:</td>
                <td><?php echo @$claimArray['claimList'][$c]['BGEN-MEMNAME']['#text'];?></td>
                <td>Claim Type(IPD/OPD) </td>
                <td>:</td>
                <td><?php echo @$claimArray['claimList'][$c]['BGEN-PATNTYN']['#text'];?></td>
              </tr>
              <tr>
                <td>Policy Number</td>
                <td>:</td>
                <td><?php echo @$claimArray['claimList'][$c]['BGEN-CHDRNUM']['#text'];?></td>
                <td>Provider Organisation </td>
                <td>:</td>
                <td><?php if(isset($claimArray['claimList'][$c]['BGEN-PROVORG']['#text']) or isset($claimArray['claimList'][$c]['BGEN-SURNAME']['#text']) ){ echo @$claimArray['claimList'][$c]['BGEN-PROVORG']['#text'];?> / <?php echo @$claimArray['claimList'][$c]['BGEN-SURNAME']['#text']; } ?></td>
              </tr>
              <tr>
                <td>Inward Number</td>
                <td>:</td>
                <td> <?php echo sanitize_data(base64_decode(@$_REQUEST['inwrdNo']));?></td>
                <td>Date & Time </td>
                <td>:</td>
                <td><?php if(isset($claimArray['claimList'][$c]['BGEN-DATIME']['#text']) && !empty($claimArray['claimList'][$c]['BGEN-DATIME']['#text'])){ $resdate = explode('-',@$claimArray['claimList'][$c]['BGEN-DATIME']['#text']); echo @$resdate[0]."-".@$resdate[1]."-".@$resdate[2];  } else { echo @$claimArray['claimList'][$c]['BGEN-DATIME']['#text']; } ?></td>
              </tr>
               <?php if($claimGroupName == 'Claim Closed'){ ?>
              <tr>
                <td>Cheque Number</td>
                <td>:</td>
                <td><?php if(isset($claimArray['claimList'][$c]['BGEN-ZNEFTREF']['#text']) && !empty($claimArray['claimList'][$c]['BGEN-ZNEFTREF']['#text'])){ echo "NA"; } else { echo @$claimArray['claimList'][$c]['BGEN-CHEQNO']['#text']; } ?></td>
                <td>NEFT No. </td>
                <td>:</td>
                <td><?php if(isset($claimArray['claimList'][$c]['BGEN-CHEQNO']['#text']) && !empty($claimArray['claimList'][$c]['BGEN-CHEQNO']['#text'])){ echo "NA"; } else { echo @$claimArray['claimList'][$c]['BGEN-ZNEFTREF']['#text']; } ?></td>
              </tr>
              <tr>
               <?php if(isset($_REQUEST['pname']) && base64_decode($_REQUEST['pname'])==1){ ?>
                <td>Payee Name:</td>
                <td>:</td>
                <?php } ?>
                <td><?php echo @$claimArray['claimList'][$c]['BGEN-CNAME']['#text']?@$claimArray['claimList'][$c]['BGEN-CNAME']['#text']:'NA';?></td>
                <td>Amount </td>
                <td>:</td>
                <td><?php echo number_format(@$claimArray['claimList'][$c]['BGEN-AMNT']['#text']);?></td>
              </tr>
              <?php } ?>
            </table>
          </div>
        </div>
        
        
        <div class="clearfix"></div>
        
        
        <div class="middlebox-network">
          <div class="col-md-12">
            <div class="title-bg textleft">
              <h1>Details Of claim Processing</h1>
            </div>
            <table class="responsive" width="100%">
              <tr>
                <td>First Report Date</td>
                <td>:</td>
                <td><?php echo dateChange(@$claimArray['claimList'][$c]['BGEN-GCFRPDTE']['#text']);?></td>
                <td>Date of Visit :</td>
                <td>:</td>
                <td><?php echo dateChange(@$claimArray['claimList'][$c]['BGEN-DTEVISIT']['#text']);?></td>
              </tr>
              <tr>
                <td>Date of Discharge :</td>
                <td>:</td>
                <td><?php echo dateChange(@$claimArray['claimList'][$c]['BGEN-DTEDCHRG']['#text']);?></td>
                <td>Document Received Date :</td>
                <td>:</td>
                <td><?php echo dateChange(@$claimArray['claimList'][$c]['BGEN-DOCRCVDT']['#text']);?></td>
              </tr>
              <tr>
                <?php if(isset($_REQUEST['secst']) && base64_decode($_REQUEST['secst'])=="PD" && !empty($claimArray['claimList'][$c]['BGEN-ZCLMRECD']['#text']) ){ ?>
                <td>Form Received On : </td>
                <td>:</td>
                <td><?php echo dateChange($claimArray['claimList'][$c]['BGEN-ZCLMRECD']['#text']);?></td>
                <?php }  ?>
              </tr>
               <?php if (($claimStatusGroup=="AI") || ($claimStatusGroup=="CC") || ($claimStatusGroup=="CA")){ ?>
              <tr>
                <td>Claim Amount(Total Incurred Amount) :</td>
                <td>:</td>
                <td><?php echo number_format(@$claimArray['claimList'][$c]['BGEN-INCURRED']['#text']);?></td>
                <td>Approved Amount:</td>
                <td>:</td>
                <td><?php echo number_format(@$claimArray['claimList'][$c]['BGEN-TLHMOSHR']['#text']);?></td>
              </tr>
              <tr>
                <td>Member Share :</td>
                <td>:</td>
                <td><?php echo number_format(@$claimArray['claimList'][$c]['BGEN-TLMBRSHR']['#text']);?></td>
                <?php if(number_format(@$claimArray['claimList'][$c]['BGEN-COPAYAMT']['#text'])>0){?>
                <td>(Copay Amount):</td>
                <td>:</td>
                <td><?php echo number_format(@$claimArray['claimList'][$c]['BGEN-COPAYAMT']['#text']);?></td>
              </tr>
               <?php } } ?>
              
              
            </table>
          </div>
        </div>
        
        <div class="clearfix"></div>
	   <?php if (($claimStatusGroup=="AI") || ($claimStatusGroup=="CC") || ($claimStatusGroup=="CA")){ ?>       
        <div class="middlebox-network">
          <div class="col-md-12">
            <div class="title-bg textleft">
              <h1>Dis- Allowance reason</h1>
            </div>
            <table class="responsive" width="100%">
            
			<?php 
            $t=0;
            for($k=0;$k<count(@$claimArray['claimList']['DISSALWNCE']);$k++){
            $t++;
            if(@$claimArray['claimList']['DISSALWNCE'][$k]['BGEN-MEMNAME']['#text']!=''){
			$length='';
			$length=strlen($claimArray['claimList']['DISSALWNCE'][$k]['BGEN-MEMNAME']['#text']);
			$append='';
			if($length<4 && $length!='')
			{
				while($length<4){
					$append.=0;
					$length++;
				}
				if($append!='')
				{
					@$disallowance=$disallowanceArray[$append.$claimArray['claimList']['DISSALWNCE'][$k]['BGEN-MEMNAME']['#text']];
				}
			} else{
				@$disallowance=$disallowanceArray[@$claimArray['claimList']['DISSALWNCE'][$k]['BGEN-MEMNAME']['#text']]; 

			}
                              if($k%2==0){
                      $addedclass='alternate_bg_last';
                      } else {
                      $addedclass='';
                      }			 
            if($disallowance!=''){
            ?>
              <tr>
                <td width="10%"><?php echo @$disallowance;?> </td>
                <td width="5%">:</td>
                <td width="50%"><?php echo number_format(@$claimArray['claimList']['DISSALWNCE'][$k]['BGEN-CLAMNUM']['#text']);?></td>
              </tr>
           <?php } } } if($t==0){ ?>   
		   <tr>
			   <td colspan="3">NA</td>
		   </tr>
			  <?php
              } ?> 
            </table>
          </div>
        </div>
        <div class="clearfix"></div>
	   <?php } ?>
       <div class="middlebox-network">
          <div class="col-md-12">
            <table class="responsive" width="100%">
              <tr>
		   <?php 
			for($k1=0;$k1<count(@$claimArray['claimList']['DISSALWNCENEWP']);$k1++){
			
			//echo @$claimArray['claimList']['DISSALWNCE'][$k]['BGEN-MEMNAME']['#text'];
			if(@$claimArray['claimList']['DISSALWNCENEWP'][$k1]['BGEN-MEMNAME']['#text']!=''){		
			$disallowancep=$disallowanceArray[@$claimArray['claimList']['DISSALWNCENEWP'][$k1]['BGEN-MEMNAME']['#text']]; 
							  if($k1%2==0){
					  $addedclass='alternate_bg_last';
					  } else {
					  $addedclass='';
					  }			 
			if(@$disallowancep!=''){
			?>
                <td width="10%"><?php echo @$disallowancep;?></td>
             
			<?php } } }
            @$claimArray['claimList']['DISSALWNCENEWS']=array_map("unserialize", array_unique(array_map("serialize", @$claimArray['claimList']['DISSALWNCENEWS'])));
            
            for($k2=0;$k2<count(@$claimArray['claimList']['DISSALWNCENEWS']);$k2++){
            
            //echo @$claimArray['claimList']['DISSALWNCE'][$k]['BGEN-MEMNAME']['#text'];
            if(@$claimArray['claimList']['DISSALWNCENEWS'][$k2]['BGEN-MEMNAME']['#text']!=''){		
            $disallowances=$disallowanceArray[@$claimArray['claimList']['DISSALWNCENEWS'][$k2]['BGEN-MEMNAME']['#text']]; 
                              if($k2%2==0){
                      $addedclass='alternate_bg_last';
                      } else {
                      $addedclass='';
                      }			 
            if(@$disallowances!=''){
            ?>
            <td><?php echo @$disallowances;?></td>
          <?php } } }
		  
		  ?>
		  </tr>
          <?php
		   if($t==0){
		  ?>
		   <tr>
               <td>NA</td>
		   </tr>
		  <?php
		  } ?> 
            </table>
          </div>
        </div>
        <div class="clearfix"></div>
        <?php  if($claimStatusGroup=="CA"){
		if(number_format(@$claimArray['claimList'][$c]['BGEN-AMNT']['#text'])>0){?>

        <div class="middlebox-network">
                  <div class="col-md-12">
                    <div class="title-bg textleft">
                      <h1>Cheque status</h1>
                    </div>
                    <table class="responsive" width="100%">
                      <tr>
                        <td width="10%">Cheque Amount :</td>
                        <td width="5%">:</td>
                        <td width="50%"><?php echo number_format(@$claimArray['claimList'][$c]['BGEN-AMNT']['#text']);?></td>
                      </tr>
                      <tr>
                        <td width="10%">Payee Name :</td>
                        <td width="5%">:</td>
                        <td width="50%"><?php echo @$claimArray['claimList'][$c]['BGEN-CNAME']['#text'];?></td>
                      </tr>
                    </table>
                  </div>
                </div>
        <div class="clearfix"></div> 
        <?php } } ?> 
	    <?php  if((@$claimType=="Cashless" && $claimStatusGroup=="AR") or (@$claimType=="Reimbursement" && $claimStatusGroup=="CR")){?>       
        <div class="middlebox-network">
                  <div class="col-md-12">
                    <div class="title-bg textleft">
                      <h1>Rejection Reason</h1>
                    </div>
                    <table class="responsive" width="100%">
                     <?php  if((@$claimType=="Cashless" && $claimStatusGroup=="AR")){?>
                      <tr>
                        <td width="10%" colspan="3"><?php echo @$claimArray['claimList'][$c]['BGEN-REASONDESC']['#text'];?> (<?php echo dateChange(@$claimArray['claimList'][$c]['BGEN-REJDATE']['#text']);?>) </td>
                      </tr>
					  <?php } else if((@$claimType=="Reimbursement" && $claimStatusGroup=="CR")){?>
                      <tr>
                        <td width="10%" colspan="3"><?php echo @$claimArray['claimList'][$c]['BGEN-REASONDESC']['#text'];?> (<?php echo dateChange(@$claimArray['claimList'][$c]['BGEN-REJDATE']['#text']);?>)</td>
                      </tr>
					 <?php if(count($claimArray['claimList']['REJECTIONREASONNEW'])>0){ 
                    
                      for($s1=0;$s1<count($claimArray['claimList']['REJECTIONREASONNEW']);$s1++){
                     ?> 
                      <tr>
                        <td width="10%" colspan="3"><?php echo $rejectionReasonArray[@$claimArray['claimList']['REJECTIONREASONNEW'][$s1]['BGEN-GCOPRSCD']['#text']];?></td>
                      </tr>
					  <?php }
                       } ?>
                      <?php } else { ?>
                      <tr>
                      <td colspan="3"><?php echo "NA"; ?></td>
                      <?php } ?>
                    </table>
                  </div>
                </div>
        <div class="clearfix"></div> 
        <?php } ?>
            
        <div class="middlebox-network">
          <div class="col-md-12">
            <div class="title-bg textleft">
              <h1>Non Medical Expense Deduction Detail</h1>
            </div>
            <table class="responsive" width="100%">
			<?php if(count(@$claimArray['claimList']['SYMPTOM'])>0){ ?>
              <tr>
                <td>
				<?php
                for($k=0;$k<count(@$claimArray['claimList']['SYMPTOM']);$k++){
                if(@$claimArray['claimList']['SYMPTOM'][$k]['BGEN-SYMPTOM']['#text']!=''){			 
                ?>
                <?php echo stripslashes(@$claimArray['claimList']['SYMPTOM'][$k]['BGEN-SYMPTOM']['#text']);?>
                
                <?php } 
                }	 
                ?>
				</td>
              </tr>
				<?php } else {
                echo " <tr><td>NA</td></tr>";
                }
			   ?>
            </table>
          </div>
        </div>

       <div class="clearfix"></div>
        <div class="middlebox-network">
          <div class="col-md-12">
            <div class="title-bg textleft">
              <h1>Case Note</h1>
            </div>
            <table class="responsive" width="100%">
			<?php
            $caseNote="";
            for($k=0;$k<count(@$claimArray['claimList']['CASENOTE']);$k++){
            if(@$claimArray['claimList']['CASENOTE'][$k]['BGEN-CASENOTE']['#text']!=''){		
            $caseNote.=stripslashes(@$claimArray['claimList']['CASENOTE'][$k]['BGEN-CASENOTE']['#text']);
            }	 
            }	
            ?>
            <tr>
                <td><?php echo @$caseNote;?></td>
            </tr>
            </table>
          </div>
        </div>
<!-- For Display claim status & query remarks. 
 *  Added by: Rahul Shrivastava
 *  Date: 05-Dec-2016 
--->
<div class="clearfix"></div>
<div class="middlebox-network">
  <div class="col-md-12">
    <div class="title-bg textleft">
      <h1>Query Remarks</h1>
    </div>
    <table class="responsive" width="100%">
        <?php
        if ($claimStatusNew['ErrorCode'] == "0") {
        ?>
        <tr>
          <td>Claim Status :</td>
          <td><?php echo $claimStatusNew['ClaimStatus']; ?></td>
        </tr>
        <tr>
          <td>Remarks :</td>
          <td><?php echo $claimStatusNew['Remarks']; ?></td>
        </tr>
        <tr>
          <td>Query Documents :</td>
            <?php
            if (count($claimStatusNew['QueryDocuments']) > 0) {
                $c = 1;
                for ($i = 0; $i < count($claimStatusNew['QueryDocuments']); $i++) {
                ?>

            <tr>
              <td style="padding-left: 40px !important;">(<?php echo $c; ?>)&nbsp;<?php echo $claimStatusNew['QueryDocuments'][$i]->DocumentType; ?> -</td>
              <td><?php echo $claimStatusNew['QueryDocuments'][$i]->DocumentRemarks; ?></td>
            </tr>
            <?php
            $c++;
                }
            } else {
            ?>

          <td><?php echo "NA"; ?></td>  
          </tr> 
            <?php }
        } else {
    ?>
        <tr>
          <td><?php echo "NA"; ?></td>
        </tr>
        <?php }//end if ?>
    </table>
  </div>
</div>
<!----End Query remarks--->

        <?php
        if($claimStatusGroup=="CD"){
        ?>
       <div class="clearfix"></div>
        <div class="middlebox-network">
          <div class="col-md-12">
            <div class="title-bg textleft">
              <h1>Deficiency</h1>
            </div>
            <table class="responsive" width="100%">
			 <?php
              $reminders=0;
              for($k=0;$k<count(@$claimArray['claimList']['Deficiency']);$k++){
              if(@$claimArray['claimList']['Deficiency'][$k]['BGEN-GFUPCDE']['#text']!='' && @$claimArray['claimList']['Deficiency'][$k]['BGEN-GFUPSTS']['#text']=="O"){	
                $def=@$claimArray['claimList']['Deficiency'][$k]['BGEN-GFUPCDE']['#text'];			 
              ?>
            <tr>
                <td colspan="2">Deficiency :</td>
                <td colspan="2"><?php echo @$deficiencyArray[@$def];?></td>
            </tr>
         <?php } }
		   $claimArray['claimList'][$c]['REMDTETHE'];
		  if($claimArray['claimList'][$c]['REMDTE']!='00000000' && $claimArray['claimList'][$c]['REMDTE']!='99999999'  && $claimArray['claimList'][$c]['REMDTE']!=''){
		 	 $reminders=1;
		  }
		   if($claimArray['claimList'][$c]['REMDTETWO']!='00000000' && $claimArray['claimList'][$c]['REMDTETWO']!='99999999'  && $claimArray['claimList'][$c]['REMDTETWO']!=''){
		 	 $reminders=2;
		  }
		   if($claimArray['claimList'][$c]['REMDTETHE']!='00000000' && $claimArray['claimList'][$c]['REMDTETHE']!='99999999' && $claimArray['claimList'][$c]['REMDTETHE']!=''){
		 	 $reminders=3;
		  }
		   ?>
        <tr>
            <td colspan="2">How many reminders till now :</td>
            <td colspan="2"><?php echo $reminders;?></td>
        </tr>
            </table>
          </div>
        </div>
        <?php } ?>
        
      </div>
        <div class="col-md-3">
            <?php include("inc/inc.right.php"); ?>
        </div> 
    </div>
  </div>
</section>
<?php include("inc/inc.ft.php"); ?>