<?php
$strErrormsg = '';
$webserviceid = 3;

include_once 'conf/conf-csapi.php';
/**----------Intiate parameters for return-------------**/
$valid = false;
$statusData = array();
/**-------------End-------------**/
$response           = array();
/**----------Testing code-------------**/
//$array                  = array('apikey'=>'5975cedf2ef7g', 'data'=>'[{"unique_id": "R303", "status":"CONFIRM"}, { "unique_id": "R305", "status":"CANCEL" }]');
//$str 			= json_encode($array);
//$strData 		= encrypt_data($str);
/**----------End of testing code-------------**/
$strData	= req_param('data');
if ($strData == '') {
        $strError = 'ERR000';
}
if ($strError == '') {
    $auth       = AuthenticateApiUser($strData,$webserviceid);
    $getData 	= json_decode(trim($auth),true);
    if ($getData['STATUS']==1) {
        $valid = true;
        /**----------Main logic goes here -------------**/
        $requested_data         =  $getData['DATA']; 
        $response_data          =  $getData['RESPONSE'];
        $userConfiguration = checkAPIconfigurations($response_data['APIUSERID'], $response_data['WSCATEGORYID'], $response_data['APIUSERTOWEBSERICEID'], $response_data['CONFIGURATION']);
        $configData 	= json_decode(trim($userConfiguration),true);
        if ($configData['STATUS'] === true) {
            $center_ids = $configData['CONFIGURATIONDATA'];
            $applist	= json_decode(trim($requested_data['data']),true); 
            if ($center_ids!='' && count($applist)>0) { 
                for($i=0;$i<count($applist);$i++) {
                    $sttusvalid = false;
                    $uniqueid   = '';
                    $status     = '';
                    $tablename  = '';
                    $historytablename = '';
                    $uniqueid = $applist[$i]['unique_id'];
                    $status   = $applist[$i]['status']; 
                    if ($uniqueid != '' && $status != '') {
                        $apptype = $uniqueid[0];
                        if ($apptype == 'R') {
                            $type = 'RETAIL';
                            $tablename          = 'CRMRETAILPOSTAPPOINTMENT';
                            $historytablename   = 'CRMRETAILAPPHISTORY';
                        } else if ($apptype == 'C') {
                            $type = 'CORPORATE';
                            $tablename          = 'CRMCOMPANYPOSTAPPOINTMENT';
                            $historytablename   = 'CRMCOMPANYAPPHISTORY';
                        } else {
                            $strErrormsg = 'ERR0020';
                        }
                        if ($strErrormsg == '') {
                            $appointmentid = substr($uniqueid,1);
                            if (!is_numeric($appointmentid)) {
                                $strErrormsg = 'ERR0020';
                            } else {
                                unset($bAppointmentDetails, $aAppointmentDetails); 
                                dbSelect("SELECT CENTERID FROM ".$tablename." WHERE APPOINTMENTID='".$appointmentid."'", $bAppointmentDetails, $aAppointmentDetails);
                                if ( $bAppointmentDetails ) {
                                    $centerid_array = explode(',', $center_ids);
                                    $_SESSION['userName'] = $requested_data['apikey'];
                                    if (in_array($aAppointmentDetails[0]['CENTERID'], $centerid_array)) {
                                        include_once(__DIR__.'/../../religare1/cs/classes/updateAppointmentStatus.php');
                                        $response = updateAppointmentStatus($appointmentid, $status, $type);
                                        $data 	= json_decode(trim($response),true);
                                        if ($data['valid'] === true) {
                                            $sttusvalid = true;
                                        } else {
                                            $strError = $data['error'];
                                        }
                                    }      
                                }
                            }
                        }
                    } else {
                        $strErrormsg = 'ERR0021';
                    }
                    $statusData[$i]['unique_id']    =  $uniqueid;
                    $statusData[$i]['status']       =  $status;
                    $statusData[$i]['error']        =  ($strError)?$errorMessages[$strError]:'';
                    $statusData[$i]['message']      =  ($sttusvalid==true)?"SUCCESS":"FAILER";
                    
                } 
            }
        }
        /**----------End of main Logic -------------**/
    } else{
        $strError = $getData['ERROR_CODE'];
    }
}
/**-------------Prepare final response to send-------------**/
$response = array(
    "response_status"   => $valid,
    "response_message"  => ($strError!='')?$errorMessages[$strError]:'',
    "error_code"        => $strError,
    "data"              => json_encode($statusData),
);
/**-------------End-------------**/
$result = json_encode($response);
/**-------------Create Log-------------**/
file_put_contents('logs/' . date('Y-M', $scriptTime) . '_'.$webservicelog[$webserviceid], "Response, " . $ScriptDateTime . "," . trim($result) . PHP_EOL . PHP_EOL, FILE_APPEND);
/**-------------End Log-------------**/
//echo $result;
echo encrypt_data($result);
?>