<?php if(isset($_GET["wsdl"])){
	require_once ( 'create-wsdl/class.phpwsdl.php' );
	PhpWsdl::RunQuickMode ();
	exit;
}

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'conf/conf.php';
//require_once '../../religare1/projectconfig/sqlServerConnect.php';
$conn_sql=db_connect();

class tpa_api {	
    
    /**
     * 
     * @author Sanjeev Kumar
     * @todo This function write to get claim data.
     * @global type $conn
     * @param string $request_data
     * @return string
     * 
     */
    function getClaimData($request_data) {
        global $conn_sql;
        if($conn_sql){
            $ip_address = '';
            if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ip_address = $_SERVER['HTTP_CLIENT_IP'];
            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_X_FORWARDED']))
                $ip_address = $_SERVER['HTTP_X_FORWARDED'];
            else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                $ip_address = $_SERVER['HTTP_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_FORWARDED']))
                $ip_address = $_SERVER['HTTP_FORWARDED'];
            else if(isset($_SERVER['REMOTE_ADDR']))
                $ip_address = $_SERVER['REMOTE_ADDR'];
            else
                $ip_address = 'UNKNOWN';
			
            $message='';
            $data=array();
            $status='';
            $decrypted_data=decrypt_data($request_data);
            file_put_contents('logs/'.date('Y-M-d').'.txt',"Request,getClaimData, ".date('d-M-Y h:i:s A').", $ip_address, ".trim($decrypted_data).PHP_EOL.PHP_EOL,FILE_APPEND);
            $request_array=json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $decrypted_data), true);
            if(!isset($request_array['username'])){
                    $message.='TPACD0011: '.TPACD0011.'</br>';
            }
            if(empty($request_array['policy_number'])){
                    $message.='TPACD0001: '.TPACD0001.'</br>';
            }
            if(empty($request_array['username'])){
                    $message.='TPACD0002: '.TPACD0002.'</br>';
            }
            if(isset($request_array['password']) && ($request_array['password']=='')){
                    $message.='TPACD0003: '.TPACD0003.'</br>';
            }
            $date_pattern='/^[0-3]?[0-9]\/[0-1]?[0-9]\/(19|20)[0-9]{2} (([0-1][0-9])|(2[0-3])):[0-5][0-9]$/';
            if(empty($request_array['from_date'])){
                    $message.='TPACD0004: '.TPACD0004.'</br>';
            }else if(!preg_match($date_pattern, $request_array['from_date'])){
                    $message.='TPACD0005: '.TPACD0005.'</br>';
            }
            if(!empty($request_array['to_date'])) if(!preg_match($date_pattern, $request_array['to_date'])){
                    $message.='TPACD0006: '.TPACD0006.'</br>';
            }
        }
        else{
               $message.='TPACD0000: '.TPACD0000.'</br>';
        }
        if(empty($message)){
            $cond='';
            if(!empty($request_array['from_date'])){
                    $cond.=" ,@FROM_DATE='".$request_array['from_date']."'";
            }
            if(!empty($request_array['to_date'])){
                    $cond.=", @TO_DATE='".$request_array['to_date']."'";
            }
            $parent_agent_code='';
            $query_agent = "EXEC GET_CRM_PARTNER_MASTER_PARENT_AGENT_CODE  @USER_NAME='".$request_array['username']."'  ,@PASS_WORD='".$request_array['password']."', @IP_ADDRESS='".$ip_address."';";
            $std=sqlsrv_query($conn_sql, $query_agent);
            if($std){
                while($row=sqlsrv_fetch_array($std,SQLSRV_FETCH_ASSOC)){
                    $parent_agent_code=$row['PARENT_AGENT_CODE'];
                }
                sqlsrv_free_stmt($std);
            }else{
                $message.='TPACD0007: '.TPACD0007.'</br>';   
                $status='ERROR';
            }
            if(trim($parent_agent_code)!=''){
                //$query_claim = "EXEC [GETCRMCLAIMDETAILSDATA]  @POLICY_NUMBER='".$request_array['policy_number']."'  ,@CREATED_ON='".$request_array['from_date']."', @CREATED_ON='".$request_array['to_date']."',@AGENT_CODE='".$parent_agent_code."';";
                $query_claim = "EXEC [GETCRMCLAIMDETAILSDATA]  @POLICY_NUMBER='".$request_array['policy_number']."' ".$cond." ,@AGENT_CODE='".$parent_agent_code."';";
				$std=sqlsrv_query($conn_sql, $query_claim);
                if($std){
                    $data=array();
                    while($row=sqlsrv_fetch_array($std,SQLSRV_FETCH_ASSOC)){
                        $data[]=$row;
                    }
                    if(count($data)==0){
                        $message.='TPACD0008: '.TPACD0008.'</br>';
                        $status='ERROR';
                    }elseif(count($data)>0){
						$message.='Data found according to your request.</br>';
                        $status='SUCCESS';
					}
                    sqlsrv_free_stmt($std);
                }else{
                    $message.='TPACD0009: '.TPACD0009.'</br>';   
                    $status='ERROR';
                }
            }else{  // End of if check parent_agent_code found in database
                $message.='TPACD0010: '.TPACD0010.'</br>';
                $status='ERROR';
            }
        }else{
            $status='ERROR';
        }
        
        $response_array=array('response_status'=>$status,'response_message'=>$message,'data'=>$data);
        $response=json_encode($response_array);     // Encode array in JSON Format
        file_put_contents('logs/'.date('Y-M-d').'.txt',"Response,getClaimData, ".date('d-M-Y h:i:s A').", $ip_address, ".trim($response).PHP_EOL.PHP_EOL,FILE_APPEND);
        $encrypted_data=encrypt_data($response);
        return $encrypted_data;
    }
    
    /**
     * 
     * @author Sanjeev Kumar
     * @todo This function write for get network details.
     * @global type $conn
     * @param string $request_data
     * @return string
     */
    function getNetworkData($request_data){
        global $conn_sql;
        $message='';
        $status='';
        $retunCount=true;
        if($conn_sql){
			$ip_address = '';
            if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ip_address = $_SERVER['HTTP_CLIENT_IP'];
            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_X_FORWARDED']))
                $ip_address = $_SERVER['HTTP_X_FORWARDED'];
            else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                $ip_address = $_SERVER['HTTP_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_FORWARDED']))
                $ip_address = $_SERVER['HTTP_FORWARDED'];
            else if(isset($_SERVER['REMOTE_ADDR']))
                $ip_address = $_SERVER['REMOTE_ADDR'];
            else
                $ip_address = 'UNKNOWN';
            $decrypted_data=decrypt_data($request_data);
            file_put_contents('logs/'.date('Y-M-d').'.txt',"Request,getNetworkData, ".date('d-M-Y h:i:s A').", $ip_address, ".trim($decrypted_data).PHP_EOL.PHP_EOL,FILE_APPEND);
            $request_array=json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $decrypted_data), true);
			if(!isset($request_array['username'])){
				$message.='TPACD0011: '.TPACD0011.'</br>';
			}
            if(empty($request_array['username'])){
                    $message.='TPAND0001: '.TPAND0001.'</br>';
            }
            if(empty($request_array['password'])){
				$message.='TPAND0002: '.TPAND0002.'</br>';
			}
            if(empty($request_array['count'])){
                $retunCount=false;
            } else {
				if($request_array['count']==="false" || $request_array['count']===false){
					$retunCount=false;
				}
			}
			if(!$retunCount){
				if(!empty($request_array['start_index']) && !empty($request_array['end_index'])){
					if(ctype_digit($request_array['start_index']) && ctype_digit($request_array['end_index'])){
						if($request_array['start_index']>$request_array['end_index'] || $request_array['start_index']<1 || $request_array['end_index']<1){
							$message.='TPAND0005: '.TPAND0005.'</br>';
						}
					} else{
						$message.='TPAND0004: '.TPAND0004.'</br>';
					}
				} else{
					$message.='TPAND0003: '.TPAND0003.'</br>';
				}
				//$message.='TPAND0006: '.TPAND0006.'</br>';
			}
        }else{
			$message.='TPACD0000: '.TPACD0000.'</br>';
        }
        if(empty($message)){    // Check if no error occurs
            $parent_agent_code='';            
            $query_agent = "EXEC GET_CRM_PARTNER_MASTER_PARENT_AGENT_CODE  @USER_NAME='".$request_array['username']."'  ,@PASS_WORD='".$request_array['password']."', @IP_ADDRESS='".$ip_address."';";
            //$query_agent="SELECT PARENT_AGENT_CODE FROM CRM_PARTNER_MASTER WHERE STATUS='ACTIVE' AND USER_NAME='".$request_array['username']."' AND PASS_WORD='".$request_array['password']."'";
            $std_agent=sqlsrv_query($conn_sql, $query_agent);
            if($std_agent){
                while($row_agent=sqlsrv_fetch_array($std_agent,SQLSRV_FETCH_ASSOC)){
                    $parent_agent_code=$row_agent['PARENT_AGENT_CODE'];
                }
                sqlsrv_free_stmt($std_agent);
            }else{
                $message.='TPAND0007: '.TPAND0007.'</br>';   
                $status='ERROR';
            }
            
            if(!empty($parent_agent_code)){    // User name and Password match in database
                if($retunCount){  // If receive count parameter in request
                    $query="SELECT count(*) as count FROM CRM_NETWORK_MASTER WHERE STATUS='ACTIVE' AND DISPLAY=1";
                    $std=sqlsrv_query($conn_sql, $query,array(), array("Scrollable"=>"static"));
                    if($std){
                        $data='';
                        $row=sqlsrv_fetch_array($std,SQLSRV_FETCH_ASSOC);
                        $data=$row['count'];
                        if($data!==''){
                            $message.='Data found according to your request.</br>';
                            $status='SUCCESS';
                        }
                        sqlsrv_free_stmt($std);
                    }else{
                        $message.='TPAND0007: '.TPAND0007.'</br>';   
                        $status='ERROR';
                    } 
                    
                }else{  // Count parameter does not received in request
                    //$query_network="SELECT * FROM (SELECT ROW_NUMBER() OVER (ORDER BY HOSPITAL_NAME) r, CRM_NETWORK_MASTER.* FROM CRM_NETWORK_MASTER  ) a WHERE STATUS='ACTIVE' AND DISPLAY='TRUE' AND a.r>'".$request_array['start_index']."' AND a.r<='".$request_array['end_index']."'";
                    
                    $query_network = "EXEC GETNETWORKHOSPITALDETAILS  @start_index='".$request_array['start_index']."'  ,@end_index='".$request_array['end_index']."';";
                    $std_network=sqlsrv_query($conn_sql, $query_network);
                    if($std_network){
                        while($row_network=sqlsrv_fetch_array($std_network,SQLSRV_FETCH_ASSOC)){
                            $data[]=$row_network;
                        }
                        if(!empty($data)){
                            $message.='Data found according to your request.</br>';
                            $status='SUCCESS';
                        }else{
                            $message.='TPAND0008: '.TPAND0008.'</br>';
                            $status='ERROR';
                        }
                        sqlsrv_free_stmt($std_network);
                    }else{
                        $message.='TPAND0009: '.TPAND0009.'</br>';   
                        $status='ERROR';
                    }
                    
                }   // End of Else block check if count is other than true
            }else{  // End of if check parent_agent_code found in database
                $message.='TPAND0010: '.TPAND0010.'</br>';
                $status='ERROR';
            }
        }else{  // End of if block check no error occurs
            $status='ERROR';
        }
        
        $response_array=array('response_status'=>$status,'response_message'=>$message,'data'=>$data);
        $response=json_encode($response_array);     // Encode array in JSON Format
        file_put_contents('logs/'.date('Y-M-d').'.txt',"Response,getNetworkData, ".date('d-M-Y h:i:s A').", $ip_address, ".trim($response).PHP_EOL.PHP_EOL,FILE_APPEND);
        $encrypted_data=encrypt_data($response);
        return $encrypted_data;
    }
    
    
    /**
     * 
     * @author Sanjeev Kumar
     * @todo This function written for generate ecard pdg url
     * @global type $conn
     * @param string $request_data
     * @return string
     * 
     */
    function getEcardURL($request_data){
        global $conn_sql;
        $message='';
        $data=array();
        $status='';
		if($conn_sql){
			$ip_address = '';
            if (isset($_SERVER['HTTP_CLIENT_IP']))
                $ip_address = $_SERVER['HTTP_CLIENT_IP'];
            else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
                $ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_X_FORWARDED']))
                $ip_address = $_SERVER['HTTP_X_FORWARDED'];
            else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
                $ip_address = $_SERVER['HTTP_FORWARDED_FOR'];
            else if(isset($_SERVER['HTTP_FORWARDED']))
                $ip_address = $_SERVER['HTTP_FORWARDED'];
            else if(isset($_SERVER['REMOTE_ADDR']))
                $ip_address = $_SERVER['REMOTE_ADDR'];
            else
                $ip_address = 'UNKNOWN';
			$decrypted_data=decrypt_data($request_data);
			file_put_contents('logs/'.date('Y-M-d').'.txt',"Request,getEcardURL, ".date('d-M-Y h:i:s A').", $ip_address, ".trim($decrypted_data).PHP_EOL.PHP_EOL,FILE_APPEND);
			$request_array=json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $decrypted_data), true);
			if(!isset($request_array['username'])){
				$message.='TPACD0011: '.TPACD0011.'</br>';
			}
			if(empty($request_array['username'])){
				$message.='TPAEU0002: '.TPAEU0002.'</br>';
			}
			if(isset($request_array['password']) && ($request_array['password']=='')){
				$message.='TPAEU0003: '.TPAEU0003.'</br>';
			}
			if(empty($request_array['policy_number'])){
				$message.='TPAEU0001: '.TPAEU0001.'</br>';
			}
			if(empty($request_array['employee_number'])){
				$message.='TPAEU0004: '.TPAEU0004.'</br>';
			}
			if(isset($request_array['employee_number']) && strlen($request_array['employee_number'])<10){
				$message.='TPAEU0005: '.TPAEU0005.'</br>';
			}
		} else{
			$message.='TPACD0000: '.TPACD0000.'</br>';
		}
        if(empty($message)){
            $parent_agent_code='';            
            $query_agent = "EXEC GETAGENTMAPPINGDATA  @user_name='".$request_array['username']."'  ,@password='".$request_array['password']."' ,@policy_number='".$request_array['policy_number']."', @ip_address='".$ip_address."';";
            $std_agent=sqlsrv_query($conn_sql, $query_agent);
            if($std_agent){
                while($row_agent=sqlsrv_fetch_array($std_agent,SQLSRV_FETCH_ASSOC)){
                    $parent_agent_code=$row_agent['PARENT_AGENT_CODE'];
                }
                sqlsrv_free_stmt($std_agent);
            }else{
                $message.='TPAEU0006: '.TPAEU0006.'</br>';   
                $status='ERROR';
            }
            if($parent_agent_code!=''){
                $salt_string  = 'qJB0rGtIn5UB1xG03efyCp';       // Salt for validation.
                $send_hash = md5(trim($request_array['policy_number']).trim($request_array['employee_number']).trim($salt_string));
                $encrypted_data=encrypt_data($request_array['policy_number'].$request_array['employee_number']);
                $encrypted_data=str_replace(array('+', '/'), array('-', '_'),$encrypted_data);    // Remove + and / from url
                
                $data=ECARD_URL.$encrypted_data.'/'.$send_hash.'.pdf'; // Ecard pdf link for download
                $message.='Ecard url generated successfully.</br>';   
                $status='SUCCESS';
            }else{
                $message.='TPAEU0007: '.TPAEU0007.'</br>';   
                $status='ERROR';
            }
        }else{
            $status='ERROR';
        }
        
        $response_array=array('response_status'=>$status,'response_message'=>$message,'data'=>$data);
        $response=json_encode($response_array);     // Encode array in JSON Format
        file_put_contents('logs/'.date('Y-M-d').'.txt',"Response,getEcardURL, ".date('d-M-Y h:i:s A').", $ip_address, ".trim($response).PHP_EOL.PHP_EOL,FILE_APPEND);
        $encrypted_data=encrypt_data($response);
        return $encrypted_data;
    }
}

$options=array('uri'=>URI_LINK,'encoding'=>'ISO-8859-1',"trace"=>1);
$server = new SoapServer(NULL,$options);
$server->setClass('tpa_api');
$server->handle();