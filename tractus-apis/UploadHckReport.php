<?php
$webserviceid = 7;
include_once 'conf/conf-csapi.php';
/**----------Intiate parameters for return-------------**/
$valid = false;
/**-------------End-------------**/
$response           = array();
/**----------Testing code-------------**/
//$array                  = array('apikey'=>'5975cedf2ef7g','unique_id'=>'R303');
//$str 			= json_encode($array);
//$strData 		= encrypt_data($str);
/**----------End of testing code-------------**/
$strData	= req_param('data');
//echo "<pre>";print_r($_FILES);die;
if ($strData == '') {
    $strError = 'ERR000';
}
if ($strError == '') {
    if (count($_FILES)==0) {
        $strError = 'ERR0025';
    }
    if ($strError == '') {											
        $auth       = AuthenticateApiUser($strData,$webserviceid);
        $getData 	= json_decode(trim($auth),true);
        if ($getData['STATUS']==1) {
            //$valid = true;
            /**----------Main logic goes here -------------**/
            $requested_data         =  $getData['DATA'];
            $response_data          =  $getData['RESPONSE'];
            $userConfiguration = checkAPIconfigurations($response_data['APIUSERID'], $response_data['WSCATEGORYID'], $response_data['APIUSERTOWEBSERICEID'], $response_data['CONFIGURATION']);
            $configData 	= json_decode(trim($userConfiguration),true);

            if ($configData['STATUS'] === true) {
                $center_ids = $configData['CONFIGURATIONDATA'];
               // print_r($center_ids);die;
                //$applist	= json_decode(trim($requested_data),true); 

                if ($center_ids!='') {  
                    $uniqueid   = ''; 
                    $tablename  = '';
                    $historytablename = '';
                    $uniqueid = $requested_data['unique_id'];
                    if ($uniqueid != '' ) {

                        $apptype = $uniqueid[0];
                        if ($apptype == 'R') {
                            $type = 'RETAIL';
                            $tablename          = 'CRMRETAILPOSTAPPOINTMENT';
                            $historytablename   = 'CRMRETAILAPPHISTORY';
                        } else if ($apptype == 'C') {
                            $type = 'CORPORATE';
                            $tablename          = 'CRMCOMPANYPOSTAPPOINTMENT';
                            $historytablename   = 'CRMCOMPANYAPPHISTORY';
                        } else {
                            $strError = 'ERR0020';
                        }
                        if ($strError == '') {
                            $appointmentid = substr($uniqueid,1);
                            if (!is_numeric($appointmentid)) {
                                $strError = 'ERR0020';
                            } else {
                                unset($bAppointmentDetails, $aAppointmentDetails); 
                                dbSelect("SELECT CENTERID FROM ".$tablename." WHERE APPOINTMENTID='".$appointmentid."'", $bAppointmentDetails, $aAppointmentDetails);
                                if ( $bAppointmentDetails ) {
                                    $centerid_array = explode(',', $center_ids);
                                    $_SESSION['userName'] = $requested_data['apikey'];
                                    if (in_array($aAppointmentDetails[0]['CENTERID'], $centerid_array)) {
                                     include_once __DIR__.'/../../religare1/projectconfig/appointment/function/uploadAppointmentReport.php';
                                        $response = uploadAppointmentReport($appointmentid, $type,  $_FILES);
                                        $data 	= json_decode(trim($response),true);
                                        //print_r($data);die;
                                        if ($data['valid'] === true) {
                                            $valid = true;
                                        } else {
                                            $message_details = $data['msgdetails'];
                                            if(isset($message_details) && !empty($message_details)){
                                              foreach($message_details as $key => $message){
                                                  //$strErrorMsg  = $message['msg'];
                                                  $errorMessages[$message['code']] = $message['msg'];
                                                  $strError  = $message['code'];
                                               }
                                            } 
                                        }
                                    }      
                                }
                            }
                        }
                    } else {
                        $strError = 'ERR0021';
                    }

                }  
            }
            /**----------End of main Logic -------------**/
        } else{
            $strError = $getData['ERROR_CODE'];
        }
    }
}
/**-------------Prepare final response to send-------------**/
$response = array(
    "response_status"   => $valid,
    "response_message"  => ($strError!='')?$errorMessages[$strError]:'',
    "error_code"        => $strError,
);
/**-------------End-------------**/
$result = json_encode($response);
/**-------------Create Log-------------**/
file_put_contents('logs/' . date('Y-M', $scriptTime) . '_'.$webservicelog[$webserviceid], "Response, " . $ScriptDateTime . "," . trim($result) . PHP_EOL . PHP_EOL, FILE_APPEND);
/**-------------End Log-------------**/
//echo $result;
echo encrypt_data($result);
?>