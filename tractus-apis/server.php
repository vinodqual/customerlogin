<?php

if (isset($_GET["wsdl"])) {
    require_once ( 'create-wsdl/class.phpwsdl.php' );
    PhpWsdl::RunQuickMode();
    exit;
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
require_once 'conf/conf.php';
//require_once '../../religare1/projectconfig/sqlServerConnect.php';
$conn_sql = db_connect_sql();
$time = time();

class tpa_api {

    private $request_array = null;
    private $ip_address = '';
    private $parent_agent_code = '';

    /**
     * 
     * @author Sanjeev Kumar
     * @todo This function created to get client IP Address
     * 
     */
    private function getClientIP() {
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $this->ip_address = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $this->ip_address = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $this->ip_address = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $this->ip_address = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $this->ip_address = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $this->ip_address = $_SERVER['REMOTE_ADDR'];
        else
            $this->ip_address = 'UNKNOWN';
    }

    /**
     * 
     * @author Sanjeev Kumar
     * @todo This function created for validate username and password.
     * @global type $time
     * @param type $request_data
     * @return string
     * 
     */
    private function validateLogRequest($request_data = array(), $method = '') {
        global $time;
        $decrypted_data = decrypt_data($request_data);
        $this->request_array = json_decode(preg_replace('/[\x00-\x1F\x80-\xFF]/', '', $decrypted_data), true);
        $message = '';
        if (!isset($this->request_array['username'])) { /* Assuming JSON will be correct if user name is there */
            $message.='TPACD0011: ' . TPACD0011 . '</br>';
        }
        if (empty($this->request_array['username'])) {
            $message.='TPACD0002: ' . TPACD0002 . '</br>';
        }
        if (empty($this->request_array['password'])) {
            $message.='TPACD0003: ' . TPACD0003 . '</br>';
        }

        file_put_contents('logs/' . date('Y-M-d', $time) . '.txt', "Request, $method " . date('d-M-Y h:i:s A', $time) . ", $this->ip_address, " . trim($decrypted_data) . PHP_EOL . PHP_EOL, FILE_APPEND);
        return $message;
    }

    /**
     * 
     * @author Sanjeev Kumar
     * @todo This function created to validate user with user name and password.
     * @global type $time
     * @return string
     * 
     */
    private function validateUser() {
        global $time;
        global $conn_sql;
        $message = '';
        $query_agent = "EXEC GET_CRM_PARTNER_MASTER_PARENT_AGENT_CODE  @USER_NAME='" . $this->request_array['username'] . "'  ,@PASS_WORD='" . $this->request_array['password'] . "', @IP_ADDRESS='" . $this->ip_address . "';";
        $std = sqlsrv_query($conn_sql, $query_agent);
        if ($std) {
            while ($row = sqlsrv_fetch_array($std, SQLSRV_FETCH_ASSOC)) {
                $this->parent_agent_code = $row['PARENT_AGENT_CODE'];
            }
            sqlsrv_free_stmt($std);
        } else {
            $message.='TPACD0007: ' . TPACD0007 . '</br>';
        }
        return $message;
    }

    /**
     * 
     * @author Sanjeev Kumar
     * @todo This function created to write response JSON.
     * @param type $response_array
     * @return type
     * 
     */
    private function getNlogResponse($response_array = array(), $method = '') {
        global $time;
        $response = json_encode($response_array);     // Encode array in JSON Format
        file_put_contents('logs/' . date('Y-M-d', $time) . '.txt', "Response, $method " . date('d-M-Y h:i:s A', $time) . ", $this->ip_address, " . trim($response) . PHP_EOL . PHP_EOL, FILE_APPEND);
        return encrypt_data($response);
    }

    /**
     * 
     * @author Sanjeev Kumar
     * @todo This function write to get claim data.
     * @global type $conn
     * @param string $request_data
     * @return string
     * 
     */
    function getClaimData($request_data) {
        global $conn_sql;
        $message = '';
        if ($conn_sql) {
            $data = array();
            $status = '';
            $this->getClientIP();
            $request_array = null;
            $message = $this->validateLogRequest($request_data, 'getClaimData');
            $cond = '';
            if (empty($this->request_array['policy_number'])) {
                $message.='TPACD0001: ' . TPACD0001 . '</br>';
            }
            $date_pattern = '/^[0-3]?[0-9]\/[0-1]?[0-9]\/(19|20)[0-9]{2} (([0-1][0-9])|(2[0-3])):[0-5][0-9]$/';
            if (empty($this->request_array['from_date'])) {
                $message.='TPACD0004: ' . TPACD0004 . '</br>';
            } else if (preg_match($date_pattern, $this->request_array['from_date'])) {
                $cond.=" ,@FROM_DATE='" . $this->request_array['from_date'] . "'";
            } else {
                $message.='TPACD0005: ' . TPACD0005 . '</br>';
            }
            if (!empty($this->request_array['to_date'])) {
                if (preg_match($date_pattern, $this->request_array['to_date'])) {
                    $cond.=", @TO_DATE='" . $this->request_array['to_date'] . "'";
                } else {
                    $message.='TPACD0006: ' . TPACD0006 . '</br>';
                }
            }
        } else {
            $message.='TPACD0000: ' . TPACD0000 . '</br>';
        }
		/* Validate Start and End */
		if(substr($this->request_array['username'], 0, 6) == 'Almond'){
		 if (!empty($this->request_array['start_index']) && !empty($this->request_array['end_index'])) {
                    if (ctype_digit($this->request_array['start_index']) && ctype_digit($this->request_array['end_index'])) {
                        if ($this->request_array['start_index'] > $this->request_array['end_index'] || $this->request_array['start_index'] < 1 || $this->request_array['end_index'] < 1) {
                            $message.='TPAND0005: ' . TPAND0005 . '</br>';
                        } else {
							 $cond.=" ,@start_index='" . $this->request_array['start_index'] . "'  ,@end_index='" . $this->request_array['end_index'] . "'";
						}
                    } else {
                        $message.='TPAND0004: ' . TPAND0004 . '</br>';
                    }
                } else {
                    $message.='TPAND0003: ' . TPAND0003 . '</br>';
                }
		}
		
		
		
        if (empty($message)) {
            $message = $this->validateUser();
            if (!empty($message))
                $status = 'ERROR';
            if (trim($this->parent_agent_code) != '') {
				if(substr($this->request_array['username'], 0, 6) == 'Almond'){
				$query_claim = "EXEC [Almondz_GetClaims]  @POLICY_NUMBER='" . $this->request_array['policy_number'] . "' " . $cond . " ,@AGENT_CODE='" . $this->parent_agent_code . "';";
				}else{
                $query_claim = "EXEC [GETCRMCLAIMDETAILSDATA]  @POLICY_NUMBER='" . $this->request_array['policy_number'] . "' " . $cond . " ,@AGENT_CODE='" . $this->parent_agent_code . "';";
				}
                $std = sqlsrv_query($conn_sql, $query_claim);
                if ($std) {
                    $data = array();
                    while ($row = sqlsrv_fetch_array($std, SQLSRV_FETCH_ASSOC)) {
                        $data[] = $row;
                    }
                    if (count($data) == 0) {
                        $message.='TPACD0008: ' . TPACD0008 . '</br>';
                        $status = 'ERROR';
                    } elseif (count($data) > 0) {
                        $message.='Data found according to your request.</br>';
                        $status = 'SUCCESS';
                    }
                    sqlsrv_free_stmt($std);
                } else {
                    $message.='TPACD0009: ' . TPACD0009 . '</br>';
                    $status = 'ERROR';
                }
            } else {  // End of if check parent_agent_code found in database
                $message.='TPACD0010: ' . TPACD0010 . '</br>';
                $status = 'ERROR';
            }
        } else {
            $status = 'ERROR';
        }

        $response_array = array('response_status' => $status, 'response_message' => $message, 'data' => $data);
        return $this->getNlogResponse($response_array, 'getClaimData');
    }

    /**
     * 
     * @author Sanjeev Kumar
     * @todo This function write for get network details.
     * @global type $conn
     * @param string $request_data
     * @return string
     */
    function getNetworkData($request_data) {
        global $conn_sql;
        $message = '';
        $status = '';
        $retunCount = true;
        if ($conn_sql) {
            $this->getClientIP();
            $request_array = null;
            $message = $this->validateLogRequest($request_data, 'getNetworkData');
            if (empty($this->request_array['count'])) {
                $retunCount = false;
            } else {
                if ($this->request_array['count'] === "false" || $this->request_array['count'] === false) {
                    $retunCount = false;
                }
            }
            if (!$retunCount) {
                if (!empty($this->request_array['start_index']) && !empty($this->request_array['end_index'])) {
                    if (ctype_digit($this->request_array['start_index']) && ctype_digit($this->request_array['end_index'])) {
                        if ($this->request_array['start_index'] > $this->request_array['end_index'] || $this->request_array['start_index'] < 1 || $this->request_array['end_index'] < 1) {
                            $message.='TPAND0005: ' . TPAND0005 . '</br>';
                        }
                    } else {
                        $message.='TPAND0004: ' . TPAND0004 . '</br>';
                    }
                } else {
                    $message.='TPAND0003: ' . TPAND0003 . '</br>';
                }
                //$message.='TPAND0006: '.TPAND0006.'</br>';
            }
        } else {
            $message.='TPACD0000: ' . TPACD0000 . '</br>';
        }
        if (empty($message)) {    // Check if no error occurs
            $message = $this->validateUser();
            if (!empty($message))
                $status = 'ERROR';
            if (trim($this->parent_agent_code) != '') {    // User name and Password match in database
                if ($retunCount) {  // If receive count parameter in request
                    $query = "SELECT count(*) as count FROM CRM_NETWORK_MASTER WHERE STATUS='ACTIVE' AND DISPLAY=1";
                    $std = sqlsrv_query($conn_sql, $query, array(), array("Scrollable" => "static"));
                    if ($std) {
                        $data = '';
                        $row = sqlsrv_fetch_array($std, SQLSRV_FETCH_ASSOC);
                        $data = $row['count'];
                        if ($data !== '') {
                            $message.='Data found according to your request.</br>';
                            $status = 'SUCCESS';
                        }
                        sqlsrv_free_stmt($std);
                    } else {
                        $message.='TPAND0007: ' . TPAND0007 . '</br>';
                        $status = 'ERROR';
                    }
                } else {  // Count parameter does not received in request  
					if(substr($this->request_array['username'], 0, 6) == 'Almond'){
                    $query_network = "EXEC Almondz_GetNetworkHospital  @start_index='" . $this->request_array['start_index'] . "'  ,@end_index='" . $this->request_array['end_index'] . "';";
					}else{
					 $query_network = "EXEC GETNETWORKHOSPITALDETAILS  @start_index='" . $this->request_array['start_index'] . "'  ,@end_index='" . $this->request_array['end_index'] . "';";	
					}
                    $std_network = sqlsrv_query($conn_sql, $query_network);
                    if ($std_network) {
                        while ($row_network = sqlsrv_fetch_array($std_network, SQLSRV_FETCH_ASSOC)) {
                            $data[] = $row_network;
                        }
                        if (!empty($data)) {
                            $message.='Data found according to your request.</br>';
                            $status = 'SUCCESS';
                        } else {
                            $message.='TPAND0008: ' . TPAND0008 . '</br>';
                            $status = 'ERROR';
                        }
                        sqlsrv_free_stmt($std_network);
                    } else {
                        $message.='TPAND0009: ' . TPAND0009 . '</br>';
                        $status = 'ERROR';
                    }
                }   // End of Else block check if count is other than true
            } else {  // End of if check parent_agent_code found in database
                $message.='TPAND0010: ' . TPAND0010 . '</br>';
                $status = 'ERROR';
            }
        } else {  // End of if block check no error occurs
            $status = 'ERROR';
        }

        $response_array = array('response_status' => $status, 'response_message' => $message, 'data' => $data);
        return $this->getNlogResponse($response_array, 'getNetworkData');
    }

    /**
     * 
     * @author Sanjeev Kumar
     * @todo This function written for generate ecard pdg url
     * @global type $conn
     * @param string $request_data
     * @return string
     * 
     */
    function getEcardURL($request_data) {
        global $conn_sql;
        $message = '';
        $data = array();
        $status = '';
        if ($conn_sql) {
            $this->getClientIP();
            $request_array = null;
            $message = $this->validateLogRequest($request_data, 'getEcardURL');
            if (empty($this->request_array['policy_number'])) {
                $message.='TPAEU0001: ' . TPAEU0001 . '</br>';
            }
            if (empty($this->request_array['employee_number'])) {
                $message.='TPAEU0004: ' . TPAEU0004 . '</br>';
            }
            if (isset($this->request_array['employee_number']) && strlen($this->request_array['employee_number']) < 10) {
                $message.='TPAEU0005: ' . TPAEU0005 . '</br>';
            }
        } else {
            $message.='TPACD0000: ' . TPACD0000 . '</br>';
        }
        if (empty($message)) {
            $message = $this->validateUser();
            if (!empty($message))
                $status = 'ERROR';
            if (trim($this->parent_agent_code) != '') {     // User name and Password match in database
                $salt_string = 'qJB0rGtIn5UB1xG03efyCp';       // Salt for validation.
                $send_hash = md5(trim($this->request_array['policy_number']) . trim($this->request_array['employee_number']) . trim($salt_string));
                $encrypted_data = encrypt_data($this->request_array['policy_number'] . $this->request_array['employee_number']);
                $encrypted_data = str_replace(array('+', '/'), array('-', '_'), $encrypted_data);    // Remove + and / from url

                $data = ECARD_URL . $encrypted_data . '/' . $send_hash . '.pdf'; // Ecard pdf link for download
                $message.='Ecard url generated successfully.</br>';
                $status = 'SUCCESS';
            } else {
                $message.='TPAEU0007: ' . TPAEU0007 . '</br>';
                $status = 'ERROR';
            }
        } else {
            $status = 'ERROR';
        }

        $response_array = array('response_status' => $status, 'response_message' => $message, 'data' => $data);
        return $this->getNlogResponse($response_array, 'getEcardURL');
    }

}

$options = array('uri' => URI_LINK, 'encoding' => 'ISO-8859-1', "send_errors" => false);
$server = new SoapServer(NULL, $options);
$server->setClass('tpa_api');
$server->handle();
