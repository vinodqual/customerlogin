<?php
/*-----------SET encryption Key's--------------*/ 
$key 		= "z5yK1lw7XYt6PKdP7Pne2Jw3zRkMAziH";
$iv 		= "i0kbCAlFTlDXshGVCT2IxLOdJ0iWEwqK";
/*-----------END--------------*/ 
/*-----------Include Common config and function files--------------*/ 
include_once __DIR__.'/../../../religare1/projectconfig/db_connect.php';
include_once __DIR__.'/../../../religare1/projectconfig/function/function_general.php';
include_once __DIR__.'/../../../religare1/projectconfig/crypto-functions.php';
$conn = db_connect();
/*-------------End------------*/ 
$strError 	= '';
$baseURL = "https://uat-my.religarehealthinsurance.com/tractus-apis/";
/*-----------------Set current date time variable-----------------*/
date_default_timezone_set('Asia/Kolkata');
$entryTime = date('d-M-Y h:i',time());
$scriptTime 	= time();
$ScriptDateTime	= date('d-M-Y h:i:s A',$scriptTime);	
$updatedby = 'AMIT';
$createdby = 'AMIT';

/*-----------------End of defined variables-----------------*/
/*-----------WS-1 API ERROR MESSAGES--------------*/
$errorMessages = array( 
	'ERR001'=>"Invalid data. Please try again",
	'ERR002'=>"Missing parameters. Please try again",
	'ERR003'=>"Invalid json formate.",
	'ERR005'=>"Policy ID Should be only numeric",
	'ERR006'=>"Unauthorized access invalid key. Please try again.",
	'ERR007'=>"Policy deactived in database.",
	'ERR008'=>"Policy Details not found in database.",
	'ERR009'=>"Your policy is expired.Please contact customer care !",
	'ERR010'=>"policy start-Date is greater then by today date.",
	'ERR011'=>"Company deactived in database.",
	'ERR012'=>"Company details not found in database.",
	'ERR013'=>"Email id can not be blank.",
	'ERR014'=>"Invalid email id.",
	'ERR015'=>"Mobile number can not be blank.",
	'ERR016'=>"Mobile number Should be only numeric.",
	'ERR017'=>"Mobile Number Should have only 10 digits.",
	'ERR018'=>"Status can not be blank.",
	'ERR019'=>"Invalid status.",
	'ERR020'=>"Password can not be blank.",
	'ERR021'=>"Encrypted password can not be blank.",
	'ERR022'=>"Comapny ID can not be blank.",
	'ERR023'=>"Comapny ID Should be only numeric.",
	'ERR024'=>"Policy number can not be blank.",
	'ERR025'=>"Policy ID can not be blank.",
	'ERR026'=>"Employee ID can not be blank.",
	'ERR027'=>"Employee name can not be blank.",
	'ERR028'=>"Customer ID can not be blank.",
	'ERR029'=>"Creation date can not be blank.",
	'ERR030'=>"Ip address can not be blank.",
	'ERR031'=>"Email id already exist againest provided policy number.",
	'ERR032'=>"Record not inserted.",
	'ERR033'=>"Employee Details not found in database.",
	'ERR034'=>"Record not updated.",
	'ERR035'=>"Creation date is not in correct format. (DD-MON-YYYY | 01-JUN-2017).",
	'ERR036'=>"Template ID Should be only numeric.",
	'ERR037'=>"Template type can not be blank.",
	'ERR038'=>"Template details not found in database.",
	'ERR039'=>"Invalid Policy.", //Inserts
);
/*-----------END OF WS-1 API ERROR MESSAGES--------------*/
$status_array 			= array("ACTIVE","DEACTIVE");
?>
