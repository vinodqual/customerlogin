<?php 
define('URI_LINK','https://uat-my.religarehealthinsurance.com/tractus-apis/');
define('ECARD_URL','https://uat-my.religarehealthinsurance.com/e-cards/');

/*---------------------- Database connection Error Message --------------*/
define('TPACD0000','Connection could not be established.'); // This message will display when database connection not established.


/*---------------------- Error Message for Claim Details -----------------
 *
 * @todo TPACD0001 Stands for TPA Claim Data, Last four digits are numeric
 */
define('TPACD0001','Policy number does not found in request parameter.'); // This message will display when policy number does not found.
define('TPACD0002','User name does not found in request parameter.'); // This message will display when user name does not found.
define('TPACD0003','Password does not found in request parameter.'); // This message will display when password does not found.
define('TPACD0004','Start Date does not found in request parameter.'); // This message will display when start date does not found .
define('TPACD0005','Please provide start date in correct format: DD/MM/YYYY HH24:MI.'); // This message will display when start date received in invailid format.
define('TPACD0006','Please provide end date in correct format: DD/MM/YYYY HH24:MI.'); // This message will display when end date received in invailid format.
define('TPACD0007','Unable to process your request.'); // This message will display when select query fails for user name and password authentication.
define('TPACD0008','No records founds according to your request.'); // This message will display when no record found in claim details request
define('TPACD0009','Unable to process your request.'); // This message will display when select query fails for fetch claim details.
define('TPACD0010','Authentication Failed. Please try with correct credentials.'); // This message will display when user name or password incorrect.
define('TPACD0011','Wrong input. Requires ecoded/encrypted JSON with desired parameters.'); // This message will display when policy number does not found.



/*---------------------- Error Message for Network Details -----------------
 *
 * @todo TPAND0001 Stands for TPA Network Data, Last four digits are numeric
 */
define('TPAND0001','User name does not found in request parameter.'); // This message will display when user name does not found.
define('TPAND0002','Password does not found in request parameter.'); // This message will display when password does not found.
define('TPAND0003','Start and End index can not be empty and should be greater than 0.'); // This message will display when count received  false or empty and also start index empty in request parameter.
define('TPAND0004','Start Index and End index should be numeric.'); // This message will display when count received  false or empty and also end index empty in request parameter.
define('TPAND0005',' Start and End index should be greater than 0, and start index should be greater than end index.'); // This message will display when count received  false or empty and also start index does not found in request parameter.
define('TPAND0006','End index does not found in request parameter.'); // This message will display when count received  false or empty and also end index does not found in request parameter.
define('TPAND0007','Unable to process your request.'); // This message will display when select query fails for user name and password authentication.
define('TPAND0008','No records founds according to your request.'); // This message will display when no record found in claim details request
define('TPAND0009','Unable to process your request.'); // This message will display when select query fails for fetch network details.
define('TPAND0010','Authentication Failed. Please try with correct credentials.'); // This message will display when user name or password incorrect.



/*---------------------- Error Message for Ecard Url -----------------
 *
 * @todo TPAEU0001 Stands for TPA e-card url, Last four digits are numeric
 */
define('TPAEU0001','Policy number does not found in request parameter.'); // This message will display when policy number does not found.
define('TPAEU0002','User name does not found in request parameter.'); // This message will display when user name does not found.
define('TPAEU0003','Password does not found in request parameter.'); // This message will display when password does not found.
define('TPAEU0004','Employee number does not found in request parameter.'); // This message will display when employee number does not found .
define('TPAEU0005','Employee number should be of 10 char.'); // This message will display when employee number is less than 10 digit.
define('TPAEU0006','Unable to process your request due to database query error.'); // This message will display when select query fails for user name and password authentication.
define('TPAEU0007','Authentication Failed. Please try with correct credentials.'); // This message will display when no record found in ecard url request



/**
 * Used to connect with database
 *  
 * @return boolean
 */
function db_connect_sql() 
{
	// Qualtech Dev DB Connection
		/*
		$serverName = "10.1.1.6"; //serverName, Port Number
		$connectionInfo = array( "Database"=>"", "UID"=>"", "PWD"=>"");
		// */
		
    // Religare Dev DB Connection
		/*
		$serverName = "10.216.9.198,3433"; //serverName, Port Number
		$connectionInfo = array( "Database"=>"tractus_uat", "UID"=>"tractusuat", "PWD"=>"Passwordtrac#321");
		// */
	
    // QC DB Connection
		/*
		$serverName = "10.216.9.198,3433"; //serverName, Port Number
		$connectionInfo = array( "Database"=>"tractus_uat", "UID"=>"tractusuat", "PWD"=>"Passwordtrac#321");
		// */
		
    // UAT DB Connection
		//*
		$serverName = "10.216.9.198,3433"; //serverName, Port Number
		$connectionInfo = array( "Database"=>"tractus_uat", "UID"=>"tractusuat", "PWD"=>"Passwordtrac#321");
		// */
		
    // Connection with MS SQL Server
    return sqlsrv_connect($serverName, $connectionInfo);
}



function encrypt_data($data){
    $key='z5yK1lw7XYt6YKdP7Pne2Jw3zRkMAziH';    // Encryption Key
    $iv="i0kbCAlFTlDXshYVCT2IxLOdJ0iWEwqK";     // IV Key
    $ciphertext_encrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key,$data, MCRYPT_MODE_CBC, $iv);
    //$ciphertext_encrypt = $iv . $ciphertext_encrypt;
    $ciphertext_base64 = base64_encode($ciphertext_encrypt);
    return $ciphertext_base64;
}
function decrypt_data($data){
    $key='z5yK1lw7XYt6YKdP7Pne2Jw3zRkMAziH';    // Encryption Key
    $iv="i0kbCAlFTlDXshYVCT2IxLOdJ0iWEwqK";     // IV Key
    //$ciphertext_base64_decode = base64_decode($data);
    //$iv_size=strlen($iv);
    //$ciphertext = substr($ciphertext_base64_decode, $iv_size);
	
	$ciphertext = base64_decode($data);
    $plaintext_decrypt = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key,$ciphertext, MCRYPT_MODE_CBC, $iv);
    return $plaintext_decrypt;
}