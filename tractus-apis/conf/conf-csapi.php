<?php
/*-----------Include Common config and function files--------------*/ 
include_once __DIR__.'/../../../religare1/projectconfig/db_connect.php';
include_once __DIR__.'/../../../religare1/projectconfig/function/function_general.php';
include_once __DIR__.'/../../../religare1/projectconfig/crypto-functions.php';
include_once __DIR__.'/../../../religare1/projectconfig/appointment/function/function_api.php';
include_once __DIR__.'/../../../religare1/cs/conf/conf.php';
$conn = db_connect();
/*webservices config name and it's id's*/
$webservicelog[1] = 'HCK_APPOINTMENTS_LISTING_JSON.txt';
$webservicelog[3] = 'HCK_APPOINTMENT_UPDATE.txt';
$webservicelog[7] = 'HCK_REPORT_UPDATE_JSON.txt';
/*-------------End------------*/ 
$strError 	= '';
$baseURL['UAT']     = "https://uat-my.religarehealthinsurance.com/tractus-apis/";
$baseURL['QC']      = "https://qc-my.religarehealthinsurance.com/tractus-apis/";
$baseURL['LOCAL']   = "http://localhost/Tractus/customerlogin/tractus-apis/";
/*-----------------Set current date time variable-----------------*/
date_default_timezone_set('Asia/Kolkata');
$entryTime      = date('d-M-Y h:i',time());
$scriptTime 	= time();
$ScriptDateTime	= date('d-M-Y h:i:s A',$scriptTime);	
$updatedby      = 'AMIT';
$createdby      = 'AMIT';
/*-----------------End of defined variables-----------------*/
/*-----------WS-1 API ERROR MESSAGES--------------*/
$errorMessages  = array (
    'ERR000'        => "Invalid data. Please try again",
    'ERR001'        => "Invalid json formate.",
    'ERR002'        => "Missing parameters. Please try again",
    'ERR003'        => "Missing parameters.", //IP NOT VALIDATED
    'ERR0001'       => "Invalid Request. Please try again.", // API key not found
    'ERR0002'       => "Invalid Request. Please try again.", // password not matched
    'ERR0003'       => "Invalid Request. Please try again.", // Service not mapped with user
    'ERR0004'       => "Invalid username or password. Please try again.", // empty username
    'ERR0005'       => "Invalid username or password. Please try again.", // user name or passsword does not found in database.
    'ERR0006'       => "login id or login type can not be blank.", //empty login id and logintype cente/chain/TPA.
    'ERR0007'       => "Center details not found in database.", //if  getCenterids function return empty.
    'ERR0008'       => "Invalid Configuration. Please contact administrator", //if configuration required and found blank in database
    'ERR0009'       => "Invalid Configuration Details found. Please contact administrator.", //if  switch case defult for configuration 
    'ERR0010'       => "Invalid Configuration Details found. Please contact administrator.", //if CS APPOINTMENT username and user type not found.
    'ERR0011'       => "Query can not be blank.",
    'ERR0012'       => "User details not found. Not valid username", //TPA USERNAME
    'ERR0013'       => "User details not found. Not valid username", //CHAIN USERNAME
    'ERR0014'       => "User details not found. Not valid username", //CENTER USERNAME
    'ERR0015'       => "No Center Mapped with ented username.", //TPA CENTERs
    'ERR0016'       => "No Center Mapped with ented username.", //CHAIN CENTERs
    'ERR0017'       => "From Date can not be null", //if from date is null 
    'ERR0018'       => "Different b/w from date and to date not more then from one day.", //if from date is null 
    'ERR0019'       => "No Appointment found in databse.", //if from date is null 
    'ERR0020'       => "Invalid unique id. Please try again.", //appointment id is not valid.
    'ERR0021'       => "Appointment id or status can not be blank.", //app id or status blank
    'ERR0022'       => "Wrong status change attempt.", //if upper to lower status changes
    'ERR0023'       => "Something went wrong. Please try again.", //if upper to lower status changes
    'ERR0024'       => "SFTP Error message. Please try again.", //if upper to lower status changes
    'ERR0025'       => "Please Select atleast a report to upload.", //if upper to lower status changes
    );
?>
