<?php
function UpdateEmployeeDataIncp($employee_array = array()){
	global $baseURL, $encryptionKey, $encryptionIV, $scriptTime, $ScriptDateTime;
	if (count($employee_array)>0) {
		$data = $employee_array;
		file_put_contents('../logs/' . date('Y-M', $scriptTime) . '_cwsMangeEmployeeData_json.txt', "Request, " . $ScriptDateTime. "," . trim(json_encode($data)) . PHP_EOL . PHP_EOL, FILE_APPEND);
		//$postData 	= encrypt_data($data);
		$url = REGISTER_USER_URL;
		//$requestUrl = $baseURL.'?data='.urlencode($postData);
	
		if($ch = curl_init($url)){
			if (curl_setopt_array($ch, array(CURLOPT_CUSTOMREQUEST=>"POST",
				CURLOPT_RETURNTRANSFER=>true, CURLOPT_POSTFIELDS=>$data,
				CURLOPT_CONNECTTIMEOUT=>10,
				CURLOPT_SSL_VERIFYPEER=>false))
			) {
				$response = curl_exec($ch);
				//print_r($response);die;
				if ($response) {
					$responseData 	= json_decode(trim($response),true);			
					if ($responseData['status']=='true') {
						return "success";
					} else {
						return $responseData['message'];
					}
				}
				file_put_contents('../logs/' . date('Y-M', $scriptTime) . '_cwsMangeEmployeeData_json.txt', "Response, " .$ScriptDateTime. "," . trim($response) . PHP_EOL . PHP_EOL, FILE_APPEND);	
			}
			curl_close($ch); 
		}
	} else {
		return "Invalid Parameters";
	}
}

?>