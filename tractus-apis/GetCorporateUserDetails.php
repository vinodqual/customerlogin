<?php
include_once 'conf/conf-login.php';
/**----------Intiate parameters for return-------------**/
$valid 		= 'VALID';
$is_opd		= 'false';
$policy_number 	= '';
$id_policy  	= '';
$id_company 	= '';
/**-------------End-------------**/
$response 	= array();
/**----------Testing code-------------**/
$array 		= array('policynumber'=>'10073722','employeeid'=>'0000R10442');
$str 			= json_encode($array);
$strData 		= encrypt_data($str);
/**----------End of testing code-------------**/
//$strData	= req_param('data');
if ($strData == '') {
	$strError = 'ERR001';
}
if ($strError == '') {
	$json_array =  	decrypt_data($strData);
	file_put_contents('logs/' . date('Y-M', $scriptTime) . '_GetCorporateUserDetails_json.txt', "Request, " . $ScriptDateTime . "," . trim($json_array) . PHP_EOL . PHP_EOL, FILE_APPEND);
	$postData 	= 	json_decode(trim($json_array),true);
	if ($postData === null && json_last_error() !== JSON_ERROR_NONE) {
	   $strError = 'ERR003';
	} 
	if ($strError == '') {
		$policynumber 	= isset($postData['policynumber'])?$postData['policynumber']:"";
		$employeeid 		= isset($postData['employeeid'])?$postData['employeeid']:"";
		/**----------Validate input parameters-------------**/
		if ($policynumber == '' || $employeeid == '') {
			$strError = 'ERR002';
		} 
		/**----------End of Validate input parameters -------------**/
		if ($strError =='' ) {
			/**----------Main logic goes here -------------**/
			$todayDate = strtotime(date('d-M-Y'));
			$UserDetails = array();			
			dbSelect("SELECT * FROM CRMEMPLOYEELOGIN WHERE POLICYNUMBER = '".$policynumber."' AND EMPLOYEEID = '".$employeeid."'", $bEmployeeList, $aEmployeeList);
			if ($bEmployeeList) {
				/**----------Get User Details by Policy number and employeeid -------------**/
				for($i=0;$i<count($aEmployeeList);$i++){			
					$UserDetails = $aEmployeeList;
				}
				/**----------End Of Get User Details.-------------**/
			} else {
				$strError = 'ERR008';
			}
			/**-------------End of main logic-------------**/
		}
		if ($strError!='') {
			$valid = 'INVALID';
		}
	}		
}
/**-------------Prepare final response to send-------------**/
$response = array("STATUS" => $valid,
	"INVALID_DESC" 	=> ($strError!='')?$errorMessages[$strError]:'',
	"USER_DETAILS" 	=>	$UserDetails,
);

echo "<pre>"; print_r($response);die;

/**-------------End-------------**/
$result = json_encode($response);

/**-------------Create Log-------------**/
file_put_contents('logs/' . date('Y-M', $scriptTime) . '_GetCorporateUserDetails_json.txt', "Response, " . $ScriptDateTime . "," . trim($result) . PHP_EOL . PHP_EOL, FILE_APPEND);

/**-------------End Log-------------**/
echo $result;
//echo encrypt_data($result);
?>