<?php
include_once 'conf/conf-login.php';
/*-----------------Intiate return parameters-----------------*/
$action		= '';
$id			= '';
$response 	= array();
/*-----------------End-----------------*/
/**----------Testing code-------------**/
//$array 		= array('loginid'=>'','companyid'=>'1428','policynumber'=>'10073722','email'=>'amit.choyal@ext.religare.in','password'=>'religare@123','creationdate'=>'01-Jun-2017','ipaddress'=>'172.25.67.177','mobilenumber'=>'9717274997','policyid'=>'2041','employeeid'=>'0000R10000','customerid'=>'50317777','encryptpassword'=>'e035764424e632c2e3186def1c8b7d62','employeename'=>'Amit Choyal','status'=>'ACTIVE');
//$array 		= array('loginid'=>'5602','email'=>'Vishal@test.com','password'=>'religare@123','mobilenumber'=>'9717089608','encryptpassword'=>'e035764424e632c2e3186def1c8b7d62','status'=>'ACTIVE');
//$str 		= json_encode($array);
//$strData 	= encrypt_data($str);
/**----------End of testing code-------------**/
$strData	= req_param('data');
if ($strData == '') {
	$strError = 'ERR001';
}
if ($strError == '') {
	$json_array = decrypt_data($strData);
	file_put_contents('logs/' . date('Y-M', $scriptTime) . '_AddUpdateEmployee_json.txt', "Request, " . $ScriptDateTime . "," . trim($json_array) . PHP_EOL . PHP_EOL, FILE_APPEND);
	$postData 	= json_decode(trim($json_array),true);
	if ($postData === null && json_last_error() !== JSON_ERROR_NONE) {
	   $strError = 'ERR003';
	}
	
	if ($strError == '') {
		$loginid 			= isset($postData['loginid'])?$postData['loginid']:'';
		$mobilenumber		= isset($postData['mobilenumber'])?$postData['mobilenumber']:'';
		$email				= isset($postData['email'])?$postData['email']:'';
		$status				= isset($postData['status'])?$postData['status']:'';
		$password			= isset($postData['password'])?$postData['password']:'';
		$encryptpassword 	= isset($postData['encryptpassword'])?$postData['encryptpassword']:'';
		$companyid			= isset($postData['companyid'])?$postData['companyid']:'';
		$policynumber		= isset($postData['policynumber'])?$postData['policynumber']:'';
		$policyid			= isset($postData['policyid'])?$postData['policyid']:'';	
		$employeeid			= isset($postData['employeeid'])?$postData['employeeid']:'';
		$employeename		= isset($postData['employeename'])?$postData['employeename']:'';
		$customerid			= isset($postData['customerid'])?$postData['customerid']:'';
		$creationdate 		= isset($postData['creationdate'])?$postData['creationdate']:'';
		$ipaddress 			= isset($postData['ipaddress'])?$postData['ipaddress']:'';
		/**----------Validate input parameters for Insert----------*/
		$datePattern = '/^[0-3][0-9]-[a-zA-Z]{3}-[0-9]{4}$/';
		if ($loginid == '') { //Only validate for insert case
			if ($companyid == '') {
				$strError = 'ERR022';
			} elseif (!is_numeric($companyid)) {
				$strError = 'ERR023';
			} elseif ($policynumber == '') {
				$strError = 'ERR024';
			} elseif ($policyid == '') {
				$strError = 'ERR025';
			} elseif (!is_numeric($policyid)) {
				$strError = 'ERR005';
			} elseif ($employeeid =='') {
				$strError = 'ERR026';
			} elseif ($employeename =='') {
				$strError = 'ERR027';
			} elseif ($customerid =='') {
				$strError = 'ERR028';
			} elseif ($creationdate =='') {
				$strError = 'ERR029';
			} elseif (!preg_match($datePattern, $creationdate)) {
				$strError = 'ERR035';
			} elseif ($ipaddress =='') {
				$strError = 'ERR030';
			}	
		}
		if ($email == '') {
			$strError = 'ERR013';
		} elseif (filter_var($email, FILTER_VALIDATE_EMAIL) == false) {
			$strError = 'ERR014';
		} elseif ($mobilenumber =='') {
			$strError = 'ERR015';
		} elseif (!is_numeric($mobilenumber)) {
			$strError = 'ERR016';      
		} elseif (strlen($mobilenumber) < 10 || strlen($mobilenumber)>10) {
			$strError = 'ERR017'; 
		} elseif ($status == '') {
			$strError = 'ERR018';
		} elseif (!in_array($status,$status_array)) {
			$strError = 'ERR019';      
		} elseif ($password == '') {
			$strError = 'ERR020';
		} elseif ($encryptpassword == '') {
			$strError = 'ERR021';
		} 
		/**----------End of Validate input parameters----------**/
		if ($strError == '' ) {
			if ($loginid == '') {
				$array = array(
					'policynumber'=>$policynumber,
					'policyid'=>$policyid,
				);
				$data 		= json_encode($array);
				$postData 	= encrypt_data($data);
				$requestUrl = $baseURL.'GetCorporatePolicyStatus.php?data='.urlencode($postData);
				if ($ch = curl_init()) {
					if (curl_setopt_array($ch,array(CURLOPT_URL => $requestUrl,
													CURLOPT_RETURNTRANSFER => true, 
													CURLOPT_CONNECTTIMEOUT => 30, 
													CURLOPT_SSL_VERIFYPEER => false
												)
										)
					) {
						$response = curl_exec($ch);
						if ($response) {
							$resultData 	= 	json_decode(trim($response),true);
							if ($resultData === null && json_last_error() !== JSON_ERROR_NONE) {
							   $strError = 'ERR003';
							} 
						}
					}
					curl_close($ch);
				}
				if (isset($resultData) && $resultData['STATUS'] == 'VALID') {
					/**----------Insert query go here----------**/ 
					dbSelect("SELECT LOGINID FROM CRMEMPLOYEELOGIN WHERE POLICYNUMBER = '".$policynumber."' AND LOWER(EMAIL)= '".strtolower($email)."' AND STATUS = 'ACTIVE'", $bUserFound, $aUserFound);
					if (!$bUserFound) {
						$check = oci_parse($conn, 'SELECT CRMEMPLOYEELOGIN_SEQ.nextval FROM DUAL');
						oci_execute($check);
						$rest = oci_fetch_assoc($check);
						$rowid = $rest['NEXTVAL'];
						$sql = "INSERT INTO CRMEMPLOYEELOGIN ( LOGINID, COMPANYID, EMPLOYEENAME, EMPLOYEEID, POLICYNUMBER, POLICYID, MOBILENUMBER, CUSTOMERID, PASSWORD, ENCRYPTPASSWORD, EMAIL, CREATIONDATE, STATUS, IPADDRESS,  CREATEDBY, UPDATEDON, UPDATEDBY) values('".$rowid."','" .$companyid."',q'[" .$employeename."]','".$employeeid."','".$policynumber."','".$policyid."','".$mobilenumber."','".$customerid."','".$password."','". $encryptpassword ."','".$email."','".$creationdate."','".$status."','".$ipaddress."','".$updatedby."','".$entryTime."','".$updatedby."')";
						$stdid 	= oci_parse($conn, $sql);
						$result = oci_execute($stdid);
						if ($result) {
							$action = "ADDED";
							$id  = $rowid;
						} else {
							$strError = "ERR032";
						}
					} else {
						$strError 	= 'ERR031';
						$id  		= $aUserFound[0]['LOGINID'];
					}
					/**----------End of Insert date----------**/
				} else {
					$strError = 'ERR039';
				}
			} else {
				/**----------Update query go here----------**/
				dbSelect("SELECT LOGINID FROM CRMEMPLOYEELOGIN WHERE LOGINID = '".$loginid."'", $bUserFound, $aUserFound);
				if ($bUserFound) {
					dbSelect("SELECT LOGINID FROM CRMEMPLOYEELOGIN WHERE POLICYNUMBER = '".$policynumber."' AND LOWER(EMAIL)= '".strtolower($email)."' AND STATUS = 'ACTIVE' AND LOGINID != '".$loginid."'", $bFound, $aFound);
					if (!$bFound) {
						$sql = "UPDATE CRMEMPLOYEELOGIN SET EMAIL = ".NullifyVal($email).", MOBILENUMBER = '".$mobilenumber."', PASSWORD =  '".$password."', ENCRYPTPASSWORD =  '".$encryptpassword."', STATUS = '".$status."', UPDATEDON = '".$entryTime."', UPDATEDBY = '".$updatedby."' WHERE LOGINID = '".$loginid."'";
						$stdid = oci_parse($conn, $sql);
						$result = oci_execute($stdid);
						if ($result) {
							$action = "UPDATED";
						} else {
							$strError = "ERR034";
						}
					}
				} else {
					$strError = "ERR033";
				}
				/**----------End of Update date----------**/
			}
			
		}
		if ($strError!='') {
			$action = "ERROR";
		}
	}		
}
/**-------------Prepare final response to send-------------**/
$response = array(
	"STATUS" => $action,
	"ERROR_DESC" => ($strError!='')?$errorMessages[$strError]:'',
	"id" => $id,
);
/**-------------End-------------**/
$result = json_encode($response);
/**-------------Create Log-------------**/
file_put_contents('logs/' . date('Y-M', $scriptTime) . '_AddUpdateEmployee_json.txt', "Response, " . $ScriptDateTime. "," . trim($result) . PHP_EOL . PHP_EOL, FILE_APPEND);
/**-------------End Log-------------**/
echo $result;
//echo encrypt_data($result);
?>