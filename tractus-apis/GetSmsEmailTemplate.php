<?php
include_once 'conf/conf-login.php';
/**----------Intiate parameters for return-------------**/
$templateid 		= '';
$templatename		= '';
$subject 			= '';
$content  			= '';
$fromemail 			= '';
$fromname 			= '';
$blank_desc 		= '';
/**-------------End-------------**/
$response 	= array();
/**----------Testing code-------------**/
//$array 		= array('templateid'=>'','templatename'=>'RET_DOWNLOAD_DOCUMENT','template_type'=>'EMAIL','policynumber'=>'','productcode'=>'');
//$str 			= json_encode($array);
//$strData 		= encrypt_data($str);
/**----------End of testing code-------------**/
$strData	= req_param('data');
if ($strData == '') {
	$strError = 'ERR001';
}
if ($strError == '') {
	$json_array =  	decrypt_data($strData);
	file_put_contents('logs/' . date('Y-M', $scriptTime) . '_GetSmsEmailTemplate_json.txt', "Request, " . $ScriptDateTime . "," . trim($json_array) . PHP_EOL . PHP_EOL, FILE_APPEND);
	$postData 	= 	json_decode(trim($json_array),true);
	if ($postData === null && json_last_error() !== JSON_ERROR_NONE) {
	   $strError = 'ERR003';
	} 
	if ($strError == '') {
		$templateid 	= isset($postData['templateid'])?$postData['templateid']:"";
		$templatename 	= isset($postData['templatename'])?$postData['templatename']:"";
		$template_type 	= isset($postData['template_type'])?$postData['template_type']:"";
		$policynumber 	= isset($postData['policynumber'])?$postData['policynumber']:"";
		$productcode 	= isset($postData['productcode'])?$postData['productcode']:"";
		/**----------Validate input parameters-------------**/
		if ($templateid == '' && $templatename == '') {
			$strError = 'ERR002';
		} elseif ($templateid && !is_numeric($templateid)) {
			$strError = 'ERR036';
		} elseif ($template_type == '') {
			$strError = 'ERR037';
		}
		/**----------End of Validate input parameters -------------**/
		if ($strError =='' ) {
			/**----------Main logic goes here -------------**/
			dbSelect("SELECT * FROM CRMEMAILTEMPLTE WHERE (TEMPLATEID = '".$templateid."' OR TEMPLATENAME = '".$templatename."')", $bTemplateDetails, $aTemplateDetails);
			if ($bTemplateDetails) {
				$templateid 		= $aTemplateDetails[0]['TEMPLATEID'];
				$templatename		= $aTemplateDetails[0]['TEMPLATENAME'];
				$subject 			= $aTemplateDetails[0]['SUBJECT'];
				$fromemail 			= $aTemplateDetails[0]['FROMEMAIL'];
				$fromname 			= $aTemplateDetails[0]['FROMNAME'];
				if (strtolower($template_type) == 'email') {
					$content = $aTemplateDetails[0]['LARGECONTENT']->load();
				} else {
					$content = $aTemplateDetails[0]['CONTENT'];
				}
			} else {
				$strError = 'ERR038';
			}
			/**-------------End of main logic-------------**/
		}
	}		
}
/**-------------Prepare final response to send-------------**/
$response = array(
	"templateid" 	=> $templateid,
	"templatename" 	=> $templatename,
	"subject" 		=> $subject,
	"content" 		=> base64_encode($content),
	"fromemail" 	=> $fromemail,
	"fromname" 		=> $fromname,
	"blank_desc" 	=> ($strError!='')?$errorMessages[$strError]:'',
);
/**-------------End-------------**/
$result = json_encode($response);
/**-------------Create Log-------------**/
file_put_contents('logs/' . date('Y-M', $scriptTime) . '_GetSmsEmailTemplate_json.txt', "Response, " . $ScriptDateTime . "," . trim($result) . PHP_EOL . PHP_EOL, FILE_APPEND);
/**-------------End Log-------------**/
echo $result;
//echo encrypt_data($result);
?>