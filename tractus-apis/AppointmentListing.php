<?php
$webserviceid = 1;
include_once 'conf/conf-csapi.php';
/**----------Intiate parameters for return-------------**/
$valid = false;
/**-------------End-------------**/
$response           = array();
$appointmentlist    = array();

/**----------Testing code-------------**/
//$array                  = array('apikey'=>'5975cedf2ef7g','from_date'=>'2017-08-10 12:10','to_date'=>'','appointment_type'=>'','policy_number'=>'','status'=>'');
//$str 			= json_encode($array);
//$strData 		= encrypt_data($str);
/**----------End of testing code-------------**/
$strData	= req_param('data');
if ($strData == '') {
        $strError = 'ERR000';
}
if ($strError == '') {
    $auth       = AuthenticateApiUser($strData,$webserviceid);
    $getData 	= json_decode(trim($auth),true);
    if ( $getData['STATUS'] == 1 ) {
        /**----------Main logic goes here -------------**/    
        $requested_data         =  $getData['DATA']; 
        $response_data          =  $getData['RESPONSE'];
        $strWhere               = '';
        $strComapanyWhere       = '';
        $strRetailWhere         = '';
        $userConfiguration = checkAPIconfigurations($response_data['APIUSERID'], $response_data['WSCATEGORYID'], $response_data['APIUSERTOWEBSERICEID'], $response_data['CONFIGURATION']);
        $configData 	= json_decode(trim($userConfiguration),true);
        if ($configData['STATUS'] === true) {
            $center_ids = $configData['CONFIGURATIONDATA'];
            if ($center_ids!='') {
                if ($configData['QUERY']!='') {
                    $valid          = true; 
                    $strWhere = " WHERE CENTERID IN (".$center_ids.")";
                    $strRetailWhere     = '';
                    $strComapanyWhere   = '';
                    /**----Check if filter Apply then create where Clause ---***/
                    $appointment_type           = !empty($requested_data['appointment_type'])?$requested_data['appointment_type']:'';
                    $status                     = !empty($requested_data['status'])?$requested_data['status']:'';
                    $policynumber               = !empty($requested_data['policy_number'])?$requested_data['policy_number']:'';
                    $requestdatefrom            = !empty($requested_data['from_date'])?$requested_data['from_date']:'';
                    $requestdateto              = !empty($requested_data['to_date'])?$requested_data['to_date']:'';
                    
                    //$appointmentdatefrom        = !empty($requested_data['from_appointment_date'])?$requested_data['from_appointment_date']:'';
                    //$appointmentdateto          = !empty($requested_data['to_appointment_date'])?$requested_data['to_appointment_date']:'';
                     if ($requestdatefrom == '') {
                        $strError = 'ERR0017';
                    } else {
                        if ($requestdateto!='') {
                            $datediff = strtotime($requestdateto) - strtotime($requestdatefrom);
                            $days =  floor($datediff / (60 * 60 * 24));
                            if ($days > 1) {
                               $strError = 'ERR0018'; 
                            }
                        } else {
                            $requestdateto      = date('d-M-Y h:i:s A',strtotime($requestdatefrom.' +1 day'));    
                        }
                    }
                    if ($strError == '') {
                         $requestdatefrom    = date('d-M-Y h:i:s A',strtotime($requestdatefrom));
                         $requestdateto      = date('d-M-Y h:i:s A',strtotime($requestdateto));                      
                        // echo $requestdatefrom."===".$requestdateto;die;
                        if ($status!='') {
                             $strWhere .= " AND STATUS = '".strtoupper($status)."'";
                        }
                        if ($policynumber!='') {
                            $strWhere .= " AND POLICYNUMBER = '".$policynumber."'";
                        }

                        if ($requestdatefrom!='' && $requestdateto!='') {
                            $strWhere .= " AND UPDATEDON BETWEEN to_date('".$requestdatefrom."', 'DD-Mon-YYYY HH:MI:SS AM') AND to_date('".$requestdateto."', 'DD-Mon-YYYY HH:MI:SS AM')";
                        }
                        $app_type = '';
                        if ($appointment_type!='') {
                            if ($appointment_type =='RETAIL') {
                                $app_type = 'retail';
                            } else {
                                $app_type = 'corporate';
                            }
                        } 
                        /*if ($appointmentdatefrom!='' && $appointmentdateto!='') {
                            $strWhere .= " AND DATE1 BETWEEN '".$appointmentdatefrom."' AND '".$appointmentdateto."'";
                        }*/
                        /*------------------------------End---------------------------------*/
                        $queries = explode('UNION',$configData['QUERY']);
                        $dbQuery = '';
                        for ($i=0;$i<count($queries);$i++) { 
                            if ($i>0)  $dbQuery .= ' UNION ';
                            if ($app_type=='corporate') $i = 1;
                            $dbQuery .= $queries[$i].$strWhere; 
                        }
                        dbSelect($dbQuery, $bCorpAppointmentList, $aCorpAppointmentList,$totalAppCount);
                        if ($bCorpAppointmentList) {
                            $appointmentlist[] = $aCorpAppointmentList;
                        } else {
                             $strError = 'ERR0019'; 
                        }
                    }
                } else {
                   $strError = 'ERR0011'; 
                }
            } else {
                $strError = 'ERR0007';
            }  
        }
    } else{
        $strError = $getData['ERROR_CODE'];
    }
}
/**-------------Prepare final response to send-------------**/
$response = array(
    "response_status"        => $valid,
    "response_message"  => ($strError!='')?$errorMessages[$strError]:'',
    "error_code"    => $strError,
    "data"          => $appointmentlist,
);
/**-------------End-------------**/
$result = json_encode($response);
/**-------------Create Log-------------**/
file_put_contents('logs/' . date('Y-M', $scriptTime) . '_'.$webservicelog[$webserviceid], "Response, " . $ScriptDateTime . "," . trim($result) . PHP_EOL . PHP_EOL, FILE_APPEND);
/**-------------End Log-------------**/
//echo $result;
echo encrypt_data($result);
?>