<?php
include_once 'conf/conf-login.php';
/**----------Intiate parameters for return-------------**/
$valid 		= 'VALID';
$is_opd		= 'false';
$policy_number 	= '';
$id_policy  	= '';
$id_company 	= '';
/**-------------End-------------**/
$response 	= array();
/**----------Testing code-------------**/
//$array 		= array('policynumber'=>'10073722','policyid'=>'','apikey'=>'6589852354556');
//$str 			= json_encode($array);
//$strData 		= encrypt_data($str);
/**----------End of testing code-------------**/
$strData	= req_param('data');
if ($strData == '') {
	$strError = 'ERR001';
}
if ($strError == '') {
	$json_array =  	decrypt_data($strData);
	file_put_contents('logs/' . date('Y-M', $scriptTime) . '_GetCorporatePolicyStatus_json.txt', "Request, " . $ScriptDateTime . "," . trim($json_array) . PHP_EOL . PHP_EOL, FILE_APPEND);
	$postData 	= 	json_decode(trim($json_array),true);
	if ($postData === null && json_last_error() !== JSON_ERROR_NONE) {
	   $strError = 'ERR003';
	} 
	if ($strError == '') {
		$policynumber 	= isset($postData['policynumber'])?$postData['policynumber']:"";
		$policyid 		= isset($postData['policyid'])?$postData['policyid']:"";
		/**----------Validate input parameters-------------**/
		if ($policynumber == '' && $policyid == '') {
			$strError = 'ERR002';
		} elseif ($strError == '' && $policyid && !is_numeric($policyid)) {
			$strError = 'ERR005';
		}
		/**----------End of Validate input parameters -------------**/
		if ($strError =='' ) {
			/**----------Main logic goes here -------------**/
			$todayDate = strtotime(date('d-M-Y'));
			$policyDetails = array();
			dbSelect("SELECT POLICYID,POLICYNUMBER,COMPANYID,POLICYSTARTDATE,POLICYENDDATE,POLICYRENEWALDATE,STATUS FROM CRMPOLICY WHERE (POLICYNUMBER = '".$policynumber."' OR POLICYID = '".$policyid."')", $bPolicyList, $aPolicyList);
			if ($bPolicyList) {
				$foundActivePolicy = false;
				/**----------Condition :: check for active policy take first active policy if found multiple.  -------------**/
				for($i=0;$i<count($aPolicyList);$i++){			
					if ($aPolicyList[$i]['STATUS'] == 'ACTIVE') {
						$foundActivePolicy = true;
						$policyDetails = $aPolicyList[$i];
						break;
					}
				}
				/**----------End for get policy details.-------------**/
				/**----------Condition :: Check if No active Policy Found-------------**/
				if ($foundActivePolicy === false) {
					$strError = 'ERR007';
				}
				/**----------End of Policy status-------------**/
				/**----------Condition :: current date should be between in policy start date and policy end date-------------**/
				if ($strError == '') {				
					if ($todayDate <= strtotime($policyDetails['POLICYENDDATE'])) {	
						if ($todayDate < strtotime($policyDetails['POLICYSTARTDATE'])) {
							$strError = 'ERR010';
						}
					} else {
						$strError = 'ERR009';
					}	
				}
				/**-------------End of date checking-------------**/
				/**-------------Codition :: Check Company Exist/Active-------------**/
				if ($strError == '') {
					dbSelect("SELECT STATUS FROM CRMCOMPANY WHERE COMPANYID = '".$policyDetails['COMPANYID']."'",$bCompanyDetail,$aCompanyDetail);
					if ($bCompanyDetail) {
						if ($aCompanyDetail[0]['STATUS'] != 'ACTIVE') {
							$strError = 'ERR011';
						}
					} else {
						$strError = 'ERR012';
					}
					$policy_number  = $policyDetails['POLICYNUMBER'];
					$id_policy  	= $policyDetails['POLICYID'];
					$id_company 	= $policyDetails['COMPANYID'];
				}
				/**-------------End of company validation-------------**/
				/**-------------Condition :: Check of OPD availeble or not-------------**/
				if ($strError == '') { 
					dbSelect("SELECT * FROM CRMCORPSUBMODMAP CCSM LEFT JOIN CRMSUBMODULEMASTER CSMM ON CCSM.SUBMODULEID = CSMM.ID WHERE CSMM.SUBMODULENAME='OPD' AND CCSM.POLICYID='".$policyDetails['POLICYID']."'", $bOpd, $aOpd);
					if ($bOpd) {
						$is_opd = 'true';
					}
				}
				/**-------------End of OPD check-------------**/
			} else {
				$strError = 'ERR008';
			}
			/**-------------End of main logic-------------**/
		}
		if ($strError!='') {
			$valid = 'INVALID';
		}
	}		
}
/**-------------Prepare final response to send-------------**/
$response = array("STATUS" => $valid,
	"INVALID_DESC" 	=> ($strError!='')?$errorMessages[$strError]:'',
	"IS_OPD" 		=> $is_opd,
	"POLICYID" 		=> $id_policy,
	"POLICYNUMBER" 	=> $policy_number,
	"COMPANYID" 	=> $id_company,
);
/**-------------End-------------**/
$result = json_encode($response);
/**-------------Create Log-------------**/
file_put_contents('logs/' . date('Y-M', $scriptTime) . '_GetCorporatePolicyStatus_json.txt', "Response, " . $ScriptDateTime . "," . trim($result) . PHP_EOL . PHP_EOL, FILE_APPEND);
/**-------------End Log-------------**/
echo $result;
//echo encrypt_data($result);
?>