<?php
ob_start();
/***
* COPYRIGHT
* Copyright 2016 Qualtech Consultants.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: add-corporate-appointment.php,v 1.0 2016/12/26 11:33:29 amit choyal Exp $
* $Author: amit choyal $
*
****************************************************************************/

require_once "inc/inc.hd.php";
$strErrorMessage = "";
if(isset($_SESSION['response_error'])){
	$strErrorMessage  = $_SESSION['response_error'];
} else {
	$responseArray = $_SESSION['dataArray'];
}
$ptype = 'customerlogin';
include_once __DIR__.'/../religare1/projectconfig/function/function_general.php';
include_once __DIR__.'/../religare1/projectconfig/appointment/classes/listMemberPlan.php';
include_once __DIR__.'/../religare1/projectconfig/appointment/function/function.php';
$loggedusername = $_SESSION['userName'];
$listMemberPlanArr  = array();
$planId				= base64_decode(req_param('id'));
if ($planId	== '' || !is_numeric($planId)) {
	header('Location: health_checkup.php?tab=bXlwbGFu');
}
$companyname 		= req_param('companyname');
$companyid 			= req_param('companyid');
$policyid 			= req_param('policyid');
$productcode 		= req_param('productcode');
$productid 			= req_param('productid');
$primarymemberid 	= req_param('primarymemberid');
$policynumber 		= $_SESSION['policyNumber'];
$empname 			= $_SESSION['empname'];
//echo "<pre>";print_r($_SESSION);die;
if ($policynumber != '' && count($_SESSION['dataArray'])>0) {
	$loggedusername = strtolower($loggedusername);
	if ($_SESSION['LOGINTYPE'] == 'CORPORATE') {
		$utype = 'corporate';
		$query_corp = "SELECT CUSTOMERID FROM CRMEMPLOYEELOGIN WHERE lower(EMAIL) like '".$loggedusername."' AND policynumber = '".$_SESSION['policyNumber']."' AND STATUS = 'ACTIVE'";
		dbSelect($query_corp, $buserdetails, $auserdetails);
		if ($buserdetails) {
			$customerid = $auserdetails[0]['CUSTOMERID'];
			$listMemberPlan		=	new listMemberPlan();
			$listMemberPlanArr	=	$listMemberPlan->getMemberDetail($_SESSION['policyNumber'], $customerid, $_SESSION['dataArray'], '', $planId, true);
			$companyname 		= $_SESSION['owner_name'];
			$companyid 			= $_SESSION['COMPANYID'];
			dbSelect("SELECT POLICYID FROM CRMPOLICY WHERE POLICYNUMBER = '".$policynumber."' AND COMPANYID = '".$companyid."'",$bPolicy,$aPolciy);
			if ($bPolicy) {
				$policyid 			= $aPolciy[0]['POLICYID'];
			}
		}
	} else {
		$utype = 'retail';
		$query_ret = "SELECT RETUSERAUTH.USERID, RETUSERPOLICYMAP.CUSTOMERID FROM RETUSERAUTH LEFT JOIN RETUSERPOLICYMAP ON RETUSERAUTH.USERID = RETUSERPOLICYMAP.USERID WHERE LOWER(RETUSERAUTH.USERNAME) like '".$loggedusername."' AND RETUSERPOLICYMAP.POLICYNUM = '".$_SESSION['policyNumber']."' AND RETUSERAUTH.USERSTATUS = 'ACTIVE' AND (RETUSERPOLICYMAP.CUSTOMERID IS NOT NULL OR RETUSERPOLICYMAP.CUSTOMERID!='')";
		dbSelect($query_ret, $bretuserdetails, $aretuserdetails);
		if ($bretuserdetails) {
			$customerId = $aretuserdetails[0]['CUSTOMERID'];
			$listMemberPlan		=	new listMemberPlan();
			$listMemberPlanArr	=	$listMemberPlan->getMemberDetail($_SESSION['policyNumber'], $customerId, $_SESSION['dataArray'],  $_SESSION['productCode'], $planId, true);////10073722, 50317769, ARRAY(), 10001101
			$productcode 		= $_SESSION['productCode'];
			$productid 			= $_SESSION['productId'];
		}
	}
	if (count($listMemberPlanArr)>0) {
		$customerid 		= $_SESSION['clientNumber'];
		$primarymemberid 		= $_SESSION['dataArray'][0]['BGEN-CLNTNM']['#text'];
		$memberDetailArr = $_SESSION['dataArray'];
		$memberListArr			=	array();
		$m						=	0;
		foreach($memberDetailArr as $memberDetailVal)
		{
			$memberListArr[$m]['QC-MEMNAME']	=	$memberDetailVal['BGEN-MEMNAME']['#text'];
			$memberListArr[$m]['QC-EMPNO']		=	$memberDetailVal['BGEN-EMPNO']['#text'];
			$memberListArr[$m]['QC-RELATIONNAME']=	ucwords($relationArray[$memberDetailVal['BGEN-DPNTTYP']['#text']]);
			$memberListArr[$m]['QC-DPNTTYP']	=	$memberDetailVal['BGEN-DPNTTYP']['#text'];
			$memberListArr[$m]['QC-PLANNO']		=	$memberDetailVal['BGEN-PLANNO']['#text'];
			$memberListArr[$m]['QC-AGE']		=	$memberDetailVal['BGEN-AGE']['#text'];
			$memberListArr[$m]['QC-DOB']		=	$memberDetailVal['BGEN-DOB']['#text'];
			$memberListArr[$m]['QC-GENDER']		=	$memberDetailVal['BGEN-GENDER']['#text'];
			$memberListArr[$m]['QC-EMAIL']		=	$memberDetailVal['BGEN-EMAIL']['#text'];
			$memberListArr[$m]['QC-CLNTNM']		=	$memberDetailVal['BGEN-CLNTNM']['#text'];
			$memberListArr[$m]['QC-CLTPHONE']	=	$memberDetailVal['BGEN-CLTPHONE']['#text'];
			$memberListArr[$m]['QC-CLTPHONE02']	=	$memberDetailVal['BGEN-CLTPHONE02']['#text'];
			$memberListArr[$m]['QC-CLTPHONE03']	=	$memberDetailVal['BGEN-CLTPHONE03']['#text'];
			$memberListArr[$m]['QC-ZBALCURR']	=	$memberDetailVal['BGEN-ZBALCURR']['#text'];
			$m++;
		}
		$planArr[0]['MEMBERS']	=	$memberListArr;
		$planArr[1]				=	$listMemberPlanArr[0];
		$planlist = json_encode($planArr);
	} else {
		$strErrorMessage = "Plan details not Found. Please contact customer care.";
	}
} else {
	$strErrorMessage = "Essential data is missing. Please try again.";
}
?>
<script type="text/javascript">
var csplanmember = <?php echo $planlist; ?>;
$(document).ready(function() {
	var num = 1;
	var planandmembers =  <?php echo $planlist; ?>;
	var finalplanlist = planandmembers;
	for (var i = num;i<finalplanlist.length;i++) {
		var plantype = ((finalplanlist[i]['PLANTYPE'])?finalplanlist[i]['PLANTYPE']:finalplanlist[i]['PAYMENTOPTIONS']);
		var planname = ((finalplanlist[i]['PLANNAME2'])?finalplanlist[i]['PLANNAME2']:finalplanlist[i]['PLANNAME'])+' - '+plantype;
		var option=$('<option value="'+finalplanlist[i]['PLANID']+'||'+plantype+'"></option>').text(planname);
		$('#planid').append(option);
	}
});
</script>
<link rel="stylesheet" href="css/jquery-ui.css" />
<section id="middleContainer">
  <div class="container-fluid">
    <div class="middlebox">
		<div class="col-md-9">
			<div class="middlebox-left">
				<?php include('inc/inc.healthplan_tab.php'); ?>
				<div class="tab-content"  id="element">
					<div class="tab-pane active" id="myPlans">
						<div class="tab-paneIn">
							<div class="title-bg textleft">
								<h1>Appointment Request Scheduler (<?php echo $listMemberPlanArr[0]['PLANNAME']?$listMemberPlanArr[0]['PLANNAME']:$listMemberPlanArr[0]['PLANNAME2']; ?>)</h1>
							</div>
							<div class="alert-message-error" id="planmsg" style="<?php echo (($strErrorMessage!='')?'display:block;':'display:none;') ?>">	
							<?php echo $strErrorMessage; ?>
							</div>
							<div id="errordiv"><ul class="errordiv"></ul></div>
							 <form name="appointmentForm" id="appointmentForm" method="post" >
								<div class="grayBorder">
									<h2>User Details</h2>
									<div style="display:none;">
									<select class="txtfield_185 plandetails" name="planid" id="planid" >
									<option value="">Select Plan</option>
								  </select></div>
									<div class="myPlanForm">
										<div class="myPlanformBox myPlanLeft">
											<label>Emp Id / Client Id</label>
											<div class="inputBox">
												<div class="inputBoxIn">
												  <input type="text" name="employeeid" class="email_f txtfield_app txtField" id="empid" readonly="yes" value="<?php echo ($_SESSION['LOGINTYPE']=='CORPORATE')?$_SESSION['empId']:$_SESSION['customerId'];?>">
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="myPlanformBox myPlanRight">
											<label>Email Id <span class="greenTxt">*</span></label>
											<div class="inputBox">
												<div class="inputBoxIn">
												  <input type="text" name="email" id="email"  class="email_f txtfield_app txtField" maxlength="255" AUTOCOMPLETE="OFF" />	
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="myPlanformBox myPlanLeft">
											<label>Mobile <span class="greenTxt">*</span></label>
											<div class="inputBox">
												<div class="inputBoxIn">
												   <input type="text" name="numbers" id="numbers" class="email_f txtfield_app txtField"  maxlength="10" AUTOCOMPLETE="OFF" onKeyPress="return kp_numeric(event)" />
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="myPlanformBox myPlanRight">
											<label>Address </label>
											<div class="inputBox">
												<div class="inputBoxIn">
													<input type="text" name="address" id="address" class="txtField" value="">
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
									<h2>Appointment Listing</h2>
									<div class="myPlanForm">
										<div class="myPlanformBox" style="width:100%;">
										<table width="100%">
											<thead>
												<tr>
													<th>Gender</th>
													<th>Relation</th>
													<th>Name</th>
													<th>Age</th>
													<th>Client ID</th>
													<th>Add</th>
												</tr>
											</thead>
											<tbody id='memberlist'>
												<td class="apperror" colspan="6">Plan Details Not Found. Please try again.</td>
											</tbody>
										</table> 
										</div>
									</div>
									<h2>Location</h2>
									<div class="myPlanForm">
										<div class="myPlanformBox myPlanLeft">
											<label>State <span class="greenTxt">*</span></label>
											<div class="inputBox">
												<div class="inputBoxIn dropBg">
												 <select class="txtfield_185 centerdetail" id="stateid" name="stateid"><option value="">Select State</option></select>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="myPlanformBox myPlanRight">
											<label>City <span class="greenTxt">*</span></label>
											<div class="inputBox">
												<div class="inputBoxIn dropBg" id="city">
												  <select class="txtfield_185 centerdetail" id="cityid" name="cityid"><option value="">Select City</option></select>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="myPlanformBox myPlanLeft">
											<label>Center <span class="greenTxt">*</span></label>
											<div class="inputBox">
												<div class="inputBoxIn dropBg" id="center">
												 <select id="centerid" class="txtfield_185 centerdetail" name="centerid"><option value="">Select Center</option></select>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="myPlanformBox myPlanRight">
											<label>Locality/Center</label>
											<div class="inputBox">
												<div class="inputBoxIn">
												 <input type="text" name="centername" id="center_autocomplete" maxlength="15" class="txtfield_app txtField" readonly>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
									<h2>Appointment Date / Time</h2>
									<div class="myPlanForm">
										<div class="myPlanformBox myPlanLeft">
											<label>Date</label>
											<div class="inputBox">
												<div class="inputBoxIn dateBg">
												  <input readonly id="appointmentdate" name="date1" type="text" class="txtfield_app txtField Date"  value="" /> 
												</div>
												<img width="25" height="25" src="images/loading.gif" class="loadingimage" id="dateloadingimage" style="display:none;">
											</div>
											<div class="clearfix"></div>
										</div>
										<div class="myPlanformBox myPlanRight">
											<label>Time Slot</label>
											<div class="inputBox">
												<div class="inputBoxIn dropBg" id="timeslotdata1">
												<select   name="timeslot" class="txtfield_185" id="timeslot">
													<option value="">Select Slot</option>
												</select>
												</div>
											</div>
											<div class="clearfix"></div>
										</div>
									</div>
									<div class="clearfix"></div>
									<div class="myPlanBtn" style="margin-top:30px;">
										<input type="hidden" name="policynumber" value="<?php echo $policynumber; ?>" id="policynumber" />
										<input type="hidden" name="empname" value="<?php echo $empname; ?>" id="empname" />
										<input type="hidden" name="companyname" value="<?php echo $companyname; ?>" id="companyname" />
										<input type="hidden" name="productcode" value="<?php echo $productcode; ?>" id="productcode" />
										<input type="hidden" name="customerid" value="<?php echo $customerid; ?>" id="customerid" />
										<?php
										echo '<input type="hidden" value="' . $utype . '" name="portal" id="portal">';
										echo '<input type="hidden" value="' . $ptype . '" name="ptype" id="ptype">';
										?>
										<input type='hidden' name='primarymemberid' value='<?php echo $primarymemberid;  ?>' id='primarymemberid'/> 
										<input type='hidden' name='policyid' value='<?php echo $policyid;  ?>'  id='policyid'/>
										<input type='hidden' name='companyid' value='<?php echo $companyid;  ?>' id='companyid'/>
										<input type='hidden' name='productid' value='<?php echo $productid;  ?>' id='productid'/>	
										
										<img width="30" height="30" src="img/loading.gif" class="loadingimage" id="loadingimage2" style="display:none; float:right;">
										<a id="AddAppointmentform" href="javascript:void(0);" class="submit-btn">Submit &gt;</a>
										<a href="Health_checkup.php?tab=ZXhpc3RpbmdhcHA=" class="cancel-btn">Cancel &gt;</a>										
										<input type="reset" class="gobtn fr submitbutton" value="Reset" id="resetform"   style="display:none;">
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-3">
			<?php include("inc/inc.right.php"); ?>
		</div>      
    </div>
  </div>
</section>
<?php include("inc/inc.ft.php"); ?>

<link rel="stylesheet" href="css/jquery-ui.css" />
<script type="text/javascript" src="js/loadingoverlay.min.js"></script>
<?php 
	require __DIR__."/../religare1/projectconfig/appointment/js/appointment.inc"; 
	require __DIR__."/../religare1/projectconfig/appointment/css/appointmentbooking.inc"; 
 ?>

<script>
$(document).ready(function() {
	$("#planid").prop("selectedIndex", 1);
	$('#planid').change();
});
</script>
<script type='text/javascript'>
function example5(msg_content, focus_Id) {
    //var $ = jQuery.noConflict();
	$('#planmsg').html('');
	$('#planmsg').html(msg_content);
	$('#planmsg').show();
	$('#planmsg').delay(3500).fadeOut(1500);
	$('html,body').animate({
		scrollTop: $("#myPlans").offset().top - 100},
	'slow');	
}
</script>