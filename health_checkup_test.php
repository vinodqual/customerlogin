<?php include("inc/inc.hd.php");
$planId = base64_decode(sanitize_data(@$_REQUEST['pid']));
if($_SESSION['LOGINTYPE']=='CORPORATE'){
$dlink = base64_decode(sanitize_data(@$_REQUEST['dlink']));
	  $healthCheckPlans=fetchhealthPlanList("where CRMCOMPANYHEALTHCHECKUPPLAN.POLICYID='".@$wellnessList[0]['POLICYID']."' AND CRMCOMPANYHEALTHCHECKUPPLAN.STATUS='ACTIVE' AND CRMCORPHEALTHCHECKUP.STATUS='ACTIVE' AND CRMCOMPANYHEALTHCHECKUPPLAN.PAYMENTOPTIONS='BUY'  ");  //function to fetch records from database
	  
	  $planId = $planId?$planId:@$healthCheckPlans[0]['PLANID'];
	if(isset($planId) && !empty($planId)){
		$planDetailsById = fetchListCondsWithColumn('EDITEDPRICE,DISCOUNTPRICE,AGEGROUPID,SI,GENDER,PLANTYPEID,PLANNAMEID,DESCRIPTION,UPLOADPDF',"CRMCOMPANYHEALTHCHECKUPPLAN","where CRMCOMPANYHEALTHCHECKUPPLAN.POLICYID='".@$wellnessList[0]['POLICYID']."' AND CRMCOMPANYHEALTHCHECKUPPLAN.STATUS='ACTIVE' AND CRMCOMPANYHEALTHCHECKUPPLAN.PAYMENTOPTIONS='BUY' AND CRMCOMPANYHEALTHCHECKUPPLAN.PLANID=".@$planId." ");
		$xolplanDetailsById = fetchListCondsWithColumn('CORPPLANTYPEID',"CRMCORPHEALTHCHECKUP","where CRMCOMPANYHEALTHCHECKUPPLAN.STATUS='ACTIVE' AND CRMCORPHEALTHCHECKUP.CORPPLANTYPEID=".@$planId." ");
	$masterPlanId = $planDetailsById[0]['PLANNAMEID'];
	} else {
	$masterPlanId = $healthCheckPlans[0]['PLANNAMEID'];
	}
} else {
	$healthCheckPlans=fetchRetailhealthPlanList("where CRMRETAILHEALTHCHECKUPPLAN.PRODUCTID=".@$_SESSION["productId"]." AND CRMRETAILHEALTHCHECKUPPLAN.STATUS='ACTIVE' AND CRMCORPHEALTHCHECKUP.STATUS='ACTIVE' AND CRMRETAILHEALTHCHECKUPPLAN.PAYMENTOPTIONS='BUY'  ");  //function to fetch records from database
	$planId = $planId?$planId:@$healthCheckPlans[0]['PLANID'];
	if(isset($planId) && !empty($planId)){
		$planDetailsById = fetchListCondsWithColumn('EDITEDPRICE,DISCOUNTPRICE,UPLOADPDF,AGEGROUPID,SI,GENDER,PLANTYPEID,PLANNAMEID,DESCRIPTION',"CRMRETAILHEALTHCHECKUPPLAN","where CRMRETAILHEALTHCHECKUPPLAN.PRODUCTID=".@$_SESSION["productId"]." AND CRMRETAILHEALTHCHECKUPPLAN.STATUS='ACTIVE' AND CRMRETAILHEALTHCHECKUPPLAN.PAYMENTOPTIONS='BUY' AND CRMRETAILHEALTHCHECKUPPLAN.PLANID=".@$planId." ");
		$xolplanDetailsById = fetchListCondsWithColumn('CORPPLANTYPEID,UPLOADPDF',"CRMCORPHEALTHCHECKUP","where CRMRETAILHEALTHCHECKUPPLAN.STATUS='ACTIVE' AND CRMCORPHEALTHCHECKUP.CORPPLANTYPEID=".@$planId." ");
	$masterPlanId = $planDetailsById[0]['PLANNAMEID'];
	} else {
	$masterPlanId = @$healthCheckPlans[0]['PLANNAMEID'];
	}
}

if(isset($_POST["AddDetails"])){
   	$corpplanid=sanitize_data(@$_POST['corpplanid']);
   	$totalSumInputval=sanitize_data(@$_POST['totalSumInputval']);
   	$membersid=sanitize_data(@$_POST['membersid']);
   	$membersprice=sanitize_data(@$_POST['membersprice']);
	
   	$membersidwithprice=array_combine(array_values($membersid),array_values($membersprice));
	$membersid = json_encode($membersid);
	$membersprice = json_encode($membersprice);
	$membersidwithprice = json_encode($membersidwithprice);
	$entryTime=date('d-m-Y H:i');
	
	if($_SESSION['LOGINTYPE']=='CORPORATE'){
			 $sql="INSERT INTO CRMEMPLOYEESELECTEDPLAN(ID,CORPPLANID,HEALTHCHECKUPPLANID,TOTALPRICE,MEMBERIDS,MEMBERPRICES,MEMBERIDWITHPRICES,CREATEDON,CREATEDBY,UPDATEDON,UPDATEDBY,POLICYNUMBER,POLICYID,COMPANYID,CUSTOMERID,EMPLOYEEID,EMAILID) values(CRMEMPLOYEESELECTEDPLAN_SEQ.nextval,'".@$planId."','".@$masterPlanId."','".@$totalSumInputval."','".@$membersid."','".@$membersprice."','".@$membersidwithprice."','".@$entryTime."','".@$_SESSION['userName']."','".@$entryTime."','".@$_SESSION['userName']."','".@$_SESSION['policyNumber']."',".@$wellnessList[0]['POLICYID'].",".@$_SESSION["COMPANYID"].",".@$_SESSION["customerIdCheck"].",".@$_SESSION["employeeIdCheck"].",'".@$_SESSION['userName']."') "; //query to insert record
						$stdid = @oci_parse($conn, $sql);
						$r = @oci_execute($stdid);
		//fetch current inserted healthcheckup plan id//
		   $qu2="select CRMEMPLOYEESELECTEDPLAN_SEQ.currval from dual";
		   $stdids2 = @oci_parse($conn, $qu2);
			@oci_execute($stdids2);
		   $lastinsertId2=oci_fetch_assoc($stdids2);
		   $currentplanInsertedId=$lastinsertId2['CURRVAL'];
		   $_SESSION["CURRENTPLANID"] = $currentplanInsertedId;
	
	} else {
			$sql="INSERT INTO CRMRETEMPLOYEESELECTEDPLAN(ID,CORPPLANID,HEALTHCHECKUPPLANID,TOTALPRICE,MEMBERIDS,MEMBERPRICES,MEMBERIDWITHPRICES,CREATEDON,CREATEDBY,UPDATEDON,UPDATEDBY,POLICYNUMBER,POLICYID,PRODUCTID,CUSTOMERID,EMPLOYEEID,EMAILID) values(CRMRETEMPLOYEESELECTEDPLAN_SEQ.nextval,'".@$planId."','".@$masterPlanId."','".@$totalSumInputval."','".@$membersid."','".@$membersprice."','".@$membersidwithprice."','".@$entryTime."','".@$_SESSION['userName']."','".@$entryTime."','".@$_SESSION['userName']."','".@$_SESSION['policyNumber']."','".@$wellnessList[0]['POLICYID']."',".@$_SESSION["productId"].",".@$_SESSION["customerId"].",'".@$_SESSION["employeeIdCheck"]."','".@$_SESSION['userName']."') "; //query to insert record
						$stdid = @oci_parse($conn, $sql);
						$r = @oci_execute($stdid);
		//fetch current inserted healthcheckup plan id//
		   $qu2="select CRMRETEMPLOYEESELECTEDPLAN_SEQ.currval from dual";
		   $stdids2 = @oci_parse($conn, $qu2);
			@oci_execute($stdids2);
		   $lastinsertId2=oci_fetch_assoc($stdids2);
		   $currentplanInsertedId=$lastinsertId2['CURRVAL'];
		   $_SESSION["CURRENTPLANID"] = $currentplanInsertedId;
	
	}
				
?>
<script>window.location.href='buy_plan.php?id=<?php echo base64_encode(@$planId); ?>'</script> <!--locate to cms_listing.php -->
<?php		
       
}

if($_SESSION['LOGINTYPE']=='RETAIL' && base64_decode(@$_REQUEST['tab'])!='myplan'){
	echo "<script>window.location.href='existing_appointments.php?tab=".base64_encode('existingapp')."'</script>";
}

?>

<script language="javascript" src="js/encrypt.js"></script>
<script>
function addtotalprice(){
	
    var total = 0;
    $("input[type=checkbox]:checked").each(function() {
        total += parseFloat($(this).val());
    });
    $(".totalSum").html(total+'/-');
    $(".totalSumInputval").val(total);
	
}
$(document).ready(function(){ 
              $('.HealthChkupPlanList li').click(function()
                   {
				   	var values=$(this).attr('data-value');
                       window.location.href='health_checkup.php?pid='+base64_encode(values);
                   });
 });
function validateHealthCheckupForm(){

	var corpplanid=$("#corpplanid").val();
	var price=$("#totalSumInputval").val();
    member_count = $('.membercount:checked').length;
    if (member_count == 0){
		alert("Please select at least one member.");
		return false;
	}
	if(corpplanid == ''){
		alert("Please select plan.");
		return false;
	}
	if(price == '' || price <= 0){
		alert("Price cannot be empty.");
		return false;
	}
	$("#HealthChkupPlanList").submit();
}
</script>
<link media="screen" rel="stylesheet" href="css/jquery-ui.css" />
<section id="middleContainer">
   <div class="container-fluid">
        <div class="middlebox"> 
            <div class="col-md-9">
               <div class="middlebox-left">
          <?php include('inc/inc.healthplan_tab.php'); ?>
          <div class="tab-content ">
          <?php if($_SESSION['LOGINTYPE']=='CORPORATE'){ ?>
            <div class="tab-pane <?php if(empty($tab) || $tab=="buyplan"){ echo "active"; } ?>" id="buyPlan">
                <div class="tab-paneIn"> 
                    <form name="HealthChkupPlanList" id="HealthChkupPlanList" class="HealthChkupPlanList" method="post" action="">
                  <div class="col-md-6">
                    <div class="tab-left-container">
                      <h1><img src="img/health-check-up.png" alt="" title=""><?php if(isset($dlink) && !empty($dlink)){ ?>Paid Tender Care Plans <?php } else { ?>Paid Health Check-up Plans<?php } ?></h1>
                      <div class="con-in ">
                        
                          <select class="cs-select cs-select1 cs-skin-border fa arrow " name="corpplanid" id="corpplanid" >
                           
                                <?php if(count($healthCheckPlans) > 0){
									if(isset($dlink) && !empty($dlink)){
										$dlink=explode("_",$dlink);
										$dlink_res1=$dlink[0];
										$dlink_res2=$dlink[1];
									}

                                $v=0;
                                    while($v<count($healthCheckPlans)){
										$seldata=0;
										if(($dlink_res2==true)){
											$seldata=1;
										}
										if($seldata==1){
											if(($dlink_res1==$healthCheckPlans[$v]['PLANID'])){
                                ?>
                                <option value="<?php echo @$healthCheckPlans[$v]['PLANID']; ?>" <?php if(@$planId == @$healthCheckPlans[$v]['PLANID']){ echo "selected";} ?>> <?php echo trim(@$healthCheckPlans[$v]['PLANNAME'])?stripslashes(@$healthCheckPlans[$v]['PLANNAME']):@$healthCheckPlans[$v]['PLANNAME2']; ?></option>
                                <?php		}		
								} else {
                                ?>
                                <option value="<?php echo @$healthCheckPlans[$v]['PLANID']; ?>" <?php if(@$planId == @$healthCheckPlans[$v]['PLANID']){ echo "selected";} ?>> <?php echo trim(@$healthCheckPlans[$v]['PLANNAME'])?stripslashes(@$healthCheckPlans[$v]['PLANNAME']):@$healthCheckPlans[$v]['PLANNAME2']; ?></option>
                                <?php		}		
                                    $v++; }
                                } else { ?>
                                <option value="">No Plans Available</option>
                                <?php } ?>
                            
                          </select>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="stepProgress">
                      <div class="stepProgressBar"></div>
                      <div class="stepProgressIn"><span class="stepactive">1</span><span class="stepTxt">Select Plan</span></div>
                      <div class="stepProgressIn"><span class="stepBar">2</span><span class="stepTxt">Enter Detail</span></div>
                      <div class="stepProgressIn"><span class="stepBar">3</span><span class="stepTxt">Review</span></div>
                      <div class="stepProgressIn"><span class="stepBar">4</span><span class="stepTxt">Payment</span></div>
                      <div class="cl"></div>
                    </div>
                  </div>
                  <div class="clearfix"></div>
                  <p>
					<?php  echo @$planDetailsById[0]['DESCRIPTION']?stripslashes(@$planDetailsById[0]['DESCRIPTION']):'NA'; ?>
					</p>
                <?php if($_SESSION['LOGINTYPE']=='CORPORATE'){ 
						$memberList = $planDetailsById[0]['EDITEDPRICE']?json_decode($planDetailsById[0]['EDITEDPRICE']):''; 
	                    $discountList = @$planDetailsById[0]['DISCOUNTPRICE']?json_decode(@$planDetailsById[0]['DISCOUNTPRICE'],true):''; 
						
						$totalprice = 0;
						$breaktext='';
						
				?>
                  <div class="tab-mid-container">
                    <table class="" width="100%">
                      <tr>
                        <th>Select Member</th>
                        <th>MRP</th>
                        <?php if(@$discountList[1]!=''){ $breaktext='break-text'; ?>
                        <th>Discounted Rates</th>
                        <?php } ?>
                        <th>&nbsp;</th>
                      </tr>
	                    <?php 
					
						if(isset($memberList) && !empty($memberList)){
						$k=1;
						foreach($memberList as $memberid=>$price){
						 $memberName = fetchListCondsWithColumn('TITLE,MEMBERTYPEID',"CRMCORPMEMBERTYPE","where STATUS='ACTIVE' AND CRMCORPMEMBERTYPE.MEMBERTYPEID=".@$memberid." ");
						if(isset($price) && !empty($price)){
						if(strtolower(stripslashes(@$memberName[0]['TITLE'])) == 'self'){
						$_SESSION['membersarray'] = array_merge(array('self'),$_SESSION['membersarray']);
						} 
						
						if(in_array(strtolower(stripslashes(@$memberName[0]['TITLE'])),$_SESSION['membersarray'])){
						//printr($_SESSION['membersarray']);
						$price = floatval($price);
                            $discountedPrice=@$discountList[$memberid]?stripslashes(@$discountList[$memberid]):stripslashes(@$price);
							$discountedPriceValue=@$discountList[$memberid];
                            $totalprice = @$discountedPrice+@$totalprice;
                        ?>
    
                      <tr>
                        <td>
					  <input checked="checked" type="checkbox" onClick="addtotalprice();" class="membercount" value="<?php echo floatval($discountedPrice); ?>" id="test<?php echo @$k; ?>" /> 
                      <label for="test<?php echo @$k; ?>"></label>
					  <?php echo stripslashes(@$memberName[0]['TITLE'])?stripslashes(@$memberName[0]['TITLE']):'NA'; ?>                          
						</td>
                        <td><cite class="WebRupee">Rs.</cite><span class="<?php echo $breaktext; ?>"> <?php echo stripslashes($price); ?>/-</span></td>
                         <?php if(@$discountList[1]!=''){ ?>
                        <td><cite class="WebRupee">Rs.</cite>  <?php echo $discountedPrice; ?>/-</td>
                        <?php } ?>
                        <td>&nbsp;</td>
                                   <input type="hidden" name="membersid[]" value="<?php echo @$memberName[0]['MEMBERTYPEID']; ?>" />
                                  <input type="hidden" name="membersprice[]" value="<?php echo stripslashes($discountedPrice); ?>" />
                      </tr>
                    <?php 
						}
						}
						$k++;	}
						} else {
						echo "<tr><td colspan='4'>NA</td></tr>";
						}
					
					?>                                             
                    </table>
                  </div>
                
                <?php } else{ ?>
                  <div class="tab-mid-container">
                    <table class="responsive" width="100%">
                      <tr>
                        <th>Select Member</th>
                        <th>MRP</th>
                        <th>Discounted Rates</th>
                        <th>&nbsp;</th>
                      </tr>
                    <?php 
                   
                    $memberList = @$planDetailsById[0]['EDITEDPRICE']?json_decode($planDetailsById[0]['EDITEDPRICE']):''; 
                    $discountList = @$planDetailsById[0]['DISCOUNTPRICE']?json_decode(@$planDetailsById[0]['DISCOUNTPRICE'],true):''; 
                    $totalprice = 0;
                    //printr($discountList);
                    if(isset($memberList) && !empty($memberList)){
                    $k=1;
                        foreach($memberList as $memberid=>$price){
                            $memberName = fetchListCondsWithColumn('TITLE,MEMBERTYPEID',"CRMCORPMEMBERTYPE","where STATUS='ACTIVE' AND CRMCORPMEMBERTYPE.MEMBERTYPEID=".@$memberid." ");
                        if(isset($price) && !empty($price)){
                            if(strtolower(stripslashes(@$memberName[0]['TITLE'])) == 'self'){
                            $_SESSION['membersarray'] = array_merge(array('self'),$_SESSION['membersarray']);
                            } 
                            
                            if(in_array(strtolower(stripslashes(@$memberName[0]['TITLE'])),$_SESSION['membersarray'])){
                        //printr($_SESSION['membersarray']);
                            $price = intval($price);
                            $discountedPrice=$discountList[$memberid]?stripslashes($discountList[$memberid]):stripslashes($price);
                            $totalprice = $discountedPrice+$totalprice;
                        ?>
                      <tr>
                        <td>
            <input checked="checked" type="checkbox" onClick="addtotalprice();" class="membercount" value="<?php echo intval($discountedPrice); ?>" id="test<?php echo @$k; ?>" >       			<label for="test<?php echo @$k; ?>"></label>
                                <?php echo stripslashes(@$memberName[0]['TITLE'])?stripslashes(@$memberName[0]['TITLE']):'NA'; ?>  </td>
                        <td><cite class="WebRupee">Rs.</cite><span class="break-text"> <?php echo stripslashes($price); ?>/-</span></td>
                        <td><cite class="WebRupee">Rs.</cite>  <?php echo $discountedPrice; ?>/-</td>
                        <td>&nbsp;</td>
                                   <input type="hidden" name="membersid[]" value="<?php echo @$memberName[0]['MEMBERTYPEID']; ?>" />
                                  <input type="hidden" name="membersprice[]" value="<?php echo stripslashes($discountedPrice); ?>" />
                      </tr>
                        <?php 
                        }
                        }
                        $k++;	}
                        } else {
                     	  echo "<tr><td colspan='4'>NA</td></tr>";
                        }
                        ?>     
                                             
                    </table>
                  </div>
                  <?php } ?>
                  <div class="col-sm-9 "> 
                   <span class="total-text"> Total Price :</span> <cite class="WebRupee1">Rs.</cite> <span class="total-price totalSum"><?php echo @$totalprice?@$totalprice:0; ?>/-
                   </span>
                   <input type="hidden" name="totalSumInputval" id="totalSumInputval" class="totalSumInputval" value="<?php echo intval($totalprice); ?>">
                    <div class="clearfix">&nbsp;</div>
                  <div class="tab-mid-container-sm">
                  <span>*</span> The price is inclusive of cost of the Health check up, Servicing Costs, Service Tax and Distribution Charges if any.
                  </div>
                   </div>
                  <div class="col-sm-3">
                    <div class="tab-mid-container-buy"><a href="javascript:void(0);" onclick="return validateHealthCheckupForm();">Buy Now ></a></div>
                    <input type="hidden" class="submitbtnplan" value="Buy Now" id="button2" name="AddDetails">
                  </div>
	            </form>
                      <div class="clearfix">&nbsp;</div>
                </div>
                <div class="colfull topMrgn"> 
                <div class="middlebox-left">
          <ul class="nav nav-tabs responsive" id="myTab">
            <li class="test-class"><a class="deco-none misc-class" href="#testDetails"> Test Details</a></li>
            <li class="test-class"><a href="#planDetails">Plan Details</a></li>
            <li><a class="deco-none" href="#centers">Centers</a></li>
            <li><a class="deco-none" href="#conditions">Conditions </a></li>
            <li><a class="deco-none" href="#howAvail">How to Avail</a></li>
          </ul>
          <div class="tab-content responsive">
            <div class="tab-pane active" id="testDetails">
            <div class="tab-paneIn">  
                <div class="tab-mid-container">
                <table class="responsive" width="100%">
                  <tr>
                    <th>Category&nbsp;(No&nbsp;of&nbsp;Tests)</th>
                    <th>Plan Name</th>
                  </tr>

				  <?php
                  $category_list=fetchListCond("CRMCORPTESTCATEGORY"," WHERE STATUS='ACTIVE' order by TESTCATEGORYID desc");
                  $s=0;
                     while($s<count($category_list)) {
                        $si2 = 0;
                        if($s%2==0){
                            $class='whitebg';
                        } else {
                            $class='';
                        }
                    $testname=array();
                
                        $array_test=fetchListCond('CRMCORPTESTNAME'," WHERE STATUS='ACTIVE' AND TESTCATEGORYID='".$category_list[$s]['TESTCATEGORYID']."' order by TESTID ASC"); 
                            
                    if(count($array_test) > 0){	
                     $c=0;
                        while($c<count($array_test)) {
                        $array_testcategory=fetchListCondsWithColumn('TESTID',"CRMCORPCATMAP"," WHERE HEALTHCHECKUPPLANID=".@$masterPlanId."");
                            $re=0;
                            while($re<count($array_testcategory)){
                                if($array_test[$c]['TESTID']==$array_testcategory[$re]['TESTID']){	
                                $testname[] = @$array_test[$c]['TESTNAME'];
                                } else {
                                $testname[] = '';
                                }
                            $re++; 
                            }
                        $c++;
                        }	
                        $filterRes = array_filter($testname);
                        $testnameres = implode(',',$filterRes);
                        if(isset($testnameres) && !empty($testnameres) && count($filterRes) > 0){
                    ?>
                    <tr>
                      <td><?php echo stripslashes($category_list[$s]['TESTCATEGORYNAME']); ?>: (<?php echo count($filterRes); ?>)</td>
                      <td><?php	 echo html_entity_decode(@$testnameres); ?></td>
                    </tr>
                        <?php }
                    	}				 
                      $s++; 
					  }
                  ?> 
                  </table>
              </div>
            </div>
            </div>
            <div class="tab-pane" id="planDetails">
              <div class="tab-paneIn">    <div class="tab-mid-container"> <table class="responsive" width="100%">
                  <tr>
                    <th>Name</th>
                    <th>Plan Name</th>
                  </tr>
                    <tr>
                      <td height="30">Age</td>
                      <td height="30" id="ageRange">
                      <?php 
					  	$ageRes=explode(',', @$planDetailsById[0]['AGEGROUPID']);
						if(count($ageRes)>0){
							$resdata=getRanges($ageRes);
							echo implode(',',$resdata);
						
						} else {
							echo "NA";
						}
						
                      ?>
                      </td>
                    </tr>
                    <tr>
                      <td height="30" class="whitebg">Grade/SI</td>
                      <td height="30" class="whitebg">
                    <?php	
                        $si_list = explode(',' , @$planDetailsById[0]['SI']);
                        sort($si_list);
                         $sidetail = array();
                        for($m=0;$m<count($si_list);$m++) {
                        $siName=fetchListById('CRMSI','SIID',$si_list[$m]); 
                                $sidetail[] = @$siName[0]['SI'];
                        }
                        $sidetail = array_filter($sidetail);
                        echo $sidetail?implode(',',$sidetail):'NA';
                     ?>
        
                      </td>
                    </tr>
                    <tr>
                      <td height="30">Gender</td>
                      <td height="30"><?php echo @$planDetailsById[0]['GENDER']?$planDetailsById[0]['GENDER']:'NA'; ?></td>
                    </tr>
                    <tr>
                      <td height="30" class="whitebg">Total Price</td>
                      <td height="30" class="whitebg totalSum"><?php echo intval($totalprice); ?>/-</td>
                    </tr>
                    <tr>
                      <td height="30">Plan Document</td>
                      <td height="30" valign="middle"><?php 
					  if($_SESSION['LOGINTYPE']=='CORPORATE'){
						  //print_r($planDetailsById);die;
						  $selectedPlanDetails =  fetchPlanDetailsById("where CRMCORPHEALTHCHECKUP.STATUS='ACTIVE' AND CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=".@$masterPlanId." and CRMCOMPANYHEALTHCHECKUPPLAN.PLANID=".@$planId." ");
						  if(isset($planDetailsById[0]['UPLOADPDF']) && !empty($planDetailsById[0]['UPLOADPDF'])){  $pdflink=stripslashes(@$planDetailsById[0]['UPLOADPDF']); ?>
                          <a href="download.php?fileName=<?php echo stripslashes(@$planDetailsById[0]['UPLOADPDF']); ?>">
                              <img src="img/pdficon.png" border="0" alt="" class="fl">
                          </a> <?php }
                          else if(isset($selectedPlanDetails[0]['UPLOADPDF2']) && !empty($selectedPlanDetails[0]['UPLOADPDF2']))
                              { $pdflink=stripslashes(@$selectedPlanDetails[0]['UPLOADPDF2']); ?>  <a href="download.php?fileName=<?php echo stripslashes(@$selectedPlanDetails[0]['UPLOADPDF2']); ?>"><img src="img/pdficon.png" border="0" alt="" class="fl"></a><?php  }
                              else { echo "NA"; }
					  } else {
	                      $selectedPlanDetails =  fetchRetailhealthPlanListById("where CRMCORPHEALTHCHECKUP.STATUS='ACTIVE' AND CRMCORPHEALTHCHECKUP.HEALTHCHECKUPPLANID=".@$masterPlanId." ");
						  if(isset($planDetailsById[0]['UPLOADPDF']) && !empty($planDetailsById[0]['UPLOADPDF'])){  ?>
                          <a href="download.php?fileName=<?php echo stripslashes(@$planDetailsById[0]['UPLOADPDF']); ?>">


                              <img src="img/pdficon.png" border="0" alt="" class="fl">
                          </a> <?php }
                          else if(isset($selectedPlanDetails[0]['UPLOADPDF']) && !empty($selectedPlanDetails[0]['UPLOADPDF']))
                              { ?>  <a href="download.php?fileName=<?php echo stripslashes(@$selectedPlanDetails[0]['UPLOADPDF']); ?>"><img src="img/pdficon.png" border="0" alt="" class="fl"></a><?php  }
                              else { echo "NA"; }
					  }

                     ?></td>
                    </tr>
                </table>  </div></div>
            </div>
            <div class="tab-pane" id="centers">
          <div class="tab-paneIn">
           <div class="tab-mid-container">
              <table class="responsive" width="100%" id="center" >
                 <td height="30">Available Health centers</td> 
                    <?php 
					  if($_SESSION['LOGINTYPE']=='CORPORATE'){
					$si_companyCenter_list=fetchListByColumnNameCond("CRMCOMPANYCENTERMAPPING","CENTERID,CHAINID,PLANID,COMPANYID","COMPANYID",@$_SESSION["COMPANYID"],'AND PLANID= '.@$planId.''); 

					} else {
					$si_companyCenter_list=fetchListByColumnNameCond("CRMRETAILCENTERMAPPING","CENTERID,CHAINID,PLANID,PRODUCTID","PRODUCTID",@$_SESSION["productId"],'AND PLANID= '.@$planId.'');       
					
					}
                    $si=0;
                    while($si<count($si_companyCenter_list)){
                     $array_center= fetchListCondsWithColumn('CENTERID,CENTERNAME',"LWCENTERMASTER","where LWCENTERMASTER.CENTERID=".@$si_companyCenter_list[$si]['CENTERID']." AND LWCENTERMASTER.STATUS='ACTIVE'");
                    if($si%2==0){
                    $class='whitebg';
                    } else {
                    $class='';
                    }
                     ?>
                        <tr>
                          <td height="30" class="<?php echo $class; ?>"><?php echo stripslashes($array_center[0]['CENTERNAME']); ?></td>
                        </tr>
                    <?php
                    $si++;
                    }
                    ?>
                </table>
                </div>
                </div>
            </div>
            <div class="tab-pane" id="conditions"> 
            <div class="tab-paneIn"> <div class="tab-mid-container"> <?php
				$res = @$selectedPlanDetails[0]['TERMSANDCONDITION']?str_replace('<p>','',$selectedPlanDetails[0]['TERMSANDCONDITION']->load()):'';
				$res = str_replace('</p>','',$res);
				echo $res?stripslashes(@$res):'NA'; 
			?></div>
                </div>
            </div>
            <div class="tab-pane" id="howAvail">
                 <div class="tab-paneIn"> 
                 <div class="tab-mid-container">
                         <?php
				$res = @$selectedPlanDetails[0]['EXCLUSION']?str_replace('<p>','',$selectedPlanDetails[0]['EXCLUSION']->load()):'';
				$res = str_replace('</p>','',$res);
				echo $res?stripslashes(@$res):'NA'; 
			?>
            </div>
            </div>
                </div>
          </div>
        </div>
                    </div> 
              <div class="clearfix">&nbsp;</div>
            </div>
            <?php } ?>
            <div class="tab-pane" id="existingAppointments"> Please Wait... </div>
            <div class="tab-pane <?php if(!empty($tab) && $tab=="myplan"){ echo "active"; } ?>" id="myPlans"> 
                <div class="tab-paneIn" id="myPlanList" style="display:block;">
                
				<?php
				
				if($_SESSION['LOGINTYPE']=='CORPORATE'){
					$healthCheckPlansList=getHealthCheckupPlansFree(@$wellnessList[0]['POLICYID']);
					
					$purchasedPlanList=getHealthCheckupPurchasePlan(@$policy,@$_SESSION['clientNumber'],@$_SESSION['COMPANYID']);
					$limitedRequest=checkLimitedPlanForEmployee(@$wellnessList[0]['POLICYID'],@$_SESSION['empNo'],@$_SESSION['clientNumber']);//function to fetch records
					$limitedPlan='';
					//echo count($limitedRequest); die;
					if(count($limitedRequest)>0){
					
						$limitedPlan = fetchListCondsWithColumn("PLANID,PLANNAME","CRMCOMPANYHEALTHCHECKUPPLAN"," WHERE POLICYID=".@$wellnessList[0]['POLICYID']." AND COMPANYID=".@$_SESSION['COMPANYID']." AND PLANTYPE='LIMITED'");
					}
					?>
                        <div class="col-sm-12"> 
                       
                        <table class="responsive" width="100%">
                          <tr>
                            <th>#</th>
                            <th>Plan Name</th>
                            <th>Member(s)</th>
                            <th>Price</th>
                            <th>Action</th>
                          </tr>
                          <tbody id="myTable">
							<?php  $k=0;
							 if(count($limitedRequest)>0 && count($limitedPlan)>0){ $k++; ?>
                          <tr>
                            <td><?php echo $k; ?></td>
                            <td><?php echo stripslashes($limitedPlan[0]['PLANNAME']); ?></td>
                            <td>NA</td>
                            <td>Limited Plan (Free)</td>
                            <td><a href="view_health_plan_details.php?id=<?php echo base64_encode(@$limitedPlan[0]['PLANID']);?>" data-toggle="modal" data-target="#exampleModal2_<?php echo @$limitedPlan[0]['PLANID']; ?>"  class="view_nav example8"><span class="eyeIcon" title="View"></span></a> <a <?php if($_SESSION['policyexpiredisables']['vas.my_plans.schedule_appointment_icon']!='NO'){ ?> href="limited_request_scheduler.php?id=<?php echo base64_encode(@$limitedPlan[0]['PLANID']);?>&tab=<?php echo base64_encode('existingapp'); ?>" <?php } else { ?>  onclick="alert('You can\'t schedule appointment for this plan as your policy is expired !')" <?php } ?>><span class="timeIcon" title="Appointment Request Scheduler"></span></a>
							  
										<div class="modal fade" id="exampleModal2_<?php echo @$limitedPlan[0]['PLANID']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
										  <div class="modal-dialog" role="document">
											<div class="modal-content">
											  <div class="modal-header">
												<div class="model-header" id="exampleModalLabel">VIEW HEALTH CHECK-UP PLAN</div>
											  </div>
											 &nbsp;&nbsp;&nbsp;Please Wait....
											  
											</div>
										  </div>
										</div>           
							  


							  </td>
                          </tr>
                             
                             <?php }
                            $a=0;
                           
                            $plan=0;
							
                            if(count(@$healthCheckPlansList)>0){
                            
                            while($a<count(@$healthCheckPlansList)){ 
                         
                                if(@$healthCheckPlansList[$a]['PLANLIMITS']=="ONLYEMPLOYEES"){
                                    $planDisplay=0;
                                    if(strtolower($_SESSION['GENDER'])==strtolower(@$healthCheckPlansList[$a]['GENDER']) || strtolower(@$healthCheckPlans[$a]['GENDER'])=="both"){
                                        $planDisplay++;
                                    }
                                    $ageName1="";
									
                                    $pieces = explode(',' , $healthCheckPlansList[$a]['AGEGROUPID']);
                                    $policyHolderAge=str_replace(",","",number_format(@$_SESSION['policyHolderAge']));
                                    for($j=0;$j<count($pieces);$j++) {
												// age group according to new
													$ageName1.=$pieces[$j].", ";
													if($policyHolderAge == $pieces[$j]){
																$planDisplay++;
																break;
													}
									}
                                    
                    
                    
                                    $siName1="";
                                    $si_list = explode(',' , $healthCheckPlansList[$a]['SI']);
                                    $policyHolderSI=str_replace(",","",number_format(@$_SESSION['policyHolderSI']));
                                    //$policyHolderSI=100000;
                                    for($j=0;$j<count($si_list);$j++) {
                                            $siName=fetchListById('CRMSI','SIID',$si_list[$j]); 
                                            $siName1.=$siName[0]['SI'].", ";
                                            if($siName[0]['SI']==$policyHolderSI){
                                                $planDisplay++;
                                            }
                                    }
                                    $planLimits="Employee Only";
                                    if($planDisplay==3){
                                    $plan=1;
                                    
                           
                                $memberList=array();
                        
                        if(@$healthCheckPlans[$a]['PAYMENTOPTIONS']=="BUY"){
                            $memberList=get_balance_member_list_for_appointment(@$purchaseId,@$healthCheckPlansList[$a]['PLANID'],@$_SESSION['COMPANYID']);//function to fetch records
                        }else{
                            $responseArray['purchaseId']=@$purchaseId;
                            $responseArray['planId']=@$healthCheckPlansList[$a]['PLANID'];
                            $responseArray['companyId']=@$_SESSION['COMPANYID'];
                            $responseArray['policyId']=@$wellnessList[0]['POLICYID'];
                            $memberList=get_member_list_for_free_appointment(@$responseArray);
                        }
                        if(count($memberList) > 0 ) { 
                        ?>
                        <h3>
                        <?php
                        echo $healthCheckPlansList[$a]['PLANNAME']?stripslashes(@$healthCheckPlansList[$a]['PLANNAME']):stripslashes(@$healthCheckPlansList[$a]['PLANNAME2']); ?> <?php if($healthCheckPlansList[$a]['PAYMENTOPTIONS']=='FREE'){ echo "(Free)";  }
                        
                         ?>
                         
                            
                        <a href="view_health_plan_details.php?id=<?php echo base64_encode(@$healthCheckPlansList[$a]['PLANID']);?>" data-toggle="modal" data-target="#exampleModal1_<?php echo @$healthCheckPlansList[$a]['PLANID']; ?>" class="view_nav example8">View</a>
                        <?php if(@$healthCheckPlansList[$a]['PAYMENTOPTIONS']=="FREE" || @$planStatus[0]['PLANNAME']!=''){?>&nbsp;|&nbsp;<a <?php if($_SESSION['policyexpiredisables']['vas.my_plans.schedule_appointment_icon']!='NO'){ ?> href="appointment_request_scheduler.php?id=<?php echo base64_encode(@$healthCheckPlansList[$a]['PLANID']);?>&tab=<?php echo base64_encode('existingapp'); ?>" <?php } ?>>Schedule Appointment
                        Now</a><?php } ?> </h3>
                          <?php  
			   $purchaseplanListByPlanId = fetchpurchasePlanDetailsByplanId(@$healthCheckPlansList[$a]['PURCHASEPLANID'],@$healthCheckPlansList[$a]['PLANID'],@$wellnessList[0]['POLICYID']);
			 
			  if(count($purchaseplanListByPlanId) > 0){ 
                            $q=0;
                            $f=1;
                           while($q<count($purchaseplanListByPlanId)){
                       	   $k++; 
                           ?>
                            <tr>
                              <td><?php echo $k; ?></td>
                              <td><?php echo @$purchaseplanListByPlanId[$q]['NAME']?stripslashes(@$purchaseplanListByPlanId[$q]['NAME']):'NA' ?></td>
                              <td><?php echo @$purchaseplanListByPlanId[$q]['RELATION']?stripslashes($relationArray[@$purchaseplanListByPlanId[$q]['RELATION']]):'NA' ?></td>
                              <td valign="middle" class="centericon">
                              <?php if(strtolower(@$purchaseplanListByPlanId[$q]['GENDER'])=='male'){ ?>
                              <span class="maleIcon"></span>
                              <?php } if(strtolower(@$purchaseplanListByPlanId[$q]['GENDER'])=='fem
							  ale'){ ?>
                              <span class="femaleIcon"></span>
                              <?php } ?>
                              </td>
                              <td><a href="view_health_plan_details.php?id=<?php echo base64_encode(@$healthCheckPlansList[$a]['PLANID']);?>" data-toggle="modal" data-target="#exampleModal1_<?php echo @$healthCheckPlansList[$a]['PLANID']; ?>"  class="view_nav example8"><span class="eyeIcon" title="View"></span></a> <a <?php if($_SESSION['policyexpiredisables']['vas.my_plans.schedule_appointment_icon']!='NO'){ ?> href="appointment_request_scheduler.php?id=<?php echo base64_encode(@$healthCheckPlansList[$a]['PLANID']);?>&tab=<?php echo base64_encode('existingapp'); ?>" <?php } else { ?>  onclick="alert('You can\'t schedule appointment for this plan as your policy is expired !')" <?php }  ?>><span class="timeIcon" title="Appointment Request Scheduler"></span></a>
							  
										<div class="modal fade" id="exampleModal1_<?php echo @$healthCheckPlansList[$a]['PLANID']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
										  <div class="modal-dialog" role="document">
											<div class="modal-content">
											  <div class="modal-header">
												<div class="model-header" id="exampleModalLabel">VIEW HEALTH CHECK-UP PLAN</div>
											  </div>
											 &nbsp;&nbsp;&nbsp;Please Wait....
											  
											</div>
										  </div>
										</div>           
							  


							  </td>
                            </tr>
                          <?php 
                          $q++;} 
                          }
                          ?>
                        <?php if(count($memberList) == 0 ) { ?>
                        <div class="fl" style="display:none;">You have already booked appointment for this plan</div>
                        <?php
                        } ?>
                        
                        <div class="midContainer_whiteboxshadow"></div>
                        
                        <?php 
							}  
						  }
						} else{ $planCheck=0;
                            
                        if((isset($responseArray[0]['BGEN-MEMNAME']['#text'])) && ($responseArray[0]['BGEN-MEMNAME']['#text'] != "")){
                                $g=0;
								 $gd=0;
                                // respone loop start
                            foreach($responseArray as $key => $response){
                            
                            if(!is_array($response)){
                             continue;
                            }
                                    if(trim($response['BGEN-GENDER']['#text'])=="F"){$gender="Female";	}
                                    if(trim($response['BGEN-GENDER']['#text'])=="M"){$gender="Male";	}
                                    
                                    
                                    if($key == 0 ){//if primary member
                                        $planDisplay=0;
                                        
                                        if(strtolower($gender)==strtolower(@$healthCheckPlansList[$a]['GENDER']) || strtolower(@$healthCheckPlansList[$a]['GENDER'])=="both" || strtolower(@$healthCheckPlansList[$a]['GENDER'])=="female"){
                                             $planDisplay++;
                                        }
                                        
                                        // Start to check If PLANLIMITS Both than Primary member covered
                                        
                                        if(@$healthCheckPlansList[$a]['PLANLIMITS']=="BOTH"){				
										
                                                    $ageName1="";
                                                    $pieces = explode(',' , $healthCheckPlansList[$a]['AGEGROUPID']);
                                                    $policyHolderAge=str_replace(",","",number_format(trim($response['BGEN-AGE']['#text'])));
                                                    for($j=0;$j<count($pieces);$j++) {
													// age group according to new
														$ageName1.=$pieces[$j].", ";
														if($policyHolderAge == $pieces[$j]){
																	$planDisplay++;
																	break;
														}
												   }
                                                    
                                                    $siName1="";
                                                    $si_list = explode(',' , $healthCheckPlansList[$a]['SI']);
                                                    $policyHolderSI=str_replace(",","",number_format(@$_SESSION['policyHolderSI']));
                                                    for($j=0;$j<count($si_list);$j++) {
                                                            $siName=fetchListById('CRMSI','SIID',@$si_list[$j]); 
                                                            $siName1.=$siName[0]['SI'].", ";
                                                            if($siName[0]['SI']==$policyHolderSI){
                                                                $planDisplay++;
                                                            }
                                                    }
                                                    
                                            }
                                             echo "<br/>policyHolderSI=".$policyHolderSI."/".$siName1."<br/>3=".$planDisplay;die;
                                            if($planDisplay==3){$planCheck=1;$planCheckformale=1;}else{$planCheck=0;}
                                            
                                            // End to check If PLANLIMITS Both than Primary member covered
                                    }else{//else not primary member
                                    
                                            $planDisplay=0;
                                            if(strtolower($gender)==strtolower(@$healthCheckPlansList[$a]['GENDER']) || strtolower(@$healthCheckPlansList[$a]['GENDER'])=="both"){
                                                $planDisplay++;
                                            }
                                            
                                            $ageName1="";
                                            $pieces = explode(',' , $healthCheckPlansList[$a]['AGEGROUPID']);
                                            $policyHolderAge=str_replace(",","",number_format(trim($response['BGEN-AGE']['#text'])));
                                                for($j=0;$j<count($pieces);$j++) {
												// age group according to new
													$ageName1.=$pieces[$j].", ";
													if($policyHolderAge == $pieces[$j]){
																$planDisplay++;
																break;
													}
											   }
                                                $siName1="";
                                                $si_list = explode(',' , $healthCheckPlansList[$a]['SI']);
                                                $policyHolderSI=str_replace(",","",number_format(@$_SESSION['policyHolderSI']));
                                                for($j=0;$j<count($si_list);$j++) {
                                                $siName=fetchListById('CRMSI','SIID',$si_list[$j]); 
                                                $siName1.=$siName[0]['SI'].", ";
                                                    if($siName[0]['SI']==$policyHolderSI){
                                                    $planDisplay++;
                                                    }
                                                }
                                                
                                                if($planDisplay==3){$planCheck=1;}else{$planCheck=0;}
                                    }					
                                }	// respone loop end	
                                    
                        if($planCheck==1 || @$planCheckformale==1){
                            if(@$healthCheckPlansList[$a]['PLANLIMITS']=="FAMILYALLOWED"){$planLimits="Family Only"; }else if(@$healthCheckPlansList[$a]['PLANLIMITS']=="BOTH"){$planLimits="Employee and Family"; }else{$planLimits="";}
                            
                        ?>
                        
						<?php 
                    if(@$purchasedPlanList[$a]['PAYMENTOPTIONS']=="BUY"){
                        $memberList=get_balance_member_list_for_appointment($purchasedPlanList[$a]['PURCHASEPLANID'],@$purchasedPlanList[$a]['PLANID'],@$_SESSION['COMPANYID']);//function to fetch records
						
                    }else{
					//echo @$purchasedPlanList[$a]['PLANID'];
                        $responseArray['purchaseId']=@$purchasedPlanList[$a]['PURCHASEPLANID'];
                        $responseArray['planId']=@$healthCheckPlansList[$a]['PLANID'];
                        $responseArray['companyId']=@$_SESSION['COMPANYID'];
                        $responseArray['policyId']=@$wellnessList[0]['POLICYID'];
						
                        $memberList=get_member_list_for_free_appointment(@$responseArray);
                    }	
					//echo count($memberList);
                    if(count($memberList) > 0 ) { 
					$plan=1;
						$gd++;
						$k++;
                    ?>
                                  <tr>
                                    <td><?php echo $k; ?></td>
                                    <td><?php
                         echo @$healthCheckPlansList[$a]['PLANNAME']?stripslashes(@$healthCheckPlansList[$a]['PLANNAME']):stripslashes(@$healthCheckPlansList[$a]['PLANNAME2']); ?> <?php if($healthCheckPlansList[$a]['PAYMENTOPTIONS']=='FREE'){ echo "(Free)";  }
                            $memberList=array();?></td>
                                    <td>
                                        <?php 
                                        if(isset($healthCheckPlansList[$a]['MEMBERTYPEID'])){
                                            $membersname= fetchcolumnListCond('TITLE','CRMCORPMEMBERTYPE',"WHERE STATUS='ACTIVE' AND MEMBERTYPEID IN(".$healthCheckPlansList[$a]['MEMBERTYPEID'].") ");
                                            $f=0;
                                            $titles=array();
                                            while($f<count($membersname)){
                                                $titles[]=$membersname[$f]['TITLE'];
                                            $f++; }
                                            if(count($titles)>0){
                                                $title=implode(',',$titles);
                                            } else {
                                                $title='';
                                            }
                                        } 
                                        echo $title?$title:'NA';
                                        ?>
                                    </td>
                                    <td><strong class="greenGeneralTxt">Free</strong></td>
                                    <td><a href="view_health_plan_details.php?id=<?php echo base64_encode(@$healthCheckPlansList[$a]['PLANID']);?>" data-toggle="modal" data-target="#exampleModal1_<?php echo @$healthCheckPlansList[$a]['PLANID']; ?>"  class="view_nav example8"><span class="eyeIcon" title="View"></span></a> <?php if(@$healthCheckPlansList[$a]['PAYMENTOPTIONS']=="FREE"){?> <a <?php if($_SESSION['policyexpiredisables']['vas.my_plans.schedule_appointment_icon']!='NO'){ ?> href="appointment_request_scheduler.php?id=<?php echo base64_encode(@$healthCheckPlansList[$a]['PLANID']);?>&tab=<?php echo base64_encode('existingapp'); ?>" <?php } else { ?>  onclick="alert('You can\'t schedule appointment for this plan as your policy is expired !')" <?php } ?>><span class="timeIcon" title="Appointment Request Scheduler"></span></a><?php } ?>
									
										<div class="modal fade" id="exampleModal1_<?php echo @$healthCheckPlansList[$a]['PLANID']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
										  <div class="modal-dialog" role="document">
											<div class="modal-content">
											  <div class="modal-header">
												<div class="model-header" id="exampleModalLabel">VIEW HEALTH CHECK-UP PLAN</div>
											  </div>
											 &nbsp;&nbsp;&nbsp;Please Wait....
											  
											</div>
										  </div>
										</div>           
									
									</td>
                                  </tr>
                        
                          <?php 
		   $purchaseplanListByPlanId = fetchpurchasePlanDetailsByplanId(@$purchasedPlanList[$a]['PURCHASEPLANID'],@$purchasedPlanList[$a]['PLANID'],@$wellnessList[0]['POLICYID']);
		  
		  if(count($purchaseplanListByPlanId) > 0){
                          ?>
                            <?php
                            $q=0;
                            $f=1;
                           while($q<count($purchaseplanListByPlanId)){
                            $gd++;
							$k++;
                           ?>
                            <tr>
                              <td><?php echo $k; ?></td>
                              <td><?php
                         echo @$healthCheckPlansList[$a]['PLANNAME']?stripslashes(@$healthCheckPlansList[$a]['PLANNAME']):stripslashes(@$healthCheckPlansList[$a]['PLANNAME2']); ?> <?php if($healthCheckPlansList[$a]['PAYMENTOPTIONS']=='FREE'){ echo "(Free)";  }
                            $memberList=array();?></td>
                              <td><?php echo @$purchaseplanListByPlanId[$q]['RELATION']?stripslashes($relationArray[@$purchaseplanListByPlanId[$q]['RELATION']]):'NA' ?></td>
                             <td><strong class="greenGeneralTxt">Free</strong></td>
                              <td><a href="view_health_plan_details.php?id=<?php echo base64_encode(@$healthCheckPlansList[$a]['PLANID']);?>" data-toggle="modal" data-target="#exampleModal1_<?php echo @$healthCheckPlansList[$a]['PLANID']; ?>" class="view_nav example8"><span class="eyeIcon" title="View"></span></a> <a <?php if($_SESSION['policyexpiredisables']['vas.my_plans.schedule_appointment_icon']!='NO'){ ?> href="appointment_request_scheduler.php?id=<?php echo base64_encode(@$healthCheckPlansList[$a]['PLANID']);?>&tab=<?php echo base64_encode('existingapp'); ?>" <?php } else { ?>  onclick="alert('You can\'t schedule appointment for this plan as your policy is expired !')" <?php } ?>><span class="timeIcon" title="Appointment Request Scheduler"></span></a>
										<div class="modal fade" id="exampleModal1_<?php echo @$healthCheckPlansList[$a]['PLANID']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
										  <div class="modal-dialog" role="document">
											<div class="modal-content">
											  <div class="modal-header">
												<div class="model-header" id="exampleModalLabel">VIEW HEALTH CHECK-UP PLAN</div>
											  </div>
											 &nbsp;&nbsp;&nbsp;Please Wait....
											</div>
										  </div>
										</div>           
							  </td>
                            </tr>
                          <?php
                          $q++;} 
                          }
                          ?>
                          
                                <?php if(count($memberList) == 0 ) { ?>
                        <div class="fl" style="display:none;">You have already booked appointment for this plan</div>
                        <?php  } ?>
                        
                         
                        <div class="midContainer_whiteboxshadow"></div>
                            <?php }
                           }
                            }
                            }
                            $a++;			
                        }
                         }
                         $a=0;
                        	if(count(@$purchasedPlanList)>0){
                         while($a<count(@$purchasedPlanList)){ 
                             $ageName1="";
                                $ageName1="";
                                $pieces = explode(',' , $purchasedPlanList[$a]['AGEGROUPID']);
                                $policyHolderAge=str_replace(",","",number_format(@$_SESSION['policyHolderAge']));
                                for($j=0;$j<count($pieces);$j++) {
                                    $age=explode('-' , $pieces[$j]);
                                    //print_r($age);
									$minAge=@$age[0]?@$age[0]:0;
									$maxAge=@$age[1]?@$age[1]:0;
                                    $ageName1.=$pieces[$j].", ";
                        
                                }
                                $siName1="";
                                $si_list = explode(',' , $purchasedPlanList[$a]['SI']);
                                for($j=0;$j<count($si_list);$j++) {
                                        $siName=fetchListById('CRMSI','SIID',$si_list[$j]); 
                                        $siName1.=$siName[0]['SI'].", ";
                                        
                                }
                                
                                if(@$purchasedPlanList[$a]['PLANLIMITS']=="ONLYEMPLOYEES"){
                                        $planLimits="Employee Only";
                                } else if(@$purchasedPlanList[$a]['PLANLIMITS']=="BOTH"){
                                        $planLimits="Employee and Family";
                                }else if(@$purchasedPlanList[$a]['PLANLIMITS']=="FAMILYALLOWED"){
                                        $planLimits="Family Only";
                                }else{
                                        $planLimits="";
                                }
                               
                        
                        if(@$purchasedPlanList[$a]['PAYMENTOPTIONS']=="BUY"){
                        $memberList=get_balance_member_list_for_appointment($purchasedPlanList[$a]['PURCHASEPLANID'],@$purchasedPlanList[$a]['PLANID'],@$_SESSION['COMPANYID']);//function to fetch records
						
                        }else{
                        $responseArray['purchaseId']=$purchasedPlanList[$a]['PURCHASEPLANID'];
                        $responseArray['planId']=@$purchasedPlanList[$a]['PLANID'];
                        $responseArray['productId']=@$_SESSION['productId'];
                        $responseArray['policyNumber']=@$_SESSION['policyNumber'];
                        $memberList=get_member_list_for_free_appointment(@$responseArray);
                        }	
                        if(count($memberList) > 0 ) {
							 $plan=1; 
                        
                          //echo @$purchasedPlanList[$a]['PURCHASEPLANID'];
						  
						 
                           $purchaseplanListByPlanId = fetchpurchasePlanDetailsByplanId(@$purchasedPlanList[$a]['PURCHASEPLANID'],@$purchasedPlanList[$a]['PLANID'],@$wellnessList[0]['POLICYID']);
                          
                          if(count($purchaseplanListByPlanId) > 0){
                          ?>
                            <?php
                            $q=0;
                            $f=1;
                           while($q<count($purchaseplanListByPlanId)){
                            $k++;
                           ?>
                            <tr>
                              <td><?php echo $k; ?></td>
                              <td><?php echo @$purchasedPlanList[$a]['PLANNAME']?stripslashes(@$purchasedPlanList[$a]['PLANNAME']):stripslashes(@$purchasedPlanList[$a]['PLANNAME2']); ?> <?php if($purchasedPlanList[$a]['PAYMENTOPTIONS']=='FREE'){ echo "(Free)";  } ?></td>
                              <td><?php echo @$purchaseplanListByPlanId[$q]['RELATION']?stripslashes($relationArray[@$purchaseplanListByPlanId[$q]['RELATION']]):'NA' ?></td>
                              <td valign="middle" class="centericon">
                               <?php echo @$purchasedPlanList[$a]['TOTALAMOUNT']?@$purchasedPlanList[$a]['TOTALAMOUNT']:'NA'; ?>
                              </td>
                              <td><a href="view_health_plan_details.php?id=<?php echo base64_encode(@$purchasedPlanList[$a]['PLANID']);?>&pId=<?php echo base64_encode(@$purchasedPlanList[$a]['PURCHASEPLANID']);?>" data-toggle="modal" data-target="#exampleModal1_<?php echo $purchasedPlanList[$a]['PLANID']; ?>"  class="view_nav example8"><span class="eyeIcon" title="View"></span></a>  <a <?php if($_SESSION['policyexpiredisables']['vas.my_plans.schedule_appointment_icon']!='NO'){ ?> href="appointment_request_scheduler.php?id=<?php echo base64_encode(@$purchasedPlanList[$a]['PLANID']);?>&pId=<?php echo base64_encode(@$purchasedPlanList[$a]['PURCHASEPLANID']);?>&tab=<?php echo base64_encode('existingapp'); ?>" <?php }else { ?>  onclick="alert('You can\'t schedule appointment for this plan as your policy is expired !')" <?php }  ?>><span class="timeIcon" title="Appointment Request Scheduler"></span></a>
							  
							  
										<div class="modal fade" id="exampleModal1_<?php echo @$purchasedPlanList[$a]['PLANID']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
										  <div class="modal-dialog" role="document">
											<div class="modal-content">
											  <div class="modal-header">
												<div class="model-header" id="exampleModalLabel">VIEW HEALTH CHECK-UP PLAN</div>
											  </div>
											 Please Wait....
											  
											</div>
										  </div>
										</div>           
							  
							  </td>
                            </tr>
                           
                          <?php 
                          $q++; } 
                          }
                          ?>
                            
                                 <?php  if(count($memberList) == 0 ) { ?>
                        <div class="fl" style="display:none;">You have already booked appointment for this plan</div>
                        <?php }  ?>
                        
                         
                        <div class="midContainer_whiteboxshadow"></div>
                        <?php } $a++; } }?>
                        
                        <?php  if($plan==0 && count($limitedRequest)==0 && count($limitedPlan)==0){?>
                                  <tr>
                                    <td colspan="5">Sorry! you don't have any health plan added. Please buy first. Click <a style="color:#000000;font:bold 17px/30px Tahoma,Arial,Helvetica,sans-serif;" href="health_checkup.php">here</a> to buy a health plan</td>
                                  </tr>
                                                         
                        <?php } else if($k==0){ 
						?>
                                  <tr>
                                    <td colspan="5">Sorry! you don't have any health plan added. Please buy first. Click <a style="color:#000000;font:bold 17px/30px Tahoma,Arial,Helvetica,sans-serif;" href="health_checkup.php">here</a> to buy a health plan</td>
                                  </tr>
                        
                        <?php
						 } else {  }?>
                         </tbody>
                        </table>
                         <?php if($plan > 0 || $k > 0){ ?>
                         <ul class="pagination pagination-lg pager" id="myPager"></ul>
                         <?php } ?>
                        </div>
                    <?php
				} else {
				
					$healthCheckPlansList=getRetHealthCheckupPlansFree(@$_SESSION['productId']);
					$purchasedPlanList=getRetHealthCheckupPurchasePlan(@$_SESSION['policyNumber'],@$_SESSION['customerId'],@$_SESSION['productId']);
					$limitedRequestList=checkLimitedPlanForRetEmployee(@$_SESSION['productId'],@$_SESSION['empNo'],@$_SESSION['customerId']);//function to fetch records
					$limitedPlanList='';
					if(count($limitedRequestList)>0){
						$limitedPlanList = fetchListCondsWithColumn("PLANID","CRMRETAILHEALTHCHECKUPPLAN"," WHERE PRODUCTID=".@$_SESSION['productId']." AND PLANNAME='LIMITED'");
					}
					
					?>
                    <div class="col-sm-12">
                    <table class="responsive" width="100%">
                      <tr>
                        <th>#</th>
                        <th>Plan Name</th>
                        <th>Member(s)</th>
                        <th>Price</th>
                        <th>Action</th>
                      </tr>
                      <tbody id="myTable">
                    <?php 	
                    $a=0;
                    $k=0;
                    $plan=0; 
					$start=1;
					$bookApp=true;
                    if(count(@$healthCheckPlansList)>0){
						while($a<count(@$healthCheckPlansList)){ 
								if(@$healthCheckPlansList[$a]['PLANLIMITS']=="ONLYEMPLOYEES"){
								$planDisplay=0;
								if(strtolower($_SESSION['GENDER'])==strtolower(@$healthCheckPlansList[$a]['GENDER']) || strtolower(@$healthCheckPlansList[$a]['GENDER'])=="both"){
									$planDisplay++;
								}
								$ageName1="";
								$pieces = explode(',' , $healthCheckPlansList[$a]['AGEGROUPID']);
								$policyHolderAge=str_replace(",","",number_format(@$_SESSION['policyHolderAge']));
								for($j=0;$j<count($pieces);$j++) {
													// age group according to new
														$ageName1.=$pieces[$j].", ";
														if($policyHolderAge == $pieces[$j]){
																	$planDisplay++;
																	break;
														}
								}
				
								$siName1="";
								$si_list = explode(',' , $healthCheckPlansList[$a]['SI']);
								$policyHolderSI=str_replace(",","",number_format(@$_SESSION['policyHolderSI']));
								//$policyHolderSI=100000;
								for($j=0;$j<count($si_list);$j++) {
										$siName=fetchListById('CRMSI','SIID',$si_list[$j]); 
										$siName1.=$siName[0]['SI'].", ";
										if($siName[0]['SI']==$policyHolderSI){
											$planDisplay++;
										}
								}
								$planLimits="Employee Only";
								if($planDisplay==3){
							   
					   
							$memberList=array();
					
					if(@$healthCheckPlansList[$a]['PAYMENTOPTIONS']=="BUY"){
						$memberList=get_balance_retmember_list_for_appointment(@$purchaseId,@$healthCheckPlansList[$a]['PLANID'],@$_SESSION['productId']);//function to fetch records
					}else{
						$responseArray['purchaseId']=@$purchaseId;
						$responseArray['planId']=@$healthCheckPlansList[$a]['PLANID'];
						$responseArray['productId']=@$_SESSION['productId'];
						$responseArray['policyNumber']=$_SESSION['policyNumber'];
						$memberList=get_retmember_list_for_free_appointment(@$responseArray);
					}
					if(count($memberList) > 0 ) {
						$plan=1; 
					?>
					 <h3>
					 <?php
				 echo @$healthCheckPlansList[$a]['PLANNAME']?stripslashes(@$healthCheckPlansList[$a]['PLANNAME']):stripslashes(@$healthCheckPlansList[$a]['PLANNAME2']); ?> <?php if($healthCheckPlansList[$a]['PAYMENTOPTIONS']=='FREE'){ echo "(Free)";  }
					
						 ?>
						 
							
						<a href="view_health_plan_details.php?id=<?php echo base64_encode(@$healthCheckPlansList[$a]['PLANID']);?>" class="view_nav example8" title="View">View</a>
						<?php if(@$healthCheckPlansList[$a]['PAYMENTOPTIONS']=="FREE" || @$planStatus[0]['PLANNAME']!=''){?>&nbsp;|&nbsp;<a <?php if($_SESSION['policyexpiredisables']['vas.my_plans.schedule_appointment_icon']!='NO'){ ?> href="appointment_request_scheduler.php?id=<?php echo base64_encode(@$healthCheckPlansList[$a]['PLANID']);?>&tab=<?php echo base64_encode('existingapp'); ?>" <?php } else { ?>  onclick="alert('You can\'t schedule appointment for this plan as your policy is expired !')" <?php }  ?>>Schedule Appointment
						Now</a><?php } ?> </h3>
						  <?php   $purchaseplanListByPlanId = fetchpurchaseRetPlanDetailsByplanId(@$healthCheckPlansList[$a]['PURCHASEPLANID'],@$healthCheckPlansList[$a]['PLANID'],@$_SESSION['productId']);
						 
						  if(count($purchaseplanListByPlanId) > 0){ 
						  
						  ?>
							<?php
							$q=0;
							$f=1;
						   while($q<count($purchaseplanListByPlanId)){
							$k++;
						   ?>
							<tr>
							  <td><?php echo $k++; ?></td>
							  <td><?php echo @$purchaseplanListByPlanId[$q]['NAME']?stripslashes(@$purchaseplanListByPlanId[$q]['NAME']):'NA' ?></td>
							  <td><?php echo @$purchaseplanListByPlanId[$q]['RELATION']?stripslashes($relationArray[@$purchaseplanListByPlanId[$q]['RELATION']]):'NA' ?></td>
							  <td valign="middle" class="centericon">
							  <?php if(strtolower(@$purchaseplanListByPlanId[$q]['GENDER'])=='male'){ ?>
							  <span class="maleIcon"></span>
							  <?php } if(strtolower(@$purchaseplanListByPlanId[$q]['GENDER'])=='female'){ ?>
							  <span class="femaleIcon"></span>
							  <?php } ?>
							  </td>
							  <td><a href="view_health_plan_details.php?id=<?php echo base64_encode(@$healthCheckPlansList[$a]['PLANID']);?>" data-toggle="modal" data-target="#exampleModal1_<?php echo @$healthCheckPlansList[$a]['PLANID']; ?>" class="view_nav example8"><span class="eyeIcon"title="View"></span></a> <a <?php if($_SESSION['policyexpiredisables']['vas.my_plans.schedule_appointment_icon']!='NO'){ ?> href="appointment_request_scheduler.php?id=<?php echo base64_encode(@$healthCheckPlansList[$a]['PLANID']);?>&tab=<?php echo base64_encode('existingapp'); ?>" <?php }else { ?>  onclick="alert('You can\'t schedule appointment for this plan as your policy is expired !')" <?php }  ?>><span class="timeIcon" title="Appointment Request Scheduler"></span></a>
								<div class="modal fade" id="exampleModal1_<?php echo @$healthCheckPlansList[$a]['PLANID']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
								  <div class="modal-dialog" role="document">
									<div class="modal-content">
									  <div class="modal-header">
										<div class="model-header" id="exampleModalLabel">VIEW HEALTH CHECK-UP PLAN</div>
									  </div>
									 Please Wait....
									  
									</div>
								  </div>
								</div>           
							  
							  </td>
							</tr>
						  <?php 
						  $q++; } 
						  }
						  ?>
						<?php if(count($memberList) == 0 ) { ?>
						<div class="fl" style="display:none;">You have already booked appointment for this plan</div>
						<?php
						} ?>
						
						<div class="midContainer_whiteboxshadow"></div>
						
						<?php }  $start++; } } else { 
							$planCheck=0;
							
							
						if((isset($responseArray[0]['BGEN-MEMNAME']['#text'])) && ($responseArray[0]['BGEN-MEMNAME']['#text'] != "")){
								$g=0;
								 $v=0;
								// respone loop start
							
							foreach($responseArray as $key => $response){
									if(!is_array($response)){
									 continue;
									}
							
									if(trim($response['BGEN-GENDER']['#text'])=="F"){$gender="Female";	}
									if(trim($response['BGEN-GENDER']['#text'])=="M"){$gender="Male";	}
									
									
									if($key == 0 ){//if primary member
										$planDisplay=0;
										
										if(strtolower($gender)==strtolower(@$healthCheckPlansList[$a]['GENDER']) || strtolower(@$healthCheckPlansList[$a]['GENDER'])=="both" || strtolower(@$healthCheckPlansList[$a]['GENDER'])=="female"){
											 $planDisplay++;
										}
										
										// Start to check If PLANLIMITS Both than Primary member covered
										
										if(@$healthCheckPlansList[$a]['PLANLIMITS']=="BOTH"){				
													$ageName1="";
													$pieces = explode(',' , $healthCheckPlansList[$a]['AGEGROUPID']);
													$policyHolderAge=str_replace(",","",number_format(trim($response['BGEN-AGE']['#text'])));
													
													for($j=0;$j<count($pieces);$j++) {
													// age group according to new
														$ageName1 .= $pieces[$j].", ";
														if($policyHolderAge == $pieces[$j]){
																$planDisplay++;
																	//break;
														}
												   }
													
													$siName1="";
													$si_list = explode(',' , $healthCheckPlansList[$a]['SI']);
													$policyHolderSI=str_replace(",","",number_format(@$_SESSION['policyHolderSI']));
													for($j=0;$j<count($si_list);$j++) {
															$siName=fetchListById('CRMSI','SIID',@$si_list[$j]); 
															$siName1.=$siName[0]['SI'].", ";
															if($siName[0]['SI']==$policyHolderSI){
																$planDisplay++;
															}
													}
													
													
											}
											
											if($planDisplay==3){$planCheck=1;$planCheckformale=1;}else{$planCheck=0;}
											
											// End to check If PLANLIMITS Both than Primary member covered
									}else{}	
												
								}	// respone loop end	
									
						if($planCheck==1 || $planCheck1==1){
							if(@$healthCheckPlansList[$a]['PLANLIMITS']=="FAMILYALLOWED"){$planLimits="Family Only"; }else if(@$healthCheckPlansList[$a]['PLANLIMITS']=="BOTH"){$planLimits="Employee and Family"; }else{$planLimits="";}
						   
					if(@$healthCheckPlansList[$a]['PAYMENTOPTIONS']=="FREE"){
						$responseArray['purchaseId']=@$healthCheckPlansList[$a]['PURCHASEPLANID'];
						$responseArray['planId']=@$healthCheckPlansList[$a]['PLANID'];
						$responseArray['productId']=@$_SESSION['productId'];
						$responseArray['policyNumber']=$_SESSION['policyNumber'];
						$memberList=get_retmember_list_for_free_appointment(@$responseArray);
					}
					
					$v++;
						if(count($memberList) > 0 ) { 
						 $plan=1;
						$k++;
						?>
								  <tr>
									<td><?php echo $k; ?></td>
									<td><?php
						 echo @$healthCheckPlansList[$a]['PLANNAME']?stripslashes(@$healthCheckPlansList[$a]['PLANNAME']):stripslashes(@$healthCheckPlansList[$a]['PLANNAME2']); ?> <?php if($healthCheckPlansList[$a]['PAYMENTOPTIONS']=='FREE'){ echo "(Free)";  }
							$memberList=array();?></td>
									<td>
										<?php 
										if(isset($healthCheckPlansList[$a]['MEMBERTYPEID'])){
											$membersname= fetchcolumnListCond('TITLE','CRMCORPMEMBERTYPE',"WHERE STATUS='ACTIVE' AND MEMBERTYPEID IN(".$healthCheckPlansList[$a]['MEMBERTYPEID'].") ");
											$f=0;
											$titles=array();
											while($f<count($membersname)){
												$titles[]=$membersname[$f]['TITLE'];
											$f++; }
											if(count($titles)>0){
												$title=implode(',',$titles);
											} else {
												$title='';
											}
										} 
										echo $title?$title:'NA';
										?>
									</td>
									<td><strong class="greenGeneralTxt">Free</strong></td>
									<td><a href="view_health_plan_details.php?id=<?php echo base64_encode(@$healthCheckPlansList[$a]['PLANID']);?>"  data-toggle="modal" data-target="#exampleModal1_<?php echo @$healthCheckPlansList[$a]['PLANID']; ?>"  class="view_nav example8"><span class="eyeIcon" title="View"></span></a> <?php if(@$healthCheckPlansList[$a]['PAYMENTOPTIONS']=="FREE"){?> <a <?php if($_SESSION['policyexpiredisables']['vas.my_plans.schedule_appointment_icon']!='NO'){ ?> href="appointment_request_scheduler.php?id=<?php echo base64_encode(@$healthCheckPlansList[$a]['PLANID']);?>&tab=<?php echo base64_encode('existingapp'); ?>" <?php }else { ?>  onclick="alert('You can\'t schedule appointment for this plan as your policy is expired !')" <?php }  ?>><span class="timeIcon" title="Appointment Request Scheduler"></span></a><?php } ?>
											<div class="modal fade" id="exampleModal1_<?php echo @$healthCheckPlansList[$a]['PLANID']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
											  <div class="modal-dialog" role="document">
												<div class="modal-content">
												  <div class="modal-header">
													<div class="model-header" id="exampleModalLabel">VIEW HEALTH CHECK-UP PLAN</div>
												  </div>
														&nbsp;&nbsp;&nbsp;Please Wait....
												  
												</div>
											  </div>
											</div>           
									
									</td>
								  </tr>
						<?php if(count($memberList) == 0 ) { ?>
						<div class="fl" style="display:none;">You have already booked appointment for this plan</div>
						<?php  } ?>
				
						 
						<div class="midContainer_whiteboxshadow"></div>
							<?php }
							 $start++;}
							}
							}
							$a++;			
						  }
                    }
                     $a=0;
                     
                     while($a<count(@$purchasedPlanList)){ 
                         $ageName1="";
                            $ageName1="";
                            $pieces = explode(',' , $purchasedPlanList[$a]['AGEGROUPID']);
                            $policyHolderAge=str_replace(",","",number_format(@$_SESSION['policyHolderAge']));
                            for($j=0;$j<count($pieces);$j++) {
									// age group according to new
										$ageName1.=$pieces[$j].", ";
                                        if($policyHolderAge == $pieces[$j]){
                                                    $planDisplay++;
                                                    break;
                                        }
                                   }
                            $siName1="";
                            $si_list = explode(',' , $purchasedPlanList[$a]['SI']);
                            for($j=0;$j<count($si_list);$j++) {
                                    $siName=fetchListById('CRMSI','SIID',$si_list[$j]); 
                                    $siName1.=$siName[0]['SI'].", ";
                                    
                            }
                            
                            if(@$purchasedPlanList[$a]['PLANLIMITS']=="ONLYEMPLOYEES"){
                                    $planLimits="Employee Only";
                            } else if(@$purchasedPlanList[$a]['PLANLIMITS']=="BOTH"){
                                    $planLimits="Employee and Family";
                            }else if(@$purchasedPlanList[$a]['PLANLIMITS']=="FAMILYALLOWED"){
                                    $planLimits="Family Only";
                            }else{
                                    $planLimits="";
                            }
                           
                   
                if(@$purchasedPlanList[$a]['PAYMENTOPTIONS']=="BUY"){
					
                    $memberList=get_balance_retmember_list_for_appointment($purchasedPlanList[$a]['PURCHASEPLANID'],@$purchasedPlanList[$a]['PLANID'],@$_SESSION['productId']);//function to fetch records
                }
				if(count($memberList) > 0 ) { 
				 $plan=1;
                 
                      //echo @$purchasedPlanList[$a]['PURCHASEPLANID'];
                       $purchaseplanListByPlanId = fetchpurchaseRetPlanDetailsByplanId(@$purchasedPlanList[$a]['PURCHASEPLANID'],@$purchasedPlanList[$a]['PLANID'],@$_SESSION['productId']);
                      
                      if(count($purchaseplanListByPlanId) > 0){
                      
                        $q=0;
                        $f=1;
                       while($q<count($purchaseplanListByPlanId)){
                        $k++;
                       ?>
                        <tr>
                          <td><?php echo $k; ?></td>
                          <td><?php echo @$purchasedPlanList[$a]['PLANNAME']?stripslashes(@$purchasedPlanList[$a]['PLANNAME']):stripslashes(@$purchasedPlanList[$a]['PLANNAME2']); ?> <?php if($purchasedPlanList[$a]['PAYMENTOPTIONS']=='FREE'){ echo "(Free)";  } ?></td>
                          <td><?php echo @$purchaseplanListByPlanId[$q]['RELATION']?stripslashes($relationArray[@$purchaseplanListByPlanId[$q]['RELATION']]):'NA' ?></td>
                          <td valign="middle" class="centericon">
                           <?php echo @$purchasedPlanList[$a]['TOTALAMOUNT']?@$purchasedPlanList[$a]['TOTALAMOUNT']:'NA'; ?>
                          </td>
                          <td><a href="view_health_plan_details.php?id=<?php echo base64_encode(@$purchasedPlanList[$a]['PLANID']);?>&pId=<?php echo base64_encode(@$purchasedPlanList[$a]['PURCHASEPLANID']);?>"  data-toggle="modal" data-target="#exampleModal1_<?php echo @$healthCheckPlansList[$a]['PLANID']."_".$purchasedPlanList[$a]['PURCHASEPLANID']; ?>" class="view_nav example8"><span class="eyeIcon" title="View"></span></a>  <a  <?php if($_SESSION['policyexpiredisables']['vas.my_plans.schedule_appointment_icon']!='NO'){ ?> href="appointment_request_scheduler.php?id=<?php echo base64_encode(@$purchasedPlanList[$a]['PLANID']);?>&pId=<?php echo base64_encode(@$purchasedPlanList[$a]['PURCHASEPLANID']);?>&tab=<?php echo base64_encode('existingapp'); ?>" <?php } else { ?>  onclick="alert('You can\'t schedule appointment for this plan as your policy is expired !')" <?php } ?>><span class="timeIcon" title="Appointment Request Scheduler"></span></a>
                          
										<div class="modal fade" id="exampleModal1_<?php echo @$healthCheckPlansList[$a]['PLANID']."_".$purchasedPlanList[$a]['PURCHASEPLANID']; ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
										  <div class="modal-dialog" role="document">
											<div class="modal-content">
											  <div class="modal-header">
												<div class="model-header" id="exampleModalLabel">VIEW HEALTH CHECK-UP PLAN</div>
											  </div>
											 &nbsp;&nbsp;&nbsp;Please Wait....
											  
											</div>
										  </div>
										</div>           
                          
                          
                          </td>
                        </tr>
                       
                      <?php 
                      $q++; $start++; } 
                      }
                      ?>
                        
                             <?php  if(count($memberList) == 0 ) { ?>
                    <div class="fl" style="display:none;">You have already booked appointment for this plan</div>
                    <?php }  ?>
                   
                     
                    <div class="midContainer_whiteboxshadow"></div>
            <?php } $a++; } ?>
            
                  <?php  if($plan==0){?>
                              <tr>
                                <td colspan="5" style="text-align:center;">Sorry! you don't have any health plan added.</td>
                              </tr>
                                                     
                  <?php } else if($k==0){ ?>
                              <tr>
                                <td colspan="5" style="text-align:center;">Sorry! you don't have any health plan added.</td>
                              </tr>
                  
                  <?php } else {  }?>
                  <tbody>
                </table>
                <?php if($plan > 0 || $k > 0){ ?>
                 <ul class="pagination pagination-lg pager" id="myPager"></ul>
                 <?php } ?>
                </div>
                    <?php
				}
                ?>  
                

<!-- end of new implementation for health plan list -->
                
                
                <div class="clearfix"></div>
                </div>
            </div>
          </div>
        </div>
        
            </div>  
            <div class="col-md-3">
                <?php include("inc/inc.right.php"); ?>
            </div> 
             
        </div>
    </div>
</section>

<?php include("inc/inc.ft.php"); ?>
 <script src="js/pagination.js"></script>
 <script>$('#myTable').pageMe({pagerSelector:'#myPager',showPrevNext:true,hidePageNumbers:false,perPage:10});</script>
 <?php if(isset($pdflink) && !empty($pdflink) && isset($dlink) && !empty($dlink)){ ?>
<script>window.location.href='download.php?fileName=<?php echo $pdflink; ?>';</script>
<?php } ?>