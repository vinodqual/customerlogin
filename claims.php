<?php /*****************************************************************************
* COPYRIGHT
* Copyright 2013 Catabatic Technology Pvt Ltd.
* All rights reserved
*
* DISCLAIMER
*
*
* AUTHOR
*
* $Id: claims.php,v 1.0 2015/10/06 04:55:0 Amit Kumar Dubey Exp $
* $Author: Amit Kumar Dubey $
*
****************************************************************************/

include("inc/inc.hd.php");  			//include header file to show header on page
include_once($apiurl);
if(isset($_SESSION['response_error'])){
	$responseError  = $_SESSION['response_error'];
}else{
	$responseArray = $_SESSION['dataArray'];
}
if(@$TPAManagedchkdis=='Yes'){
	echo "<script>window.location.href='dashboard.php';</script>"; 
	exit;
}
$claimArray=array();
/**
 * 
 * @author Sanjeev Kumar
 * @todo This code written for upload reimbursement document.
 * Code written on 16-Feb-2017
 * In upload_reimbursement_doc() function pass file varisable, policy number and count for visible input field.
 * 
 */
$message_details=array();                   // Line added for initialize the message details array.
if(isset($_POST['formtype'])){
    $name		 = sanitize_center(@$_SESSION['policyHolderName']);	
    $phone		 =  sanitize_center(@$_POST['txtPhone']);
    $email		 =  sanitize_center(@$_POST['txtEmail']);
    $policyNumber=  sanitize_data(@$_SESSION['policyNumber']);
    $enquiry     =  sanitize_center(@$_POST['txtEnquiry']);
    $formtype     =  sanitize_data(@$_POST['formtype']);
    if($formtype=='CORPORATE' && $_SESSION['LOGINTYPE']=='CORPORATE'){
        if(empty($policyNumber)){
            $message_details[00]['msg']=SFTP0021;
            $message_details[00]['code']='SFTP0021';
            $message_details[00]['status']='ERROR';
        }
        $count=isset($_POST['count_visible_file_upload'])?$_POST['count_visible_file_upload']:'';
        if(empty($count) || $count<1){
            $message_details[01]['msg']=SFTP0022;
            $message_details[01]['code']='SFTP0022';
            $message_details[01]['status']='ERROR';
        }
        if(empty($message_details)){
            $entryTime=date('d-m-Y H:i');
            $sql="INSERT INTO CRMCOMPANYREIMBURSEMENTREQUEST (REQUESTID,COMPANYID,EMPLOYEEID,POLICYNO,EMPLOYEENAME,EMAIL,REMARKS,STATUS,CREATEDON,CREATEDBY,UPDATEDON,UPDATEDBY,REQUESTDATE,IPADDRESS) values(	CRMCOMREIMBREQU_SEQ.nextval,'".@$_SESSION['COMPANYID']."','".@$_SESSION['empId']."','".@$policyNumber."','".@$name."','".@$email."','".addslashes(@$enquiry)."','PENDING','".@$entryTime."','".@$_SESSION['userName']."','".@$entryTime."','".@$_SESSION['userName']."','".date('d-M-Y')."','".@$_SERVER['REMOTE_ADDR']."') ";
            $stdid = @oci_parse($conn, $sql);
            if(!oci_execute($stdid, OCI_NO_AUTO_COMMIT)){
                $message_details[02]['msg']=SFTP0011;
                $message_details[02]['code']='SFTP0011';
                $message_details[02]['status']='ERROR';
            }
            if(empty($message_details)){
                $check1 = @oci_parse($conn, 'SELECT CRMCOMREIMBREQU_SEQ.currval FROM DUAL');
                if(!oci_execute($check1, OCI_NO_AUTO_COMMIT)){
                    $message_details[02]['msg']=SFTP0011;
                    $message_details[02]['code']='SFTP0011';
                    $message_details[02]['status']='ERROR';
                }
                if(empty($message_details)){
                    $res=oci_fetch_assoc($check1);
                    $rowid=$res['CURRVAL'];
                    $roll_back='YES';
                    $success_file='';
                    for($r=1;$r<=$count;$r++){      // Iterate loop to count number of files user want to upload
                        $response_array=upload_reimbursement_doc('upload'.$r,$policyNumber);        // Function used to upload reimbursement document
                        if(isset($response_array['status']) && $response_array['status']=='SUCCESS'){       //Check if no error occurs in other report file uploading
                            $filename=$response_array['file_name'];
                            $docTitle     =  sanitize_center(@$_POST['docTitle'.$r]);
                            $sql1="INSERT INTO CRMCOMPANYREIMBURSEMENTDOCS (DOCID,REQUESTID,DOCTITLE,FILENAME,UPLOADDATE,CREATEDON,CREATEDBY,UPDATEDON,UPDATEDBY,IPADDRESS) values(	SYSTEM.CRMCOMREIMBDOCS_SEQ.nextval,'".@$rowid."','".@$docTitle."','".@$filename."','".date('d-M-Y')."','".@$entryTime."','".@$_SESSION['userName']."','".@$entryTime."','".@$_SESSION['userName']."','".@$_SERVER['REMOTE_ADDR']."') ";
                            $stdid = @oci_parse($conn, $sql1);
                            if(oci_execute($stdid)){
                                $success_file.=$filename.', ';
                                oci_commit($conn);
                                $roll_back='NO';
                                $message_details[02]['msg']=str_replace('#FILENAME#',$success_file,SFTP0012);
                                $message_details[02]['code']='SFTP0012';
                                $message_details[02]['status']='SUCCESS';
                            }else{
                                $message_details[$i]['msg']=SFTP0011;
                                $message_details[$i]['code']='SFTP0011';
                                $message_details[$i]['status']='ERROR';
                            }
                        }else{
                            $message_details=$response_array;

                        }   // End of check for file upload successfully 
                    }   // End of for loop for insert document details
                    if($roll_back=='YES'){     // Roll back transaction if no file upload successfully.
                        oci_rollback($conn);
                    }
                }   // End of check if successfully fetch current valus of sequence
            }   // End of check if successfully insert reimburserequest   
        }
    } else {            // Code start for RETAIL USERS
        if(empty($policyNumber)){
            $message_details[00]['msg']=SFTP0021;
            $message_details[00]['code']='SFTP0021';
            $message_details[00]['status']='ERROR';
        }
        $count=isset($_POST['count_visible_file_upload'])?$_POST['count_visible_file_upload']:'';
        if(empty($count) || $count<1){
            $message_details[01]['msg']=SFTP0022;
            $message_details[01]['code']='SFTP0022';
            $message_details[01]['status']='ERROR';
        }
        if(empty($message_details) && !empty($_SESSION['productId'])){
            $entryTime=date('d-m-Y H:i');
            $sql="INSERT INTO CRMRETAILREIMBURSEMENTREQUEST (REQUESTID,PRODUCTID,EMPLOYEEID,POLICYNO,EMPLOYEENAME,EMAIL,REMARKS,STATUS,CREATEDON,CREATEDBY,UPDATEDON,UPDATEDBY,REQUESTDATE,IPADDRESS) values(CRMRETREIMBREQU_SEQ.nextval,'".@$_SESSION['productId']."','".@$_SESSION['customerId']."','".@$policyNumber."','".@$name."','".@$email."','".addslashes(@$enquiry)."','PENDING','".@$entryTime."','".@$_SESSION['userName']."','".@$entryTime."','".@$_SESSION['userName']."','".date('d-M-Y')."','".@$_SERVER['REMOTE_ADDR']."') ";
            $stdid = @oci_parse($conn, $sql);
            if(!oci_execute($stdid, OCI_NO_AUTO_COMMIT)){
                $message_details[02]['msg']=SFTP0011;
                $message_details[02]['code']='SFTP0011';
                $message_details[02]['status']='ERROR';
            }
            if(empty($message_details)){
                $check1 = @oci_parse($conn, 'SELECT CRMRETREIMBREQU_SEQ.currval FROM DUAL');
                if(!oci_execute($check1, OCI_NO_AUTO_COMMIT)){
                    $message_details[02]['msg']=SFTP0011;
                    $message_details[02]['code']='SFTP0011';
                    $message_details[02]['status']='ERROR';
                }
                if(empty($message_details)){
                    $res=oci_fetch_assoc($check1);
                    $rowid=$res['CURRVAL'];
                    $roll_back='YES';
                    $success_file='';
                    for($r=1;$r<=$count;$r++){      // Iterate loop to count number of files user want to upload
                        $response_array=upload_reimbursement_doc('upload'.$r,$policyNumber);        // Function used to upload reimbursement document
                        if(isset($response_array['status']) && $response_array['status']=='SUCCESS'){       //Check if no error occurs in other report file uploading
                            $filename=$response_array['file_name'];
                            $docTitle     =  sanitize_center(@$_POST['docTitle'.$r]);
                            $sql1="INSERT INTO CRMRETAILREIMBURSEMENTDOCS (DOCID,REQUESTID,PRODUCTID,DOCTITLE,FILENAME,UPLOADDATE,CREATEDON,CREATEDBY,UPDATEDON,UPDATEDBY,IPADDRESS) values(CRMRETREIMBDOCS_SEQ.nextval,'".@$rowid."','".@$_SESSION['productId']."','".@$docTitle."','".@$filename."','".date('d-M-Y')."','".@$entryTime."','".@$_SESSION['userName']."','".@$entryTime."','".@$_SESSION['userName']."','".@$_SERVER['REMOTE_ADDR']."') ";
                            $stdid = @oci_parse($conn, $sql1);
                            if(oci_execute($stdid)){
                                $success_file.=$filename.', ';
                                oci_commit($conn);
                                $roll_back='NO';
                                $message_details[02]['msg']=str_replace('#FILENAME#',$success_file,SFTP0012);
                                $message_details[02]['code']='SFTP0012';
                                $message_details[02]['status']='SUCCESS';
                            }else{
                                $message_details[$i]['msg']=SFTP0011;
                                $message_details[$i]['code']='SFTP0011';
                                $message_details[$i]['status']='ERROR';
                            }
                        }else{
                            $message_details=$response_array;

                        }   // End of check for file upload successfully 
                    }   // End of for loop for insert document details
                    if($roll_back=='YES'){     // Roll back transaction if no file upload successfully.
                        oci_rollback($conn);
                    }
                }// End of check if successfully fetch current valus of sequence
            }   // End of check if successfully insert reimburserequest 
        }
	FunSendReimbursementMail($email,$policyNumber,$name,$phone,$enquiry);
    } 	
	//echo "<script>window.location.href='claims.php?tab=cmVpbWJ1cg==&msg=$msg'</script>";
	//echo "<script>window.location.replace(claims.php?tab=cmVpbWJ1cg==');</script>";
	//header('Location: claims.php?tab=cmVpbWJ1cg==&msg='.$msg);
	//exit;
}
/*------------------- End of Code -----------------------*/

?>

<style>
div.error {
	position:absolute;
	margin-top:14px;
	color:red;
	background-color:#fff;
	padding:3px;
	text-align:left;
	z-index:1;
	margin-left: -17px;
	font-size:14px;
}
</style>
<style>
div #upload1-error {
	margin-left: 231px;
    margin-top: 52px;
}
div #upload2-error {
	margin-left: 231px;
    margin-top: 52px;
}
div #upload3-error {
	margin-left: 231px;
    margin-top: 52px;
}
div #upload4-error {
	margin-left: 231px;
    margin-top: 52px;
}
div #upload5-error {
	margin-left: 231px;
    margin-top: 52px;
}
</style>
<script>
$(document).ready(function(){
	$(".downloaddocument").attr('onclick',"window.location.href='downloadCenter.php'");
	$(".downloadhospitals").attr('onclick',"window.location.href='network-hospital-for-uat.php'");
	$(".nonprefferedhospitals").attr('onclick',"window.location.href='non-preferred-hospitals.php'");
});
</script>

<section id="middleContainer">
  <div class="container-fluid">
    <div class="middlebox">
      <div class="col-md-9">
        <div class="middlebox-left">
          <ul class="nav nav-tabs responsive" id="myTab">
            <li class="test-class <?php if(empty($tab) || $tab=="claims"){ echo "active"; } ?>"><a class="deco-none misc-class" href="#claims"> Claims</a></li>
            <li class="test-class <?php if(!empty($tab) && $tab=="reimbur"){ echo "active"; } ?>"><a href="#reimbursment">Reimbursement</a></li>
            <li class="test-class downloaddocument <?php if(!empty($tab) && $tab=="downloaddocument"){ echo "active"; } ?>"><a href="javascript:void(0);" onclick="window.location.href='downloadCenter.php'">Download Documents</a></li>
            <li class="test-class downloadhospitals <?php if(!empty($tab) && $tab=="downloadhospitals"){ echo "active"; } ?>"><a href="javascript:void(0);" onclick="window.location.href='network-hospital-for-uat.php'">Network Hospitals</a></li>
			<li class="test-class nonprefferedhospitals <?php if(!empty($tab) && $tab=="nonprefferedhospitals"){ echo "active"; } ?>"><a href="javascript:void(0);" onclick="window.location.href='non-preferred-hospitals.php'">Non Preferred Hospitals</a></li>
          </ul>
          <div class="tab-content responsive">
            <div class="tab-pane <?php if(empty($tab) || $tab=="claims"){ echo "active"; } ?>" id="claims">
            
            <div class="tab-paneIn">
              <div class="col-md-12">
                
                
                <div class="title-bg textleft">
                <h1>Policy Details</h1>
                </div>
                
                <div class="myPlanForm">
                   <?php if(isset($responseArray) && !empty($responseArray)){ ?>
                    <table class="responsive" width="100%">
                  <tr>
                    <th>Owner Name</th>
                    <th>Employee Name</th>
                    <th>Policy No.</th>
                    <th>Employee ID/Customer Id</th>
                  </tr>
                  <tr>                    
                    <td><?php  echo (!empty($_SESSION['owner_name']))?$_SESSION['owner_name']:'NA';?></td>
                    <td><?php  				
					echo (!empty($responseArray[0]['BGEN-MEMNAME']['#text']) && isset($responseArray[0]['BGEN-MEMNAME']['#text']))? ucwords(strtolower(trim($responseArray[0]['BGEN-MEMNAME']['#text']))):'NA';?></td>
                    <td><?php echo (!empty($_SESSION['policyNumber']))	?	$_SESSION['policyNumber']:'NA'	;?></td>
                    <td><?php echo (!empty($responseArray[0]['BGEN-EMPNO']['#text'] ))?$responseArray[0]['BGEN-EMPNO']['#text']:@$_SESSION['customerId'];	;?></td>
                  </tr> 
                                                     
                </table>
                   <?php } else { ?>
				   <table class="responsive" width="100%">
                    <tr>
                        <td colspan="10" align="center" style="text-align:-moz-center;border:none;"><font color="#cc0000"><b>Please Try Again or </b></font><a href="individual-policy-registration.php" target="_blank">Register your policy</a></td>
                    </tr>
					</table>
                   <?php } ?>
	            </div> 
              </div>
              <div class="clearfix"></div>
              </div>
              <div class="colfullBot">
              <div class="tab-paneIn">
              <div class="col-md-12">
              
                <div class="title-bg textleft">
                <h1>Claim List</h1>
                </div>
                
                <div class="myPlanForm">
                    	
                        
                <table class="responsive" width="100%">
                 <?php
            $displayFromrecievedonCol=''; $displayPayeename='';
	  	$claimNumbersArr = array();
		
           if(count(@$responseArray)>0 && !empty($responseArray)){ ?>
		    <?php
		   $r=0;
                   
		foreach($responseArray as $key => $response){
			if($key == 0){
				$clntNumber=trim($response['BGEN-CLNTNM']['#text']);
			}else{
				$clntNumber=trim($response['BGEN-CLNTNM']['#text']);
			}
			$query['GCOCCNO']="";
			$query['CHDRNUM']=@$_SESSION['policyNumber'];
			//$query['CHDRNUM']='10003683';
			$query['CLAMNUM']='';
			$query['PREAUTNO']="";
			$query['CLNTNUM']=$clntNumber;
			//$query['CLNTNUM']='50106617';
			$claimArray = getClaimEnquiry($query);
			//echo "<pre>"; print_r($claimArray ); die;
			for($c=0;$c<count(@$claimArray['claimList']);$c++){
			switch(@$claimArray['claimList'][$c]['BGEN-CLMSRVTP']['#text']){
				case 'R': $claimType="Reimbursement";
						break;
				case 'C': $claimType="Cashless";
						break;
			}
			$benefitGroup=@$claimArray['claimList'][$c]['BGEN-BNFTGRP']['#text'];
			if($benefitGroup!='HHCK'){
				$claimStatusGroup=@$claimArray['claimList'][$c]['BGEN-GCSTS']['#text'];
				$secondaryStatusGroup=@$claimArray['claimList'][$c]['BGEN-GCSECSTS']['#text'];
				$benefitGroupName=@$benefitGroupArray[@$benefitGroup];
				$claimGroupName=@$claimStatusArray[@$claimStatusGroup];
				$secondaryStatus=@$secondaryStatusArray[@$secondaryStatusGroup];
							
							$claimNumbersArr[] = $claimArray['claimList'][$c]['BGEN-CLAMNUM']['#text'];
				 if(@$claimArray['claimList'][$c]['BGEN-GCSECSTS']['#text']=="PD"){
					
					$displayFromrecievedonCol="&secst=".base64_encode(@$claimArray['claimList'][$c]['BGEN-GCSECSTS']['#text']);
				 }
				 if(@$claimArray['claimList'][$c]['BGEN-GCSECSTS']['#text']=="AP" && @$claimArray['claimList'][$c]['BGEN-GCSTS']['#text']=="CC"){
					
					$displayPayeename="&pname=".base64_encode(1);
				 }
							
				 $r++;
				 
				 if($r == 1){
		?>
            <tr>
                    <th>Claim/AL </th>
                    <th>Member Name</th>
                    <th>Claim/AL Type</th>
                    <th>Benefit Gp</th>
                    <th>Date of Visit</th>
                    <th>Claim Amt</th>
                    <th>Claim/AL Status</th>
                    <th>Sec. Status</th>
                    <th>App. Amt.</th>
                    <th>Action</th>
                  </tr>
           <?php } ?>
            <tr>
              <td><?php echo $claimArray['claimList'][$c]['BGEN-CLAMNUM']['#text'];?> - <?php echo $claimArray['claimList'][$c]['BGEN-GCOCCNO']['#text'];?></td>
              <td><?php echo ltrim($claimArray['claimList'][$c]['BGEN-MEMNAME']['#text'],".");?></td>
              <td><?php echo $claimType;?></td>
              <td><?php echo $benefitGroupName;?></td>
              <td><?php echo dateChange($claimArray['claimList'][$c]['BGEN-DTEVISIT']['#text']);?></td>
              <td><?php echo number_format($claimArray['claimList'][$c]['BGEN-INCURRED']['#text']);?></td>
              <td><?php echo $claimGroupName;?></td>
              <td><?php if(@$claimArray['claimList'][$c]['BGEN-GCSECSTS']['#text']=="PI"){echo "";}else{echo $secondaryStatus;}?></td>
              <td><?php if ($claimStatusGroup=="CA" || @$secondaryStatusGroup=="AP"){ echo number_format($claimArray['claimList'][$c]['BGEN-TLHMOSHR']['#text']); }else{echo "0";}?></td>
            
                    <td>
                    <a href="claim_details.php?claimNumber=<?php echo base64_encode($claimArray['claimList'][$c]['BGEN-CLAMNUM']['#text']);?>&gcocNo=<?php echo base64_encode($claimArray['claimList'][$c]['BGEN-GCOCCNO']['#text']);?>&clntNum=<?php echo base64_encode($claimArray['claimList'][$c]['BGEN-CLNTNUM']['#text']);?>&inwrdNo=<?php echo base64_encode(@$claimArray['claimList'][$c]['BGEN-INWRDNO']['#text']).$displayFromrecievedonCol.$displayPayeename; ?>" class="view_nav"><span class="eyeIcon" title="Claim Summary"></span></a>
                    </td>
            </tr>
            <?php }
			}   }
				if($r == 0){
				?>
                <tr>
				<td colspan="10" align="center" style="text-align:-moz-center;border:none;"><font color="#cc0000"><b>No record(s) found.</b></font></td>
            </tr>
                <?php
				}
			 }  else{ ?>
            <tr>
				<td colspan="10" align="center" style="text-align:-moz-center;border:none;"><font color="#cc0000"><b>No record(s) found.</b></font></td>
            </tr>
            <?php } ?>
                </table>
                    </div>                     
              </div>
              <div class="clearfix"></div>
              </div>
              </div>
            </div>
           <div class="tab-pane <?php if(!empty($tab) && $tab=="reimbur"){ echo "active"; } ?>" id="reimbursment"><div class="tab-paneIn">
            <div class="col-md-12">
                <div class="title-bg textleft">
                <h1>Reimbursements</h1>
                </div>
                <div class="myPlanForm">
                   <?php 
				if($_SESSION['LOGINTYPE']=='CORPORATE'){
					$reimbursementList=getReimbursementList(@$_SESSION['empId'],@$_SESSION['policyNumber']);	
				 } else {
					$reimbursementList=getRetReimbursementList(@$_SESSION['customerId'],@$_SESSION['policyNumber']);	
    			 }
				 ?>
		   <table class="responsive" width="100%">
            <tr>
              <th>S.No.</th>
              <th>Request No.</th>
              <th>Request Date</th>
              <th>Reason of Resubmission</th>
              <th>Status</th>
              <th class="brdrnone">Action</th>
            </tr>
           	 <tbody id="myTable" >
					<?php
                    $a=0;
                    $k=1;
                    if(count(@$reimbursementList)>0){
                    while($a<count(@$reimbursementList)){
                    ?>
                         <tr>
                      <td><?php echo $k;?>.</td>
                      <td><?php echo stripslashes(@$reimbursementList[$a]['REQUESTID']);?></td>
                      <td><?php echo stripslashes(@$reimbursementList[$a]['REQUESTDATE']);?></td>
                      <td><?php echo stripslashes(@$reimbursementList[$a]['REASONFORRESUBMISSION'])?stripslashes(@$reimbursementList[$a]['REASONFORRESUBMISSION']):'NA';?></td>
                      <td><?php echo stripslashes(@$reimbursementList[$a]['STATUS']);?></td>
                      <td class="brdrnone"><?php if(@$reimbursementList[$a]['STATUS']=='OPEN' && @$reimbursementList[$a]['REASONFORRESUBMISSION']!=''){?>
                    <a href="reimbursement_doc.php?requestId=<?php echo base64_encode(@$reimbursementList[$a]['REQUESTID']);?>" class="view_nav">Re-Submit</a>
                    <?php }else{echo "NA";} ?>&nbsp; | <a data-toggle="modal" data-target="#exampleModal1"  href="view_reimbursement.php?id=<?php echo base64_encode(@$reimbursementList[$a]['REQUESTID']);?>" class="view_nav example8">View</a></td>
                    </tr>
                     <?php $a++;$k++;} }else{ ?>
                         <tr>
                    <td colspan="6" style="padding-left:312px !important;">No Reimbursement</td>
                    </tr>
                    <?php } ?>
       			 </tbody>
                </table>
                <?php if(count(@$reimbursementList) > 0){ ?>
                <ul class="pagination pagination-lg pager" id="myPager"></ul>
                <?php } ?>
                    </div>                     
              </div>
            <div class="clearfix"></div></div>
            <div class="colfullBot">
              <div class="tab-paneIn">
              <div class="col-md-12">
                <div class="title-bg textleft">
                <h1>Claim Intimation</h1>
                </div>
                 <form action="" method="post" name="DocRequestForm" id="DocRequestForm" enctype="multipart/form-data">
                	<div class="grayBorder">
                	<h2>User Details</h2>
                    <div class="myPlanForm">
                    	<div class="myPlanformBox30per myPlanLeft mrgnMyPlan">
                        	<label>Client ID/Emp No. <span class="greenTxt">*</span></label>
                            <div class="inputBox inputBoxcolor">
								
                            	<div class="inputBoxIn">
                            	  <?php 
     								if($_SESSION['LOGINTYPE']=='CORPORATE'){
									  echo @$_SESSION['empId']?@$_SESSION['empId']:'NA';
									} else {
									  echo @$_SESSION['customerId']?@$_SESSION['customerId']:'NA';
									}
								  ?>
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        <div class="myPlanformBox30per myPlanLeft">
                        	<label>Name <span class="greenTxt">*</span></label>
                            <div class="inputBox inputBoxcolor">
                            	<div class="inputBoxIn">
                                	<?php echo @$_SESSION['policyHolderName']?@$_SESSION['policyHolderName']:'NA';?>
                                </div>
                            </div>
                        <div class="clearfix"></div></div>
                        
                        
                        <div class="myPlanformBox30per myPlanRight">
                        	<label>Policy No <span class="greenTxt">*</span></label>
                            <div class="inputBox inputBoxcolor">
                            	<div class="inputBoxIn">
                                	<?php echo @$_SESSION['policyNumber']?@$_SESSION['policyNumber']:'NA';?>
                                </div>
                            </div>
                        <div class="clearfix"></div></div>
                        
                        
                        <div class="myPlanformBox30per myPlanLeft mrgnMyPlan form-group">
                        	<label>Email Id <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                            	  <input type="text" id="txtEmail" name="txtEmail" class="txtField"  value="" placeholder="Enter Email">
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        
                        <div class="myPlanformBox30per myPlanLeft">
                        	<label>Contact No <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                            	  <input type="text" name="txtPhone" id="txtPhone" class="txtField" value="" placeholder="Enter Contact No">
                            	</div>
                            </div>
                        <div class="clearfix"></div></div>
                        
                        
                        <div class="myPlanformBox30per myPlanRight">
                        	<label>Request <span class="greenTxt">*</span></label>
                            <div class="inputBox">
                            	<div class="inputBoxIn">
                                	<input type="text" name="txtEnquiry" id="txtEnquiry"  value="" class="txtField" placeholder="Enter Request" >
                                </div>
                            </div>
                        <div class="clearfix"></div></div>
                        
                        
                    <div class="clearfix"></div></div>
                   
                    <h2>Upload Docs</h2>
                    <div class="myPlanForm">
                    	
                        
                        <div class="uploadDocTitle">Doc Title <span class="greenTxt">*</span></div>
                        <div>
                            <div class="uploadDocFormBox">
                                <div class="leftCol45"><div class="inputBox forMobInput">
                                    <div class="inputBoxIn">
                                      <input type="text" name="docTitle1" id="docTitle1" AUTOCOMPLETE="OFF"  class="txtField" value="" placeholder="Enter Title" onFocus="this.placeholder=''" onBlur="this.placeholder='Enter Title'">
                                    </div>
                                </div>
								<input type="file" name="upload1" id="upload1" style="-moz-opacity:0 ; filter:alpha(opacity: 0); opacity: 0; position: absolute; margin-left:345px; width:2px;" onChange="$('#filename1').html(this.value)">
								
                                <!--<input type="file" name="upload1" id="upload1" style="display:none" onChange="$('#filename1').html(this.value)"> -->
								
                                <a class="cancel-btn right forMob" href="javascript:document.getElementById('upload1').click();">Browse &gt;</a>
                                </div>
                                <div class="rightCol55"><div class="inputBox">
                                    <div class="inputBoxIn" id="filename1">
                                      No file Selected
                                    </div>
                                </div>
                                <a class="plus-btn right" onclick="showClass('docdiv2');" href="javascript:void(0);">+</a></div>
                            </div>
                            <div class="uploadDocGrayTitle uploadDocGrayTitleMob">[*.jpg,*.xls,*.doc and *.pdf only]</div>
                		</div>
                
                        <div class="docdiv2" style="display:none;">
                            <div class="uploadDocFormBox">
                                <div class="leftCol45"><div class="inputBox forMobInput">
                                    <div class="inputBoxIn">
                                      <input type="text" name="docTitle2" id="docTitle2" maxlength="200" AUTOCOMPLETE="OFF"  class="txtField" placeholder="Enter Title" onFocus="this.placeholder=''" onBlur="this.placeholder='Enter Title'" value="">
                                    </div>
                                </div>
                                <input type="file" name="upload2" id="upload2" style="-moz-opacity:0 ; filter:alpha(opacity: 0); opacity: 0; position: absolute; margin-left:345px; width:2px;" onChange="$('#filename2').html(this.value)">
                                <a class="cancel-btn right forMob" href="javascript:document.getElementById('upload2').click();">Browse &gt;</a>
                                </div>
                                <div class="rightCol55"><div class="inputBox">
                                    <div class="inputBoxIn" id="filename2">
                                      No file Selected
                                    </div>
                                </div>
                                <a class="plus-btn right"  onclick="showClass('docdiv3');" href="javascript:void(0);">+</a></div>
                            </div>
                            <div class="uploadDocGrayTitle uploadDocGrayTitleMob">[*.jpg,*.xls,*.doc and *.pdf only]</div>
                		</div>
                
                        <div class="docdiv3" style="display:none;">
                            <div class="uploadDocFormBox">
                                <div class="leftCol45"><div class="inputBox forMobInput">
                                    <div class="inputBoxIn">
                                      <input type="text" name="docTitle3" id="docTitle3" maxlength="200" AUTOCOMPLETE="OFF" class="txtField" placeholder="Enter Title" onFocus="this.placeholder=''" onBlur="this.placeholder='Enter Title'" value="">
									  
                                    </div>
                                </div>
                                <input type="file" name="upload3" id="upload3" style="-moz-opacity:0 ; filter:alpha(opacity: 0); opacity: 0; position: absolute; margin-left:345px; width:2px;" onChange="$('#filename3').html(this.value)">
                                <a class="cancel-btn right forMob" href="javascript:document.getElementById('upload3').click();">Browse &gt;</a>
                                </div>
                                <div class="rightCol55"><div class="inputBox">
                                    <div class="inputBoxIn" id="filename3" name="filename3">
                                      No file Selected
                                    </div>
                                </div>
                                <a class="plus-btn right"  onclick="showClass('docdiv4');" href="javascript:void(0);">+</a></div>
                            </div>
                            <div class="uploadDocGrayTitle uploadDocGrayTitleMob">[*.jpg,*.xls,*.doc and *.pdf only]</div>
                		</div>
                
                        <div class="docdiv4" style="display:none;">
                            <div class="uploadDocFormBox">
                                <div class="leftCol45"><div class="inputBox forMobInput">
                                    <div class="inputBoxIn">
                                      <input type="text" name="docTitle4" id="docTitle4" maxlength="200" AUTOCOMPLETE="OFF"  class="txtField" placeholder="Enter Title" onFocus="this.placeholder=''" onBlur="this.placeholder='Enter Title'" value="">
                                    </div>
                                </div>
                                <input type="file" name="upload4" id="upload4" style="-moz-opacity:0 ; filter:alpha(opacity: 0); opacity: 0; position: absolute; margin-left:345px; width:2px;" onChange="$('#filename4').html(this.value)">
                                <a class="cancel-btn right forMob" href="javascript:document.getElementById('upload4').click();">Browse &gt;</a>
                                </div>
                                <div class="rightCol55"><div class="inputBox">
                                    <div class="inputBoxIn" id="filename4">
                                      No file Selected
                                    </div>
                                </div>
                                <a class="plus-btn right"  onclick="showClass('docdiv5');" href="javascript:void(0);">+</a></div>
                            </div>
                            <div class="uploadDocGrayTitle uploadDocGrayTitleMob">[*.jpg,*.xls,*.doc and *.pdf only]</div>
                		</div>
                
                        <div class="docdiv5" style="display:none;">
                            <div class="uploadDocFormBox">
                                <div class="leftCol45"><div class="inputBox forMobInput">
                                    <div class="inputBoxIn">
                                      <input type="text" name="docTitle5" id="docTitle5" maxlength="200" AUTOCOMPLETE="OFF"  class="txtField" placeholder="Enter Title" onFocus="this.placeholder=''" onBlur="this.placeholder='Enter Title'" value="">
                                    </div>
                                </div>
                                <input type="file" name="upload5" id="upload5" style="-moz-opacity:0 ; filter:alpha(opacity: 0); opacity: 0; position: absolute; margin-left:345px; width:2px;" onChange="$('#filename5').html(this.value)">
                                <a class="cancel-btn right forMob" href="javascript:document.getElementById('upload5').click();">Browse &gt;</a>
                                </div>
                                <div class="rightCol55"><div class="inputBox">
                                    <div class="inputBoxIn" id="filename5">
                                      No file Selected
                                    </div>
                                </div>
                                <a class="plus-btn right" href="javascript:void(0);">+</a></div>
                            </div>
                            <div class="uploadDocGrayTitle uploadDocGrayTitleMob">[*.jpg,*.xls,*.doc and *.pdf only]</div>
                		</div>
                
                
                    <div class="clearfix"></div>
                    
                    </div>

                   <div class="myPlanBtn">
						<input type="hidden" value="<?php echo @$_SESSION['LOGINTYPE']; ?>" name="formtype" >
                    	<a href="javascript:void(0);" onClick="$('#DocRequestForm').submit();" class="submit-btn">Submit ></a>
                    </div> 
                </div>                     
                 </form>
              </div>
              <div class="clearfix"></div>
              </div>
              </div>
            </div>            
          </div>
        </div>
      </div>
        <div class="col-md-3">
            <?php include("inc/inc.right.php"); ?>
        </div> 
    </div>
  </div>
</section>
<script>
     $(function(){
         $('.closeBox').click(function(){
              $('#exampleModal1').modal('hide');
          });
      });
 </script>

<script type='text/javascript'>
$(window).load(function(){
     $(function(){
         $(".dropdown-menu").on("click", "li", function(event){
             console.log("You clicked the drop downs", event)
         })
     })
});
</script>

<div class="modal fade" id="exampleModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="model-header" id="exampleModalLabel">Reimbursement Doc List</div>
      </div>
     Please Wait....
      
    </div>
  </div>
</div>
<?php include("inc/inc.ft.php"); ?>
<script src="js/jquery.validate.min.js" type="text/javascript"></script>
<script src="js/additional-methods.min.js" type="text/javascript"></script>
<script src="js/owl.carousel.js" type="text/javascript"></script>
<script type="text/javascript">
	$.validator.addMethod("noStartEndWhiteSpaces", function(value, element) {
			return this.optional(element) || /^[a-zA-Z0-9](?:[a-zA-Z0-9 ]*[a-zA-Z0-9])?$/.test(value);
		}, "Spaces or special characters not allowed.");		
	$.validator.addMethod("noStartEndWhiteSpacesAlpha", function(value, element) {
		return this.optional(element) || /^[a-zA-Z](?:[a-zA-Z ]*[a-zA-Z])?$/.test(value);
	}, "Number not allowed.");

	jQuery.validator.addMethod("specialChars", function (value, element) {
	var regex = new RegExp("^[a-zA-Z0-9]+$");
	var key = value;

	if (!regex.test(key)) {
		return false;
	}
	return true;
	}, "Special characters not allowed");
		
    $("#DocRequestForm").validate({
    rules :{
        "txtEmail" : {
            required : true,
			email: true
        },
		"txtPhone" : {
		   required : true,
		   number: true,
		   maxlength: 10,
		   minlength: 10
        },
		"docTitle1" :{
			required : true,
		},
		"docTitle2" :{
			required : true,
		},
		"docTitle3" :{
			required : true,
		},
		"docTitle4" :{
			required : true,
		},
		"docTitle5" :{
			required : true,
		},
		"txtEnquiry" :{
			required : true,
		},
		"upload1":{
			extension: "jpg|doc|pdf|xls|"
		},
		"upload2":{
			extension: "jpg|doc|pdf|xls|"
		},
		"upload3":{
			extension: "jpg|doc|pdf|xls|"
		},
		"upload4":{
			extension: "jpg|doc|pdf|xls|"
		},
		"upload5":{
			extension: "jpg|doc|pdf|xls|"
		}
		
			
    },
    messages :{
        "txtEmail" : {
            required : 'Please enter email'
        },
		"txtPhone" : {
            required : 'Please enter contact no'
        },
		"docTitle1":{
			required: 'Please enter doc title'
		},
		"docTitle2":{
			required: 'Please enter doc title'
		},
		"docTitle3":{
			required: 'Please enter doc title'
		},
		"docTitle4":{
			required: 'Please enter doc title'
		},
		"docTitle5":{
			required: 'Please enter doc title'
		},
		"upload1":{
			extension: 'Please choose correct file'
		},
		"upload2":{
			extension: 'Please choose correct file'
		},
		"upload3":{
			extension: 'Please choose correct file'
		},
		"upload4":{
			extension: 'Please choose correct file'
		},
		"upload5":{
			extension: 'Please choose correct file'
		}
    },
	
	errorElement: "div"	
  }); 

// custom rule for checking . and character after . for email validation
jQuery.validator.addMethod("email", function (value, element) {
	return this.optional(element) || (/^[a-zA-Z0-9]+([-._][a-zA-Z0-9]+)*@([a-zA-Z0-9]+(-[a-zA-Z0-9]+)*\.)+[a-zA-Z]{2,4}$/.test(value) && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(value));
}, 'Please enter valid email address.');

</script>
<script src="js/pagination.js"></script>
