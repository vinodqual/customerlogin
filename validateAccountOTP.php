<?php
include_once("conf/conf.php"); //include configuration file to use databse connectivity 
include_once("conf/common_functions.php");
global $conn;
$code =sanitize_data($_POST['code']);

//echo $query = "SELECT OTP FROM CUSTOMEROTP WHERE OTP='$_POST['code']' AND STATUS='1'";
$query = "SELECT OTP,USERTYPE,EMAIL FROM CUSTOMEROTP WHERE OTP='$code' AND STATUS=1";
$sql = @oci_parse($conn, $query);	
@oci_execute($sql);
$record = oci_fetch_assoc($sql);
if($record['OTP'] != ''){
	// update record
	if($record['USERTYPE'] =='CORPORATE'){
		// update center table
		$status ='ACTIVE';
		$attempts = 0;
		$email = $record['EMAIL'];
		// update status
		$sql2 = "UPDATE CRMEMPLOYEELOGIN SET STATUS='$status',BADATTEMPTS='$attempts' WHERE EMAIL='$email'";
		$stdid = @oci_parse($conn, $sql2);
		$r = @oci_execute($stdid); 
		$queryFetch=fetchcolumnListCond("EMAIL,PASSWORD,EMPLOYEENAME","CRMEMPLOYEELOGIN","WHERE EMAIL='".@$email."' AND STATUS='ACTIVE' ");
		
		$emailmessage = '';
		// send email to user with login details
		$Mailtemplates=GetTemplateContentByTemplateName('CORP_ACCOUNT_REACTIVATED');
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
		$message  = $Mailtemplates[0]['LARGECONTENT']?stripslashes($Mailtemplates[0]['LARGECONTENT']->load()):'';
		$headers  = 'From:'.stripslashes($Mailtemplates[0]['FROMNAME']).' <'.stripslashes($Mailtemplates[0]['FROMEMAIL']). "> \r\n" .
			'MIME-Version: 1.0' . "\r\n" .
			'Content-type: text/html; charset=iso-8859-1';
					// Mail it
		$to      = $email;
		if(mail($to, $subject, $message, $headers)){
		$usertype = 'CORPORATE';
		$reasontomail = 'ACCOUNTACTIVATIONBYUSER';
		$sendby = '';
		$entryTime=date('d-M-Y');
		$status = 'ACTIVE';
		$sendfrom = 'wellness@religare.com';
		$ipAddress = get_client_ip();
		$emailmessage = $message;
		//query to insert records
					$sqlLog="INSERT INTO PPHCMAILLOG (MAILID,MAILTO,SUBJECT,MESSAGE,USERTYPE,REASONTOMAIL,SENDBY,SENDDATE,STATUS,SENDFROM,IPADDRESS) values(PPHCMAILLOG_SEQ.nextval,q'[".$to."]','".@$subject."',:largecontent,'".@$usertype."','".@$reasontomail."','".@$sendby."','".@$entryTime."','".@$status."','".@$sendfrom."','".@$ipAddress."') "; 	
					$sqlLogExe = @oci_parse($conn, $sqlLog);
					oci_bind_by_name($sqlLogExe, ':largecontent', $message);
					$r = @oci_execute($sqlLogExe);
		}
	}
	if($record['USERTYPE'] =='RETAIL'){
		// update center table
		$status ='ACTIVE';
		$attempts = 0;
		$email = $record['EMAIL'];
		//$userid = $record['USERID'];
		$sql2 = "UPDATE RETUSERAUTH SET USERSTATUS='$status',UNSUCCESSFULATTEMPTS='$attempts' WHERE LOWER(USERNAME)= '".strtolower($email)."'";
		$stdid = @oci_parse($conn, $sql2);
		$r = @oci_execute($stdid); 
		// fetch login details
		$queryFetch=fetchcolumnListCond("USERNAME,PASSWORD","RETUSERAUTH","WHERE LOWER(USERNAME)='".strtolower(@$email)."' AND USERSTATUS='ACTIVE' ");
		require_once 'passwordverify/PkcsKeyGenerator.php';
		require_once 'passwordverify/DesEncryptor.php';
		require_once 'passwordverify/PbeWithMd5AndDes.php';
		$dbpassword=@$queryFetch[0]['PASSWORD'];
		$decryptPass = PbeWithMd5AndDes::decrypt($dbpassword, $keystring);
		$emailmessage = '';
		// send email to user with login details
		$Mailtemplates=GetTemplateContentByTemplateName('RET_ACCOUNT_REACTIVATED');
		$subject  = stripslashes($Mailtemplates[0]['SUBJECT']);
		$message  = $Mailtemplates[0]['LARGECONTENT']?stripslashes($Mailtemplates[0]['LARGECONTENT']->load()):'';
		$headers  = 'From:'.stripslashes($Mailtemplates[0]['FROMNAME']).' <'.stripslashes($Mailtemplates[0]['FROMEMAIL']). "> \r\n" .
			'MIME-Version: 1.0' . "\r\n" .
			'Content-type: text/html; charset=iso-8859-1';
		$to = $email;

		if(mail($to, $subject, $message, $headers)){
		$usertype = 'RETAIL';
		$reasontomail = 'ACCOUNTACTIVATIONBYUSER';
		$sendby = '';
		$entryTime=date('d-M-Y');
		$status = 'ACTIVE';
		$sendfrom = 'wellness@religare.com';
		$ipAddress = get_client_ip();
		$emailmessage = $message;
		//query to insert records
					$sqlLog="INSERT INTO PPHCMAILLOG (MAILID,MAILTO,SUBJECT,MESSAGE,USERTYPE,REASONTOMAIL,SENDBY,SENDDATE,STATUS,SENDFROM,IPADDRESS) values(PPHCMAILLOG_SEQ.nextval,q'[".$to."]','".@$subject."',:largecontent,'".@$usertype."','".@$reasontomail."','".@$sendby."','".@$entryTime."','".@$status."','".@$sendfrom."','".@$ipAddress."') "; 	
					$sqlLogExe = @oci_parse($conn, $sqlLog);
					oci_bind_by_name($sqlLogExe, ':largecontent', $message);
					$r = @oci_execute($sqlLogExe);
		}
	}
}
echo ($record['OTP'] == sanitize_data($_POST['code'])) ? 'success' : 'incorrectOTP';
exit;
?>